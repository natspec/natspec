package de.devboost.natspec.examples.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.devboost.natspec.examples.test.TestSupport;

/**
 * JUnit test case generated from the file #className#.natspec. 
 *
 * Never modify this file. It will be overwritten by any changes
 * in #className#.natspec. 
 */
public class _NatSpecTemplate {

	private TestSupport testSupport = null;

	@Test
	public void executeScript() throws Exception {
		/* @MethodBody */
	}
	
	@Before
	public void setUp() {
		testSupport = new TestSupport();
	}
	
	@After 
	public void shutdown() {
		if (testSupport != null)  
			testSupport = null;
	}
}
