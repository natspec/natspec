package de.devboost.natspec.examples.test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.devboost.natspec.examples.test.TestSupport;

/**
 * JUnit test case generated from the file #className#.natspec. 
 *
 * Never modify this file. It will be overwritten by any changes
 * in #className#.natspec. 
 */
public class Story_BasicMath {

	private TestSupport testSupport = null;

	@Test
	public void executeScript() throws Exception {
		// The code in this method is generated from: /de.devboost.natspec.examples/src-endusertests/de/devboost/natspec/examples/test/Story_BasicMath.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Add 3 and 4
		java.lang.Integer integer_3_4 = testSupport.add(3, 4);
		// Assert result equals 7
		testSupport.assertResultEquals(7, integer_3_4);
		// Subtract 7 from 10
		java.lang.Integer integer_7_10 = testSupport.subtract(7, 10);
		// Assert result equals 3
		testSupport.assertResultEquals(3, integer_7_10);
		
	}
	
	@Before
	public void setUp() {
		testSupport = new TestSupport();
	}
	
	@After 
	public void shutdown() {
		if (testSupport != null)  
			testSupport = null;
	}
}
