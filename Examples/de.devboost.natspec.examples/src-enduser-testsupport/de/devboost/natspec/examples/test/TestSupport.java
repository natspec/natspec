package de.devboost.natspec.examples.test;

import org.junit.Assert;

import de.devboost.natspec.annotations.TextSyntax;

/**
 * Helper class to map NatSpec sentences to java code.
 * 
 * Please see /de.devboost.natspec.examples/synonyms.txt for any word synonyms.
 * 
 * @author Christian Wende <christian.wende@devboost.de>
 */
public class TestSupport {

	/**
	 * Public constructor for test support class
	 */
	public TestSupport() {
		super();
	}

	/**
	 * Adds two integers.
	 * 
	 * @param a
	 *            first integer to add
	 * @param b
	 *            second integer to add
	 * @return the sum of the given integer values
	 */
	@TextSyntax("Add #1 and #2")
	public Integer add(int a, int b) {
		return a + b;
	}

	/**
	 * Subtracts two integers.
	 * 
	 * @param a
	 *            The first argument for subtraction
	 * @param b
	 *            The second argument for the subtraction
	 * @return
	 */
	@TextSyntax("Subtract #1 from #2")
	public Integer subtract(int a, int b) {
		return b - a;
	}

	/**
	 * Compares two integers for equality.
	 * 
	 * @param expected
	 *            the expected integer
	 * @param result
	 *            the calculated integer
	 */
	@TextSyntax("Assert result equals #1")
	public void assertResultEquals(Integer expected, Integer result) {
		Assert.assertEquals(expected, result);
	}
}
