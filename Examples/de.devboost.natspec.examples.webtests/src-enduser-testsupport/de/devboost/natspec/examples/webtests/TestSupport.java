package de.devboost.natspec.examples.webtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.FrameWindow;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.javascript.host.Event;

import de.devboost.natspec.annotations.TextSyntax;

public class TestSupport {

	public TestSupport() {
		super();
	}

	@TextSyntax("Open #1")
	public HtmlPage openUrl(String url) throws Exception {
		final WebClient webClient = new WebClient();
		final HtmlPage page = webClient.getPage("http://" + url);
		Thread.sleep(4000);
		return page;
	}

	@TextSyntax("Has input #2")
	public void hasTextfield(HtmlPage page, String fieldname) throws Exception {
		HtmlInput textfield = getInputForName(page, fieldname);

		Assert.assertNotNull("Expected input " + fieldname, textfield);
	}

	@TextSyntax("Set input #2 to #3")
	public void hasTextfield(HtmlPage page, String fieldname, String value)
			throws Exception {
		HtmlInput textfield = getInputForName(page, fieldname);
		textfield.setValueAttribute(value);
	}

	@TextSyntax("Submit form with id #2")
	public void submitFormWithId(HtmlPage page, String formid) throws Exception {
		DomElement elementForId = getElementForId(page, formid);
		if (elementForId == null || elementForId instanceof HtmlForm) {
			fail("Could not find form for " + formid);
		}
		HtmlForm form = (HtmlForm) elementForId;
		form.fireEvent(Event.TYPE_SUBMIT);
	}

	private DomElement getElementForId(HtmlPage page, String elementId) {
		DomElement elementById = page.getElementById(elementId);
		if (elementById == null) {
			List<FrameWindow> frames = page.getFrames();
			for (FrameWindow frameWindow : frames) {
				Page enclosedPage = frameWindow.getEnclosedPage();
				if (enclosedPage instanceof HtmlPage) {
					HtmlPage htmlPage = (HtmlPage) enclosedPage;
					DomElement elementForId = getElementForId(htmlPage,
							elementId);
					if (elementForId != null)
						return elementForId;
				}
			}
		}

		return elementById;
	}

	private HtmlInput getInputForName(HtmlPage page, String fieldname) {
		String xpathString = "//input[@name='" + fieldname + "']";
		List<?> input = page.getByXPath(xpathString);
		if (input.size() == 0) {
			input = searchInnerPages(page, xpathString);
		}
		HtmlInput textfield = (HtmlInput) input.get(0);
		return textfield;
	}

	private List<?> searchInnerPages(HtmlPage page, String xpath) {
		List<FrameWindow> frames = page.getFrames();
		List<?> input;
		for (FrameWindow frameWindow : frames) {
			Page enclosedPage = frameWindow.getEnclosedPage();
			if (enclosedPage instanceof HtmlPage) {
				HtmlPage innerPage = (HtmlPage) enclosedPage;
				input = innerPage.getByXPath(xpath);
				if (input.size() == 1) {
					return input;
				}
				List<?> searchInnerPages = searchInnerPages(innerPage, xpath);
				if (searchInnerPages != null && searchInnerPages.size() > 0)
					return searchInnerPages;

			}
		}
		return null;
	}

	@TextSyntax("Can download #2")
	public void getDownload(HtmlPage page, List<String> linkName)
			throws Exception {
		DomNodeList<DomElement> hrefs = page.getElementsByTagName("a");
		String joinedLinkName = StringUtils.join(linkName, " ");
		HtmlAnchor searched = null;

		for (DomElement domElement : hrefs) {
			if (domElement.getTextContent().contains(joinedLinkName)) {
				String attributeValue = domElement.getAttribute("href");
				searched = page.getAnchorByHref(attributeValue);
			}
		}
		assertNotNull("could not find download " + joinedLinkName, searched);
	}

	@TextSyntax("Has element #1")
	public void hasElement_(String elementName, HtmlPage page) throws Exception {
		DomElement elementForId = getElementForId(page, elementName);
		assertNotNull("Could not find element " + elementName, elementForId);
	}

	@TextSyntax("Set #1 to #2")
	public void setHeightTo(String fieldname, String value, HtmlPage page) {
		DomElement elementById = page.getElementById(fieldname);
		elementById.setTextContent(value);
	}
	
	@TextSyntax("Open URL #1")
	public HtmlUnitDriver openURL(String url) {
		HtmlUnitDriver driver = new HtmlUnitDriver();
		String baseUrl = url;
		driver.get(baseUrl);
		// driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}

	@TextSyntax("Set #1 to #2")
	public void setHeightTo(String name, String value, HtmlUnitDriver driver) {
		WebElement findElement = driver.findElement(By.name(name));
		findElement.clear();
		findElement.sendKeys(value);
	}

	@TextSyntax("Press #1")
	public void pressCalculateBMI(List<String> buttonName, HtmlUnitDriver driver) {
		WebElement findElement = driver.findElement(By.name("Submit"));
		findElement.submit();
	}

	@TextSyntax("The #3 of #1 is #2")
	public void attributeValueIs(String name, String value, String attributeName, HtmlUnitDriver driver) {
		WebElement findElement = driver.findElement(By.name(name));
		assertEquals(value, findElement.getAttribute(attributeName));
	}

}
