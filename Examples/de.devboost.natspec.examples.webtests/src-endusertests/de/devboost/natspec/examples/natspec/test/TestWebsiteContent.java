package de.devboost.natspec.examples.natspec.test;

import org.junit.Before;
import org.junit.Test;

import de.devboost.natspec.examples.webtests.TestSupport;

public class TestWebsiteContent {

	protected TestSupport testSupport;

	@Test
	public void executeScript() throws Exception {
		// The code in this method is generated from: /de.devboost.natspec.examples.webtests/src-endusertests/de/devboost/natspec/examples/natspec/test/TestWebsiteContent.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Open www.nat-spec.com
		com.gargoylesoftware.htmlunit.html.HtmlPage htmlPage_www_nat_spec_com = testSupport.openUrl("www.nat-spec.com");
		// Has element about
		testSupport.hasElement_("about", htmlPage_www_nat_spec_com);
		// Has element benefits
		testSupport.hasElement_("benefits", htmlPage_www_nat_spec_com);
		// Has element features
		testSupport.hasElement_("features", htmlPage_www_nat_spec_com);
		// Has element downloads
		testSupport.hasElement_("downloads", htmlPage_www_nat_spec_com);
		// Has element documentation
		testSupport.hasElement_("documentation", htmlPage_www_nat_spec_com);
		// Has element license
		testSupport.hasElement_("license", htmlPage_www_nat_spec_com);
		// Can download NatSpec License Agreement
		testSupport.getDownload(htmlPage_www_nat_spec_com, java.util.Arrays.asList(new java.lang.String[] {"NatSpec", "License", "Agreement"}));
		// Can download Windows 32bit
		testSupport.getDownload(htmlPage_www_nat_spec_com, java.util.Arrays.asList(new java.lang.String[] {"Windows", "32bit"}));
		// Can download Windows 64bit
		testSupport.getDownload(htmlPage_www_nat_spec_com, java.util.Arrays.asList(new java.lang.String[] {"Windows", "64bit"}));
		// Can download Mac OSX
		testSupport.getDownload(htmlPage_www_nat_spec_com, java.util.Arrays.asList(new java.lang.String[] {"Mac", "OSX"}));
		// Can download Linux 64bit
		testSupport.getDownload(htmlPage_www_nat_spec_com, java.util.Arrays.asList(new java.lang.String[] {"Linux", "64bit"}));
		
	}

	@Before
	public void setUp() {
		testSupport = new TestSupport();
	}
}
