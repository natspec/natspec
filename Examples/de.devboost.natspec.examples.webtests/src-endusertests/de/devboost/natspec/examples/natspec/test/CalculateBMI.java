package de.devboost.natspec.examples.natspec.test;

import org.junit.Before;
import org.junit.Test;

import de.devboost.natspec.examples.webtests.TestSupport;

public class CalculateBMI {

	protected TestSupport testSupport;

	@Test
	public void executeScript() throws Exception {
		// The code in this method is generated from: /de.devboost.natspec.examples.webtests/src-endusertests/de/devboost/natspec/examples/natspec/test/CalculateBMI.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Open URL http://localhost/xampp/bmi.php
		org.openqa.selenium.htmlunit.HtmlUnitDriver htmlUnitDriver_http___localhost_xampp_bmi_php = testSupport.openURL("http://localhost/xampp/bmi.php");
		// Set height to 200
		testSupport.setHeightTo("height", "200", htmlUnitDriver_http___localhost_xampp_bmi_php);
		// Set weight to 100
		testSupport.setHeightTo("weight", "100", htmlUnitDriver_http___localhost_xampp_bmi_php);
		// Press Calculate BMI
		testSupport.pressCalculateBMI(java.util.Arrays.asList(new java.lang.String[] {"Calculate", "BMI"}), htmlUnitDriver_http___localhost_xampp_bmi_php);
		// The Value of BMI is 25
		testSupport.attributeValueIs("BMI", "25", "Value", htmlUnitDriver_http___localhost_xampp_bmi_php);
		
	}

	@Before
	public void setUp() {
		testSupport = new TestSupport();
	}
}
