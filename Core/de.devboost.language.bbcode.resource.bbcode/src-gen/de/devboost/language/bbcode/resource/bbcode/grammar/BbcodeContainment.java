/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

public class BbcodeContainment extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeTerminal {
	
	private final EClass[] allowedTypes;
	
	public BbcodeContainment(EStructuralFeature feature, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, EClass[] allowedTypes, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.allowedTypes = allowedTypes;
	}
	
	public EClass[] getAllowedTypes() {
		return allowedTypes;
	}
	
	public String toString() {
		String typeRestrictions = null;
		if (allowedTypes != null && allowedTypes.length > 0) {
			typeRestrictions = de.devboost.language.bbcode.resource.bbcode.util.BbcodeStringUtil.explode(allowedTypes, ", ", new de.devboost.language.bbcode.resource.bbcode.IBbcodeFunction1<String, EClass>() {
				public String execute(EClass eClass) {
					return eClass.getName();
				}
			});
		}
		return getFeature().getName() + (typeRestrictions == null ? "" : "[" + typeRestrictions + "]");
	}
	
}
