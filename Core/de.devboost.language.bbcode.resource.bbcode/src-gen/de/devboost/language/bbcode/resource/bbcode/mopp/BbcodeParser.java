// $ANTLR 3.4

	package de.devboost.language.bbcode.resource.bbcode.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class BbcodeParser extends BbcodeANTLRParserBase {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "LINEBREAK", "TEXT", "WHITESPACE", "'[/b]'", "'[/color]'", "'[/i]'", "'[/s]'", "'[/u]'", "'[b]'", "'[color='", "'[i]'", "'[s]'", "'[u]'", "']'"
    };

    public static final int EOF=-1;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int LINEBREAK=4;
    public static final int TEXT=5;
    public static final int WHITESPACE=6;

    // delegates
    public BbcodeANTLRParserBase[] getDelegates() {
        return new BbcodeANTLRParserBase[] {};
    }

    // delegators


    public BbcodeParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public BbcodeParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
        this.state.initializeRuleMemo(20 + 1);
         

    }

    public String[] getTokenNames() { return BbcodeParser.tokenNames; }
    public String getGrammarFileName() { return "Bbcode.g"; }


    	private de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolverFactory tokenResolverFactory = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenResolverFactory();
    	
    	/**
    	 * the index of the last token that was handled by collectHiddenTokens()
    	 */
    	private int lastPosition;
    	
    	/**
    	 * A flag that indicates whether the parser should remember all expected elements.
    	 * This flag is set to true when using the parse for code completion. Otherwise it
    	 * is set to false.
    	 */
    	private boolean rememberExpectedElements = false;
    	
    	private Object parseToIndexTypeObject;
    	private int lastTokenIndex = 0;
    	
    	/**
    	 * A list of expected elements the were collected while parsing the input stream.
    	 * This list is only filled if <code>rememberExpectedElements</code> is set to
    	 * true.
    	 */
    	private List<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> expectedElements = new ArrayList<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal>();
    	
    	private int mismatchedTokenRecoveryTries = 0;
    	/**
    	 * A helper list to allow a lexer to pass errors to its parser
    	 */
    	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
    	
    	/**
    	 * Another helper list to allow a lexer to pass positions of errors to its parser
    	 */
    	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
    	
    	/**
    	 * A stack for incomplete objects. This stack is used filled when the parser is
    	 * used for code completion. Whenever the parser starts to read an object it is
    	 * pushed on the stack. Once the element was parser completely it is popped from
    	 * the stack.
    	 */
    	List<EObject> incompleteObjects = new ArrayList<EObject>();
    	
    	private int stopIncludingHiddenTokens;
    	private int stopExcludingHiddenTokens;
    	private int tokenIndexOfLastCompleteElement;
    	
    	private int expectedElementsIndexOfLastCompleteElement;
    	
    	/**
    	 * The offset indicating the cursor position when the parser is used for code
    	 * completion by calling parseToExpectedElements().
    	 */
    	private int cursorOffset;
    	
    	/**
    	 * The offset of the first hidden token of the last expected element. This offset
    	 * is used to discard expected elements, which are not needed for code completion.
    	 */
    	private int lastStartIncludingHidden;
    	
    	private de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap;
    	
    	private de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeSyntaxErrorMessageConverter(tokenNames);
    	
    	@Override
    	public void reportError(RecognitionException re) {
    		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    	}
    	
    	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
    		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
    			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
    				if (resource == null) {
    					// the resource can be null if the parser is used for code completion
    					return true;
    				}
    				resource.addProblem(new de.devboost.language.bbcode.resource.bbcode.IBbcodeProblem() {
    					public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity getSeverity() {
    						return de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity.ERROR;
    					}
    					public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType getType() {
    						return de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType.SYNTAX_ERROR;
    					}
    					public String getMessage() {
    						return errorMessage;
    					}
    					public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> getQuickFixes() {
    						return null;
    					}
    				}, column, line, startIndex, stopIndex);
    				return true;
    			}
    		});
    	}
    	
    	protected void addErrorToResource(de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeLocalizedMessage message) {
    		if (message == null) {
    			return;
    		}
    		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
    	}
    	
    	public void addExpectedElement(EClass eClass, int[] ids) {
    		if (!this.rememberExpectedElements) {
    			return;
    		}
    		int terminalID = ids[0];
    		int followSetID = ids[1];
    		de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement terminal = de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeFollowSetProvider.TERMINALS[terminalID];
    		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] containmentFeatures = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[ids.length - 2];
    		for (int i = 2; i < ids.length; i++) {
    			containmentFeatures[i - 2] = de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeFollowSetProvider.LINKS[ids[i]];
    		}
    		de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace containmentTrace = new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace(eClass, containmentFeatures);
    		EObject container = getLastIncompleteElement();
    		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal expectedElement = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal(container, terminal, followSetID, containmentTrace);
    		setPosition(expectedElement, input.index());
    		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
    		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
    			// clear list of expected elements
    			this.expectedElements.clear();
    			this.expectedElementsIndexOfLastCompleteElement = 0;
    		}
    		lastStartIncludingHidden = startIncludingHiddenTokens;
    		this.expectedElements.add(expectedElement);
    	}
    	
    	protected void collectHiddenTokens(EObject element) {
    	}
    	
    	protected void copyLocalizationInfos(final EObject source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
    			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
    				locationMap.setCharStart(target, locationMap.getCharStart(source));
    				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
    				locationMap.setColumn(target, locationMap.getColumn(source));
    				locationMap.setLine(target, locationMap.getLine(source));
    				return true;
    			}
    		});
    	}
    	
    	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
    			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
    				if (source == null) {
    					return true;
    				}
    				locationMap.setCharStart(target, source.getStartIndex());
    				locationMap.setCharEnd(target, source.getStopIndex());
    				locationMap.setColumn(target, source.getCharPositionInLine());
    				locationMap.setLine(target, source.getLine());
    				return true;
    			}
    		});
    	}
    	
    	/**
    	 * Sets the end character index and the last line for the given object in the
    	 * location map.
    	 */
    	protected void setLocalizationEnd(Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
    		if (disableLocationMap) {
    			return;
    		}
    		final de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap = this.locationMap;
    		if (locationMap == null) {
    			// the locationMap can be null if the parser is used for code completion
    			return;
    		}
    		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
    			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
    				locationMap.setCharEnd(object, endChar);
    				locationMap.setLine(object, endLine);
    				return true;
    			}
    		});
    	}
    	
    	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTextParser createInstance(InputStream actualInputStream, String encoding) {
    		try {
    			if (encoding == null) {
    				return new BbcodeParser(new CommonTokenStream(new BbcodeLexer(new ANTLRInputStream(actualInputStream))));
    			} else {
    				return new BbcodeParser(new CommonTokenStream(new BbcodeLexer(new ANTLRInputStream(actualInputStream, encoding))));
    			}
    		} catch (IOException e) {
    			new de.devboost.language.bbcode.resource.bbcode.util.BbcodeRuntimeUtil().logError("Error while creating parser.", e);
    			return null;
    		}
    	}
    	
    	/**
    	 * This default constructor is only used to call createInstance() on it.
    	 */
    	public BbcodeParser() {
    		super(null);
    	}
    	
    	protected EObject doParse() throws RecognitionException {
    		this.lastPosition = 0;
    		// required because the lexer class can not be subclassed
    		((BbcodeLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
    		((BbcodeLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
    		Object typeObject = getTypeObject();
    		if (typeObject == null) {
    			return start();
    		} else if (typeObject instanceof EClass) {
    			EClass type = (EClass) typeObject;
    			if (type.getInstanceClass() == de.devboost.language.bbcode.BBCodeText.class) {
    				return parse_de_devboost_language_bbcode_BBCodeText();
    			}
    			if (type.getInstanceClass() == de.devboost.language.bbcode.EmptyStyle.class) {
    				return parse_de_devboost_language_bbcode_EmptyStyle();
    			}
    			if (type.getInstanceClass() == de.devboost.language.bbcode.Bold.class) {
    				return parse_de_devboost_language_bbcode_Bold();
    			}
    			if (type.getInstanceClass() == de.devboost.language.bbcode.Italic.class) {
    				return parse_de_devboost_language_bbcode_Italic();
    			}
    			if (type.getInstanceClass() == de.devboost.language.bbcode.Strikethrough.class) {
    				return parse_de_devboost_language_bbcode_Strikethrough();
    			}
    			if (type.getInstanceClass() == de.devboost.language.bbcode.Underline.class) {
    				return parse_de_devboost_language_bbcode_Underline();
    			}
    			if (type.getInstanceClass() == de.devboost.language.bbcode.Color.class) {
    				return parse_de_devboost_language_bbcode_Color();
    			}
    		}
    		throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeUnexpectedContentTypeException(typeObject);
    	}
    	
    	public int getMismatchedTokenRecoveryTries() {
    		return mismatchedTokenRecoveryTries;
    	}
    	
    	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
    		mismatchedTokenRecoveryTries++;
    		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
    	}
    	
    	public Object getParseToIndexTypeObject() {
    		return parseToIndexTypeObject;
    	}
    	
    	protected Object getTypeObject() {
    		Object typeObject = getParseToIndexTypeObject();
    		if (typeObject != null) {
    			return typeObject;
    		}
    		Map<?,?> options = getOptions();
    		if (options != null) {
    			typeObject = options.get(de.devboost.language.bbcode.resource.bbcode.IBbcodeOptions.RESOURCE_CONTENT_TYPE);
    		}
    		return typeObject;
    	}
    	
    	/**
    	 * Implementation that calls {@link #doParse()} and handles the thrown
    	 * RecognitionExceptions.
    	 */
    	public de.devboost.language.bbcode.resource.bbcode.IBbcodeParseResult parse() {
    		// Reset parser state
    		terminateParsing = false;
    		postParseCommands = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>>();
    		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeParseResult parseResult = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeParseResult();
    		if (disableLocationMap) {
    			locationMap = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeDevNullLocationMap();
    		} else {
    			locationMap = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeLocationMap();
    		}
    		// Run parser
    		try {
    			EObject result =  doParse();
    			if (lexerExceptions.isEmpty()) {
    				parseResult.setRoot(result);
    				parseResult.setLocationMap(locationMap);
    			}
    		} catch (RecognitionException re) {
    			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
    		} catch (IllegalArgumentException iae) {
    			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
    				// can be caused if a null is set on EMF models where not allowed. this will just
    				// happen if other errors occurred before
    			} else {
    				iae.printStackTrace();
    			}
    		}
    		for (RecognitionException re : lexerExceptions) {
    			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
    		}
    		parseResult.getPostParseCommands().addAll(postParseCommands);
    		return parseResult;
    	}
    	
    	public List<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> parseToExpectedElements(EClass type, de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource dummyResource, int cursorOffset) {
    		this.rememberExpectedElements = true;
    		this.parseToIndexTypeObject = type;
    		this.cursorOffset = cursorOffset;
    		this.lastStartIncludingHidden = -1;
    		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
    		de.devboost.language.bbcode.resource.bbcode.IBbcodeParseResult result = parse();
    		for (EObject incompleteObject : incompleteObjects) {
    			Lexer lexer = (Lexer) tokenStream.getTokenSource();
    			int endChar = lexer.getCharIndex();
    			int endLine = lexer.getLine();
    			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
    		}
    		if (result != null) {
    			EObject root = result.getRoot();
    			if (root != null) {
    				dummyResource.getContentsInternal().add(root);
    			}
    			for (de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource> command : result.getPostParseCommands()) {
    				command.execute(dummyResource);
    			}
    		}
    		// remove all expected elements that were added after the last complete element
    		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
    		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
    		Set<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> currentFollowSet = new LinkedHashSet<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal>();
    		List<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> newFollowSet = new ArrayList<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal>();
    		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
    			de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal expectedElementI = expectedElements.get(i);
    			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
    				currentFollowSet.add(expectedElementI);
    			} else {
    				break;
    			}
    		}
    		int followSetID = 26;
    		int i;
    		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
    			CommonToken nextToken = (CommonToken) tokenStream.get(i);
    			if (nextToken.getType() < 0) {
    				break;
    			}
    			if (nextToken.getChannel() == 99) {
    				// hidden tokens do not reduce the follow set
    			} else {
    				// now that we have found the next visible token the position for that expected
    				// terminals can be set
    				for (de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal nextFollow : newFollowSet) {
    					lastTokenIndex = 0;
    					setPosition(nextFollow, i);
    				}
    				newFollowSet.clear();
    				// normal tokens do reduce the follow set - only elements that match the token are
    				// kept
    				for (de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal nextFollow : currentFollowSet) {
    					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
    						// keep this one - it matches
    						Collection<de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
    						for (de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]> newFollowerPair : newFollowers) {
    							de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement newFollower = newFollowerPair.getLeft();
    							EObject container = getLastIncompleteElement();
    							de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace containmentTrace = new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace(null, newFollowerPair.getRight());
    							de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal newFollowTerminal = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal(container, newFollower, followSetID, containmentTrace);
    							newFollowSet.add(newFollowTerminal);
    							expectedElements.add(newFollowTerminal);
    						}
    					}
    				}
    				currentFollowSet.clear();
    				currentFollowSet.addAll(newFollowSet);
    			}
    			followSetID++;
    		}
    		// after the last token in the stream we must set the position for the elements
    		// that were added during the last iteration of the loop
    		for (de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal nextFollow : newFollowSet) {
    			lastTokenIndex = 0;
    			setPosition(nextFollow, i);
    		}
    		return this.expectedElements;
    	}
    	
    	public void setPosition(de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal expectedElement, int tokenIndex) {
    		int currentIndex = Math.max(0, tokenIndex);
    		for (int index = lastTokenIndex; index < currentIndex; index++) {
    			if (index >= input.size()) {
    				break;
    			}
    			CommonToken tokenAtIndex = (CommonToken) input.get(index);
    			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
    				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
    			}
    		}
    		lastTokenIndex = Math.max(0, currentIndex);
    		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
    	}
    	
    	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
    		if (!rememberExpectedElements) {
    			return super.recoverFromMismatchedToken(input, ttype, follow);
    		} else {
    			return null;
    		}
    	}
    	
    	private void startIncompleteElement(Object object) {
    		if (object instanceof EObject) {
    			this.incompleteObjects.add((EObject) object);
    		}
    	}
    	
    	private void completedElement(Object object, boolean isContainment) {
    		if (isContainment && !this.incompleteObjects.isEmpty()) {
    			boolean exists = this.incompleteObjects.remove(object);
    			if (!exists) {
    			}
    		}
    		if (object instanceof EObject) {
    			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
    			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
    		}
    	}
    	
    	private EObject getLastIncompleteElement() {
    		if (incompleteObjects.isEmpty()) {
    			return null;
    		}
    		return incompleteObjects.get(incompleteObjects.size() - 1);
    	}
    	



    // $ANTLR start "start"
    // Bbcode.g:498:1: start returns [ EObject element = null] : (c0= parse_de_devboost_language_bbcode_BBCodeText ) EOF ;
    public final EObject start() throws RecognitionException {
        EObject element =  null;

        int start_StartIndex = input.index();

        de.devboost.language.bbcode.BBCodeText c0 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 1) ) { return element; }

            // Bbcode.g:499:2: ( (c0= parse_de_devboost_language_bbcode_BBCodeText ) EOF )
            // Bbcode.g:500:2: (c0= parse_de_devboost_language_bbcode_BBCodeText ) EOF
            {
            if ( state.backtracking==0 ) {
            		// follow set for start rule(s)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[0]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[1]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[2]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[3]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[4]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[5]);
            		expectedElementsIndexOfLastCompleteElement = 0;
            	}

            // Bbcode.g:510:2: (c0= parse_de_devboost_language_bbcode_BBCodeText )
            // Bbcode.g:511:3: c0= parse_de_devboost_language_bbcode_BBCodeText
            {
            pushFollow(FOLLOW_parse_de_devboost_language_bbcode_BBCodeText_in_start82);
            c0=parse_de_devboost_language_bbcode_BBCodeText();

            state._fsp--;
            if (state.failed) return element;

            if ( state.backtracking==0 ) { element = c0; }

            }


            match(input,EOF,FOLLOW_EOF_in_start89); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		retrieveLayoutInformation(element, null, null, false);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 1, start_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "start"



    // $ANTLR start "parse_de_devboost_language_bbcode_BBCodeText"
    // Bbcode.g:519:1: parse_de_devboost_language_bbcode_BBCodeText returns [de.devboost.language.bbcode.BBCodeText element = null] : ( ( (a0_0= parse_de_devboost_language_bbcode_Style ) ) )+ ;
    public final de.devboost.language.bbcode.BBCodeText parse_de_devboost_language_bbcode_BBCodeText() throws RecognitionException {
        de.devboost.language.bbcode.BBCodeText element =  null;

        int parse_de_devboost_language_bbcode_BBCodeText_StartIndex = input.index();

        de.devboost.language.bbcode.Style a0_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 2) ) { return element; }

            // Bbcode.g:522:2: ( ( ( (a0_0= parse_de_devboost_language_bbcode_Style ) ) )+ )
            // Bbcode.g:523:2: ( ( (a0_0= parse_de_devboost_language_bbcode_Style ) ) )+
            {
            // Bbcode.g:523:2: ( ( (a0_0= parse_de_devboost_language_bbcode_Style ) ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==TEXT||(LA1_0 >= 12 && LA1_0 <= 16)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Bbcode.g:524:3: ( (a0_0= parse_de_devboost_language_bbcode_Style ) )
            	    {
            	    // Bbcode.g:524:3: ( (a0_0= parse_de_devboost_language_bbcode_Style ) )
            	    // Bbcode.g:525:4: (a0_0= parse_de_devboost_language_bbcode_Style )
            	    {
            	    // Bbcode.g:525:4: (a0_0= parse_de_devboost_language_bbcode_Style )
            	    // Bbcode.g:526:5: a0_0= parse_de_devboost_language_bbcode_Style
            	    {
            	    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_BBCodeText130);
            	    a0_0=parse_de_devboost_language_bbcode_Style();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBBCodeText();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a0_0 != null) {
            	    						if (a0_0 != null) {
            	    							Object value = a0_0;
            	    							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.BB_CODE_TEXT__STYLE_PARTS, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_0_0_0_0_0_0_0, a0_0, true);
            	    						copyLocalizationInfos(a0_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[6]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[7]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[8]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[9]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[10]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[11]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
            	    if (state.backtracking>0) {state.failed=true; return element;}
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[12]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[13]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[14]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[15]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[16]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[17]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 2, parse_de_devboost_language_bbcode_BBCodeText_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_BBCodeText"



    // $ANTLR start "parse_de_devboost_language_bbcode_EmptyStyle"
    // Bbcode.g:570:1: parse_de_devboost_language_bbcode_EmptyStyle returns [de.devboost.language.bbcode.EmptyStyle element = null] : (a0= TEXT ) ;
    public final de.devboost.language.bbcode.EmptyStyle parse_de_devboost_language_bbcode_EmptyStyle() throws RecognitionException {
        de.devboost.language.bbcode.EmptyStyle element =  null;

        int parse_de_devboost_language_bbcode_EmptyStyle_StartIndex = input.index();

        Token a0=null;



        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 3) ) { return element; }

            // Bbcode.g:573:2: ( (a0= TEXT ) )
            // Bbcode.g:574:2: (a0= TEXT )
            {
            // Bbcode.g:574:2: (a0= TEXT )
            // Bbcode.g:575:3: a0= TEXT
            {
            a0=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_de_devboost_language_bbcode_EmptyStyle190); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createEmptyStyle();
            				startIncompleteElement(element);
            			}
            			if (a0 != null) {
            				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.EMPTY_STYLE__TEXT), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.EMPTY_STYLE__TEXT), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_1_0_0_0, resolved, true);
            				copyLocalizationInfos((CommonToken) a0, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[18]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[19]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[20]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[21]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[22]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[23]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[24]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[25]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[26]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[27]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[28]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 3, parse_de_devboost_language_bbcode_EmptyStyle_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_EmptyStyle"



    // $ANTLR start "parse_de_devboost_language_bbcode_Bold"
    // Bbcode.g:622:1: parse_de_devboost_language_bbcode_Bold returns [de.devboost.language.bbcode.Bold element = null] : a0= '[b]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/b]' ;
    public final de.devboost.language.bbcode.Bold parse_de_devboost_language_bbcode_Bold() throws RecognitionException {
        de.devboost.language.bbcode.Bold element =  null;

        int parse_de_devboost_language_bbcode_Bold_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        de.devboost.language.bbcode.Style a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 4) ) { return element; }

            // Bbcode.g:625:2: (a0= '[b]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/b]' )
            // Bbcode.g:626:2: a0= '[b]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/b]'
            {
            a0=(Token)match(input,12,FOLLOW_12_in_parse_de_devboost_language_bbcode_Bold226); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBold();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[29]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[30]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[31]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[32]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[33]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[34]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[35]);
            	}

            // Bbcode.g:646:2: ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==TEXT||(LA2_0 >= 12 && LA2_0 <= 16)) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // Bbcode.g:647:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    {
            	    // Bbcode.g:647:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    // Bbcode.g:648:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    {
            	    // Bbcode.g:648:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    // Bbcode.g:649:5: a1_0= parse_de_devboost_language_bbcode_Style
            	    {
            	    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Bold255);
            	    a1_0=parse_de_devboost_language_bbcode_Style();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBold();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a1_0 != null) {
            	    						if (a1_0 != null) {
            	    							Object value = a1_0;
            	    							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.BOLD__CHILDREN, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_1_0_0_0, a1_0, true);
            	    						copyLocalizationInfos(a1_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[36]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[37]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[38]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[39]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[40]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[41]);
            	    				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[42]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[43]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[44]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[45]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[46]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[47]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[48]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[49]);
            	}

            a2=(Token)match(input,7,FOLLOW_7_in_parse_de_devboost_language_bbcode_Bold296); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBold();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[50]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[51]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[52]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[53]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[54]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[55]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[56]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[57]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[58]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[59]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[60]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 4, parse_de_devboost_language_bbcode_Bold_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_Bold"



    // $ANTLR start "parse_de_devboost_language_bbcode_Italic"
    // Bbcode.g:719:1: parse_de_devboost_language_bbcode_Italic returns [de.devboost.language.bbcode.Italic element = null] : a0= '[i]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/i]' ;
    public final de.devboost.language.bbcode.Italic parse_de_devboost_language_bbcode_Italic() throws RecognitionException {
        de.devboost.language.bbcode.Italic element =  null;

        int parse_de_devboost_language_bbcode_Italic_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        de.devboost.language.bbcode.Style a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 5) ) { return element; }

            // Bbcode.g:722:2: (a0= '[i]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/i]' )
            // Bbcode.g:723:2: a0= '[i]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/i]'
            {
            a0=(Token)match(input,14,FOLLOW_14_in_parse_de_devboost_language_bbcode_Italic325); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createItalic();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[61]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[62]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[63]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[64]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[65]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[66]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[67]);
            	}

            // Bbcode.g:743:2: ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==TEXT||(LA3_0 >= 12 && LA3_0 <= 16)) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // Bbcode.g:744:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    {
            	    // Bbcode.g:744:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    // Bbcode.g:745:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    {
            	    // Bbcode.g:745:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    // Bbcode.g:746:5: a1_0= parse_de_devboost_language_bbcode_Style
            	    {
            	    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Italic354);
            	    a1_0=parse_de_devboost_language_bbcode_Style();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createItalic();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a1_0 != null) {
            	    						if (a1_0 != null) {
            	    							Object value = a1_0;
            	    							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.ITALIC__CHILDREN, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_1_0_0_0, a1_0, true);
            	    						copyLocalizationInfos(a1_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[68]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[69]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[70]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[71]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[72]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[73]);
            	    				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[74]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[75]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[76]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[77]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[78]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[79]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[80]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[81]);
            	}

            a2=(Token)match(input,9,FOLLOW_9_in_parse_de_devboost_language_bbcode_Italic395); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createItalic();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[82]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[83]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[84]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[85]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[86]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[87]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[88]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[89]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[90]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[91]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[92]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 5, parse_de_devboost_language_bbcode_Italic_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_Italic"



    // $ANTLR start "parse_de_devboost_language_bbcode_Strikethrough"
    // Bbcode.g:816:1: parse_de_devboost_language_bbcode_Strikethrough returns [de.devboost.language.bbcode.Strikethrough element = null] : a0= '[s]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/s]' ;
    public final de.devboost.language.bbcode.Strikethrough parse_de_devboost_language_bbcode_Strikethrough() throws RecognitionException {
        de.devboost.language.bbcode.Strikethrough element =  null;

        int parse_de_devboost_language_bbcode_Strikethrough_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        de.devboost.language.bbcode.Style a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 6) ) { return element; }

            // Bbcode.g:819:2: (a0= '[s]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/s]' )
            // Bbcode.g:820:2: a0= '[s]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/s]'
            {
            a0=(Token)match(input,15,FOLLOW_15_in_parse_de_devboost_language_bbcode_Strikethrough424); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createStrikethrough();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[93]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[94]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[95]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[96]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[97]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[98]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[99]);
            	}

            // Bbcode.g:840:2: ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==TEXT||(LA4_0 >= 12 && LA4_0 <= 16)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // Bbcode.g:841:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    {
            	    // Bbcode.g:841:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    // Bbcode.g:842:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    {
            	    // Bbcode.g:842:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    // Bbcode.g:843:5: a1_0= parse_de_devboost_language_bbcode_Style
            	    {
            	    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Strikethrough453);
            	    a1_0=parse_de_devboost_language_bbcode_Style();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createStrikethrough();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a1_0 != null) {
            	    						if (a1_0 != null) {
            	    							Object value = a1_0;
            	    							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.STRIKETHROUGH__CHILDREN, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_1_0_0_0, a1_0, true);
            	    						copyLocalizationInfos(a1_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[100]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[101]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[102]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[103]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[104]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[105]);
            	    				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[106]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[107]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[108]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[109]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[110]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[111]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[112]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[113]);
            	}

            a2=(Token)match(input,10,FOLLOW_10_in_parse_de_devboost_language_bbcode_Strikethrough494); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createStrikethrough();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[114]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[115]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[116]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[117]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[118]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[119]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[120]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[121]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[122]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[123]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[124]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 6, parse_de_devboost_language_bbcode_Strikethrough_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_Strikethrough"



    // $ANTLR start "parse_de_devboost_language_bbcode_Underline"
    // Bbcode.g:913:1: parse_de_devboost_language_bbcode_Underline returns [de.devboost.language.bbcode.Underline element = null] : a0= '[u]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/u]' ;
    public final de.devboost.language.bbcode.Underline parse_de_devboost_language_bbcode_Underline() throws RecognitionException {
        de.devboost.language.bbcode.Underline element =  null;

        int parse_de_devboost_language_bbcode_Underline_StartIndex = input.index();

        Token a0=null;
        Token a2=null;
        de.devboost.language.bbcode.Style a1_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 7) ) { return element; }

            // Bbcode.g:916:2: (a0= '[u]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/u]' )
            // Bbcode.g:917:2: a0= '[u]' ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )* a2= '[/u]'
            {
            a0=(Token)match(input,16,FOLLOW_16_in_parse_de_devboost_language_bbcode_Underline523); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createUnderline();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[125]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[126]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[127]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[128]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[129]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[130]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[131]);
            	}

            // Bbcode.g:937:2: ( ( (a1_0= parse_de_devboost_language_bbcode_Style ) ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==TEXT||(LA5_0 >= 12 && LA5_0 <= 16)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // Bbcode.g:938:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    {
            	    // Bbcode.g:938:3: ( (a1_0= parse_de_devboost_language_bbcode_Style ) )
            	    // Bbcode.g:939:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    {
            	    // Bbcode.g:939:4: (a1_0= parse_de_devboost_language_bbcode_Style )
            	    // Bbcode.g:940:5: a1_0= parse_de_devboost_language_bbcode_Style
            	    {
            	    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Underline552);
            	    a1_0=parse_de_devboost_language_bbcode_Style();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createUnderline();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a1_0 != null) {
            	    						if (a1_0 != null) {
            	    							Object value = a1_0;
            	    							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.UNDERLINE__CHILDREN, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_1_0_0_0, a1_0, true);
            	    						copyLocalizationInfos(a1_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[132]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[133]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[134]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[135]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[136]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[137]);
            	    				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[138]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[139]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[140]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[141]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[142]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[143]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[144]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[145]);
            	}

            a2=(Token)match(input,11,FOLLOW_11_in_parse_de_devboost_language_bbcode_Underline593); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createUnderline();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[146]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[147]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[148]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[149]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[150]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[151]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[152]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[153]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[154]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[155]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[156]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 7, parse_de_devboost_language_bbcode_Underline_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_Underline"



    // $ANTLR start "parse_de_devboost_language_bbcode_Color"
    // Bbcode.g:1010:1: parse_de_devboost_language_bbcode_Color returns [de.devboost.language.bbcode.Color element = null] : a0= '[color=' (a1= TEXT ) a2= ']' ( ( (a3_0= parse_de_devboost_language_bbcode_Style ) ) )* a4= '[/color]' ;
    public final de.devboost.language.bbcode.Color parse_de_devboost_language_bbcode_Color() throws RecognitionException {
        de.devboost.language.bbcode.Color element =  null;

        int parse_de_devboost_language_bbcode_Color_StartIndex = input.index();

        Token a0=null;
        Token a1=null;
        Token a2=null;
        Token a4=null;
        de.devboost.language.bbcode.Style a3_0 =null;




        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 8) ) { return element; }

            // Bbcode.g:1013:2: (a0= '[color=' (a1= TEXT ) a2= ']' ( ( (a3_0= parse_de_devboost_language_bbcode_Style ) ) )* a4= '[/color]' )
            // Bbcode.g:1014:2: a0= '[color=' (a1= TEXT ) a2= ']' ( ( (a3_0= parse_de_devboost_language_bbcode_Style ) ) )* a4= '[/color]'
            {
            a0=(Token)match(input,13,FOLLOW_13_in_parse_de_devboost_language_bbcode_Color622); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_0, null, true);
            		copyLocalizationInfos((CommonToken)a0, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[157]);
            	}

            // Bbcode.g:1028:2: (a1= TEXT )
            // Bbcode.g:1029:3: a1= TEXT
            {
            a1=(Token)match(input,TEXT,FOLLOW_TEXT_in_parse_de_devboost_language_bbcode_Color640); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            			if (terminateParsing) {
            				throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            			}
            			if (element == null) {
            				element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
            				startIncompleteElement(element);
            			}
            			if (a1 != null) {
            				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
            				tokenResolver.setOptions(getOptions());
            				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolveResult result = getFreshTokenResolveResult();
            				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.COLOR__VALUE), result);
            				Object resolvedObject = result.getResolvedToken();
            				if (resolvedObject == null) {
            					addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
            				}
            				java.lang.String resolved = (java.lang.String) resolvedObject;
            				if (resolved != null) {
            					Object value = resolved;
            					element.eSet(element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.COLOR__VALUE), value);
            					completedElement(value, false);
            				}
            				collectHiddenTokens(element);
            				retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_1, resolved, true);
            				copyLocalizationInfos((CommonToken) a1, element);
            			}
            		}

            }


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[158]);
            	}

            a2=(Token)match(input,17,FOLLOW_17_in_parse_de_devboost_language_bbcode_Color661); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_2, null, true);
            		copyLocalizationInfos((CommonToken)a2, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[159]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[160]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[161]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[162]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[163]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[164]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[165]);
            	}

            // Bbcode.g:1084:2: ( ( (a3_0= parse_de_devboost_language_bbcode_Style ) ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==TEXT||(LA6_0 >= 12 && LA6_0 <= 16)) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // Bbcode.g:1085:3: ( (a3_0= parse_de_devboost_language_bbcode_Style ) )
            	    {
            	    // Bbcode.g:1085:3: ( (a3_0= parse_de_devboost_language_bbcode_Style ) )
            	    // Bbcode.g:1086:4: (a3_0= parse_de_devboost_language_bbcode_Style )
            	    {
            	    // Bbcode.g:1086:4: (a3_0= parse_de_devboost_language_bbcode_Style )
            	    // Bbcode.g:1087:5: a3_0= parse_de_devboost_language_bbcode_Style
            	    {
            	    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Color690);
            	    a3_0=parse_de_devboost_language_bbcode_Style();

            	    state._fsp--;
            	    if (state.failed) return element;

            	    if ( state.backtracking==0 ) {
            	    					if (terminateParsing) {
            	    						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
            	    					}
            	    					if (element == null) {
            	    						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
            	    						startIncompleteElement(element);
            	    					}
            	    					if (a3_0 != null) {
            	    						if (a3_0 != null) {
            	    							Object value = a3_0;
            	    							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.COLOR__CHILDREN, value);
            	    							completedElement(value, true);
            	    						}
            	    						collectHiddenTokens(element);
            	    						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_3_0_0_0, a3_0, true);
            	    						copyLocalizationInfos(a3_0, element);
            	    					}
            	    				}

            	    }


            	    if ( state.backtracking==0 ) {
            	    				// expected elements (follow set)
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[166]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[167]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[168]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[169]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[170]);
            	    				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[171]);
            	    				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[172]);
            	    			}

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[173]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[174]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[175]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[176]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[177]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[178]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[179]);
            	}

            a4=(Token)match(input,8,FOLLOW_8_in_parse_de_devboost_language_bbcode_Color731); if (state.failed) return element;

            if ( state.backtracking==0 ) {
            		if (element == null) {
            			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
            			startIncompleteElement(element);
            		}
            		collectHiddenTokens(element);
            		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_4, null, true);
            		copyLocalizationInfos((CommonToken)a4, element);
            	}

            if ( state.backtracking==0 ) {
            		// expected elements (follow set)
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[180]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[181]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[182]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[183]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[184]);
            		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[185]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[186]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[187]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[188]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[189]);
            		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[190]);
            	}

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 8, parse_de_devboost_language_bbcode_Color_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_Color"



    // $ANTLR start "parse_de_devboost_language_bbcode_Style"
    // Bbcode.g:1157:1: parse_de_devboost_language_bbcode_Style returns [de.devboost.language.bbcode.Style element = null] : (c0= parse_de_devboost_language_bbcode_EmptyStyle |c1= parse_de_devboost_language_bbcode_Bold |c2= parse_de_devboost_language_bbcode_Italic |c3= parse_de_devboost_language_bbcode_Strikethrough |c4= parse_de_devboost_language_bbcode_Underline |c5= parse_de_devboost_language_bbcode_Color );
    public final de.devboost.language.bbcode.Style parse_de_devboost_language_bbcode_Style() throws RecognitionException {
        de.devboost.language.bbcode.Style element =  null;

        int parse_de_devboost_language_bbcode_Style_StartIndex = input.index();

        de.devboost.language.bbcode.EmptyStyle c0 =null;

        de.devboost.language.bbcode.Bold c1 =null;

        de.devboost.language.bbcode.Italic c2 =null;

        de.devboost.language.bbcode.Strikethrough c3 =null;

        de.devboost.language.bbcode.Underline c4 =null;

        de.devboost.language.bbcode.Color c5 =null;


        try {
            if ( state.backtracking>0 && alreadyParsedRule(input, 9) ) { return element; }

            // Bbcode.g:1158:2: (c0= parse_de_devboost_language_bbcode_EmptyStyle |c1= parse_de_devboost_language_bbcode_Bold |c2= parse_de_devboost_language_bbcode_Italic |c3= parse_de_devboost_language_bbcode_Strikethrough |c4= parse_de_devboost_language_bbcode_Underline |c5= parse_de_devboost_language_bbcode_Color )
            int alt7=6;
            switch ( input.LA(1) ) {
            case TEXT:
                {
                alt7=1;
                }
                break;
            case 12:
                {
                alt7=2;
                }
                break;
            case 14:
                {
                alt7=3;
                }
                break;
            case 15:
                {
                alt7=4;
                }
                break;
            case 16:
                {
                alt7=5;
                }
                break;
            case 13:
                {
                alt7=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return element;}
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;

            }

            switch (alt7) {
                case 1 :
                    // Bbcode.g:1159:2: c0= parse_de_devboost_language_bbcode_EmptyStyle
                    {
                    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_EmptyStyle_in_parse_de_devboost_language_bbcode_Style756);
                    c0=parse_de_devboost_language_bbcode_EmptyStyle();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c0; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 2 :
                    // Bbcode.g:1160:4: c1= parse_de_devboost_language_bbcode_Bold
                    {
                    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Bold_in_parse_de_devboost_language_bbcode_Style766);
                    c1=parse_de_devboost_language_bbcode_Bold();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c1; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 3 :
                    // Bbcode.g:1161:4: c2= parse_de_devboost_language_bbcode_Italic
                    {
                    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Italic_in_parse_de_devboost_language_bbcode_Style776);
                    c2=parse_de_devboost_language_bbcode_Italic();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c2; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 4 :
                    // Bbcode.g:1162:4: c3= parse_de_devboost_language_bbcode_Strikethrough
                    {
                    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Strikethrough_in_parse_de_devboost_language_bbcode_Style786);
                    c3=parse_de_devboost_language_bbcode_Strikethrough();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c3; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 5 :
                    // Bbcode.g:1163:4: c4= parse_de_devboost_language_bbcode_Underline
                    {
                    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Underline_in_parse_de_devboost_language_bbcode_Style796);
                    c4=parse_de_devboost_language_bbcode_Underline();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c4; /* this is a subclass or primitive expression choice */ }

                    }
                    break;
                case 6 :
                    // Bbcode.g:1164:4: c5= parse_de_devboost_language_bbcode_Color
                    {
                    pushFollow(FOLLOW_parse_de_devboost_language_bbcode_Color_in_parse_de_devboost_language_bbcode_Style806);
                    c5=parse_de_devboost_language_bbcode_Color();

                    state._fsp--;
                    if (state.failed) return element;

                    if ( state.backtracking==0 ) { element = c5; /* this is a subclass or primitive expression choice */ }

                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }

        finally {
        	// do for sure before leaving
            if ( state.backtracking>0 ) { memoize(input, 9, parse_de_devboost_language_bbcode_Style_StartIndex); }

        }
        return element;
    }
    // $ANTLR end "parse_de_devboost_language_bbcode_Style"

    // Delegated rules


 

    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_BBCodeText_in_start82 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_start89 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_BBCodeText130 = new BitSet(new long[]{0x000000000001F022L});
    public static final BitSet FOLLOW_TEXT_in_parse_de_devboost_language_bbcode_EmptyStyle190 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_parse_de_devboost_language_bbcode_Bold226 = new BitSet(new long[]{0x000000000001F0A0L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Bold255 = new BitSet(new long[]{0x000000000001F0A0L});
    public static final BitSet FOLLOW_7_in_parse_de_devboost_language_bbcode_Bold296 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_parse_de_devboost_language_bbcode_Italic325 = new BitSet(new long[]{0x000000000001F220L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Italic354 = new BitSet(new long[]{0x000000000001F220L});
    public static final BitSet FOLLOW_9_in_parse_de_devboost_language_bbcode_Italic395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_parse_de_devboost_language_bbcode_Strikethrough424 = new BitSet(new long[]{0x000000000001F420L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Strikethrough453 = new BitSet(new long[]{0x000000000001F420L});
    public static final BitSet FOLLOW_10_in_parse_de_devboost_language_bbcode_Strikethrough494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_parse_de_devboost_language_bbcode_Underline523 = new BitSet(new long[]{0x000000000001F820L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Underline552 = new BitSet(new long[]{0x000000000001F820L});
    public static final BitSet FOLLOW_11_in_parse_de_devboost_language_bbcode_Underline593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_parse_de_devboost_language_bbcode_Color622 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_TEXT_in_parse_de_devboost_language_bbcode_Color640 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_parse_de_devboost_language_bbcode_Color661 = new BitSet(new long[]{0x000000000001F120L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Style_in_parse_de_devboost_language_bbcode_Color690 = new BitSet(new long[]{0x000000000001F120L});
    public static final BitSet FOLLOW_8_in_parse_de_devboost_language_bbcode_Color731 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_EmptyStyle_in_parse_de_devboost_language_bbcode_Style756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Bold_in_parse_de_devboost_language_bbcode_Style766 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Italic_in_parse_de_devboost_language_bbcode_Style776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Strikethrough_in_parse_de_devboost_language_bbcode_Style786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Underline_in_parse_de_devboost_language_bbcode_Style796 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_parse_de_devboost_language_bbcode_Color_in_parse_de_devboost_language_bbcode_Style806 = new BitSet(new long[]{0x0000000000000002L});

}