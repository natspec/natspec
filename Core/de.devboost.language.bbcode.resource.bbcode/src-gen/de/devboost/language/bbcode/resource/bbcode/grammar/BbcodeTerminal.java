/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import org.eclipse.emf.ecore.EStructuralFeature;

public class BbcodeTerminal extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement {
	
	private final EStructuralFeature feature;
	private final int mandatoryOccurencesAfter;
	
	public BbcodeTerminal(EStructuralFeature feature, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, int mandatoryOccurencesAfter) {
		super(cardinality, null);
		this.feature = feature;
		this.mandatoryOccurencesAfter = mandatoryOccurencesAfter;
	}
	
	public EStructuralFeature getFeature() {
		return feature;
	}
	
	public int getMandatoryOccurencesAfter() {
		return mandatoryOccurencesAfter;
	}
	
	public String toString() {
		return feature.getName() + "[]";
	}
	
}
