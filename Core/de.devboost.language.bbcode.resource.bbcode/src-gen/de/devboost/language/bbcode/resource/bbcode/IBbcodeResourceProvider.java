/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;


/**
 * Implementors of this interface provide an EMF resource.
 */
public interface IBbcodeResourceProvider {
	
	/**
	 * Returns the resource.
	 */
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource getResource();
	
}
