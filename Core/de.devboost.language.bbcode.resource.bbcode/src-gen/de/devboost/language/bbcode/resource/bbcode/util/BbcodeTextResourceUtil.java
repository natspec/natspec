/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.util;

import java.io.File;
import java.util.Map;
import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;

/**
 * Class BbcodeTextResourceUtil can be used to perform common tasks on text
 * resources, such as loading and saving resources, as well as, checking them for
 * errors. This class is deprecated and has been replaced by
 * de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.
 */
public class BbcodeTextResourceUtil {
	
	/**
	 * Use
	 * de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated
	public static de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResource getResource(IFile file) {
		return new de.devboost.language.bbcode.resource.bbcode.util.BbcodeEclipseProxy().getResource(file);
	}
	
	/**
	 * Use
	 * de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated
	public static de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResource getResource(File file, Map<?,?> options) {
		return de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.getResource(file, options);
	}
	
	/**
	 * Use
	 * de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated
	public static de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResource getResource(URI uri) {
		return de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.getResource(uri);
	}
	
	/**
	 * Use
	 * de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.getResource(
	 * ) instead.
	 */
	@Deprecated
	public static de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResource getResource(URI uri, Map<?,?> options) {
		return de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil.getResource(uri, options);
	}
	
}
