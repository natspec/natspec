/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;

public class BbcodeGrammarInformationProvider {
	
	public final static EStructuralFeature ANONYMOUS_FEATURE = EcoreFactory.eINSTANCE.createEAttribute();
	static {
		ANONYMOUS_FEATURE.setName("_");
	}
	
	public final static BbcodeGrammarInformationProvider INSTANCE = new BbcodeGrammarInformationProvider();
	
	private Set<String> keywords;
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment BBCODE_0_0_0_0_0_0_0 = INSTANCE.getBBCODE_0_0_0_0_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment getBBCODE_0_0_0_0_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.BB_CODE_TEXT__STYLE_PARTS), de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, new EClass[] {de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStyle(), }, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_0_0_0_0_0_0 = INSTANCE.getBBCODE_0_0_0_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_0_0_0_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_0_0_0_0_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_0_0_0_0_0 = INSTANCE.getBBCODE_0_0_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_0_0_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_0_0_0_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound BBCODE_0_0_0_0 = INSTANCE.getBBCODE_0_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound getBBCODE_0_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound(BBCODE_0_0_0_0_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.PLUS);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_0_0_0 = INSTANCE.getBBCODE_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_0_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_0_0 = INSTANCE.getBBCODE_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule BBCODE_0 = INSTANCE.getBBCODE_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule getBBCODE_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), BBCODE_0_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder BBCODE_1_0_0_0 = INSTANCE.getBBCODE_1_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder getBBCODE_1_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.EMPTY_STYLE__TEXT), "TEXT", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_1_0_0 = INSTANCE.getBBCODE_1_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_1_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_1_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_1_0 = INSTANCE.getBBCODE_1_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_1_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_1_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule BBCODE_1 = INSTANCE.getBBCODE_1();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule getBBCODE_1() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), BBCODE_1_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_2_0_0_0 = INSTANCE.getBBCODE_2_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_2_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[b]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment BBCODE_2_0_0_1_0_0_0 = INSTANCE.getBBCODE_2_0_0_1_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment getBBCODE_2_0_0_1_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.BOLD__CHILDREN), de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, new EClass[] {de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStyle(), }, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_2_0_0_1_0_0 = INSTANCE.getBBCODE_2_0_0_1_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_2_0_0_1_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_2_0_0_1_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_2_0_0_1_0 = INSTANCE.getBBCODE_2_0_0_1_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_2_0_0_1_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_2_0_0_1_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound BBCODE_2_0_0_1 = INSTANCE.getBBCODE_2_0_0_1();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound getBBCODE_2_0_0_1() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound(BBCODE_2_0_0_1_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.STAR);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_2_0_0_2 = INSTANCE.getBBCODE_2_0_0_2();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_2_0_0_2() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[/b]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_2_0_0 = INSTANCE.getBBCODE_2_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_2_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_2_0_0_0, BBCODE_2_0_0_1, BBCODE_2_0_0_2);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_2_0 = INSTANCE.getBBCODE_2_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_2_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_2_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule BBCODE_2 = INSTANCE.getBBCODE_2();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule getBBCODE_2() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), BBCODE_2_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_3_0_0_0 = INSTANCE.getBBCODE_3_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_3_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[i]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment BBCODE_3_0_0_1_0_0_0 = INSTANCE.getBBCODE_3_0_0_1_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment getBBCODE_3_0_0_1_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.ITALIC__CHILDREN), de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, new EClass[] {de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStyle(), }, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_3_0_0_1_0_0 = INSTANCE.getBBCODE_3_0_0_1_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_3_0_0_1_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_3_0_0_1_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_3_0_0_1_0 = INSTANCE.getBBCODE_3_0_0_1_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_3_0_0_1_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_3_0_0_1_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound BBCODE_3_0_0_1 = INSTANCE.getBBCODE_3_0_0_1();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound getBBCODE_3_0_0_1() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound(BBCODE_3_0_0_1_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.STAR);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_3_0_0_2 = INSTANCE.getBBCODE_3_0_0_2();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_3_0_0_2() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[/i]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_3_0_0 = INSTANCE.getBBCODE_3_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_3_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_3_0_0_0, BBCODE_3_0_0_1, BBCODE_3_0_0_2);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_3_0 = INSTANCE.getBBCODE_3_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_3_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_3_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule BBCODE_3 = INSTANCE.getBBCODE_3();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule getBBCODE_3() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), BBCODE_3_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_4_0_0_0 = INSTANCE.getBBCODE_4_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_4_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[s]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment BBCODE_4_0_0_1_0_0_0 = INSTANCE.getBBCODE_4_0_0_1_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment getBBCODE_4_0_0_1_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.STRIKETHROUGH__CHILDREN), de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, new EClass[] {de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStyle(), }, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_4_0_0_1_0_0 = INSTANCE.getBBCODE_4_0_0_1_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_4_0_0_1_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_4_0_0_1_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_4_0_0_1_0 = INSTANCE.getBBCODE_4_0_0_1_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_4_0_0_1_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_4_0_0_1_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound BBCODE_4_0_0_1 = INSTANCE.getBBCODE_4_0_0_1();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound getBBCODE_4_0_0_1() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound(BBCODE_4_0_0_1_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.STAR);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_4_0_0_2 = INSTANCE.getBBCODE_4_0_0_2();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_4_0_0_2() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[/s]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_4_0_0 = INSTANCE.getBBCODE_4_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_4_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_4_0_0_0, BBCODE_4_0_0_1, BBCODE_4_0_0_2);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_4_0 = INSTANCE.getBBCODE_4_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_4_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_4_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule BBCODE_4 = INSTANCE.getBBCODE_4();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule getBBCODE_4() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), BBCODE_4_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_5_0_0_0 = INSTANCE.getBBCODE_5_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_5_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[u]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment BBCODE_5_0_0_1_0_0_0 = INSTANCE.getBBCODE_5_0_0_1_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment getBBCODE_5_0_0_1_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.UNDERLINE__CHILDREN), de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, new EClass[] {de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStyle(), }, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_5_0_0_1_0_0 = INSTANCE.getBBCODE_5_0_0_1_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_5_0_0_1_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_5_0_0_1_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_5_0_0_1_0 = INSTANCE.getBBCODE_5_0_0_1_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_5_0_0_1_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_5_0_0_1_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound BBCODE_5_0_0_1 = INSTANCE.getBBCODE_5_0_0_1();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound getBBCODE_5_0_0_1() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound(BBCODE_5_0_0_1_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.STAR);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_5_0_0_2 = INSTANCE.getBBCODE_5_0_0_2();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_5_0_0_2() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[/u]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_5_0_0 = INSTANCE.getBBCODE_5_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_5_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_5_0_0_0, BBCODE_5_0_0_1, BBCODE_5_0_0_2);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_5_0 = INSTANCE.getBBCODE_5_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_5_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_5_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule BBCODE_5 = INSTANCE.getBBCODE_5();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule getBBCODE_5() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), BBCODE_5_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_6_0_0_0 = INSTANCE.getBBCODE_6_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_6_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[color=", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder BBCODE_6_0_0_1 = INSTANCE.getBBCODE_6_0_0_1();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder getBBCODE_6_0_0_1() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.COLOR__VALUE), "TEXT", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_6_0_0_2 = INSTANCE.getBBCODE_6_0_0_2();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_6_0_0_2() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment BBCODE_6_0_0_3_0_0_0 = INSTANCE.getBBCODE_6_0_0_3_0_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment getBBCODE_6_0_0_3_0_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainment(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.COLOR__CHILDREN), de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, new EClass[] {de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStyle(), }, 0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_6_0_0_3_0_0 = INSTANCE.getBBCODE_6_0_0_3_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_6_0_0_3_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_6_0_0_3_0_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_6_0_0_3_0 = INSTANCE.getBBCODE_6_0_0_3_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_6_0_0_3_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_6_0_0_3_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound BBCODE_6_0_0_3 = INSTANCE.getBBCODE_6_0_0_3();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound getBBCODE_6_0_0_3() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCompound(BBCODE_6_0_0_3_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.STAR);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword BBCODE_6_0_0_4 = INSTANCE.getBBCODE_6_0_0_4();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword getBBCODE_6_0_0_4() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword("[/color]", de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence BBCODE_6_0_0 = INSTANCE.getBBCODE_6_0_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence getBBCODE_6_0_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_6_0_0_0, BBCODE_6_0_0_1, BBCODE_6_0_0_2, BBCODE_6_0_0_3, BBCODE_6_0_0_4);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice BBCODE_6_0 = INSTANCE.getBBCODE_6_0();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getBBCODE_6_0() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE, BBCODE_6_0_0);
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule BBCODE_6 = INSTANCE.getBBCODE_6();
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule getBBCODE_6() {
		return new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), BBCODE_6_0, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality.ONE);
	}
	
	
	public static String getSyntaxElementID(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement syntaxElement) {
		if (syntaxElement == null) {
			// null indicates EOF
			return "<EOF>";
		}
		for (Field field : de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.class.getFields()) {
			Object fieldValue;
			try {
				fieldValue = field.get(null);
				if (fieldValue == syntaxElement) {
					String id = field.getName();
					return id;
				}
			} catch (Exception e) { }
		}
		return null;
	}
	
	public static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement getSyntaxElementByID(String syntaxElementID) {
		try {
			return (de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement) de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.class.getField(syntaxElementID).get(null);
		} catch (Exception e) {
			return null;
		}
	}
	
	public final static de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule[] RULES = new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule[] {
		BBCODE_0,
		BBCODE_1,
		BBCODE_2,
		BBCODE_3,
		BBCODE_4,
		BBCODE_5,
		BBCODE_6,
	};
	
	/**
	 * Returns all keywords of the grammar. This includes all literals for boolean and
	 * enumeration terminals.
	 */
	public Set<String> getKeywords() {
		if (this.keywords == null) {
			this.keywords = new LinkedHashSet<String>();
			for (de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeRule rule : RULES) {
				findKeywords(rule, this.keywords);
			}
		}
		return keywords;
	}
	
	/**
	 * Finds all keywords in the given element and its children and adds them to the
	 * set. This includes all literals for boolean and enumeration terminals.
	 */
	private void findKeywords(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement element, Set<String> keywords) {
		if (element instanceof de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword) {
			keywords.add(((de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword) element).getValue());
		} else if (element instanceof de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeBooleanTerminal) {
			keywords.add(((de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeBooleanTerminal) element).getTrueLiteral());
			keywords.add(((de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeBooleanTerminal) element).getFalseLiteral());
		} else if (element instanceof de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeEnumerationTerminal) {
			de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeEnumerationTerminal terminal = (de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeEnumerationTerminal) element;
			for (String key : terminal.getLiteralMapping().keySet()) {
				keywords.add(key);
			}
		}
		for (de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement child : element.getChildren()) {
			findKeywords(child, this.keywords);
		}
	}
	
}
