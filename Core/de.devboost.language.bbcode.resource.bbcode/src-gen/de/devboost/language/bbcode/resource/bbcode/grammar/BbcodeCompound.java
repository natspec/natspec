/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


public class BbcodeCompound extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement {
	
	public BbcodeCompound(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice choice, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality) {
		super(cardinality, new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement[] {choice});
	}
	
	public String toString() {
		return "(" + getChildren()[0] + ")";
	}
	
}
