/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


public class BbcodeSequence extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement {
	
	public BbcodeSequence(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement... elements) {
		super(cardinality, elements);
	}
	
	public String toString() {
		return de.devboost.language.bbcode.resource.bbcode.util.BbcodeStringUtil.explode(getChildren(), " ");
	}
	
}
