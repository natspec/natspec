/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;


public enum BbcodeEProblemSeverity {
	WARNING, ERROR;
}
