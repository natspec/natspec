/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


public abstract class BbcodeFormattingElement extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement {
	
	public BbcodeFormattingElement(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality) {
		super(cardinality, null);
	}
	
}
