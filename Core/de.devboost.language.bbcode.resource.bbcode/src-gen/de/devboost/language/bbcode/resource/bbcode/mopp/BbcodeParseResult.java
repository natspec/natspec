/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.ArrayList;
import java.util.Collection;
import org.eclipse.emf.ecore.EObject;

public class BbcodeParseResult implements de.devboost.language.bbcode.resource.bbcode.IBbcodeParseResult {
	
	private EObject root;
	
	private de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap;
	
	private Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>> commands = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>>();
	
	public BbcodeParseResult() {
		super();
	}
	
	public EObject getRoot() {
		return root;
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap getLocationMap() {
		return locationMap;
	}
	
	public void setRoot(EObject root) {
		this.root = root;
	}
	
	public void setLocationMap(de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap) {
		this.locationMap = locationMap;
	}
	
	public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>> getPostParseCommands() {
		return commands;
	}
	
}
