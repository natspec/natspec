/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.debug;

import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointManager;
import org.eclipse.debug.core.model.DebugElement;
import org.eclipse.debug.core.model.IDebugTarget;

public abstract class BbcodeDebugElement extends DebugElement {
	
	/**
	 * Constructs a new debug element in the given target.
	 */
	public BbcodeDebugElement(IDebugTarget target) {
		super(target);
	}
	
	public String getModelIdentifier() {
		return de.devboost.language.bbcode.resource.bbcode.mopp.BbcodePlugin.DEBUG_MODEL_ID;
	}
	
	/**
	 * <p>
	 * Returns the breakpoint manager.
	 * </p>
	 * 
	 * @return the breakpoint manager
	 */
	protected IBreakpointManager getBreakpointManager() {
		return DebugPlugin.getDefault().getBreakpointManager();
	}
	
}
