/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.debug;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.sourcelookup.AbstractSourceLookupParticipant;

public class BbcodeSourceLookupParticipant extends AbstractSourceLookupParticipant {
	
	public String getSourceName(Object object) throws CoreException {
		if (object instanceof de.devboost.language.bbcode.resource.bbcode.debug.BbcodeStackFrame) {
			de.devboost.language.bbcode.resource.bbcode.debug.BbcodeStackFrame frame = (de.devboost.language.bbcode.resource.bbcode.debug.BbcodeStackFrame) object;
			return frame.getResourceURI();
		}
		return null;
	}
	
}
