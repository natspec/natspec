/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A class to represent placeholders in a grammar.
 */
public class BbcodePlaceholder extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeTerminal {
	
	private final String tokenName;
	
	public BbcodePlaceholder(EStructuralFeature feature, String tokenName, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, int mandatoryOccurencesAfter) {
		super(feature, cardinality, mandatoryOccurencesAfter);
		this.tokenName = tokenName;
	}
	
	public String getTokenName() {
		return tokenName;
	}
	
}
