/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


public class BbcodeWhiteSpace extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeFormattingElement {
	
	private final int amount;
	
	public BbcodeWhiteSpace(int amount, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality) {
		super(cardinality);
		this.amount = amount;
	}
	
	public int getAmount() {
		return amount;
	}
	
	public String toString() {
		return "#" + getAmount();
	}
	
}
