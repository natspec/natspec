/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;

import java.util.Collection;

public interface IBbcodeProblem {
	public String getMessage();
	public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity getSeverity();
	public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType getType();
	public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> getQuickFixes();
}
