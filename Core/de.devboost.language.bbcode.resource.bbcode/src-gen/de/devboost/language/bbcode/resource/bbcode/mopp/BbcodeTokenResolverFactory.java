/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The BbcodeTokenResolverFactory class provides access to all generated token
 * resolvers. By giving the name of a defined token, the corresponding resolve can
 * be obtained. Despite the fact that this class is called TokenResolverFactory is
 * does NOT create new token resolvers whenever a client calls methods to obtain a
 * resolver. Rather, this class maintains a map of all resolvers and creates each
 * resolver at most once.
 */
public class BbcodeTokenResolverFactory implements de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolverFactory {
	
	private Map<String, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver> tokenName2TokenResolver;
	private Map<String, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver> featureName2CollectInTokenResolver;
	private static de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver defaultResolver = new de.devboost.language.bbcode.resource.bbcode.analysis.BbcodeDefaultTokenResolver();
	
	public BbcodeTokenResolverFactory() {
		tokenName2TokenResolver = new LinkedHashMap<String, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver>();
		featureName2CollectInTokenResolver = new LinkedHashMap<String, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver>();
		registerTokenResolver("TEXT", new de.devboost.language.bbcode.resource.bbcode.analysis.BbcodeTEXTTokenResolver());
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver createTokenResolver(String tokenName) {
		return internalCreateResolver(tokenName2TokenResolver, tokenName);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver createCollectInTokenResolver(String featureName) {
		return internalCreateResolver(featureName2CollectInTokenResolver, featureName);
	}
	
	protected boolean registerTokenResolver(String tokenName, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver resolver){
		return internalRegisterTokenResolver(tokenName2TokenResolver, tokenName, resolver);
	}
	
	protected boolean registerCollectInTokenResolver(String featureName, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver resolver){
		return internalRegisterTokenResolver(featureName2CollectInTokenResolver, featureName, resolver);
	}
	
	protected de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver deRegisterTokenResolver(String tokenName){
		return tokenName2TokenResolver.remove(tokenName);
	}
	
	private de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver internalCreateResolver(Map<String, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver> resolverMap, String key) {
		if (resolverMap.containsKey(key)){
			return resolverMap.get(key);
		} else {
			return defaultResolver;
		}
	}
	
	private boolean internalRegisterTokenResolver(Map<String, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver> resolverMap, String key, de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver resolver) {
		if (!resolverMap.containsKey(key)) {
			resolverMap.put(key,resolver);
			return true;
		}
		return false;
	}
	
}
