/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.debug;

import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamsProxy;

public class BbcodeDebugProcess extends de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugElement implements IProcess, de.devboost.language.bbcode.resource.bbcode.debug.IBbcodeDebugEventListener {
	
	private ILaunch launch;
	
	private boolean terminated = false;
	
	public BbcodeDebugProcess(ILaunch launch) {
		super(launch.getDebugTarget());
		this.launch = launch;
	}
	
	public boolean canTerminate() {
		return !terminated;
	}
	
	public boolean isTerminated() {
		return terminated;
	}
	
	public void terminate() throws DebugException {
		terminated = true;
	}
	
	public String getLabel() {
		return null;
	}
	
	public ILaunch getLaunch() {
		return launch;
	}
	
	public IStreamsProxy getStreamsProxy() {
		return null;
	}
	
	public void setAttribute(String key, String value) {
	}
	
	public String getAttribute(String key) {
		return null;
	}
	
	public int getExitValue() throws DebugException {
		return 0;
	}
	
	public void handleMessage(de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message) {
		if (message.hasType(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.TERMINATED)) {
			terminated = true;
		} else {
			// ignore other events
		}
	}
	
}
