/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

public class BbcodeResourceFactory implements Resource.Factory {
	
	public BbcodeResourceFactory() {
		super();
	}
	
	public Resource createResource(URI uri) {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResource(uri);
	}
	
}
