/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


public enum BbcodeCardinality {
	
	ONE, PLUS, QUESTIONMARK, STAR;
	
}
