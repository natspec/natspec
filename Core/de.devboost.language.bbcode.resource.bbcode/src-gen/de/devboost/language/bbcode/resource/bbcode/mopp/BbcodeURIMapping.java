/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import org.eclipse.emf.common.util.URI;

/**
 * <p>
 * A basic implementation of the
 * de.devboost.language.bbcode.resource.bbcode.IBbcodeURIMapping interface that
 * can map identifiers to URIs.
 * </p>
 * 
 * @param <ReferenceType> unused type parameter which is needed to implement
 * de.devboost.language.bbcode.resource.bbcode.IBbcodeURIMapping.
 */
public class BbcodeURIMapping<ReferenceType> implements de.devboost.language.bbcode.resource.bbcode.IBbcodeURIMapping<ReferenceType> {
	
	private URI uri;
	private String identifier;
	private String warning;
	
	public BbcodeURIMapping(String identifier, URI newIdentifier, String warning) {
		super();
		this.uri = newIdentifier;
		this.identifier = identifier;
		this.warning = warning;
	}
	
	public URI getTargetIdentifier() {
		return uri;
	}
	
	public String getIdentifier() {
		return identifier;
	}
	
	public String getWarning() {
		return warning;
	}
	
}
