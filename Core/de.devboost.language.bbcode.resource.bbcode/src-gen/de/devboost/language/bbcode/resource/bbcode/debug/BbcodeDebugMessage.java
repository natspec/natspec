/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.debug;

import java.util.ArrayList;
import java.util.List;

/**
 * DebugMessages are exchanged between the debug server (the Eclipse debug
 * framework) and the debug client (a running process or interpreter). To exchange
 * messages they are serialized and sent over sockets.
 */
public class BbcodeDebugMessage {
	
	private static final char DELIMITER = ':';
	private de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes messageType;
	private String[] arguments;
	
	public BbcodeDebugMessage(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes messageType, String[] arguments) {
		super();
		this.messageType = messageType;
		this.arguments = arguments;
	}
	
	public BbcodeDebugMessage(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes messageType, List<String> arguments) {
		super();
		this.messageType = messageType;
		this.arguments = new String[arguments.size()];
		for (int i = 0; i < arguments.size(); i++) {
			this.arguments[i] = arguments.get(i);
		}
	}
	
	public de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes getMessageType() {
		return messageType;
	}
	
	public String[] getArguments() {
		return arguments;
	}
	
	public String serialize() {
		List<String> parts = new ArrayList<String>();
		parts.add(messageType.name());
		for (String argument : arguments) {
			parts.add(argument);
		}
		return de.devboost.language.bbcode.resource.bbcode.util.BbcodeStringUtil.encode(DELIMITER, parts);
	}
	
	public static BbcodeDebugMessage deserialize(String response) {
		List<String> parts = de.devboost.language.bbcode.resource.bbcode.util.BbcodeStringUtil.decode(response, DELIMITER);
		String messageType = parts.get(0);
		String[] arguments = new String[parts.size() - 1];
		for (int i = 1; i < parts.size(); i++) {
			arguments[i - 1] = parts.get(i);
		}
		de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes type = de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.valueOf(messageType);
		BbcodeDebugMessage message = new BbcodeDebugMessage(type, arguments);
		return message;
	}
	
	public boolean hasType(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes type) {
		return this.messageType == type;
	}
	
	public String getArgument(int index) {
		return getArguments()[index];
	}
	
	public String toString() {
		return this.getClass().getSimpleName() + "[" + messageType.name() + ": " + de.devboost.language.bbcode.resource.bbcode.util.BbcodeStringUtil.explode(arguments, ", ") + "]";
	}
	
}
