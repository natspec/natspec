/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;


public class BbcodeTokenStyleInformationProvider {
	
	public static String TASK_ITEM_TOKEN_NAME = "TASK_ITEM";
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle getDefaultTokenStyle(String tokenName) {
		if ("TASK_ITEM".equals(tokenName)) {
			return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenStyle(new int[] {0x7F, 0x9F, 0xBF}, null, true, false, false, false);
		}
		return null;
	}
	
}
