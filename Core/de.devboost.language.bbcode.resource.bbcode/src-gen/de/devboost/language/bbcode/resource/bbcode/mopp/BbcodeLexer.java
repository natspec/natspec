// $ANTLR 3.4

	package de.devboost.language.bbcode.resource.bbcode.mopp;
	
	import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.RecognitionException;


import org.antlr.runtime3_4_0.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class BbcodeLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__7=7;
    public static final int T__8=8;
    public static final int T__9=9;
    public static final int T__10=10;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int LINEBREAK=4;
    public static final int TEXT=5;
    public static final int WHITESPACE=6;

    	public List<RecognitionException> lexerExceptions  = new ArrayList<RecognitionException>();
    	public List<Integer> lexerExceptionPositions = new ArrayList<Integer>();
    	
    	public void reportError(RecognitionException e) {
    		lexerExceptions.add(e);
    		lexerExceptionPositions.add(((ANTLRStringStream) input).index());
    	}


    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public BbcodeLexer() {} 
    public BbcodeLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public BbcodeLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "Bbcode.g"; }

    // $ANTLR start "T__7"
    public final void mT__7() throws RecognitionException {
        try {
            int _type = T__7;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:20:6: ( '[/b]' )
            // Bbcode.g:20:8: '[/b]'
            {
            match("[/b]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__7"

    // $ANTLR start "T__8"
    public final void mT__8() throws RecognitionException {
        try {
            int _type = T__8;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:21:6: ( '[/color]' )
            // Bbcode.g:21:8: '[/color]'
            {
            match("[/color]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__8"

    // $ANTLR start "T__9"
    public final void mT__9() throws RecognitionException {
        try {
            int _type = T__9;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:22:6: ( '[/i]' )
            // Bbcode.g:22:8: '[/i]'
            {
            match("[/i]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__9"

    // $ANTLR start "T__10"
    public final void mT__10() throws RecognitionException {
        try {
            int _type = T__10;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:23:7: ( '[/s]' )
            // Bbcode.g:23:9: '[/s]'
            {
            match("[/s]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__10"

    // $ANTLR start "T__11"
    public final void mT__11() throws RecognitionException {
        try {
            int _type = T__11;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:24:7: ( '[/u]' )
            // Bbcode.g:24:9: '[/u]'
            {
            match("[/u]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__11"

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:25:7: ( '[b]' )
            // Bbcode.g:25:9: '[b]'
            {
            match("[b]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:26:7: ( '[color=' )
            // Bbcode.g:26:9: '[color='
            {
            match("[color="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:27:7: ( '[i]' )
            // Bbcode.g:27:9: '[i]'
            {
            match("[i]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:28:7: ( '[s]' )
            // Bbcode.g:28:9: '[s]'
            {
            match("[s]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:29:7: ( '[u]' )
            // Bbcode.g:29:9: '[u]'
            {
            match("[u]"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:30:7: ( ']' )
            // Bbcode.g:30:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "WHITESPACE"
    public final void mWHITESPACE() throws RecognitionException {
        try {
            int _type = WHITESPACE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:1168:11: ( ( ' ' | '\\t' | '\\f' ) )
            // Bbcode.g:1169:2: ( ' ' | '\\t' | '\\f' )
            {
            if ( input.LA(1)=='\t'||input.LA(1)=='\f'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WHITESPACE"

    // $ANTLR start "TEXT"
    public final void mTEXT() throws RecognitionException {
        try {
            int _type = TEXT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:1172:5: ( ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '-' | '_' | '#' ) )+ ) )
            // Bbcode.g:1173:2: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '-' | '_' | '#' ) )+ )
            {
            // Bbcode.g:1173:2: ( ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '-' | '_' | '#' ) )+ )
            // Bbcode.g:1173:3: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '-' | '_' | '#' ) )+
            {
            // Bbcode.g:1173:3: ( ( 'A' .. 'Z' | 'a' .. 'z' | '0' .. '9' | '-' | '_' | '#' ) )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='#'||LA1_0=='-'||(LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // Bbcode.g:
            	    {
            	    if ( input.LA(1)=='#'||input.LA(1)=='-'||(input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TEXT"

    // $ANTLR start "LINEBREAK"
    public final void mLINEBREAK() throws RecognitionException {
        try {
            int _type = LINEBREAK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // Bbcode.g:1175:10: ( ( ( '\\r\\n' | '\\r' | '\\n' ) ) )
            // Bbcode.g:1176:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            {
            // Bbcode.g:1176:2: ( ( '\\r\\n' | '\\r' | '\\n' ) )
            // Bbcode.g:1176:3: ( '\\r\\n' | '\\r' | '\\n' )
            {
            // Bbcode.g:1176:3: ( '\\r\\n' | '\\r' | '\\n' )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='\r') ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1=='\n') ) {
                    alt2=1;
                }
                else {
                    alt2=2;
                }
            }
            else if ( (LA2_0=='\n') ) {
                alt2=3;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }
            switch (alt2) {
                case 1 :
                    // Bbcode.g:1176:4: '\\r\\n'
                    {
                    match("\r\n"); 



                    }
                    break;
                case 2 :
                    // Bbcode.g:1176:13: '\\r'
                    {
                    match('\r'); 

                    }
                    break;
                case 3 :
                    // Bbcode.g:1176:20: '\\n'
                    {
                    match('\n'); 

                    }
                    break;

            }


            }


             _channel = 99; 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LINEBREAK"

    public void mTokens() throws RecognitionException {
        // Bbcode.g:1:8: ( T__7 | T__8 | T__9 | T__10 | T__11 | T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | WHITESPACE | TEXT | LINEBREAK )
        int alt3=14;
        switch ( input.LA(1) ) {
        case '[':
            {
            switch ( input.LA(2) ) {
            case '/':
                {
                switch ( input.LA(3) ) {
                case 'b':
                    {
                    alt3=1;
                    }
                    break;
                case 'c':
                    {
                    alt3=2;
                    }
                    break;
                case 'i':
                    {
                    alt3=3;
                    }
                    break;
                case 's':
                    {
                    alt3=4;
                    }
                    break;
                case 'u':
                    {
                    alt3=5;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 6, input);

                    throw nvae;

                }

                }
                break;
            case 'b':
                {
                alt3=6;
                }
                break;
            case 'c':
                {
                alt3=7;
                }
                break;
            case 'i':
                {
                alt3=8;
                }
                break;
            case 's':
                {
                alt3=9;
                }
                break;
            case 'u':
                {
                alt3=10;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 1, input);

                throw nvae;

            }

            }
            break;
        case ']':
            {
            alt3=11;
            }
            break;
        case '\t':
        case '\f':
        case ' ':
            {
            alt3=12;
            }
            break;
        case '#':
        case '-':
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case 'A':
        case 'B':
        case 'C':
        case 'D':
        case 'E':
        case 'F':
        case 'G':
        case 'H':
        case 'I':
        case 'J':
        case 'K':
        case 'L':
        case 'M':
        case 'N':
        case 'O':
        case 'P':
        case 'Q':
        case 'R':
        case 'S':
        case 'T':
        case 'U':
        case 'V':
        case 'W':
        case 'X':
        case 'Y':
        case 'Z':
        case '_':
        case 'a':
        case 'b':
        case 'c':
        case 'd':
        case 'e':
        case 'f':
        case 'g':
        case 'h':
        case 'i':
        case 'j':
        case 'k':
        case 'l':
        case 'm':
        case 'n':
        case 'o':
        case 'p':
        case 'q':
        case 'r':
        case 's':
        case 't':
        case 'u':
        case 'v':
        case 'w':
        case 'x':
        case 'y':
        case 'z':
            {
            alt3=13;
            }
            break;
        case '\n':
        case '\r':
            {
            alt3=14;
            }
            break;
        default:
            NoViableAltException nvae =
                new NoViableAltException("", 3, 0, input);

            throw nvae;

        }

        switch (alt3) {
            case 1 :
                // Bbcode.g:1:10: T__7
                {
                mT__7(); 


                }
                break;
            case 2 :
                // Bbcode.g:1:15: T__8
                {
                mT__8(); 


                }
                break;
            case 3 :
                // Bbcode.g:1:20: T__9
                {
                mT__9(); 


                }
                break;
            case 4 :
                // Bbcode.g:1:25: T__10
                {
                mT__10(); 


                }
                break;
            case 5 :
                // Bbcode.g:1:31: T__11
                {
                mT__11(); 


                }
                break;
            case 6 :
                // Bbcode.g:1:37: T__12
                {
                mT__12(); 


                }
                break;
            case 7 :
                // Bbcode.g:1:43: T__13
                {
                mT__13(); 


                }
                break;
            case 8 :
                // Bbcode.g:1:49: T__14
                {
                mT__14(); 


                }
                break;
            case 9 :
                // Bbcode.g:1:55: T__15
                {
                mT__15(); 


                }
                break;
            case 10 :
                // Bbcode.g:1:61: T__16
                {
                mT__16(); 


                }
                break;
            case 11 :
                // Bbcode.g:1:67: T__17
                {
                mT__17(); 


                }
                break;
            case 12 :
                // Bbcode.g:1:73: WHITESPACE
                {
                mWHITESPACE(); 


                }
                break;
            case 13 :
                // Bbcode.g:1:84: TEXT
                {
                mTEXT(); 


                }
                break;
            case 14 :
                // Bbcode.g:1:89: LINEBREAK
                {
                mLINEBREAK(); 


                }
                break;

        }

    }


 

}