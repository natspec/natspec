/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import org.eclipse.emf.ecore.EClass;

public class BbcodeSyntaxCoverageInformationProvider {
	
	public EClass[] getClassesWithSyntax() {
		return new EClass[] {
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(),
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(),
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(),
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(),
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(),
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(),
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(),
		};
	}
	
	public EClass[] getStartSymbols() {
		return new EClass[] {
			de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(),
		};
	}
	
}
