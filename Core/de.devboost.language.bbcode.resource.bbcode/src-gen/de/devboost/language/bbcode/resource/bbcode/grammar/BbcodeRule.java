/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * A class to represent a rules in the grammar.
 */
public class BbcodeRule extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement {
	
	private final EClass metaclass;
	
	public BbcodeRule(EClass metaclass, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice choice, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality) {
		super(cardinality, new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement[] {choice});
		this.metaclass = metaclass;
	}
	
	public EClass getMetaclass() {
		return metaclass;
	}
	
	public de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice getDefinition() {
		return (de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeChoice) getChildren()[0];
	}
	
}

