/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * Abstract super class for all expected elements. Provides methods to add
 * followers.
 */
public abstract class BbcodeAbstractExpectedElement implements de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement {
	
	private EClass ruleMetaclass;
	
	private Set<de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]>> followers = new LinkedHashSet<de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]>>();
	
	public BbcodeAbstractExpectedElement(EClass ruleMetaclass) {
		super();
		this.ruleMetaclass = ruleMetaclass;
	}
	
	public EClass getRuleMetaclass() {
		return ruleMetaclass;
	}
	
	public void addFollower(de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement follower, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] path) {
		followers.add(new de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]>(follower, path));
	}
	
	public Collection<de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]>> getFollowers() {
		return followers;
	}
	
}
