/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


public class BbcodeChoice extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement {
	
	public BbcodeChoice(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement... choices) {
		super(cardinality, choices);
	}
	
	public String toString() {
		return de.devboost.language.bbcode.resource.bbcode.util.BbcodeStringUtil.explode(getChildren(), "|");
	}
	
}
