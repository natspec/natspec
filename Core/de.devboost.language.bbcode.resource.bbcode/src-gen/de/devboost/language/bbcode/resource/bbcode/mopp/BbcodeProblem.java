/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

public class BbcodeProblem implements de.devboost.language.bbcode.resource.bbcode.IBbcodeProblem {
	
	private String message;
	private de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType type;
	private de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity severity;
	private Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> quickFixes;
	
	public BbcodeProblem(String message, de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType type, de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity severity) {
		this(message, type, severity, Collections.<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix>emptySet());
	}
	
	public BbcodeProblem(String message, de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType type, de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity severity, de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix quickFix) {
		this(message, type, severity, Collections.singleton(quickFix));
	}
	
	public BbcodeProblem(String message, de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType type, de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity severity, Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> quickFixes) {
		super();
		this.message = message;
		this.type = type;
		this.severity = severity;
		this.quickFixes = new LinkedHashSet<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix>();
		this.quickFixes.addAll(quickFixes);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType getType() {
		return type;
	}
	
	public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity getSeverity() {
		return severity;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> getQuickFixes() {
		return quickFixes;
	}
	
}
