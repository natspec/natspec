/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.Collections;
import java.util.Set;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A representation for a range in a document where a structural feature (e.g., a
 * reference) is expected.
 */
public class BbcodeExpectedStructuralFeature extends de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeAbstractExpectedElement {
	
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder placeholder;
	
	public BbcodeExpectedStructuralFeature(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodePlaceholder placeholder) {
		super(placeholder.getMetaclass());
		this.placeholder = placeholder;
	}
	
	public EStructuralFeature getFeature() {
		return placeholder.getFeature();
	}
	
	/**
	 * Returns the expected placeholder.
	 */
	public de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement getSymtaxElement() {
		return placeholder;
	}
	
	public String getTokenName() {
		return placeholder.getTokenName();
	}
	
	public Set<String> getTokenNames() {
		return Collections.singleton(getTokenName());
	}
	
	public String toString() {
		return "EFeature " + getFeature().getEContainingClass().getName() + "." + getFeature().getName();
	}
	
	public boolean equals(Object o) {
		if (o instanceof BbcodeExpectedStructuralFeature) {
			return getFeature().equals(((BbcodeExpectedStructuralFeature) o).getFeature());
		}
		return false;
	}
	@Override
	public int hashCode() {
		return getFeature().hashCode();
	}
	
}
