/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.eclipse.emf.common.util.URI;

/**
 * <p>
 * A basic implementation of the
 * de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolveResult
 * interface that collects mappings in a list.
 * </p>
 * 
 * @param <ReferenceType> the type of the references that can be contained in this
 * result
 */
public class BbcodeReferenceResolveResult<ReferenceType> implements de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolveResult<ReferenceType> {
	
	private Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceMapping<ReferenceType>> mappings;
	private String errorMessage;
	private boolean resolveFuzzy;
	private Set<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> quickFixes;
	
	public BbcodeReferenceResolveResult(boolean resolveFuzzy) {
		super();
		this.resolveFuzzy = resolveFuzzy;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> getQuickFixes() {
		if (quickFixes == null) {
			quickFixes = new LinkedHashSet<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix>();
		}
		return Collections.unmodifiableSet(quickFixes);
	}
	
	public void addQuickFix(de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix quickFix) {
		if (quickFixes == null) {
			quickFixes = new LinkedHashSet<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix>();
		}
		quickFixes.add(quickFix);
	}
	
	public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceMapping<ReferenceType>> getMappings() {
		return mappings;
	}
	
	public boolean wasResolved() {
		return mappings != null;
	}
	
	public boolean wasResolvedMultiple() {
		return mappings != null && mappings.size() > 1;
	}
	
	public boolean wasResolvedUniquely() {
		return mappings != null && mappings.size() == 1;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void addMapping(String identifier, ReferenceType target) {
		if (!resolveFuzzy && target == null) {
			throw new IllegalArgumentException("Mapping references to null is only allowed for fuzzy resolution.");
		}
		addMapping(identifier, target, null);
	}
	
	public void addMapping(String identifier, ReferenceType target, String warning) {
		if (mappings == null) {
			mappings = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeElementMapping<ReferenceType>(identifier, target, warning));
		errorMessage = null;
	}
	
	public void addMapping(String identifier, URI uri) {
		addMapping(identifier, uri, null);
	}
	
	public void addMapping(String identifier, URI uri, String warning) {
		if (mappings == null) {
			mappings = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceMapping<ReferenceType>>(1);
		}
		mappings.add(new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeURIMapping<ReferenceType>(identifier, uri, warning));
	}
}
