/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.util;

import de.devboost.language.bbcode.BBCodeText;
import de.devboost.language.bbcode.Bold;
import de.devboost.language.bbcode.Color;
import de.devboost.language.bbcode.CompositeStyle;
import de.devboost.language.bbcode.EmptyStyle;
import de.devboost.language.bbcode.Italic;
import de.devboost.language.bbcode.Strikethrough;
import de.devboost.language.bbcode.Style;
import de.devboost.language.bbcode.Underline;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import org.eclipse.emf.ecore.EObject;

/**
 * This class provides basic infrastructure to interpret models. To implement
 * concrete interpreters, subclass this abstract interpreter and override the
 * interprete_* methods. The interpretation can be customized by binding the two
 * type parameters (ResultType, ContextType). The former is returned by all
 * interprete_* methods, while the latter is passed from method to method while
 * traversing the model. The concrete traversal strategy can also be exchanged.
 * One can use a static traversal strategy by pushing all objects to interpret on
 * the interpretation stack (using addObjectToInterprete()) before calling
 * interprete(). Alternatively, the traversal strategy can be dynamic by pushing
 * objects on the interpretation stack during interpretation.
 */
public class AbstractBbcodeInterpreter<ResultType, ContextType> {
	
	private Stack<EObject> interpretationStack = new Stack<EObject>();
	private List<de.devboost.language.bbcode.resource.bbcode.IBbcodeInterpreterListener> listeners = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeInterpreterListener>();
	private EObject nextObjectToInterprete;
	private ContextType currentContext;
	
	public ResultType interprete(ContextType context) {
		ResultType result = null;
		EObject next = null;
		currentContext = context;
		while (!interpretationStack.empty()) {
			try {
				next = interpretationStack.pop();
			} catch (EmptyStackException ese) {
				// this can happen when the interpreter was terminated between the call to empty()
				// and pop()
				break;
			}
			nextObjectToInterprete = next;
			notifyListeners(next);
			result = interprete(next, context);
			if (!continueInterpretation(context, result)) {
				break;
			}
		}
		currentContext = null;
		return result;
	}
	
	/**
	 * Override this method to stop the overall interpretation depending on the result
	 * of the interpretation of a single model elements.
	 */
	public boolean continueInterpretation(ContextType context, ResultType result) {
		return true;
	}
	
	public ResultType interprete(EObject object, ContextType context) {
		ResultType result = null;
		if (object instanceof de.devboost.language.bbcode.BBCodeText) {
			result = interprete_de_devboost_language_bbcode_BBCodeText((de.devboost.language.bbcode.BBCodeText) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.CompositeStyle) {
			result = interprete_de_devboost_language_bbcode_CompositeStyle((de.devboost.language.bbcode.CompositeStyle) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.EmptyStyle) {
			result = interprete_de_devboost_language_bbcode_EmptyStyle((de.devboost.language.bbcode.EmptyStyle) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.Bold) {
			result = interprete_de_devboost_language_bbcode_Bold((de.devboost.language.bbcode.Bold) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.Italic) {
			result = interprete_de_devboost_language_bbcode_Italic((de.devboost.language.bbcode.Italic) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.Strikethrough) {
			result = interprete_de_devboost_language_bbcode_Strikethrough((de.devboost.language.bbcode.Strikethrough) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.Underline) {
			result = interprete_de_devboost_language_bbcode_Underline((de.devboost.language.bbcode.Underline) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.Color) {
			result = interprete_de_devboost_language_bbcode_Color((de.devboost.language.bbcode.Color) object, context);
		}
		if (result != null) {
			return result;
		}
		if (object instanceof de.devboost.language.bbcode.Style) {
			result = interprete_de_devboost_language_bbcode_Style((de.devboost.language.bbcode.Style) object, context);
		}
		if (result != null) {
			return result;
		}
		return result;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_BBCodeText(BBCodeText bBCodeText, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_Style(Style style, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_CompositeStyle(CompositeStyle compositeStyle, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_EmptyStyle(EmptyStyle emptyStyle, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_Bold(Bold bold, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_Italic(Italic italic, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_Strikethrough(Strikethrough strikethrough, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_Underline(Underline underline, ContextType context) {
		return null;
	}
	
	public ResultType interprete_de_devboost_language_bbcode_Color(Color color, ContextType context) {
		return null;
	}
	
	private void notifyListeners(EObject element) {
		for (de.devboost.language.bbcode.resource.bbcode.IBbcodeInterpreterListener listener : listeners) {
			listener.handleInterpreteObject(element);
		}
	}
	
	/**
	 * Adds the given object to the interpretation stack. Attention: Objects that are
	 * added first, are interpret last.
	 */
	public void addObjectToInterprete(EObject object) {
		interpretationStack.push(object);
	}
	
	/**
	 * Adds the given collection of objects to the interpretation stack. Attention:
	 * Collections that are added first, are interpret last.
	 */
	public void addObjectsToInterprete(Collection<? extends EObject> objects) {
		for (EObject object : objects) {
			addObjectToInterprete(object);
		}
	}
	
	/**
	 * Adds the given collection of objects in reverse order to the interpretation
	 * stack.
	 */
	public void addObjectsToInterpreteInReverseOrder(Collection<? extends EObject> objects) {
		List<EObject> reverse = new ArrayList<EObject>(objects.size());
		reverse.addAll(objects);
		Collections.reverse(reverse);
		addObjectsToInterprete(reverse);
	}
	
	/**
	 * Adds the given object and all its children to the interpretation stack such
	 * that they are interpret in top down order.
	 */
	public void addObjectTreeToInterpreteTopDown(EObject root) {
		List<EObject> objects = new ArrayList<EObject>();
		objects.add(root);
		Iterator<EObject> it = root.eAllContents();
		while (it.hasNext()) {
			EObject eObject = (EObject) it.next();
			objects.add(eObject);
		}
		addObjectsToInterpreteInReverseOrder(objects);
	}
	
	public void addListener(de.devboost.language.bbcode.resource.bbcode.IBbcodeInterpreterListener newListener) {
		listeners.add(newListener);
	}
	
	public boolean removeListener(de.devboost.language.bbcode.resource.bbcode.IBbcodeInterpreterListener listener) {
		return listeners.remove(listener);
	}
	
	public EObject getNextObjectToInterprete() {
		return nextObjectToInterprete;
	}
	
	public Stack<EObject> getInterpretationStack() {
		return interpretationStack;
	}
	
	public void terminate() {
		interpretationStack.clear();
	}
	
	public ContextType getCurrentContext() {
		return currentContext;
	}
	
}
