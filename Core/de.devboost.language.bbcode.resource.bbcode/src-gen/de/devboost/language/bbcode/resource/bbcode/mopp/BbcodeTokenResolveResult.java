/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;


/**
 * A basic implementation of the ITokenResolveResult interface.
 */
public class BbcodeTokenResolveResult implements de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolveResult {
	
	private String errorMessage;
	private Object resolvedToken;
	
	public BbcodeTokenResolveResult() {
		super();
		clear();
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Object getResolvedToken() {
		return resolvedToken;
	}
	
	public void setErrorMessage(String message) {
		errorMessage = message;
	}
	
	public void setResolvedToken(Object resolvedToken) {
		this.resolvedToken = resolvedToken;
	}
	
	public void clear() {
		errorMessage = "Can't resolve token.";
		resolvedToken = null;
	}
	
}
