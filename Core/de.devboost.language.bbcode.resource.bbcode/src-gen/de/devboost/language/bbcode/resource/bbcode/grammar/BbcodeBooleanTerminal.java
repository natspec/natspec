/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A class to represent boolean terminals in a grammar.
 */
public class BbcodeBooleanTerminal extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeTerminal {
	
	private String trueLiteral;
	private String falseLiteral;
	
	public BbcodeBooleanTerminal(EStructuralFeature attribute, String trueLiteral, String falseLiteral, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, int mandatoryOccurrencesAfter) {
		super(attribute, cardinality, mandatoryOccurrencesAfter);
		assert attribute instanceof EAttribute;
		this.trueLiteral = trueLiteral;
		this.falseLiteral = falseLiteral;
	}
	
	public String getTrueLiteral() {
		return trueLiteral;
	}
	
	public String getFalseLiteral() {
		return falseLiteral;
	}
	
	public EAttribute getAttribute() {
		return (EAttribute) getFeature();
	}
	
}
