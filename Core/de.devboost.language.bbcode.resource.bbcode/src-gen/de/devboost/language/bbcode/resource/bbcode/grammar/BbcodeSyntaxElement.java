/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import org.eclipse.emf.ecore.EClass;

/**
 * The abstract super class for all elements of a grammar. This class provides
 * methods to traverse the grammar rules.
 */
public abstract class BbcodeSyntaxElement {
	
	private BbcodeSyntaxElement[] children;
	private BbcodeSyntaxElement parent;
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality;
	
	public BbcodeSyntaxElement(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, BbcodeSyntaxElement[] children) {
		this.cardinality = cardinality;
		this.children = children;
		if (this.children != null) {
			for (BbcodeSyntaxElement child : this.children) {
				child.setParent(this);
			}
		}
	}
	
	/**
	 * Sets the parent of this syntax element. This method must be invoked at most
	 * once.
	 */
	public void setParent(BbcodeSyntaxElement parent) {
		assert this.parent == null;
		this.parent = parent;
	}
	
	/**
	 * Returns the parent of this syntax element. This parent is determined by the
	 * containment hierarchy in the CS model.
	 */
	public BbcodeSyntaxElement getParent() {
		return parent;
	}
	
	public BbcodeSyntaxElement[] getChildren() {
		if (children == null) {
			return new BbcodeSyntaxElement[0];
		}
		return children;
	}
	
	public EClass getMetaclass() {
		return parent.getMetaclass();
	}
	
	public de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality getCardinality() {
		return cardinality;
	}
	
}
