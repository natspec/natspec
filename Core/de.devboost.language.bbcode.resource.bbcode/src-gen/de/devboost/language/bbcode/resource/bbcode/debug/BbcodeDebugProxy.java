/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.debug;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import org.eclipse.debug.core.model.IValue;
import org.eclipse.debug.core.model.IVariable;

/**
 * The DebugProxy allows to communicate between the interpreter, which runs in a
 * separate thread or process and the Eclipse Debug framework (i.e., the
 * DebugTarget class).
 */
public class BbcodeDebugProxy {
	
	public static final int STARTUP_DELAY = 1000;
	
	private PrintStream output;
	
	private BufferedReader reader;
	
	private de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugTarget debugTarget;
	
	private de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugCommunicationHelper communicationHelper = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugCommunicationHelper();
	
	public BbcodeDebugProxy(de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugTarget debugTarget, int requestPort) throws UnknownHostException, IOException {
		this.debugTarget = debugTarget;
		// give interpreter a chance to start
		try {
			Thread.sleep(STARTUP_DELAY);
		} catch (InterruptedException e) {
		}
		startSocket(requestPort);
	}
	
	private void startSocket(int requestPort) throws UnknownHostException, IOException {
		// creating client proxy socket (trying to connect)...
		Socket client = new Socket("localhost", requestPort);
		// creating client proxy socket - done. (connected)
		try {
			BufferedInputStream input = new BufferedInputStream(client.getInputStream());
			reader = new BufferedReader(new InputStreamReader(input));
		} catch (IOException e) {
			System.out.println(e);
		}
		try {
			output = new PrintStream(client.getOutputStream());
		} catch (IOException e) {
			System.out.println(e);
		}
	}
	
	public void resume() {
		sendCommand(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.RESUME);
	}
	
	public void stepOver() {
		sendCommand(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.STEP_OVER);
	}
	
	public void stepInto() {
		sendCommand(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.STEP_INTO);
	}
	
	public void stepReturn() {
		sendCommand(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.STEP_RETURN);
	}
	
	public void terminate() {
		sendCommand(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.EXIT);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage getStack() {
		return sendCommandAndRead(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.GET_STACK);
	}
	
	public void addLineBreakpoint(String location, int line) {
		de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.ADD_LINE_BREAKPOINT, new String[] {location, Integer.toString(line)});
		communicationHelper.sendEvent(message, output);
	}
	
	public void removeLineBreakpoint(String location, int line) {
		de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.REMOVE_LINE_BREAKPOINT, new String[] {location, Integer.toString(line)});
		communicationHelper.sendEvent(message, output);
	}
	
	public IVariable[] getStackVariables(String stackFrame) {
		de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage response = sendCommandAndRead(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.GET_FRAME_VARIABLES, new String[] {stackFrame});
		String[] ids = response.getArguments();
		// fetch all variables
		IVariable[] variables = getVariables(ids);
		return variables;
	}
	
	public IVariable[] getVariables(String... requestedIDs) {
		de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage response = sendCommandAndRead(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes.GET_VARIABLES, requestedIDs);
		String[] varStrings = response.getArguments();
		de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugVariable[] variables  = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugVariable[varStrings.length];
		int i = 0;
		for (String varString : varStrings) {
			Map<String, String> properties = de.devboost.language.bbcode.resource.bbcode.util.BbcodeStringUtil.convertFromString(varString);
			
			// convert varString to variables and values
			String valueString = properties.get("!valueString");
			String valueRefType = "valueRefType";
			Map<String, Long> childVariables = new TreeMap<String, Long>(new Comparator<String>() {
				public int compare(String s1, String s2) {
					return s1.compareToIgnoreCase(s2);
				}
			});
			for (String property : properties.keySet()) {
				// ignore special properties - they are not children
				if (property.startsWith("!")) {
					continue;
				}
				childVariables.put(property, Long.parseLong(properties.get(property)));
			}
			String id = properties.get("!id");
			IValue value = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugValue(debugTarget, id, valueString, valueRefType, childVariables);
			
			String variableName = properties.get("!name");
			String variableRefType = properties.get("!type");
			
			de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugVariable variable = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugVariable(debugTarget, variableName, value, variableRefType);
			variables[i++] = variable;
		}
		return variables;
	}
	
	private void sendCommand(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes command, String... parameters) {
		de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage(command, parameters);
		communicationHelper.sendEvent(message, output);
	}
	
	private de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage sendCommandAndRead(de.devboost.language.bbcode.resource.bbcode.debug.EBbcodeDebugMessageTypes command, String... parameters) {
		de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message = new de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage(command, parameters);
		return communicationHelper.sendAndReceive(message, output, reader);
	}
	
}
