/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;

import java.util.Collection;
import java.util.Set;
import org.eclipse.emf.ecore.EClass;

/**
 * An element that is expected at a given position in a resource stream.
 */
public interface IBbcodeExpectedElement {
	
	/**
	 * Returns the names of all tokens that are expected at the given position.
	 */
	public Set<String> getTokenNames();
	
	/**
	 * Returns the metaclass of the rule that contains the expected element.
	 */
	public EClass getRuleMetaclass();
	
	/**
	 * Returns the syntax element that is expected.
	 */
	public de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement getSymtaxElement();
	
	/**
	 * Adds an element that is a valid follower for this element.
	 */
	public void addFollower(de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement follower, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] path);
	
	/**
	 * Returns all valid followers for this element. Each follower is represented by a
	 * pair of an expected elements and the containment trace that leads from the
	 * current element to the follower.
	 */
	public Collection<de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]>> getFollowers();
	
}
