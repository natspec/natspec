/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.debug;


public interface IBbcodeDebugEventListener {
	
	/**
	 * Notification that the given event occurred in the while debugging.
	 */
	public void handleMessage(de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message);
}
