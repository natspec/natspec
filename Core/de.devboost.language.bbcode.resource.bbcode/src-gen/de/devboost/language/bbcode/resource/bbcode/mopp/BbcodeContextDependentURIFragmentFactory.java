/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * <p>
 * A factory for ContextDependentURIFragments. Given a feasible reference
 * resolver, this factory returns a matching fragment that used the resolver to
 * resolver proxy objects.
 * </p>
 * 
 * @param <ContainerType> the type of the class containing the reference to be
 * resolved
 * @param <ReferenceType> the type of the reference to be resolved
 */
public class BbcodeContextDependentURIFragmentFactory<ContainerType extends EObject, ReferenceType extends EObject>  implements de.devboost.language.bbcode.resource.bbcode.IBbcodeContextDependentURIFragmentFactory<ContainerType, ReferenceType> {
	
	private final de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<ContainerType, ReferenceType> resolver;
	
	public BbcodeContextDependentURIFragmentFactory(de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<ContainerType, ReferenceType> resolver) {
		this.resolver = resolver;
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeContextDependentURIFragment<?> create(String identifier, ContainerType container, EReference reference, int positionInReference, EObject proxy) {
		
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContextDependentURIFragment<ContainerType, ReferenceType>(identifier, container, reference, positionInReference, proxy) {
			public de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<ContainerType, ReferenceType> getResolver() {
				return resolver;
			}
		};
	}
}
