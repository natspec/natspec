/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;

import org.eclipse.emf.ecore.EObject;

public interface IBbcodeInterpreterListener {
	
	public void handleInterpreteObject(EObject element);
}
