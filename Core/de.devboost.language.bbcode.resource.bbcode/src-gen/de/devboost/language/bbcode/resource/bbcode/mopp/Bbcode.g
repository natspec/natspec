grammar Bbcode;

options {
	superClass = BbcodeANTLRParserBase;
	backtrack = true;
	memoize = true;
}

@lexer::header {
	package de.devboost.language.bbcode.resource.bbcode.mopp;
	
	import java.util.ArrayList;
import java.util.List;
import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.RecognitionException;
}

@lexer::members {
	public List<RecognitionException> lexerExceptions  = new ArrayList<RecognitionException>();
	public List<Integer> lexerExceptionPositions = new ArrayList<Integer>();
	
	public void reportError(RecognitionException e) {
		lexerExceptions.add(e);
		lexerExceptionPositions.add(((ANTLRStringStream) input).index());
	}
}
@header{
	package de.devboost.language.bbcode.resource.bbcode.mopp;
	
	import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.runtime3_4_0.ANTLRInputStream;
import org.antlr.runtime3_4_0.BitSet;
import org.antlr.runtime3_4_0.CommonToken;
import org.antlr.runtime3_4_0.CommonTokenStream;
import org.antlr.runtime3_4_0.IntStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.RecognitionException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
}

@members{
	private de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolverFactory tokenResolverFactory = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenResolverFactory();
	
	/**
	 * the index of the last token that was handled by collectHiddenTokens()
	 */
	private int lastPosition;
	
	/**
	 * A flag that indicates whether the parser should remember all expected elements.
	 * This flag is set to true when using the parse for code completion. Otherwise it
	 * is set to false.
	 */
	private boolean rememberExpectedElements = false;
	
	private Object parseToIndexTypeObject;
	private int lastTokenIndex = 0;
	
	/**
	 * A list of expected elements the were collected while parsing the input stream.
	 * This list is only filled if <code>rememberExpectedElements</code> is set to
	 * true.
	 */
	private List<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> expectedElements = new ArrayList<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal>();
	
	private int mismatchedTokenRecoveryTries = 0;
	/**
	 * A helper list to allow a lexer to pass errors to its parser
	 */
	protected List<RecognitionException> lexerExceptions = Collections.synchronizedList(new ArrayList<RecognitionException>());
	
	/**
	 * Another helper list to allow a lexer to pass positions of errors to its parser
	 */
	protected List<Integer> lexerExceptionPositions = Collections.synchronizedList(new ArrayList<Integer>());
	
	/**
	 * A stack for incomplete objects. This stack is used filled when the parser is
	 * used for code completion. Whenever the parser starts to read an object it is
	 * pushed on the stack. Once the element was parser completely it is popped from
	 * the stack.
	 */
	List<EObject> incompleteObjects = new ArrayList<EObject>();
	
	private int stopIncludingHiddenTokens;
	private int stopExcludingHiddenTokens;
	private int tokenIndexOfLastCompleteElement;
	
	private int expectedElementsIndexOfLastCompleteElement;
	
	/**
	 * The offset indicating the cursor position when the parser is used for code
	 * completion by calling parseToExpectedElements().
	 */
	private int cursorOffset;
	
	/**
	 * The offset of the first hidden token of the last expected element. This offset
	 * is used to discard expected elements, which are not needed for code completion.
	 */
	private int lastStartIncludingHidden;
	
	private de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap;
	
	private de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeSyntaxErrorMessageConverter syntaxErrorMessageConverter = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeSyntaxErrorMessageConverter(tokenNames);
	
	@Override
	public void reportError(RecognitionException re) {
		addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
	}
	
	protected void addErrorToResource(final String errorMessage, final int column, final int line, final int startIndex, final int stopIndex) {
		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
				if (resource == null) {
					// the resource can be null if the parser is used for code completion
					return true;
				}
				resource.addProblem(new de.devboost.language.bbcode.resource.bbcode.IBbcodeProblem() {
					public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity getSeverity() {
						return de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemSeverity.ERROR;
					}
					public de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType getType() {
						return de.devboost.language.bbcode.resource.bbcode.BbcodeEProblemType.SYNTAX_ERROR;
					}
					public String getMessage() {
						return errorMessage;
					}
					public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeQuickFix> getQuickFixes() {
						return null;
					}
				}, column, line, startIndex, stopIndex);
				return true;
			}
		});
	}
	
	protected void addErrorToResource(de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeLocalizedMessage message) {
		if (message == null) {
			return;
		}
		addErrorToResource(message.getMessage(), message.getColumn(), message.getLine(), message.getCharStart(), message.getCharEnd());
	}
	
	public void addExpectedElement(EClass eClass, int[] ids) {
		if (!this.rememberExpectedElements) {
			return;
		}
		int terminalID = ids[0];
		int followSetID = ids[1];
		de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement terminal = de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeFollowSetProvider.TERMINALS[terminalID];
		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] containmentFeatures = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[ids.length - 2];
		for (int i = 2; i < ids.length; i++) {
			containmentFeatures[i - 2] = de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeFollowSetProvider.LINKS[ids[i]];
		}
		de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace containmentTrace = new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace(eClass, containmentFeatures);
		EObject container = getLastIncompleteElement();
		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal expectedElement = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal(container, terminal, followSetID, containmentTrace);
		setPosition(expectedElement, input.index());
		int startIncludingHiddenTokens = expectedElement.getStartIncludingHiddenTokens();
		if (lastStartIncludingHidden >= 0 && lastStartIncludingHidden < startIncludingHiddenTokens && cursorOffset > startIncludingHiddenTokens) {
			// clear list of expected elements
			this.expectedElements.clear();
			this.expectedElementsIndexOfLastCompleteElement = 0;
		}
		lastStartIncludingHidden = startIncludingHiddenTokens;
		this.expectedElements.add(expectedElement);
	}
	
	protected void collectHiddenTokens(EObject element) {
	}
	
	protected void copyLocalizationInfos(final EObject source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
				locationMap.setCharStart(target, locationMap.getCharStart(source));
				locationMap.setCharEnd(target, locationMap.getCharEnd(source));
				locationMap.setColumn(target, locationMap.getColumn(source));
				locationMap.setLine(target, locationMap.getLine(source));
				return true;
			}
		});
	}
	
	protected void copyLocalizationInfos(final CommonToken source, final EObject target) {
		if (disableLocationMap) {
			return;
		}
		final de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
				if (source == null) {
					return true;
				}
				locationMap.setCharStart(target, source.getStartIndex());
				locationMap.setCharEnd(target, source.getStopIndex());
				locationMap.setColumn(target, source.getCharPositionInLine());
				locationMap.setLine(target, source.getLine());
				return true;
			}
		});
	}
	
	/**
	 * Sets the end character index and the last line for the given object in the
	 * location map.
	 */
	protected void setLocalizationEnd(Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>> postParseCommands , final EObject object, final int endChar, final int endLine) {
		if (disableLocationMap) {
			return;
		}
		final de.devboost.language.bbcode.resource.bbcode.IBbcodeLocationMap locationMap = this.locationMap;
		if (locationMap == null) {
			// the locationMap can be null if the parser is used for code completion
			return;
		}
		postParseCommands.add(new de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>() {
			public boolean execute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
				locationMap.setCharEnd(object, endChar);
				locationMap.setLine(object, endLine);
				return true;
			}
		});
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTextParser createInstance(InputStream actualInputStream, String encoding) {
		try {
			if (encoding == null) {
				return new BbcodeParser(new CommonTokenStream(new BbcodeLexer(new ANTLRInputStream(actualInputStream))));
			} else {
				return new BbcodeParser(new CommonTokenStream(new BbcodeLexer(new ANTLRInputStream(actualInputStream, encoding))));
			}
		} catch (IOException e) {
			new de.devboost.language.bbcode.resource.bbcode.util.BbcodeRuntimeUtil().logError("Error while creating parser.", e);
			return null;
		}
	}
	
	/**
	 * This default constructor is only used to call createInstance() on it.
	 */
	public BbcodeParser() {
		super(null);
	}
	
	protected EObject doParse() throws RecognitionException {
		this.lastPosition = 0;
		// required because the lexer class can not be subclassed
		((BbcodeLexer) getTokenStream().getTokenSource()).lexerExceptions = lexerExceptions;
		((BbcodeLexer) getTokenStream().getTokenSource()).lexerExceptionPositions = lexerExceptionPositions;
		Object typeObject = getTypeObject();
		if (typeObject == null) {
			return start();
		} else if (typeObject instanceof EClass) {
			EClass type = (EClass) typeObject;
			if (type.getInstanceClass() == de.devboost.language.bbcode.BBCodeText.class) {
				return parse_de_devboost_language_bbcode_BBCodeText();
			}
			if (type.getInstanceClass() == de.devboost.language.bbcode.EmptyStyle.class) {
				return parse_de_devboost_language_bbcode_EmptyStyle();
			}
			if (type.getInstanceClass() == de.devboost.language.bbcode.Bold.class) {
				return parse_de_devboost_language_bbcode_Bold();
			}
			if (type.getInstanceClass() == de.devboost.language.bbcode.Italic.class) {
				return parse_de_devboost_language_bbcode_Italic();
			}
			if (type.getInstanceClass() == de.devboost.language.bbcode.Strikethrough.class) {
				return parse_de_devboost_language_bbcode_Strikethrough();
			}
			if (type.getInstanceClass() == de.devboost.language.bbcode.Underline.class) {
				return parse_de_devboost_language_bbcode_Underline();
			}
			if (type.getInstanceClass() == de.devboost.language.bbcode.Color.class) {
				return parse_de_devboost_language_bbcode_Color();
			}
		}
		throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeUnexpectedContentTypeException(typeObject);
	}
	
	public int getMismatchedTokenRecoveryTries() {
		return mismatchedTokenRecoveryTries;
	}
	
	public Object getMissingSymbol(IntStream arg0, RecognitionException arg1, int arg2, BitSet arg3) {
		mismatchedTokenRecoveryTries++;
		return super.getMissingSymbol(arg0, arg1, arg2, arg3);
	}
	
	public Object getParseToIndexTypeObject() {
		return parseToIndexTypeObject;
	}
	
	protected Object getTypeObject() {
		Object typeObject = getParseToIndexTypeObject();
		if (typeObject != null) {
			return typeObject;
		}
		Map<?,?> options = getOptions();
		if (options != null) {
			typeObject = options.get(de.devboost.language.bbcode.resource.bbcode.IBbcodeOptions.RESOURCE_CONTENT_TYPE);
		}
		return typeObject;
	}
	
	/**
	 * Implementation that calls {@link #doParse()} and handles the thrown
	 * RecognitionExceptions.
	 */
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeParseResult parse() {
		// Reset parser state
		terminateParsing = false;
		postParseCommands = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource>>();
		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeParseResult parseResult = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeParseResult();
		if (disableLocationMap) {
			locationMap = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeDevNullLocationMap();
		} else {
			locationMap = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeLocationMap();
		}
		// Run parser
		try {
			EObject result =  doParse();
			if (lexerExceptions.isEmpty()) {
				parseResult.setRoot(result);
				parseResult.setLocationMap(locationMap);
			}
		} catch (RecognitionException re) {
			addErrorToResource(syntaxErrorMessageConverter.translateParseError(re));
		} catch (IllegalArgumentException iae) {
			if ("The 'no null' constraint is violated".equals(iae.getMessage())) {
				// can be caused if a null is set on EMF models where not allowed. this will just
				// happen if other errors occurred before
			} else {
				iae.printStackTrace();
			}
		}
		for (RecognitionException re : lexerExceptions) {
			addErrorToResource(syntaxErrorMessageConverter.translateLexicalError(re, lexerExceptions, lexerExceptionPositions));
		}
		parseResult.getPostParseCommands().addAll(postParseCommands);
		return parseResult;
	}
	
	public List<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> parseToExpectedElements(EClass type, de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource dummyResource, int cursorOffset) {
		this.rememberExpectedElements = true;
		this.parseToIndexTypeObject = type;
		this.cursorOffset = cursorOffset;
		this.lastStartIncludingHidden = -1;
		final CommonTokenStream tokenStream = (CommonTokenStream) getTokenStream();
		de.devboost.language.bbcode.resource.bbcode.IBbcodeParseResult result = parse();
		for (EObject incompleteObject : incompleteObjects) {
			Lexer lexer = (Lexer) tokenStream.getTokenSource();
			int endChar = lexer.getCharIndex();
			int endLine = lexer.getLine();
			setLocalizationEnd(result.getPostParseCommands(), incompleteObject, endChar, endLine);
		}
		if (result != null) {
			EObject root = result.getRoot();
			if (root != null) {
				dummyResource.getContentsInternal().add(root);
			}
			for (de.devboost.language.bbcode.resource.bbcode.IBbcodeCommand<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource> command : result.getPostParseCommands()) {
				command.execute(dummyResource);
			}
		}
		// remove all expected elements that were added after the last complete element
		expectedElements = expectedElements.subList(0, expectedElementsIndexOfLastCompleteElement + 1);
		int lastFollowSetID = expectedElements.get(expectedElementsIndexOfLastCompleteElement).getFollowSetID();
		Set<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> currentFollowSet = new LinkedHashSet<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal>();
		List<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal> newFollowSet = new ArrayList<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal>();
		for (int i = expectedElementsIndexOfLastCompleteElement; i >= 0; i--) {
			de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal expectedElementI = expectedElements.get(i);
			if (expectedElementI.getFollowSetID() == lastFollowSetID) {
				currentFollowSet.add(expectedElementI);
			} else {
				break;
			}
		}
		int followSetID = 26;
		int i;
		for (i = tokenIndexOfLastCompleteElement; i < tokenStream.size(); i++) {
			CommonToken nextToken = (CommonToken) tokenStream.get(i);
			if (nextToken.getType() < 0) {
				break;
			}
			if (nextToken.getChannel() == 99) {
				// hidden tokens do not reduce the follow set
			} else {
				// now that we have found the next visible token the position for that expected
				// terminals can be set
				for (de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal nextFollow : newFollowSet) {
					lastTokenIndex = 0;
					setPosition(nextFollow, i);
				}
				newFollowSet.clear();
				// normal tokens do reduce the follow set - only elements that match the token are
				// kept
				for (de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal nextFollow : currentFollowSet) {
					if (nextFollow.getTerminal().getTokenNames().contains(getTokenNames()[nextToken.getType()])) {
						// keep this one - it matches
						Collection<de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]>> newFollowers = nextFollow.getTerminal().getFollowers();
						for (de.devboost.language.bbcode.resource.bbcode.util.BbcodePair<de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[]> newFollowerPair : newFollowers) {
							de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement newFollower = newFollowerPair.getLeft();
							EObject container = getLastIncompleteElement();
							de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace containmentTrace = new de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeContainmentTrace(null, newFollowerPair.getRight());
							de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal newFollowTerminal = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal(container, newFollower, followSetID, containmentTrace);
							newFollowSet.add(newFollowTerminal);
							expectedElements.add(newFollowTerminal);
						}
					}
				}
				currentFollowSet.clear();
				currentFollowSet.addAll(newFollowSet);
			}
			followSetID++;
		}
		// after the last token in the stream we must set the position for the elements
		// that were added during the last iteration of the loop
		for (de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal nextFollow : newFollowSet) {
			lastTokenIndex = 0;
			setPosition(nextFollow, i);
		}
		return this.expectedElements;
	}
	
	public void setPosition(de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedTerminal expectedElement, int tokenIndex) {
		int currentIndex = Math.max(0, tokenIndex);
		for (int index = lastTokenIndex; index < currentIndex; index++) {
			if (index >= input.size()) {
				break;
			}
			CommonToken tokenAtIndex = (CommonToken) input.get(index);
			stopIncludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			if (tokenAtIndex.getChannel() != 99 && !anonymousTokens.contains(tokenAtIndex)) {
				stopExcludingHiddenTokens = tokenAtIndex.getStopIndex() + 1;
			}
		}
		lastTokenIndex = Math.max(0, currentIndex);
		expectedElement.setPosition(stopExcludingHiddenTokens, stopIncludingHiddenTokens);
	}
	
	public Object recoverFromMismatchedToken(IntStream input, int ttype, BitSet follow) throws RecognitionException {
		if (!rememberExpectedElements) {
			return super.recoverFromMismatchedToken(input, ttype, follow);
		} else {
			return null;
		}
	}
	
	private void startIncompleteElement(Object object) {
		if (object instanceof EObject) {
			this.incompleteObjects.add((EObject) object);
		}
	}
	
	private void completedElement(Object object, boolean isContainment) {
		if (isContainment && !this.incompleteObjects.isEmpty()) {
			boolean exists = this.incompleteObjects.remove(object);
			if (!exists) {
			}
		}
		if (object instanceof EObject) {
			this.tokenIndexOfLastCompleteElement = getTokenStream().index();
			this.expectedElementsIndexOfLastCompleteElement = expectedElements.size() - 1;
		}
	}
	
	private EObject getLastIncompleteElement() {
		if (incompleteObjects.isEmpty()) {
			return null;
		}
		return incompleteObjects.get(incompleteObjects.size() - 1);
	}
	
}

start returns [ EObject element = null]
:
	{
		// follow set for start rule(s)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[0]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[1]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[2]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[3]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[4]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[5]);
		expectedElementsIndexOfLastCompleteElement = 0;
	}
	(
		c0 = parse_de_devboost_language_bbcode_BBCodeText{ element = c0; }
	)
	EOF	{
		retrieveLayoutInformation(element, null, null, false);
	}
	
;

parse_de_devboost_language_bbcode_BBCodeText returns [de.devboost.language.bbcode.BBCodeText element = null]
@init{
}
:
	(
		(
			(
				a0_0 = parse_de_devboost_language_bbcode_Style				{
					if (terminateParsing) {
						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
					}
					if (element == null) {
						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBBCodeText();
						startIncompleteElement(element);
					}
					if (a0_0 != null) {
						if (a0_0 != null) {
							Object value = a0_0;
							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.BB_CODE_TEXT__STYLE_PARTS, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_0_0_0_0_0_0_0, a0_0, true);
						copyLocalizationInfos(a0_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[6]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[7]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[8]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[9]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[10]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[11]);
			}
			
		)
		
	)+	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[12]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[13]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[14]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[15]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[16]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[17]);
	}
	
;

parse_de_devboost_language_bbcode_EmptyStyle returns [de.devboost.language.bbcode.EmptyStyle element = null]
@init{
}
:
	(
		a0 = TEXT		
		{
			if (terminateParsing) {
				throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
			}
			if (element == null) {
				element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createEmptyStyle();
				startIncompleteElement(element);
			}
			if (a0 != null) {
				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a0.getText(), element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.EMPTY_STYLE__TEXT), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a0).getLine(), ((CommonToken) a0).getCharPositionInLine(), ((CommonToken) a0).getStartIndex(), ((CommonToken) a0).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.EMPTY_STYLE__TEXT), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_1_0_0_0, resolved, true);
				copyLocalizationInfos((CommonToken) a0, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[18]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[19]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[20]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[21]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[22]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[23]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[24]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[25]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[26]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[27]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[28]);
	}
	
;

parse_de_devboost_language_bbcode_Bold returns [de.devboost.language.bbcode.Bold element = null]
@init{
}
:
	a0 = '[b]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBold();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[29]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[30]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[31]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[32]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[33]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[34]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[35]);
	}
	
	(
		(
			(
				a1_0 = parse_de_devboost_language_bbcode_Style				{
					if (terminateParsing) {
						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
					}
					if (element == null) {
						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBold();
						startIncompleteElement(element);
					}
					if (a1_0 != null) {
						if (a1_0 != null) {
							Object value = a1_0;
							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.BOLD__CHILDREN, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_1_0_0_0, a1_0, true);
						copyLocalizationInfos(a1_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[36]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[37]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[38]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[39]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[40]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[41]);
				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[42]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[43]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[44]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[45]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[46]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[47]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[48]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[49]);
	}
	
	a2 = '[/b]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createBold();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[50]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[51]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[52]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[53]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[54]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[55]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[56]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[57]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[58]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[59]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[60]);
	}
	
;

parse_de_devboost_language_bbcode_Italic returns [de.devboost.language.bbcode.Italic element = null]
@init{
}
:
	a0 = '[i]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createItalic();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[61]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[62]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[63]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[64]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[65]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[66]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[67]);
	}
	
	(
		(
			(
				a1_0 = parse_de_devboost_language_bbcode_Style				{
					if (terminateParsing) {
						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
					}
					if (element == null) {
						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createItalic();
						startIncompleteElement(element);
					}
					if (a1_0 != null) {
						if (a1_0 != null) {
							Object value = a1_0;
							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.ITALIC__CHILDREN, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_1_0_0_0, a1_0, true);
						copyLocalizationInfos(a1_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[68]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[69]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[70]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[71]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[72]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[73]);
				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[74]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[75]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[76]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[77]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[78]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[79]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[80]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[81]);
	}
	
	a2 = '[/i]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createItalic();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[82]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[83]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[84]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[85]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[86]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[87]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[88]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[89]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[90]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[91]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[92]);
	}
	
;

parse_de_devboost_language_bbcode_Strikethrough returns [de.devboost.language.bbcode.Strikethrough element = null]
@init{
}
:
	a0 = '[s]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createStrikethrough();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[93]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[94]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[95]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[96]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[97]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[98]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[99]);
	}
	
	(
		(
			(
				a1_0 = parse_de_devboost_language_bbcode_Style				{
					if (terminateParsing) {
						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
					}
					if (element == null) {
						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createStrikethrough();
						startIncompleteElement(element);
					}
					if (a1_0 != null) {
						if (a1_0 != null) {
							Object value = a1_0;
							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.STRIKETHROUGH__CHILDREN, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_1_0_0_0, a1_0, true);
						copyLocalizationInfos(a1_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[100]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[101]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[102]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[103]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[104]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[105]);
				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[106]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[107]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[108]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[109]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[110]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[111]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[112]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[113]);
	}
	
	a2 = '[/s]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createStrikethrough();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[114]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[115]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[116]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[117]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[118]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[119]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[120]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[121]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[122]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[123]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[124]);
	}
	
;

parse_de_devboost_language_bbcode_Underline returns [de.devboost.language.bbcode.Underline element = null]
@init{
}
:
	a0 = '[u]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createUnderline();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[125]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[126]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[127]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[128]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[129]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[130]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[131]);
	}
	
	(
		(
			(
				a1_0 = parse_de_devboost_language_bbcode_Style				{
					if (terminateParsing) {
						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
					}
					if (element == null) {
						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createUnderline();
						startIncompleteElement(element);
					}
					if (a1_0 != null) {
						if (a1_0 != null) {
							Object value = a1_0;
							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.UNDERLINE__CHILDREN, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_1_0_0_0, a1_0, true);
						copyLocalizationInfos(a1_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[132]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[133]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[134]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[135]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[136]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[137]);
				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[138]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[139]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[140]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[141]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[142]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[143]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[144]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[145]);
	}
	
	a2 = '[/u]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createUnderline();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[146]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[147]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[148]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[149]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[150]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[151]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[152]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[153]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[154]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[155]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[156]);
	}
	
;

parse_de_devboost_language_bbcode_Color returns [de.devboost.language.bbcode.Color element = null]
@init{
}
:
	a0 = '[color=' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_0, null, true);
		copyLocalizationInfos((CommonToken)a0, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[157]);
	}
	
	(
		a1 = TEXT		
		{
			if (terminateParsing) {
				throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
			}
			if (element == null) {
				element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
				startIncompleteElement(element);
			}
			if (a1 != null) {
				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolver tokenResolver = tokenResolverFactory.createTokenResolver("TEXT");
				tokenResolver.setOptions(getOptions());
				de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolveResult result = getFreshTokenResolveResult();
				tokenResolver.resolve(a1.getText(), element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.COLOR__VALUE), result);
				Object resolvedObject = result.getResolvedToken();
				if (resolvedObject == null) {
					addErrorToResource(result.getErrorMessage(), ((CommonToken) a1).getLine(), ((CommonToken) a1).getCharPositionInLine(), ((CommonToken) a1).getStartIndex(), ((CommonToken) a1).getStopIndex());
				}
				java.lang.String resolved = (java.lang.String) resolvedObject;
				if (resolved != null) {
					Object value = resolved;
					element.eSet(element.eClass().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.COLOR__VALUE), value);
					completedElement(value, false);
				}
				collectHiddenTokens(element);
				retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_1, resolved, true);
				copyLocalizationInfos((CommonToken) a1, element);
			}
		}
	)
	{
		// expected elements (follow set)
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[158]);
	}
	
	a2 = ']' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_2, null, true);
		copyLocalizationInfos((CommonToken)a2, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[159]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[160]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[161]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[162]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[163]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[164]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[165]);
	}
	
	(
		(
			(
				a3_0 = parse_de_devboost_language_bbcode_Style				{
					if (terminateParsing) {
						throw new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTerminateParsingException();
					}
					if (element == null) {
						element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
						startIncompleteElement(element);
					}
					if (a3_0 != null) {
						if (a3_0 != null) {
							Object value = a3_0;
							addObjectToList(element, de.devboost.language.bbcode.BbcodePackage.COLOR__CHILDREN, value);
							completedElement(value, true);
						}
						collectHiddenTokens(element);
						retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_3_0_0_0, a3_0, true);
						copyLocalizationInfos(a3_0, element);
					}
				}
			)
			{
				// expected elements (follow set)
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[166]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[167]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[168]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[169]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[170]);
				addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[171]);
				addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[172]);
			}
			
		)
		
	)*	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[173]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[174]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[175]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[176]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[177]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[178]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[179]);
	}
	
	a4 = '[/color]' {
		if (element == null) {
			element = de.devboost.language.bbcode.BbcodeFactory.eINSTANCE.createColor();
			startIncompleteElement(element);
		}
		collectHiddenTokens(element);
		retrieveLayoutInformation(element, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_4, null, true);
		copyLocalizationInfos((CommonToken)a4, element);
	}
	{
		// expected elements (follow set)
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[180]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[181]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[182]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[183]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[184]);
		addExpectedElement(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText(), de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[185]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[186]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[187]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[188]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[189]);
		addExpectedElement(null, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectationConstants.EXPECTATIONS[190]);
	}
	
;

parse_de_devboost_language_bbcode_Style returns [de.devboost.language.bbcode.Style element = null]
:
	c0 = parse_de_devboost_language_bbcode_EmptyStyle{ element = c0; /* this is a subclass or primitive expression choice */ }
	|	c1 = parse_de_devboost_language_bbcode_Bold{ element = c1; /* this is a subclass or primitive expression choice */ }
	|	c2 = parse_de_devboost_language_bbcode_Italic{ element = c2; /* this is a subclass or primitive expression choice */ }
	|	c3 = parse_de_devboost_language_bbcode_Strikethrough{ element = c3; /* this is a subclass or primitive expression choice */ }
	|	c4 = parse_de_devboost_language_bbcode_Underline{ element = c4; /* this is a subclass or primitive expression choice */ }
	|	c5 = parse_de_devboost_language_bbcode_Color{ element = c5; /* this is a subclass or primitive expression choice */ }
	
;

WHITESPACE:
	(' ' | '\t' | '\f')
	{ _channel = 99; }
;
TEXT:
	((('A'..'Z' | 'a'..'z' | '0'..'9' | '-'| '_' | '#' ))+)
;
LINEBREAK:
	(('\r\n' | '\r' | '\n'))
	{ _channel = 99; }
;

