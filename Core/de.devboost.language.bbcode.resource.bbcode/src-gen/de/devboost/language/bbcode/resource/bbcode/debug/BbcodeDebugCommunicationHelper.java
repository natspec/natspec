/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.debug;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

public class BbcodeDebugCommunicationHelper {
	
	public void sendEvent(de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message, PrintStream stream) {
		synchronized (stream) {
			stream.println(message.serialize());
		}
	}
	
	/**
	 * <p>
	 * Sends a message using the given stream and waits for an answer.
	 * </p>
	 * 
	 * @param messageType the type of message to send
	 * @param stream the stream to send the message to
	 * @param reader the reader to obtain the answer from
	 * @param parameters additional parameter to send
	 * 
	 * @return the answer that is received
	 */
	public de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage sendAndReceive(de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage message, PrintStream stream, BufferedReader reader) {
		synchronized (stream) {
			sendEvent(message, stream);
			de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage response = receive(reader);
			return response;
		}
	}
	
	/**
	 * <p>
	 * Receives a message from the given reader. This method block until a message has
	 * arrived.
	 * </p>
	 * 
	 * @param reader the read to obtain the message from
	 * 
	 * @return the received message
	 */
	public de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage receive(BufferedReader reader) {
		try {
			String response = reader.readLine();
			if (response == null) {
				return null;
			}
			de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage receivedMessage = de.devboost.language.bbcode.resource.bbcode.debug.BbcodeDebugMessage.deserialize(response);
			return receivedMessage;
		} catch (IOException e) {
			return null;
		}
	}
	
}
