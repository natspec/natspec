/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class provides information about how brackets must be handled in the
 * editor (e.g., whether they must be closed automatically).
 */
public class BbcodeBracketInformationProvider {
	
	public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeBracketPair> getBracketPairs() {
		Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeBracketPair> result = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeBracketPair>();
		return result;
	}
	
}
