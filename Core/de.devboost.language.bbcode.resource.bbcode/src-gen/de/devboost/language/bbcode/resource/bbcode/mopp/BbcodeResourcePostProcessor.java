/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;


public class BbcodeResourcePostProcessor implements de.devboost.language.bbcode.resource.bbcode.IBbcodeResourcePostProcessor {
	
	public void process(de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResource resource) {
		// Set the overrideResourcePostProcessor option to false to customize resource
		// post processing.
	}
	
	public void terminate() {
		// To signal termination to the process() method, setting a boolean field is
		// recommended. Depending on the value of this field process() can stop its
		// computation. However, this is only required for computation intensive
		// post-processors.
	}
	
}
