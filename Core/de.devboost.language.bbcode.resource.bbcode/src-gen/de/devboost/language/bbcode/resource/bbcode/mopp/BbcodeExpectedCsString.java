/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.Collections;
import java.util.Set;

/**
 * A representation for a range in a document where a keyword (i.e., a static
 * string) is expected.
 */
public class BbcodeExpectedCsString extends de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeAbstractExpectedElement {
	
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword keyword;
	
	public BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeKeyword keyword) {
		super(keyword.getMetaclass());
		this.keyword = keyword;
	}
	
	public String getValue() {
		return keyword.getValue();
	}
	
	/**
	 * Returns the expected keyword.
	 */
	public de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement getSymtaxElement() {
		return keyword;
	}
	
	public Set<String> getTokenNames() {
		return Collections.singleton("'" + getValue() + "'");
	}
	
	public String toString() {
		return "CsString \"" + getValue() + "\"";
	}
	
	public boolean equals(Object o) {
		if (o instanceof BbcodeExpectedCsString) {
			return getValue().equals(((BbcodeExpectedCsString) o).getValue());
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return getValue().hashCode();
	}
	
}
