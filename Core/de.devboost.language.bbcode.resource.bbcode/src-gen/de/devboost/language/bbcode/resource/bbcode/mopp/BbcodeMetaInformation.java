/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource.Factory;

public class BbcodeMetaInformation implements de.devboost.language.bbcode.resource.bbcode.IBbcodeMetaInformation {
	
	public String getSyntaxName() {
		return "bbcode";
	}
	
	public String getURI() {
		return "http://devboost.de/languages/bbcode";
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTextScanner createLexer() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeAntlrScanner(new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeLexer());
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTextParser createParser(InputStream inputStream, String encoding) {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeParser().createInstance(inputStream, encoding);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTextPrinter createPrinter(OutputStream outputStream, de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource) {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodePrinter2(outputStream, resource);
	}
	
	public EClass[] getClassesWithSyntax() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeSyntaxCoverageInformationProvider().getClassesWithSyntax();
	}
	
	public EClass[] getStartSymbols() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeSyntaxCoverageInformationProvider().getStartSymbols();
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolverSwitch getReferenceResolverSwitch() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeReferenceResolverSwitch();
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenResolverFactory getTokenResolverFactory() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenResolverFactory();
	}
	
	public String getPathToCSDefinition() {
		return "de.devboost.language.bbcode/metamodel/bbcode.cs";
	}
	
	public String[] getTokenNames() {
		return de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeParser.tokenNames;
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle getDefaultTokenStyle(String tokenName) {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenStyleInformationProvider().getDefaultTokenStyle(tokenName);
	}
	
	public Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeBracketPair> getBracketPairs() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeBracketInformationProvider().getBracketPairs();
	}
	
	public EClass[] getFoldableClasses() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeFoldingInformationProvider().getFoldableClasses();
	}
	
	public Factory createResourceFactory() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResourceFactory();
	}
	
	public de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeNewFileContentProvider getNewFileContentProvider() {
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeNewFileContentProvider();
	}
	
	public void registerResourceFactory() {
		// if no resource factory registered, register delegator
		if (Factory.Registry.INSTANCE.getExtensionToFactoryMap().get(getSyntaxName()) == null) {
			Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(getSyntaxName(), new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeResourceFactoryDelegator());
		}
	}
	
	/**
	 * Returns the key of the option that can be used to register a preprocessor that
	 * is used as a pipe when loading resources. This key is language-specific. To
	 * register one preprocessor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getInputStreamPreprocessorProviderOptionKey() {
		return getSyntaxName() + "_" + "INPUT_STREAM_PREPROCESSOR_PROVIDER";
	}
	
	/**
	 * Returns the key of the option that can be used to register a post-processors
	 * that are invoked after loading resources. This key is language-specific. To
	 * register one post-processor for multiple resource types, it must be registered
	 * individually using all keys.
	 */
	public String getResourcePostProcessorProviderOptionKey() {
		return getSyntaxName() + "_" + "RESOURCE_POSTPROCESSOR_PROVIDER";
	}
	
	public String getLaunchConfigurationType() {
		return "de.devboost.language.bbcode.resource.bbcode.ui.launchConfigurationType";
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeNameProvider createNameProvider() {
		return new de.devboost.language.bbcode.resource.bbcode.analysis.BbcodeDefaultNameProvider();
	}
	
	public String[] getSyntaxHighlightableTokenNames() {
		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeAntlrTokenHelper tokenHelper = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeAntlrTokenHelper();
		List<String> highlightableTokens = new ArrayList<String>();
		String[] parserTokenNames = getTokenNames();
		for (int i = 0; i < parserTokenNames.length; i++) {
			// If ANTLR is used we need to normalize the token names
			if (!tokenHelper.canBeUsedForSyntaxHighlighting(i)) {
				continue;
			}
			String tokenName = tokenHelper.getTokenName(parserTokenNames, i);
			if (tokenName == null) {
				continue;
			}
			highlightableTokens.add(tokenName);
		}
		highlightableTokens.add(de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME);
		return highlightableTokens.toArray(new String[highlightableTokens.size()]);
	}
	
}
