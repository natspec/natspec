/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


/**
 * A class to represent a keyword in the grammar.
 */
public class BbcodeKeyword extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement {
	
	private final String value;
	
	public BbcodeKeyword(String value, de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality) {
		super(cardinality, null);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	public String toString() {
		return value;
	}
	
}
