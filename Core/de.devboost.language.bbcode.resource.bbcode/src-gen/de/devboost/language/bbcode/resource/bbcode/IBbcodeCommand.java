/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;


/**
 * A simple interface for commands that can be executed and that return
 * information about the success of their execution.
 */
public interface IBbcodeCommand<ContextType> {
	
	public boolean execute(ContextType context);
}
