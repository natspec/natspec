/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.ArrayList;
import java.util.List;

public class BbcodeSyntaxElementDecorator {
	
	/**
	 * the syntax element to be decorated
	 */
	private de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement decoratedElement;
	
	/**
	 * an array of child decorators (one decorator per child of the decorated syntax
	 * element
	 */
	private BbcodeSyntaxElementDecorator[] childDecorators;
	
	/**
	 * a list of the indices that must be printed
	 */
	private List<Integer> indicesToPrint = new ArrayList<Integer>();
	
	public BbcodeSyntaxElementDecorator(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement decoratedElement, BbcodeSyntaxElementDecorator[] childDecorators) {
		super();
		this.decoratedElement = decoratedElement;
		this.childDecorators = childDecorators;
	}
	
	public void addIndexToPrint(Integer index) {
		indicesToPrint.add(index);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeSyntaxElement getDecoratedElement() {
		return decoratedElement;
	}
	
	public BbcodeSyntaxElementDecorator[] getChildDecorators() {
		return childDecorators;
	}
	
	public Integer getNextIndexToPrint() {
		if (indicesToPrint.size() == 0) {
			return null;
		}
		return indicesToPrint.remove(0);
	}
	
	public String toString() {
		return "" + getDecoratedElement();
	}
	
}
