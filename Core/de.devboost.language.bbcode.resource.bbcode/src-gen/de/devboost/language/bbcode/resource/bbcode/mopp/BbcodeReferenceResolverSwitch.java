/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class BbcodeReferenceResolverSwitch implements de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolverSwitch {
	
	/**
	 * This map stores a copy of the options the were set for loading the resource.
	 */
	private Map<Object, Object> options;
	
	
	public void setOptions(Map<?, ?> options) {
		if (options != null) {
			this.options = new LinkedHashMap<Object, Object>();
			this.options.putAll(options);
		}
	}
	
	public void resolveFuzzy(String identifier, EObject container, EReference reference, int position, de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolveResult<EObject> result) {
		if (container == null) {
			return;
		}
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<? extends EObject, ? extends EObject> getResolver(EStructuralFeature reference) {
		return null;
	}
	
	@SuppressWarnings({"rawtypes", "unchecked"})
	public <ContainerType extends EObject, ReferenceType extends EObject> de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<ContainerType, ReferenceType> getResolverChain(EStructuralFeature reference, de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<ContainerType, ReferenceType> originalResolver) {
		if (options == null) {
			return originalResolver;
		}
		Object value = options.get(de.devboost.language.bbcode.resource.bbcode.IBbcodeOptions.ADDITIONAL_REFERENCE_RESOLVERS);
		if (value == null) {
			return originalResolver;
		}
		if (!(value instanceof Map)) {
			// send this to the error log
			new de.devboost.language.bbcode.resource.bbcode.util.BbcodeRuntimeUtil().logWarning("Found value with invalid type for option " + de.devboost.language.bbcode.resource.bbcode.IBbcodeOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + Map.class.getName() + ", but was " + value.getClass().getName() + ")", null);
			return originalResolver;
		}
		Map<?,?> resolverMap = (Map<?,?>) value;
		Object resolverValue = resolverMap.get(reference);
		if (resolverValue == null) {
			return originalResolver;
		}
		if (resolverValue instanceof de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver) {
			de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver replacingResolver = (de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver) resolverValue;
			if (replacingResolver instanceof de.devboost.language.bbcode.resource.bbcode.IBbcodeDelegatingReferenceResolver) {
				// pass original resolver to the replacing one
				((de.devboost.language.bbcode.resource.bbcode.IBbcodeDelegatingReferenceResolver) replacingResolver).setDelegate(originalResolver);
			}
			return replacingResolver;
		} else if (resolverValue instanceof Collection) {
			Collection replacingResolvers = (Collection) resolverValue;
			de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver replacingResolver = originalResolver;
			for (Object next : replacingResolvers) {
				if (next instanceof de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceCache) {
					de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver nextResolver = (de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver) next;
					if (nextResolver instanceof de.devboost.language.bbcode.resource.bbcode.IBbcodeDelegatingReferenceResolver) {
						// pass original resolver to the replacing one
						((de.devboost.language.bbcode.resource.bbcode.IBbcodeDelegatingReferenceResolver) nextResolver).setDelegate(replacingResolver);
					}
					replacingResolver = nextResolver;
				} else {
					// The collection contains a non-resolver. Send a warning to the error log.
					new de.devboost.language.bbcode.resource.bbcode.util.BbcodeRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.devboost.language.bbcode.resource.bbcode.IBbcodeOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.devboost.language.bbcode.resource.bbcode.IBbcodeDelegatingReferenceResolver.class.getName() + ", but was " + next.getClass().getName() + ")", null);
				}
			}
			return replacingResolver;
		} else {
			// The value for the option ADDITIONAL_REFERENCE_RESOLVERS has an unknown type.
			new de.devboost.language.bbcode.resource.bbcode.util.BbcodeRuntimeUtil().logWarning("Found value with invalid type in value map for option " + de.devboost.language.bbcode.resource.bbcode.IBbcodeOptions.ADDITIONAL_REFERENCE_RESOLVERS + " (expected " + de.devboost.language.bbcode.resource.bbcode.IBbcodeDelegatingReferenceResolver.class.getName() + ", but was " + resolverValue.getClass().getName() + ")", null);
			return originalResolver;
		}
	}
	
}
