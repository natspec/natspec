/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;


/**
 * Implementors of this interface can provide a post-processor for text resources.
 */
public interface IBbcodeResourcePostProcessorProvider {
	
	/**
	 * Returns the processor that shall be called after text resource are successfully
	 * parsed.
	 */
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeResourcePostProcessor getResourcePostProcessor();
	
}
