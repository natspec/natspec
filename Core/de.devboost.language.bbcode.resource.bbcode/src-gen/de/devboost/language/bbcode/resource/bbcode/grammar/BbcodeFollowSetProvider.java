/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * This class provides the follow sets for all terminals of the grammar. These
 * sets are used during code completion.
 */
public class BbcodeFollowSetProvider {
	
	public final static de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement TERMINALS[] = new de.devboost.language.bbcode.resource.bbcode.IBbcodeExpectedElement[13];
	
	public final static EStructuralFeature[] FEATURES = new EStructuralFeature[2];
	
	public final static de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] LINKS = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[144];
	
	public final static de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] EMPTY_LINK_ARRAY = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[0];
	
	public static void initializeTerminals0() {
		TERMINALS[0] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedStructuralFeature(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_1_0_0_0);
		TERMINALS[1] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_0);
		TERMINALS[2] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_0);
		TERMINALS[3] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_0);
		TERMINALS[4] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_0);
		TERMINALS[5] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_0);
		TERMINALS[6] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_2_0_0_2);
		TERMINALS[7] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_3_0_0_2);
		TERMINALS[8] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_4_0_0_2);
		TERMINALS[9] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_5_0_0_2);
		TERMINALS[10] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_4);
		TERMINALS[11] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedStructuralFeature(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_1);
		TERMINALS[12] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeExpectedCsString(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeGrammarInformationProvider.BBCODE_6_0_0_2);
	}
	
	public static void initializeTerminals() {
		initializeTerminals0();
	}
	
	public static void initializeFeatures0() {
		FEATURES[0] = de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBBCodeText().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.BB_CODE_TEXT__STYLE_PARTS);
		FEATURES[1] = de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getCompositeStyle().getEStructuralFeature(de.devboost.language.bbcode.BbcodePackage.COMPOSITE_STYLE__CHILDREN);
	}
	
	public static void initializeFeatures() {
		initializeFeatures0();
	}
	
	public static void initializeLinks0() {
		LINKS[0] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[1] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[2] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[3] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[4] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[5] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[6] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[7] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[8] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[9] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[10] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[11] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[12] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[13] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[14] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[15] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[16] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[17] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[18] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[19] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[20] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[21] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[22] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[23] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[24] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[25] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[26] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[27] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[28] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[29] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[30] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[31] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[32] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[33] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[34] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[35] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[36] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[37] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[38] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[39] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[40] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[41] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[42] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[43] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[44] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[45] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[46] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[47] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[48] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[49] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[50] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[51] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[52] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[53] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[54] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[55] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[56] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[57] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[58] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[59] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[60] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[61] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[62] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[63] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[64] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[65] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[66] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[67] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[68] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[69] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[70] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[71] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[72] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[73] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[74] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[75] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[76] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[77] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[78] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[79] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[80] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[81] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[82] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[83] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[84] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[85] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[86] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[87] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[88] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[89] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[90] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[91] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[92] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[93] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[94] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[95] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[96] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[97] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[98] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[99] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[100] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[101] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[102] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[103] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[104] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[105] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[106] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[107] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[108] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[109] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[110] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[111] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[112] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[113] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[114] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[115] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[116] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[117] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[118] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[119] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
		LINKS[120] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[121] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[122] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[123] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[124] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[125] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[126] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[127] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[128] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[129] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[130] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[131] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[132] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]);
		LINKS[133] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]);
		LINKS[134] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]);
		LINKS[135] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]);
		LINKS[136] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]);
		LINKS[137] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]);
		LINKS[138] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]);
		LINKS[139] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]);
		LINKS[140] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]);
		LINKS[141] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]);
		LINKS[142] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]);
		LINKS[143] = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]);
	}
	
	public static void initializeLinks() {
		initializeLinks0();
	}
	
	public static void wire0() {
		TERMINALS[0].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]), });
		TERMINALS[0].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]), });
		TERMINALS[0].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]), });
		TERMINALS[0].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]), });
		TERMINALS[0].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]), });
		TERMINALS[0].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]), });
		TERMINALS[0].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[0].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[0].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[0].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[0].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
		TERMINALS[1].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]), });
		TERMINALS[1].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]), });
		TERMINALS[1].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]), });
		TERMINALS[1].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]), });
		TERMINALS[1].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]), });
		TERMINALS[1].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]), });
		TERMINALS[1].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]), });
		TERMINALS[6].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]), });
		TERMINALS[6].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]), });
		TERMINALS[6].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]), });
		TERMINALS[6].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]), });
		TERMINALS[6].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]), });
		TERMINALS[6].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[6].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
		TERMINALS[2].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]), });
		TERMINALS[2].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]), });
		TERMINALS[2].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]), });
		TERMINALS[2].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]), });
		TERMINALS[2].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]), });
		TERMINALS[2].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]), });
		TERMINALS[2].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[7].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]), });
		TERMINALS[7].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]), });
		TERMINALS[7].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]), });
		TERMINALS[7].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]), });
		TERMINALS[7].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]), });
		TERMINALS[7].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]), });
		TERMINALS[7].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[7].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[7].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[7].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[7].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
		TERMINALS[3].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]), });
		TERMINALS[3].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]), });
		TERMINALS[3].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]), });
		TERMINALS[3].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]), });
		TERMINALS[3].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]), });
		TERMINALS[3].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]), });
		TERMINALS[3].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[8].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]), });
		TERMINALS[8].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]), });
		TERMINALS[8].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]), });
		TERMINALS[8].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]), });
		TERMINALS[8].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]), });
		TERMINALS[8].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]), });
		TERMINALS[8].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[8].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[8].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[8].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[8].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
		TERMINALS[4].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]), });
		TERMINALS[4].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]), });
		TERMINALS[4].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]), });
		TERMINALS[4].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]), });
		TERMINALS[4].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]), });
		TERMINALS[4].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]), });
		TERMINALS[4].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]), });
		TERMINALS[9].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]), });
		TERMINALS[9].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]), });
		TERMINALS[9].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]), });
		TERMINALS[9].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]), });
		TERMINALS[9].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]), });
		TERMINALS[9].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[9].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
		TERMINALS[5].addFollower(TERMINALS[11], EMPTY_LINK_ARRAY);
		TERMINALS[11].addFollower(TERMINALS[12], EMPTY_LINK_ARRAY);
		TERMINALS[12].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[1]), });
		TERMINALS[12].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[1]), });
		TERMINALS[12].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[1]), });
		TERMINALS[12].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[1]), });
		TERMINALS[12].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[1]), });
		TERMINALS[12].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[1]), });
		TERMINALS[12].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[0], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getEmptyStyle(), FEATURES[0]), });
		TERMINALS[10].addFollower(TERMINALS[1], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getBold(), FEATURES[0]), });
		TERMINALS[10].addFollower(TERMINALS[2], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getItalic(), FEATURES[0]), });
		TERMINALS[10].addFollower(TERMINALS[3], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getStrikethrough(), FEATURES[0]), });
		TERMINALS[10].addFollower(TERMINALS[4], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getUnderline(), FEATURES[0]), });
		TERMINALS[10].addFollower(TERMINALS[5], new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature[] {new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeContainedFeature(de.devboost.language.bbcode.BbcodePackage.eINSTANCE.getColor(), FEATURES[0]), });
		TERMINALS[10].addFollower(TERMINALS[6], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[7], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[8], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[9], EMPTY_LINK_ARRAY);
		TERMINALS[10].addFollower(TERMINALS[10], EMPTY_LINK_ARRAY);
	}
	
	public static void wire() {
		wire0();
	}
	
	static {
		// initialize the arrays
		initializeTerminals();
		initializeFeatures();
		initializeLinks();
		// wire the terminals
		wire();
	}
}
