/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.mopp;

import org.antlr.runtime3_4_0.ANTLRStringStream;
import org.antlr.runtime3_4_0.Lexer;
import org.antlr.runtime3_4_0.Token;

public class BbcodeAntlrScanner implements de.devboost.language.bbcode.resource.bbcode.IBbcodeTextScanner {
	
	private Lexer antlrLexer;
	
	public BbcodeAntlrScanner(Lexer antlrLexer) {
		this.antlrLexer = antlrLexer;
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTextToken getNextToken() {
		if (antlrLexer.getCharStream() == null) {
			return null;
		}
		final Token current = antlrLexer.nextToken();
		if (current == null || current.getType() < 0) {
			return null;
		}
		de.devboost.language.bbcode.resource.bbcode.IBbcodeTextToken result = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeANTLRTextToken(current);
		return result;
	}
	
	public void setText(String text) {
		antlrLexer.setCharStream(new ANTLRStringStream(text));
	}
	
}
