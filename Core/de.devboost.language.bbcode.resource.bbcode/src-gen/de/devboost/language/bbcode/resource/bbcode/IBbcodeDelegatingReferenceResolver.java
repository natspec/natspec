/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode;

import org.eclipse.emf.ecore.EObject;

/**
 * <p>
 * A delegating reference resolver is an extension of a normal reference resolver
 * that can be configured with another resolver that it may delegate method calls
 * to. This interface can be implemented by additional resolvers to customize
 * resolving using the load option ADDITIONAL_REFERENCE_RESOLVERS.
 * </p>
 * 
 * @see de.devboost.language.bbcode.resource.bbcode.IBbcodeOptions
 */
public interface IBbcodeDelegatingReferenceResolver<ContainerType extends EObject, ReferenceType extends EObject> extends de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<ContainerType, ReferenceType> {
	
	/**
	 * Sets the delegate for this resolver.
	 */
	public void setDelegate(de.devboost.language.bbcode.resource.bbcode.IBbcodeReferenceResolver<ContainerType, ReferenceType> delegate);
	
}
