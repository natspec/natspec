/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.grammar;


public class BbcodeLineBreak extends de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeFormattingElement {
	
	private final int tabs;
	
	public BbcodeLineBreak(de.devboost.language.bbcode.resource.bbcode.grammar.BbcodeCardinality cardinality, int tabs) {
		super(cardinality);
		this.tabs = tabs;
	}
	
	public int getTabs() {
		return tabs;
	}
	
	public String toString() {
		return "!" + getTabs();
	}
	
}
