/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.natspec.resource.natspec.analysis;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import de.devboost.natspec.resource.natspec.INatspecTokenResolveResult;
import de.devboost.natspec.resource.natspec.INatspecTokenResolver;

public class NatspecTEXTTokenResolver implements INatspecTokenResolver {
	
	private NatspecDefaultTokenResolver defaultTokenResolver = new NatspecDefaultTokenResolver(true);
	
	@Override
	public String deResolve(Object value, EStructuralFeature feature, EObject container) {
		// By default token de-resolving is delegated to the DefaultTokenResolver.
		String result = defaultTokenResolver.deResolve(value, feature, container, null, null, null);
		return result;
	}
	
	@Override
	public void resolve(String lexem, EStructuralFeature feature, INatspecTokenResolveResult result) {
		// By default token resolving is delegated to the DefaultTokenResolver.
		defaultTokenResolver.resolve(lexem, feature, result, null, null, null);
	}
	
	@Override
	public void setOptions(Map<?,?> options) {
		defaultTokenResolver.setOptions(options);
	}
}
