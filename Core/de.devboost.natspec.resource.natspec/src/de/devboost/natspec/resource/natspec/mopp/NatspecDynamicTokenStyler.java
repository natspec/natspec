package de.devboost.natspec.resource.natspec.mopp;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.INatspecTextToken;
import de.devboost.natspec.resource.natspec.INatspecTokenStyle;

public class NatspecDynamicTokenStyler {
	
	/**
	 * This method is called to dynamically style tokens.
	 * 
	 * @param resource the TextResource that contains the token
	 * @param token the token to obtain a style for
	 * @param staticStyle the token style as set in the editor preferences (is
	 * <code>null</code> if syntax highlighting for the token is disabled)
	 */
	public INatspecTokenStyle getDynamicTokenStyle(INatspecTextResource resource, INatspecTextToken token, INatspecTokenStyle staticStyle) {
		String text = token.getText();
		URI uri = resource.getURI();
		
		List<ISyntaxPatternProvider> providers = SyntaxPatternRegistry.REGISTRY.getRegistryEntries();
		for (ISyntaxPatternProvider provider : providers) {
			Collection<ISyntaxPattern<? extends Object>> patterns = provider.getPatterns(uri);
			for (ISyntaxPattern<? extends Object> pattern : patterns) {
				Collection<ISyntaxPatternPart> parts = pattern.getParts();
				for (ISyntaxPatternPart part : parts) {
					if (part instanceof StaticWord) {
						StaticWord staticWord = (StaticWord) part;
						if (text.equals(staticWord.getText())) {
							// found keyword
							int[] color = new int[] {0x80, 0x00, 0x55};
							NatspecTokenStyle newStyle = new NatspecTokenStyle(color, null, true, false, false, false);
							return newStyle;
						}
					}
				}
			}
		}
		return staticStyle;
	}

	public void setOffset(int offset) {
		// not required
	}
}
