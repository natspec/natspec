package de.devboost.natspec.resource.natspec.mopp;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.resource.natspec.INatspecBuilder;
import de.devboost.natspec.resource.natspec.NatspecBuilderRegistry;

public class NatspecBuilder implements INatspecBuilder {
	
	@Override
	public boolean isBuildingNeeded(URI uri) {
		// Building is required if one of the registered builders wants to build
		// the resource at the given URI.
		NatspecBuilderRegistry registry = NatspecBuilderRegistry.REGISTRY;
		Set<INatspecBuilder> builders = registry.getBuilders();
		for (INatspecBuilder builder : builders) {
			if (builder.isBuildingNeeded(uri)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public IStatus build(NatspecResource resource, IProgressMonitor monitor) {
		// Call all registered builders (than want to build the resource).
		NatspecBuilderRegistry registry = NatspecBuilderRegistry.REGISTRY;
		Set<INatspecBuilder> builders = registry.getBuilders();
		for (INatspecBuilder builder : builders) {
			if (builder.isBuildingNeeded(resource.getURI())) {
				builder.build(resource, monitor);
			}
		}
		return Status.OK_STATUS;
	}
	
	/**
	 * Handles the deletion of the given resource.
	 */
	@Override
	public IStatus handleDeletion(URI uri, IProgressMonitor monitor) {
		// by default nothing is done when a resource is deleted
		return Status.OK_STATUS;
	}
}
