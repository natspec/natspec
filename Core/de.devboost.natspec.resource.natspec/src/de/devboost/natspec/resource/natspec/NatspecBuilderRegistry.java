package de.devboost.natspec.resource.natspec;

import java.util.LinkedHashSet;
import java.util.Set;

// This is a temporary solution to trigger arbitrary builders on NatSpec files.
public class NatspecBuilderRegistry {
	
	public final static NatspecBuilderRegistry REGISTRY = new NatspecBuilderRegistry();

	private Set<INatspecBuilder> builders = new LinkedHashSet<INatspecBuilder>();
	
	public void registerBuilder(INatspecBuilder newBuilder) {
		builders.add(newBuilder);
	}

	public Set<INatspecBuilder> getBuilders() {
		return builders;
	}
}
