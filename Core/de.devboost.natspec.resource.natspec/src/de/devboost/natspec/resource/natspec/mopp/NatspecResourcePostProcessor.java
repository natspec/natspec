package de.devboost.natspec.resource.natspec.mopp;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.devboost.natspec.Document;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.Word;
import de.devboost.natspec.parsing.ISentenceParser;
import de.devboost.natspec.parsing.SentenceParserFactory;
import de.devboost.natspec.resource.natspec.INatspecResourcePostProcessor;

public class NatspecResourcePostProcessor implements INatspecResourcePostProcessor {
	
	private boolean terminated = false;
	private ISentenceParser parser = new SentenceParserFactory().createSentenceParser();
	
	@Override
	public void process(NatspecResource resource) {
		List<EObject> contents = resource.getContents();
		for (EObject root : contents) {
			if (root instanceof Document) {
				Document document = (Document) root;
				process(document);
			}
		}
	}

	private void process(Document document) {
		Iterator<Sentence> iterator = document.getContents().iterator();
		while (iterator.hasNext()) {
			if (terminated) {
				return;
			}
			
			Sentence sentence = iterator.next();
			if (SentenceUtil.INSTANCE.isEmpty(sentence)) {
				iterator.remove();
				continue;
			}
			
			List<Word> words = parser.split(sentence);
			sentence.getWords().addAll(words);
		}
	}

	@Override
	public void terminate() {
		terminated = true;
	}
}
