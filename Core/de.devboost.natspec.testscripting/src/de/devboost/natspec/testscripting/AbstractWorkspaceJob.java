package de.devboost.natspec.testscripting;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;

public abstract class AbstractWorkspaceJob extends Job {

	public AbstractWorkspaceJob(String name) {
		super(name);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		setRule(root);
	}

	protected abstract IStatus run(IProgressMonitor monitor);

}