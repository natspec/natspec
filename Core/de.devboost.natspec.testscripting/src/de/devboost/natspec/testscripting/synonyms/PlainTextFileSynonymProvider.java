package de.devboost.natspec.testscripting.synonyms;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.TestConnectorPlugin;

/**
 * The {@link PlainTextFileSynonymProvider} is a default implementation of the
 * {@link ISynonymProvider} interface that retrieves synonyms from a plain text
 * file called <code>synonyms.txt</code> that must be located in the same
 * project as the NatSpec specification which used the synonyms. Each line the
 * text file can contain a comma separated list of synonyms.
 */
public class PlainTextFileSynonymProvider implements ISynonymProvider {
	
	private static final String SYNONYM_DELIMITER = ",";

	public static final String SYNONYMS_TXT = "synonyms.txt";
	
	/**
	 * For each project we keep the synonyms which were read from the synonyms
	 * file in this map. The cache is required to avoid reading the synonym file
	 * every time the synonyms are requested.
	 */
	private Map<String, Set<Set<String>>> projectNameToCachedSynonymsMap = new LinkedHashMap<String, Set<Set<String>>>();
	
	@Override
	public Set<Set<String>> getSynonyms(URI contextURI) {
		String projectName = getNameOfContainingProject(contextURI);
		if (projectName == null) {
			return Collections.emptySet();
		}
		refreshSynonymsFromFile(contextURI);
		return projectNameToCachedSynonymsMap.get(projectName);
	}
	
	private String getNameOfContainingProject(URI contextURI) {
		IProject project = getContainingProject(contextURI);
		if (project == null) {
			return null;
		}
		
		return project.getName();
	}

	private IProject getContainingProject(URI contextURI) {
		if (!contextURI.isPlatform()) {
			return null;
		}
		IFile file = new NatspecEclipseProxy().getFileForURI(contextURI);
		IProject project = file.getProject();
		return project;
	}

	private void refreshSynonymsFromFile(URI contextURI) {
		IProject project = getContainingProject(contextURI);
		if (project == null) {
			return;
		}

		Set<Set<String>> synonymSets = readSynonymsFromFile(project);
		projectNameToCachedSynonymsMap.put(project.getName(), synonymSets);
	}

	private Set<Set<String>> readSynonymsFromFile(IProject project) {
		Set<Set<String>> synonymSets = new LinkedHashSet<Set<String>>();
		IFile synonymsFile = project.getFile(SYNONYMS_TXT);
		if (!synonymsFile.exists()) {
			return synonymSets;
		}

		try {
			InputStream contents = synonymsFile.getContents(true);
			LineNumberReader reader = new LineNumberReader(new InputStreamReader(contents, synonymsFile.getCharset()));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] synonyms = line.split(SYNONYM_DELIMITER);
				// remove whitespace before and after words
				for (int i = 0; i < synonyms.length; i++) {
					synonyms[i] = synonyms[i].trim();
				}
				putSynonyms(synonymSets, synonyms);
			}
		} catch (CoreException e) {
			TestConnectorPlugin.logWarning("Exception while reading synonyms from plain text file.", e);
		} catch (IOException e) {
			TestConnectorPlugin.logWarning("Exception while reading synonyms from plain text file.", e);
		}
		return synonymSets;
	}

	private void putSynonyms(Set<Set<String>> synonymSets, String[] synonyms) {
		// search for an existing set
		for (Set<String> synonymSet : synonymSets) {
			for (String synonym : synonyms) {
				if (synonymSet.contains(synonym)) {
					// found a set
					synonymSet.addAll(Arrays.asList(synonyms));
					return;
				}
			}
		}
		
		// no set was found
		synonymSets.add(new LinkedHashSet<String>(Arrays.asList(synonyms)));
	}

	/**
	 * Resets the synonyms for the given project. The synonyms will we read
	 * again when they're requested the next time.
	 */
	public void resetSynonyms(String projectName) {
		projectNameToCachedSynonymsMap.remove(projectName);
	}
}
