package de.devboost.natspec.testscripting;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * The {@link NameHelper} class allows to convert fully qualified Java class names to simple names be removing the
 * package name. It also provides methods to remove type arguments from names of parameterized generic types.
 */
public class NameHelper {

	public final static NameHelper INSTANCE = new NameHelper();

	private NameHelper() {
		super();
	}

	/**
	 * Returns the simple name for the given fully qualified name by removing the package name. Type Arguments are not
	 * removed.
	 */
	public String getSimpleName(String qualifiedName) {
		String simpleName = getNameWithoutTypeArguments(qualifiedName);
		int lastDot = simpleName.lastIndexOf(".");
		if (lastDot > 0) {
			simpleName = simpleName.substring(lastDot + 1);
		}
		return simpleName;
	}

	/**
	 * Returns the simple names for the given fully qualified names by removing the package names. Type Arguments are
	 * not removed.
	 */
	public List<String> getSimpleNames(List<String> qualifiedNames) {
		List<String> simpleTypeNames = new LinkedList<String>();
		for (String qname : qualifiedNames) {
			simpleTypeNames.add(getSimpleName(qname));
		}
		return simpleTypeNames;
	}

	/**
	 * Returns the simple names for the given fully qualified names by removing the package names. Type Arguments are
	 * not removed.
	 */
	public String[] getSimpleNames(String[] qualifiedNames) {
		String[] simpleTypeNames = new String[qualifiedNames.length];
		for (int i = 0; i < qualifiedNames.length; i++) {
			String qualifiedName = qualifiedNames[i];
			simpleTypeNames[i] = getSimpleName(qualifiedName);
		}
		return simpleTypeNames;
	}

	/**
	 * Returns the name of the given fully qualified class without any type arguments (if present). The package name is
	 * not removed. Thus, the class name will still be fully qualified if it was before.
	 */
	public String getNameWithoutTypeArguments(String qualifiedName) {
		String name = qualifiedName;
		int firstGreaterThan = name.indexOf("<");
		if (firstGreaterThan > 0) {
			name = name.substring(0, firstGreaterThan);
		}
		return name;
	}

	public List<String> getTypeArguments(String qualifiedName) {
		List<String> typeArguments = new ArrayList<String>(1);
		int firstGreaterThan = qualifiedName.indexOf("<");
		if (firstGreaterThan > 0) {
			qualifiedName = qualifiedName.substring(firstGreaterThan + 1, qualifiedName.length() - 1);
			// Split at comma, but not if the comma is located within nested
			// type arguments.
			char[] charArray = qualifiedName.toCharArray();
			int level = 0;
			int lastEnd = 0;
			for (int i = 0; i < charArray.length; i++) {
				char next = charArray[i];
				if (next == '<') {
					level++;
					continue;
				}
				if (next == '>') {
					level--;
					continue;
				}
				if (next == ',' && level == 0) {
					String typeArgument = qualifiedName.substring(lastEnd, i);
					typeArguments.add(typeArgument.trim());
					lastEnd = i + 1;
				}
			}
			if (lastEnd < charArray.length) {
				String typeArgument = qualifiedName.substring(lastEnd, charArray.length);
				typeArguments.add(typeArgument.trim());
			}
		}
		return typeArguments;
	}
}
