package de.devboost.natspec.testscripting;

import org.eclipse.core.runtime.QualifiedName;

public interface IProperties {

	public String PATH_PROPERTY = "testoutputpath";

	public QualifiedName QUALIFIED_PATH_PROPERTY = new QualifiedName("", PATH_PROPERTY);

}
