package de.devboost.natspec.testscripting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.osgi.framework.BundleContext;

import de.devboost.natspec.registries.SynonymProviderRegistry;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.builder.SynonymChangeListener;
import de.devboost.natspec.testscripting.synonyms.PlainTextFileSynonymProvider;
import de.devboost.shared.commons.ChangeCollector;

public class TestConnectorPlugin extends Plugin {

	public static final String PLUGIN_ID = "de.devboost.natspec.testscripting";

	private static TestConnectorPlugin instance;

	private final Set<IFile> natspecFiles = new LinkedHashSet<IFile>();

	private final PlainTextFileSynonymProvider synonymProvider = new PlainTextFileSynonymProvider();

	private final List<ChangeCollector<IResource>> scriptValidators = new ArrayList<ChangeCollector<IResource>>();

	private final Set<IRevalidationVisitor> revalidationVisitors = new LinkedHashSet<IRevalidationVisitor>();

	private boolean testScriptSetIsUpToDate;

	private boolean debugFeatureActive;

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		instance = this;

		Job job = new RegisterCommonProvidersJob();
		job.schedule();

		RevalidateScriptsListener listener = new RevalidateScriptsListener();
		SyntaxPatternRegistry.REGISTRY.addListener(listener);
		SynonymProviderRegistry.REGISTRY.addListener(listener);

		try {
			Class.forName("de.devboost.natspec.featuretoggle.debug.DebugFeatureToggle");
			TestConnectorPlugin.logInfo("Debug support is active.", null);
			debugFeatureActive = true;
		} catch (ClassNotFoundException e) {
			// Feature toggle class not found. Deactivate feature.
			TestConnectorPlugin.logInfo("Debug support is inactive.", null);
			return;
		}
	}

	public boolean isDebugFeatureActive() {
		return debugFeatureActive;
	}

	public void registerProviders() {
		// register synonym provider
		SynonymProviderRegistry.REGISTRY.add(synonymProvider);

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		workspace.addResourceChangeListener(new SynonymChangeListener(), IResourceChangeEvent.POST_CHANGE);
	}

	public PlainTextFileSynonymProvider getSynonymProvider() {
		return synonymProvider;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		SynonymProviderRegistry.REGISTRY.remove(synonymProvider);

		// we must do this at the very end, since the tearDown() method requires
		// access to the plug-in instance (to save its state to the workspace
		// metadata folder)
		instance = null;
		super.stop(context);
	}

	public static TestConnectorPlugin getInstance() {
		return instance;
	}

	public void addTestScript(NatspecResource resource) {
		IFile file = new NatspecEclipseProxy().getFileForResource(resource);
		natspecFiles.add(file);
	}

	/**
	 * Returns all NatSpec scripts that are known to be present in the current workspace. To check whether this list is
	 * up-to-date use {@link #isTestScriptSetUpToDate()}.
	 */
	public Set<IFile> getTestScripts() {
		// We return an unmodifiable set to make sure that the set of NatSpec files is modified using addTestScript()
		// only.
		return Collections.unmodifiableSet(natspecFiles);
	}

	// TODO the log methods are copied from JLoopPlugin
	public static IStatus logError(String message) {
		return logError(message, null);
	}

	/**
	 * Helper method for error logging.
	 * 
	 * @param message
	 *            the error message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	public static IStatus logError(String message, Throwable throwable) {
		return log(IStatus.ERROR, message, throwable);
	}

	/**
	 * Helper method for logging informations.
	 * 
	 * @param message
	 *            the information message to log
	 * @param throwable
	 *            the exception that describes the information in detail (can be null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logInfo(String message, Throwable throwable) {
		return log(IStatus.INFO, message, throwable);
	}

	/**
	 * Helper method for logging warnings.
	 * 
	 * @param message
	 *            the warning message to log
	 * @param throwable
	 *            the exception that describes the warning in detail (can be null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logWarning(String message, Throwable throwable) {
		return log(IStatus.WARNING, message, throwable);
	}

	/**
	 * Helper method for logging.
	 * 
	 * @param type
	 *            the type of the message to log
	 * @param message
	 *            the message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	protected static IStatus log(int type, String message, Throwable throwable) {
		IStatus status;
		if (throwable != null) {
			status = new Status(type, TestConnectorPlugin.PLUGIN_ID, 0, message, throwable);
		} else {
			status = new Status(type, TestConnectorPlugin.PLUGIN_ID, message);
		}
		final TestConnectorPlugin pluginInstance = TestConnectorPlugin.getInstance();
		if (pluginInstance == null) {
			System.err.println(message);
			if (throwable != null) {
				throwable.printStackTrace();
			}
		} else {
			pluginInstance.getLog().log(status);
		}
		return status;
	}

	public void triggerScriptValidation(IResource changedResource) {
		Set<IResource> resources = Collections.<IResource> singleton(changedResource);
		synchronized (scriptValidators) {
			for (ChangeCollector<IResource> changeCollector : scriptValidators) {
				changeCollector.addChanges(resources);
			}
		}
	}

	public boolean addScriptValidator(ChangeCollector<IResource> e) {
		synchronized (scriptValidators) {
			return scriptValidators.add(e);
		}
	}

	public boolean isTestScriptSetUpToDate() {
		return testScriptSetIsUpToDate;
	}

	public void setTestScriptSetUpToDate() {
		testScriptSetIsUpToDate = true;
	}

	public Set<IRevalidationVisitor> getRevalidationVisitors() {
		return revalidationVisitors;
	}

	public void addRevalidationVisitor(IRevalidationVisitor revalidationVisitor) {
		revalidationVisitors.add(revalidationVisitor);
	}
}
