package de.devboost.natspec.testscripting.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextSplitter {
	
	public final static TextSplitter INSTANCE = new TextSplitter();
	
	private TextSplitter() {
		super();
	}

	public List<Line> split(String text) {
		// TODO Check whether we have a constant for this somewhere
		Pattern lineBreakPattern = Pattern.compile("\\r?\\n");
		Matcher matcher = lineBreakPattern.matcher(text);
		int lastStart = 0;
		int lineNumber = 0;
		List<Line> lines = new ArrayList<Line>();
		while (matcher.find()) {
			int lineBreakOffset = matcher.start();
			String line = text.substring(lastStart, lineBreakOffset);
			lines.add(new Line(line, new Position(lineNumber, lastStart)));
			lineNumber++;
			lastStart = matcher.end();
		}
		if (lastStart < text.length()) {
			String line = text.substring(lastStart, text.length());
			lines.add(new Line(line, new Position(lineNumber, lastStart)));
		}
		return lines;
	}
}
