package de.devboost.natspec.testscripting.util;

public class TypeStringUtil {

	public static final char TYPE_SEPARATOR = ',';
	
	public final static TypeStringUtil INSTANCE = new TypeStringUtil();
	
	private TypeStringUtil() {
		super();
	}

	public String getTypeString(Iterable<String> types) {
		StringBuilder sb = new StringBuilder();
		for (String type : types) {
			sb.append(type);
			sb.append(TYPE_SEPARATOR);
		}
		return sb.toString();
	}


	public String getTypeString(String[] types) {
		StringBuilder sb = new StringBuilder();
		for (String type : types) {
			sb.append(type);
			sb.append(TYPE_SEPARATOR);
		}
		return sb.toString();
	}
}
