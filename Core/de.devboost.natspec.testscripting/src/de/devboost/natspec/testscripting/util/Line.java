package de.devboost.natspec.testscripting.util;

public class Line {

	private final String text;
	private final Position position;
	
	public Line(String text, Position position) {
		super();
		this.text = text;
		this.position = position;
	}

	public String getText() {
		return text;
	}

	public Position getPosition() {
		return position;
	}
}
