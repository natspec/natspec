package de.devboost.natspec.testscripting.util;

public class Position {

	private final int line;
	private final int offset;
	private final boolean valid;
	
	public Position() {
		this(-1, -1);
	}

	public Position(int line, int offset) {
		super();
		this.line = line;
		this.offset = offset;
		this.valid = line >= 0 && offset >= 0;
	}

	public int getLine() {
		return line;
	}

	public int getOffset() {
		return offset;
	}

	public boolean isValid() {
		return valid;
	}
}
