package de.devboost.natspec.testscripting;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.jobs.Job;

import de.devboost.shared.commons.ChangeCollector;

public class WorkspaceChangeCollector extends ChangeCollector<IResource> {

	@Override
	protected void notifyNewChangedObjectArrived() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceDescription workspaceDescription = workspace.getDescription();
		boolean isAutoBuilding = workspaceDescription.isAutoBuilding();
		if (!isAutoBuilding) {
			return;
		}
		
		Job revalidateScriptsJob = new RevalidateScriptsJob(this);
		revalidateScriptsJob.schedule();
	}
}
