package de.devboost.natspec.testscripting;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public class RegisterCommonProvidersJob extends AbstractWorkspaceJob {

	private static final String REGISTERING_COMMON_NATSPEC_PROVIDERS = "Registering Common NatSpec Providers";

	public RegisterCommonProvidersJob() {
		super(REGISTERING_COMMON_NATSPEC_PROVIDERS);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		TestConnectorPlugin activator = TestConnectorPlugin.getInstance();
		if (activator != null) {
			activator.registerProviders();
		} else {
			String className = getClass().getSimpleName();
			String message = className + ".run() plugin is null";
			TestConnectorPlugin.logWarning(message, null);
		}
		
		return Status.OK_STATUS;
	}
}
