/**
 * This package contains various builders and compilation participants that act
 * if changes are applied to NatSpec-related resources in the current Eclipse
 * workspace. For example, code for NatSpec scripts is regenerated when a script
 * is changed, synonym lists are refreshed when the synonyms file is edited and
 * patterns are registered when new text syntax annotations are found.
 */
package de.devboost.natspec.testscripting.builder;