package de.devboost.natspec.testscripting.builder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.testscripting.AbstractTemplateFileFinder;

public abstract class AbstractTemplateInstantiator implements ITemplateInstantiator {

	protected final AbstractTemplateFileFinder templateFileFinder;

	public AbstractTemplateInstantiator(AbstractTemplateFileFinder templateFileFinder) {
		this.templateFileFinder = templateFileFinder;
	}

	@Override
	public String instantiateTemplate(String templateContent, String packageName, String importStatements,
			String className, String generatedStatements, String generatedParameterStatements, String generatedMethods) {

		String result = templateContent;
		result = addImports(importStatements, result);
		result = replacePackageName(packageName, result);
		result = replaceClassName(className, result);
		result = replaceMethodBody(generatedStatements, result);
		result = replaceParametrizationCode(generatedParameterStatements, result);
		result = addMethods(generatedMethods, result);
		return result;
	}

	protected String replaceParametrizationCode(String generatedParameterStatements, String result) {
		// Default implementation does no replacement. Sub classes may override this behavior.
		return result;
	}

	public abstract String addMethods(String generatedMethods, String result);

	public abstract String replaceMethodBody(String generatedStatements, String result);

	public abstract String replaceClassName(String className, String result);

	public abstract String replacePackageName(String packageName, String result);

	public abstract String addImports(String importStatements, String result);

	protected String replaceAll(String templateContent, String placeholder, String replacement) {
		return templateContent.replaceAll(placeholder, replacement);
	}

	protected String replace(String templateContent, Pattern placeholder, String replacement) {
		int index = getIndexOf(templateContent, placeholder);
		if (index < 0) {
			// The placeholder was not found. Return the template content as it is.
			return templateContent;
		}

		String replacementWithTabs = doIndentation(templateContent, replacement, index);

		// This is required to make sure that back slashes and dollar signs are inserted literally while replacing.
		replacementWithTabs = Matcher.quoteReplacement(replacementWithTabs);
		Matcher matcher = placeholder.matcher(templateContent);
		return matcher.replaceFirst(replacementWithTabs);
	}

	protected abstract String doIndentation(String templateContent, String replacement, int index);

	protected int getIndexOf(String text, Pattern pattern) {
		Matcher matcher = pattern.matcher(text);
		if (matcher.find()) {
			return matcher.start();
		}

		return -1;
	}

	protected String getCharNTimesString(int count, String text) {
		StringBuilder result = new StringBuilder();
		for (int t = 0; t < count; t++) {
			result.append(text);
		}
		
		return result.toString();
	}

	protected int countTabsBeforeIndex(String templateContent, int index) {
		return StringUtils.INSTANCE.countCharactersBeforeIndex(templateContent, '\t', index);
	}

	protected int countSpacesBeforeIndex(String templateContent, int index) {
		return StringUtils.INSTANCE.countCharactersBeforeIndex(templateContent, ' ', index);
	}
}
