package de.devboost.natspec.testscripting.builder;

public interface ITemplateInstantiator {

	public String instantiateTemplate(String templateContent, String packageName, String importStatements,
			String className, String generatedBodyStatements, String generatedParameterStatements,
			String generatedMethods);
}
