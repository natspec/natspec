package de.devboost.natspec.testscripting.builder;

import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;

/**
 * The {@link AbstractResourceChangeListener} collects all changes contained in a {@link IResourceChangeEvent} and calls
 * a template method ({@link #handleResourceChange(IResource)}) for each changed resource. This common behavior is used
 * by subclasses of this class.
 */
public abstract class AbstractResourceChangeListener implements IResourceChangeListener {

	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		IResourceDelta delta = event.getDelta();
		int eventType = event.getType();
		handleChange(delta, eventType);
	}

	private void handleChange(IResourceDelta delta, int eventType) {
		if (delta == null) {
			return;
		}
		IResource resource = delta.getResource();
		if (resource != null) {
			int deltaFlags = delta.getFlags();
			if (deltaFlags != IResourceDelta.MARKERS) {
				handleResourceChange(delta, eventType);
			}

			if ((deltaFlags & IResourceDelta.MARKERS) > 0) {
				IMarkerDelta[] markerDeltas = delta.getMarkerDeltas();
				if (markerDeltas.length > 0) {
					handleMarkerDeltas(markerDeltas);
				}
			}

		}

		IResourceDelta[] affectedChildren = delta.getAffectedChildren();
		for (IResourceDelta childDelta : affectedChildren) {
			handleChange(childDelta, eventType);
		}
	}

	protected abstract void handleMarkerDeltas(IMarkerDelta[] markerDeltas);

	protected abstract void handleResourceChange(IResourceDelta delta, int eventType);
}
