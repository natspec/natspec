package de.devboost.natspec.testscripting.builder;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;

/**
 * The {@link CodeGenerationContext} is used by the NatSpec code generator while iterating over the sentences to
 * generate code for. It collects the code fragments for the sentences in order to build a complete code block at the
 * end of code generation. In addition, a set of imported classes and a counter used to compute unique variable names
 * are held by the this class.
 */
public class CodeGenerationContext implements ICodeGenerationContext {

	private final List<String> codeFragments = new ArrayList<String>();
	private final Set<String> importedClasses = new LinkedHashSet<String>();
	
	private int counter = 0;
	
	private boolean loggingEnabled;

	@Override
	public int getNextCounter() {
		return counter++;
	}

	/**
	 * Adds a code fragment to this context.
	 */
	@Override
	public void addCode(String code) {
		codeFragments.add(code);
	}

	/**
	 * Adds a class that must be imported.
	 */
	@Override
	public void addImport(String classToImport) {
		importedClasses.add(classToImport);
	}

	/**
	 * Returns a composed version of all code fragments that were added previously to this context.
	 */
	public String getCode() {
		StringBuilder result = new StringBuilder();
		for (String code : codeFragments) {
			result.append(code);
		}
		return result.toString();
	}

	/**
	 * Returns all imported classes that were added to this context.
	 */
	@Override
	public Set<String> getImports() {
		return importedClasses;
	}

	@Override
	public boolean isLoggingEnabled() {
		return loggingEnabled;
	}

	@Override
	public void setLoggingEnabled(boolean printLoggingStatements) {
		this.loggingEnabled = printLoggingStatements;
	}
}
