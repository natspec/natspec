package de.devboost.natspec.testscripting.builder;

import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;

import de.devboost.natspec.registries.SynonymProviderRegistry;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.synonyms.PlainTextFileSynonymProvider;

/**
 * The {@link SynonymChangeListener} listens to changes that are applied to the synonyms file. If such a change is
 * applied, all scripts are revalidated and their code is regenerated.
 */
public class SynonymChangeListener extends AbstractResourceChangeListener {

	@Override
	public void handleResourceChange(IResourceDelta delta, int eventType) {
		IResource resource = delta.getResource();
		if (resource == null) {
			return;
		}
		
		String name = resource.getName();
		if (PlainTextFileSynonymProvider.SYNONYMS_TXT.equals(name)) {
			TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
			if (plugin != null) {
				PlainTextFileSynonymProvider synonymProvider = plugin.getSynonymProvider();
				synonymProvider.resetSynonyms(resource.getProject().getName());
			}
			
			SynonymProviderRegistry.REGISTRY.notifyListeners();
		}
	}

	@Override
	protected void handleMarkerDeltas(IMarkerDelta[] markerDeltas) {
		// Do nothing.
	}
}
