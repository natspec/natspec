package de.devboost.natspec.testscripting;

public class Field {

	/**
	 * The name of the field.
	 */
	private final String name;
	
	/**
	 * The fully qualified type of the field.
	 */
	private final String type;
	
	public Field(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return "Field [name=" + name + ", type=" + type + "]";
	}
}
