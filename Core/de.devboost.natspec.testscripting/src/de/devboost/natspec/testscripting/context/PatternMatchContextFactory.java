package de.devboost.natspec.testscripting.context;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.matching.IPatternMatchContext;

public class PatternMatchContextFactory {

	public final static PatternMatchContextFactory INSTANCE = new PatternMatchContextFactory();

	private PatternMatchContextFactory() {
		super();
	}

	public IPatternMatchContext createPatternMatchContext(URI contextURI) {
		return new PatternMatchContext(contextURI);
	}

	public IPatternMatchContext createPatternMatchContext(URI contextURI, ISynonymProvider localSynonymProvider) {
		return new PatternMatchContext(contextURI, localSynonymProvider);
	}

	public IPatternMatchContext createPatternMatchContext(URI contextURI, ISynonymProvider localSynonymProvider,
			boolean matchFuzzy) {
		return new PatternMatchContext(contextURI, localSynonymProvider, matchFuzzy);
	}
}
