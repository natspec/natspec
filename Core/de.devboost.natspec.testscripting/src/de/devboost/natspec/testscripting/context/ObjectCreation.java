package de.devboost.natspec.testscripting.context;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.patterns.IClass;

/**
 * An {@link ObjectCreation} represents the creation of an object during the interpretation of a NatSpec document.
 * Instances of this class are usually created by sentences that refer to methods with a return type that is not void.
 */
public class ObjectCreation {

	private final static StringUtils STRING_UTILS = StringUtils.INSTANCE;

	/** The context in which this object was created. */
	private final IPatternMatchContext context;

	/** The type of this object */
	private final IClass type;

	/** The identifiers that can be used to refer to this object */
	private final List<String> identifiers;

	/**
	 * The simple name of this object's type (derived from {@link #type} during object initialization, cached to avoid
	 * duplicate computation).
	 */
	private final String simpleTypeName;

	/**
	 * The qualified name of this object's type (derived from {@link #type} during object initialization, cached to
	 * avoid duplicate computation).
	 */
	private final String qualifiedTypeName;

	/**
	 * The name of the variable this object will be assigned to in the code generated from the NatSpec file
	 */
	private String variableName;

	/**
	 * Creates a new {@link ObjectCreation} instance.
	 * 
	 * @param context
	 *            the context in which the object is created
	 * @param simpleTypeName
	 *            the simple name of the object's type
	 * @param qualifiedTypeName
	 *            the fully qualified name of the object's type
	 * @param identifiers
	 *            the identifiers that can be used to refer to the object
	 * 
	 * @deprecated Use {@link #ObjectCreation(IPatternMatchContext, JavaType, List)} instead.
	 */
	public ObjectCreation(IPatternMatchContext context, String simpleTypeName, String qualifiedTypeName,
			List<String> identifiers) {

		this(context, new CommonType(qualifiedTypeName), identifiers);
	}

	/**
	 * Creates a new {@link ObjectCreation} instance.
	 * 
	 * @param context
	 *            the context in which the object is created
	 * @param type
	 *            the object's type
	 * @param identifiers
	 *            the identifiers that can be used to refer to the object
	 */
	public ObjectCreation(IPatternMatchContext context, IClass type, List<String> identifiers) {
		this.context = context;
		this.type = type;
		this.qualifiedTypeName = type.getQualifiedName();
		this.simpleTypeName = NameHelper.INSTANCE.getSimpleName(qualifiedTypeName);
		this.identifiers = Collections.unmodifiableList(identifiers);

		initializeVariableName();
	}

	/**
	 * Returns the type of this object.
	 */
	public IClass getObjectType() {
		return type;
	}

	/**
	 * Use {@link #getObjectType()} instead.
	 * 
	 * @return the qualified name of the object's type
	 */
	@Deprecated
	public String getType() {
		return qualifiedTypeName;
	}

	/**
	 * Use {@link #getObjectType()} instead.
	 * 
	 * @return the simple name of the object's type
	 */
	public String getSimpleTypeName() {
		return simpleTypeName;
	}

	/**
	 * Returns the identifiers that can be used to reference this object.
	 */
	public List<String> getIdentifiers() {
		return identifiers;
	}

	/**
	 * Returns the name of the variable this object can be assigned to in the code generated from the NatSpec file.
	 */
	public String getVariableName() {
		return variableName;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " (" + simpleTypeName + ") [" + identifiers + "]";
	}

	/**
	 * Initializes {@link #variableName} with a fresh name that hasn't been used in the same context before. The name
	 * will contain the simple type name of this object.
	 */
	private void initializeVariableName() {
		String lowerSimpleName = STRING_UTILS.toFirstLower(simpleTypeName);

		String lowerSimpleNameWithoutTypeArguments;
		int indexOf = lowerSimpleName.indexOf("<");
		if (indexOf >= 0) {
			lowerSimpleNameWithoutTypeArguments = lowerSimpleName.substring(0, indexOf);
		} else {
			lowerSimpleNameWithoutTypeArguments = lowerSimpleName;
		}

		// Replace array brackets with a valid Java identifier character.
		lowerSimpleNameWithoutTypeArguments = replaceArrayBrackets(lowerSimpleNameWithoutTypeArguments);

		StringBuilder nameBuilder = new StringBuilder();
		nameBuilder.append(lowerSimpleNameWithoutTypeArguments);
		nameBuilder.append("_");

		int identifierCount = identifiers.size();
		for (int i = 0; i < identifierCount; i++) {
			String identifier = identifiers.get(i);
			for (char character : identifier.toCharArray()) {
				if (!Character.isJavaIdentifierPart(character)) {
					nameBuilder.append("_");
				} else {
					nameBuilder.append(character);
				}
			}

			if (i < identifierCount - 1) {
				nameBuilder.append("_");
			}
		}

		String originalName = nameBuilder.toString();
		String finalName = originalName;
		int counter = 0;
		Set<String> usedVariableNames = context.getUsedVariableNames();
		while (usedVariableNames.contains(finalName)) {
			finalName = originalName + counter;
			counter++;
		}

		this.variableName = finalName;
	}

	private String replaceArrayBrackets(String name) {
		name = name.replace("[", "_");
		name = name.replace("]", "_");
		return name;
	}
}
