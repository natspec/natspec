package de.devboost.natspec.testscripting.context;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.devboost.natspec.testscripting.patterns.AbstractClass;
import de.devboost.natspec.testscripting.patterns.IClass;

// TODO Is this class still used?
public class CommonType extends AbstractClass {

	private final String qualifiedTypeName;
	
	public CommonType(String qualifiedTypeName) {
		this.qualifiedTypeName = qualifiedTypeName;
	}

	@Override
	public String getQualifiedName() {
		return qualifiedTypeName;
	}

	@Override
	public String getQualifiedNameWithTypeArguments() {
		return qualifiedTypeName;
	}

	@Override
	public List<? extends IClass> getSuperTypes() {
		return Collections.singletonList(this);
	}

	@Override
	public List<? extends IClass> getTypeArguments() {
		return Collections.emptyList();
	}

	@Override
	public Set<String> getAllSuperTypes() {
		return Collections.singleton(qualifiedTypeName);
	}
}
