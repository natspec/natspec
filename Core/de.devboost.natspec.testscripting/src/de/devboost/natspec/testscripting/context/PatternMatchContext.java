package de.devboost.natspec.testscripting.context;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.matching.AbstractPatternMatchContext;
import de.devboost.natspec.registries.SynonymProviderRegistry;
import de.devboost.natspec.testscripting.patterns.IClass;

/**
 * The {@link PatternMatchContext} is used to pass state when matching a list of sentences. For example, a first
 * sentence may add something to the context (see {@link #addObjectToContext(Object)}) that is required by a second
 * sentence. The {@link PatternMatchContext} also provides information about the {@link URI} of the document containing
 * the sentences.
 */
public class PatternMatchContext extends AbstractPatternMatchContext {

	/**
	 * This map is used to store the last object that was added to this context (for all types).
	 */
	private final Map<String, ObjectCreation> typeToLastObjectMap = new LinkedHashMap<String, ObjectCreation>();

	private ObjectCreation lastObject;

	/**
	 * Creates a new {@link PatternMatchContext} that uses the global {@link SynonymProviderRegistry}.
	 * 
	 * @param contextURI
	 *            the URI of the NatSpec script that is evaluated
	 */
	public PatternMatchContext(URI contextURI) {
		this(contextURI, null);
	}

	/**
	 * Creates a new {@link PatternMatchContext} that uses the given synonym provider in addition to the global
	 * {@link SynonymProviderRegistry}.
	 * 
	 * @param contextURI
	 *            the URI of the NatSpec script that is evaluated
	 * @param localSynonymProvider
	 *            a context-local provider for synonyms
	 */
	public PatternMatchContext(URI contextURI, ISynonymProvider localSynonymProvider) {
		this(contextURI, localSynonymProvider, false);
	}

	public PatternMatchContext(URI contextURI, ISynonymProvider localSynonymProvider, boolean matchFuzzy) {
		super(contextURI, localSynonymProvider, matchFuzzy);
	}

	public void addObjectToContext(Object object) {
		super.addObjectToContext(object);

		// Collect used variable names
		if (object instanceof ObjectCreation) {
			ObjectCreation objectCreation = (ObjectCreation) object;
			lastObject = objectCreation;
			String variableName = objectCreation.getVariableName();
			getUsedVariableNames().add(variableName);

			IClass objectType = objectCreation.getObjectType();
			// Remember this object for all its compatible types
			Set<String> allSuperTypes = objectType.getAllSuperTypes();
			for (String type : allSuperTypes) {
				typeToLastObjectMap.put(type, objectCreation);
			}
		}
	}

	@Override
	public ObjectCreation getLastObjectByType(String qualifiedTypeName) {
		return typeToLastObjectMap.get(qualifiedTypeName);
	}

	@Override
	public boolean isLast(Object object) {
		return lastObject == object;
	}
}
