package de.devboost.natspec.testscripting.debug;

import de.devboost.natspec.resource.natspec.mopp.NatspecMarkerHelper;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;

public interface IDebugConstants {

	public final static String CORRESSPONDING_RESOURCE = NatspecResource.class.getName();
	
	public final static String CORRESSPONDING_MARKER_ID = NatspecMarkerHelper.class.getName();
}
