/**
 * This package contains classes that are related to debugging of NatSpec 
 * scripts.
 */
package de.devboost.natspec.testscripting.debug;