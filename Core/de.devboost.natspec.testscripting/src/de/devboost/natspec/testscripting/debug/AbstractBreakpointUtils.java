package de.devboost.natspec.testscripting.debug;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointManager;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.debug.core.model.ILineBreakpoint;

import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.debug.NatspecLineBreakpoint;
import de.devboost.natspec.testscripting.AbstractGeneratedFileFinder;
import de.devboost.natspec.testscripting.util.Position;

public abstract class AbstractBreakpointUtils {

	private final AbstractGeneratedFileFinder generatedFileFinder;

	public AbstractBreakpointUtils(AbstractGeneratedFileFinder generatedFileFinder) {
		this.generatedFileFinder = generatedFileFinder;
	}

	/**
	 * Creates two breakpoints, one within the NatSpec file and another one in the file containing the generated code.
	 * 
	 * @param natspecFile
	 *            the NatSpec file
	 * @param generatedFile
	 *            the file containing the generated code
	 * @param positionInSpec
	 *            the position of the sentence to debug in the NatSpec file
	 * @param positionInCode
	 *            the position of the code generated for the sentence to debug
	 */
	public void createBreakpoints(IFile natspecFile, IFile generatedFile, Position positionInSpec,
			Position positionInCode) throws DebugException, CoreException {

		long markerID = addBreakpointToNatspecFile(natspecFile, positionInSpec);
		if (generatedFile.exists()) {
			addBreakpointToGeneratedFile(generatedFile, positionInCode, natspecFile, markerID);
		}
	}

	public long addBreakpointToNatspecFile(IFile resource, Position position) throws CoreException {

		int line = position.getLine();
		int lineNumberStartAt0 = line + 1;
		NatspecLineBreakpoint lineBreakpoint = new NatspecLineBreakpoint(resource, lineNumberStartAt0);
		IBreakpointManager breakpointManager = getBreakPointManager();
		breakpointManager.addBreakpoint(lineBreakpoint);

		return lineBreakpoint.getMarker().getId();
	}

	/**
	 * Template method for creation of a breakpoint in a generated file. This is specific to the language of the
	 * generated code and must thus be implemented in concrete sub classes.
	 * 
	 * @param generatedFile
	 *            the file containing the generated code
	 * @param position
	 *            the position where to add the breakpoint
	 * @param markerID
	 *            the ID of the marker that was created in the NatSpec file
	 * @throws CoreException
	 */
	public abstract void addBreakpointToGeneratedFile(IFile generatedFile, Position position, IFile natspecFile,
			long markerID) throws CoreException;

	/**
	 * Removes two breakpoints (one from the NatSpec file and one from the file containing the generated code).
	 * 
	 * @param specificationFile
	 *            the NatSpec file
	 * @param generatedFile
	 *            the file containing the generated code
	 * @param lineInSpec
	 *            the line where the breakpoint is located in the specification
	 * @param lineInCode
	 *            the line where the breakpoint is located in the generated code
	 * @return <code>true</code> if the breakpoint was removed from the specification, otherwise <code>false</code>
	 */
	public boolean removeExistingBreakpoints(IFile specificationFile, IFile generatedFile, int lineInSpec,
			int lineInCode) throws CoreException {

		IBreakpointManager breakpointManager = getBreakPointManager();
		IBreakpoint[] breakpoints = breakpointManager.getBreakpoints();

		boolean natspecBreakpointRemoved = false;
		boolean generatedBreakpointRemoved = false;

		for (IBreakpoint breakpoint : breakpoints) {

			natspecBreakpointRemoved |= removeBreakpoint(specificationFile, lineInSpec, breakpoint);
			generatedBreakpointRemoved |= removeBreakpoint(generatedFile, lineInCode, breakpoint);

			if (generatedBreakpointRemoved && natspecBreakpointRemoved) {
				// Both break points have been removed, we do not need to examine the remaining break points.
				break;
			}
		}

		// We do not care whether the breakpoint in the generated code was removed or not. The important thing is that
		// the breakpoint in the NatSpec document was removed.
		return natspecBreakpointRemoved;
	}

	/**
	 * Removed the break point that is located at the given line from the file if the break point resource matches the
	 * file.
	 * 
	 * @param file
	 *            the file to remove the breakpoint from
	 * @param line
	 *            the line where the breakpoint is located
	 * @param breakpoint
	 *            the breakpoint to remove
	 * 
	 * @return <code>true</code> if the break point was found and removed, otherwise <code>false</code>
	 */
	private boolean removeBreakpoint(IFile file, int line, IBreakpoint breakpoint) throws CoreException {

		if (!file.exists()) {
			return false;
		}

		IMarker marker = breakpoint.getMarker();
		IResource breakpointResource = marker.getResource();
		if (!file.equals(breakpointResource)) {
			return false;
		}

		if (breakpoint instanceof ILineBreakpoint) {
			ILineBreakpoint lineBreakpoint = (ILineBreakpoint) breakpoint;
			// We must add 1 because line numbers start at zero
			int actualLine = line + 1;
			if (lineBreakpoint.getLineNumber() == actualLine) {
				// Remove break point
				breakpoint.delete();
				return true;
			}
		}
		return false;
	}

	/**
	 * Transfers all break points from the NatSpec resource to the file containing the generated code.
	 */
	public void synchronizeBreakpoints(INatspecTextResource resource, IFile natspecFile, IFile generatedFile)
			throws CoreException {

		IBreakpointManager breakpointManager = getBreakPointManager();
		IBreakpoint[] breakpoints = breakpointManager.getBreakpoints();

		for (IBreakpoint breakpoint : breakpoints) {

			IMarker marker = breakpoint.getMarker();
			IResource breakPointResource = marker.getResource();
			if (!natspecFile.equals(breakPointResource)) {
				continue;
			}

			if (breakpoint instanceof ILineBreakpoint) {
				ILineBreakpoint lineBreakpoint = (ILineBreakpoint) breakpoint;
				int lineInSpec = lineBreakpoint.getLineNumber();
				int offsetInSpec = lineBreakpoint.getCharStart();
				// TODO Explain why we subtract 1 here
				Position positionInSpec = new Position(lineInSpec - 1, offsetInSpec);
				Position positionInCode = generatedFileFinder.findSentenceInCode(resource, generatedFile,
						positionInSpec);
				String markerIDString = marker.getAttribute(IDebugConstants.CORRESSPONDING_MARKER_ID, null);
				if (markerIDString != null) {
					long markerID = Long.parseLong(markerIDString);
					addBreakpointToGeneratedFile(generatedFile, positionInCode, natspecFile, markerID);
				}
			}
		}
	}

	private IBreakpointManager getBreakPointManager() {
		DebugPlugin plugin = DebugPlugin.getDefault();
		IBreakpointManager breakpointManager = plugin.getBreakpointManager();
		return breakpointManager;
	}
}
