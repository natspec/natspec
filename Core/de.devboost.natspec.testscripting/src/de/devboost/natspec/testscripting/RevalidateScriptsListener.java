package de.devboost.natspec.testscripting;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;

import de.devboost.natspec.registries.IRegistryListener;

/**
 * The {@link RevalidateScriptsListener} listens to registry events and trigger the validation of all NatSpec scripts in
 * the workspace if the set of registered patterns or the synonyms have changed.
 */
public class RevalidateScriptsListener implements IRegistryListener {

	@Override
	public void notifyChanged() {
		TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
		if (plugin == null) {
			return;
		}

		// When the patterns have changed, we must re-validate the whole workspace, because all NatSpec scripts are
		// potentially affected.
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot workspaceRoot = workspace.getRoot();
		plugin.triggerScriptValidation(workspaceRoot);
	}
}
