package de.devboost.natspec.testscripting;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceVisitor;

public interface IRevalidationVisitor extends IResourceVisitor {

	@Override
	public boolean visit(IResource resource);
}
