package de.devboost.natspec.testscripting;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;

/**
 * The {@link AbstractTemplateFileFinder} can be used to find the NatSpec template that applies to a given NatSpec file.
 * The name of the template class is configured by sub classes.
 */
public class AbstractTemplateFileFinder {

	private final String templateFileName;

	private final String fileExtension;

	public AbstractTemplateFileFinder(String templateFileName, String fileExtension) {
		this.templateFileName = templateFileName;
		this.fileExtension = fileExtension;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public String getTemplateFileName() {
		return templateFileName;
	}

	/**
	 * Returns the file that contains the NatSpec template class which applies to the given NatSpec file.
	 * 
	 * @param natspecFile
	 *            the file to search the template class for
	 * 
	 * @return the file containing the template class or <code>null</code> if no template can be found
	 */
	public IFile findTemplateFile(IFile natspecFile) {
		IContainer container = natspecFile.getParent();
		while (container != null) {
			IFile templateFile;
			if (container instanceof IFolder) {
				IFolder folder = (IFolder) container;
				templateFile = folder.getFile(templateFileName);
			} else if (container instanceof IProject) {
				IProject project = (IProject) container;
				templateFile = project.getFile(templateFileName);
			} else {
				break;
			}
			
			if (templateFile != null && templateFile.exists()) {
				return templateFile;
			}
			
			container = container.getParent();
		}
		
		return null;
	}
}
