package de.devboost.natspec.testscripting;

import java.util.List;

public interface IPatternRegisterCallback {

	public void register(IPatternRegistration registration);

	public void register(List<IPatternRegistration> registrations);
}
