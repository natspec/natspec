package de.devboost.natspec.testscripting;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.ecore.EObject;

import com.google.common.io.ByteStreams;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.resource.natspec.INatspecLocationMap;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.patterns.ICommentGenerator;
import de.devboost.natspec.testscripting.util.Line;
import de.devboost.natspec.testscripting.util.Position;
import de.devboost.natspec.testscripting.util.TextSplitter;

public abstract class AbstractGeneratedFileFinder {

	private static final NatspecEclipseProxy NATSPEC_ECLIPSE_PROXY = new NatspecEclipseProxy();
	// how much we need to add, to get the linenumber of the method call?
	public static final int LINENUMBER_OFFSET = 1;

	public IFile getGeneratedFileForSpecification(INatspecTextResource resource) {
		IFile file = NATSPEC_ECLIPSE_PROXY.getFileForResource(resource);
		return getGeneratedFileForSpecification(file);
	}

	public IFile getGeneratedFileForSpecification(IFile natspecFile) {

		IProject project = natspecFile.getProject();
		String className = getGeneratedFileNameWithoutExtension(natspecFile);

		// TODO Check whether this code can be found anywhere else
		IPath outputFolderPath = natspecFile.getParent().getProjectRelativePath();
		// We must consider the target path for code generation which can be
		// configured as project property.
		try {
			String targetPath = project.getPersistentProperty(IProperties.QUALIFIED_PATH_PROPERTY);
			if (targetPath != null) {
				if (targetPath.startsWith("/")) {
					outputFolderPath = project.getFile(new Path(targetPath)).getProjectRelativePath();
				} else {
					outputFolderPath = outputFolderPath.append(targetPath);
				}
			}
		} catch (CoreException e) {
			// ignore. output path is null, which is default.
		}
		IPath outputFilePath = outputFolderPath.append(className).addFileExtension(getExtensionForGeneratedFiles());
		IFile generatedFile = project.getFile(outputFilePath);
		return generatedFile;
	}

	protected abstract String getGeneratedFileNameWithoutExtension(IFile natspecFile);

	protected abstract String getExtensionForGeneratedFiles();

	public Position findSentenceInCode(IFile resource, IFile generatedFile, Position positionInSpec) {
		INatspecTextResource natspecResource = getNatspecResourceForFile(resource);
		return findSentenceInCode(natspecResource, generatedFile, positionInSpec);
	}

	public Position findSentenceInCode(INatspecTextResource resource, IFile generatedFile, Position positionInSpec) {

		String generatedContent = getGeneratedCode(generatedFile);
		if (generatedContent == null) {
			return new Position();
		}

		int lineInSpec = positionInSpec.getLine();
		int offsetInSpec = positionInSpec.getOffset();
		Sentence sentence = getBreakableSentence(resource, offsetInSpec);
		if (sentence == null) {
			return new Position();
		}

		int numberOfSameSentencesBefore = countOccurencesBefore(resource, lineInSpec, sentence);

		List<Line> lines = TextSplitter.INSTANCE.split(generatedContent);

		// Iterate over generated lines of code and try to find sentence
		// (ignoring the same sentences before).
		String comment = getCommentGenerator().getComment(sentence.getWords());
		for (int i = 0; i < lines.size(); i++) {
			Line line = lines.get(i);
			String text = line.getText();
			boolean foundComment = text.trim().equals(comment.trim());
			if (foundComment) {
				if (numberOfSameSentencesBefore == 0) {
					// We return the position of the line after the comment,
					// because this is where the breakpoint belongs.
					if (lines.size() > i + 1) {
						return lines.get(i + 1).getPosition();
					} else {
						// There is no next line. Can't set a breakpoint.
						return new Position();
					}
				}

				numberOfSameSentencesBefore--;
			}
		}

		return new Position();
	}

	/**
	 * Returns that sentence at the given offset if a breakpoint can be set on the sentence. One cannot set breakpoints
	 * on empty sentences and comments.
	 * 
	 * @param resource
	 *            the NatSpec resource
	 * @param offsetInSpec
	 *            the character offset of the sentence
	 * 
	 * @return the found sentence (if breakable), otherwise <code>null</code>
	 */
	private Sentence getBreakableSentence(INatspecTextResource resource, int offsetInSpec) {

		Sentence sentence = getSentence(resource, offsetInSpec);
		if (sentence == null) {
			return null;
		}
		if (SentenceUtil.INSTANCE.isEmpty(sentence)) {
			return null;
		}
		if (SentenceUtil.INSTANCE.isComment(sentence)) {
			return null;
		}

		return sentence;
	}

	/**
	 * Template method to allow concrete sub classes to use custom {@link ICommentGenerator}s.
	 */
	protected abstract ICommentGenerator getCommentGenerator();

	private Sentence getSentence(INatspecTextResource resource, int offsetInSpec) {

		INatspecLocationMap locationMap = resource.getLocationMap();
		// TODO +1?
		List<EObject> elementsAtLine = locationMap.getElementsAt(offsetInSpec);

		// Try to find sentence at offset
		if (elementsAtLine.isEmpty()) {
			return null;
		}

		EObject firstElement = elementsAtLine.get(0);
		if (firstElement instanceof Sentence) {
			return (Sentence) firstElement;
		}

		return null;
	}

	private int countOccurencesBefore(INatspecTextResource resource, int sentenceLineInSpec, Sentence sentence) {

		INatspecLocationMap locationMap = resource.getLocationMap();
		int numberOfSameSentencesBefore = 0;
		Iterator<EObject> elements = resource.getAllContents();
		while (elements.hasNext()) {
			EObject element = (EObject) elements.next();
			if (element instanceof Sentence) {
				Sentence currentSentence = (Sentence) element;
				// ignore comments
				if (SentenceUtil.INSTANCE.isComment(currentSentence)) {
					continue;
				}

				int line = locationMap.getLine(currentSentence);
				if (line >= sentenceLineInSpec + 1) {
					break;
				}

				if (currentSentence.getText().equals(sentence.getText())) {
					numberOfSameSentencesBefore++;
				}
			}
		}
		return numberOfSameSentencesBefore;
	}

	private String getGeneratedCode(IFile generatedFile) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		InputStream inputStream = null;
		try {
			inputStream = generatedFile.getContents();
			ByteStreams.copy(inputStream, outputStream);
			byte[] bytes = outputStream.toByteArray();
			String generatedContent = new String(bytes, generatedFile.getCharset());
			return generatedContent;
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Can't read code from generated file.", e);
			return null;
		} catch (IOException e) {
			TestConnectorPlugin.logError("Can't read code from generated file.", e);
			return null;
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				// Ignore this
			}
		}
	}

	protected INatspecTextResource getNatspecResourceForFile(IFile file) {
		return NATSPEC_ECLIPSE_PROXY.getResource(file);
	}
}
