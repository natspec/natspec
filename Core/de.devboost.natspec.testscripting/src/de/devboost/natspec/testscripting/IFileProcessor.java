package de.devboost.natspec.testscripting;

import java.util.Collection;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;

public interface IFileProcessor {

	/**
	 * Process the given list of files and return the ones that contain patterns.
	 * 
	 * @param monitor
	 *            a progress monitor (used to handle cancellation)
	 */
	public Collection<IFile> process(Collection<IFile> files, IProgressMonitor monitor);

}