package de.devboost.natspec.testscripting;

import java.util.List;

import org.eclipse.core.resources.IFile;

import de.devboost.natspec.patterns.ISyntaxPattern;

/**
 * An {@link IPatternRegistration} provides all data to register a list of patterns that are provided by a given type
 * (i.e., a support class).
 */
public interface IPatternRegistration {

	public IFile getFile();
	
	public String getTypename();
	
	public List<ISyntaxPattern<?>> getPatterns();
}
