package de.devboost.natspec.testscripting;

import java.util.List;

import org.eclipse.core.resources.IFile;

import de.devboost.natspec.patterns.ISyntaxPattern;

public class PatternRegistration implements IPatternRegistration {

	private final IFile file;
	private final String typename;
	private final List<ISyntaxPattern<?>> patterns;

	public PatternRegistration(IFile file, String typename, List<ISyntaxPattern<?>> patterns) {
		this.file = file;
		this.typename = typename;
		this.patterns = patterns;
	}

	public IFile getFile() {
		return file;
	}

	public String getTypename() {
		return typename;
	}

	public List<ISyntaxPattern<?>> getPatterns() {
		return patterns;
	}
}
