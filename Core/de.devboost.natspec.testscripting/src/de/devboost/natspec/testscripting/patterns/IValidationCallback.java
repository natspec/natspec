package de.devboost.natspec.testscripting.patterns;

/**
 * This interface must be implemented if a provider of a Java model wants to be informed about validation errors that
 * are detected while processing the Java model (e.g., invalid annotation values).
 */
public interface IValidationCallback {

	public void beginValidation();

	public void addWarning(String message);

	public void addError(String message);
}
