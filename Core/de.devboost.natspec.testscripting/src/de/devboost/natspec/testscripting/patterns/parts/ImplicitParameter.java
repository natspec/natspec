package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.context.ObjectCreation;

/**
 * An {@link ImplicitParameter} is an {@link ISyntaxPatternPart} that matches objects in the context based on their
 * type. An {@link ImplicitParameter} is not reflected in the text of a NatSpec document. Solely the pattern matching
 * context is examined to check whether the {@link ImplicitParameter} can be found or not.
 */
public class ImplicitParameter extends AbstractStyleable implements ISyntaxPatternPart, IComparable {

	private final String qualifiedTypeName;
	private final String simpleTypeName;
	private final boolean considerLastObjectOnly;
	
	/**
	 * A flag that indicates whether this parameter is optional. If an {@link ImplicitParameter} is optional and no
	 * object with the correct type can be found in the pattern matching context, <code>null</code> is passed as
	 * argument to the support method for the parameter.
	 */
	private final boolean optional;

	// FIXME Add documentation
	private final boolean isThis;

	/**
	 * Creates a new {@link ImplicitParameter} that matches the give type. If the context contains an object of the
	 * given type, it is accepted as a match for this parameter. If the context contains multiple objects of the given
	 * type, the one that was added last is matched.
	 * 
	 * @param qualifiedType
	 *            the required type of the object in the context
	 */
	public ImplicitParameter(String qualifiedType) {
		this(qualifiedType, false);
	}

	/**
	 * Creates a new {@link ImplicitParameter} that matches the give type. If <code>considerLastObjectOnly</code> is
	 * <code>true</code> only the last object that was added to the context is considered as potential match for this
	 * parameter.
	 * 
	 * @param qualifiedType
	 *            the required type of the object in the context
	 * @param considerLastObjectOnly
	 *            only consider the last object that was added to the context. If the context contains an object with
	 *            the correct type, but this object is not the last one that was added, this parameter will not match
	 */
	public ImplicitParameter(String qualifiedType, boolean considerLastObjectOnly) {
		this(qualifiedType, considerLastObjectOnly, false);
	}

	/**
	 * Creates a new {@link ImplicitParameter} that matches the give type. If <code>considerLastObjectOnly</code> is
	 * <code>true</code> only the last object that was added to the context is considered as potential match for this
	 * parameter.
	 * 
	 * @param qualifiedType
	 *            the required type of the object in the context
	 * @param considerLastObjectOnly
	 *            only consider the last object that was added to the context. If the context contains an object with
	 *            the correct type, but this object is not the last one that was added, this parameter will not match
	 * @param optional
	 *            if set to <code>true</code>, <code>null</code> is used as argument if no matching object is available
	 *            in the context
	 */
	public ImplicitParameter(String qualifiedType, boolean considerLastObjectOnly, boolean optional) {
		this(qualifiedType, considerLastObjectOnly, optional, false);
	}

	public ImplicitParameter(String qualifiedType, boolean considerLastObjectOnly, boolean optional, boolean isThis) {
		this.qualifiedTypeName = qualifiedType;
		this.simpleTypeName = NameHelper.INSTANCE.getSimpleName(qualifiedType);
		this.considerLastObjectOnly = considerLastObjectOnly;
		this.optional = optional;
		this.isThis = isThis;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {
		if (isThis) {
			return createImplicitParameterMatch(null);
		}
		
		Object lastObject = context.getLastObjectByType(qualifiedTypeName);
		if (lastObject == null) {
			// No matching object found.
			return createImplicitParameterMatch(null);
		}

		if (considerLastObjectOnly) {
			boolean isLast = context.isLast(lastObject);
			if (!isLast) {
				// Found object with correct type, but it is not the one that was added to the context last.
				return createImplicitParameterMatch(null);
			}
		}

		// It is safe to cast object here, because getLastObjectByType() does not return objects which are not an
		// instance of ObjectCreation.
		return createImplicitParameterMatch((ObjectCreation) lastObject);
	}

	private ISyntaxPatternPartMatch createImplicitParameterMatch(ObjectCreation object) {
		if (object == null) {
			boolean isOptional = isOptional();
			boolean isThis = isThis();
			if (isOptional || isThis) {
				return new ImplicitParameterMatch(isOptional, isThis);
			} else {
				return null;
			}
		}
		
		return new ImplicitParameterMatch(object);
	}

	private boolean isThis() {
		return isThis;
	}

	public boolean isOptional() {
		return optional;
	}

	@Override
	public String toString() {
		return toSimpleString();
	}

	@Override
	public String toSimpleString() {
		return "Context <" + simpleTypeName + ">:";
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (considerLastObjectOnly ? 1231 : 1237);
		result = prime * result + (optional ? 1231 : 1237);
		result = prime * result + ((qualifiedTypeName == null) ? 0 : qualifiedTypeName.hashCode());
		return result;
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		ImplicitParameter other = (ImplicitParameter) obj;
		if (considerLastObjectOnly != other.considerLastObjectOnly) {
			return false;
		}
		
		if (optional != other.optional) {
			return false;
		}
		
		if (qualifiedTypeName == null) {
			if (other.qualifiedTypeName != null) {
				return false;
			}
		} else if (!qualifiedTypeName.equals(other.qualifiedTypeName)) {
			return false;
		}
		
		return super.isEqualTo(obj);
	}
}
