package de.devboost.natspec.testscripting.patterns;

public abstract class AbstractCodeFragment implements ICodeFragment {

	/**
	 * This is a default implementation for {@link ICodeFragment#isParameterizationCode()}. It returns <code>false</code>
	 * as parameterization code was introduced later.
	 */
	public boolean isParameterizationCode() {
		return false;
	}
}
