package de.devboost.natspec.testscripting.patterns.parts;

import de.devboost.natspec.patterns.parts.StaticWord;

/**
 * Use {@link StaticWord} instead.
 */
@Deprecated
public class TypeWord extends StaticWord {

	public TypeWord(String text) {
		super(text);
	}
}
