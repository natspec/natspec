package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.patterns.IClass;

/**
 * A {@link ListParameter} can be used to match a list of an arbitrary syntax pattern part.
 */
public class ListParameter extends AbstractListParameter {

	public ListParameter(ISyntaxPatternPart listElement, IClass listElementType) {
		super(listElement, listElementType);
	}

	protected ISyntaxPatternPartMatch createMatch(List<ISyntaxPatternPartMatch> elementMatches,
			List<Word> allMatchedWords) {
		
		return new ListMatch(allMatchedWords, elementMatches, getListElementType());
	}

	@Override
	public String toSimpleString() {
		return "List [" + getListElement() + "]";
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (!(obj instanceof ListParameter)) {
			return false;
		}

		return super.isEqualTo(obj);
	}
}
