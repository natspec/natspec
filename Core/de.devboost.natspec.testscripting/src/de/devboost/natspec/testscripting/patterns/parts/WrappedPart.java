package de.devboost.natspec.testscripting.patterns.parts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;
import de.devboost.natspec.util.ComparisonHelper;

/**
 * A {@link WrappedPart} wraps another {@link ISyntaxPatternPart} by adding a prefix and a suffix.
 */
public class WrappedPart extends AbstractStyleable implements ISyntaxPatternPart, IComparable, ICompletable {

	private static final NatspecFactory NATSPEC_FACTORY = NatspecFactory.eINSTANCE;

	private final ISyntaxPatternPart delegate;
	private final String prefix;
	private final String suffix;

	public WrappedPart(ISyntaxPatternPart delegate, String prefix, String suffix) {
		super();
		this.delegate = delegate;
		this.prefix = prefix;
		this.suffix = suffix;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		if (words.isEmpty()) {
			return null;
		}

		List<Word> newWords = null;
		if (delegate instanceof ListParameter) {
			newWords = handleManyWordsPaternParts(words);
			if (newWords == null) {
				return null;
			}
			ListParameter castedDelegate = (ListParameter) delegate;
			WrappedListParameter wrappedListParameter = new WrappedListParameter(castedDelegate.getListElement(),
					castedDelegate.getListElementType(), prefix, suffix);
			return wrappedListParameter.match(newWords, context);
		}
		
		if (delegate instanceof DateParameter) {
			newWords = handleManyWordsPaternParts(words);
			if (newWords == null) {
				return null;
			}
			
			return delegate.match(newWords, context);
		}

		newWords = handleOneWordPatternParts(words);
		if (newWords == null) {
			return null;
		}

		return delegate.match(newWords, context);
	}

	protected List<Word> handleManyWordsPaternParts(List<Word> words) {
		// We need to get the first for the prefix and the last word for suffix
		// If the list contains one word, first and last word are the same
		Word firstWord = words.get(0);
		String firstWordText = firstWord.getText();
		if (!firstWordText.startsWith(prefix)) {
			return null;
		}
		
		// If the first word has the length 1 and starts and ends with the suffix
		// and there are more than one word, then it should not match (fix for NATSPEC-373)
		if (firstWordText.endsWith(suffix) && firstWordText.length() == 1 && words.size() > 1) {
			return null;
		}

		int suffixLength = suffix.length();
		List<Word> newWords = new ArrayList<Word>();
		if (suffixLength != 0) {

			for (Iterator<Word> iterator = words.iterator(); iterator.hasNext();) {
				Word word = iterator.next();
				String currentWordText = word.getText();
				newWords.add(word);
				if (currentWordText.endsWith(suffix)) {
					break;
				} else if (!iterator.hasNext()) {
					// There should be a word wich ends with suffix
					// so this does not match and we can stop here
					return null;
				}
			}
		} else {
			newWords = new ArrayList<Word>(words);
		}

		Word lastWord = newWords.get(newWords.size() - 1);
		String lastWordText = lastWord.getText();

		// TODO Cache length of prefix and suffix
		int prefixLength = prefix.length();

		newWords.remove(0);
		Word newFirstWord = NATSPEC_FACTORY.createWord();

		if (firstWord.equals(lastWord)) {
			generateNewWordsForOneWord(firstWord, firstWordText, prefixLength, suffixLength, newWords, newFirstWord);
		} else {
			String wrappedfirstWordText = firstWordText.substring(prefixLength, firstWordText.length());
			String wrappedlastWordText = lastWordText.substring(0, lastWordText.length() - suffixLength);
			newWords.remove(newWords.size() - 1);
			Word newLastWord = NATSPEC_FACTORY.createWord();
			newFirstWord.setText(wrappedfirstWordText);
			newLastWord.setText(wrappedlastWordText);
			// Set offset
			newFirstWord.setOffset(firstWord.getOffset() + prefixLength);
			newLastWord.setOffset(lastWord.getOffset());
			newWords.add(0, newFirstWord);
			newWords.add(newWords.size(), newLastWord);
		}
		return newWords;
	}

	protected List<Word> handleOneWordPatternParts(List<Word> words) {
		Word firstWord = words.get(0);
		String firstWordText = firstWord.getText();
		boolean hasPrefix = firstWordText.startsWith(prefix);
		boolean hasSuffix = firstWordText.endsWith(suffix);
		if (!hasPrefix || !hasSuffix) {
			// Word must have both prefix and suffix to match
			return null;
		}

		// TODO Cache length of prefix and suffix
		int prefixLength = prefix.length();
		int suffixLength = suffix.length();
		// Check whether first word contained both the prefix and the suffix. If the prefix and the suffix are the same,
		// startsWith(prefix) and endsWith(suffix) might be true, but the word may consist only of the prefix/suffix.
		if (firstWordText.length() < prefixLength + suffixLength) {
			return null;
		}
		
		List<Word> newWords = new ArrayList<Word>(words);
		newWords.remove(0);
		Word newFirstWord = NATSPEC_FACTORY.createWord();
		generateNewWordsForOneWord(firstWord, firstWordText, prefixLength, suffixLength, newWords, newFirstWord);
		return newWords;
	}

	protected void generateNewWordsForOneWord(Word firstWord, String firstWordText, int prefixLength, int suffixLength,
			List<Word> newWords, Word newFirstWord) {

		String wrappedText = firstWordText.substring(prefixLength, firstWordText.length() - suffixLength);
		newFirstWord.setText(wrappedText);
		// Set offset
		newFirstWord.setOffset(firstWord.getOffset() + prefixLength);
		newWords.add(0, newFirstWord);
	}

	@Override
	public String toSimpleString() {
		return prefix + delegate.toSimpleString() + suffix;
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		WrappedPart other = (WrappedPart) obj;
		if (!ComparisonHelper.INSTANCE.isEqualTo(delegate, other.delegate)) {
			return false;
		}
		if (!ComparisonHelper.INSTANCE.isEqualTo(prefix, other.prefix)) {
			return false;
		}
		if (!ComparisonHelper.INSTANCE.isEqualTo(suffix, other.suffix)) {
			return false;
		}
		return super.isEqualTo(other);
	}

	@Override
	public int computeHashCode() {
		// TODO Delegating to delegate.hashCode() may not be correct.
		final int prime = 31;
		int result = 1;
		result = prime * result + ((delegate == null) ? 0 : delegate.hashCode());
		result = prime * result + ((prefix == null) ? 0 : prefix.hashCode());
		result = prime * result + ((suffix == null) ? 0 : suffix.hashCode());
		return result;
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		if (delegate instanceof ICompletable) {
			ICompletable completableDelegate = (ICompletable) delegate;
			String proposal = prefix + completableDelegate.getCompletionProposal(context) + suffix;
			return proposal;
		}
		
		// TODO: maybe show hint that implementation of ICompletable is necessary
		return prefix + " " + suffix;
	}
}
