package de.devboost.natspec.testscripting.patterns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.IPatternRegistration;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.util.ComparisonHelper;

public abstract class AbstractDynamicSyntaxPatternProvider implements ISyntaxPatternProvider {

	private static final NatspecEclipseProxy NATSPEC_ECLIPSE_PROXY = new NatspecEclipseProxy();

	private final Map<String, List<ISyntaxPattern<?>>> testSupportClassToPatternsMap = new LinkedHashMap<String, List<ISyntaxPattern<?>>>();

	private final Map<String, Set<String>> testSupportFilePathToClassesMap = new LinkedHashMap<String, Set<String>>();

	public Map<String, List<ISyntaxPattern<?>>> getTestSupportClassToPatternsMap() {
		return testSupportClassToPatternsMap;
	}

	/**
	 * Returns all syntax patterns that were extracted from classes contained in the same project as the file referenced
	 * by the given URI. Usually the URI of the NatSpec file at hand is passed to this method.
	 */
	@Override
	public Collection<ISyntaxPattern<? extends Object>> getPatterns(URI uri) {
		if (uri == null) {
			return getAllPatterns();
		}

		if (!uri.isPlatform()) {
			return Collections.emptySet();
		}

		IFile natspecFile = NATSPEC_ECLIPSE_PROXY.getFileForURI(uri);

		List<ISyntaxPattern<?>> patterns = doGetPatterns(natspecFile);
		return patterns;
	}

	/**
	 * Concrete subclasses must implement this method to specify how the patterns for the given NatSpec file are
	 * derived.
	 * 
	 * @param natspecFile
	 *            the NatSpec file to search patterns for
	 * @return a list of syntax patterns that apply to the NatSpec file
	 */
	protected abstract List<ISyntaxPattern<?>> doGetPatterns(IFile natspecFile);

	protected List<ISyntaxPattern<?>> getAllPatterns() {
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		for (String fieldType : testSupportClassToPatternsMap.keySet()) {
			List<ISyntaxPattern<?>> patternsForField = testSupportClassToPatternsMap.get(fieldType);
			if (patternsForField != null) {
				patterns.addAll(patternsForField);
			}
		}

		return patterns;
	}

	/**
	 * Returns that paths of all classes that provide syntax patterns.
	 * 
	 * @return a list of workspace-relative paths.
	 */
	public Set<String> getProvidingFiles() {
		return testSupportFilePathToClassesMap.keySet();
	}

	private void addPatternType(IFile testSupportFile, String testSupportClass) {
		IPath testSupportFilePath = testSupportFile.getFullPath();
		String path = testSupportFilePath.toString();
		Set<String> types = testSupportFilePathToClassesMap.get(path);
		if (types == null) {
			types = new LinkedHashSet<String>();
			testSupportFilePathToClassesMap.put(path, types);
		}
		types.add(testSupportClass);
	}

	public void register(List<IPatternRegistration> registrations) {

		boolean patternsHaveChanged = false;
		for (IPatternRegistration registration : registrations) {
			String typename = registration.getTypename();
			IFile file = registration.getFile();
			addPatternType(file, typename);

			List<ISyntaxPattern<?>> patterns = registration.getPatterns();
			patternsHaveChanged |= registerPatterns(typename, patterns);
		}

		if (patternsHaveChanged) {
			SyntaxPatternRegistry.REGISTRY.notifyListeners();
		}
	}

	protected boolean registerPatterns(String qualifiedTypeName, List<ISyntaxPattern<?>> newPatterns) {

		List<ISyntaxPattern<?>> oldPatterns = testSupportClassToPatternsMap.get(qualifiedTypeName);
		if (oldPatterns == null) {
			// There is no need to register an empty set of patterns if there were no registered pattern before.
			if (newPatterns.isEmpty()) {
				return false;
			}
		} else {
			// Do not set new patterns if they haven't changed.
			if (ComparisonHelper.INSTANCE.isEqualTo(oldPatterns, newPatterns)) {
				TestConnectorPlugin.logInfo(
						"Patterns for type: " + qualifiedTypeName + " remain unchanged. Patterns are: " + newPatterns,
						null);
				return false;
			}
		}

		String message = "Registered pattern(s) for type: " + qualifiedTypeName + " are " + newPatterns;
		TestConnectorPlugin.logInfo(message, null);
		testSupportClassToPatternsMap.put(qualifiedTypeName, newPatterns);
		return true;
	}
}
