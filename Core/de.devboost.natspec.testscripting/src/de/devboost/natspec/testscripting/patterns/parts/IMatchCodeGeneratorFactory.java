package de.devboost.natspec.testscripting.patterns.parts;

import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.parts.DefaultSentencePartMatch;
import de.devboost.natspec.patterns.parts.DoubleMatch;
import de.devboost.natspec.patterns.parts.FloatMatch;
import de.devboost.natspec.patterns.parts.IntegerMatch;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;

/**
 * The {@link IMatchCodeGeneratorFactory} interface is used to decouple the code generation for specific
 * {@link ISyntaxPatternPartMatch}es from class {@link AbstractCodeGenerator}. This way we can reuse the
 * {@link AbstractCodeGenerator} for different languages.
 */
public interface IMatchCodeGeneratorFactory {

	public ICodeFragment createGenerator(ISyntaxPatternPartMatch match);

	public ICodeFragment createGenerator(StringMatch match);

	public ICodeFragment createGenerator(DateMatch match);

	public ICodeFragment createGenerator(ImplicitParameterMatch match);

	public ICodeFragment createGenerator(ListMatch match);

	public ICodeFragment createGenerator(ManyMatch match);

	public ICodeFragment createGenerator(ObjectMatch match);

	public ICodeFragment createGenerator(DoubleMatch match);

	public ICodeFragment createGenerator(FloatMatch match);

	public ICodeFragment createGenerator(IntegerMatch match);

	public ICodeFragment createGenerator(BooleanMatch match);

	public ICodeFragment createGenerator(DefaultSentencePartMatch match);

	public ICodeFragment createGeneratorForUnknown(ISyntaxPatternPartMatch match);
}
