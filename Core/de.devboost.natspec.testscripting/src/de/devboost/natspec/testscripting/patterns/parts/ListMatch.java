package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.patterns.IClass;

/**
 * A {@link ListMatch} represents a match of a {@link ListParameter} to a sequence of words.
 */
public class ListMatch extends AbstractListMatch {

	public ListMatch(List<Word> matchedWords, List<ISyntaxPatternPartMatch> elementMatches, IClass listElementType) {
		super(matchedWords, elementMatches, listElementType);
	}
}
