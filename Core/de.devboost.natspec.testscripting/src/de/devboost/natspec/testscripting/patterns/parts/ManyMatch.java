package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.patterns.IClass;

public class ManyMatch extends AbstractListMatch {

	public ManyMatch(List<Word> matchedWords, List<ISyntaxPatternPartMatch> elementMatches, IClass listElementType) {
		super(matchedWords, elementMatches, listElementType);
	}
}
