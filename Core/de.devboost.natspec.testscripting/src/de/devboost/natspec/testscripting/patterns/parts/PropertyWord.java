package de.devboost.natspec.testscripting.patterns.parts;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.StaticWordMatch;

/**
 * A {@link PropertyWord} is a special word that matches based on camel case syntax. For example, the
 * {@link PropertyWord} 'basicValue' can be matched against the word sequence 'basic value'. Also, the matching rules
 * for {@link StaticWord}s apply. That is, words are matched case-insensitive and with consideration of synonyms.
 */
public class PropertyWord extends StaticWord {

	// Compile regular expression and reuse it
	private final static Pattern PATTERN = Pattern.compile("(?<!(^|[A-Z]))(?=[A-Z])|(?<!^)(?=[A-Z][a-z])");

	private LinkedList<StaticWord> wordsFromCamelcase;
	private StaticWord propertyName;

	public PropertyWord(String text) {
		super(text);
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		if (words.isEmpty()) {
			return null;
		}

		StaticWord propertyName = getPropertyName();
		ISyntaxPatternPartMatch propertyNameMatch = propertyName.match(words, context);
		if (propertyNameMatch != null) {
			return propertyNameMatch;
		}

		List<Word> matchedWords = new LinkedList<Word>();
		List<StaticWord> propertyWords = getWordsFromCamelcase();
		boolean camelcaseMatchError = false;
		int wordIndex = 0;
		for (StaticWord staticWord : propertyWords) {
			List<Word> currentWords = words.subList(wordIndex, words.size());
			ISyntaxPatternPartMatch match = staticWord.match(currentWords, context);
			if (match == null) { // word not matched
				camelcaseMatchError = true;
				break;
			} else {
				matchedWords.addAll(match.getMatchedWords());
			}
			wordIndex++;
		}

		if (camelcaseMatchError) {
			return null;
		} else {
			return new StaticWordMatch(matchedWords);
		}
	}

	private StaticWord getPropertyName() {
		// Reuse 'propertyName' (create it lazy)
		if (propertyName != null) {
			return propertyName;
		}

		propertyName = new StaticWord(getText());
		return propertyName;
	}

	private List<StaticWord> getWordsFromCamelcase() {
		// Reuse list (create it lazy)
		if (wordsFromCamelcase != null) {
			return wordsFromCamelcase;
		}

		wordsFromCamelcase = new LinkedList<StaticWord>();
		String[] individualWords = PATTERN.split(getText());
		for (String word : individualWords) {
			wordsFromCamelcase.add(new StaticWord(word));
		}

		return wordsFromCamelcase;
	}
}
