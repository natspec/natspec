package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.ObjectCreation;

public class ObjectMatch extends AbstractSyntaxPatternPartMatch {

	private final ObjectCreation objectCreation;

	public ObjectMatch(List<Word> words, ObjectCreation objectCreation) {
		super(words);
		this.objectCreation = objectCreation;
	}
	
	public ObjectCreation getObjectCreation() {
		return objectCreation;
	}
}
