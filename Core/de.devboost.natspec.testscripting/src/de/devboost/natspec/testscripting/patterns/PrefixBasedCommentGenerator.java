package de.devboost.natspec.testscripting.patterns;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.WordUtil;

/**
 * A {@link PrefixBasedCommentGenerator} constructs comments by adding a specific prefix before the list of words that
 * form the comment itself. This is a common practice in many programming languages.
 */
public class PrefixBasedCommentGenerator implements ICommentGenerator {

	private final String prefix;
	
	public PrefixBasedCommentGenerator(String prefix) {
		this.prefix = prefix;
	}

	@Override
	public String getComment(List<Word> words) {
		String sentence = WordUtil.INSTANCE.toString(words);
		return getComment(sentence);
	}

	public String getComment(String text) {
		StringBuilder comment = new StringBuilder();
		comment.append(this.prefix);
		comment.append(" ");
		comment.append(text);
		comment.append("\n");
		return comment.toString();
	}
}
