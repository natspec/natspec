package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.IParameter;

/**
 * An {@link IParameterFactory} can be used to derive syntax pattern parts for method parameters considering their
 * types.
 */
public interface IParameterFactory {

	/**
	 * Creates a syntax pattern part that matches instances of the given explicit parameter.
	 * 
	 * @param parameter
	 *            the parameter that is associated with the part
	 * 
	 * @return the created pattern part
	 * 
	 * @throws UnsupportedTypeException
	 *             if the given parameter is not supported
	 */
	public ISyntaxPatternPart createParameter(IParameter parameter) throws UnsupportedTypeException;

	/**
	 * Creates an explicit syntax pattern part that matches instances of the given type.
	 * 
	 * @param type
	 *            the type that is associated with the part
	 * 
	 * @return the created pattern part
	 * 
	 * @throws UnsupportedTypeException
	 *             if the given type is not supported
	 */
	public ISyntaxPatternPart createParameter(IClass type) throws UnsupportedTypeException;

	/**
	 * Creates a syntax pattern part that matches instances of the given parameter.
	 * 
	 * @param parameter
	 *            the parameter that is associated with the part
	 * 
	 * @param implicit
	 *            <code>true</code> if the parameter is implicit (i.e., there is no place holder for the parameter in
	 *            the text syntax specification)
	 * 
	 * @return the created pattern part
	 * 
	 * @throws UnsupportedTypeException
	 *             if the given parameter is not supported
	 */
	public ISyntaxPatternPart createParameter(IParameter parameter, boolean implicit) throws UnsupportedTypeException;

	/**
	 * Creates a syntax pattern part that matches instances of the given type.
	 * 
	 * @param type
	 *            the type that is associated with the part
	 * 
	 * @param implicit
	 *            <code>true</code> if the parameter is implicit (i.e., there is no place holder for the parameter in
	 *            the text syntax specification)
	 * 
	 * @return the created pattern part
	 * 
	 * @throws UnsupportedTypeException
	 *             if the given type is not supported
	 */
	public ISyntaxPatternPart createParameter(IClass type, boolean implicit) throws UnsupportedTypeException;

	/**
	 * Returns <code>true</code> if the given syntax specification represents the default syntax pattern (i.e., the
	 * pattern that matches all sentences).
	 * 
	 * @param parts
	 *            the parts of the syntax specification
	 * @param parameters
	 *            the parameters of the method associated with the pattern
	 * @return <code>true</code> if the specification matches all sentences, otherwise <code>false</code>
	 */
	public boolean isDefaultSyntaxPattern(String[] parts, List<? extends IParameter> parameters);

	/**
	 * Returns <code>true</code> if the given parameters represent the default syntax pattern (i.e., the pattern that
	 * matches all sentences).
	 * 
	 * @param parameters
	 *            the parameters of the method associated with the pattern
	 * @return <code>true</code> if the specification matches all sentences, otherwise <code>false</code>
	 */
	public boolean isDefaultSyntaxPatternWithMany(List<? extends IParameter> parameters);
}
