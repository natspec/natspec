package de.devboost.natspec.testscripting.patterns;

import java.util.List;
import java.util.Set;

/**
 * This interface is an abstraction for classes as they're used in most
 * object-oriented languages.
 */
public interface IClass {

	/**
	 * Returns the fully qualified name of this class (without type arguments).
	 */
	public String getQualifiedName();

	/**
	 * Returns the fully qualified name of this class including type arguments.
	 */
	public String getQualifiedNameWithTypeArguments();

	/**
	 * Returns all direct super types and implemented interfaces of this class.
	 */
	public List<? extends IClass> getSuperTypes();

	/**
	 * Returns all type arguments.
	 */
	public List<? extends IClass> getTypeArguments();

	/**
	 * Returns the fully qualified names of all super types of this type
	 * (direct and indirect) including the name of the type itself.
	 */
	public Set<String> getAllSuperTypes();

	/**
	 * Returns a comma-separated list of all qualified super types of this type.
	 */
	public String getAllSuperTypeString();
}
