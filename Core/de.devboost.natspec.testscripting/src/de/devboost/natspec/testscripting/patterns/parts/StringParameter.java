package de.devboost.natspec.testscripting.patterns.parts;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

/**
 * A {@link StringParameter} is an {@link ISyntaxPatternPart} that matches to an arbitrary, single word.
 */
public class StringParameter extends AbstractStyleable implements ISyntaxPatternPart, ICompletable, IComparable {

	private static final int HASH_CODE = StringParameter.class.getName().hashCode();

	private final String completionProposal;

	public StringParameter() {
		this(null);
	}

	public StringParameter(String completionProposal) {
		this.completionProposal = completionProposal;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		if (words.isEmpty()) {
			return null;
		}

		Word word = words.get(0);
		String text = word.getText();
		return new StringMatch(Collections.singletonList(word), text);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	@Override
	public String toSimpleString() {
		return "<String>";
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		if (completionProposal != null) {
			return completionProposal;
		} else {
			return "someText";
		}
	}

	@Override
	public boolean isEqualTo(Object object) {
		// Since this class does not have fields, all objects are equal.
		if(object == null || !(object instanceof StringParameter)){
			return false;
		}
		return super.isEqualTo(object);
	}

	@Override
	public int computeHashCode() {
		// Since this class does not have fields, all objects can have the same
		// hash code.
		return HASH_CODE;
	}
}
