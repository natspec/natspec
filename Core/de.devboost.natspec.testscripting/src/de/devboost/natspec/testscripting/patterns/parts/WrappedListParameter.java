package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.patterns.IClass;

/**
 * A {@link WrappedListParameter} can be used to match a list of an arbitrary syntax pattern part.
 */
public class WrappedListParameter extends AbstractListParameter {

	private final String prefix;
	private final String suffix;

	public WrappedListParameter(ISyntaxPatternPart listElement, IClass listElementType, String prefix, String suffix) {
		super(listElement, listElementType);
		this.prefix = prefix;
		this.suffix = suffix;
	}

	protected ISyntaxPatternPartMatch createMatch(List<ISyntaxPatternPartMatch> elementMatches,
			List<Word> allMatchedWords) {
		return new WrappedListMatch(allMatchedWords, elementMatches, getListElementType(), prefix, suffix);
	}

	@Override
	public String toSimpleString() {
		return "List [" + getListElement() + "]";
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (!(obj instanceof WrappedListParameter)) {
			return false;
		}

		return super.isEqualTo(obj);
	}
}
