package de.devboost.natspec.testscripting.patterns.parts;

import java.util.Collections;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.ObjectCreation;

public class ImplicitParameterMatch extends AbstractSyntaxPatternPartMatch {

	private final ObjectCreation objectCreation;
	private final boolean isOptional;
	private final boolean isThis;

	public ImplicitParameterMatch(ObjectCreation objectCreation) {
		super(Collections.<Word>emptyList());
		this.objectCreation = objectCreation;
		this.isOptional = false;
		this.isThis = false;
	}

	public ImplicitParameterMatch(boolean isOptional, boolean isThis) {
		super(Collections.<Word>emptyList());
		this.objectCreation = null;
		this.isOptional = isOptional;
		this.isThis = isThis;
	}

	@Override
	public boolean hasMatched() {
		return true;
	}

	/**
	 * Returns the {@link ObjectCreation} to which the {@link ImplicitParameter} was matched. If the
	 * {@link ImplicitParameter} was optional and no {@link ObjectCreation} was found, <code>null</code> is returned.
	 */
	public ObjectCreation getObjectCreation() {
		return objectCreation;
	}

	public boolean isOptional() {
		return isOptional;
	}

	public boolean isThis() {
		return isThis;
	}
}
