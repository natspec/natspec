package de.devboost.natspec.testscripting.patterns;

import java.util.List;

import de.devboost.natspec.Word;

public interface ICommentGenerator {

	public String getComment(List<Word> words);
}
