package de.devboost.natspec.testscripting.patterns;

import java.util.Map;

import de.devboost.natspec.patterns.ISyntaxPatternPart;

public interface IParameterIndexProvider {

	Map<ISyntaxPatternPart, Integer> getPartToParameterIndexMap();

}
