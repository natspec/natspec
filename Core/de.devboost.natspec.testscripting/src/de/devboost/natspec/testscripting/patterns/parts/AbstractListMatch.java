package de.devboost.natspec.testscripting.patterns.parts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.patterns.IClass;

public class AbstractListMatch extends AbstractSyntaxPatternPartMatch {

	private final List<ISyntaxPatternPartMatch> elementMatches;
	private final IClass listElementType;

	public AbstractListMatch(List<Word> matchedWords, List<ISyntaxPatternPartMatch> elementMatches, IClass listElementType) {
		super(matchedWords);
		this.elementMatches = elementMatches;
		this.listElementType = listElementType;
	}
	
	@Override
	public List<Word> getOptionalMatchedWords() {
		// all words in the list are optional, because the empty list is also
		// a valid match
		return getMatchedWords();
	}
	
	@Override
	public void removeOptionalMatchedWords(List<Word> words) {
		// first, we need to remove the words from the list of matches words
		List<Word> newMatchedWords = new ArrayList<Word>(getMatchedWords());
		newMatchedWords.removeAll(words);
		setMatchedWords(newMatchedWords);
		
		// second, we need to remove all part matches that correspond to the
		// words which must be removed
		for (Word word : words) {
			Iterator<ISyntaxPatternPartMatch> iterator = elementMatches.iterator();
			while (iterator.hasNext()) {
				ISyntaxPatternPartMatch elementMatch = iterator.next();
				List<Word> matchedWords = elementMatch.getMatchedWords();
				if (matchedWords.contains(word)) {
					iterator.remove();
				}
			}
		}
	}

	public List<ISyntaxPatternPartMatch> getElementMatches() {
		return elementMatches;
	}

	public IClass getListElementType() {
		return listElementType;
	}
}
