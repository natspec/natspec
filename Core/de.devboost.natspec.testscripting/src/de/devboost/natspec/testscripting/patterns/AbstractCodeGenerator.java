package de.devboost.natspec.testscripting.patterns;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.builder.CodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.parts.IMatchCodeGeneratorFactory;

public abstract class AbstractCodeGenerator extends AbstractCodeFragment {

	private final ISyntaxPatternMatch<?> match;
	private final ICommentGenerator commentGenerator;
	private final IMatchCodeGeneratorFactory codeGeneratorFactory;

	public AbstractCodeGenerator(ISyntaxPatternMatch<?> match,
			IMatchCodeGeneratorFactory codeGeneratorFactory,
			ICommentGenerator commentGenerator) {
		
		super();
		this.match = match;
		this.commentGenerator = commentGenerator;
		this.codeGeneratorFactory = codeGeneratorFactory;
	}

	public ISyntaxPatternMatch<?> getMatch() {
		return match;
	}

	protected String getComment() {
		List<Word> words = getMatch().getWords();
		return commentGenerator.getComment(words);
	}

	protected String getCode(ISyntaxPatternPartMatch match) {
		return getCode(match, null);
	}
	
	protected String getCode(ISyntaxPatternPartMatch match, ICodeGenerationContext globalContext) {
		ICodeFragment generator = this.codeGeneratorFactory.createGenerator(match);
		CodeGenerationContext localContext = new CodeGenerationContext();
		generator.generateSourceCode(localContext);
		String code = localContext.getCode();

		if (globalContext != null) {
			// Transfer imports to global context. Otherwise they will be lost.
			globalContext.getImports().addAll(localContext.getImports());
		}
		return code;
	}
}
