package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

public class StringMatch extends AbstractSyntaxPatternPartMatch {

	private final String value;

	public StringMatch(List<Word> words, String value) {
		super(words);
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
