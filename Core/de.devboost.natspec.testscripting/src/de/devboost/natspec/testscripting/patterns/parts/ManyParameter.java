package de.devboost.natspec.testscripting.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.patterns.IClass;

public class ManyParameter extends AbstractListParameter {

	public ManyParameter(ISyntaxPatternPart listElement, IClass listElementType) {
		super(listElement, listElementType);
	}

	@Override
	public String toSimpleString() {
		return "Many [" + getListElement() + "]";
	}

	@Override
	protected ISyntaxPatternPartMatch createMatch(List<ISyntaxPatternPartMatch> elementMatches,
			List<Word> allMatchedWords) {

		return new ManyMatch(allMatchedWords, elementMatches, getListElementType());
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (!(obj instanceof ManyParameter)) {
			return false;
		}

		return super.isEqualTo(obj);
	}
}
