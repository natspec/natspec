package de.devboost.natspec.testscripting.patterns.parts;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

// TODO Implement or delete this class
// TODO Rename this class to EnumerationParameter
public class EnumerationArgument extends AbstractStyleable implements ISyntaxPatternPart, ICompletable, IComparable {

	private static final int HASH_CODE = EnumerationArgument.class.getName().hashCode();

	public EnumerationArgument(String qualifiedTypeName) {
		super();
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {
		if (words.isEmpty()) {
			return null;
		} else {
			Word word = words.get(0);
			return new StringMatch(Collections.singletonList(word), word.getText());
		}
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	@Override
	public String toSimpleString() {
		// TODO Adjust this
		return "EnumLiteral";
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		// TODO Implement completion proposal
		return null;
	}

	@Override
	public boolean isEqualTo(Object object) {
		// Since this class does not have fields, all objects are equal.
		if(object == null || !(object instanceof EnumerationArgument)){
			return false;
		}
		return super.isEqualTo(object);
	}

	@Override
	public int computeHashCode() {
		// Since this class does not have fields, all objects can have the same
		// hash code.
		return HASH_CODE;
	}
}
