package de.devboost.natspec.testscripting.patterns.parts;

import java.util.Date;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

public class DateMatch extends AbstractSyntaxPatternPartMatch {

	private final Date date;

	public DateMatch(List<Word> words, Date date) {
		super(words);
		this.date = date;
	}
	
	public Date getDate() {
		return date;
	}
}
