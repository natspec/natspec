package de.devboost.natspec.testscripting.patterns;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.devboost.language.bbcode.BBCodeText;
import de.devboost.language.bbcode.Bold;
import de.devboost.language.bbcode.Color;
import de.devboost.language.bbcode.EmptyStyle;
import de.devboost.language.bbcode.Italic;
import de.devboost.language.bbcode.Strikethrough;
import de.devboost.language.bbcode.Underline;
import de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DefaultSentenceMatchParameter;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.patterns.parts.styling.Style;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;
import de.devboost.natspec.testscripting.patterns.parts.WrappedPart;

public class PatternParser<GeneratorType extends ICodeFragment> {

	private final static Pattern PLACEHOLDER_PATTERN = Pattern.compile("#[0-9]+");

	private final IParameterFactory parameterFactory;
	private final ICodeGeneratorFactory<GeneratorType> codeGeneratorFactory;

	public PatternParser(IParameterFactory parameterFactory,
			ICodeGeneratorFactory<GeneratorType> codeGeneratorFactory) {
		
		this.parameterFactory = parameterFactory;
		this.codeGeneratorFactory = codeGeneratorFactory;
	}

	private static class Placeholder {

		private final String prefix;
		private final String suffix;
		private final int parameterIndex;

		public Placeholder(String prefix, String suffix, int parameterIndex) {
			this.prefix = prefix;
			this.suffix = suffix;
			this.parameterIndex = parameterIndex;
		}

		public String getPrefix() {
			return prefix;
		}

		public String getSuffix() {
			return suffix;
		}

		public int getParameterIndex() {
			return parameterIndex;
		}
	}

	/**
	 * Parses the given syntax specification and returns a respective syntax pattern.
	 * 
	 * @param projectName
	 *            the project containing the type that holds the method that is associated with the syntax pattern
	 * @param containgTypeName
	 *            the type that holds the method that is associated with the syntax pattern
	 * @param methodName
	 *            the method that is associated with the syntax pattern
	 * @param syntaxSpecification
	 *            the syntax specification (i.e., the value of the <code>@TextSyntax</code> annotation)
	 * @param parameters
	 *            the parameters of the method that is associated with the syntax pattern
	 * @param returnType
	 *            the return type of method that is associated with the syntax pattern or <code>null</code> if the
	 *            method's return type is <code>void</code>
	 * @param callback
	 *            a callback that handled warnings and errors
	 *            
	 * @return a syntax pattern or <code>null</code> if parsing the syntax specification has failed
	 */
	// FIXME jreimann Split this method into smaller ones
	public ISyntaxPattern<?> parse(String projectName, String containgTypeName, String methodName,
			String syntaxSpecification, List<? extends IParameter> parameters, IClass returnType,
			IValidationCallback callback, Style style, boolean doBBCodeStylingOfTextParts, boolean parameterSyntax) {

		List<ISyntaxPatternPart> patternParts = new ArrayList<ISyntaxPatternPart>();
		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();

		// Check whether syntax specification is empty
		if (syntaxSpecification.trim().isEmpty()) {
			callback.addError("Syntax specification must not be empty.");
			return null;
		}

		// clean syntaxSpecification from BBCode
		List<Style> styles = null;
		if (doBBCodeStylingOfTextParts) {
			BBCodeText bbCodedText = getBBCodedText(syntaxSpecification);
			if (bbCodedText != null) {
				List<Color> invalidColorStyles = bbCodedText.getInvalidColorStyles();
				for (Color invalidColor : invalidColorStyles) {
					String invalidColorString = invalidColor.getValue();
					callback.addWarning("Color '" + invalidColorString + "' is not valid.");
				}
				String bbCodeCleanedSyntaxSpecification = bbCodedText.getText();
				styles = getStyleInformationForEachWord(bbCodedText);
				syntaxSpecification = bbCodeCleanedSyntaxSpecification;
			} else {
				callback.addError("This styled text syntax cannot be parsed correctly.");
			}
		}

		String[] parts = syntaxSpecification.split(" ");
		List<IParameter> parametersWithErrors = new ArrayList<IParameter>(4);

		boolean isDefaultSyntaxPattern = parameterFactory.isDefaultSyntaxPattern(parts, parameters);
		if (isDefaultSyntaxPattern) {
			boolean isMany = parameterFactory.isDefaultSyntaxPatternWithMany(parameters);
			DefaultSentenceMatchParameter fullSentenceParameter = new DefaultSentenceMatchParameter(isMany);
			patternParts.add(fullSentenceParameter);
			partToParameterIndexMap.put(fullSentenceParameter, 0);
		} else {
			// TODO Separate validation of parts from creation of pattern

			// In this map we collect all parameter indices that have been used. This is required to detect whether an
			// index was used multiple times, which causes a warning.
			Set<Integer> usedParameterIndices = new LinkedHashSet<Integer>();
			boolean foundDuplicateParameterIndex = false;

			for (int i = 0; i < parts.length; i++) {
				String part = parts[i];

				List<Placeholder> placeholders = findPlaceholder(part);
				// check if a specific style information is given
				Style partStyle = null;
				if (styles != null && !styles.isEmpty()) {
					partStyle = styles.get(i);
				}
				if (!placeholders.isEmpty()) {
					for (Placeholder placeholder : placeholders) {
						int index = placeholder.getParameterIndex();
						// specify type
						if (index < parameters.size()) {
							IParameter parameter = parameters.get(index);
							ISyntaxPatternPart parameterPart;
							try {
								parameterPart = parameterFactory.createParameter(parameter, false);
							} catch (UnsupportedTypeException e) {
								if (!e.isHandled()) {
									callback.addError(e.getMessage());
								}
								parametersWithErrors.add(parameter);
								continue;
							}

							String prefix = placeholder.getPrefix();
							String suffix = placeholder.getSuffix();
							// wrap parameter if required
							if (!prefix.isEmpty() || !suffix.isEmpty()) {
								parameterPart = new WrappedPart(parameterPart, prefix, suffix);
							}
							if (usedParameterIndices.contains(index)) {
								foundDuplicateParameterIndex = true;
							}
							usedParameterIndices.add(index);
							if (partStyle != null) {
								parameterPart.setStyle(partStyle);
							} else {
								parameterPart.setStyle(style);
							}
							patternParts.add(parameterPart);
							partToParameterIndexMap.put(parameterPart, index);
						} else {
							// Signal validation error to annotate error marker
							callback.addWarning("Invalid parameter index: " + (index + 1));
						}
					}
				} else {
					StaticWord staticWord = new StaticWord(part);
					if (partStyle != null) {
						staticWord.setStyle(partStyle);
					} else {
						staticWord.setStyle(style);
					}
					patternParts.add(staticWord);
				}
			}

			if (foundDuplicateParameterIndex) {
				callback.addWarning("Found duplicate parameter index.");
			}
		}
		
		// for all parameters that have not been explicitly mapped, add implicit parameters
		for (int i = 0; i < parameters.size(); i++) {
			if (partToParameterIndexMap.values().contains(Integer.valueOf(i))) {
				continue;
			}

			IParameter parameter = parameters.get(i);
			if (parametersWithErrors.contains(parameter)) {
				continue;
			}

			ISyntaxPatternPart parameterPart;
			try {
				parameterPart = parameterFactory.createParameter(parameter, true);
			} catch (UnsupportedTypeException e) {
				if (!e.isHandled()) {
					callback.addError(e.getMessage());
				}
				continue;
			}

			// Note: the implicit parts must be added to the front to make sure they're considered during the matching
			// the process. I'm (mseifert) not sure whether this is still required.
			patternParts.add(0, parameterPart);
			partToParameterIndexMap.put(parameterPart, i);
		}

		DynamicSyntaxPattern<GeneratorType> result = new DynamicSyntaxPattern<GeneratorType>(projectName,
				containgTypeName, methodName, parameters, returnType, patternParts, partToParameterIndexMap,
				parameterFactory, codeGeneratorFactory, parameterSyntax);
		return result;
	}

	/**
	 * Parses the given <code>syntaxSpecification</code> into a BBCode {@link Sentence}. If no BBCode {@link Sentence}
	 * could be parsed, <code>null</code> is returned.
	 */
	private BBCodeText getBBCodedText(String syntaxSpecification) {
		Resource resource;
		try {
			resource = BbcodeResourceUtil.getResource(syntaxSpecification.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// FIXME jreimann Write the exception to the Eclipse error log (Use TestConnectorPlugin.logError())
			e.printStackTrace();
			return null;
		}

		if (resource == null) {
			return null;
		}
		
		List<EObject> contents = resource.getContents();
		if (contents != null && !contents.isEmpty()) {
			EObject model = contents.get(0);
			if (model instanceof BBCodeText) {
				return (BBCodeText) model;
			}
		}

		return null;
	}

	/**
	 * Returns a list of {@link Style}s, one for each word in the given <code>syntaxSpecification</code>. The index of a
	 * {@link Style} object in the list corresponds to the index of a word in the <code>syntaxSpecification</code>.
	 */
	// FIXME jreimann Split this method into smaller ones
	private List<Style> getStyleInformationForEachWord(BBCodeText bbcodeText) {
		List<Style> styles = new ArrayList<Style>();
		if (bbcodeText == null) {
			return styles;
		}
		
		List<EmptyStyle> textNodes = new ArrayList<EmptyStyle>();
		Iterator<EObject> iterator = bbcodeText.eAllContents();
		while (iterator.hasNext()) {
			EObject next = (EObject) iterator.next();
			if (next instanceof EmptyStyle) { // then it's a leaf
				textNodes.add((EmptyStyle) next);
			}
		}
		
		for (EmptyStyle textNode : textNodes) {
			EObject container = textNode.eContainer();
			boolean bold = false;
			boolean italic = false;
			boolean underline = false;
			boolean strikethrough = false;
			int[] color = null;
			while (container != null && !(container instanceof BBCodeText)) {
				if (container instanceof Bold) {
					bold = true;
				} else if (container instanceof Italic) {
					italic = true;
				} else if (container instanceof Underline) {
					underline = true;
				} else if (container instanceof Strikethrough) {
					strikethrough = true;
				} else if (container instanceof Color) {
					Color bbcodeColor = (Color) container;
					int red = bbcodeColor.getRed();
					int green = bbcodeColor.getGreen();
					int blue = bbcodeColor.getBlue();
					color = new int[] { red, green, blue };
				}
				
				container = container.eContainer();
			}
			
			String[] textParts = textNode.getText().trim().split(" ");
			for (int i = 0; i < textParts.length; i++) {
				Style currentStyle = new Style(bold, italic, underline, strikethrough, color);
				styles.add(currentStyle);
			}
		}

		return styles;
	}

	// FIXME jreimann Split this method into smaller ones
	private List<Placeholder> findPlaceholder(String part) {
		Matcher matcher = PLACEHOLDER_PATTERN.matcher(part);

		LinkedList<String> parts = new LinkedList<String>();
		int lastEnd = 0;
		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();
			// compute text before match
			String textBeforeMatch = part.substring(lastEnd, start);
			parts.add(textBeforeMatch);
			String match = part.substring(start, end);
			parts.add(match);
			lastEnd = end;
		}
		String textAfterLastMatch = part.substring(lastEnd, part.length());
		parts.add(textAfterLastMatch);

		List<Placeholder> placeholders = new ArrayList<Placeholder>();
		String prefix = "";
		String placeholder = null;
		String suffix = "";
		boolean beforePlaceholder = true;
		while (!parts.isEmpty()) {
			String head = parts.poll();
			if (head.startsWith("#")) {
				// found placeholder
				if (beforePlaceholder) {
					placeholder = head;
					beforePlaceholder = false;
					// continue collecting suffix
				} else {
					String indexText = placeholder.substring(1);
					int index = getIndex(indexText);
					if (index >= 0) {
						placeholders.add(new Placeholder(prefix, suffix, index));
					}
					placeholder = null;
					prefix = "";
					suffix = "";
				}
			} else {
				if (beforePlaceholder) {
					prefix = head;
				} else {
					suffix = head;
				}
			}
		}

		if (placeholder != null) {
			String indexText = placeholder.substring(1);
			int index = getIndex(indexText);
			if (index >= 0) {
				placeholders.add(new Placeholder(prefix, suffix, index));
			}
		}
		return placeholders;
	}

	private int getIndex(String indexText) {
		String indexString = indexText;
		try {
			int index = Integer.parseInt(indexString) - 1;
			return index;
		} catch (NumberFormatException nfe) {
			// ignore this. failure is handled by calling method.
		}
		return -1;
	}
}
