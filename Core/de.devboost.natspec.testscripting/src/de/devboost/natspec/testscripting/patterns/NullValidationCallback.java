package de.devboost.natspec.testscripting.patterns;

public class NullValidationCallback implements IValidationCallback {
	
	public final static NullValidationCallback INSTANCE = new NullValidationCallback();

	private NullValidationCallback() {
	}

	@Override
	public void beginValidation() {
		// do nothing
	}
	
	@Override
	public void addWarning(String message) {
		// do nothing
	}

	@Override
	public void addError(String message) {
		// do nothing
	}
}
