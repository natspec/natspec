package de.devboost.natspec.testscripting.patterns;

import de.devboost.natspec.testscripting.util.TypeStringUtil;

public abstract class AbstractClass implements IClass {

	private String allSuperTypeString;

	@Override
	public String getAllSuperTypeString() {
		if (allSuperTypeString == null) {
			allSuperTypeString = TypeStringUtil.INSTANCE.getTypeString(getAllSuperTypes());
		}
		return allSuperTypeString;
	}
}
