package de.devboost.natspec.testscripting.patterns.parts;

import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.parts.DefaultSentencePartMatch;
import de.devboost.natspec.patterns.parts.DoubleMatch;
import de.devboost.natspec.patterns.parts.FloatMatch;
import de.devboost.natspec.patterns.parts.IntegerMatch;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.patterns.AbstractCodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;

public abstract class AbstractMatchCodeGeneratorFactory implements IMatchCodeGeneratorFactory {

	@Override
	public ICodeFragment createGenerator(final ISyntaxPatternPartMatch match) {

		if (match instanceof BooleanMatch) {
			final BooleanMatch booleanMatch = (BooleanMatch) match;
			return this.createGenerator(booleanMatch);
		}
		if (match instanceof StringMatch) {
			final StringMatch stringMatch = (StringMatch) match;
			return this.createGenerator(stringMatch);
		}
		if (match instanceof DateMatch) {
			final DateMatch dateMatch = (DateMatch) match;
			return this.createGenerator(dateMatch);
		}
		if (match instanceof IntegerMatch) {
			final IntegerMatch integerMatch = (IntegerMatch) match;
			return createGenerator(integerMatch);
		}
		if (match instanceof DoubleMatch) {
			final DoubleMatch doubleMatch = (DoubleMatch) match;
			return createGenerator(doubleMatch);
		}
		if (match instanceof FloatMatch) {
			final FloatMatch floatMatch = (FloatMatch) match;
			return createGenerator(floatMatch);
		}
		if (match instanceof ObjectMatch) {
			final ObjectMatch objectMatch = (ObjectMatch) match;
			return createGenerator(objectMatch);
		}
		if (match instanceof ListMatch) {
			final ListMatch listMatch = (ListMatch) match;
			return createGenerator(listMatch);
		}
		if (match instanceof ManyMatch) {
			final ManyMatch manyMatch = (ManyMatch) match;
			return createGenerator(manyMatch);
		}
		if (match instanceof ImplicitParameterMatch) {
			final ImplicitParameterMatch implicitMatch = (ImplicitParameterMatch) match;
			return createGenerator(implicitMatch);
		}
		if (match instanceof DefaultSentencePartMatch) { // must stay in the last
															// position to ensure
															// that is only
															// evaluated when no
															// other pattern matched
			final DefaultSentencePartMatch sentenceMatch = (DefaultSentencePartMatch) match;
			return createGenerator(sentenceMatch);
		}

		return createGeneratorForUnknown(match);
	}

	@Override
	public ICodeFragment createGeneratorForUnknown(final ISyntaxPatternPartMatch match) {
		return new AbstractCodeFragment() {
			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode("/* TODO unknown match: " + match + " */");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final ImplicitParameterMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				ObjectCreation objectCreation = match.getObjectCreation();
				// If the ImplicitParameter was optional and no matching object was found in the context, null is passed
				// as argument to the support method
				if (objectCreation == null) {
					if (match.isOptional()) {
						context.addCode("null");
					} else {
						// Assert that match isThis
						context.addCode("this");
					}
				} else {
					context.addCode(objectCreation.getVariableName());
				}
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final IntegerMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(Integer.toString(match.getValue()));
			}
		};
	}
}
