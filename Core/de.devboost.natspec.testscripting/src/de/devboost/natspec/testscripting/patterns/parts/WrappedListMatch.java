package de.devboost.natspec.testscripting.patterns.parts;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.patterns.IClass;

/**
 * A {@link WrappedListMatch} represents a match of a {@link ListParameter} to a sequence of words.
 */
public class WrappedListMatch extends ListMatch {

	private final String prefix;
	private final String suffix;

	public WrappedListMatch(List<Word> matchedWords, List<ISyntaxPatternPartMatch> elementMatches,
			IClass listElementType, String prefix, String suffix) {

		super(matchedWords, elementMatches, listElementType);
		this.prefix = prefix;
		this.suffix = suffix;
	}

	@Override
	public List<Word> getOptionalMatchedWords() {
		// all words in the list are optional, because the empty list is also
		// a valid match
		List<Word> matchedWords = getMatchedWords();

		if (matchedWords.isEmpty()) {
			return Collections.emptyList();
		}

		if (suffix.length() != 0) {
			return Collections.emptyList();
		} else if (prefix.length() != 0) {
			LinkedList<Word> optionalWords = new LinkedList<Word>();
			optionalWords.add(matchedWords.get(0));
		}
		return matchedWords;
	}
}
