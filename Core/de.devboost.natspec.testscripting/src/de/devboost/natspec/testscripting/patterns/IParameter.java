package de.devboost.natspec.testscripting.patterns;

import java.util.List;

public interface IParameter {

	public IClass getType();

	public List<IAnnotation> getAnnotations();

	public String getName();
}
