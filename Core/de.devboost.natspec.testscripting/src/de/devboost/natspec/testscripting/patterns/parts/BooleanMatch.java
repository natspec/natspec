package de.devboost.natspec.testscripting.patterns.parts;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

public class BooleanMatch extends AbstractSyntaxPatternPartMatch {

	private final boolean value;
	private final boolean optional;

	public BooleanMatch(boolean value) {
		super(Collections.<Word>emptyList());
		this.value = value;
		this.optional = true;
	}
	
	public BooleanMatch(Word matchedWord, boolean value) {
		super(Collections.singletonList(matchedWord));
		this.value = value;
		this.optional = false;
	}
	
	public boolean getValue() {
		return value;
	}
	
	@Override
	public List<Word> getOptionalMatchedWords() {
		if (optional) {
			return getMatchedWords();
		} else {
			return Collections.emptyList();
		}
	}
}
