package de.devboost.natspec.testscripting.patterns.parts;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.IMultiCompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.IWordMatcher;
import de.devboost.natspec.matching.WordMatcherFactory;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

public class BooleanParameter extends AbstractStyleable implements ISyntaxPatternPart, IMultiCompletable, IComparable {

	private final String trueValue;
	private final String falseValue;
	
	private List<IWordMatcher> trueWordMatchers;
	private List<IWordMatcher> falseWordMatchers;

	/**
	 * Creates a new {@link BooleanParameter} that assigns 'trueValue' to <code>true</code> and 'falseValue' to
	 * <code>false</code>. Either 'trueValue' or 'falseValue' can be empty, but not both of them.
	 * 
	 * @param trueValue
	 *            non-null string that represents <code>true</code>
	 * @param falseValue
	 *            non-null string that represents <code>false</code>
	 */
	public BooleanParameter(String trueValue, String falseValue) {
		this.trueValue = trueValue;
		this.falseValue = falseValue;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		Word word;
		String text;
		if (words.isEmpty()) {
			word = null;
			text = null;
		} else {
			word = words.get(0);
			text = word.getText().toLowerCase();
		}

		if (!trueValue.isEmpty()) {
			if (text != null) {
				if (matches(getTrueWordMatchers(), context, words)) {
					return new BooleanMatch(word, true);
				}
			}
		}

		if (!falseValue.isEmpty()) {
			if (text != null) {
				if (matches(getFalseWordMatchers(), context, words)) {
					return new BooleanMatch(word, false);
				}
			}
		}

		if (trueValue.isEmpty()) {
			return new BooleanMatch(true);
		}
		if (falseValue.isEmpty()) {
			return new BooleanMatch(false);
		}

		return null;
	}

	private boolean matches(List<IWordMatcher> wordMatchers, IPatternMatchContext context, List<Word> words) {
		if (words.size() < wordMatchers.size()) {
			// No enough words left
			return false;
		}
		
		for (int i = 0; i < wordMatchers.size(); i++) {
			IWordMatcher wordMatcher = wordMatchers.get(i);
			Word word = words.get(i);
			if (!wordMatcher.match(context, word.getLowerCaseText())) {
				return false;
			}
		}

		return true;
	}

	private List<IWordMatcher> getTrueWordMatchers() {
		if (trueWordMatchers == null) {
			trueWordMatchers = createWordMatchers(trueValue);
		}
		
		return trueWordMatchers;
	}

	private List<IWordMatcher> getFalseWordMatchers() {
		if (falseWordMatchers == null) {
			falseWordMatchers = createWordMatchers(falseValue);
		}
		
		return falseWordMatchers;
	}

	private List<IWordMatcher> createWordMatchers(String value) {
		String[] parts = value.split(" ");
		
		List<IWordMatcher> wordMatchers = new ArrayList<IWordMatcher>(parts.length);
		for (String part : parts) {
			wordMatchers.add(WordMatcherFactory.INSTANCE.createWordMatcher(part));
		}
		
		return wordMatchers;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	@Override
	public String toSimpleString() {
		if (trueValue.isEmpty()) {
			return "(" + falseValue + ")?";
		} else if (falseValue.isEmpty()) {
			return "(" + trueValue + ")?";
		} else {
			return "[" + trueValue + " | " + falseValue + "]";
		}
	}

	@Override
	public Set<String> getCompletionProposals(IPatternMatchContext context) {
		Set<String> proposals = new LinkedHashSet<String>(2);
		proposals.add(trueValue);
		proposals.add(falseValue);
		return proposals;
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((falseValue == null) ? 0 : falseValue.hashCode());
		result = prime * result + ((trueValue == null) ? 0 : trueValue.hashCode());
		return result;
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		BooleanParameter other = (BooleanParameter) obj;
		if (falseValue == null) {
			if (other.falseValue != null) {
				return false;
			}
		} else if (!falseValue.equals(other.falseValue)) {
			return false;
		}
		
		if (trueValue == null) {
			if (other.trueValue != null) {
				return false;
			}
		} else if (!trueValue.equals(other.trueValue)) {
			return false;
		}
		
		return super.isEqualTo(obj);
	}
}
