package de.devboost.natspec.testscripting.patterns;

/**
 * An {@link ICodeFragment} can be used to generate a piece of source code.
 * <p>
 * Note: Do not implement this interface directly. Instead extend the default
 * implementation ({@link AbstractCodeFragment}).
 */
public interface ICodeFragment {

	/**
	 * Generate a piece of source code and add it to the given context using
	 * {@link ICodeGenerationContext#addCode(String)}.
	 * 
	 * @param context the context to collect the code fragments
	 */
	public void generateSourceCode(ICodeGenerationContext context);
	
	public boolean isParameterizationCode();
}
