package de.devboost.natspec.testscripting.patterns;

import java.util.Set;

public interface ICodeGenerationContext {

	public int getNextCounter();
	public void addCode(String code);
	public void addImport(String code);
	public String getCode();
	public Set<String> getImports();
	
	public boolean isLoggingEnabled();
	public void setLoggingEnabled(boolean loggingEnabled);
}
