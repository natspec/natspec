package de.devboost.natspec.testscripting.patterns;

import java.util.Map;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;

/**
 * The {@link ICodeGeneratorFactory} interfaces is used to decouple the generation of code for matched syntax patterns
 * from the syntax pattern itself. This allows to reuse the {@link DynamicSyntaxPattern} class for different target
 * languages.
 * 
 * @param <GeneratorType>
 *            the code generator class
 */
public interface ICodeGeneratorFactory<GeneratorType extends ICodeFragment> {

	/**
	 * Creates a new code generator that can produce code for the given matched sentence.
	 * 
	 * @param match
	 *            the matched sentence
	 * @param qualifiedTypeName
	 *            the type containing the test support method
	 * @param methodName
	 *            the name of the test support method
	 * @param parameterTypes
	 *            the qualified names of the parameters of the test support method
	 * @param qualifiedReturnType
	 *            the return type of the test support method
	 * @param argumentMatches
	 *            a map from argument indices to their matches
	 * @param returnVariableName
	 *            the name of the variable to which the return value must be assigned
	 * @return the code generator
	 */
	public GeneratorType createCodeGenerator(ISyntaxPatternMatch<GeneratorType> match, String qualifiedTypeName,
			String methodName, String[] parameterTypes, String qualifiedReturnType,
			Map<Integer, ISyntaxPatternPartMatch> argumentMatches, String returnVariableName, boolean parameterSyntax);
}
