package de.devboost.natspec.testscripting.patterns.parts;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.IWordMatcher;
import de.devboost.natspec.matching.WordMatcherFactory;
import de.devboost.natspec.matching.WordUtil;

public class ComplexParameterMatcher {

	public final static ComplexParameterMatcher INSTANCE = new ComplexParameterMatcher();

	private ComplexParameterMatcher() {
	}

	/**
	 * See {@link #matchStrings(List, List, IPatternMatchContext, String)}. This methods does the same, but takes
	 * objects of type {@link Word} instead of plain strings.
	 */
	public int match(List<Word> words, List<String> identifiers, IPatternMatchContext context, String simpleTypeName) {

		WordMatcherFactory factory = WordMatcherFactory.INSTANCE;
		IWordMatcher wordMatcher = factory.createWordMatcher(simpleTypeName);
		int identifierCount = identifiers.size();
		if (identifierCount > words.size()) {
			// too few words
			return 0;
		}

		if (identifierCount == 0 && words.size() > 0) {
			Word word = words.get(0);
			if (wordMatcher.match(context, word.getLowerCaseText())) {
				return 1;
			}
		}

		List<String> texts = WordUtil.INSTANCE.toStringList(words);
		return matchStrings(texts, identifiers, context, simpleTypeName);
	}

	/**
	 * Matches the given list of words against the list of identifiers. This methods differs from
	 * {@link #matchLists(List, List, IPatternMatchContext)} as it allows the two lists to match not only if all of
	 * their words match, but also if the list match if one prepends <code>simpleTypeName</code> to the list of
	 * identifiers.
	 * 
	 * @param words
	 *            the list of words (found in a NatSpec document)
	 * @param identifiers
	 *            the list of identifiers (that identify an object)
	 * @param context
	 *            the context in which the matching must be performed
	 * @param simpleTypeName
	 *            the simple name of the object that is identified by the identifiers
	 * 
	 * @return the number of matching words at the head of the words list
	 */
	public int matchStrings(List<String> words, List<String> identifiers, IPatternMatchContext context,
			String simpleTypeName) {

		int identifierCount = identifiers.size();
		int matching = matchLists(words, identifiers, context);
		if (matching == identifierCount) {
			return matching;
		}

		if (simpleTypeName == null) {
			return 0;
		}

		List<String> identifiersWithTypeInFront = new ArrayList<String>();
		identifiersWithTypeInFront.add(simpleTypeName);
		identifiersWithTypeInFront.addAll(identifiers);
		identifierCount = identifiersWithTypeInFront.size();
		if (identifierCount > words.size() - 1) {
			// too few words
			return 0;
		}
		matching = matchLists(words, identifiersWithTypeInFront, context);
		if (matching == identifierCount) {
			return matching;
		}

		return 0;
	}

	/**
	 * Matches the given two list of words against each other using the passed context.
	 * 
	 * @return the number of matching words at the head of both lists
	 */
	private int matchLists(List<String> list1, List<String> list2, IPatternMatchContext context) {

		WordMatcherFactory matcherFactory = WordMatcherFactory.INSTANCE;

		int size1 = list1.size();
		int size2 = list2.size();
		int minSize = Math.min(size1, size2);
		
		for (int i = 0; i < minSize; i++) {
			String text1 = list1.get(i);
			String text2 = list2.get(i);

			IWordMatcher matcher = matcherFactory.createWordMatcher(text2);
			if (!matcher.match(context, text1.toLowerCase())) {
				return i;
			}
		}
		
		return minSize;
	}
}
