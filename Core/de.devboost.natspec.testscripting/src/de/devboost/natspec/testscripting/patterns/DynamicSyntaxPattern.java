package de.devboost.natspec.testscripting.patterns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.ISyntaxPatternSourceProvider;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;
import de.devboost.natspec.util.ComparisonHelper;

/**
 * A {@link DynamicSyntaxPattern} can be used to map a syntax pattern to a
 * support method.
 */
public class DynamicSyntaxPattern<GeneratorType extends ICodeFragment> extends
		AbstractSyntaxPattern<GeneratorType> implements IComparable, IParameterIndexProvider, ISyntaxPatternSourceProvider {

	// Attention: If fields are added here, make sure to adjust isEqualTo()!
	private final String projectName;
	private final String qualifiedTypeName;
	private final String methodName;
	private final IClass returnType;
	private final List<ISyntaxPatternPart> parts;
	private final Map<ISyntaxPatternPart, Integer> partToParameterIndexMap;
	private final List<? extends IParameter> parameters;
	private final ICodeGeneratorFactory<GeneratorType> codeGeneratorFactory;
	private final boolean parameterSyntax;

	public DynamicSyntaxPattern(String projectName, String qualifiedTypeName, String methodName,
			List<? extends IParameter> parameters, IClass returnType, List<ISyntaxPatternPart> parts,
			Map<ISyntaxPatternPart, Integer> partToParameterIndexMap, IParameterFactory parameterFactory,
			ICodeGeneratorFactory<GeneratorType> codeGeneratorFactory) {

		this(projectName, qualifiedTypeName, methodName, parameters, returnType, parts, partToParameterIndexMap,
				parameterFactory, codeGeneratorFactory, false);
	}

	/**
	 * Creates a new {@link DynamicSyntaxPattern}.
	 * 
	 * @param projectName
	 *            the name of the Eclipse project that contains the support
	 *            class
	 * @param qualifiedTypeName
	 *            the qualified name of the support class
	 * @param methodName
	 *            the name of the method that implements the semantics for the
	 *            syntax pattern
	 * @param parameters
	 *            the parameters of the method that implements the semantics for
	 *            the syntax pattern
	 * @param returnType
	 *            the return type of the method that implements the semantics
	 *            for the syntax pattern
	 * @param parts
	 *            the syntax pattern parts
	 * @param partToParameterIndexMap
	 *            a map that associates syntax pattern parts with parameter
	 *            indices
	 * @param parameterFactory
	 *            a factory to create syntax pattern parts for parameters
	 * @param codeGeneratorFactory
	 *            a factory to create code generators
	 */
	public DynamicSyntaxPattern(String projectName, String qualifiedTypeName,
			String methodName, List<? extends IParameter> parameters,
			IClass returnType, List<ISyntaxPatternPart> parts,
			Map<ISyntaxPatternPart, Integer> partToParameterIndexMap,
			IParameterFactory parameterFactory,
			ICodeGeneratorFactory<GeneratorType> codeGeneratorFactory,
			boolean parameterSyntax) {

		super(parameterFactory);
		this.projectName = projectName;
		this.qualifiedTypeName = qualifiedTypeName;
		this.methodName = methodName;
		this.returnType = returnType;
		this.parameters = parameters;
		this.parts = Collections.unmodifiableList(parts);
		this.partToParameterIndexMap = partToParameterIndexMap;
		this.codeGeneratorFactory = codeGeneratorFactory;
		this.parameterSyntax = parameterSyntax;
	}

	@Override
	public List<ISyntaxPatternPart> getParts() {
		return parts;
	}

	@Override
	public GeneratorType createUserData(ISyntaxPatternMatch<GeneratorType> match) {
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();

		Map<Integer, ISyntaxPatternPartMatch> argumentMatches = new LinkedHashMap<Integer, ISyntaxPatternPartMatch>(partsToMatchesMap.size());
		// find matches for method arguments
		for (ISyntaxPatternPart argumentPart : partsToMatchesMap.keySet()) {
			Integer index = partToParameterIndexMap.get(argumentPart);
			if (index == null) {
				continue;
			}
			int parameterIndex = index;
			ISyntaxPatternPartMatch argumentMatch = partsToMatchesMap
					.get(argumentPart);
			argumentMatches.put(parameterIndex, argumentMatch);
		}

		String qualifiedReturnType = null;
		String returnVariableName = null;
		if (returnType != null) {
			qualifiedReturnType = returnType.getQualifiedName();
			List<String> identifiers = new ArrayList<String>();
			// We must sort the indices of the parameters to make sure they are
			// used in the same order in the resulting identifier list as in the
			// original sentence.
			List<Integer> indices = new ArrayList<Integer>(argumentMatches.keySet());
			Collections.sort(indices);
			
			for (int index : indices) {
				ISyntaxPatternPartMatch argumentMatch = argumentMatches
						.get(index);
				List<Word> matchedWords = argumentMatch.getMatchedWords();
				identifiers.addAll(WordUtil.INSTANCE.toStringList(matchedWords));
			}
			IPatternMatchContext context = match.getContext();
			ObjectCreation objectCreation = new ObjectCreation(context,
					returnType, identifiers);
			returnVariableName = objectCreation.getVariableName();
			context.addObjectToContext(objectCreation);
		}

		String[] parameterTypes = getTypes(parameters);
		
		return codeGeneratorFactory.createCodeGenerator(match, qualifiedTypeName, methodName, parameterTypes,
				qualifiedReturnType, argumentMatches, returnVariableName, parameterSyntax);
	}

	private String[] getTypes(List<? extends IParameter> parameters) {
		if (parameters == null) {
			return null;
		}
		
		String[] parameterTypes = new String[parameters.size()];
		for (int i = 0; i < parameterTypes.length; i++) {
			parameterTypes[i] = parameters.get(i).getType().getQualifiedName();
		}
		
		return parameterTypes;
	}

	@Override
	public String getProjectName() {
		return projectName;
	}

	@Override
	public String getQualifiedTypeName() {
		return qualifiedTypeName;
	}

	@Override
	public String getMethodName() {
		return methodName;
	}

	public List<? extends IParameter> getParameters() {
		return parameters;
	}
	
	public IClass getReturnType() {
		return returnType;
	}

	@Override
	public Map<ISyntaxPatternPart, Integer> getPartToParameterIndexMap() {
		return Collections.unmodifiableMap(partToParameterIndexMap);
	}
	
	public ICodeGeneratorFactory<GeneratorType> getCodeGeneratorFactory() {
		return codeGeneratorFactory;
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		DynamicSyntaxPattern<?> other = (DynamicSyntaxPattern<?>) obj;
		ComparisonHelper comparisonHelper = ComparisonHelper.INSTANCE;
		if (!comparisonHelper.isEqualTo(projectName, other.projectName)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(methodName, other.methodName)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(parameters, other.parameters)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(partToParameterIndexMap, other.partToParameterIndexMap)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(parts, other.parts)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(qualifiedTypeName, other.qualifiedTypeName)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(returnType, other.returnType)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(codeGeneratorFactory, other.codeGeneratorFactory)) {
			return false;
		}
		if (!comparisonHelper.isEqualTo(parameterSyntax, other.parameterSyntax)) {
			return false;
		}
		return true;
	}
}
