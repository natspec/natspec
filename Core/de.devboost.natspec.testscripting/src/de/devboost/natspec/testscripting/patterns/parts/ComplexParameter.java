package de.devboost.natspec.testscripting.patterns.parts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.Word;
import de.devboost.natspec.completion.IMultiCompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.util.ComparisonHelper;

public class ComplexParameter extends AbstractStyleable implements ISyntaxPatternPart, IMultiCompletable, IComparable {

	private final String qualifiedTypeName;
	private final String simpleTypeName;

	@Deprecated
	public ComplexParameter() {
		this.qualifiedTypeName = null;
		this.simpleTypeName = null;
	}

	public ComplexParameter(String qualifiedTypeName) {
		this.qualifiedTypeName = qualifiedTypeName;
		this.simpleTypeName = NameHelper.INSTANCE.getSimpleName(qualifiedTypeName);
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {
		if (words.isEmpty()) {
			return null;
		}
		
		ComplexParameterMatcher complexParameterMatcher = ComplexParameterMatcher.INSTANCE;

		Collection<Object> objectsInContext = context.getObjectsInContext();
		for (Object objectInContext : objectsInContext) {
			// Find identifying arguments
			if (objectInContext instanceof ObjectCreation) {
				ObjectCreation entityCreation = (ObjectCreation) objectInContext;
				IClass objectType = entityCreation.getObjectType();
				Set<String> allSuperTypes = objectType.getAllSuperTypes();
				if (allSuperTypes.contains(qualifiedTypeName)) {
					// TODO Figure out why a new list is created here. The identifiers of an ObjectCreation should not
					// change after its creation and neither should this list be changed by
					// ComplexParameterMatcher.match()?
					List<String> identifiers = new ArrayList<String>(entityCreation.getIdentifiers());
					int matching = complexParameterMatcher.match(words, identifiers, context, simpleTypeName);
					if (matching > 0) {
						List<Word> matchingWords = words.subList(0, matching);
						return new ObjectMatch(matchingWords, entityCreation);
					}
				}
			}
		}
		
		return null;
	}

	public String getQualifiedTypeName() {
		return qualifiedTypeName;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [" + qualifiedTypeName + "]";
	}

	@Override
	public String toSimpleString() {
		return "<" + simpleTypeName + ">";
	}

	@Override
	public Set<String> getCompletionProposals(IPatternMatchContext context) {
		Set<String> proposals = new LinkedHashSet<String>();
		Collection<Object> objectsInContext = context.getObjectsInContext();
		for (Object objectInContext : objectsInContext) {
			// find identifying arguments
			if (objectInContext instanceof ObjectCreation) {
				ObjectCreation entityCreation = (ObjectCreation) objectInContext;
				String type = entityCreation.getObjectType().getQualifiedName();
				if (type.equals(qualifiedTypeName)) {
					List<String> identifiers = new ArrayList<String>(entityCreation.getIdentifiers());
					proposals.add(StringUtils.INSTANCE.explode(identifiers, " "));
				}
			}
		}
		
		return proposals;
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ComplexParameter other = (ComplexParameter) obj;
		if (!ComparisonHelper.INSTANCE.isEqualTo(qualifiedTypeName, other.qualifiedTypeName)) {
			return false;
		}
		if (!ComparisonHelper.INSTANCE.isEqualTo(simpleTypeName, other.simpleTypeName)) {
			return false;
		}
		return super.isEqualTo(obj);
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((qualifiedTypeName == null) ? 0 : qualifiedTypeName
						.hashCode());
		result = prime * result
				+ ((simpleTypeName == null) ? 0 : simpleTypeName.hashCode());
		return result;
	}
}
