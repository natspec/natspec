package de.devboost.natspec.testscripting.patterns.parts;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

/**
 * A {@link DateParameter} can be used to match words that represent a date.
 * Currently, the following date formats are accepted:
 * 
 * <ol>
 * <li>dd.MM.yyyy</li>
 * <li>dd.MM.yyyy&nbsp;HH:mm</li>
 * </ol>
 */
public class DateParameter extends AbstractStyleable implements ISyntaxPatternPart, ICompletable, IComparable {

	public static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
	public static final String DATE_TIME_FORMAT_PATTERN = "dd.MM.yyyy HH:mm";
	
	private final static SimpleDateFormat DATE_FORMAT;
	private final static SimpleDateFormat DATE_TIME_FORMAT;
	
	private static final int HASH_CODE = DateParameter.class.getName().hashCode();
	
	static {
		DATE_FORMAT = createDateFormat(DATE_FORMAT_PATTERN);
		DATE_TIME_FORMAT = createDateFormat(DATE_TIME_FORMAT_PATTERN);
	}

	public static SimpleDateFormat createDateFormat(String pattern) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
		dateFormat.setLenient(false);
		return dateFormat;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {
		
		if (words.isEmpty()) {
			return null;
		}
		
		Word word1 = words.get(0);
		String text1 = word1.getText();
		if (words.size() > 1) {
			// try using two words
			Word word2 = words.get(1);
			String text2 = word2.getText();
			String text = text1 + " " + text2;
			// matched 2 words
			Date date = getDate(DATE_TIME_FORMAT, text);
			if (date != null) {
				List<Word> matchedWords = new ArrayList<Word>(2);
				matchedWords.add(word1);
				matchedWords.add(word2);
				return new DateMatch(matchedWords, date);
			}
		}
		
		// try using the first word only
		Date date = getDate(DATE_FORMAT, text1);
		if (date != null) {
			return new DateMatch(Collections.singletonList(word1), date);
		}
		
		// no match found
		return null;
	}

	private Date getDate(SimpleDateFormat format, String text) {
		try {
			return format.parse(text);
		} catch (NumberFormatException e) {
			// ignore
			return null;
		} catch (ParseException e) {
			// ignore
			return null;
		}
	}

	@Override
	public String toSimpleString() {
		return "<Date>";
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		return DATE_FORMAT.format(new Date());
	}

	@Override
	public boolean isEqualTo(Object object) {
		// Since this class does not have fields, all objects are equal.
		if(object == null || !(object instanceof DateParameter)){
			return false;
		}
		return super.isEqualTo(object);
	}

	@Override
	public int computeHashCode() {
		// Since this class does not have fields, all objects can have the same
		// hash code.
		return HASH_CODE;
	}
}
