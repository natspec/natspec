package de.devboost.natspec.testscripting.patterns.parts;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.util.ComparisonHelper;

public abstract class AbstractListParameter extends AbstractStyleable implements ISyntaxPatternPart, ICompletable, IComparable {

	private final ISyntaxPatternPart listElement;
	private final IClass listElementType;

	/**
	 * Creates a new {@link AbstractListParameter} that matches a sequence if the given list elements.
	 * 
	 * @param listElement
	 *            the syntax pattern part that matches elements of the list
	 * @param listElementType
	 *            the type of the elements in the list
	 */
	public AbstractListParameter(ISyntaxPatternPart listElement, IClass listElementType) {
		this.listElement = listElement;
		this.listElementType = listElementType;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		List<ISyntaxPatternPartMatch> elementMatches = new ArrayList<ISyntaxPatternPartMatch>();
		List<Word> allMatchedWords = new ArrayList<Word>();
		boolean matches;
		do {
			ISyntaxPatternPartMatch match = listElement.match(words, context);
			if (match == null) {
				break;
			}
			matches = match.hasMatched();
			if (matches) {
				List<Word> matchedWords = match.getMatchedWords();
				allMatchedWords.addAll(matchedWords);
				elementMatches.add(match);
				words = words.subList(matchedWords.size(), words.size());
			}
		} while (matches);

		return createMatch(elementMatches, allMatchedWords);
	}

	protected abstract ISyntaxPatternPartMatch createMatch(List<ISyntaxPatternPartMatch> elementMatches,
			List<Word> allMatchedWords);

	public ISyntaxPatternPart getListElement() {
		return listElement;
	}

	public IClass getListElementType() {
		return listElementType;
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		ISyntaxPatternPart listElement = getListElement();
		if (listElement instanceof ICompletable) {
			ICompletable completable = (ICompletable) listElement;
			return completable.getCompletionProposal(context);
		} else {
			return null;
		}
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		AbstractListParameter other = (AbstractListParameter) obj;
		if (!ComparisonHelper.INSTANCE.isEqualTo(listElement, other.listElement)) {
			return false;
		}
		if (!ComparisonHelper.INSTANCE.isEqualTo(listElementType, other.listElementType)) {
			return false;
		}
		
		return super.isEqualTo(other);
	}

	@Override
	public int computeHashCode() {
		// TODO Delegating to listElement.hashCode() and listElementType.hashCode() may not be correct.
		final int prime = 31;
		int result = 1;
		result = prime * result + ((listElement == null) ? 0 : listElement.hashCode());
		result = prime * result + ((listElementType == null) ? 0 : listElementType.hashCode());
		return result;
	}
}
