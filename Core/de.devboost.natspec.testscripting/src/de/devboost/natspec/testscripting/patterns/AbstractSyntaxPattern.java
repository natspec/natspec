package de.devboost.natspec.testscripting.patterns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.SyntaxPatternMatchFactory;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.context.CommonType;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;
import de.devboost.natspec.util.ComparisonHelper;

public abstract class AbstractSyntaxPattern<UserDataType> implements
		ISyntaxPattern<UserDataType>, IComparable {

	private final static SyntaxPatternMatchFactory PATTERN_MATCH_FACTORY = new SyntaxPatternMatchFactory();

	// Attention: If fields are added, make sure to adjust isEqualTo()!
	private final IParameterFactory parameterFactory;

	private List<ISyntaxPatternPart> parts;
	
	// Cached context and pattern match
	private IPatternMatchContext previousContext;
	private ISyntaxPatternMatch<UserDataType> previousSyntaxPatternMatch;

	public AbstractSyntaxPattern(IParameterFactory parameterFactory) {
		super();
		this.parameterFactory = parameterFactory;
	}

	/**
	 * Returns the parts of this pattern. The returned list can only be modified
	 * when this method is called for the first time on instances of this class.
	 * After that, an unmodifiable list is returned.
	 */
	public List<ISyntaxPatternPart> getParts() {
		if (parts == null) {
			parts = new ArrayList<ISyntaxPatternPart>();
			return parts;
		}
		return Collections.unmodifiableList(parts);
	}

	@Override
	public ISyntaxPatternMatch<UserDataType> createPatternMatch(IPatternMatchContext context) {

		if (previousContext == context) {
			boolean isIncomplete = !previousSyntaxPatternMatch.isComplete();
			// Incomplete matches can be reused
			if (isIncomplete) {
				previousSyntaxPatternMatch.reset();
				return previousSyntaxPatternMatch;
			}
		}
		
		// Different context, create new syntax pattern match
		previousContext = context;
		previousSyntaxPatternMatch = PATTERN_MATCH_FACTORY.createSyntaxPatternMatch(this, context);
		return previousSyntaxPatternMatch;
	}

	protected ISyntaxPatternPart createParameter(IClass type)
			throws UnsupportedTypeException {
		return getParameterFactory().createParameter(type, false);
	}

	/**
	 * Use {@link #createParameter(JavaType)} instead.
	 * 
	 * @throws UnsupportedTypeException if the type is not supported by NatSpec 
	 */
	@Deprecated
	protected ISyntaxPatternPart createParameter(String qualifiedName)
			throws UnsupportedTypeException {
		return createParameter(new CommonType(qualifiedName));
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		for (ISyntaxPatternPart part : getParts()) {
			result.append(part.toSimpleString() + " ");
		}
		return result.toString().trim();
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractSyntaxPattern<?> other = (AbstractSyntaxPattern<?>) obj;
		if (!ComparisonHelper.INSTANCE.isEqualTo(parts, other.parts)) {
			return false;
		}
		if (!ComparisonHelper.INSTANCE.isEqualTo(parameterFactory, other.parameterFactory)) {
			return false;
		}
		return true;
	}
	
	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((parts == null) ? 0 : parts.hashCode());
		return result;
	}

	public IParameterFactory getParameterFactory() {
		return parameterFactory;
	}
	
	public boolean allowsOtherMatches() {
		return false;
	}
	
	@Override
	public String getDocumentation() {
		return null;
	}
}
