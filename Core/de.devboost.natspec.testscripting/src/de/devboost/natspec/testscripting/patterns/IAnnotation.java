package de.devboost.natspec.testscripting.patterns;

/**
 * This interface is an abstraction for classes as they're used in most
 * object-oriented languages.
 */
public interface IAnnotation {

	public IClass getAnnotationType();
}
