package de.devboost.natspec.testscripting;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

/**
 * The {@link RevalidateScriptsJob} scans resources in the Eclipse workspace for NatSpec files using the
 * {@link IRevalidationVisitor}s. If NatSpec files are found, the respective resources are reloaded to trigger a
 * revalidation of their contents. This is required to remove error markers that are not longer valid, because new
 * syntax patterns were registered. The {@link IRevalidationVisitor}s do also trigger code generation for all found
 * NatSpec files.
 */
public class RevalidateScriptsJob extends Job {

	private static final String JOB_NAME = "Revalidating NatSpec scripts";

	private final WorkspaceChangeCollector workspaceChangeCollector;

	private final IWorkspaceRoot workspaceRoot;

	public RevalidateScriptsJob(WorkspaceChangeCollector workspaceChangeCollector) {
		super(JOB_NAME);

		this.workspaceChangeCollector = workspaceChangeCollector;

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		workspaceRoot = workspace.getRoot();
		setRule(workspaceRoot);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		Set<IResource> changedResources = workspaceChangeCollector.retrieveChanges();
		scanWorkspace(changedResources, monitor);
		return Status.OK_STATUS;
	}

	private void scanWorkspace(Set<IResource> changedResources, IProgressMonitor monitor) {
		TestConnectorPlugin instance = TestConnectorPlugin.getInstance();
		if (instance == null) {
			return;
		}

		if (instance.isTestScriptSetUpToDate()) {
			// We use changedResources to optimize performance.
			Set<IFile> testScripts = getAffectedTestScripts(changedResources, instance);
			scanResources(testScripts, monitor);
		} else {
			// test script cache was not initialized yet
			scanWorkspace();
		}
	}

	protected Set<IFile> getAffectedTestScripts(Set<IResource> changedResources, TestConnectorPlugin instance) {

		Set<IFile> allTestScripts = instance.getTestScripts();
		// Create a copy of the set to allow modifications (removal of files
		// that are not affected by the resource changes).
		Set<IFile> testScripts = new LinkedHashSet<IFile>(allTestScripts);

		// Iterate over all test script files and check whether each file is
		// affected by one of the changed resources.
		Iterator<IFile> testScriptIterator = testScripts.iterator();
		while (testScriptIterator.hasNext()) {
			IFile testScript = (IFile) testScriptIterator.next();
			String testScriptPath = testScript.getFullPath().toString();

			// Check whether 'testScript' is affected (a file is affected if it
			// is located in a child path).
			boolean isAffectedByChange = false;
			for (IResource changedResource : changedResources) {
				String changedPath = changedResource.getFullPath().toString();
				if (testScriptPath.startsWith(changedPath)) {
					isAffectedByChange = true;
					break;
				}
			}

			// If the NatSpec script is not affected by any of the changes
			// (e.g., because it is located in a different folder), we can
			// remove the script from the set of scripts that must be
			// revalidated.
			if (!isAffectedByChange) {
				testScriptIterator.remove();
			}
		}

		return testScripts;
	}

	private void scanResources(Set<IFile> testScripts, IProgressMonitor monitor) {
		int size = testScripts.size();
		Set<IRevalidationVisitor> revalidationVisitors = getRevalidationVisitors();
		monitor.beginTask(JOB_NAME, size * revalidationVisitors.size());
		int index = 1;
		for (IRevalidationVisitor revalidationVisitor : revalidationVisitors) {
			for (IFile testScript : testScripts) {
				String taskName = "Revalidating " + testScript.getName() + " (" + index + "/" + size + ")";
				monitor.subTask(taskName);
				revalidationVisitor.visit(testScript);
				monitor.worked(1);
				index++;
			}
		}
		monitor.done();
	}

	private void scanWorkspace() {
		try {
			TestConnectorPlugin.logInfo("Starting workspace scan.", null);

			Set<IRevalidationVisitor> revalidationVisitors = getRevalidationVisitors();
			for (IRevalidationVisitor visitor : revalidationVisitors) {
				workspaceRoot.accept(visitor);
			}

			TestConnectorPlugin.logInfo("Workspace scan complete.", null);
			TestConnectorPlugin instance = TestConnectorPlugin.getInstance();
			if (instance != null) {
				instance.setTestScriptSetUpToDate();
			}
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while revalidating test scripts", e);
		}
	}

	private Set<IRevalidationVisitor> getRevalidationVisitors() {
		TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
		if (plugin == null) {
			return Collections.emptySet();
		}

		Set<IRevalidationVisitor> revalidationVisitors = plugin.getRevalidationVisitors();
		return revalidationVisitors;
	}
}
