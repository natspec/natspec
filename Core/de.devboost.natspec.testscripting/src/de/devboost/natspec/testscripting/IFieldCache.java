package de.devboost.natspec.testscripting;

import java.util.Collection;
import java.util.Map;

import org.eclipse.core.resources.IFile;

/**
 * An {@link IFieldCache} caches information about the fields contained in classes (e.g., their names and their types).
 * The cache was introduced to overcome performance issues when analyzing NatSpec template classes.
 * <p>
 * Information about the fields contained in such template classes is required to determine the patterns that are
 * available for NatSpec files the template applies to. To avoid multiple analysis of the same NatSpec template over and
 * over again, the cache provides this information. The cache is cleared selectively when classes are recompiled.
 */
public interface IFieldCache {

	/**
	 * Returns a map from all field names to the type of all fields declared in the class contained in the given file
	 * and all super classes (direct and indirect) of this class.
	 */
	public Map<String, String> getFieldNameToTypeMap(IFile templateFile);

	/**
	 * Returns the types of all fields declared in the class contained in the given file and all super classes (direct
	 * and indirect) of this class. This is a shortcut for {@link #getFieldNameToTypeMap(IFile)}.values().
	 */
	public Collection<String> getAllFieldTypes(IFile templateFile);

	/**
	 * Clears all data stored in the cache for the given file (i.e., the types of its fields and the file containing its
	 * super class).
	 */
	public void clearCache(IFile file);
}
