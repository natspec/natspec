/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.jface.action.IAction;

public class BbcodeOutlinePageLinkWithEditorAction extends de.devboost.language.bbcode.resource.bbcode.ui.AbstractBbcodeOutlinePageAction {
	
	public BbcodeOutlinePageLinkWithEditorAction(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Link with Editor", IAction.AS_CHECK_BOX);
		initialize("icons/link_with_editor_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewer().setLinkWithEditor(on);
	}
	
}
