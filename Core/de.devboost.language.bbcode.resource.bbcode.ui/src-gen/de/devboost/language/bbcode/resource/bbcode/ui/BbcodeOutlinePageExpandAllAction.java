/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.jface.action.IAction;

public class BbcodeOutlinePageExpandAllAction extends de.devboost.language.bbcode.resource.bbcode.ui.AbstractBbcodeOutlinePageAction {
	
	public BbcodeOutlinePageExpandAllAction(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Expand all", IAction.AS_PUSH_BUTTON);
		initialize("icons/expand_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().expandAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
