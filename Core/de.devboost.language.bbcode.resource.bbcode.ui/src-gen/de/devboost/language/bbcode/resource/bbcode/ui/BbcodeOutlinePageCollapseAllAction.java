/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.jface.action.IAction;

public class BbcodeOutlinePageCollapseAllAction extends de.devboost.language.bbcode.resource.bbcode.ui.AbstractBbcodeOutlinePageAction {
	
	public BbcodeOutlinePageCollapseAllAction(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Collapse all", IAction.AS_PUSH_BUTTON);
		initialize("icons/collapse_all_icon.gif");
	}
	
	public void runInternal(boolean on) {
		if (on) {
			getTreeViewer().collapseAll();
		}
	}
	
	public boolean keepState() {
		return false;
	}
	
}
