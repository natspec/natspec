/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.core.resources.IResource;

public class BbcodeUIMetaInformation extends de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeMetaInformation {
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeHoverTextProvider getHoverTextProvider() {
		return new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeHoverTextProvider();
	}
	
	public de.devboost.language.bbcode.resource.bbcode.ui.BbcodeImageProvider getImageProvider() {
		return de.devboost.language.bbcode.resource.bbcode.ui.BbcodeImageProvider.INSTANCE;
	}
	
	public de.devboost.language.bbcode.resource.bbcode.ui.BbcodeColorManager createColorManager() {
		return new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeColorManager();
	}
	
	/**
	 * @deprecated this method is only provided to preserve API compatibility. Use
	 * createTokenScanner(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResour
	 * ce, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeColorManager) instead.
	 */
	public de.devboost.language.bbcode.resource.bbcode.ui.BbcodeTokenScanner createTokenScanner(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeColorManager colorManager) {
		return (de.devboost.language.bbcode.resource.bbcode.ui.BbcodeTokenScanner) createTokenScanner(null, colorManager);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.ui.IBbcodeTokenScanner createTokenScanner(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeColorManager colorManager) {
		return new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeTokenScanner(resource, colorManager);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.ui.BbcodeCodeCompletionHelper createCodeCompletionHelper() {
		return new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeCodeCompletionHelper();
	}
	
	@SuppressWarnings("rawtypes")
	public Object createResourceAdapter(Object adaptableObject, Class adapterType, IResource resource) {
		return new de.devboost.language.bbcode.resource.bbcode.ui.debug.BbcodeLineBreakpointAdapter();
	}
	
}
