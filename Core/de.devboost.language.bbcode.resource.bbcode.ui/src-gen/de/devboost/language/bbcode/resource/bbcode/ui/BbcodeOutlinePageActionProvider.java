/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.action.IAction;

public class BbcodeOutlinePageActionProvider {
	
	public List<IAction> getActions(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageTreeViewer treeViewer) {
		// To add custom actions to the outline view, set the
		// 'overrideOutlinePageActionProvider' option to <code>false</code> and modify
		// this method.
		List<IAction> defaultActions = new ArrayList<IAction>();
		defaultActions.add(new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageCollapseAllAction(treeViewer));
		defaultActions.add(new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageExpandAllAction(treeViewer));
		defaultActions.add(new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageAutoExpandAction(treeViewer));
		defaultActions.add(new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageLexicalSortingAction(treeViewer));
		defaultActions.add(new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageTypeSortingAction(treeViewer));
		return defaultActions;
	}
	
}
