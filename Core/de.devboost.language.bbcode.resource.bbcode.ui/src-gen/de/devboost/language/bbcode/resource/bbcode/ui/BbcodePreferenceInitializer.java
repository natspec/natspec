/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import java.util.Collection;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * This class can be used to initialize default preference values.
 */
public class BbcodePreferenceInitializer extends AbstractPreferenceInitializer {
	
	public void initializeDefaultPreferences() {
		
		initializeDefaultSyntaxHighlighting();
		initializeDefaultBrackets();
		initializeDefaultsContentAssist();
		
		IPreferenceStore store = de.devboost.language.bbcode.resource.bbcode.ui.BbcodeUIPlugin.getDefault().getPreferenceStore();
		// Set default value for matching brackets
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_MATCHING_BRACKETS_COLOR, "192,192,192");
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_MATCHING_BRACKETS_CHECKBOX, true);
		
	}
	
	protected void initializeDefaultBrackets() {
		IPreferenceStore store = de.devboost.language.bbcode.resource.bbcode.ui.BbcodeUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultBrackets(store, new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeMetaInformation());
	}
	
	protected void initializeDefaultBrackets(IPreferenceStore store, de.devboost.language.bbcode.resource.bbcode.IBbcodeMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		// set default brackets
		de.devboost.language.bbcode.resource.bbcode.ui.BbcodeBracketSet bracketSet = new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeBracketSet();
		final Collection<de.devboost.language.bbcode.resource.bbcode.IBbcodeBracketPair> bracketPairs = metaInformation.getBracketPairs();
		if (bracketPairs != null) {
			for (de.devboost.language.bbcode.resource.bbcode.IBbcodeBracketPair bracketPair : bracketPairs) {
				bracketSet.addBracketPair(bracketPair.getOpeningBracket(), bracketPair.getClosingBracket(), bracketPair.isClosingEnabledInside(), bracketPair.isCloseAfterEnter());
			}
		}
		store.setDefault(languageId + de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_BRACKETS_SUFFIX, bracketSet.serialize());
	}
	
	public void initializeDefaultSyntaxHighlighting() {
		IPreferenceStore store = de.devboost.language.bbcode.resource.bbcode.ui.BbcodeUIPlugin.getDefault().getPreferenceStore();
		initializeDefaultSyntaxHighlighting(store, new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeMetaInformation());
	}
	
	protected void initializeDefaultSyntaxHighlighting(IPreferenceStore store, de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeMetaInformation metaInformation) {
		String languageId = metaInformation.getSyntaxName();
		String[] tokenNames = metaInformation.getSyntaxHighlightableTokenNames();
		if (tokenNames == null) {
			return;
		}
		for (int i = 0; i < tokenNames.length; i++) {
			String tokenName = tokenNames[i];
			de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle style = metaInformation.getDefaultTokenStyle(tokenName);
			if (style != null) {
				String color = getColorString(style.getColorAsRGB());
				setProperties(store, languageId, tokenName, color, style.isBold(), true, style.isItalic(), style.isStrikethrough(), style.isUnderline());
			} else {
				setProperties(store, languageId, tokenName, "0,0,0", false, false, false, false, false);
			}
		}
	}
	
	private void initializeDefaultsContentAssist() {
		IPreferenceStore store = de.devboost.language.bbcode.resource.bbcode.ui.BbcodeUIPlugin.getDefault().getPreferenceStore();
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_CONTENT_ASSIST_ENABLED, de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_CONTENT_ASSIST_ENABLED_DEFAULT);
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_CONTENT_ASSIST_DELAY, de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_CONTENT_ASSIST_DELAY_DEFAULT);
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_CONTENT_ASSIST_TRIGGERS, de.devboost.language.bbcode.resource.bbcode.ui.BbcodePreferenceConstants.EDITOR_CONTENT_ASSIST_TRIGGERS_DEFAULT);
	}
	
	protected void setProperties(IPreferenceStore store, String languageID, String tokenName, String color, boolean bold, boolean enable, boolean italic, boolean strikethrough, boolean underline) {
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.BOLD), bold);
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.COLOR), color);
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.ENABLE), enable);
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.ITALIC), italic);
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.STRIKETHROUGH), strikethrough);
		store.setDefault(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageID, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.UNDERLINE), underline);
	}
	
	protected String getColorString(int[] colorAsRGB) {
		if (colorAsRGB == null) {
			return "0,0,0";
		}
		if (colorAsRGB.length != 3) {
			return "0,0,0";
		}
		return colorAsRGB[0] + "," +colorAsRGB[1] + ","+ colorAsRGB[2];
	}
	
}

