/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.jface.text.rules.ITokenScanner;

public interface IBbcodeTokenScanner extends ITokenScanner {
	
	public String getTokenText();
	
}
