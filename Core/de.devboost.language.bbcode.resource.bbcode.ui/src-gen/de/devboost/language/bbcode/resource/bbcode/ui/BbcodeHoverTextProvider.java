/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.emf.ecore.EObject;

public class BbcodeHoverTextProvider implements de.devboost.language.bbcode.resource.bbcode.IBbcodeHoverTextProvider {
	
	private de.devboost.language.bbcode.resource.bbcode.ui.BbcodeDefaultHoverTextProvider defaultProvider = new de.devboost.language.bbcode.resource.bbcode.ui.BbcodeDefaultHoverTextProvider();
	
	public String getHoverText(EObject container, EObject referencedObject) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(referencedObject);
	}
	
	public String getHoverText(EObject object) {
		// Set option overrideHoverTextProvider to false and customize this method to
		// obtain custom hover texts.
		return defaultProvider.getHoverText(object);
	}
	
}
