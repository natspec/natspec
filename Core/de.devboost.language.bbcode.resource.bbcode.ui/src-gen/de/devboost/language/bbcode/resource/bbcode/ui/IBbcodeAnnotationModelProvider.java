/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.jface.text.source.IAnnotationModel;

/**
 * A provider for annotation models.
 */
public interface IBbcodeAnnotationModelProvider {
	
	/**
	 * Returns the annotation model.
	 */
	public IAnnotationModel getAnnotationModel();
	
}
