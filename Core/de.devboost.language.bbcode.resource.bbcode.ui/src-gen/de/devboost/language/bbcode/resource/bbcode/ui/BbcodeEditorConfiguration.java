/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;


/**
 * This class is deprecated and not used as of EMFText 1.4.1. The original
 * contents of this class have been moved to
 * de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSourceViewerConfiguration.
 * This class is only generated to avoid compile errors with existing versions of
 * this class.
 */
@Deprecated
public class BbcodeEditorConfiguration {
	
}
