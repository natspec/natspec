/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.jface.action.IAction;

public class BbcodeOutlinePageTypeSortingAction extends de.devboost.language.bbcode.resource.bbcode.ui.AbstractBbcodeOutlinePageAction {
	
	public BbcodeOutlinePageTypeSortingAction(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Group types", IAction.AS_CHECK_BOX);
		initialize("icons/group_types_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setGroupTypes(on);
		getTreeViewer().refresh();
	}
	
}
