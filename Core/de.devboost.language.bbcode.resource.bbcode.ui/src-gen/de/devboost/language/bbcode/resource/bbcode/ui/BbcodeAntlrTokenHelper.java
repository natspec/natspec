/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;


/**
 * This class is only generated for backwards compatiblity. The original contents
 * of this class have been moved to class
 * de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeAntlrTokenHelper.
 */
public class BbcodeAntlrTokenHelper {
	// This class is intentionally left empty.
}
