/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;


/**
 * An enumeration of all position categories.
 */
public enum BbcodePositionCategory {
	BRACKET, DEFINITION, PROXY;
}
