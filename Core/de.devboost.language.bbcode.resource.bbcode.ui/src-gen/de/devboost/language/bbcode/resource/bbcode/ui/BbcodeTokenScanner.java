/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

/**
 * An adapter from the Eclipse
 * <code>org.eclipse.jface.text.rules.ITokenScanner</code> interface to the
 * generated lexer.
 */
public class BbcodeTokenScanner implements de.devboost.language.bbcode.resource.bbcode.ui.IBbcodeTokenScanner {
	
	private de.devboost.language.bbcode.resource.bbcode.IBbcodeTextScanner lexer;
	private de.devboost.language.bbcode.resource.bbcode.IBbcodeTextToken currentToken;
	private List<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextToken> nextTokens;
	private int offset;
	private String languageId;
	private IPreferenceStore store;
	private de.devboost.language.bbcode.resource.bbcode.ui.BbcodeColorManager colorManager;
	private de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource;
	
	/**
	 * <p>
	 * Creates a new BbcodeTokenScanner.
	 * </p>
	 * 
	 * @param resource The resource to scan
	 * @param colorManager A manager to obtain color objects
	 */
	public BbcodeTokenScanner(de.devboost.language.bbcode.resource.bbcode.IBbcodeTextResource resource, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeColorManager colorManager) {
		this.resource = resource;
		this.colorManager = colorManager;
		this.lexer = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeMetaInformation().createLexer();
		this.languageId = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeMetaInformation().getSyntaxName();
		de.devboost.language.bbcode.resource.bbcode.ui.BbcodeUIPlugin plugin = de.devboost.language.bbcode.resource.bbcode.ui.BbcodeUIPlugin.getDefault();
		if (plugin != null) {
			this.store = plugin.getPreferenceStore();
		}
		this.nextTokens = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextToken>();
	}
	
	public int getTokenLength() {
		return currentToken.getLength();
	}
	
	public int getTokenOffset() {
		return offset + currentToken.getOffset();
	}
	
	public IToken nextToken() {
		boolean isOriginalToken = true;
		if (!nextTokens.isEmpty()) {
			currentToken = nextTokens.remove(0);
			isOriginalToken = false;
		} else {
			currentToken = lexer.getNextToken();
		}
		if (currentToken == null || !currentToken.canBeUsedForSyntaxHighlighting()) {
			return Token.EOF;
		}
		
		if (isOriginalToken) {
			splitCurrentToken();
		}
		
		TextAttribute textAttribute = null;
		String tokenName = currentToken.getName();
		if (tokenName != null) {
			de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle staticStyle = getStaticTokenStyle();
			// now call dynamic token styler to allow to apply modifications to the static
			// style
			de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle dynamicStyle = getDynamicTokenStyle(staticStyle);
			if (dynamicStyle != null) {
				textAttribute = getTextAttribute(dynamicStyle);
			}
		}
		
		return new Token(textAttribute);
	}
	
	public void setRange(IDocument document, int offset, int length) {
		this.offset = offset;
		try {
			lexer.setText(document.get(offset, length));
		} catch (BadLocationException e) {
			// ignore this error. It might occur during editing when locations are outdated
			// quickly.
		}
	}
	
	public String getTokenText() {
		return currentToken.getText();
	}
	
	public int[] convertToIntArray(RGB rgb) {
		if (rgb == null) {
			return null;
		}
		return new int[] {rgb.red, rgb.green, rgb.blue};
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle getStaticTokenStyle() {
		String tokenName = currentToken.getName();
		String enableKey = de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.ENABLE);
		if (store == null) {
			return null;
		}
		
		boolean enabled = store.getBoolean(enableKey);
		if (!enabled) {
			return null;
		}
		
		String colorKey = de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.COLOR);
		RGB foregroundRGB = PreferenceConverter.getColor(store, colorKey);
		RGB backgroundRGB = null;
		boolean bold = store.getBoolean(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.BOLD));
		boolean italic = store.getBoolean(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.ITALIC));
		boolean strikethrough = store.getBoolean(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.STRIKETHROUGH));
		boolean underline = store.getBoolean(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.getPreferenceKey(languageId, tokenName, de.devboost.language.bbcode.resource.bbcode.ui.BbcodeSyntaxColoringHelper.StyleProperty.UNDERLINE));
		return new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenStyle(convertToIntArray(foregroundRGB), convertToIntArray(backgroundRGB), bold, italic, strikethrough, underline);
	}
	
	public de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle getDynamicTokenStyle(de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle staticStyle) {
		de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeDynamicTokenStyler dynamicTokenStyler = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeDynamicTokenStyler();
		dynamicTokenStyler.setOffset(offset);
		de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle dynamicStyle = dynamicTokenStyler.getDynamicTokenStyle(resource, currentToken, staticStyle);
		return dynamicStyle;
	}
	
	public TextAttribute getTextAttribute(de.devboost.language.bbcode.resource.bbcode.IBbcodeTokenStyle tokeStyle) {
		int[] foregroundColorArray = tokeStyle.getColorAsRGB();
		Color foregroundColor = null;
		if (colorManager != null) {
			foregroundColor = colorManager.getColor(new RGB(foregroundColorArray[0], foregroundColorArray[1], foregroundColorArray[2]));
		}
		int[] backgroundColorArray = tokeStyle.getBackgroundColorAsRGB();
		Color backgroundColor = null;
		if (backgroundColorArray != null) {
			RGB backgroundRGB = new RGB(backgroundColorArray[0], backgroundColorArray[1], backgroundColorArray[2]);
			if (colorManager != null) {
				backgroundColor = colorManager.getColor(backgroundRGB);
			}
		}
		int style = SWT.NORMAL;
		if (tokeStyle.isBold()) {
			style = style | SWT.BOLD;
		}
		if (tokeStyle.isItalic()) {
			style = style | SWT.ITALIC;
		}
		if (tokeStyle.isStrikethrough()) {
			style = style | TextAttribute.STRIKETHROUGH;
		}
		if (tokeStyle.isUnderline()) {
			style = style | TextAttribute.UNDERLINE;
		}
		return new TextAttribute(foregroundColor, backgroundColor, style);
	}
	
	/**
	 * Tries to split the current token if it contains task items.
	 */
	public void splitCurrentToken() {
		final String text = currentToken.getText();
		final String name = currentToken.getName();
		final int line = currentToken.getLine();
		final int charStart = currentToken.getOffset();
		final int column = currentToken.getColumn();
		
		List<de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTaskItem> taskItems = new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTaskItemDetector().findTaskItems(text, line, charStart);
		
		// this is the offset for the next token to be added
		int offset = charStart;
		int itemBeginRelative;
		List<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextToken> newItems = new ArrayList<de.devboost.language.bbcode.resource.bbcode.IBbcodeTextToken>();
		for (de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTaskItem taskItem : taskItems) {
			int itemBegin = taskItem.getCharStart();
			int itemLine = taskItem.getLine();
			int itemColumn = 0;
			
			itemBeginRelative = itemBegin - charStart;
			// create token before task item
			String textBefore = text.substring(offset - charStart, itemBeginRelative);
			int textBeforeLength = textBefore.length();
			newItems.add(new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTextToken(name, textBefore, offset, textBeforeLength, line, column, true));
			
			// create token for the task item itself
			offset = offset + textBeforeLength;
			String itemText = taskItem.getKeyword();
			int itemTextLength = itemText.length();
			newItems.add(new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTextToken(de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTokenStyleInformationProvider.TASK_ITEM_TOKEN_NAME, itemText, offset, itemTextLength, itemLine, itemColumn, true));
			
			offset = offset + itemTextLength;
		}
		
		if (!taskItems.isEmpty()) {
			// create token after last task item
			String textAfter = text.substring(offset - charStart);
			newItems.add(new de.devboost.language.bbcode.resource.bbcode.mopp.BbcodeTextToken(name, textAfter, offset, textAfter.length(), line, column, true));
		}
		
		if (!newItems.isEmpty()) {
			// replace tokens
			currentToken = newItems.remove(0);
			nextTokens = newItems;
		}
		
	}
}
