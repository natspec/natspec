/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import org.eclipse.jface.action.IAction;

public class BbcodeOutlinePageLexicalSortingAction extends de.devboost.language.bbcode.resource.bbcode.ui.AbstractBbcodeOutlinePageAction {
	
	public BbcodeOutlinePageLexicalSortingAction(de.devboost.language.bbcode.resource.bbcode.ui.BbcodeOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Sort alphabetically", IAction.AS_CHECK_BOX);
		initialize("icons/sort_lexically_icon.gif");
	}
	
	public void runInternal(boolean on) {
		getTreeViewerComparator().setSortLexically(on);
		getTreeViewer().refresh();
	}
	
}
