/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;


/**
 * A provider for BracketHandler objects.
 */
public interface IBbcodeBracketHandlerProvider {
	
	/**
	 * Returns the bracket handler.
	 */
	public de.devboost.language.bbcode.resource.bbcode.ui.IBbcodeBracketHandler getBracketHandler();
	
}
