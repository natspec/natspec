/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.language.bbcode.resource.bbcode.ui;

import java.util.List;

/**
 * A class which can be overridden to customize code completion proposals.
 */
public class BbcodeProposalPostProcessor {
	
	public List<de.devboost.language.bbcode.resource.bbcode.ui.BbcodeCompletionProposal> process(List<de.devboost.language.bbcode.resource.bbcode.ui.BbcodeCompletionProposal> proposals) {
		// the default implementation does returns the proposals as they are
		return proposals;
	}
	
}
