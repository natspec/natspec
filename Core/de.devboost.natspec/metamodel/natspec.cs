@SuppressWarnings(explicitSyntaxChoice,noRuleForMetaClass,tokenOverlapping)
SYNTAXDEF natspec
FOR <http://www.devboost.de/natspec>
START Document

OPTIONS {
	usePredefinedTokens = "false";
	overrideResourcePostProcessor = "false";
	overrideDynamicTokenStyler = "false";
	overrideBuilder = "false";
	overrideUIMetaInformation = "false";
	overrideUIDotClasspath = "false";
	overrideUIBuildProperties = "false";
	overrideUIPluginXML = "false";
	overridePluginXML = "false";
	overrideOccurrence = "false";
	overrideQuickAssistProcessor = "false";
	overrideMarkerAnnotation = "false";
	overrideHyperlinkDetector = "false";
	overrideCodeCompletionHelper = "false";
	overrideManifest = "false";
	overrideUIManifest = "false";
	overrideTaskItemDetector = "false";
	overrideOutlinePageActionProvider = "false";
	overrideHoverTextProvider = "false";
	
	disableLaunchSupport = "true";
	EMFTargetVersion = "2.9";
}

TOKENS {
	DEFINE LINEBREAK  $('\r'|'\n')+$;
	DEFINE TEXT  $(~('\r'|'\n'))+$;
}

RULES {
	Document ::= contents*;
	
	@SuppressWarnings(featureWithoutSyntax)
	Sentence ::= text[TEXT];
}
