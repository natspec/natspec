package de.devboost.natspec.matching;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.Word;

/**
 * {@link AbstractSyntaxPatternPartMatch} is the base implementation for all
 * classes that implement {@link ISyntaxPatternPartMatch}. This default 
 * implementation provides functionality to store the list of matched words. It
 * also assumes that all created matches are complete. Subclasses may override
 * this behavior.
 */
public abstract class AbstractSyntaxPatternPartMatch implements ISyntaxPatternPartMatch {

	private List<Word> matchedWords;
	
	public AbstractSyntaxPatternPartMatch(List<Word> matchedWords) {
		super();
		// Create a copy of the word list to make sure this list is not altered
		// outside this class later on.
		this.matchedWords = Collections.unmodifiableList(matchedWords);
	}

	/**
	 * Returns an unmodifiable list of words that were matched to this pattern
	 * part.
	 */
	@Override
	public List<Word> getMatchedWords() {
		return matchedWords;
	}
	
	protected void setMatchedWords(List<Word> matchedWords) {
		this.matchedWords = Collections.unmodifiableList(matchedWords);
	}

	@Override
	public List<Word> getOptionalMatchedWords() {
		return Collections.emptyList();
	}

	@Override
	public void removeOptionalMatchedWords(List<Word> words) {
		throw new RuntimeException("No optional words.");
	}

	@Override
	public boolean hasMatched() {
		return true;
	}
}
