package de.devboost.natspec.matching;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;

/**
 * The {@link MultipleMatchPrioritizer} can be used to prioritize all complete matches after the count of StaticWords.
 * If a sentence contains more StaticWords it is considered to be more specific.
 */
public class MultipleMatchPrioritizer {
	
	public static final MultipleMatchPrioritizer INSTANCE = new MultipleMatchPrioritizer();

	private MultipleMatchPrioritizer() {
		super();
	}

	public void prioritize(List<ISyntaxPatternMatch<? extends Object>> completeMatches) {

		LinkedHashMap<ISyntaxPatternMatch<? extends Object>, Integer> matchToStaticWordCountMap = new LinkedHashMap<ISyntaxPatternMatch<? extends Object>, Integer>();
		int countMax = 0;
		for (ISyntaxPatternMatch<? extends Object> match : completeMatches) {
			int countNew = countStaticWords(match);
			matchToStaticWordCountMap.put(match, countNew);
			if (countNew > countMax) {
				countMax = countNew;
			}
		}
		
		for (Entry<ISyntaxPatternMatch<? extends Object>, Integer> entry : matchToStaticWordCountMap.entrySet()) {
			if (entry.getValue() < countMax) {
				ISyntaxPatternMatch<? extends Object> match = entry.getKey();
				completeMatches.remove(match);
			}
		}
	}

	public int countStaticWords(ISyntaxPatternMatch<?> match) {
		int count = 0;
		
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
		Set<ISyntaxPatternPart> parts = partsToMatchesMap.keySet();
		for (ISyntaxPatternPart part : parts) {
			if (part instanceof StaticWord) {
				count++;
			}
		}
		
		return count;
	}
}
