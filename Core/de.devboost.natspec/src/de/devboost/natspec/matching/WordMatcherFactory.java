package de.devboost.natspec.matching;

import de.devboost.natspec.internal.matching.WordMatcher;

public class WordMatcherFactory {
	
	public final static WordMatcherFactory INSTANCE = new WordMatcherFactory();
	
	private WordMatcherFactory() {
		super();
	}

	public IWordMatcher createWordMatcher(String text) {
		return new WordMatcher(text);
	}
}
