package de.devboost.natspec.matching;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.patterns.ISyntaxPattern;

/**
 * An {@link IPatternMatchContext} is used while matching patterns against a NatSpec document. The context keeps a list
 * of objects which are created by sentences.
 */
public interface IPatternMatchContext {

	public URI getContextURI();

	public ISynonymProvider getLocalSynonymProvider();

	/**
	 * Returns all objects that were added to the context by interpreting previous sentences.
	 */
	public List<Object> getObjectsInContext();

	/**
	 * Adds an object to this context.
	 */
	public void addObjectToContext(Object object);

	/**
	 * Adds the given objects to this context.
	 */
	public void addObjectsToContext(List<Object> objects);
	
	/**
	 * Returns all patterns in this context.
	 * 
	 * @return all patterns registered and active in the context
	 */
	public Collection<ISyntaxPattern<? extends Object>> getPatternsInContext();

	/**
	 * Returns all variable names that are already used in the same context.
	 */
	public Set<String> getUsedVariableNames();

	/**
	 * Returns the last object that was added to this context, having a type that is compatible with the given types.
	 * 
	 * @param qualifiedTypeName
	 *            the fully qualified name of the required object type
	 * @return a matching object or <code>null</code> if no matching object was found
	 */
	public Object getLastObjectByType(String qualifiedTypeName);

	/**
	 * Returns the name of the field in the template that has the given fully qualified type or <code>null</code> if no
	 * such field is available.
	 * 
	 * @param qualifiedTypeName
	 *            the type of the requested field
	 * @return a field name or <code>null</code> if field was not found
	 */
	public String getTemplateFieldByType(String qualifiedTypeName);

	/**
	 * Sets the mapping from fields contained in the template to their type.
	 * 
	 * @param templateFieldMap
	 *            a map which maps field names to a fully qualified type name
	 */
	public void setTemplateFieldNameToTypeMap(Map<String, String> templateFieldMap);

	/**
	 * Returns <code>true</code> if the given object is the last one that was added to this context.
	 * 
	 * @param object
	 *            the object to check
	 * @return <code>true</code> if the object was the last to be added, otherwise <code>false</code>
	 */
	public boolean isLast(Object object);

	public Map<String, String> getTemplateFieldTypeToNameMap();
	
	/**
	 * Returns <code>true</code> if matching is performed to determine code completion proposals. If this is the case,
	 * different matching rules apply (e.g., partial words match).
	 */
	public boolean matchFuzzy();
}
