package de.devboost.natspec.matching;

/**
 * A {@link IWordMatcher} can be used to determined whether a word matches a 
 * given text.
 */
public interface IWordMatcher {

	/**
	 * Checks whether this word matches against the given text in a specific
	 * context. The context is required to determine the set of synonyms that
	 * must be considered while matching the texts.
	 * 
	 * <b>Attention: 'otherText' must be in lower case!</b>
	 */
	public boolean match(IPatternMatchContext context, String textToMatchAgainst);
}
