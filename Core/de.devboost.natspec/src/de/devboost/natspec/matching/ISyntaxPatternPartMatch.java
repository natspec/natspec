package de.devboost.natspec.matching;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * An {@link ISyntaxPatternPartMatch} is a match between a 
 * {@link ISyntaxPatternPart} and a corresponding piece of text (i.e., a list of 
 * words). An {@link ISyntaxPatternMatch} can be partial, which is indicated by
 * returning <code>false</code> when {@link #hasMatched()} is called.
 */
public interface ISyntaxPatternPartMatch {

	/**
	 * Returns a list of words that were matched to the 
	 * {@link ISyntaxPatternPart} that corresponds to this match.
	 */
	public List<Word> getMatchedWords();

	/**
	 * Returns a list of words that were matched to the 
	 * {@link ISyntaxPatternPart}, but which could also be removed and the
	 * match would still be valid.
	 */
	public List<Word> getOptionalMatchedWords();
	
	/**
	 * Removes words that were matches, but were optional.
	 */
	public void removeOptionalMatchedWords(List<Word> words);
	
	/**
	 * Returns <code>true</code> if this match is complete, otherwise false.
	 */
	public boolean hasMatched();
}
