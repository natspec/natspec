package de.devboost.natspec.matching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.Word;
import de.devboost.natspec.internal.parsing.SentenceParser;
import de.devboost.natspec.parsing.ISentenceParser;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DefaultSentenceMatchParameter;
import de.devboost.natspec.registries.SyntaxPatternRegistry;

/**
 * The {@link MatchService} can used to match individual sentences or whole documents against registered syntax patterns
 * (see {@link SyntaxPatternRegistry}).
 */
// TODO hide this behind some interface and move implementation to an internal package?
public class MatchService {

	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;

	public MatchService(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	/**
	 * Tries to match all sentences contained in the given resource against all registered syntax patterns.
	 * 
	 * @return a list of all complete matches (appearing in the order of the sentences in the resource)
	 */
	public List<ISyntaxPatternMatch<? extends Object>> match(Resource resource, IPatternMatchContext context) {
		return match(resource, context, false, true);
	}

	/**
	 * Tries to match the given sentence against all registered syntax patterns.
	 * 
	 * @return a list of all complete matches. there can be multiple matches if multiple patterns match the sentence.
	 */
	public List<ISyntaxPatternMatch<? extends Object>> match(Sentence sentence, IPatternMatchContext context) {
		return match(sentence, context, false, true, usePrioritizer);
	}

	/**
	 * Tries to match all sentences contained in the given resource against all registered syntax patterns.
	 * 
	 * @param resource
	 *            the resource containing the sentences to match
	 * 
	 * @param context
	 *            the context that must be used for matching
	 * 
	 * @param includeIncompleteMatches
	 *            if <code>true</code>, incomplete matches are returned in addition to the complete ones
	 * 
	 * @param filterObsoleteDefaultMatches
	 *            if <code>true</code>, matches using the full default sentence match are removed from the result list
	 *            if there is another complete match
	 * 
	 * @return a list of matches (appearing in the order of the sentences in the resource)
	 */
	public List<ISyntaxPatternMatch<?>> match(Resource resource, IPatternMatchContext context,
			boolean includeIncompleteMatches, boolean filterObsoleteDefaultMatches) {

		List<ISyntaxPatternMatch<? extends Object>> matches = new ArrayList<ISyntaxPatternMatch<? extends Object>>();

		// iterate over the whole resource to find sentences
		Iterator<EObject> allContents = resource.getAllContents();
		while (allContents.hasNext()) {
			EObject eObject = (EObject) allContents.next();
			if (eObject instanceof Sentence) {
				Sentence sentence = (Sentence) eObject;
				// TODO This code can also be found in the NatspecValidator
				// where in addition to skipping comments, empty lines are
				// ignored. We must merge this code to avoid duplication.
				if (SentenceUtil.INSTANCE.isComment(sentence)) {
					continue;
				}
				List<ISyntaxPatternMatch<?>> matchesForSentence = match(sentence, context, includeIncompleteMatches,
						filterObsoleteDefaultMatches, usePrioritizer);
				matches.addAll(matchesForSentence);
			}
		}
		return matches;
	}

	/**
	 * Tries to match the given sentence against all registered syntax patterns.
	 * 
	 * @param sentence
	 *            the sentences to match
	 * 
	 * @param context
	 *            the context that must be used for matching
	 * 
	 * @param includeIncompleteMatches
	 *            if <code>true</code>, incomplete matches are returned in addition to the complete ones
	 * 
	 * @param filterObsoleteDefaultMatches
	 *            if <code>true</code>, matches using the full default sentence match are removed from the result list
	 *            if there is another complete match
	 * 
	 * @return a list of matches
	 */
	public List<ISyntaxPatternMatch<?>> match(Sentence sentence, IPatternMatchContext context,
			boolean includeIncompleteMatches, boolean filterObsoleteDefaultMatches, boolean prioritizeMatches) {

		Collection<ISyntaxPattern<? extends Object>> patterns = context.getPatternsInContext();

		return match(sentence, context, includeIncompleteMatches, filterObsoleteDefaultMatches, prioritizeMatches,
				patterns);
	}

	/**
	 * Tries to match the given sentence against a collection of syntax patterns.
	 * 
	 * @param sentence
	 *            the sentences to match
	 * 
	 * @param context
	 *            the context that must be used for matching
	 * 
	 * @param includeIncompleteMatches
	 *            if <code>true</code>, incomplete matches are returned in addition to the complete ones
	 * 
	 * @param filterObsoleteDefaultMatches
	 *            if <code>true</code>, matches using the full default sentence match are removed from the result list
	 *            if there is another complete match
	 * 
	 * @param patterns
	 *            the syntax patterns to match against
	 * 
	 * @return a list of matches
	 */
	public List<ISyntaxPatternMatch<?>> match(Sentence sentence, IPatternMatchContext context,
			boolean includeIncompleteMatches, boolean filterObsoleteDefaultMatches, boolean prioritizeMatches,
			Collection<ISyntaxPattern<?>> patterns) {

		ISentenceParser parser = new SentenceParser();
		List<Word> words = parser.split(sentence);

		ISyntaxPatternMatcher matcher = SyntaxPatternMatcherFactory.INSTANCE
				.createSyntaxPatternMatcher(useOptimizedMatcher);
		List<ISyntaxPatternMatch<? extends Object>> matches = matcher.match(words, patterns, context,
				includeIncompleteMatches);

		if (filterObsoleteDefaultMatches) {
			matches = filterObsoleteDefaultMatches(includeIncompleteMatches, filterObsoleteDefaultMatches, matches);
		}
		if (prioritizeMatches) {
			prioritizeMatches(includeIncompleteMatches, matches);
		}

		List<Object> cachedMatches = sentence.getMatches();
		cachedMatches.clear();
		cachedMatches.addAll(matches);
		return matches;
	}

	protected void prioritizeMatches(boolean includeIncompleteMatches,
			List<ISyntaxPatternMatch<? extends Object>> matches) {

		boolean isMultipleMatch = getCompleteMatches(includeIncompleteMatches, matches).size() > 1;
		if (isMultipleMatch) {
			MultipleMatchPrioritizer.INSTANCE.prioritize(matches);
		}
	}

	protected List<ISyntaxPatternMatch<? extends Object>> filterObsoleteDefaultMatches(boolean includeIncompleteMatches,
			boolean filterObsoleteDefaultMatches, List<ISyntaxPatternMatch<? extends Object>> matches) {

		boolean isMultipleMatch = getCompleteMatches(includeIncompleteMatches, matches).size() > 1;
		if (isMultipleMatch) {
			// We only consider complete matches here since otherwise the
			// default pattern would not be obsolete
			if (filterObsoleteDefaultMatches) {
				matches = filterObsoleteDefaultMatches(matches);
			}
		}
		return matches;
	}

	protected List<ISyntaxPatternMatch<? extends Object>> getCompleteMatches(boolean includeIncompleteMatches,
			List<ISyntaxPatternMatch<? extends Object>> matches) {
		List<ISyntaxPatternMatch<? extends Object>> completeMatches = new ArrayList<ISyntaxPatternMatch<? extends Object>>();

		if (includeIncompleteMatches) {
			completeMatches = filterCompleteMatches(matches);
		} else {
			completeMatches.addAll(matches);
		}
		return completeMatches;
	}

	private List<ISyntaxPatternMatch<? extends Object>> filterObsoleteDefaultMatches(
			List<ISyntaxPatternMatch<? extends Object>> matches) {

		List<ISyntaxPatternMatch<? extends Object>> filtered = new ArrayList<ISyntaxPatternMatch<? extends Object>>();
		for (ISyntaxPatternMatch<? extends Object> match : matches) {
			if (isDefaultSentencePatternMatch(match)) {
				continue;
			}
			filtered.add(match);
		}

		// We only consider default matches as obsolete as long as
		// there is another complete match
		if (!filtered.isEmpty()) {
			return filtered;
		}
		return matches;
	}

	/**
	 * Returns a list containing only the complete matches contained in the given list of matches.
	 */
	public List<ISyntaxPatternMatch<? extends Object>> filterCompleteMatches(
			List<ISyntaxPatternMatch<? extends Object>> matches) {

		List<ISyntaxPatternMatch<? extends Object>> completeMatches = new ArrayList<ISyntaxPatternMatch<? extends Object>>();
		for (ISyntaxPatternMatch<? extends Object> match : matches) {
			if (match.isComplete()) {
				completeMatches.add(match);
			}
		}
		return completeMatches;
	}

	/**
	 * Returns whether the given match is a match with the default syntax pattern that captures all sentences that were
	 * not matched by a more concrete pattern.
	 * 
	 * @param match
	 *            the match to be tested for being a match against the default syntax pattern
	 * 
	 * @return true if the match is a match against the default pattern
	 */
	public boolean isDefaultSentencePatternMatch(ISyntaxPatternMatch<? extends Object> match) {

		ISyntaxPattern<? extends Object> pattern = match.getPattern();
		return isDefaultSentencePattern(pattern);
	}

	private boolean isDefaultSentencePattern(ISyntaxPattern<? extends Object> pattern) {

		List<ISyntaxPatternPart> parts = pattern.getParts();
		if (parts == null) {
			return false;
		}

		int partsSize = parts.size();
		if (partsSize < 1) {
			return false;
		}

		for (int i = 0; i < partsSize; i++) {
			ISyntaxPatternPart syntaxPatternPart = parts.get(i);
			if (syntaxPatternPart instanceof DefaultSentenceMatchParameter) {
				return true;
			}
		}

		return false;
	}
}
