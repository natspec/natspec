package de.devboost.natspec.matching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.Word;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * The {@link ExpectationHelper} class can be used to determine the most 
 * probable pattern for a list of partial pattern matches. It assumes that the
 * longest match corresponds to the pattern the user tried to write. If multiple
 * matches have the same length, multiple expectations are returned.
 */
// TODO move to internal package?
public class ExpectationHelper {

	public List<ISyntaxPatternPart> getExpectedParts(ISyntaxPatternMatch<?> match) {
		if (match == null) {
			return Collections.emptyList();
		}
		// create list with one element
		List<ISyntaxPatternMatch<?>> matches = new ArrayList<ISyntaxPatternMatch<?>>(1);
		matches.add(match);
		return getExpectedParts(matches);
	}

	public List<ISyntaxPatternPart> getExpectedParts(List<ISyntaxPatternMatch<?>> matches) {
		List<ISyntaxPatternMatch<?>> longestMatches = getLongestMatches(matches);
		if (longestMatches.isEmpty()) {
			return Collections.emptyList();
		}
		
		List<ISyntaxPatternPart> expectedParts = new ArrayList<ISyntaxPatternPart>();
		
		ISyntaxPatternMatch<?> firstMatch = longestMatches.get(0);
		int length = getMatchedPartCount(firstMatch);
		for (ISyntaxPatternMatch<?> match : longestMatches) {
			List<ISyntaxPatternPart> parts = match.getPattern().getParts();
			// get part after the last part that matched
			if (parts.size() > length) {
				ISyntaxPatternPart part = parts.get(length);
				expectedParts.add(part);
			}
		}
		return expectedParts;
	}

	public List<ISyntaxPatternMatch<?>> getLongestMatches(List<ISyntaxPatternMatch<?>> matches) {
		List<ISyntaxPatternMatch<?>> longestMatches = new ArrayList<ISyntaxPatternMatch<?>>();

		int max = -1;
		for (ISyntaxPatternMatch<?> match : matches) {
			int matchLength = getLength(match);
			if (matchLength > max) {
				longestMatches.clear();
				max = matchLength;
			}
			if (matchLength >= max) {
				longestMatches.add(match);
			}
		}
		
		return longestMatches;
	}

	/**
	 * Returns the number of parts that were matched in the given match. This is
	 * different from {@link #getLength(ISyntaxPatternMatch)} which returns the
	 * number of words that were matched.
	 */
	private int getMatchedPartCount(ISyntaxPatternMatch<?> match) {
		int count = 0;
		
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
		Collection<ISyntaxPatternPartMatch> partMatches = partsToMatchesMap.values();
		for (ISyntaxPatternPartMatch partMatch : partMatches) {
			if (partMatch.hasMatched()) {
				count++;
			}
		}
		
		return count;
	}

	/**
	 * Returns the length of the given match (i.e., the number of words that
	 * were matched).
	 */
	private int getLength(ISyntaxPatternMatch<?> match) {
		int length = 0;
		
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
		Collection<ISyntaxPatternPartMatch> partMatches = partsToMatchesMap.values();
		for (ISyntaxPatternPartMatch partMatch : partMatches) {
			List<Word> matchedWords = partMatch.getMatchedWords();
			length += matchedWords.size();
		}
		
		return length;
	}
}
