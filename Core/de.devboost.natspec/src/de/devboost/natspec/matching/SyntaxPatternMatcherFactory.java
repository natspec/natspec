package de.devboost.natspec.matching;

import de.devboost.natspec.internal.matching.basic.BasicSyntaxPatternMatcher;
import de.devboost.natspec.internal.matching.tree.TreeBasedSyntaxPatternMatcher;

public class SyntaxPatternMatcherFactory {
	
	public final static SyntaxPatternMatcherFactory INSTANCE = new SyntaxPatternMatcherFactory();
	
	private static final TreeBasedSyntaxPatternMatcher TREE_BASED_SYNTAX_PATTERN_MATCHER = new TreeBasedSyntaxPatternMatcher();

	private SyntaxPatternMatcherFactory() {
		super();
	}

	public ISyntaxPatternMatcher createSyntaxPatternMatcher(boolean useOptimizedMatcher) {
		if (useOptimizedMatcher) {
			return TREE_BASED_SYNTAX_PATTERN_MATCHER;
		} else {
			return new BasicSyntaxPatternMatcher();
		}
	}
}
