package de.devboost.natspec.matching;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.Word;

//TODO move to internal package?
public class WordUtil {

	public static final WordUtil INSTANCE = new WordUtil();

	private WordUtil() {
		super();
	}

	/**
	 * Converts the list of words to a list of strings containing the text
	 * values of the words.
	 */
	public List<String> toStringList(List<Word> words) {
		int size = words.size();
		List<String> texts = new ArrayList<String>(size);
		for (int i = 0; i < size; i++) {
			Word word = words.get(i);
			String text = word.getText();
			texts.add(text);
		}
		return texts;
	}

	/**
	 * Converts the list of words to a string containing the text values of the
	 * words.
	 */
	public String toString(List<Word> words) {
		StringBuilder sentenceText = new StringBuilder();
		// Construct comment from the list of matched words
		int wordCount = words.size();
		int wordCountMinusOne = wordCount - 1;
		for (int i = 0; i < wordCount; i++) {
			Word word = words.get(i);
			String text = word.getText();
			sentenceText.append(text);
			// Add whitespace if this is not the last word
			if (i < wordCountMinusOne) {
				sentenceText.append(" ");
			}
		}
		return sentenceText.toString();
	}
}
