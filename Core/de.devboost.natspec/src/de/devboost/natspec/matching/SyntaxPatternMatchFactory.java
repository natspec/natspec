package de.devboost.natspec.matching;

import de.devboost.natspec.internal.matching.basic.BasicSyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;

public class SyntaxPatternMatchFactory {

	public <UserDataType> BasicSyntaxPatternMatch<UserDataType> createSyntaxPatternMatch(
			ISyntaxPattern<UserDataType> pattern, IPatternMatchContext context) {

		return new BasicSyntaxPatternMatch<UserDataType>(pattern, context);
	}
}
