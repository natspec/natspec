package de.devboost.natspec.matching;

import java.util.List;
import java.util.Map;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.Word;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * An {@link ISyntaxPatternMatch} is a potentially partial match between a 
 * {@link Sentence} and a syntax pattern ({@link ISyntaxPattern}).
 * 
 * The life cycle of a ISyntaxPatternMatch is as follows:
 * <ol>
 * <li>{@link #match(List)} is called</li>
 * <li>{@link #isComplete()} is called to check whether all words were matched</li>
 * <li>if {@link #isComplete()} returned <code>true</code>, {@link #createUserData()} is 
 * invoked to allow the match to create a custom object that contains further
 * information about itself. These objects can be used later on to process the
 * match (e.g., to generate code for it).</li>
 * </ul>
 * <li>after {@link #createUserData()} was invoked, clients may call 
 * {@link #getUserData()}</li>
 * <li>if {@link #isComplete()} returned <code>true</code>, {@link #getPartsToMatchesMap()}
 * may be called by clients to retrieve more specific data about this match.</li>
 * </ol>
 * 
 * The methods {@link #getPattern()} and {@link #getContext()} can be invoked at
 * any time. Implementations of this interface should initialize these two
 * properties in their constructors.
 */
public interface ISyntaxPatternMatch<UserDataType> {

	/**
	 * Returns the patterns this match corresponds to.
	 */
	public ISyntaxPattern<UserDataType> getPattern();
	
	/**
	 * Returns the context in which this match was found.
	 */
	public IPatternMatchContext getContext();
	
	/**
	 * Tries to match the given list of words to the pattern corresponding to
	 * this match. If all words required for the pattern are found, the match
	 * is considers as complete (see {@link #isComplete()}, otherwise it is
	 * incomplete. 
	 */
	public void match(List<Word> wordList);
	
	/**
	 * Returns <code>true</code> if all words required for the pattern 
	 * associated with this match were found during the call of 
	 * {@link #match(List)}.
	 */
	public boolean isComplete();
	
	/**
	 * Create a custom object that provides specific information about this
	 * match.
	 */
	public void createUserData();
	
	/**
	 * Returns a custom user object providing specific data about this match.
	 * This method must be called after {@link #createUserData()}.
	 */
	// TODO do we really need {@link #createUserData()} and {@link #getUserData()}?
	// Maybe creating the user data object on the first call to {@link #getUserData()}
	// is sufficient?
	public UserDataType getUserData();
	
	/**
	 * Returns a map that provides information about how parts of the syntax
	 * pattern were matched to text fragments of sentences.
	 */
	public Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> getPartsToMatchesMap();

	public void reset();

	/**
	 * Returns the words of the sentence that was matched. If this is a partial match, <code>null</code> is returned.
	 */
	public List<Word> getWords();
}
