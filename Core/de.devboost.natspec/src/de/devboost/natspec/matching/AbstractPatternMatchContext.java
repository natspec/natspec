package de.devboost.natspec.matching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.registries.SyntaxPatternRegistry;

public abstract class AbstractPatternMatchContext implements IPatternMatchContext {

	private final URI contextURI;
	private final Collection<ISyntaxPattern<? extends Object>> patterns;
	private final Set<String> usedVariableNames = new LinkedHashSet<String>();
	private final Map<String, String> templateFieldTypeToNameMap = new LinkedHashMap<String, String>(8);
	private final boolean matchFuzzy;

	private ISynonymProvider localSynonymProvider;
	private List<Object> objectsInContext;

	public AbstractPatternMatchContext(URI contextURI) {
		this(contextURI, null);
	}

	public AbstractPatternMatchContext(URI contextURI, ISynonymProvider localSynonymProvider) {
		this(contextURI, localSynonymProvider, false);
	}

	public AbstractPatternMatchContext(URI contextURI, ISynonymProvider localSynonymProvider, boolean matchFuzzy) {
		this.contextURI = contextURI;
		this.localSynonymProvider = localSynonymProvider;
		this.matchFuzzy = matchFuzzy;
		this.patterns = initPatterns();
	}

	public void addObjectToContext(Object object) {
		if (objectsInContext == null) {
			objectsInContext = new ArrayList<Object>();
		}
		objectsInContext.add(object);
	}

	@Override
	public void addObjectsToContext(List<Object> objects) {
		for (Object object : objects) {
			addObjectToContext(object);
		}
	}

	public List<Object> getObjectsInContext() {
		if (objectsInContext == null) {
			return Collections.emptyList();
		}

		return objectsInContext;
	}

	@Override
	public URI getContextURI() {
		return contextURI;
	}

	public ISynonymProvider getLocalSynonymProvider() {
		return localSynonymProvider;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "(" + contextURI + ") " + objectsInContext;
	}

	@Override
	public Collection<ISyntaxPattern<? extends Object>> getPatternsInContext() {
		return patterns;
	}

	private Collection<ISyntaxPattern<? extends Object>> initPatterns() {

		Collection<ISyntaxPattern<? extends Object>> patterns = new ArrayList<ISyntaxPattern<? extends Object>>();

		SyntaxPatternRegistry registry = SyntaxPatternRegistry.REGISTRY;
		List<ISyntaxPatternProvider> providers = registry.getRegistryEntries();
		for (ISyntaxPatternProvider provider : providers) {
			Collection<ISyntaxPattern<? extends Object>> providedPatterns = null;
			// we do not trust providers. they may throw unexpected exceptions.
			// to continue work anyway, we wrap the provider call in a try/catch
			try {
				providedPatterns = provider.getPatterns(getContextURI());
			} catch (Throwable e) {
				NatSpecPlugin.logError("Exception/Error while evaluating pattern provider: " + provider.getClass(), e);
			}

			// nasty providers may return null instead of an empty list.
			// therefore we must check this.
			if (providedPatterns == null) {
				continue;
			}
			patterns.addAll(providedPatterns);
		}
		return patterns;
	}

	@Override
	public Set<String> getUsedVariableNames() {
		return usedVariableNames;
	}

	@Override
	public String getTemplateFieldByType(String qualifiedTypeName) {
		return templateFieldTypeToNameMap.get(qualifiedTypeName);
	}

	@Override
	public void setTemplateFieldNameToTypeMap(Map<String, String> templateFieldNameToTypeMap) {
		for (Entry<String, String> entry : templateFieldNameToTypeMap.entrySet()) {
			String fieldName = entry.getKey();
			String type = entry.getValue();
			templateFieldTypeToNameMap.put(type, fieldName);
		}
	}

	@Override
	public Map<String, String> getTemplateFieldTypeToNameMap() {
		return templateFieldTypeToNameMap;
	}

	@Override
	public boolean isLast(Object object) {
		return false;
	}

	@Override
	public boolean matchFuzzy() {
		return matchFuzzy;
	}
}
