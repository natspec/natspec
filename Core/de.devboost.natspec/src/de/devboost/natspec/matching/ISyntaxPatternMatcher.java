package de.devboost.natspec.matching;

import java.util.Collection;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.patterns.ISyntaxPattern;

public interface ISyntaxPatternMatcher {

	/**
	 * Returns all complete matches for the given pattern and the list of words.
	 * The matching is performed using the given context.
	 * 
	 * @param words the words to match against
	 * @param pattern the pattern to match
	 * @param context the context to be used while matching
	 * @return a list of complete matches
	 */
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words, ISyntaxPattern<? extends Object> pattern, IPatternMatchContext context);

	/**
	 * Returns all matches for the given pattern and the list of words.
	 * The matching is performed using the given context.
	 * 
	 * @param words the words to match against
	 * @param pattern the pattern to match
	 * @param context the context to be used while matching
	 * @param if <code>true</code>, incomplete matches are returned as well
	 * @return a list of matches
	 */
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words, ISyntaxPattern<? extends Object> pattern, IPatternMatchContext context, boolean includeIncompleteMatches);

	/**
	 * Returns all complete matches for the given patterns and the list of 
	 * words. The matching is performed using the given context.
	 * 
	 * @param words the words to match against
	 * @param patterns the patterns to match
	 * @param context the context to be used while matching
	 * @return a list of complete matches
	 */
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words, Collection<ISyntaxPattern<? extends Object>> patterns, IPatternMatchContext context);
	
	/**
	 * Returns all complete matches for the given list of patterns and the list 
	 * of words. The matching is performed using the given context.
	 * 
	 * @param words the words to match against
	 * @param patterns the patterns to match
	 * @param context the context to be used while matching
	 * @param includeIncompleteMatches if true, incomplete matches are returned
	 * 
	 * @return a list of complete matches
	 */
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words,
			Collection<ISyntaxPattern<? extends Object>> patterns,
			IPatternMatchContext context, boolean includeIncompleteMatches);
}
