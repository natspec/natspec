package de.devboost.natspec.matching;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;

import de.devboost.license.client.IClosedSourcePlugins;
import de.devboost.license.client.LicenseCheckUtil;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.patterns.ISyntaxPattern;

/**
 * The {@link ExternalMatchService} extends the {@link MatchService} with license checking functionality.
 */
public class ExternalMatchService extends MatchService {

	public ExternalMatchService(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}
	
	@Override
	public List<ISyntaxPatternMatch<? extends Object>> match(Resource resource, IPatternMatchContext context) {
		if (hasNoLicense()) {
			return null;
		}
		
		return super.match(resource, context);
	}
	
	@Override
	public List<ISyntaxPatternMatch<?>> match(Resource resource, IPatternMatchContext context,
			boolean includeIncompleteMatches, boolean filterObsoleteDefaultMatches) {
		if (hasNoLicense()) {
			return null;
		}
		
		return super.match(resource, context, includeIncompleteMatches, filterObsoleteDefaultMatches);
	}
	
	/**
	 * Matches the given sentence in the given context. Returns <code>null</code> if no valid license can be found.
	 */
	@Override
	public List<ISyntaxPatternMatch<? extends Object>> match(Sentence sentence, IPatternMatchContext context) {
		if (hasNoLicense()) {
			return null;
		}
		
		return super.match(sentence, context);
	}

	@Override
	public List<ISyntaxPatternMatch<?>> match(Sentence sentence, IPatternMatchContext context,
			boolean includeIncompleteMatches, boolean filterObsoleteDefaultMatches, boolean prioritizeMatches) {
		if (hasNoLicense()) {
			return null;
		}
		
		return super.match(sentence, context, includeIncompleteMatches, filterObsoleteDefaultMatches, prioritizeMatches);
	}
	
	@Override
	public List<ISyntaxPatternMatch<?>> match(Sentence sentence, IPatternMatchContext context,
			boolean includeIncompleteMatches, boolean filterObsoleteDefaultMatches, boolean prioritizeMatches,
			Collection<ISyntaxPattern<?>> patterns) {
		if (hasNoLicense()) {
			return null;
		}
		
		return super.match(sentence, context, includeIncompleteMatches, filterObsoleteDefaultMatches, prioritizeMatches,
				patterns);
	}

	private boolean hasNoLicense() {
		// Check license
		LicenseCheckUtil licenseCheckUtil = new LicenseCheckUtil(IClosedSourcePlugins.NATSPEC_FEATURE_ID);
		boolean noLicense = !licenseCheckUtil.hasValidLicense();
		return noLicense;
	}
}
