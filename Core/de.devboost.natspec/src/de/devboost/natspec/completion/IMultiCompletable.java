package de.devboost.natspec.completion;

import java.util.Set;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * The {@link IMultiCompletable} interface can be optionally implemented by
 * instances of {@link ISyntaxPatternPart} to provide completion proposals
 * (e.g., default values that can be inserted and which will match the syntax
 * pattern part).
 * 
 * @see ICompletable
 */
public interface IMultiCompletable {

	/**
	 * Returns a set of strings that can be inserted at a position in a
	 * sentence.
	 */
	public Set<String> getCompletionProposals(IPatternMatchContext context);
}
