package de.devboost.natspec.completion;

import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.matching.IPatternMatchContext;

/**
 * The {@link ICompletable} interface can be optionally implemented by instances
 * of {@link ISyntaxPatternPart} to provide a completion proposal (e.g., a
 * default value that can be inserted and which will match the syntax pattern
 * part).
 */
public interface ICompletable {

	/**
	 * Returns a string that can be inserted at a position in a sentence.
	 */
	public String getCompletionProposal(IPatternMatchContext context);
}
