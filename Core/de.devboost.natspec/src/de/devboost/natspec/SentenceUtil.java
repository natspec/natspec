package de.devboost.natspec;

/**
 * The {@link SentenceUtil} can be used to determine the type of sentences based on their content (e.g., whether a
 * sentence is a comment of not).
 */
public class SentenceUtil {

	public static final SentenceUtil INSTANCE = new SentenceUtil();

	public static final String COMMENT_PREFIX = "//";

	public static final String OUTLINE_MARKER_PREFIX = "///";

	private SentenceUtil() {
		super();
	}

	/**
	 * Creates a sentence from the given text.
	 */
	public Sentence createSentence(String text) {
		Sentence sentence = NatspecFactory.eINSTANCE.createSentence();
		sentence.setText(text);
		
		String[] words = text.split(" ");
		for (String word : words) {
			Word newWord = NatspecFactory.eINSTANCE.createWord();
			newWord.setText(word);
			sentence.getWords().add(newWord);
		}
		
		return sentence;
	}

	/**
	 * Determines whether the sentence is a comment. Currently sentences which start with two slashes are recognized as
	 * comments.
	 */
	// TODO replace this by some dynamic configuration mechanism (something like a ISentencePreprocessor?)
	public boolean isComment(Sentence sentence) {
		String text = getTrimmedText(sentence);
		return text.startsWith(COMMENT_PREFIX);
	}

	/**
	 * Determines whether the sentence is an outline marker. Currently sentences which start with three slashes are
	 * recognized as outline markers.
	 */
	public boolean isOutlineMarker(Sentence sentence) {
		String text = getTrimmedText(sentence);
		return text.startsWith(OUTLINE_MARKER_PREFIX);
	}

	/**
	 * Returns <code>true</code> if the sentence is empty (after trimming all whitespace characters).
	 */
	public boolean isEmpty(Sentence sentence) {
		String text = getTrimmedText(sentence);
		return text.isEmpty();
	}

	/**
	 * Returns the pure text of the given sentence. If the sentence is a comment, the comment prefix is removed. If the
	 * sentence is a outline marker, the outline marker prefix is removed. Also, the text is trimmed.
	 */
	public String getText(Sentence sentence) {
		String text = getTrimmedText(sentence);

		if (SentenceUtil.INSTANCE.isOutlineMarker(sentence)) {
			text = removePrefixAndTrim(text, SentenceUtil.OUTLINE_MARKER_PREFIX);
		} else if (SentenceUtil.INSTANCE.isComment(sentence)) {
			text = removePrefixAndTrim(text, SentenceUtil.COMMENT_PREFIX);
		}

		return text;
	}

	private String removePrefixAndTrim(String text, String prefix) {
		text = text.substring(prefix.length());
		// We need to trim the text again, because after removing the prefix, there might be whitespace characters in
		// the front.
		text = text.trim();
		return text;
	}

	private String getTrimmedText(Sentence sentence) {
		String text = sentence.getText();
		if (text == null) {
			return "";
		}

		text = text.trim();
		return text;
	}
}
