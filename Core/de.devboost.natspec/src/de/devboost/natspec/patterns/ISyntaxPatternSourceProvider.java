package de.devboost.natspec.patterns;

/**
 * The {@link ISyntaxPatternSourceProvider} interface can be optionally implemented by {@link ISyntaxPattern}s to
 * provide information about the origin of the syntax pattern. This information is used to provide hyperlinks.
 */
public interface ISyntaxPatternSourceProvider {

	public String getProjectName();

	public String getQualifiedTypeName();

	public String getMethodName();
}
