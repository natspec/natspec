package de.devboost.natspec.patterns;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.parts.styling.IStyleable;

/**
 * An {@link ISyntaxPatternPart} is a part of a syntax pattern. This can be
 * either a static word or some more complex part that may match against
 * multiple words only. There can also be syntax pattern parts the do not
 * require words to be matched against, but some context in order to let the
 * corresponding pattern match.
 */
public interface ISyntaxPatternPart extends IStyleable {

	/**
	 * Try to match this part of the pattern to the head of the given list of
	 * words. If no match is found, either <code>null</code> or an instance of
	 * {@link ISyntaxPatternPartMatch} that returns <code>false</code> for
	 * {@link ISyntaxPatternPartMatch#hasMatched()} is returned.
	 * <p>
	 * The list of words that is passed to this methods is unmodifiable.
	 * 
	 * @param words
	 *            the list of words to match against
	 * @param context
	 *            the context in which to perform the matching
	 */
	public ISyntaxPatternPartMatch match(List<Word> words,
			IPatternMatchContext context);

	// TODO does this belong here?
	public String toSimpleString();
	
}
