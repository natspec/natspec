package de.devboost.natspec.patterns;

import java.util.Collection;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.registries.SyntaxPatternRegistry;

/**
 * An {@link ISyntaxPatternProvider} provides syntax patterns. It can be
 * registered with the {@link SyntaxPatternRegistry} to be available to classes
 * that match sentences against syntax patterns.
 */
public interface ISyntaxPatternProvider {

	/**
	 * Returns the patterns that apply to the resource at the given {@link URI}.
	 * We pass the {@link URI} because patterns can be specific to a project or
	 * even to a single file. If the {@link URI} is <code>null</code> the
	 * provider is requested to return all patterns regardless of the
	 * {@link URI}s they apply to.
	 * 
	 * @param uri
	 *            a {@link URI} the patterns must apply to or <code>null</code>
	 *            if the provider is requested to return all patterns no matter
	 *            to which {@link URI} they apply to
	 * @return a collection of syntax patterns
	 */
	public Collection<ISyntaxPattern<? extends Object>> getPatterns(URI uri);
}
