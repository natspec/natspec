package de.devboost.natspec.patterns;

/**
 * The {@link IComparable} interface can be implemented by
 * {@link ISyntaxPattern}s and {@link ISyntaxPatternPart}s. If the interface is
 * implemented, NatSpec can check whether patterns are equal. This is used to
 * avoid unnecessary revalidation of NatSpec files. We do neither use
 * {@link #equals(Object)} nor {@link #hashCode()} as this has side effects on
 * Sets and Maps which store elements of the types mentioned above.
 * 
 * Implementing {@link IComparable} is optional, but strongly recommended.
 */
public interface IComparable {

	public boolean isEqualTo(Object object);
	
	public int computeHashCode();
}
