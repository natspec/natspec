package de.devboost.natspec.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

/**
 * An {@link IntegerMatch} represents a match from a {@link IntegerArgument} to
 * a word in a sentence.
 */
public class IntegerMatch extends AbstractSyntaxPatternPartMatch {

	private final int value;

	public IntegerMatch(List<Word> words, int value) {
		super(words);
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " [" + value + "]";
	}
}
