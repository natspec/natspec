package de.devboost.natspec.patterns.parts;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

/**
 * An {@link DoubleArgument} is a simple syntax pattern part that matches floating point numbers.
 */
public class DoubleArgument extends AbstractStyleable implements ISyntaxPatternPart, ICompletable, IComparable {

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		if (words.isEmpty()) {
			return null;
		}

		Word first = words.get(0);
		String text = first.getText();
		if (text == null) {
			return null;
		}

		try {
			double value = Double.parseDouble(text);
			return new DoubleMatch(Collections.singletonList(first), value);
		} catch (NumberFormatException nfe) {
			return null;
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	@Override
	public String toSimpleString() {
		return "<Double>";
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		return "1.0";
	}

	@Override
	public boolean isEqualTo(Object object) {
		// Since this class does not have fields, all objects are equal.
		if (object == null || !(object instanceof DoubleArgument)) {
			return false;
		}
		return super.isEqualTo(object);
	}

	@Override
	public int computeHashCode() {
		// Since this class does not have fields, all objects can have the same
		// hash code.
		return 0;
	}
}
