package de.devboost.natspec.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

/**
 * A {@link DefaultSentencePartMatch} is a match that corresponds to a list of
 * {@link StaticWord}s.
 */
public class DefaultSentencePartMatch extends AbstractSyntaxPatternPartMatch {

	private final boolean isMany;

	public DefaultSentencePartMatch(List<Word> matchedWords, boolean isMany) {
		super(matchedWords);
		this.isMany = isMany;
	}

	public boolean isMany() {
		return isMany;
	}
}
