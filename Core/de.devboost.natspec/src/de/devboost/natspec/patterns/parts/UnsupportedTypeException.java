package de.devboost.natspec.patterns.parts;

/**
 * This exception is thrown if one tries to create a syntax pattern part for a
 * type that is not supported by NatSpec.
 */
public class UnsupportedTypeException extends Exception {

	private static final long serialVersionUID = -8448458288855971828L;
	
	private final boolean handled;

	public UnsupportedTypeException(String message) {
		this(message, false);
	}

	public UnsupportedTypeException(String message, boolean handled) {
		super(message);
		this.handled = handled;
	}

	public boolean isHandled() {
		return handled;
	}
}
