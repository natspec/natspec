package de.devboost.natspec.patterns.parts.styling;

import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * Interface which provides styling for {@link ISyntaxPatternPart}.
 * 
 * @author Jan Reimann (jan.reimann@devboost.de)
 *
 */
public interface IStyleable {
	
	/**
	 * Sets the style.
	 * @param style
	 */
	public void setStyle(Style style);
	
	/**
	 * Returns the style.
	 * @return
	 */
	public Style getStyle();

}
