package de.devboost.natspec.patterns.parts.styling;

import java.util.Arrays;

import de.devboost.natspec.patterns.IComparable;

public class Style implements IComparable {

	private final boolean bold;
	private final boolean italic;
	private final boolean underline;
	private final boolean strikethrough;
	private final int[] color;

	public Style(boolean bold, boolean italic, boolean underline, boolean strikethrough, int[] color) {
		super();
		this.bold = bold;
		this.italic = italic;
		this.underline = underline;
		this.strikethrough = strikethrough;
		this.color = color;
	}

	public boolean isBold() {
		return bold;
	}

	public boolean isItalic() {
		return italic;
	}

	public boolean isUnderline() {
		return underline;
	}

	public boolean isStrikethrough() {
		return strikethrough;
	}

	public int[] getColor() {
		return color;
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Style other = (Style) obj;
		if(bold != other.bold){
			return false;
		}
		if(!Arrays.equals(color, other.color)){
			return false;
		}
		if(italic != other.italic){
			return false;
		}
		if(strikethrough != other.strikethrough){
			return false;
		}
		if(underline != other.underline){
			return false;
		}
		return true;
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (bold ? 1231 : 1237);
		result = prime * result + Arrays.hashCode(color);
		result = prime * result + (italic ? 1231 : 1237);
		result = prime * result + (strikethrough ? 1231 : 1237);
		result = prime * result + (underline ? 1231 : 1237);
		return result;
	}
}
