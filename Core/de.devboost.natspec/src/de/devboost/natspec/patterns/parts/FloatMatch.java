package de.devboost.natspec.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

/**
 * An {@link FloatMatch} represents a match from a {@link FloatArgument} to a word in a sentence.
 */
public class FloatMatch extends AbstractSyntaxPatternPartMatch {

	private final float value;

	public FloatMatch(List<Word> words, float value) {
		super(words);
		this.value = value;
	}

	public float getValue() {
		return value;
	}
}
