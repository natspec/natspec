package de.devboost.natspec.patterns.parts;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.internal.matching.WordMatcher;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;
import de.devboost.natspec.util.ComparisonHelper;

/**
 * A {@link StaticWord} is the most simple part of a syntax pattern. Static words match text that equals the word or one
 * of the words synonyms. Also the match is performed case insensitive.
 */
public class StaticWord extends AbstractStyleable implements ISyntaxPatternPart, ICompletable, IComparable {

	private final String text;

	private WordMatcher wordMatcher;

	public StaticWord(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	private WordMatcher getWordMatcher() {
		if (wordMatcher == null) {
			wordMatcher = new WordMatcher(getText());
		}
		return wordMatcher;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {
		if (words.isEmpty()) {
			return null;
		}

		Word head = words.get(0);
		String lowerCaseText = head.getLowerCaseText();

		WordMatcher wordMatcher = getWordMatcher();
		if (wordMatcher.match(context, lowerCaseText)) {
			return new StaticWordMatch(Collections.singletonList(head));
		}

		// not matching.
		return null;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [" + getText() + "]";
	}

	@Override
	public String toSimpleString() {
		return getText();
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		return getText();
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		StaticWord other = (StaticWord) obj;
		if (!ComparisonHelper.INSTANCE.isEqualTo(text, other.text)) {
			return false;
		}
		return super.isEqualTo(other);
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}
}
