package de.devboost.natspec.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

/**
 * A {@link StaticWordMatch} is a match that corresponds to a 
 * {@link StaticWord}.
 */
public class StaticWordMatch extends AbstractSyntaxPatternPartMatch {

	public StaticWordMatch(List<Word> matchedWords) {
		super(matchedWords);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + getMatchedWords();
	}
}
