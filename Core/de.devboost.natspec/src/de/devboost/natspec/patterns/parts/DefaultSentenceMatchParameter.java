package de.devboost.natspec.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

public class DefaultSentenceMatchParameter extends AbstractStyleable implements ISyntaxPatternPart,
		ICompletable, IComparable {

	private final boolean isMany;
	
	public DefaultSentenceMatchParameter() {
		this(false);
	}

	public DefaultSentenceMatchParameter(boolean isMany) {
		super();
		this.isMany = isMany;
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		// TODO Implement this or remove "implements ICompletable"
		return null;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {
		return new DefaultSentencePartMatch(words, isMany);
	}

	@Override
	public String toSimpleString() {
		// TODO Is this really what we'd like to see in the SyntaxPatternView?
		return "Complete Sentence Match";
	}

	@Override
	public boolean isEqualTo(Object object) {
		// Since this class does not have fields, all objects are equal.
		if(object == null || !(object instanceof DefaultSentenceMatchParameter)){
			return false;
		}
		return super.isEqualTo(object);
	}

	@Override
	public int computeHashCode() {
		// Since this class does not have fields, all objects can have the same
		// hash code.
		return 0;
	}
}