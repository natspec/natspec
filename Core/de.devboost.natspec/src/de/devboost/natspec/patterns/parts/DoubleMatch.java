package de.devboost.natspec.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

/**
 * An {@link DoubleMatch} represents a match from a {@link DoubleArgument} to a word in a sentence.
 */
public class DoubleMatch extends AbstractSyntaxPatternPartMatch {

	private final double value;

	public DoubleMatch(List<Word> words, double value) {
		super(words);
		this.value = value;
	}

	public double getValue() {
		return value;
	}
}
