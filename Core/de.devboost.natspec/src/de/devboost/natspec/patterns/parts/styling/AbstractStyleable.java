package de.devboost.natspec.patterns.parts.styling;

import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.util.ComparisonHelper;

public class AbstractStyleable implements IStyleable, IComparable {

	private Style style;

	@Override
	public void setStyle(Style style) {
		this.style = style;
	}

	@Override
	public Style getStyle() {
		return style;
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractStyleable other = (AbstractStyleable) obj;
		if (style == null) {
			if (other.style != null) {
				return false;
			}
		} else if (!ComparisonHelper.INSTANCE.isEqualTo(style, other.style)) {
			return false;
		}
		return true;
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((style == null) ? 0 : style.computeHashCode());
		return result;
	}

}
