package de.devboost.natspec.patterns;

import java.util.List;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;

/**
 * An {@link ISyntaxPattern} specifies a pattern one can match sentences against. A pattern consists of
 * {@link ISyntaxPatternPart}s (see {@link #getParts()}). Patterns must provide new instances of corresponding match
 * objects (see {@link #createPatternMatch(IPatternMatchContext)}). Also, patterns can be asked by clients to create
 * specific user data for a match. This user data object encapsulates the semantics of the pattern.
 *
 * @param <UserDataType>
 *            an object encapsulating the semantics of this pattern
 */
public interface ISyntaxPattern<UserDataType> {

	/**
	 * Returns the parts of this pattern.
	 */
	public List<ISyntaxPatternPart> getParts();

	/**
	 * Create a new match instance that can be used to match this pattern against sentences. This method is required to
	 * avoid warnings about the type parameter <code>UserDataType</code>. Actually all patterns may use the same class
	 * implementing {@link ISyntaxPatternMatch}, but to create instances of this class without having the compiler
	 * complain about type safety, the class must be instantiated in a context where the type parameter
	 * <code>UserDataType</code> is bound. This is usually only the case for concrete pattern implementations.
	 */
	public ISyntaxPatternMatch<UserDataType> createPatternMatch(IPatternMatchContext context);
	
	/**
	 * Create and return a user data object that encapsulates the semantics of this pattern with regard to the given
	 * match. The semantics of the pattern may differ depending on the text that was matched to this pattern.
	 */
	public UserDataType createUserData(ISyntaxPatternMatch<UserDataType> match);

	/**
	 * Returns <code>true</code> if matches to other syntax patterns are allowed.
	 * 
	 * @return <code>true</code> if multiple matches are allowed, otherwise <code>false</code>
	 */
	public boolean allowsOtherMatches();
	
	/**
	 * Returns the documentation of a SyntaxPattern if provided. This is mainly used in the
	 * de.devboost.natspec.resource.natspec.ui.DynamicPatternHoverTextProvider.
	 * 
	 * @return the documentation for this pattern in HTML
	 */
	public String getDocumentation();
}
