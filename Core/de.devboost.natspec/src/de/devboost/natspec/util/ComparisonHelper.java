package de.devboost.natspec.util;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.devboost.natspec.patterns.IComparable;

/**
 * The {@link ComparisonHelper} class can be used to compare {@link IComparable}
 * s as well as lists and maps containing {@link IComparable}s.
 */
public class ComparisonHelper {
	
	public final static ComparisonHelper INSTANCE = new ComparisonHelper();
	
	private ComparisonHelper() {
		super();
	}

	/**
	 * Performs a null-safe comparison of <code>object1</code> and
	 * <code>object2</code>. If both objects are <code>null</code> or if they
	 * are equal, <code>true</code> is returned.
	 * 
	 * @param object1
	 *            first object to compare
	 * @param object2
	 *            second object to compare
	 * @return <code>true</code> is the objects are equal or both
	 *         <code>null</code>, otherwise <code>false</code>.
	 */
	public boolean isEqualTo(Object object1, Object object2) {
		if (object1 == null && object2 == null) {
			return true;
		}
		if (object1 == null && object2 != null) {
			return false;
		}
		
		if (object1 instanceof IComparable && object2 instanceof IComparable) {
			IComparable comparable1 = (IComparable) object1;
			IComparable comparable2 = (IComparable) object2;
			return comparable1.isEqualTo(comparable2);
		} else {
			return object1.equals(object2);
		}
	}

	/**
	 * Performs a null-safe comparison of the given two lists. If both lists are
	 * <code>null</code> or if they are equal, <code>true</code> is returned. To
	 * consider two lists being equal, they must have the same size and contain
	 * the same objects.
	 * 
	 * @param list1
	 *            first list to compare
	 * @param list2
	 *            second list to compare
	 * @return <code>true</code> is the lists are equal or both
	 *         <code>null</code>, otherwise <code>false</code>.
	 */
	public boolean isEqualTo(List<?> list1, List<?> list2) {
		if (list1 == null && list2 == null) {
			return true;
		}
		if (list1 == null && list2 != null) {
			return false;
		}
		
		int size1 = list1.size();
		int size2 = list2.size();
		if (size1 != size2) {
			return false;
		}
		
		for (int i = 0; i < size1; i++) {
			Object object1 = list1.get(i);
			Object object2 = list2.get(i);
			if (!isEqualTo(object1, object2)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Performs a null-safe comparison of the given two maps. If both maps are
	 * <code>null</code> or if they are equal, <code>true</code> is returned. To
	 * consider two maps being equal, they must have the same size and contain
	 * the same key/value pairs.
	 * 
	 * @param map1
	 *            first map to compare
	 * @param map2
	 *            second map to compare
	 * @return <code>true</code> is the maps are equal or both <code>null</code>
	 *         , otherwise <code>false</code>.
	 */
	public boolean isEqualTo(Map<?, ?> map1, Map<?, ?> map2) {
		if (map1 == null && map2 == null) {
			return true;
		}
		if (map1 == null && map2 != null) {
			return false;
		}
		
		Set<?> keySet1 = map1.keySet();
		Set<?> keySet2 = map2.keySet();
		int keyCount1 = keySet1.size();
		int keyCount2 = keySet2.size();
		if (keyCount1 != keyCount2) {
			return false;
		}
		
		Iterator<?> iterator1 = keySet1.iterator();
		Iterator<?> iterator2 = keySet2.iterator();
		while (iterator1.hasNext()) {
			Object key1 = iterator1.next();
			Object key2 = iterator2.next();
			if (!isEqualTo(key1, key2)) {
				return false;
			}
			Object value1 = map1.get(key1);
			Object value2 = map2.get(key2);
			if (!isEqualTo(value1, value2)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Performs a null-safe comparison of the given two arrays. If both arrays
	 * are <code>null</code> or if they are equal, <code>true</code> is
	 * returned. To consider two arrays being equal, they must have the same
	 * length and contain the same objects.
	 * 
	 * @param array1
	 *            first array to compare
	 * @param array2
	 *            second array to compare
	 * @return <code>true</code> is the arrays are equal or both
	 *         <code>null</code>, otherwise <code>false</code>.
	 */
	public boolean isEqualToArray(Object[] array1, Object[] array2) {
		if (array1 == null && array2 == null) {
			return true;
		}
		if (array1 == null && array2 != null) {
			return false;
		}
		
		int size1 = array1.length;
		int size2 = array2.length;
		if (size1 != size2) {
			return false;
		}
		
		for (int i = 0; i < size1; i++) {
			Object object1 = array1[i];
			Object object2 = array2[i];
			if (!isEqualTo(object1, object2)) {
				return false;
			}
		}
		return true;
	}
}
