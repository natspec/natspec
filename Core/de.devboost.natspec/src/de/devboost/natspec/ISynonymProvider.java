package de.devboost.natspec;

import java.util.Set;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.registries.SynonymProviderRegistry;

/**
 * An {@link ISynonymProvider} provides sets of synonyms within a given context.
 * The context is specified by a {@link URI} of a document. ISynonymProviders
 * can be registered with the {@link SynonymProviderRegistry} which is used by
 * all document processors to look up synonyms.
 */
public interface ISynonymProvider {

	/**
	 * Returns a set of synonyms sets. Each synonym sets contains the words that
	 * are synonymous.
	 * 
	 * @param contextURI the URI of the document for which synonyms are 
	 *        requested.
	 * @return a set of synonym sets
	 */
	public Set<Set<String>> getSynonyms(URI contextURI);
}
