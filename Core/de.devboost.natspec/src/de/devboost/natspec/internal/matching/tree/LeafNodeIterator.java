package de.devboost.natspec.internal.matching.tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LeafNodeIterator<T> implements Iterator<MatchTreeNode<T>> {
	
	private List<MatchTreeNode<T>> remainingNodes = new ArrayList<MatchTreeNode<T>>();
	private MatchTreeNode<T> next; 

	public void initialize(MatchTreeNode<T> rootNode) {
		this.remainingNodes.clear();
		this.remainingNodes.add(rootNode);
		this.next = null;
	}

	@Override
	public boolean hasNext() {
		next = computeNext();
		return next != null;
	}

	private MatchTreeNode<T> computeNext() {
		while (!remainingNodes.isEmpty()) {
			MatchTreeNode<T> next = remainingNodes.remove(0);

			if (next.hasChildren()) {
				// Collect leaf nodes recursively
				List<MatchTreeNode<T>> children = next.getChildren();
				remainingNodes.addAll(children);

				boolean isNotEmpty = next.getPatterns().hasNext();
				if (isNotEmpty) {
					if (!next.hasRemainingWords()) {
						return next;
					}
				}
			} else {
				// No children found so matchTreeNode is a leaf node
				return next;
			}
		};
		
		// There is no remaining nodes
		return null;
	}

	@Override
	public MatchTreeNode<T> next() {
		return next;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
