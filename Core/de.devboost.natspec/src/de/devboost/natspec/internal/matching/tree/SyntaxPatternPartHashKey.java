package de.devboost.natspec.internal.matching.tree;

import java.util.HashMap;

import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * The {@link SyntaxPatternPartHashKey} is necessary to use the methods from
 * {@link IComparable} instead of {@link Object#hashCode()} and
 * {@link Object#equals(Object)} when {@link ISyntaxPatternPart}s are stored in
 * a {@link HashMap}. This class delegates calls to {@link #equals(Object)} to
 * {@link IComparable#isEqualTo(Object)} and calls to {@link #hashCode()} to
 * {@link IComparable#computeHashCode()} if the {@link ISyntaxPatternPart}
 * implements {@link IComparable}. If not, the calls are forwarded to
 * {@link Object#equals(Object)} and {@link Object#hashCode()}, which is the
 * default behavior.
 */
public class SyntaxPatternPartHashKey {

	private final IComparable comparablePart;
	private final ISyntaxPatternPart nonComparablePart;

	public SyntaxPatternPartHashKey(ISyntaxPatternPart part) {
		super();
		if (part instanceof IComparable) {
			this.comparablePart = (IComparable) part;
			this.nonComparablePart = null;
		} else {
			this.nonComparablePart = part;
			this.comparablePart = null;
		}
	}

	public ISyntaxPatternPart getSyntaxPatternPart() {
		if (comparablePart != null) {
			return (ISyntaxPatternPart) comparablePart;
		}
		
		return nonComparablePart;
	}

	@Override
	public int hashCode() {
		int result;
		if (comparablePart != null) {
			result = comparablePart.computeHashCode();
		} else {
			result = nonComparablePart.hashCode();
		}
		
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SyntaxPatternPartHashKey) {
			SyntaxPatternPartHashKey hashedPart = (SyntaxPatternPartHashKey) obj;
			if (hashedPart != null) {
				if (hashedPart.comparablePart != null) {
					obj = hashedPart.comparablePart;
				} else {
					obj = hashedPart.nonComparablePart;
				}
			}
		}
		
		boolean result;
		if (comparablePart != null) {
			result = comparablePart.isEqualTo(obj);
		} else {
			result = nonComparablePart.equals(obj);
		}

		return result;
	}
}
