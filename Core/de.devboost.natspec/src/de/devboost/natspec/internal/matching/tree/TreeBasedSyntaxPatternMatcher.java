package de.devboost.natspec.internal.matching.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.devboost.essentials.IntReference;
import de.devboost.natspec.Word;
import de.devboost.natspec.internal.matching.AbstractSyntaxPatternMatcher;
import de.devboost.natspec.internal.matching.list.WindowedList;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

public class TreeBasedSyntaxPatternMatcher extends AbstractSyntaxPatternMatcher {

	// This map is reused which makes this class thread-unsafe
	private final Map<Integer, ISyntaxPatternPartMatch> partsToMatchesMap = new LinkedHashMap<Integer, ISyntaxPatternPartMatch>();
	private final LeafNodeIterator<Object> leafNodeIterator = new LeafNodeIterator<Object>();
	private final IntReference partCountRef = new IntReference(0);

	private PatternTreeNode<?> rootNode = null;
	private int lastPatternSize = -1;
	private long lastPatternHashSum = 0;

	@Override
	// This method is synchronized because the PatternTreeNode class is not thread-safe
	public synchronized List<ISyntaxPatternMatch<? extends Object>> match(
			List<Word> words,
			Collection<ISyntaxPattern<? extends Object>> patterns,
			IPatternMatchContext context, boolean includeIncompleteMatches) {

		List<ISyntaxPatternMatch<? extends Object>> matches = match(words,
				context, patterns, includeIncompleteMatches);
		return prepareResult(matches, includeIncompleteMatches);
	}

	public void reset() {
		rootNode = null;
		lastPatternSize = -1;
		lastPatternHashSum = 0;
	}

	// TODO Figure out how we can use generic types without any warnings
	@SuppressWarnings({ "unchecked" })
	private List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words,
			IPatternMatchContext context,
			Collection<ISyntaxPattern<? extends Object>> patterns,
			boolean includeIncompleteMatches) {

		initializeRootNodeIfRequired(patterns);

		Map<SyntaxPatternPartHashKey, ?> childMap = rootNode.getChildMap();

		Set<SyntaxPatternPartHashKey> parts = childMap.keySet();
		List<MatchTreeNode<?>> matchTreeNodes = new ArrayList<MatchTreeNode<?>>(
				parts.size());
		for (SyntaxPatternPartHashKey hashedPart : parts) {
			PatternTreeNode<Object> childNode = (PatternTreeNode<Object>) childMap
					.get(hashedPart);
			ISyntaxPatternPart part = hashedPart.getSyntaxPatternPart();
			MatchTreeNode<Object> matchTreeNode = new MatchTreeNode<Object>(
					null, part, childNode);
			if (!(words instanceof WindowedList)) {
				words = new WindowedList<Word>(words);
			}
			matchTreeNode.match((WindowedList<Word>) words, context);
			matchTreeNodes.add(matchTreeNode);
		}

		List<ISyntaxPatternMatch<? extends Object>> matches = new ArrayList<ISyntaxPatternMatch<? extends Object>>();
		for (MatchTreeNode<?> matchTreeNode : matchTreeNodes) {
			createPatternMatch(context, patterns, matchTreeNode, matches,
					words, includeIncompleteMatches);
		}
		return matches;
	}

	private void initializeRootNodeIfRequired(
			Collection<ISyntaxPattern<? extends Object>> patterns) {

		if (rootNode == null) {
			initializeRootNode(patterns);
			return;
		}

		if (patterns.isEmpty()) {
			initializeRootNode(patterns);
			return;
		}

		// FIXME 2.3 This is not 100% exact. If patterns change, but the sum of their hash code is the same, we will
		// use outdated patterns. However, correctly comparing whether the patterns are same as for the last call, will
		// make the benefit of the caching the root node obsolete.
		long patternHash = getPatternHashSum(patterns);
		if (lastPatternSize == patterns.size() && lastPatternHashSum == patternHash) {
			return;
		}

		initializeRootNode(patterns);
	}

	/**
	 * Returns the sum of all hash code for the given patterns.
	 */
	private long getPatternHashSum(Collection<ISyntaxPattern<? extends Object>> patterns) {
		int sum = 0;
		for (ISyntaxPattern<? extends Object> pattern : patterns) {
			// We intentionally use the System hash code here to determine whether the patterns have changed, because 
			// it can be computed quickly and it is very likely to change if one of the patterns is changed as a new 
			// instance will be create for each new pattern (having a new hash code).
			sum += pattern.hashCode();
		}
		return sum;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initializeRootNode(
			Collection<ISyntaxPattern<? extends Object>> patterns) {
		rootNode = new PatternTreeNode(patterns);
		rootNode.split();

		lastPatternSize = patterns.size();
		if (lastPatternSize > 0) {
			lastPatternHashSum = getPatternHashSum(patterns);
		} else {
			lastPatternHashSum = 0;
		}
	}

	// TODO Figure out how we can use generic types without any warnings
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void createPatternMatch(IPatternMatchContext context,
			Collection<ISyntaxPattern<? extends Object>> patterns,
			MatchTreeNode matchTreeNode,
			List<ISyntaxPatternMatch<? extends Object>> matches,
			List<Word> words, boolean includeIncompleteMatches) {

		// First find the leaf nodes that end matching branches in the tree.
		// We use a custom iterator for performance reasons here (reduced memory
		// consumption). The iterator is reused.
		leafNodeIterator.initialize(matchTreeNode);

		// Second rebuild the branches by going backwards through the tree
		// starting at the matching leafs.
		while (leafNodeIterator.hasNext()) {
			partsToMatchesMap.clear();

			MatchTreeNode<?> leafNode = leafNodeIterator.next();
			processLeafNode(context, words, includeIncompleteMatches, matches, leafNode);
		}

		// Clear reused map to free memory
		partsToMatchesMap.clear();
	}

	private void processLeafNode(IPatternMatchContext context, List<Word> words,
			boolean includeIncompleteMatches,
			List<ISyntaxPatternMatch<? extends Object>> matches,
			MatchTreeNode<?> leafNode) {
		
		// A flag that indicates whether the leaf node and all its parent nodes
		// were complete.
		boolean complete = isBranchComplete(leafNode, includeIncompleteMatches, partCountRef);
		if (!complete && !includeIncompleteMatches) {
			return;
		}
		int partCount = partCountRef.getValue();

		// For incomplete matches we must include the child patterns, because
		// the leaf node of an incomplete match is not necessarily the last node
		// of the pattern.
		boolean includeChildPatterns = includeIncompleteMatches;
		PatternVisitor patternVisitor = new PatternVisitor(partCount, complete, partsToMatchesMap, context, words, matches);
		leafNode.visit(patternVisitor, includeChildPatterns);
	}

	private boolean isBranchComplete(MatchTreeNode<?> leafNode, boolean includeIncompleteMatches, IntReference partCountRef) {
		boolean complete = true;
		// A counter to figure out how many pattern parts were matched.
		int partCount = 0;
		MatchTreeNode<?> node = leafNode;
		while (node != null) {
			ISyntaxPatternPartMatch partMatch = node.getMatch();
			if (partMatch != null) {
				int depth = node.getDepth();
				partsToMatchesMap.put(depth, partMatch);
			}
			// All nodes on the branch must be complete in order to consider
			// the whole branch as complete.
			complete &= node.isComplete();
			if (!complete && !includeIncompleteMatches) {
				return false;
			}
			
			node = node.getParent();
			partCount++;
		}
		partCountRef.setValue(partCount);

		return complete;
	}
}
