/**
 * This package contains the initial and basic implementation of the pattern
 * matching algorithm.
 */
package de.devboost.natspec.internal.matching.basic;