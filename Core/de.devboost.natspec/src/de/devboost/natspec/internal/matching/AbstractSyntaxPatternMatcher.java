package de.devboost.natspec.internal.matching;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternMatcher;
import de.devboost.natspec.patterns.ISyntaxPattern;

public abstract class AbstractSyntaxPatternMatcher implements ISyntaxPatternMatcher {

	@Override
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words, ISyntaxPattern<? extends Object> pattern, IPatternMatchContext context) {
		List<ISyntaxPattern<? extends Object>> list = new ArrayList<ISyntaxPattern<? extends Object>>(1);
		list.add(pattern);
		return match(words, list, context);
	}

	@Override
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words, ISyntaxPattern<? extends Object> pattern, IPatternMatchContext context, boolean includeIncompleteMatches) {
		List<ISyntaxPattern<? extends Object>> list = new ArrayList<ISyntaxPattern<? extends Object>>(1);
		list.add(pattern);
		return match(words, list, context, includeIncompleteMatches);
	}

	@Override
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words, Collection<ISyntaxPattern<? extends Object>> patterns, IPatternMatchContext context) {
		return match(words, patterns, context, false);
	}

	/**
	 * Iterates over the given list of matches and triggers the creation of user
	 * data for all complete matches. If <code>includeIncompleteMatches</code>
	 * is <code>false</code>, the incomplete matches are excluded from the
	 * result list.
	 * 
	 * @param matches
	 *            a unfiltered (raw) list of matches
	 * @param includeIncompleteMatches
	 * @return
	 */
	protected List<ISyntaxPatternMatch<? extends Object>> prepareResult(
			List<ISyntaxPatternMatch<? extends Object>> matches,
			boolean includeIncompleteMatches) {
		
		List<ISyntaxPatternMatch<? extends Object>> resultingMatches = new ArrayList<ISyntaxPatternMatch<? extends Object>>();
		for (ISyntaxPatternMatch<? extends Object> match : matches) {
			boolean isComplete = match.isComplete();
			if (isComplete) {
				match.createUserData();
			}
			if (isComplete || includeIncompleteMatches) {
				resultingMatches.add(match);
			}
		}
		return resultingMatches;
	}
}
