/**
 * This package contains an alternative implementation for the pattern matching
 * algorithm that base on a transformation of a set of patterns to tree where
 * common pattern prefixes are represented by a node in this tree.
 */
package de.devboost.natspec.internal.matching.tree;