package de.devboost.natspec.internal.matching.tree;

import java.util.Iterator;

public class EmptyIterator<T> implements Iterator<T> {
	
	private final static EmptyIterator<?> INSTANCE = new EmptyIterator<Object>();
	
	@SuppressWarnings("unchecked")
	public static <T> EmptyIterator<T> getInstance() {
		return (EmptyIterator<T>) INSTANCE;
	}
	
	private EmptyIterator() {
		super();
	}

	@Override
	public boolean hasNext() {
		return false;
	}

	@Override
	public T next() {
		return null;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
