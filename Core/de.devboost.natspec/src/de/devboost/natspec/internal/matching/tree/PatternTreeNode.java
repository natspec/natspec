package de.devboost.natspec.internal.matching.tree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * A {@link PatternTreeNode} represents a node in a tree of patterns. Each node
 * in such a tree corresponds to a {@link ISyntaxPatternPart} that is shared by
 * multiple syntax patterns. For example, all patterns that start with the same
 * static word, can be represented by one {@link PatternTreeNode} which in turn
 * owns child nodes for all different variations of the patterns.
 * 
 * The aim of {@link PatternTreeNode}s is to obtain a more compact
 * representation of syntax pattern that helps to reduce the effort for matching
 * patterns. The reduced effort is implemented by matching shared prefixes for
 * sets of syntax patterns only once instead of matching all patterns
 * individually.
 */
// TODO Use childrenMap only for the root node
public class PatternTreeNode<T> {

	private final int depth;
	private final Map<SyntaxPatternPartHashKey, PatternTreeNode<T>> childrenMap = new LinkedHashMap<SyntaxPatternPartHashKey, PatternTreeNode<T>>();
	private List<PatternTreeNode<T>> children;

	private boolean wasSplit = false;
	private List<ISyntaxPattern<T>> patterns;

	public PatternTreeNode(Collection<ISyntaxPattern<T>> patterns) {
		this(patterns, 0);
	}
	
	private PatternTreeNode(int depth) {
		this(Collections.<ISyntaxPattern<T>>emptyList(), depth);
	}
	
	private PatternTreeNode(Collection<ISyntaxPattern<T>> patterns, int depth) {
		super();
		// We create a copy of the patterns list.
		this.patterns = new ArrayList<ISyntaxPattern<T>>(patterns);
		this.depth = depth;
	}
	
	public void split() {
		if (wasSplit) {
			return;
		}
		wasSplit = true;
		
		// These are the patterns that 'end' at this node, because they do not have further parts.
		ArrayList<ISyntaxPattern<T>> leafPatterns = new ArrayList<ISyntaxPattern<T>>();
		
		for (ISyntaxPattern<T> pattern : patterns) {
			// Use remaining parts instead of all parts
			List<ISyntaxPatternPart> parts = pattern.getParts();
			if (depth >= parts.size()) {
				leafPatterns.add(pattern);
				continue;
			}
			ISyntaxPatternPart part = parts.get(depth);
			PatternTreeNode<T> childNode = childrenMap.get(new SyntaxPatternPartHashKey(part));
			if (childNode == null) {
				childNode = new PatternTreeNode<T>(depth + 1);
				childrenMap.put(new SyntaxPatternPartHashKey(part), childNode);
			}
			childNode.addPattern(pattern);
		}

		// Since the list of leaf patterns will not change anymore, we can
		// compact the list.
		leafPatterns.trimToSize();
		// We replace the original list of patterns with the 'leaf pattern'
		// (i.e., the patterns which have not been moved to child nodes).
		this.patterns = leafPatterns;
		
		this.children = new ArrayList<PatternTreeNode<T>>(childrenMap.values());
	}

	private void addPattern(ISyntaxPattern<T> pattern) {
		this.patterns.add(pattern);
	}

	public Map<SyntaxPatternPartHashKey, PatternTreeNode<T>> getChildMap() {
		return childrenMap;
	}

	public Iterator<ISyntaxPattern<T>> getPatterns() {
		return getPatterns(false);
	}
	
	public Iterator<ISyntaxPattern<T>> getPatterns(boolean includeChildPatterns) {
		if (includeChildPatterns) {
			if (childrenMap.isEmpty()) {
				if (patterns.isEmpty()) {
					return EmptyIterator.getInstance();
				} else {
					Iterator<ISyntaxPattern<T>> patternIterator = getPatternIterator();
					return new PatternTreeNodeIterator<T>(patternIterator, null);
				}
			} else {
				Iterator<ISyntaxPattern<T>> patternIterator = getPatternIterator();
				return new PatternTreeNodeIterator<T>(patternIterator, childrenMap.values());
			}
		} else {
			return getPatternIterator();
		}
	}

	private Iterator<ISyntaxPattern<T>> getPatternIterator() {
		if (patterns.isEmpty()) {
			return EmptyIterator.getInstance();
		} else {
			return patterns.iterator();
		}
	}
	
	public int getDepth() {
		return depth;
	}

	public boolean hasChildren() {
		return !childrenMap.isEmpty();
	}

	@SuppressWarnings("unchecked")
	public void visit(PatternVisitor patternVisitor,
			boolean includeChildPatterns) {

		int patternSize = patterns.size();
		for (int i = 0; i < patternSize; i++) {
			ISyntaxPattern<T> pattern = patterns.get(i);
			patternVisitor.visit((ISyntaxPattern<Object>) pattern);
		}
		
		if (!includeChildPatterns) {
			return;
		}

		if (children == null) {
			return;
		}

		int childSize = children.size();
		for (int i = 0; i < childSize; i++) {
			PatternTreeNode<T> childNode = children.get(i);
			childNode.visit(patternVisitor, includeChildPatterns);
		}
	}
}
