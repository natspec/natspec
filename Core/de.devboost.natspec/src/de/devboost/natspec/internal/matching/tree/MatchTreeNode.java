package de.devboost.natspec.internal.matching.tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.devboost.natspec.Word;
import de.devboost.natspec.internal.matching.list.WindowedList;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * A {@link MatchTreeNode} represents a node in a match tree for syntax
 * patterns. Each {@link MatchTreeNode} represents the result of matching a
 * {@link ISyntaxPatternPart} in a {@link PatternTreeNode}.
 */
public class MatchTreeNode<T> {

	private final MatchTreeNode<T> parent;
	protected final ISyntaxPatternPart part;
	protected final PatternTreeNode<T> patternTreeNode;

	protected ISyntaxPatternPartMatch match;
	private T userData;
	protected boolean startedMatching;
	protected boolean noRemainingWords;
	private List<MatchTreeNode<T>> children;

	public MatchTreeNode(MatchTreeNode<T> parent, ISyntaxPatternPart part,
			PatternTreeNode<T> patternTreeNode) {
		
		super();
		this.parent = parent;
		this.part = part;
		this.patternTreeNode = patternTreeNode;
	}
	
	protected boolean matchChildren(WindowedList<Word> remainingWords, IPatternMatchContext context) {
		boolean foundMatchingChild = false;
		for (MatchTreeNode<T> child : getChildren()) {
			foundMatchingChild |= child.match(remainingWords, context);
		}
		return foundMatchingChild;
	}

	public boolean match(WindowedList<Word> words, IPatternMatchContext context) {
		this.startedMatching = true;

		if (match != null) {
			throw new IllegalStateException(
					"Method match() must not be called more than once.");
		}
		
		match = part.match(words, context);
		if (match != null) {
			List<Word> matchedWords = match.getMatchedWords();
			/* Continue match for remaining words. */
			int matched = matchedWords.size();
			int remaining = words.size() - matchedWords.size();
			words.window(matched, remaining);

			// Trigger matching for child nodes in tree
			split(context);
			boolean foundMatchingChild = matchChildren(words, context);
			if (!foundMatchingChild) {
				/*
				 * Try to remove optional words from this match to make the
				 * child nodes match.
				 */
				List<Word> optionalMatchedWords = match
						.getOptionalMatchedWords();

				/* Start with a completely windowed list of matched words */
				WindowedList<Word> optionalEnd = new WindowedList<Word>(
						optionalMatchedWords);
				optionalEnd.window(optionalEnd.size(), 0);

				for (int i = 1; i <= optionalMatchedWords.size(); i++) {
					/*
					 * Increase the visibility of the matched words element by
					 * element, starting at the end.
					 */
					optionalEnd.increaseFrontWindow(1);
					/*
					 * Decrease the words not matched by the parent pattern by
					 * one element.
					 */
					words.increaseFrontWindow(1);

					/* Check if a matching child can be found. */
					foundMatchingChild = matchChildren(words, context);

					if (foundMatchingChild) {
						match.removeOptionalMatchedWords(optionalEnd);
						break;
					}
				}

				/*
				 * If no child match was found, decrease the window for the
				 * words match by the parent pattern again.
				 */
				words.decreaseFrontWindow(optionalEnd.size());
			}

			noRemainingWords = words.isEmpty();

			/*
			 * Undo window shrinking for matched words to allow further matches
			 * for the same words.
			 */
			words.increaseFrontWindow(matched);

			return true;
		}
		return false;
	}

	private void split(IPatternMatchContext context) {
		// TODO Merge split() with getChildMap()
		patternTreeNode.split();

		Map<SyntaxPatternPartHashKey, PatternTreeNode<T>> childMap = patternTreeNode
				.getChildMap();
		Set<SyntaxPatternPartHashKey> hashedChildParts = childMap.keySet();
		List<MatchTreeNode<T>> children = getChildren(hashedChildParts.size());
		for (SyntaxPatternPartHashKey hashedChildPart : hashedChildParts) {
			PatternTreeNode<T> newChild = childMap.get(hashedChildPart);
			ISyntaxPatternPart childPart = hashedChildPart
					.getSyntaxPatternPart();
			MatchTreeNode<T> childNode = new MatchTreeNode<T>(
					this, childPart, newChild);
			children.add(childNode);
		}
	}

	public Iterator<ISyntaxPattern<T>> getPatterns() {
		return patternTreeNode.getPatterns();
	}

	public Iterator<ISyntaxPattern<T>> getPatterns(boolean includeChildPatterns) {
		return patternTreeNode.getPatterns(includeChildPatterns);
	}
	
	public boolean isComplete() {
		return startedMatching && match != null && hasNoChildrenAndNoRemainingWords();
	}

	private boolean hasNoChildrenAndNoRemainingWords() {
		if (children == null || children.isEmpty()) {
			return noRemainingWords;
		} else {
			return true;
		}
	}

	public T getUserData() {
		return userData;
	}

	public List<MatchTreeNode<T>> getChildren() {
		return getChildren(10);
	}
	
	public boolean hasChildren() {
		if (children == null) {
			return false;
		}
		return !getChildren().isEmpty();
	}
	
	public List<MatchTreeNode<T>> getChildren(int initialSize) {
		if (children == null) {
			children = new ArrayList<MatchTreeNode<T>>(initialSize);
		}
		return children;
	}

	public MatchTreeNode<T> getParent() {
		return parent;
	}

	public ISyntaxPatternPart getPart() {
		return part;
	}

	public ISyntaxPatternPartMatch getMatch() {
		return match;
	}

	public int getDepth() {
		return patternTreeNode.getDepth();
	}

	public boolean hasRemainingWords() {
		return !noRemainingWords;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + part + " - complete:" + isComplete();
	}

	public void visit(PatternVisitor patternVisitor,
			boolean includeChildPatterns) {
		patternTreeNode.visit(patternVisitor, includeChildPatterns);
	}
}
