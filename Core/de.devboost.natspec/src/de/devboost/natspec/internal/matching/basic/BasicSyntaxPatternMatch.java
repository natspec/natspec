package de.devboost.natspec.internal.matching.basic;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

/**
 * This class in the default implementation for the ISyntaxPatternMatch 
 * interface. It is not intended to create subclasses of this class.
 *
 * @param <UserDataType> the type of object that can be attached to the match.
 */
public class BasicSyntaxPatternMatch<UserDataType> implements
		ISyntaxPatternMatch<UserDataType> {

	private final ISyntaxPattern<UserDataType> pattern;
	private final IPatternMatchContext context;

	/**
	 * A mapping from the pattern parts to their actual matches. This mapping
	 * can be used to determine which part of a pattern has been mapped to which
	 * words.
	 * 
	 * This map is initialized lazily by {@link #getPartsToMatchesMap()}.
	 */
	private Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap;
	
	/**
	 * A flag to keep track whether {@link #match(List)} was called. This is 
	 * required to make sure that {@link #isComplete()} returns 
	 * <code>true</code> only if matching was actually performed. 
	 */
	private boolean startedMatching = false;
	
	/**
	 * A flag the is used by {@link #match(List)} to store the final state of
	 * the matching process.
	 */
	private boolean matches = true;
	
	/**
	 * An index of the next pattern part to match against the list of words
	 * passed to {@link #match(List)}. This field is used to preserve the state
	 * of the matching process once {@link #match(List)} has finished.
	 */
	private int nextPartToMatch = 0;

	private UserDataType userData;
	
	private List<Word> remainingWords;
	private List<Word> words;

	public BasicSyntaxPatternMatch(ISyntaxPattern<UserDataType> pattern, IPatternMatchContext context) {
		super();
		this.pattern = pattern;
		this.context = context;
	}
	
	@Override
	public void match(List<Word> words) {
		startedMatching = true;
		List<ISyntaxPatternPart> parts = pattern.getParts();
		int partCount = parts.size();
		if (nextPartToMatch >= partCount) {
			// no more parts to match: failure (this method is only called if
			// their are words left).
			matches = false;
			return;
		}
		
		// at the beginning of the matching process all words remain to be 
		// matched
		if (remainingWords == null) {
			remainingWords = new ArrayList<Word>(words);
		} else {
			remainingWords.clear();
			remainingWords.addAll(words);
		}
		
		boolean matched = false;
		do {
			// try to match the next part of the syntax pattern
			ISyntaxPatternPart nextPart = parts.get(nextPartToMatch);
			nextPartToMatch++;
			ISyntaxPatternPartMatch match = nextPart.match(remainingWords, context);
			if (match == null) {
				
				ISyntaxPatternPart previousPart = null;
				if (nextPartToMatch > 1) {
					previousPart = parts.get(nextPartToMatch - 2);
				}
				
				// if there is a previous part, we try to remove optional words
				// to make the next part match
				if (previousPart != null) {
					ISyntaxPatternPartMatch previousMatch = getPartFromPartsToMatchesMap(previousPart);
					if (previousMatch != null) {
						List<Word> optionalMatchedWords = previousMatch.getOptionalMatchedWords();
						for (int i = 1; i <= optionalMatchedWords.size(); i++) {
							List<Word> optionalEnd = new ArrayList<Word>();
							optionalEnd.addAll(optionalMatchedWords.subList(optionalMatchedWords.size() - i, optionalMatchedWords.size()));
							List<Word> endOfPreviousPlusRemaining = new ArrayList<Word>();
							endOfPreviousPlusRemaining.addAll(optionalEnd);
							endOfPreviousPlusRemaining.addAll(remainingWords);
							match = nextPart.match(endOfPreviousPlusRemaining, context);
							if (match != null) {
								previousMatch.removeOptionalMatchedWords(optionalEnd);
								remainingWords.addAll(0, optionalEnd);
								// found match using a sub list of the previous match
								break;
							}
						}
					}
				}
				
				if (match == null) {
					// if the part does still not match, we can stop
					break;
				}
			}
			List<Word> matchedWords = match.getMatchedWords();
			int matchedWordCount = matchedWords.size();
			matched = match.hasMatched();
			if (matched) {
				getPartsToMatchesMap().put(nextPart, match);
			}
			// remove the matched words from head of list to obtain a new list
			// of the remaining words
			remainingWords = remainingWords.subList(matchedWordCount, remainingWords.size());
			
		} while (matched && nextPartToMatch < partCount);
		
		boolean allWordsWereMatched = remainingWords.isEmpty();
		boolean allPartsWereUsed = nextPartToMatch >= partCount;
		boolean allPartsWereMatched = getPartsToMatchesMapSize() >= partCount;
		
		matches = allWordsWereMatched && allPartsWereUsed && allPartsWereMatched;
		if (matches) {
			this.words = words;
		}
	}

	private ISyntaxPatternPartMatch getPartFromPartsToMatchesMap(
			ISyntaxPatternPart previousPart) {
		// We avoid the initialization that happens in getPartsToMatchesMap(),
		// because it is not required for getting a value if the map is still
		// null.
		if (partsToMatchesMap == null) {
			return null;
		}
		return getPartsToMatchesMap().get(previousPart);
	}

	private int getPartsToMatchesMapSize() {
		// We avoid the initialization that happens in getPartsToMatchesMap(),
		// because it is not required for getting size if the map is still null.
		if (partsToMatchesMap == null) {
			return 0;
		}
		return getPartsToMatchesMap().values().size();
	}

	@Override
	public boolean isComplete() {
		return matches && startedMatching;
	}

	@Override
	public ISyntaxPattern<UserDataType> getPattern() {
		return pattern;
	}
	
	@Override
	public IPatternMatchContext getContext() {
		return context;
	}

	@Override
	public Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> getPartsToMatchesMap() {
		if (partsToMatchesMap == null) {
			// We must use an IdentityHashMap because we cannot rely on the
			// correct implementation of hashcode() and equals() in syntax
			// pattern parts.
			partsToMatchesMap = new IdentityHashMap<ISyntaxPatternPart, ISyntaxPatternPartMatch>();
		}
		return partsToMatchesMap;
	}

	@Override
	public UserDataType getUserData() {
		return userData;
	}

	@Override
	public void createUserData() {
		this.userData = getPattern().createUserData(this);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + partsToMatchesMap;
	}

	@Override
	public void reset() {
		partsToMatchesMap = null;
		startedMatching = false;
		matches = true;
		nextPartToMatch = 0;
		userData = null;
		words = null;
	}

	@Override
	public List<Word> getWords() {
		return words;
	}
}
