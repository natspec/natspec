package de.devboost.natspec.internal.matching.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * This is a {@link List} that in fact is immutable, but of which parts can be
 * shadowed by calling the {@link #window(int, int)} method.
 * 
 * @author Claas Wilke
 */
public class WindowedList<ElementType> implements List<ElementType> {

	/** The contained {@link List} of this {@link WindowedList}. */
	private List<ElementType> containedList;

	/**
	 * The lower bound of this {@link WindowedList}. This is a index for the
	 * {@link WindowedList#containedList} to be used internally. All elements
	 * below this index are not visible. The element identified by the lower
	 * bound is included in the elements visible for this {@link WindowedList}.
	 */
	private int lowerBound;

	/**
	 * The upper bound of this {@link WindowedList} This is a index for the
	 * {@link WindowedList#containedList} to be used internally. All elements
	 * above this index are not visible. The element identified by the upper
	 * bound is included in the elements visible for this {@link WindowedList}.
	 */
	private int upperBound;

	/**
	 * Creates a new {@link WindowedList} based on the elements from the given
	 * {@link List}.
	 * 
	 * @param elements
	 *            The elements of this {@link WindowedList}. <strong>This
	 *            {@link List} will be copied for the constructed
	 *            {@link WindowedList}.</strong>
	 */
	public WindowedList(List<ElementType> elements) {
		containedList = new ArrayList<ElementType>(elements);
		lowerBound = 0;
		upperBound = containedList.size() - 1;
	}

	/**
	 * Creates a new {@link WindowedList} based on the elements from the given
	 * {@link List}.
	 * 
	 * @param elements
	 *            The elements of this {@link WindowedList}. <strong>This
	 *            {@link List} will not be copied for the constructed
	 *            {@link WindowedList}.</strong>
	 */
	private WindowedList(List<ElementType> elements, int lowerBound,
			int upperBound) {
		if (lowerBound < 0) {
			throw new IndexOutOfBoundsException("LowerBound cannot be below 0.");
		} else if (upperBound >= elements.size()) {
			throw new IndexOutOfBoundsException(
					"UpperBound cannot be above the size of the given elements List.");
		} else if (upperBound - lowerBound < -1) {
			throw new IndexOutOfBoundsException(
					"UpperBound cannot be smaller than lower bound.");
		}

		containedList = elements;
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
	}

	@Override
	public boolean add(ElementType e) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public void add(int index, ElementType element) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public boolean addAll(Collection<? extends ElementType> c) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public boolean addAll(int index, Collection<? extends ElementType> c) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public boolean contains(Object o) {
		return indexOf(o) >= 0;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		Iterator<?> it = c.iterator();
		while (it.hasNext()) {
			if (indexOf(it.next()) == -1) {
				return false;
			}
		}

		return true;
	}

	@Override
	public void clear() {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	/*
	 * Copied from {@link AbstractList}.
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof List))
			return false;

		ListIterator<ElementType> e1 = listIterator();
		ListIterator<?> e2 = ((List<?>) o).listIterator();
		while (e1.hasNext() && e2.hasNext()) {
			ElementType o1 = e1.next();
			Object o2 = e2.next();
			if (!(o1 == null ? o2 == null : o1.equals(o2)))
				return false;
		}
		return !(e1.hasNext() || e2.hasNext());
	}

	@Override
	public ElementType get(int index) {
		if (index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException("Index must be between 0 and "
					+ (size() - 1) + "!");
		}

		return containedList.get(index + lowerBound);
	}

	/*
	 * Copied from {@link AbstractList}.
	 */
	public int hashCode() {
		int hashCode = 1;
		for (int index = 0; index < size(); index++) {
			ElementType obj = get(index);
			hashCode = 31 * hashCode + (obj == null ? 0 : obj.hashCode());
		}
		return hashCode;
	}

	/**
	 * Decreases the front window of this {@link WindowedList} by the given
	 * number of elements. <strong>Means, that the visible window of this
	 * {@link WindowedList} will get smaller!</strong>
	 * 
	 * @param elements
	 *            The number of elements to decrease the front window.
	 */
	/* TODO cwilke: test me! */
	public void decreaseFrontWindow(int elements) {

		if (elements < 0) {
			throw new IllegalArgumentException(
					"Number of elements cannot be negative.");
		} else if (upperBound - lowerBound + 1 - elements < 0) {
			throw new IndexOutOfBoundsException("Tried to decrease front window by "
					+ elements + " but only " + (upperBound - lowerBound + 1)
					+ " elements left.");
		}

		/* If all elements match, increase the lower bound. */
		lowerBound += elements;
	}

	/**
	 * Increases the front window of this {@link WindowedList} by the given
	 * number of elements. <strong>Means, that the visible window of this
	 * {@link WindowedList} will get larger!</strong>
	 * 
	 * @param elements
	 *            The number of elements to increase the front window.
	 */
	public void increaseFrontWindow(int elements) {

		if (elements < 0) {
			throw new IllegalArgumentException(
					"Number of elements cannot be negative.");
		} else if (lowerBound - elements < 0) {
			throw new IndexOutOfBoundsException("Tried to increase front window by "
					+ elements + " but only " + (lowerBound + 1)
					+ " elements left.");
		}

		/* If all elements match, decrease the lower bound. */
		lowerBound -= elements;
	}

	@Override
	public int indexOf(Object o) {
		int index = -1;
		ListIterator<ElementType> it = listIterator();
		while (it.hasNext()) {
			ElementType element = it.next();
			if (null != element && element.equals(o)) {
				index = it.previousIndex();
				break;
			}
		}

		return index;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	@Override
	public Iterator<ElementType> iterator() {
		return listIterator();
	}

	@Override
	public int lastIndexOf(Object o) {
		int index = -1;
		ListIterator<ElementType> it = listIterator(size());
		while (it.hasPrevious()) {
			ElementType element = it.previous();
			if (null != element && element.equals(o)) {
				index = it.nextIndex();
				break;
			}
		}

		return index;
	}

	@Override
	public ListIterator<ElementType> listIterator() {
		return listIterator(0);
	}

	@Override
	public ListIterator<ElementType> listIterator(int index) {
		return new WindowedListIterator<ElementType>(this, index);
	}

	@Override
	public ElementType remove(int index) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public boolean remove(Object o) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public ElementType set(int index, ElementType element) {
		throw new UnsupportedOperationException("A "
				+ this.getClass().getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public int size() {
		return upperBound - lowerBound + 1;
	}

	@Override
	public List<ElementType> subList(int fromIndex, int toIndex) {
		return new WindowedList<ElementType>(containedList, lowerBound
				+ fromIndex, lowerBound + toIndex - 1);
	}

	@Override
	public Object[] toArray() {
		return containedList.subList(lowerBound, upperBound + 1).toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return containedList.subList(lowerBound, upperBound + 1).toArray(a);
	}

	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append("[");
		for (int index = lowerBound; index <= upperBound; index++) {
			result.append(containedList.get(index));
			if (index < upperBound) {
				result.append(", ");
			}
		}
		result.append("]");

		return result.toString();
	}

	/**
	 * Sets the window for this {@link WindowedList} relative to the current
	 * window.
	 * 
	 * @param fromIndex
	 *            The index of first element to be retained.
	 * @param size
	 *            The size of the window in elements
	 */
	public void window(int fromIndex, int size) {
		if (fromIndex < 0 || fromIndex > containedList.size() - lowerBound) {
			throw new IndexOutOfBoundsException(
					"From index must be between 0 and "
							+ (containedList.size() - lowerBound) + ".");
		}

		if (size < 0 || size > upperBound - lowerBound + 1) {
			throw new IndexOutOfBoundsException("Size must be between o and "
					+ (upperBound - lowerBound + 1) + ".");
		}

		lowerBound += fromIndex;
		upperBound = lowerBound + size - 1;
	}
}
