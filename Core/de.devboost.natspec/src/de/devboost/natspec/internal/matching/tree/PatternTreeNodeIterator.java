package de.devboost.natspec.internal.matching.tree;

import java.util.Collection;
import java.util.Iterator;

import de.devboost.natspec.patterns.ISyntaxPattern;

/**
 * The {@link PatternTreeNodeIterator} can be used to iterate of the patterns
 * that are associated with {@link PatternTreeNode}s. This class was introduced
 * to avoid the creation of collections (e.g., lists) when iterating over all
 * patterns in a tree rooted at a {@link PatternTreeNode}.
 */
public class PatternTreeNodeIterator<T> implements Iterator<ISyntaxPattern<T>> {
	
	private final Iterator<PatternTreeNode<T>> childNodesIterator;

	private Iterator<ISyntaxPattern<T>> currentIterator;
	
	public PatternTreeNodeIterator(Iterator<ISyntaxPattern<T>> iterator,
			Collection<PatternTreeNode<T>> childNodes) {
		this.currentIterator = iterator;
		if (childNodes == null) {
			this.childNodesIterator = EmptyIterator.getInstance();
		} else {
			this.childNodesIterator = childNodes.iterator();
		}
	}

	@Override
	public boolean hasNext() {
		while (currentIterator != null) {
			if (currentIterator.hasNext()) {
				return true;
			}
			currentIterator = findNextIterator();
		}
		
		return false;
	}

	private Iterator<ISyntaxPattern<T>> findNextIterator() {
		if (childNodesIterator.hasNext()) {
			return childNodesIterator.next().getPatterns(true);
		}

		return null;
	}

	@Override
	public ISyntaxPattern<T> next() {
		if (currentIterator == null) {
			return null;
		}
		
		return currentIterator.next();
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}
}
