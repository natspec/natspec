package de.devboost.natspec.internal.matching.tree;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

public class PatternVisitor {

	private final int partCount;
	private final boolean complete;
	private final Map<Integer, ISyntaxPatternPartMatch> partsToMatchesMap;
	private final IPatternMatchContext context;
	private final List<Word> words;
	private final List<ISyntaxPatternMatch<? extends Object>> matches;

	public PatternVisitor(int partCount, boolean complete,
			Map<Integer, ISyntaxPatternPartMatch> partsToMatchesMap,
			IPatternMatchContext context, List<Word> words,
			List<ISyntaxPatternMatch<? extends Object>> matches) {

		this.partCount = partCount;
		this.complete = complete;
		this.partsToMatchesMap = partsToMatchesMap;
		this.context = context;
		this.words = words;
		this.matches = matches;
	}

	public void visit(ISyntaxPattern<Object> pattern) {
		// Since we compute the completeness of the leaf node starting
		// at the last node that was matched, this list node may have
		// children (which we did not try to match). Thus, a leaf node
		// is only complete w.r.t. a pattern if the number of matched
		// nodes/parts equals the number of parts of the pattern.
		// Otherwise, the match is too short for the current pattern.
		List<ISyntaxPatternPart> parts = pattern.getParts();
		int partsSize = parts.size();
		boolean completeForPattern = partsSize == partCount && complete;
		// The 'partsToMatchesMap' is reused. Thus, we must create a
		// copy. This more efficient than creating a new map in the
		// first place, because the outer loop does not reach this
		// part of the code most of the time as many leaf nodes do yield
		// incomplete matches.
		ISyntaxPatternPartMatch[] partMatchArray = new ISyntaxPatternPartMatch[partsSize];
		Set<Entry<Integer, ISyntaxPatternPartMatch>> partsToMatchesEntries = partsToMatchesMap.entrySet();
		for (Entry<Integer, ISyntaxPatternPartMatch> partsToMatchesEntry : partsToMatchesEntries) {
			Integer depth = partsToMatchesEntry.getKey();
			ISyntaxPatternPartMatch partMatch = partsToMatchesEntry.getValue();
			partMatchArray[depth - 1] = partMatch;
		}

		TreeBasedPatternMatch<? extends Object> match = new TreeBasedPatternMatch<Object>(
				context, pattern, partMatchArray, completeForPattern,
				completeForPattern ? words : null);
		matches.add(match);
	}
}
