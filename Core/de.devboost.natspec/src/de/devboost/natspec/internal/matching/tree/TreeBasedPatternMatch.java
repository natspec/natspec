package de.devboost.natspec.internal.matching.tree;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

public class TreeBasedPatternMatch<T> implements ISyntaxPatternMatch<T> {

	private final IPatternMatchContext context;
	private final ISyntaxPattern<T> pattern;
	private final ISyntaxPatternPartMatch[] partMatchArray;
	private final List<Word> words;
	
	private T userData;
	private boolean complete;
	private Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap;
	
	public TreeBasedPatternMatch(IPatternMatchContext context,
			ISyntaxPattern<T> pattern,
			ISyntaxPatternPartMatch[] partMatchArray,
			boolean complete, List<Word> words) {
		
		super();
		this.context = context;
		this.pattern = pattern;
		this.partMatchArray = partMatchArray;
		this.complete = complete;
		this.words = words;
	}

	@Override
	public ISyntaxPattern<T> getPattern() {
		return pattern;
	}

	@Override
	public IPatternMatchContext getContext() {
		return context;
	}

	@Override
	public void match(List<Word> wordList) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isComplete() {
		return complete;
	}

	@Override
	public void createUserData() {
		this.userData = pattern.createUserData(this);
	}

	@Override
	public T getUserData() {
		return userData;
	}

	@Override
	public Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> getPartsToMatchesMap() {
		if (partsToMatchesMap == null) {
			// Initialize partsToMatchesMap
			partsToMatchesMap = new LinkedHashMap<ISyntaxPatternPart, ISyntaxPatternPartMatch>();
			List<ISyntaxPatternPart> parts = pattern.getParts();
			for (int i = 0; i < partMatchArray.length; i++) {
				ISyntaxPatternPartMatch partMatch = partMatchArray[i];
				if (partMatch != null) {
					ISyntaxPatternPart part = parts.get(i);
					partsToMatchesMap.put(part, partMatch);
				}
			}
		}
		return partsToMatchesMap;
	}

	@Override
	public void reset() {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Word> getWords() {
		return words;
	}
}
