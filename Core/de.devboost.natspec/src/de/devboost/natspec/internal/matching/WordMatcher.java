package de.devboost.natspec.internal.matching;

import java.util.Set;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.IWordMatcher;
import de.devboost.natspec.registries.SynonymProviderRegistry;

/**
 * The {@link WordMatcher} class is used to compare individual words against text fragments. We use this special class
 * instead of simply using {@link String#equals(Object)}, because we need to take synonyms into account and to match
 * case insensitive.
 */
public class WordMatcher implements IWordMatcher {

	private final String text;
	
	private transient IPatternMatchContext lastContext;
	private transient Set<String> lastSynonyms;

	/**
	 * Creates a matcher that can match the given text against other strings using
	 * {@link #match(PatternMatchContext, String)}.
	 */
	public WordMatcher(String text) {
		super();
		this.text = text;
	}

	/**
	 * Matches the text in this word matcher against another text. Attention: 'otherText' must be in lower case!
	 */
	@Override
	public boolean match(IPatternMatchContext context, String otherText) {
		Set<String> synonyms = getSynonyms(context);
		if (context.matchFuzzy()) {
			for (String synonym : synonyms) {
				if (synonym.startsWith(otherText)) {
					return true;
				}
			}
			return false;
		} else {
			return synonyms.contains(otherText);
		}
	}

	/**
	 * Fetches all synonyms for the given context using the {@link SynonymProviderRegistry}. If synonyms for the same
	 * context were requested right before, the synonyms are returned from a cache.
	 */
	private Set<String> getSynonyms(IPatternMatchContext context) {
		// We do only refresh the synonyms if the context has changed. This is
		// for performance reasons.
		if (lastContext == context) {
			return lastSynonyms;
		}

		SynonymProviderRegistry registry = SynonymProviderRegistry.REGISTRY;
		lastSynonyms = registry.getSynonyms(context, text);
		lastContext = context;
		return lastSynonyms;
	}
}
