package de.devboost.natspec.internal.matching.list;

import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * A {@link ListIterator} implementation for the {@link WindowedList}
 * {@link List} implementation.
 * 
 * @author Claas Wilke
 * 
 * @param <ElementType>
 *            The element type of this {@link ListIterator}.
 */
public class WindowedListIterator<ElementType> implements
		ListIterator<ElementType> {

	/** The current index of this {@link WindowedListIterator}. */
	private int currentIndex;

	/**
	 * The {@link WindowedList} this {@link WindowedListIterator} belongs to.
	 */
	private List<ElementType> list;

	/**
	 * Hash code of the {@link WindowedList} of this
	 * {@link WindowedListIterator} to check for
	 * {@link ConcurrentModificationException}s.
	 */
	private final int listHash;

	/**
	 * Creates a new {@link WindowedListIterator} for a given
	 * {@link WindowedList}.
	 * 
	 * @param list
	 *            The {@link WindowedList}.
	 * @param index
	 *            The index to start with.
	 */
	protected WindowedListIterator(WindowedList<ElementType> list, int index) {
		this.list = list;
		this.listHash = list.hashCode();
		currentIndex = index;
	}

	@Override
	public void add(ElementType e) {
		throw new UnsupportedOperationException("A "
				+ WindowedList.class.getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public boolean hasNext() {
		checkModification();
		return currentIndex < list.size();
	}

	@Override
	public boolean hasPrevious() {
		checkModification();
		return currentIndex > 0;
	}

	@Override
	public ElementType next() {
		checkModification();
		try {
			ElementType result = list.get(currentIndex);
			currentIndex++;
			return result;
		} catch (IndexOutOfBoundsException e) {
			throw new NoSuchElementException(e.getMessage());
		}
	}

	@Override
	public int nextIndex() {
		checkModification();
		return currentIndex;
	}

	@Override
	public ElementType previous() {
		checkModification();
		try {
			currentIndex--;
			ElementType result = list.get(currentIndex);
			return result;
		} catch (IndexOutOfBoundsException e) {
			throw new NoSuchElementException(e.getMessage());
		}
	}

	@Override
	public int previousIndex() {
		checkModification();
		return currentIndex - 1;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("A "
				+ WindowedList.class.getName()
				+ " is immutable and, thus, does not support this method.");
	}

	@Override
	public void set(ElementType e) {
		throw new UnsupportedOperationException("A "
				+ WindowedList.class.getName()
				+ " is immutable and, thus, does not support this method.");
	}

	/**
	 * Helper method checking for {@link ConcurrentModificationException}s.
	 */
	private void checkModification() {
		if (list.hashCode() != listHash) {
			throw new ConcurrentModificationException("WindowedList " + list
					+ " has been modified.");
		}
	}
}
