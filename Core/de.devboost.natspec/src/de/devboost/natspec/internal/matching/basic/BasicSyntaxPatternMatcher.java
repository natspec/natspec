package de.devboost.natspec.internal.matching.basic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.internal.matching.AbstractSyntaxPatternMatcher;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.registries.SyntaxPatternRegistry;

/**
 * The SyntaxPatternMatcher is the heart of the NatSpec framework. It is
 * responsible to find valid matches for syntax patterns. In contrast to the
 * {@link MatchService} class, the {@link BasicSyntaxPatternMatcher} does not
 * use the {@link SyntaxPatternRegistry}, but requires clients to pass the set
 * of patterns to find matches for directly.
 */
public class BasicSyntaxPatternMatcher extends AbstractSyntaxPatternMatcher {

	@Override
	public List<ISyntaxPatternMatch<? extends Object>> match(List<Word> words,
			Collection<ISyntaxPattern<? extends Object>> patterns,
			IPatternMatchContext context, boolean includeIncompleteMatches) {

		// create fresh match objects
		List<ISyntaxPatternMatch<? extends Object>> matches = createNewMatches(patterns, context);
		
		// perform the actual matching
		matchInternal(words, matches);
		
		// select the requested matches
		List<ISyntaxPatternMatch<? extends Object>> resultingMatches = prepareResult(
				matches, includeIncompleteMatches);
		return resultingMatches;
	}

	private List<ISyntaxPatternMatch<? extends Object>> createNewMatches(
			Collection<ISyntaxPattern<? extends Object>> patterns,
			IPatternMatchContext context) {
		
		List<ISyntaxPatternMatch<? extends Object>> matches = new ArrayList<ISyntaxPatternMatch<? extends Object>>(patterns.size());
		for (ISyntaxPattern<? extends Object> pattern : patterns) {
			ISyntaxPatternMatch<? extends Object> newPatternMatch = pattern.createPatternMatch(context);
			matches.add(newPatternMatch);
		}
		return matches;
	}

	private void matchInternal(List<Word> words,
			Collection<ISyntaxPatternMatch<? extends Object>> matches) {
		
		List<Word> wordList = Collections.unmodifiableList(words);
		for (ISyntaxPatternMatch<? extends Object> match : matches) {
			match.match(wordList);
		}
	}
}
