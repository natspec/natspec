package de.devboost.natspec.internal.parsing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.Word;
import de.devboost.natspec.parsing.ISentenceParser;

/**
 * The {@link SentenceParser} is the default and also the only implementation of
 * the {@link ISentenceParser} interface. It splits the text of sentences into
 * a sequence of words.
 */
public class SentenceParser implements ISentenceParser {

	@Override
	public List<Word> split(Sentence sentence) {
		String text = sentence.getText();
		if (text == null) {
			return Collections.emptyList();
		}
		
		List<Word> words = new ArrayList<Word>();
		int lastIndex = 0;
		int indexOfSpace;
		while ((indexOfSpace = text.indexOf(" ", lastIndex)) >= 0) {
			Word newWord = createWord(text, lastIndex, indexOfSpace);
			if (newWord != null) {
				words.add(newWord);
			}
			lastIndex = indexOfSpace + 1;
		}
		Word newWord = createWord(text, lastIndex, text.length());
		if (newWord != null) {
			words.add(newWord);
		}
		return words;
	}

	private Word createWord(String text, int start, int end) {
		String part = text.substring(start, end);
		if (part.length() == 0) {
			// we do not create empty words
			return null;
		}
		// we do remove leading tab characters from words
		while (part.startsWith("\t")) {
			part = part.substring(1, part.length());
			start++;
		}
		// we do remove following tab characters from words
		while (part.endsWith("\t")) {
			part = part.substring(0, part.length() - 1);
		}
		
		// create the word and set its properties
		Word word = NatspecFactory.eINSTANCE.createWord();
		word.setText(part);
		word.setOffset(start);
		return word;
	}
}
