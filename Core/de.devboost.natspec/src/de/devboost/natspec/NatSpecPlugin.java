package de.devboost.natspec;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.devboost.license.client.IClosedSourcePlugins;
import de.devboost.license.client.LicenseCheckUtil;

public class NatSpecPlugin extends AbstractUIPlugin {

	private static final String PLUGIN_ID = "de.devboost.natspec";

	private static final LicenseCheckUtil LICENSE_CHECK_UTIL = new LicenseCheckUtil(IClosedSourcePlugins.NATSPEC_FEATURE_ID);

	private static NatSpecPlugin instance;

	public static boolean USE_TREE_BASED_MATCHER = true;
	public static boolean USE_PRIORITIZER = false;

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		instance = this;
		
		USE_TREE_BASED_MATCHER = isFeatureToggleEnabled(
				"de.devboost.natspec.featuretoggle.treematcher.TreeMatcherFeatureToggle",
				"Tree-based pattern matcher", USE_TREE_BASED_MATCHER);
		
		USE_PRIORITIZER = isFeatureToggleEnabled(
				"de.devboost.natspec.featuretoggle.prioritizer.PrioritizerFeatureToggle",
				"Prioritizer", USE_PRIORITIZER);
	}

	protected boolean isFeatureToggleEnabled(String featureToggleClassName,
			String featureDescription, boolean defaultValue) {
		boolean enableFeature = defaultValue;
		try {
			Class.forName(featureToggleClassName);
			enableFeature = true;
		} catch (ClassNotFoundException e) {
			// Ignore this
		}
		logInfo(featureDescription + " is " + (enableFeature ? "active" : "inactive") + ".", null);
		return enableFeature;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		instance = null;
		super.stop(context);
	}

	public static NatSpecPlugin getInstance() {
		return instance;
	}

	// TODO Use PluginLogger instead
	/**
	 * Helper method for error logging.
	 * 
	 * @param message
	 *            the error message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	public static IStatus logError(String message, Throwable throwable) {
		return log(IStatus.ERROR, message, throwable);
	}

	/**
	 * Helper method for logging informations.
	 * 
	 * @param message
	 *            the information message to log
	 * @param throwable
	 *            the exception that describes the information in detail (can be
	 *            null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logInfo(String message, Throwable throwable) {
		return log(IStatus.INFO, message, throwable);
	}

	/**
	 * Helper method for logging warnings.
	 * 
	 * @param message
	 *            the warning message to log
	 * @param throwable
	 *            the exception that describes the warning in detail (can be
	 *            null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logWarning(String message, Throwable throwable) {
		return log(IStatus.WARNING, message, throwable);
	}

	/**
	 * Helper method for logging.
	 * 
	 * @param type
	 *            the type of the message to log
	 * @param message
	 *            the message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	protected static IStatus log(int type, String message, Throwable throwable) {
		IStatus status;
		if (throwable != null) {
			status = new Status(type, NatSpecPlugin.PLUGIN_ID, 0, message,
					throwable);
		} else {
			status = new Status(type, NatSpecPlugin.PLUGIN_ID, message);
		}
		final NatSpecPlugin pluginInstance = NatSpecPlugin.getInstance();
		if (pluginInstance == null) {
			System.err.println(message);
			if (throwable != null) {
				throwable.printStackTrace();
			}
		} else {
			pluginInstance.getLog().log(status);
		}
		return status;
	}

	public static boolean hasValidLicense() {
		return LICENSE_CHECK_UTIL.hasValidLicense();
	}
}
