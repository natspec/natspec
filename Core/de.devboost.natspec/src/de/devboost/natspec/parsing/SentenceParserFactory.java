package de.devboost.natspec.parsing;

import de.devboost.natspec.internal.parsing.SentenceParser;

public class SentenceParserFactory {

	public ISentenceParser createSentenceParser() {
		return new SentenceParser();
	}
}
