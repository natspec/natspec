package de.devboost.natspec.parsing;

import java.util.List;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.Word;

/**
 * {@link ISentenceParser}s can be used to split sentences into a list of words.
 * Currently, sentences are split at space characters. In addition to splitting
 * sentences, the {@link ISentenceParser} keeps track of the position of words
 * within the sentence (i.e., the start index).
 */
public interface ISentenceParser {

	/**
	 * Splits the given sentence into a list of words.
	 */
	public List<Word> split(Sentence sentence);

}