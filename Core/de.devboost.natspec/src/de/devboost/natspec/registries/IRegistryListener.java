package de.devboost.natspec.registries;

/**
 * {@link IRegistryListener}s can by attached to a registry to get notifications
 * when new entries are added or existing entries are removed from the registry.
 */
public interface IRegistryListener {

	/**
	 * Notifies the listener that the set of entries in the registry has 
	 * changed.
	 */
	public void notifyChanged();

	
}
