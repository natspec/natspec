package de.devboost.natspec.registries;

import java.util.List;

/**
 * {@link IRegistry} is an interface that defines the basic operations of a
 * registry. This interface is parameterized by the type that is registered.
 *
 * @param <RegisteredType> the type of object managed by this registry
 */
public interface IRegistry<RegisteredType> {

	/**
	 * Adds an entry to the registry.
	 */
	public void add(RegisteredType registryEntry);
	
	/**
	 * Removes an entry to the registry.
	 */
	public void remove(RegisteredType registryEntry);

	/**
	 * Returns all entries from the registry.
	 */
	public List<RegisteredType> getRegistryEntries();
}
