package de.devboost.natspec.registries;

/**
 * Listener to track pattern requests towards the {@link SyntaxPatternRegistry}
 * 
 * @author cwende
 * 
 */
public interface IPatternRequestedListener {

	/**
	 * Is called whenever someone requests patterns from the
	 * {@link SyntaxPatternRegistry}
	 */
	public void notifyPatternRequested(String uri);

}
