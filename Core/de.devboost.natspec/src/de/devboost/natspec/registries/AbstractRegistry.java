package de.devboost.natspec.registries;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRegistry<RegisteredType> implements
		IRegistry<RegisteredType> {

	/**
	 * The list of listeners that are interested in changes applied to this
	 * registry.
	 */
	private List<IRegistryListener> listeners = new ArrayList<IRegistryListener>();
	
	/**
	 * Adds a listener that will be notified when items are added or removed
	 * from the registry.
	 */
	public void addListener(IRegistryListener listener) {
		synchronized (listeners) {
			listeners.add(listener);
		}
	}

	/**
	 * Removes the listener so that it will not be notified anymore when items
	 * are added or removed from the registry.
	 */
	public void removeListener(IRegistryListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}

	public void notifyListeners() {
		synchronized (listeners) {
			for (IRegistryListener listener : listeners) {
				listener.notifyChanged();
			}
		}
	}
}
