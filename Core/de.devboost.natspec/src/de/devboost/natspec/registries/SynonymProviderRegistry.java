package de.devboost.natspec.registries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.matching.IPatternMatchContext;

/**
 * The {@link SynonymProviderRegistry} is a global registry where plug-ins can add synonym providers.
 */
// TODO context is not handled correctly
public class SynonymProviderRegistry extends AbstractRegistry<ISynonymProvider> {

	public final static SynonymProviderRegistry REGISTRY = new SynonymProviderRegistry();
	
	/**
	 * The list of registered providers.
	 */
	private List<ISynonymProvider> providers = new ArrayList<ISynonymProvider>();

	private Map<URI, Map<String, Set<String>>> uriToWordToSynonymSetMap = new LinkedHashMap<URI, Map<String,Set<String>>>();
	
	@Override
	public synchronized void add(ISynonymProvider newProvider) {
		this.providers.add(newProvider);
		notifyListeners();
	}

	@Override
	public synchronized void remove(ISynonymProvider provider) {
		this.providers.remove(provider);
		notifyListeners();
	}

	@Override
	public List<ISynonymProvider> getRegistryEntries() {
		return providers;
	}
	
	@Override
	public void notifyListeners() {
		uriToWordToSynonymSetMap = new LinkedHashMap<URI, Map<String,Set<String>>>();
		super.notifyListeners();
	}
	
	public Set<String> getSynonyms(IPatternMatchContext context, String word) {
		Map<String, Set<String>> wordToSynonymMap = getWordToSynonymMap(context);
		if (word == null) {
			return Collections.emptySet();
		}
		
		String lowerCase = word.toLowerCase();
		if (wordToSynonymMap == null) {
			return Collections.singleton(lowerCase);
		}
		
		Set<String> synonyms = wordToSynonymMap.get(lowerCase);
		if (synonyms == null) {
			return Collections.singleton(lowerCase);
		}
		
		return synonyms;
	}

	private Map<String, Set<String>> getWordToSynonymMap(IPatternMatchContext context) {
		URI uri = context.getContextURI();
		
		Map<String, Set<String>> wordToSynonymSetMap = uriToWordToSynonymSetMap.get(uri);
		if (wordToSynonymSetMap == null || context.getLocalSynonymProvider() != null) {
			wordToSynonymSetMap = refreshWordToSynonymMap(context);
			uriToWordToSynonymSetMap.put(uri, wordToSynonymSetMap);
		}
		
		return wordToSynonymSetMap;
	}

	private Map<String, Set<String>> refreshWordToSynonymMap(IPatternMatchContext context) {
		Map<String, Set<String>> wordToSynonymSetMap = new LinkedHashMap<String, Set<String>>();
		Set<Set<String>> newSynonymSets = getSynonyms(context);
		for (Set<String> newSynonymSet : newSynonymSets) {
			for (String word : newSynonymSet) {
				Set<String> synonymSet = wordToSynonymSetMap.get(word);
				if (synonymSet == null) {
					synonymSet = new LinkedHashSet<String>();
					wordToSynonymSetMap.put(word.toLowerCase(), synonymSet);
				}
				for (String newSynonym : newSynonymSet) {
					synonymSet.add(newSynonym.toLowerCase());
				}
			}
		}
		return wordToSynonymSetMap;
	}

	private Set<Set<String>> getSynonyms(IPatternMatchContext context) {
		Set<Set<String>> synonymSets = new LinkedHashSet<Set<String>>();

		// find providers for synonyms
		List<ISynonymProvider> providers = getRegistryEntries();
		ISynonymProvider localSynonymProvider = context.getLocalSynonymProvider();
		if (localSynonymProvider != null) {
			providers.add(localSynonymProvider);
		}
		
		URI uri = context.getContextURI();
		for (ISynonymProvider synonymProvider : providers) {
			synonymSets.addAll(synonymProvider.getSynonyms(uri));
		}
		return synonymSets;
	}
}
