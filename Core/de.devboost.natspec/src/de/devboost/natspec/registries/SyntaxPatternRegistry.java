package de.devboost.natspec.registries;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;

/**
 * The {@link SyntaxPatternRegistry} is a global registry where plug-ins can
 * register syntax pattern providers. These patterns are used by the
 * {@link MatchService} class.
 * 
 * TODO Rename this class to SyntaxPatternProviderRegistry and provide
 * deprecated wrapper to.
 */
public class SyntaxPatternRegistry extends
		AbstractRegistry<ISyntaxPatternProvider> {

	// TODO deal with different programming languages here
	public final static SyntaxPatternRegistry REGISTRY = new SyntaxPatternRegistry();

	/**
	 * The list of registered pattern providers.
	 */
	private List<ISyntaxPatternProvider> providers = new ArrayList<ISyntaxPatternProvider>();

	/**
	 * The list of listeners that are interested when patterns are requested.
	 */
	private List<IPatternRequestedListener> requestListeners = new ArrayList<IPatternRequestedListener>();
	
	// This is a singleton
	private SyntaxPatternRegistry() {
		super();
	}

	/**
	 * Adds a new syntax pattern provider to this registry.
	 */
	@Override
	public synchronized void add(ISyntaxPatternProvider newProvider) {
		this.providers.add(newProvider);
		notifyListeners();
	}

	/**
	 * Removes a syntax pattern provider from this registry.
	 */
	@Override
	public synchronized void remove(ISyntaxPatternProvider provider) {
		this.providers.remove(provider);
		notifyListeners();
	}

	/**
	 * Returns all registered syntax pattern providers.
	 */
	@Override
	public synchronized List<ISyntaxPatternProvider> getRegistryEntries() {
		// We return an unmodifiable copy of the provider list to avoid
		// ConcurrentModificationExceptions. To ensure that the list is not
		// changed while copying it, this method and all other methods that
		// access the <code>providers</code> field are synchronized.
		List<ISyntaxPatternProvider> copy = new ArrayList<ISyntaxPatternProvider>();
		copy.addAll(providers);
		List<ISyntaxPatternProvider> unmodifiableList = Collections
				.unmodifiableList(copy);
		return unmodifiableList;
	}

	/**
	 * Returns all registered syntax patterns that apply to the NatSpec file
	 * located at the given URI.
	 */
	public List<ISyntaxPattern<?>> getAllSyntaxPatterns(URI uri) {
		notifyRequestListeners(uri);
		List<ISyntaxPattern<?>> allPatterns = new LinkedList<ISyntaxPattern<?>>();
		List<ISyntaxPatternProvider> providers = getRegistryEntries();
		for (ISyntaxPatternProvider provider : providers) {
			allPatterns.addAll(provider.getPatterns(uri));
		}
		return allPatterns;
	}
	
	/**
	 * Adds a listener that will be notified when syntax patterns are requested
	 * from the registry.
	 */
	public void addRequestListener(IPatternRequestedListener listener) {
		synchronized (requestListeners) {
			requestListeners.add(listener);
		}
	}

	/**
	 * Notifies all registered request listeners that the patterns for the given
	 * URI were requested.
	 * 
	 * @param uri the URI of the NatSpec file for which pattern were requested
	 */
	public void notifyRequestListeners(URI uri) {
		synchronized (requestListeners) {
			for (IPatternRequestedListener listener : requestListeners) {
				String uriString = "null";
				if (uri != null) {
					uriString = uri.toString();
				}
				listener.notifyPatternRequested(uriString);
			}
		}
	}
}
