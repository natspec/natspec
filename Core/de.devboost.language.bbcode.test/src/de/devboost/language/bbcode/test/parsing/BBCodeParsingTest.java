package de.devboost.language.bbcode.test.parsing;

import org.junit.Test;

import de.devboost.language.bbcode.test.support.BBCodeTestSupport;

public class BBCodeParsingTest {

	protected BBCodeTestSupport testSupport = new BBCodeTestSupport();

	@SuppressWarnings("unused")
	@Test
	public void runTest() throws Exception {
		int used;
		// generated code will be inserted here
		// The code in this method is generated from: /de.devboost.language.bbcode.test/src/de/devboost/language/bbcode/test/parsing/BBCodeParsingTest.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Assert the following bbcoded text is parsed correctly: Cancel [color=darkgray]customer[/color] order [color=#11ff22][u]#1[/u][/color]
		de.devboost.language.bbcode.BBCodeText bBCodeText_Cancel__color_darkgray_customer__color__order__color__11ff22__u__1__u___color_ = testSupport.createBBCode(new java.lang.StringBuilder().append("Cancel").append(" ").append("[color=darkgray]customer[/color]").append(" ").append("order").append(" ").append("[color=#11ff22][u]#1[/u][/color]").toString(), false);
		// Assert result contains 1 underline styles
		testSupport.assertResultContainsUnderlineStyles(1, bBCodeText_Cancel__color_darkgray_customer__color__order__color__11ff22__u__1__u___color_);
		// Assert result contains 2 color styles
		testSupport.assertResultContainsColorStyles(2, bBCodeText_Cancel__color_darkgray_customer__color__order__color__11ff22__u__1__u___color_);
		// Assert result contains color style with text: customer
		de.devboost.language.bbcode.Color color_customer = testSupport.assertResultContainsColorStyleWithCustomer(new java.lang.StringBuilder().append("customer").toString(), bBCodeText_Cancel__color_darkgray_customer__color__order__color__11ff22__u__1__u___color_);
		// Assert the following bbcoded text is parsed correctly: [s]Cancel[/s] [b]customer[/b] order [u][i]#1[/i][/u]
		de.devboost.language.bbcode.BBCodeText bBCodeText__s_Cancel__s___b_customer__b__order__u__i__1__i___u_ = testSupport.createBBCode(new java.lang.StringBuilder().append("[s]Cancel[/s]").append(" ").append("[b]customer[/b]").append(" ").append("order").append(" ").append("[u][i]#1[/i][/u]").toString(), false);
		// Assert result contains 1 underline styles
		testSupport.assertResultContainsUnderlineStyles(1, bBCodeText__s_Cancel__s___b_customer__b__order__u__i__1__i___u_);
		// Assert result contains 1 strikethrough styles
		testSupport.assertResultContainsStrikethroughStyles(1, bBCodeText__s_Cancel__s___b_customer__b__order__u__i__1__i___u_);
		// Assert result contains 1 italic styles
		testSupport.assertResultContainsItalicStyles(1, bBCodeText__s_Cancel__s___b_customer__b__order__u__i__1__i___u_);
		// Assert result contains 1 bold styles
		testSupport.assertResultContainsBoldStyles(1, bBCodeText__s_Cancel__s___b_customer__b__order__u__i__1__i___u_);
		// Assert result contains 0 color styles
		testSupport.assertResultContainsColorStyles(0, bBCodeText__s_Cancel__s___b_customer__b__order__u__i__1__i___u_);
		
	}
}
