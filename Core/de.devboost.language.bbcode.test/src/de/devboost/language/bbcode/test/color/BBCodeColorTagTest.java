package de.devboost.language.bbcode.test.color;

import org.junit.Test;

import de.devboost.language.bbcode.test.support.BBCodeTestSupport;

public class BBCodeColorTagTest {

	protected BBCodeTestSupport testSupport = new BBCodeTestSupport();

	@SuppressWarnings("unused")
	@Test
	public void runTest() throws Exception {
		int used;
		// generated code will be inserted here
		// The code in this method is generated from: /de.devboost.language.bbcode.test/src/de/devboost/language/bbcode/test/color/BBCodeColorTagTest.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Assert the following bbcoded text is parsed correctly: [color=blue]Test[/color]
		de.devboost.language.bbcode.BBCodeText bBCodeText__color_blue_Test__color_ = testSupport.createBBCode(new java.lang.StringBuilder().append("[color=blue]Test[/color]").toString(), false);
		// Assert the bbcoded colors are valid
		de.devboost.language.bbcode.BBCodeText bBCodeText_ = testSupport.assertTheFollowingBbcodedColorIs(bBCodeText__color_blue_Test__color_, false);
		// Assert the following bbcoded text is parsed correctly: [color=#121212]Test[/color]
		de.devboost.language.bbcode.BBCodeText bBCodeText__color__121212_Test__color_ = testSupport.createBBCode(new java.lang.StringBuilder().append("[color=#121212]Test[/color]").toString(), false);
		// Assert the bbcoded colors are valid
		de.devboost.language.bbcode.BBCodeText bBCodeText_0 = testSupport.assertTheFollowingBbcodedColorIs(bBCodeText__color__121212_Test__color_, false);
		// Assert the following bbcoded text is not parsed correctly: [color=#121212%11]Test[/color]
		de.devboost.language.bbcode.BBCodeText bBCodeText__color__121212_11_Test__color__not = testSupport.createBBCode(new java.lang.StringBuilder().append("[color=#121212%11]Test[/color]").toString(), true);
		// Assert the bbcoded colors are not valid
		de.devboost.language.bbcode.BBCodeText bBCodeText_not = testSupport.assertTheFollowingBbcodedColorIs(bBCodeText__color__121212_11_Test__color__not, true);
		// Assert the following bbcoded text is parsed correctly: [color=121212]Test[/color]
		de.devboost.language.bbcode.BBCodeText bBCodeText__color_121212_Test__color_ = testSupport.createBBCode(new java.lang.StringBuilder().append("[color=121212]Test[/color]").toString(), false);
		// Assert the bbcoded colors are not valid
		de.devboost.language.bbcode.BBCodeText bBCodeText_not0 = testSupport.assertTheFollowingBbcodedColorIs(bBCodeText__color_121212_Test__color_, true);
		// Assert the following bbcoded text is parsed correctly: [color=#1212121111]Test[/color]
		de.devboost.language.bbcode.BBCodeText bBCodeText__color__1212121111_Test__color_ = testSupport.createBBCode(new java.lang.StringBuilder().append("[color=#1212121111]Test[/color]").toString(), false);
		// Assert the bbcoded colors are not valid
		de.devboost.language.bbcode.BBCodeText bBCodeText_not1 = testSupport.assertTheFollowingBbcodedColorIs(bBCodeText__color__1212121111_Test__color_, true);
		// Assert the following bbcoded text is parsed correctly: [color=#121212][b]Test[/color][/b]
		de.devboost.language.bbcode.BBCodeText bBCodeText__color__121212__b_Test__color___b_ = testSupport.createBBCode(new java.lang.StringBuilder().append("[color=#121212][b]Test[/color][/b]").toString(), false);
		// Assert the bbcoded colors are valid
		de.devboost.language.bbcode.BBCodeText bBCodeText_1 = testSupport.assertTheFollowingBbcodedColorIs(bBCodeText__color__121212__b_Test__color___b_, false);
		// Assert the following bbcoded text is parsed correctly: Cancel [color=darkgray]customer[/color] order [color=#11ff22][u]#1[/u][/color]
		de.devboost.language.bbcode.BBCodeText bBCodeText_Cancel__color_darkgray_customer__color__order__color__11ff22__u__1__u___color_ = testSupport.createBBCode(new java.lang.StringBuilder().append("Cancel").append(" ").append("[color=darkgray]customer[/color]").append(" ").append("order").append(" ").append("[color=#11ff22][u]#1[/u][/color]").toString(), false);
		// Assert the following bbcoded text is parsed correctly: Cancel customer order [u][i]#1[/i][/u]
		de.devboost.language.bbcode.BBCodeText bBCodeText_Cancel_customer_order__u__i__1__i___u_ = testSupport.createBBCode(new java.lang.StringBuilder().append("Cancel").append(" ").append("customer").append(" ").append("order").append(" ").append("[u][i]#1[/i][/u]").toString(), false);
		
	}
}
