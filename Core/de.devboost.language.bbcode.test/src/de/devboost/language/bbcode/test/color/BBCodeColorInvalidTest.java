package de.devboost.language.bbcode.test.color;

import org.junit.Test;

import de.devboost.language.bbcode.test.support.BBCodeTestSupport;

public class BBCodeColorInvalidTest {

	protected BBCodeTestSupport testSupport = new BBCodeTestSupport();

	@SuppressWarnings("unused")
	@Test
	public void runTest() throws Exception {
		int used;
		// generated code will be inserted here
		// The code in this method is generated from: /de.devboost.language.bbcode.test/src/de/devboost/language/bbcode/test/color/BBCodeColorInvalidTest.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Create color with value #2F303G
		de.devboost.language.bbcode.Color color__2F303G = testSupport.createColorWithValue("#2F303G");
		// Assert red has value 0
		testSupport.assertRedHasValue(0, color__2F303G);
		// Assert blue has value 0
		testSupport.assertBlueHasValue(0, color__2F303G);
		// Assert green has value 0
		testSupport.assertGreenHasValue(0, color__2F303G);
		// Assert color is not valid
		testSupport.assertColorIsValid(true, color__2F303G);
		
	}
}
