package de.devboost.language.bbcode.test.support;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.devboost.language.bbcode.BBCodeText;
import de.devboost.language.bbcode.Bold;
import de.devboost.language.bbcode.Color;
import de.devboost.language.bbcode.EmptyStyle;
import de.devboost.language.bbcode.Italic;
import de.devboost.language.bbcode.Strikethrough;
import de.devboost.language.bbcode.Style;
import de.devboost.language.bbcode.Underline;
import de.devboost.language.bbcode.custom.ColorCustom;
import de.devboost.language.bbcode.resource.bbcode.util.BbcodeResourceUtil;
import de.devboost.natspec.annotations.Many;
import de.devboost.natspec.annotations.TextSyntax;

public class BBCodeTestSupport {

	@TextSyntax("Create color with value #1")
	public Color createColorWithValue(String value) {
		Color colorCustom = new ColorCustom();
		colorCustom.setValue(value);
		return colorCustom;
	}

	@TextSyntax("Assert red has value #1")
	public void assertRedHasValue(int expectedRed, Color color) {
		assertEquals(expectedRed, color.getRed());
	}

	@TextSyntax("Assert green has value #1")
	public void assertGreenHasValue(int expectedGreen, Color color) {
		assertEquals(expectedGreen, color.getGreen());
	}

	@TextSyntax("Assert blue has value #1")
	public void assertBlueHasValue(int expecteBlue, Color color) {
		assertEquals(expecteBlue, color.getBlue());
	}

	@TextSyntax("Assert color is #1 valid")
	public void assertColorIsValid(boolean not, Color color) {
		assertEquals(!not, color.isValid());
	}

	@TextSyntax("Assert the following bbcoded text is #2 parsed correctly: #1")
	public BBCodeText createBBCode(@Many String text, boolean not) {
		BBCodeText bbCodedText = getBBCodedText(text);

		if (!not) {
			assertNotNull("Text is invalid but is expected to be valid", bbCodedText);
		} else {
			assertNull("The text is valid but is expected to be invalid", bbCodedText);
		}
		return bbCodedText;
	}

	private BBCodeText getBBCodedText(String text) {
		BBCodeText bbCodedText = null;
		Resource resource;
		try {
			resource = BbcodeResourceUtil.getResource(text.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
		if (resource != null) {
			EList<EObject> contents = resource.getContents();
			if (contents != null && !contents.isEmpty()) {
				EObject model = contents.get(0);
				if (model instanceof BBCodeText) {
					bbCodedText = (BBCodeText) model;
				}
			}
		}
		return bbCodedText;
	}

	@TextSyntax("Assert the bbcoded colors are #2 valid")
	public BBCodeText assertTheFollowingBbcodedColorIs(BBCodeText bbCodedText, boolean not) {
		if(not && (bbCodedText == null)){
			assertTrue(true);
			return null;
		}
		List<Color> invalidColorStyles = bbCodedText.getInvalidColorStyles();
		String invalidColors = "";
		if(!invalidColorStyles.isEmpty()){
			invalidColors += invalidColorStyles.get(0).getValue();
		}
		if(invalidColorStyles.size() > 1){
			for (int i = 1; i < invalidColorStyles.size(); i++) {
				invalidColors += ", " + invalidColorStyles.get(i).getValue();
			}
		}
		if (!not) {
			assertTrue("Text following colors are invalid but are expected to be valid: " + invalidColors, invalidColorStyles.isEmpty());
		} else {
			assertTrue("The contained colors are valid but are expected to be invalid", !invalidColorStyles.isEmpty());
		}
		return bbCodedText;
	}

	@TextSyntax("Assert result contains #1 underline styles")
	public void assertResultContainsUnderlineStyles(int count, BBCodeText bbcodeText) {
		List<Underline> underlines = filterContentsByClass(bbcodeText, Underline.class);
		assertEquals("Expected and current underline counts must be equal", count, underlines.size());
	}

	@TextSyntax("Assert result contains #1 color styles")
	public void assertResultContainsColorStyles(int count, BBCodeText bbcodeText) {
		List<Color> color = filterContentsByClass(bbcodeText, Color.class);
		assertEquals("Expected and current color counts must be equal", count, color.size());
	}
	
	@TextSyntax("Assert result contains #1 italic styles")
	public void assertResultContainsItalicStyles(int count, BBCodeText bbcodeText) {
		List<Italic> italic = filterContentsByClass(bbcodeText, Italic.class);
		assertEquals("Expected and current italic counts must be equal", count, italic.size());
	}
	
	@TextSyntax("Assert result contains #1 bold styles")
	public void assertResultContainsBoldStyles(int count, BBCodeText bbcodeText) {
		List<Bold> bold = filterContentsByClass(bbcodeText, Bold.class);
		assertEquals("Expected and current bold counts must be equal", count, bold.size());
	}
	
	@TextSyntax("Assert result contains #1 strikethrough styles")
	public void assertResultContainsStrikethroughStyles(int count, BBCodeText bbcodeText) {
		List<Strikethrough> strikethrough = filterContentsByClass(bbcodeText, Strikethrough.class);
		assertEquals("Expected and current strikethrough counts must be equal", count, strikethrough.size());
	}
	
	private <Metaclass> List<Metaclass> filterContentsByClass(BBCodeText bbcodeText, Class<Metaclass> metaclass) {
		List<Metaclass> filteredElements = new ArrayList<Metaclass>();
		TreeIterator<EObject> allContents = bbcodeText.eAllContents();
		while (allContents.hasNext()) {
			EObject element = (EObject) allContents.next();
			Class<? extends EObject> currentMetaclass = element.getClass();
			if(metaclass.isAssignableFrom(currentMetaclass)){
				Metaclass castedELement = metaclass.cast(element);
				filteredElements.add(castedELement);
			}
		}
		return filteredElements;
	}

	@TextSyntax("Assert result contains color style with text: #1")
	public Color assertResultContainsColorStyleWithCustomer(@Many String text, BBCodeText bbcodeText) {
		List<Color> colors = filterContentsByClass(bbcodeText, Color.class);
		Color foundColor = null;
		for (Color color : colors) {
			List<Style> children = color.getChildren();
			Style style = children.get(0);
			if(style instanceof EmptyStyle){
				EmptyStyle emptyStyle = (EmptyStyle) style;
				String childText = emptyStyle.getText().trim();
				if(text.trim().equals(childText)){
					foundColor = color;
					break;
				}
			}
		}
		assertTrue("Parsed bbcode text doesn't contain a color style with the given text", foundColor != null);
		return foundColor;
	}
}
