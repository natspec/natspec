package de.devboost.language.bbcode.test.color;

import org.junit.Test;

import de.devboost.language.bbcode.test.support.BBCodeTestSupport;

public class BBCodeColorHTMLTest {

	protected BBCodeTestSupport testSupport = new BBCodeTestSupport();

	@SuppressWarnings("unused")
	@Test
	public void runTest() throws Exception {
		int used;
		// generated code will be inserted here
		// The code in this method is generated from: /de.devboost.language.bbcode.test/src/de/devboost/language/bbcode/test/color/BBCodeColorHTMLTest.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Create color with value green
		de.devboost.language.bbcode.Color color_green = testSupport.createColorWithValue("green");
		// Assert color is valid
		testSupport.assertColorIsValid(false, color_green);
		// Assert red has value 0
		testSupport.assertRedHasValue(0, color_green);
		// Assert green has value 128
		testSupport.assertGreenHasValue(128, color_green);
		// Assert blue has value 0
		testSupport.assertBlueHasValue(0, color_green);
		// Create color with value white
		de.devboost.language.bbcode.Color color_white = testSupport.createColorWithValue("white");
		// Assert color is valid
		testSupport.assertColorIsValid(false, color_white);
		// Assert green has value 255
		testSupport.assertGreenHasValue(255, color_white);
		// Assert blue has value 255
		testSupport.assertBlueHasValue(255, color_white);
		// Assert red has value 255
		testSupport.assertRedHasValue(255, color_white);
		// Create color with value black
		de.devboost.language.bbcode.Color color_black = testSupport.createColorWithValue("black");
		// Assert color is valid
		testSupport.assertColorIsValid(false, color_black);
		// Assert green has value 0
		testSupport.assertGreenHasValue(0, color_black);
		// Assert blue has value 0
		testSupport.assertBlueHasValue(0, color_black);
		// Assert red has value 0
		testSupport.assertRedHasValue(0, color_black);
		// Create color with value blue
		de.devboost.language.bbcode.Color color_blue = testSupport.createColorWithValue("blue");
		// Assert green has value 0
		testSupport.assertGreenHasValue(0, color_blue);
		// Assert blue has value 255
		testSupport.assertBlueHasValue(255, color_blue);
		// Assert red has value 0
		testSupport.assertRedHasValue(0, color_blue);
		// Assert color is valid
		testSupport.assertColorIsValid(false, color_blue);
		// Create color with value red
		de.devboost.language.bbcode.Color color_red = testSupport.createColorWithValue("red");
		// Assert green has value 0
		testSupport.assertGreenHasValue(0, color_red);
		// Assert blue has value 0
		testSupport.assertBlueHasValue(0, color_red);
		// Assert red has value 255
		testSupport.assertRedHasValue(255, color_red);
		// Assert color is valid
		testSupport.assertColorIsValid(false, color_red);
		// Create color with value weiss
		de.devboost.language.bbcode.Color color_weiss = testSupport.createColorWithValue("weiss");
		// Assert color is not valid
		testSupport.assertColorIsValid(true, color_weiss);
		// Assert green has value 0
		testSupport.assertGreenHasValue(0, color_weiss);
		// Assert blue has value 0
		testSupport.assertBlueHasValue(0, color_weiss);
		// Assert red has value 0
		testSupport.assertRedHasValue(0, color_weiss);
		
	}
}
