package de.devboost.language.bbcode.test.color;

import org.junit.Test;

import de.devboost.language.bbcode.test.support.BBCodeTestSupport;

public class BBCodeColorTest {

	protected BBCodeTestSupport testSupport = new BBCodeTestSupport();

	@SuppressWarnings("unused")
	@Test
	public void runTest() throws Exception {
		int used;
		// generated code will be inserted here
		// The code in this method is generated from: /de.devboost.language.bbcode.test/src/de/devboost/language/bbcode/test/color/BBCodeColorTest.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Create color with value #2F3031
		de.devboost.language.bbcode.Color color__2F3031 = testSupport.createColorWithValue("#2F3031");
		// Assert red has value 47
		testSupport.assertRedHasValue(47, color__2F3031);
		// Assert green has value 48
		testSupport.assertGreenHasValue(48, color__2F3031);
		// Assert blue has value 49
		testSupport.assertBlueHasValue(49, color__2F3031);
		// Assert color is valid
		testSupport.assertColorIsValid(false, color__2F3031);
		
	}
}
