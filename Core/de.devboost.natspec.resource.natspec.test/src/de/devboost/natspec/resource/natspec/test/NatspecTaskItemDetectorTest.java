package de.devboost.natspec.resource.natspec.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import de.devboost.natspec.resource.natspec.mopp.NatspecTaskItem;
import de.devboost.natspec.resource.natspec.mopp.NatspecTaskItemDetector;

public class NatspecTaskItemDetectorTest {

	@Test
	public void testTaskItemDetection() {
		assertTaskItemCount("Some text", 0);
		assertTaskItemCount("// Some comment", 0);
		assertTaskItemCount("Some TODO outside of comment", 0);
		assertTaskItemCount("// Some TODO in a comment", 1);
		assertTaskItemCount("  // Some TODO in a comment with spaces before", 1);
		assertTaskItemCount("\t// Some TODO in a comment with tabs before", 1);
	}

	protected void assertTaskItemCount(String text, int expected) {
		int line = expected;
		int charStart = expected;
		List<NatspecTaskItem> taskItems = new NatspecTaskItemDetector().findTaskItems(text, line, charStart);
		assertEquals("Wrong number of detected task items", expected, taskItems.size());
	}
}
