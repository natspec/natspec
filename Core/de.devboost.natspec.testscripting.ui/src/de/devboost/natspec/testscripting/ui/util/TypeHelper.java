package de.devboost.natspec.testscripting.ui.util;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.IParameter;

public class TypeHelper {

	public List<String> getTypeNames(List<? extends IClass> parameterTypes) {
		List<String> parameterTypeNames = new ArrayList<String>();
		for (IClass parameterType : parameterTypes) {
			String name = parameterType.getQualifiedNameWithTypeArguments();
			parameterTypeNames.add(name);
		}
		return parameterTypeNames;
	}

	public List<String> getTypeNamesForParameters(
			List<? extends IParameter> parameters) {
		List<String> parameterTypeNames = new ArrayList<String>();
		for (IParameter parameter : parameters) {
			IClass parameterType = parameter.getType();
			String name = parameterType.getQualifiedNameWithTypeArguments();
			parameterTypeNames.add(name);
		}
		return parameterTypeNames;
	}
}
