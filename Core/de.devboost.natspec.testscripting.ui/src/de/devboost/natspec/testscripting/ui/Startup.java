package de.devboost.natspec.testscripting.ui;

import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IStartup;

import de.devboost.natspec.patterns.parts.DefaultSentenceMatchParameter;
import de.devboost.natspec.patterns.parts.DoubleArgument;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.resource.natspec.INatspecTokenStyle;
import de.devboost.natspec.resource.natspec.mopp.NatspecTokenStyle;
import de.devboost.natspec.resource.natspec.ui.CommentTokenDecorator;
import de.devboost.natspec.resource.natspec.ui.OutlineMarkerTokenDecorator;
import de.devboost.natspec.resource.natspec.ui.TokenStyleProvider;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.patterns.parts.BooleanParameter;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.DateParameter;
import de.devboost.natspec.testscripting.patterns.parts.PropertyWord;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		TestConnectorPlugin.getInstance();
		// This triggers the activation of this bundle, which causes some side
		// effects (e.g., registration of listeners).
		Activator.getDefault();

		registerTokenStyles();
	}

	private boolean isDarkTheme() {
		ThemeDetector themeDetector = new ThemeDetector();
		Display.getDefault().syncExec(themeDetector);
		boolean darkTheme = themeDetector.isDarkTheme();
		return darkTheme;
	}

	private void registerTokenStyles() {
		// register custom token styles
		addTokenStyle(PropertyWord.class, 0, 0, 192, true);
		registerTypeWordTokenStyle();

		boolean darkTheme = isDarkTheme();

		int red;
		int green;
		int blue;
		if (darkTheme) {
			red = 0xA0;
			green = 0xA0;
			blue = 0xA0;
		} else {
			red = 0x40;
			green = 0x40;
			blue = 0x40;
		}
		boolean isBold = true;

		addTokenStyle(IntegerArgument.class, red, green, blue, isBold);
		addTokenStyle(DoubleArgument.class, red, green, blue, isBold);
		addTokenStyle(StringParameter.class, red, green, blue, isBold);
		addTokenStyle(DateParameter.class, red, green, blue, isBold);
		addTokenStyle(BooleanParameter.class, red, green, blue, isBold);

		addTokenStyle(ComplexParameter.class, 127, 159, 191, isBold);

		addTokenStyle(CommentTokenDecorator.class, 34, 139, 34,
				false, false, false, false);
		
		addTokenStyle(OutlineMarkerTokenDecorator.class, 34, 139, 34,
				true, false, false, false);
		
		addTokenStyle(DefaultSentenceMatchParameter.class, 150, 150, 150,
				false, true, false, false);
	}

	// TypeWord is deprecated, but we do still add a token style until TypeWord
	// is entirely removed from NatSpec.
	@SuppressWarnings("deprecation")
	private void registerTypeWordTokenStyle() {
		addTokenStyle(de.devboost.natspec.testscripting.patterns.parts.TypeWord.class, 0, 0, 0, true);
	}

	private void addTokenStyle(Class<?> clazz, int red, int green, int blue,
			boolean bold) {
		addTokenStyle(clazz, red, green, blue, bold, false, false, false);
	}

	private void addTokenStyle(Class<?> clazz, int red, int green, int blue,
			boolean bold, boolean italic, boolean strikethrough,
			boolean underline) {
		int[] color = new int[] { red, green, blue };
		int[] backgroundColor = null;
		INatspecTokenStyle style = new NatspecTokenStyle(color,
				backgroundColor, bold, italic, strikethrough, underline);
		TokenStyleProvider.PROVIDER.addStyle(clazz, style);
	}
}
