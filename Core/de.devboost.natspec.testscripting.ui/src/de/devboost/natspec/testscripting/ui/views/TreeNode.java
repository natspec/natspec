package de.devboost.natspec.testscripting.ui.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;

class TreeNode implements IAdaptable {
	
	private String name;
	private TreeNode parent;
	private List<TreeNode> children = new ArrayList<TreeNode>();

	public TreeNode(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setParent(TreeNode parent) {
		this.parent = parent;
	}

	public TreeNode getParent() {
		return parent;
	}

	public void addChild(TreeNode child) {
		children.add(child);
		child.setParent(this);
	}

	public TreeNode[] getChildren() {
		return (TreeNode[]) children.toArray(new TreeNode[children
				.size()]);
	}

	public boolean hasChildren() {
		return children.size() > 0;
	}

	public String toString() {
		return getName();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object getAdapter(Class key) {
		return null;
	}
}
