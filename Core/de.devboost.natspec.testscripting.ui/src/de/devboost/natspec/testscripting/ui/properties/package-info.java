/**
 * This package contains NatSpec property pages.
 */
package de.devboost.natspec.testscripting.ui.properties;