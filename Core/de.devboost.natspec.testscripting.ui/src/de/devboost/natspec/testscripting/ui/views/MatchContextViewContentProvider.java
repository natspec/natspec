package de.devboost.natspec.testscripting.ui.views;

public class MatchContextViewContentProvider extends AbstractTreeContentProvider {

	public MatchContextViewContentProvider(MatchContextView matchContextView) {
		super(matchContextView);
	}

	@Override
	protected TreeNode createRootNodeForEmptyTree() {
		return new TreeNode(MatchContextView.NO_OBJECTS_IN_CONTEXT);
	}
}
