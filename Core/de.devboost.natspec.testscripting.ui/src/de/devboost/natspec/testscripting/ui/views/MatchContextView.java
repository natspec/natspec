package de.devboost.natspec.testscripting.ui.views;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.custom.CaretEvent;
import org.eclipse.swt.custom.CaretListener;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.StyledTextContent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.resource.natspec.INatspecOptions;
import de.devboost.natspec.resource.natspec.ui.NatspecEditor;
import de.devboost.natspec.resource.natspec.ui.NatspecImageProvider;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContext;
import de.devboost.natspec.testscripting.ui.Activator;

/**
 * The {@link MatchContextView} can be used to inspect the state of the pattern
 * match context for a NatSpec document depending on the current cursor
 * position. If shows all objects that were added to the context up to the
 * sentence located at the cursor position.
 */
public class MatchContextView extends AbstractView implements CaretListener {

	public static final String NO_OBJECTS_IN_CONTEXT = "No objects in context";

	private static final String HIDE_OVERRIDDEN_OBJECTS_ENABLED_PROPERTY = "hideOverriddenObjectsEnabled";
	
	private MatchContextViewContentProvider contentProvider;
	private StyledText currentTextWidget;
	
	/**
	 * The {@link URI} of the NatSpec document that is shown in the currently
	 * active editor.
	 */
	private URI currentURI;
	
	private IAction hideOverriddenObjectsAction;

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		
		contentProvider = new MatchContextViewContentProvider(this);

		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(new MatchContextViewLabelProvider());
		viewer.setInput(getViewSite());
	}
	
	@Override
	protected void createActions() {
		createHideOverriddenObjectsAction();
	}

	@Override
	protected void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(hideOverriddenObjectsAction);
	}

	private void createHideOverriddenObjectsAction() {
		String text = "Hide overridden objects";
		hideOverriddenObjectsAction = new Action(text, IAction.AS_CHECK_BOX) {

			@Override
			public void run() {
				refreshTreeContent();
				storeActionState(this, HIDE_OVERRIDDEN_OBJECTS_ENABLED_PROPERTY);
			}
		};
		
		hideOverriddenObjectsAction.setToolTipText(text);
		hideOverriddenObjectsAction.setImageDescriptor(NatspecImageProvider.INSTANCE.getImageDescriptor("platform:/plugin/" + Activator.PLUGIN_ID + "/icons/hide_overridden.png"));
		
		restoreActionState(hideOverriddenObjectsAction, HIDE_OVERRIDDEN_OBJECTS_ENABLED_PROPERTY);
	}

	private void registerCaretListener(IEditorPart activeEditor) {
		if (activeEditor instanceof NatspecEditor) {
			NatspecEditor natspecEditor = (NatspecEditor) activeEditor;
			registerCaretListener(natspecEditor);
		}
	}

	private void registerCaretListener(NatspecEditor natspecEditor) {
		Viewer viewer = natspecEditor.getViewer();
		if (viewer instanceof ITextViewer) {
			ITextViewer textViewer = (ITextViewer) viewer;
			StyledText textWidget = textViewer.getTextWidget();
			if (textWidget != currentTextWidget) {
				if (currentTextWidget != null
						&& !currentTextWidget.isDisposed()) {
					currentTextWidget.removeCaretListener(this);
				}
				textWidget.addCaretListener(this);
				currentTextWidget = textWidget;
				currentURI = natspecEditor.getResource().getURI();
			} else {
				// Text widget has not changed. no need to remove or add caret
				// listener.
			}
		}
		refreshTreeContent();
	}

	@Override
	public void caretMoved(CaretEvent event) {
		refreshTreeContent();
	}
	
	private void refreshTreeContent() {
		Widget widget = currentTextWidget;
		if (widget == null) {
			return;
		}
		if (!(widget instanceof StyledText)) {
			return;
		}
		
		String text = getTextUpToCurrentLine(widget);
		
		Resource resource = getResource(text);
		if (resource == null) {
			return;
		}
		
		IPatternMatchContext context = new PatternMatchContext(currentURI);
		MatchService matchService = new MatchService(
				NatSpecPlugin.USE_TREE_BASED_MATCHER,
				NatSpecPlugin.USE_PRIORITIZER);
		matchService.match(resource, context);

		List<ObjectCreationNode> nodes = getNodesForObjectsInContext(context);
		
		TreeNode root = new TreeNode(null);
		for (ObjectCreationNode node : nodes) {
			root.addChild(node);
		}

		contentProvider.setInvisibleRoot(root);
		viewer.refresh();
		viewer.expandAll();
	}

	private List<ObjectCreationNode> getNodesForObjectsInContext(
			IPatternMatchContext context) {
		
		List<ObjectCreationNode> nodes = new ArrayList<ObjectCreationNode>();
		List<Object> objectsInContext = context.getObjectsInContext();
		for (Object objectInContext : objectsInContext) {
			if (objectInContext instanceof ObjectCreation) {
				ObjectCreation objectCreation = (ObjectCreation) objectInContext;
				ObjectCreationNode node = new ObjectCreationNode(objectCreation);
				nodes.add(node);
			}
		}
		
		// We iterate backwards over all created objects to tag that hidden ones
		// as hidden.
		Set<String> hiddenTypes = new LinkedHashSet<String>();
		for (int i = nodes.size() - 1; i >= 0; i--) {
			ObjectCreationNode child = nodes.get(i);
			ObjectCreation objectCreation = child.getObjectCreation();
			String qualifiedName = objectCreation.getObjectType().getQualifiedName();
			if (hiddenTypes.contains(qualifiedName)) {
				child.setHidden(true);
			} else {
				hiddenTypes.add(qualifiedName);
			}
		}

		// Remove hidden nodes if the respective action in the view is checked
		if (hideOverriddenObjectsAction.isChecked()) {
			Iterator<ObjectCreationNode> iterator = nodes.iterator();
			while (iterator.hasNext()) {
				ObjectCreationNode objectCreationNode = iterator.next();
				if (objectCreationNode.isHidden()) {
					iterator.remove();
				}
			}
		}
		return nodes;
	}

	private Resource getResource(String text) {
		ResourceSetImpl resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.createResource(URI.createURI("temp.natspec"));
		String encoding = "UTF-8";
		try {
			InputStream inputStream = new ByteArrayInputStream(text.getBytes(encoding));
			Map<?, ?> options = Collections.singletonMap(INatspecOptions.OPTION_ENCODING, encoding);
			resource.load(inputStream, options);
		} catch (IOException e) {
			Activator.LOGGER.logError("Can't parse NatSpec document.", e);
			return null;
		}
		
		return resource;
	}

	private String getTextUpToCurrentLine(Widget widget) {
		StyledText styledText = (StyledText) widget;
		int caretOffset = styledText.getCaretOffset();

		StyledTextContent content = styledText.getContent();
		int length = content.getCharCount() - 1;
		if (length < 0) {
			// Document is empty
			return "";
		}
		String text = content.getTextRange(0, length);
		int endOfLineAfterCaret = text.indexOf("\n", caretOffset);
		if (endOfLineAfterCaret >= 0) {
			text = text.substring(0, endOfLineAfterCaret);
		}
		return text;
	}

	@Override
	protected void partChanged(IEditorPart activeEditor, boolean force) {
		registerCaretListener(activeEditor);
	}
}
