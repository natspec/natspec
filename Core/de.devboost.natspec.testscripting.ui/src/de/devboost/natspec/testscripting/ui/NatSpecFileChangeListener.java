package de.devboost.natspec.testscripting.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;
import de.devboost.natspec.resource.natspec.ui.ErrorMessageHelper;
import de.devboost.natspec.testscripting.builder.AbstractResourceChangeListener;

/**
 * The {@link NatSpecFileChangeListener} listens to changes applied to '.natspec' files. If a file is modified and no
 * valid license is available a warning dialog is shown.
 */
public class NatSpecFileChangeListener extends AbstractResourceChangeListener {

	private static final String FEATURE_NAME = "'Code generation'";
	private static final String NATSPEC_FILE_EXTENSION = new NatspecMetaInformation().getSyntaxName();

	@Override
	protected void handleResourceChange(IResourceDelta delta, int eventType) {
		int deltaFlags = delta.getFlags();
		if ((deltaFlags & IResourceDelta.CONTENT) == 0) {
			return;
		}
		IResource resource = delta.getResource();
		if (resource == null) {
			return;
		}

		String fileExtension = resource.getFileExtension();
		// We also need to check if it is a file, to avoid double dialogs
		boolean isFile = resource instanceof IFile;
		if (!NATSPEC_FILE_EXTENSION.equals(fileExtension) || !isFile) {
			// Not of interest
			return;
		}

		if (NatSpecPlugin.hasValidLicense()) {
			// Found license.
			return;
		}
		// No license no code generation
		ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();
		errorMessageHelper.showErrorMessage(FEATURE_NAME);
	}

	@Override
	protected void handleMarkerDeltas(IMarkerDelta[] markerDeltas) {
		// Do nothing.
	}
}
