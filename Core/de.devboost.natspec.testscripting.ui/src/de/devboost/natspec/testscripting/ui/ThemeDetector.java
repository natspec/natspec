package de.devboost.natspec.testscripting.ui;

import org.eclipse.e4.ui.css.swt.theme.ITheme;
import org.eclipse.e4.ui.css.swt.theme.IThemeEngine;
import org.eclipse.swt.widgets.Display;

@SuppressWarnings("restriction")
public class ThemeDetector implements Runnable {

	private boolean isDarkTheme;

	@Override
	public void run() {
		IThemeEngine engine = (IThemeEngine) Display.getDefault().getData("org.eclipse.e4.ui.css.swt.theme");
		ITheme activeTheme = engine.getActiveTheme();
		String activeThemeID = activeTheme.getId();
		isDarkTheme = "org.eclipse.e4.ui.css.theme.e4_dark".equals(activeThemeID);
	}

	public boolean isDarkTheme() {
		return isDarkTheme;
	}
}
