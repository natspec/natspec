package de.devboost.natspec.testscripting.ui.views;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.help.IWorkbenchHelpSystem;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.registries.IRegistryListener;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.ui.ErrorMessageHelper;
import de.devboost.natspec.resource.natspec.ui.NatspecEditor;
import de.devboost.natspec.resource.natspec.ui.NatspecImageProvider;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.ui.Activator;

/**
 * The {@link SyntaxPatternView} shows all syntax patterns which are available for the document shown in the currently
 * active editor.
 */
public class SyntaxPatternView extends AbstractView {
	
	public static final List<IPatternClickHandler> CLICK_HANDLERS = new ArrayList<IPatternClickHandler>(); 

	public static final List<IPatternClassClickHandler> CLASS_CLICK_HANDLERS  = new ArrayList<IPatternClassClickHandler>();
	
	public static final String NO_PATTERNS_AVAILABLE_FOR_ACTIVE_EDITOR = "No patterns available for active editor.";
	
	private static final String FEATURE_NAME = "'Syntax Pattern View'";

	private static final String GROUP_BY_TEST_SUPPORT_CLASS_ENABLED_PROPERTY = "groupByTestSupportClassEnabled";
	private static final String LINK_WITH_EDITOR_ENABLED_PROPERTY = "linkWithEditorEnabled";

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "de.devboost.natspec.testscripting.ui.views.SyntaxPatternView";

	private Action linkWithEditorAction;
	private Action groupByTestSupportClassAction;
	private Action collapseAllAction;
	private Action doubleClickAction;

	private SyntaxPatternViewContentProvider contentProvider;

	private IRegistryListener syntaxPatternRegistryListener;

	private class NameSorter extends ViewerSorter {
	}

	private void refreshTreeContent(IEditorPart activeEditor) {
		// This synchronized is required to avoid the multiple refresh requests
		// are executed at the same time. This is needed because syntax pattern
		// providers cannot be assumed to be thread-safe. If one of the syntax
		// pattern providers is not thread-safe, exceptions will be thrown while
		// refreshing the view's content which is neither desired nor
		// acceptable.
		synchronized (SyntaxPatternView.class) {
			refreshTreeContentThreadUnsafe(activeEditor);
		}
	}

	private void refreshTreeContentThreadUnsafe(IEditorPart activeEditor) {
		TreeNode invisibleRoot = new TreeNode(NO_PATTERNS_AVAILABLE_FOR_ACTIVE_EDITOR);
		contentProvider.setInvisibleRoot(invisibleRoot);

		URI uri = null;
		if (activeEditor != null && activeEditor instanceof NatspecEditor) {
			NatspecEditor natspecEditor = (NatspecEditor) activeEditor;
			uri = natspecEditor.getResource().getURI();
		}

		if (!linkWithEditorAction.isChecked()) {
			uri = null;
		}

		if (!linkWithEditorAction.isChecked() || uri != null) {
			addNodesForPatterns(invisibleRoot, uri);
		}

		viewer.refresh();
		viewer.expandAll();
	}

	private void addNodesForPatterns(TreeNode rootNode, URI uri) {
		
		SyntaxPatternRegistry patternRegistry = SyntaxPatternRegistry.REGISTRY;
		List<ISyntaxPatternProvider> providers = patternRegistry.getRegistryEntries();
		for (ISyntaxPatternProvider provider : providers) {
			addNodesForPatterns(rootNode, uri, provider);
		}
	}

	private void addNodesForPatterns(TreeNode rootNode, URI uri,
			ISyntaxPatternProvider provider) {
		
		Collection<ISyntaxPattern<?>> patterns = provider.getPatterns(uri);
		for (ISyntaxPattern<?> pattern : patterns) {
			addNodeForPattern(rootNode, provider, pattern);
		}
	}

	protected void addNodeForPattern(TreeNode rootNode,
			ISyntaxPatternProvider provider,
			ISyntaxPattern<?> pattern) {
		
		StringBuilder label = new StringBuilder();
		List<ISyntaxPatternPart> parts = pattern.getParts();
		for (ISyntaxPatternPart part : parts) {
			label.append(part.toSimpleString());
			label.append(" ");
		}
		
		PatternNode patternNode = new PatternNode(label.toString(), pattern);
		if (groupByTestSupportClassAction.isChecked()) {
			String supportClassName;
			String projectName;
			if (pattern instanceof DynamicSyntaxPattern) {
				DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
				// If the patterns is a dynamic syntax pattern, it
				// contains information about its origin (i.e., the
				// support class containing the pattern).
				supportClassName = dynamicSyntaxPattern.getQualifiedTypeName();
				projectName = dynamicSyntaxPattern.getProjectName();
			} else {
				// If the patterns is not dynamic syntax pattern, it
				// stems from a reflective pattern provider. Thus,
				// we use the name of the provider.
				supportClassName = provider.getClass().getName();
				projectName = null;
			}
			
			SupportClassNode classNode = findClassNode(rootNode, supportClassName);
			// Create node for test support class
			if (classNode == null) {
				classNode = new SupportClassNode(supportClassName, projectName);
				rootNode.addChild(classNode);
			}
			classNode.addChild(patternNode);
		} else {
			rootNode.addChild(patternNode);
		}
	}

	private SupportClassNode findClassNode(TreeNode root,
			String supportClassName) {
		
		for (TreeNode node : root.getChildren()) {
			if (node instanceof SupportClassNode) {
				SupportClassNode classNode = (SupportClassNode) node;
				if (classNode.getName().equals(supportClassName)) {
					return classNode;
				}
			}
		}
		return null;
	}

	/**
	 * This is a callback that will allow us to create the viewer and initialize
	 * it.
	 */
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		contentProvider = new SyntaxPatternViewContentProvider(this);
		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(new SyntaxPatternViewLabelProvider());
		viewer.setSorter(new NameSorter());
		viewer.setInput(getViewSite());

		// Create the help context id for the viewer's control
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchHelpSystem helpSystem = workbench.getHelpSystem();
		Control control = viewer.getControl();
		helpSystem.setHelp(control, "de.devboost.natspec.testscripting.ui.viewer");

		hookDoubleClickAction();
		
		// Register as listener to the SyntaxPatternRegistry to refresh view if
		// new patterns are registered
		this.syntaxPatternRegistryListener = new IRegistryListener() {
			
			@Override
			public void notifyChanged() {
				Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						refreshTreeContent();
					}
				});
			}
		};
		SyntaxPatternRegistry.REGISTRY.addListener(syntaxPatternRegistryListener);
	}
	
	@Override
	public void dispose() {
		SyntaxPatternRegistry.REGISTRY.removeListener(syntaxPatternRegistryListener);
		super.dispose();
	}

	@Override
	protected void createActions() {
		createDoubleClickAction();
		createLinkWithEditorAction();
		createGroupByTestSupportClassAction();
		createCollapseAllAction();
	}

	@SuppressWarnings("unused")
	private void hookContextMenu() {
		MenuManager menuManager = new MenuManager("#PopupMenu");
		menuManager.setRemoveAllWhenShown(true);
		menuManager.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager manager) {
				SyntaxPatternView.this.fillContextMenu(manager);
			}
		});
		
		Menu menu = menuManager.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(menuManager, viewer);
	}

	@Override
	protected void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalToolBar(bars.getToolBarManager());
	}

	@SuppressWarnings("unused")
	private void fillLocalPullDown(IMenuManager manager) {
		manager.add(collapseAllAction);
		manager.add(linkWithEditorAction);
		manager.add(groupByTestSupportClassAction);
	}

	private void fillContextMenu(IMenuManager manager) {
		manager.add(collapseAllAction);
		manager.add(linkWithEditorAction);
		manager.add(groupByTestSupportClassAction);
		// Other plug-ins can contribute there actions here
		manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(collapseAllAction);
		manager.add(linkWithEditorAction);
		manager.add(groupByTestSupportClassAction);
	}

	private void createLinkWithEditorAction() {
		linkWithEditorAction = new Action("Link with editor", IAction.AS_CHECK_BOX) {

			@Override
			public void run() {
				refreshTreeContent();
				storeActionState(this, LINK_WITH_EDITOR_ENABLED_PROPERTY);
			}
		};
		linkWithEditorAction.setToolTipText("Show only patterns that apply to document in editor");
		linkWithEditorAction.setImageDescriptor(NatspecImageProvider.INSTANCE.getImageDescriptor("platform:/plugin/de.devboost.natspec.resource.natspec.ui/icons/link_with_editor_icon.gif"));
		restoreActionState(linkWithEditorAction, LINK_WITH_EDITOR_ENABLED_PROPERTY);
	}

	private void createGroupByTestSupportClassAction() {
		String text = "Group by test support class";
		groupByTestSupportClassAction = new Action(text, IAction.AS_CHECK_BOX) {

			@Override
			public void run() {
				refreshTreeContent();
				storeActionState(this, GROUP_BY_TEST_SUPPORT_CLASS_ENABLED_PROPERTY);
			}
		};
		
		groupByTestSupportClassAction.setToolTipText(text);
		groupByTestSupportClassAction.setImageDescriptor(NatspecImageProvider.INSTANCE.getImageDescriptor("platform:/plugin/" + Activator.PLUGIN_ID + "/icons/group_by_class.png"));
		
		restoreActionState(groupByTestSupportClassAction, GROUP_BY_TEST_SUPPORT_CLASS_ENABLED_PROPERTY);
	}

	private void createCollapseAllAction() {
		String text = "Collapse all";
		collapseAllAction = new Action(text, IAction.AS_PUSH_BUTTON) {

			@Override
			public void run() {
				viewer.collapseAll();
			}
		};
		
		collapseAllAction.setToolTipText(text);
		collapseAllAction.setImageDescriptor(NatspecImageProvider.INSTANCE.getImageDescriptor("platform:/plugin/" + Activator.PLUGIN_ID + "/icons/collapse_all.png"));
	}

	private void createDoubleClickAction() {
		doubleClickAction = new Action() {
			
			public void run() {
				doDoubleClick();
			}
		};
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				if (!NatSpecPlugin.hasValidLicense()) {
					// no license, no double click handling
					ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();
					errorMessageHelper.showErrorMessage(FEATURE_NAME);
					return;
				}
				doubleClickAction.run();
			}
		});
	}

	@Override
	protected void partChanged(IEditorPart activeEditor, boolean force) {
		// We must not refresh the tree contents if the tree is not
		// linked to the editor. The content won't change anyway and
		// refreshing just yields flickering.
		if (!linkWithEditorAction.isChecked() && !force) {
			return;
		}
		
		refreshTreeContent(activeEditor);
	}

	private void refreshTreeContent() {
		IEditorPart activeEditor = null;
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow activeWorkbenchWindow = workbench.getActiveWorkbenchWindow();
		if (activeWorkbenchWindow != null) {
			IWorkbenchPage activePage = activeWorkbenchWindow.getActivePage();
			activeEditor = activePage.getActiveEditor();
		}
		refreshTreeContent(activeEditor);
	}

	private void doDoubleClick() {
		ISelection selection = viewer.getSelection();
		IStructuredSelection structuredSelection = (IStructuredSelection) selection;
		Object obj = structuredSelection.getFirstElement();
		if (obj instanceof PatternNode) {
			PatternNode treeParent = (PatternNode) obj;
			ISyntaxPattern<?> pattern = treeParent.getPattern();
			if (pattern == null) {
				return;
			}

			for (IPatternClickHandler handler : CLICK_HANDLERS) {
				if (handler.handle(pattern)) {
					// Stop when the handler has done something.
					break;
				}
			}
		}
		
		if (obj instanceof SupportClassNode) {
			SupportClassNode supportClassNode = (SupportClassNode) obj;
			String name = supportClassNode.getName();
			String projectName = supportClassNode.getProjectName();
			for (IPatternClassClickHandler handler : CLASS_CLICK_HANDLERS) {
				if (handler.handle(projectName, name)) {
					// Stop when the handler has done something.
					break;
				}
			}
		}
	}
}
