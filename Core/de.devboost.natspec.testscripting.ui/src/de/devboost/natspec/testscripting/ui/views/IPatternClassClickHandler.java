package de.devboost.natspec.testscripting.ui.views;

public interface IPatternClassClickHandler {

	public boolean handle(String projectName, String qualifiedClassName);
}
