package de.devboost.natspec.testscripting.ui.views;


public class SyntaxPatternViewContentProvider extends AbstractTreeContentProvider {
	
	public SyntaxPatternViewContentProvider(SyntaxPatternView syntaxPatternView) {
		super(syntaxPatternView);
	}
	
	@Override
	protected TreeNode createRootNodeForEmptyTree() {
		return new TreeNode(SyntaxPatternView.NO_PATTERNS_AVAILABLE_FOR_ACTIVE_EDITOR);
	}
}
