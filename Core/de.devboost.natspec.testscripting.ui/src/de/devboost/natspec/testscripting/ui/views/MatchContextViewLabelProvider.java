package de.devboost.natspec.testscripting.ui.views;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import de.devboost.natspec.resource.natspec.ui.NatspecImageProvider;
import de.devboost.natspec.testscripting.ui.Activator;

public class MatchContextViewLabelProvider extends LabelProvider {

	private Image objectImage;
	private Image objectOverrideImage;

	public String getText(Object obj) {
		return obj.toString();
	}

	public Image getImage(Object obj) {
		if (obj instanceof ObjectCreationNode) {
			ObjectCreationNode objectCreationNode = (ObjectCreationNode) obj;
			boolean hidden = objectCreationNode.isHidden();
			if (hidden) {
				return getObjectOverrideImage();
			} else {
				return getObjectImage();
			}
		}
		return null;
	}

	private Image getObjectOverrideImage() {
		if (objectOverrideImage == null) {
			objectOverrideImage = getImage("create_override.png");
		}
		return objectOverrideImage;
	}

	private Image getObjectImage() {
		if (objectImage == null) {
			objectImage = getImage("create.png");
		}
		return objectImage;
	}

	private Image getImage(String imageName) {
		return NatspecImageProvider.INSTANCE.getImage("platform:/plugin/" + Activator.PLUGIN_ID + "/icons/" + imageName);
	}
}
