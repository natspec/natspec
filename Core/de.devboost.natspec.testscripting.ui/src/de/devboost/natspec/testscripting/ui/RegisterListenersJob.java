package de.devboost.natspec.testscripting.ui;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

public class RegisterListenersJob extends Job {

	public RegisterListenersJob() {
		super("Registering NatSpec listeners");
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		Activator plugin = Activator.getDefault();
		if (plugin == null) {
			return Status.OK_STATUS;
		}

		plugin.registerListeners();
		return Status.OK_STATUS;
	}
}
