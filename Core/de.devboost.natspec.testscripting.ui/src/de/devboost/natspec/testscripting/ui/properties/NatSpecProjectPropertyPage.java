package de.devboost.natspec.testscripting.ui.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;

import de.devboost.natspec.testscripting.IProperties;

public class NatSpecProjectPropertyPage extends PropertyPage {

	private static final String PATH_TITLE = "Output folder:";

	private static final String DEFAULT_PATH = ".";

	private static final int PATH_FIELD_WIDTH = 20;
	
	private Text pathValueText;

	private void addFirstSection(Composite parent) {
		Composite composite = createDefaultComposite(parent);

		// label for path field
		Label pathLabel = new Label(composite, SWT.NONE);
		pathLabel.setText(PATH_TITLE);

		// path text field
		pathValueText = new Text(composite, SWT.SINGLE | SWT.BORDER);
		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = SWT.FILL;
		gd.widthHint = convertWidthInCharsToPixels(PATH_FIELD_WIDTH);
		pathValueText.setLayoutData(gd);

		// Populate owner text field
		try {
			IProject element = getTarget();
			String owner = element.getPersistentProperty(IProperties.QUALIFIED_PATH_PROPERTY);
			pathValueText.setText((owner != null) ? owner : DEFAULT_PATH);
		} catch (CoreException e) {
			pathValueText.setText(DEFAULT_PATH);
		}
	}

	/**
	 * @see PreferencePage#createContents(Composite)
	 */
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		GridData data = new GridData(GridData.FILL);
		data.grabExcessHorizontalSpace = true;
		composite.setLayoutData(data);

		addFirstSection(composite);
		return composite;
	}

	private Composite createDefaultComposite(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		composite.setLayout(layout);

		GridData data = new GridData();
		data.grabExcessHorizontalSpace = true;
		data.verticalAlignment = GridData.FILL;
		data.horizontalAlignment = GridData.FILL;
		composite.setLayoutData(data);

		return composite;
	}
	
	@Override
	protected void performDefaults() {
		super.performDefaults();
		pathValueText.setText(DEFAULT_PATH);
	}

	public boolean performOk() {
		// store the value from the path field
		try {
			IProject element = getTarget();
			element.setPersistentProperty(
				new QualifiedName("", IProperties.PATH_PROPERTY),
				pathValueText.getText());
		} catch (CoreException e) {
			return false;
		}
		return true;
	}

	private IProject getTarget() {
		IAdaptable element = getElement();
		return (IProject) element.getAdapter(IProject.class);
	}
}