package de.devboost.natspec.testscripting.ui.views;

import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public abstract class AbstractPartListener2 implements IPartListener2 {

	@Override
	public void partClosed(IWorkbenchPartReference partRef) {
		// Empty default implementation
	}

	@Override
	public void partDeactivated(IWorkbenchPartReference partRef) {
		// Empty default implementation
	}

	@Override
	public void partHidden(IWorkbenchPartReference partRef) {
		// Empty default implementation
	}

	@Override
	public void partVisible(IWorkbenchPartReference partRef) {
		// Empty default implementation
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference partRef) {
		// Empty default implementation
	}

	@Override
	public void partOpened(IWorkbenchPartReference partRef) {
		refresh(partRef, true);
	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference partRef) {
		refresh(partRef, false);
	}
	
	@Override
	public void partActivated(IWorkbenchPartReference partRef) {
		refresh(partRef, false);
	}

	/**
	 * A template method this must be implemented by concrete sub classes which
	 * is called whenever a part of the workbench is opened, brought to top or
	 * activated. If the part is opened, <code>force</code> is <code>true</code>
	 * , otherwise it is <code>false</code>.
	 */
	protected abstract void refresh(IWorkbenchPartReference partRef,
			boolean force);

	/**
	 * Returns the editor that is currently active in the workbench.
	 */
	protected IEditorPart getActiveEditor() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		IWorkbenchWindow activeWindow = workbench.getActiveWorkbenchWindow();
		IWorkbenchPage activePage = activeWindow.getActivePage();
		IEditorPart activeEditor = activePage.getActiveEditor();
		return activeEditor;
	}
}
