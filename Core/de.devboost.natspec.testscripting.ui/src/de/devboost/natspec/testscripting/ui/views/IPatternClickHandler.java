package de.devboost.natspec.testscripting.ui.views;

import de.devboost.natspec.patterns.ISyntaxPattern;

public interface IPatternClickHandler {

	public boolean handle(ISyntaxPattern<?> pattern);

}
