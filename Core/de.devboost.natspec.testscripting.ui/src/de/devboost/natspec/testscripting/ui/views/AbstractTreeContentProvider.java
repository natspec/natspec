package de.devboost.natspec.testscripting.ui.views;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.part.ViewPart;

public abstract class AbstractTreeContentProvider implements
		IStructuredContentProvider, ITreeContentProvider {

	/**
	 * The view that is associated with this content provider.
	 */
	private final ViewPart view;

	/**
	 * The invisible root node that is used as the root node for the content
	 * tree.
	 */
	private TreeNode invisibleRoot;

	public AbstractTreeContentProvider(ViewPart view) {
		super();
		this.view = view;
	}

	@Override
	public Object[] getElements(Object parent) {
		IViewSite viewSite = view.getViewSite();
		if (parent.equals(viewSite)) {
			if (invisibleRoot == null) {
				invisibleRoot = createRootNodeForEmptyTree();
			}
			return getChildren(invisibleRoot);
		}
		return getChildren(parent);
	}

	protected abstract TreeNode createRootNodeForEmptyTree();

	@Override
	public Object[] getChildren(Object parent) {
		if (parent instanceof TreeNode) {
			TreeNode treeNode = (TreeNode) parent;
			return treeNode.getChildren();
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object child) {
		if (child instanceof TreeNode) {
			TreeNode treeNode = (TreeNode) child;
			return treeNode.getParent();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object parent) {
		if (parent instanceof TreeNode) {
			TreeNode treeNode = (TreeNode) parent;
			return treeNode.hasChildren();
		}
		return false;
	}

	public void setInvisibleRoot(TreeNode invisibleRoot) {
		this.invisibleRoot = invisibleRoot;
	}

	public boolean isInitialized() {
		return this.invisibleRoot != null;
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// Do nothing
	}

	@Override
	public void dispose() {
		// Do nothing
	}
}
