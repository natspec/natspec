package de.devboost.natspec.testscripting.ui.views;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;

import de.devboost.natspec.resource.natspec.ui.NatspecImageProvider;
import de.devboost.natspec.testscripting.ui.Activator;

public class SyntaxPatternViewLabelProvider extends LabelProvider {

	private Image patternImage;
	private Image classImage;

	public String getText(Object obj) {
		return obj.toString();
	}

	public Image getImage(Object obj) {
		if (obj instanceof PatternNode) {
			if (patternImage == null) {
				Activator plugin = Activator.getDefault();
				Bundle bundle = plugin.getBundle();
				URL installURL = bundle.getEntry("/");
				try {
					URL url = new URL(installURL, "icons/pattern.png");
					ImageDescriptor imageDescriptor = ImageDescriptor.createFromURL(url);
					patternImage = imageDescriptor.createImage();
				} catch (MalformedURLException e) {
					// ignore
				}
			}
			return patternImage;
		} else if (obj instanceof SupportClassNode) {
			if (classImage == null) {
				classImage = NatspecImageProvider.INSTANCE.getImage("platform:/plugin/" + Activator.PLUGIN_ID + "/icons/class.png");
			}
			return classImage;
		}
		return null;
	}
}
