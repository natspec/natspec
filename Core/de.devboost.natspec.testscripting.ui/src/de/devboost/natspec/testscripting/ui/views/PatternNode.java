package de.devboost.natspec.testscripting.ui.views;

import de.devboost.natspec.patterns.ISyntaxPattern;

class PatternNode extends TreeNode {
	
	private ISyntaxPattern<?> pattern;

	public PatternNode(String name, ISyntaxPattern<?> pattern) {
		super(name);
		this.pattern = pattern;
	}

	public ISyntaxPattern<?> getPattern() {
		return pattern;
	}
}