package de.devboost.natspec.testscripting.ui.views;

public class SupportClassNode extends TreeNode {

	private final String projectName;

	public SupportClassNode(String name, String projectName) {
		super(name);
		this.projectName = projectName;
	}

	public String getProjectName() {
		return projectName;
	}
}
