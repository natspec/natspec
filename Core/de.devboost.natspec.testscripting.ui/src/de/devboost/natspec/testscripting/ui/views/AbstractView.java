package de.devboost.natspec.testscripting.ui.views;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.part.ViewPart;

import de.devboost.natspec.testscripting.ui.Activator;

/**
 * The {@link AbstractView} is the common super class for the custom NatSpec
 * views. It provides methods to store and restore the state of view actions.
 */
public abstract class AbstractView extends ViewPart {

	protected TreeViewer viewer;

	private AbstractPartListener2 partListener;
	
	@Override
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);

		partListener = new AbstractPartListener2() {
			
			@Override
			protected void refresh(IWorkbenchPartReference partRef, boolean force) {
				IEditorPart activeEditor = getActiveEditor();
				partChanged(activeEditor, force);
			}
		};
		getSite().getPage().addPartListener(partListener);

		createActions();
		contributeToActionBars();
	}

	protected abstract void partChanged(IEditorPart activeEditor, boolean force);

	protected abstract void createActions();

	protected abstract void contributeToActionBars();

	/**
	 * Returns the qualified name of the preferences key using the qualified
	 * name of the current class as prefix.
	 */
	private String getQualifiedKey(String preferenceKey) {
		return getClass().getName() + "." + preferenceKey;
	}

	/**
	 * Stores the state of the given action to the plug-ins preference store
	 * using the given preference key.
	 */
	protected void storeActionState(IAction action, String preferenceKey) {
		Activator plugin = Activator.getDefault();
		if (plugin == null) {
			return;
		}
		
		IPreferenceStore preferenceStore = plugin.getPreferenceStore();
		if (preferenceStore == null) {
			return;
		}
		
		String qualifiedKey = getQualifiedKey(preferenceKey);
		preferenceStore.setValue(qualifiedKey, action.isChecked());
	}

	/**
	 * Restores the state of the given action from the plug-ins preference store
	 * using the given preference key.
	 */
	protected void restoreActionState(IAction action, String preferenceKey) {
		// read previous state of the action from preference store
		Activator plugin = Activator.getDefault();
		if (plugin == null) {
			return;
		}
		
		IPreferenceStore preferenceStore = plugin.getPreferenceStore();
		if (preferenceStore == null) {
			return;
		}
		
		String qualifiedKey = getQualifiedKey(preferenceKey);
		boolean enabled = preferenceStore.getBoolean(qualifiedKey);
		action.setChecked(enabled);
	}

	@Override
	public void dispose() {
		IWorkbenchPartSite site = getSite();
		IWorkbenchPage page = site.getPage();
		page.removePartListener(partListener);
		super.dispose();
	}

	/**
	 * Passes the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}
