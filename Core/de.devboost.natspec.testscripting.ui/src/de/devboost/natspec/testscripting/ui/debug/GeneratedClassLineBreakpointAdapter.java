package de.devboost.natspec.testscripting.ui.debug;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.texteditor.ITextEditor;

import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;
import de.devboost.natspec.resource.natspec.ui.debug.NatspecLineBreakpointAdapter;
import de.devboost.natspec.testscripting.AbstractGeneratedFileFinder;
import de.devboost.natspec.testscripting.debug.AbstractBreakpointUtils;
import de.devboost.natspec.testscripting.util.Position;

public class GeneratedClassLineBreakpointAdapter extends
		NatspecLineBreakpointAdapter {

	private static final String NATSPEC_FILE_EXTENSION = new NatspecMetaInformation().getSyntaxName();
	
	private AbstractGeneratedFileFinder generatedFileFinder;
	private AbstractBreakpointUtils breakpointUtils;

	public GeneratedClassLineBreakpointAdapter(
			AbstractGeneratedFileFinder generatedFileFinder,
			AbstractBreakpointUtils breakpointUtils) {
		
		super();
		this.generatedFileFinder = generatedFileFinder;
		this.breakpointUtils = breakpointUtils;
	}

	// FIXME 2.3 Check whether using the line number of the selection is correct 
	@Override
	public void toggleLineBreakpoints(IWorkbenchPart part, ISelection selection)
			throws CoreException {
		
		if (!(selection instanceof ITextSelection)) {
			return;
		}
		ITextSelection textSelection = (ITextSelection) selection;
		
		ITextEditor textEditor = getEditor(part);
		if (textEditor == null) {
			return;
		}
		
		IEditorInput editorInput = textEditor.getEditorInput();
		IFile resource = (IFile) editorInput.getAdapter(IFile.class);
		IFile generatedFile = generatedFileFinder.getGeneratedFileForSpecification(resource);

		int lineInSpec = textSelection.getStartLine();
		int offsetInSpec = textSelection.getOffset();
		Position positionInSpec = new Position(lineInSpec, offsetInSpec);
		Position positionInCode = generatedFileFinder.findSentenceInCode(resource,
				generatedFile, positionInSpec);

		int lineInCode = positionInCode.getLine();
		boolean removedBreakpoints = breakpointUtils
				.removeExistingBreakpoints(resource, generatedFile,
						lineInSpec, lineInCode);
		if (removedBreakpoints) {
			// breakpoints were found and removed
			return;
		}

		// no breakpoints were found, thus we must add new ones
		breakpointUtils.createBreakpoints(resource, generatedFile,
				positionInSpec, positionInCode);
	}

	private ITextEditor getEditor(IWorkbenchPart part) {
		if (part instanceof ITextEditor) {
			ITextEditor editorPart = (ITextEditor) part;
			IEditorInput editorInput = editorPart.getEditorInput();
			IResource resource = (IResource) editorInput.getAdapter(IResource.class);
			if (resource == null) {
				return null;
			}
			String extension = resource.getFileExtension();
			if (extension != null
					&& extension.equals(NATSPEC_FILE_EXTENSION)) {
				return editorPart;
			}
		}
		
		return null;
	}
}
