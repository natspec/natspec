package de.devboost.natspec.testscripting.ui.views;

import de.devboost.natspec.testscripting.context.ObjectCreation;

public class ObjectCreationNode extends TreeNode {

	private ObjectCreation objectCreation;
	private boolean hidden;

	public ObjectCreationNode(ObjectCreation objectCreation) {
		super(getName(objectCreation));
		this.objectCreation = objectCreation;
	}

	private static String getName(ObjectCreation objectCreation) {
		String name = objectCreation.getSimpleTypeName() + ": " + objectCreation.getVariableName();
		return name;
	}

	public ObjectCreation getObjectCreation() {
		return objectCreation;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
}
