package de.devboost.natspec.testscripting.ui;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.devboost.shared.eclipse.logging.PluginLogger;

/**
 * The activator class controls the plug-in life cycle.
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "de.devboost.natspec.testscripting.ui"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	public final static PluginLogger LOGGER = new PluginLogger(PLUGIN_ID) {

		@Override
		protected Plugin getPluginInstance() {
			return Activator.getDefault();
		}
	};
	
	/**
	 * The constructor
	 */
	public Activator() {
		super();
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		Job job = new RegisterListenersJob();
		job.schedule();
	}

	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance of this plug-in.
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in
	 * relative path.
	 * 
	 * @param path the path relative to this plug-ins root
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	public void registerListeners() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		workspace.addResourceChangeListener(new NatSpecFileChangeListener(), IResourceChangeEvent.POST_CHANGE);
	}
}
