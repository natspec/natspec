package de.devboost.natspec.development.ui;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.development.PatternExporter;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.registries.SyntaxPatternRegistry;

public class ExportPatternsHandler extends AbstractHandler {

	public Object execute(ExecutionEvent event) throws ExecutionException {

		List<ISyntaxPattern<?>> allPatterns = SyntaxPatternRegistry.REGISTRY
				.getAllSyntaxPatterns(null);
		
		String message;
		try {
			File userHome = new File(System.getProperty("user.home"));
			File file = new File(userHome, "exported_patterns.xml");
			new PatternExporter().writePatternsToFile(allPatterns, file);
			message = "Patterns were exported successfully ("
					+ file.getAbsolutePath() + ").";
		} catch (IOException e) {
			NatSpecPlugin.logError("Pattern export failed.", e);
			message = "Pattern export failed: " + e.getMessage();
		}

		IWorkbenchWindow window = HandlerUtil
				.getActiveWorkbenchWindowChecked(event);
		Shell shell = window.getShell();
		MessageDialog.openInformation(shell, "NatSpec Development Tooling",
				message);
		return null;
	}
}
