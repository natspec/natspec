package de.devboost.natspec.validation.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.test.ITestDataProvider2;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;
import de.devboost.natspec.validation.NatspecValidator;

@RunWith(Parameterized.class)
public class NatspecValidatorTest extends AbstractNatSpecTestCase {
	
	private static final NatspecFactory NATSPEC_FACTORY = NatspecFactory.eINSTANCE;
	
	private final String input;
	private final List<ISyntaxPattern<?>> patterns;

	private final class AccessibleNatspecValidator extends NatspecValidator {
		
		@Override
		protected List<ConstraintStatus> doValidate(Document document, URI uri,
				boolean useOptimizedMatcher, boolean usePrioritizer) {
			
			return super.doValidate(document, uri, useOptimizedMatcher,
					usePrioritizer);
		}
		
		@Override
		protected IPatternMatchContext createContext(URI uri) {
			IPatternMatchContext context = super.createContext(uri);
			if (patterns != null) {
				context.getPatternsInContext().addAll(patterns);
			}
			return context;
		}
	}
	
	public NatspecValidatorTest(String input, boolean useOptimizedMatcher,
			boolean usePrioritizer, List<ISyntaxPattern<?>> patterns) {
		
		super(useOptimizedMatcher, usePrioritizer);
		this.input = input;
		this.patterns = patterns;
	}

	@Parameters(name="{index} - {0}-{1}-{2}")
	public static Collection<Object[]> getTestData() {
		ITestDataProvider2 provider = new ITestDataProvider2() {
			
			@Override
			public Collection<Object[]> getTestData(boolean useOptimizedMatcher, boolean usePrioritizer) {
				return NatspecValidatorTest.getTestData(useOptimizedMatcher, usePrioritizer);
			}
		};
		return new BooleanCombinator().createBooleanCombinations(provider);
	}
	
	public static Collection<Object[]> getTestData(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		SyntaxPatternMock<Object> pattern1 = new SyntaxPatternMock<Object>();
		pattern1.addPart(new StaticWord("Login"));
		pattern1.addPart(new StringParameter());
		
		SyntaxPatternMock<Object> pattern2 = new SyntaxPatternMock<Object>();
		pattern2.addPart(new StaticWord("Login"));
		pattern2.addPart(new StringParameter());
		pattern2.addPart(new StaticWord("at"));

		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		patterns.add(pattern1);
		patterns.add(pattern2);
		
		Collection<Object[]> result = new ArrayList<Object[]>();
		result.add(new Object[] {" ", useOptimizedMatcher, usePrioritizer, null});
		result.add(new Object[] {"\t", useOptimizedMatcher, usePrioritizer, null});
		result.add(new Object[] {"Login joe", useOptimizedMatcher, usePrioritizer, patterns});
		result.add(new Object[] {"Login joe at", useOptimizedMatcher, usePrioritizer, patterns});
		return result;
	}

	@Test
	public void testNoError() {
		assertNoErrors(input, useOptimizedMatcher(), usePrioritizer());
	}

	private void assertNoErrors(String text, boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		System.out.println("NatspecValidatorTest.assertNoErrors('" + text + "', " + useOptimizedMatcher + ")");
		AccessibleNatspecValidator validator = new AccessibleNatspecValidator();
		Document document = NATSPEC_FACTORY.createDocument();
		List<Sentence> sentences = document.getContents();
		Sentence sentence = NATSPEC_FACTORY.createSentence();
		sentence.setText(text);
		sentences.add(sentence);
		List<ConstraintStatus> result = validator.doValidate(document, null, useOptimizedMatcher, usePrioritizer);
		for (ConstraintStatus constraintStatus : result) {
			System.out.println("NatspecValidatorTest.assertNoErrors() " + constraintStatus.getMessage());
		}
		assertEquals(0, result.size());
	}
}
