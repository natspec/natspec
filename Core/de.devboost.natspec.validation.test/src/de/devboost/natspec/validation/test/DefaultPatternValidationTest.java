package de.devboost.natspec.validation.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.DefaultSentenceMatchParameter;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.CommonType;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContext;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;
import de.devboost.natspec.validation.NatspecValidator;

@RunWith(Parameterized.class)
public class DefaultPatternValidationTest {

	private static final NatspecFactory NATSPEC_FACTORY = NatspecFactory.eINSTANCE;
	
	private final class AccessibleNatspecValidator extends NatspecValidator {
		
		@Override
		protected List<ConstraintStatus> doValidate(Document document, URI uri,
				boolean useOptimizedMatcher, boolean usePrioritizer) {
			
			return super.doValidate(document, uri, useOptimizedMatcher,
					usePrioritizer);
		}
		
		@Override
		protected IPatternMatchContext createContext(URI uri) {
			
			IPatternMatchContext context = new PatternMatchContext(uri) {
				
				@Override
				public Collection<ISyntaxPattern<? extends Object>> getPatternsInContext() {
					String type = String.class.getName();
					
					SyntaxPatternMock<Object> pattern1 = new SyntaxPatternMock<Object>();
					pattern1.addPart(new ImplicitParameter(type));
					pattern1.addPart(new DefaultSentenceMatchParameter());
					
					SyntaxPatternMock<Object> pattern2 = new SyntaxPatternMock<Object>();
					pattern2.addPart(new ImplicitParameter(type));
					pattern2.addPart(new StaticWord("Do"));
					
					List<ISyntaxPattern<? extends Object>> patterns = new ArrayList<ISyntaxPattern<? extends Object>>();
					patterns.add(pattern1);
					patterns.add(pattern2);
					return patterns;
				}
			};
			
			context.addObjectToContext(new ObjectCreation(context, new CommonType(String.class.getName()), Arrays.asList(new String[] {"wine"})));
			
			return context;
		}
	}


	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;
	
	public DefaultPatternValidationTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		return new BooleanCombinator().createBooleanCombinations();
	}

	@Test
	public void testDefaultPattern() {
		AccessibleNatspecValidator validator = new AccessibleNatspecValidator();
		Document document = NATSPEC_FACTORY.createDocument();
		List<Sentence> sentences = document.getContents();
		Sentence sentence = NATSPEC_FACTORY.createSentence();
		sentence.setText("Some text");
		sentences.add(sentence);
		List<ConstraintStatus> result = validator.doValidate(document, null, useOptimizedMatcher, usePrioritizer);
		assertEquals(0, result.size());
	}
}
