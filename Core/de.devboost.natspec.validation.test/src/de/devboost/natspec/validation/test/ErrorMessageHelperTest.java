package de.devboost.natspec.validation.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.ExpectationHelper;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DefaultSentenceMatchParameter;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.CommonType;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;
import de.devboost.natspec.validation.ErrorMessageHelper;

@RunWith(Parameterized.class)
public class ErrorMessageHelperTest extends AbstractNatSpecTestCase {

	public ErrorMessageHelperTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testErrorMessageCreation() {
		List<ISyntaxPatternPart> expectedParts = new ArrayList<ISyntaxPatternPart>();
		expectedParts.add(new StaticWord("W1"));
		expectedParts.add(new StaticWord("W2"));
		assertErrorMessage("'W1' or 'W2'.", expectedParts);
	}

	@Test
	public void testErrorMessageSorting() {
		List<ISyntaxPatternPart> expectedParts = new ArrayList<ISyntaxPatternPart>();
		expectedParts.add(new StaticWord("W2"));
		expectedParts.add(new StaticWord("W1"));
		assertErrorMessage("'W1' or 'W2'.", expectedParts);
	}

	@Test
	public void testErrorMessageNoDuplicates() {
		List<ISyntaxPatternPart> expectedParts = new ArrayList<ISyntaxPatternPart>();
		expectedParts.add(new StaticWord("W1"));
		expectedParts.add(new StaticWord("W1"));
		assertErrorMessage("'W1'.", expectedParts);
	}

	@Test
	public void testErrorMessageForImplicitParameters() {
		List<ISyntaxPatternPart> expectedParts = new ArrayList<ISyntaxPatternPart>();
		String qualifiedType = "MyType";
		expectedParts.add(new ImplicitParameter(qualifiedType));
		assertErrorMessage("'Context <MyType>:'.", expectedParts);
	}

	@Test
	public void testDefaultPattern() {
		
		String type = String.class.getName();

		SyntaxPatternMock<Object> pattern1 = new SyntaxPatternMock<Object>();
		pattern1.addPart(new ImplicitParameter(type));
		pattern1.addPart(new DefaultSentenceMatchParameter());
		
		SyntaxPatternMock<Object> pattern2 = new SyntaxPatternMock<Object>();
		pattern2.addPart(new ImplicitParameter(type));
		pattern2.addPart(new StaticWord("Do"));
		
		String text = "Some arbitrary text";
		
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		context.addObjectToContext(new ObjectCreation(context, new CommonType(String.class.getName()), Arrays.asList(new String[] {"wine"})));
		
		List<ISyntaxPattern<? extends Object>> patterns = new ArrayList<ISyntaxPattern<? extends Object>>();
		patterns.add(pattern1);
		patterns.add(pattern2);
		
		List<ISyntaxPatternMatch<? extends Object>> matches = getAllMatches(patterns, text, context);
		
		assertEquals("Two matches expected.", 2, matches.size());
		assertTrue("First match expected to be complete.", matches.get(0).isComplete());
		assertFalse("Second match expected to be incomplete.", matches.get(1).isComplete());
		
		List<ISyntaxPatternPart> expectedParts = new ExpectationHelper().getExpectedParts(matches);
		assertTrue(expectedParts.isEmpty());
		assertErrorMessage("", expectedParts);
	}

	private void assertErrorMessage(String expected, List<ISyntaxPatternPart> expectedParts) {
		String errorMessage = new ErrorMessageHelper().getErrorMessage(expectedParts);
		String expectedMessage = ErrorMessageHelper.ERROR_PREFIX;
		if (!expected.isEmpty()) {
			expectedMessage += ErrorMessageHelper.EXPECTATION_PREFIX;
			expectedMessage += expected;
		}
		assertEquals(expectedMessage, errorMessage);
	}
}
