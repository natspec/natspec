package de.devboost.natspec.validation;

import org.eclipse.emf.common.util.URI;

public interface IValidationPredicate {

	public boolean shouldValidate(URI uri);
}
