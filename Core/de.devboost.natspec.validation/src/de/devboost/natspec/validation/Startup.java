package de.devboost.natspec.validation;

import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.validation.model.Category;
import org.eclipse.emf.validation.model.CategoryManager;
import org.eclipse.emf.validation.service.IConstraintDescriptor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.progress.UIJob;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		UIJob job = new UIJob("Check NatSpec configuration") {
			
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				checkConstraintsAreEnabled();
				return Status.OK_STATUS;
			}
		};
		job.schedule();
	}

	private void checkConstraintsAreEnabled() {
		String categoryPath = NatspecValidator.CATEGORY_PATH;
		CategoryManager instance = CategoryManager.getInstance();
		Category category = instance.findCategory(categoryPath);
		if (category == null) {
			return;
		}
		
		Set<IConstraintDescriptor> constraints = category.getConstraints();
		for (IConstraintDescriptor constraint : constraints) {
			boolean enabled = constraint.isEnabled();
			if (!enabled) {
				Shell shell = Display.getCurrent().getActiveShell();
				MessageDialog.openError(shell, "Warning", "The NatSpec constraints are disabled! Please correct your workspace preferences (Windows -> Preferences -> Model Validation -> Constraints).");
				return;
			}
		}
	}
}
