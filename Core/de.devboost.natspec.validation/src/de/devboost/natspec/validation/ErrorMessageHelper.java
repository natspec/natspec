package de.devboost.natspec.validation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.patterns.ISyntaxPatternPart;

public class ErrorMessageHelper {

	public final static String ERROR_PREFIX = "Unknown sentence.";
	public final static String EXPECTATION_PREFIX = " Expected: ";

	public String getErrorMessage(List<ISyntaxPatternPart> expectedParts) {
		StringBuilder errorMessage = new StringBuilder();
		errorMessage.append(ERROR_PREFIX);
		if (!expectedParts.isEmpty()) {
			errorMessage.append(EXPECTATION_PREFIX);
			errorMessage.append(toString(expectedParts));
			errorMessage.append(".");
		}
		return errorMessage.toString();
	}

	private String toString(List<ISyntaxPatternPart> parts) {
		// We use a set to avoid duplicates.
		Collection<String> partsAsSet = new LinkedHashSet<String>();
		for (ISyntaxPatternPart part : parts) {
			partsAsSet.add("'" + part.toSimpleString() +  "'");
		}
		
		// Sort expected texts alphabetically.
		List<String> partsAsText = new ArrayList<String>(partsAsSet);
		Collections.sort(partsAsText);
		return StringUtils.INSTANCE.explode(partsAsText, " or ");
	}
}
