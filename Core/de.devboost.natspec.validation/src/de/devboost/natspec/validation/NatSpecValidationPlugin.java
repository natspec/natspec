package de.devboost.natspec.validation;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class NatSpecValidationPlugin extends AbstractUIPlugin {

	private static NatSpecValidationPlugin instance;

	private Set<IValidationPredicate> validationPredicates = new LinkedHashSet<IValidationPredicate>();

	public NatSpecValidationPlugin() {
	}

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		instance = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		instance = null;
		super.stop(context);
	}

	public static NatSpecValidationPlugin getInstance() {
		return instance;
	}

	public void addValidationPredicate(IValidationPredicate validationPredicate) {
		validationPredicates.add(validationPredicate);
	}

	public IValidationPredicate getValidationPredicate() {
		return new IValidationPredicate() {

			@Override
			public boolean shouldValidate(URI uri) {
				for (IValidationPredicate validationPredicate : validationPredicates) {
					if (validationPredicate.shouldValidate(uri)) {
						return true;
					}
				}
				return false;
			}

		};
	}
}
