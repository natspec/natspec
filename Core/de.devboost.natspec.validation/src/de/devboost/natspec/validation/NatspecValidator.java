package de.devboost.natspec.validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.emf.validation.model.Category;
import org.eclipse.emf.validation.model.ConstraintSeverity;
import org.eclipse.emf.validation.model.ConstraintStatus;
import org.eclipse.emf.validation.model.EvaluationMode;
import org.eclipse.emf.validation.model.IModelConstraint;
import org.eclipse.emf.validation.service.AbstractConstraintDescriptor;
import org.eclipse.emf.validation.service.IConstraintDescriptor;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;

/**
 * The {@link NatspecValidator} is used to validate NatSpec specifications against the available syntax patterns. If
 * sentences within a specification cannot be matched, errors are reported.
 */
public class NatspecValidator extends AbstractModelConstraint implements IModelConstraint {

	private final static String PACKAGE_NAME = NatspecValidator.class.getPackage().getName();
	
	public final static String CATEGORY_PATH = PACKAGE_NAME + ".category1";

	private static final int MAX_VALIDATION_ERRORS_PER_FILE = 20;

	@Override
	public IStatus validate(IValidationContext context) {
		try {
			boolean useTreeBasedMatcher = NatSpecPlugin.USE_TREE_BASED_MATCHER;
			boolean usePrioritizer = NatSpecPlugin.USE_PRIORITIZER;
			return validateUnsafe(context, useTreeBasedMatcher, usePrioritizer);
		} catch (NoClassDefFoundError e) {
			// TODO: this can be caused when the validation and the reloading of test customization bundles conflict
			// with each other. we need to resolve the cause for this.
		}
		return Status.OK_STATUS;
	}

	private IStatus validateUnsafe(IValidationContext context, boolean useOptimizedMatcher, boolean usePrioritizer) {
		EObject target = context.getTarget();
		return validateUnsafe(target, useOptimizedMatcher, usePrioritizer);
	}

	private IStatus validateUnsafe(EObject target, boolean useOptimizedMatcher, boolean usePrioritizer) {
		NatSpecValidationPlugin instance = NatSpecValidationPlugin.getInstance();
		IValidationPredicate validationPredicate = instance.getValidationPredicate();
		Resource eResource = target.eResource();
		URI uri = eResource.getURI();
		if (!validationPredicate.shouldValidate(uri)) {
			return Status.OK_STATUS;
		}
		
		List<ConstraintStatus> statusList;
		if (target instanceof Document) {
			Document document = (Document) target;
			statusList = validate(document, useOptimizedMatcher, usePrioritizer);
		} else {
			return Status.OK_STATUS;
		}

		MultiStatus multiStatus = new MultiStatus(PACKAGE_NAME, 0, "Unknown sentences.", null);
		int count = 0;
		for (ConstraintStatus status : statusList) {
			multiStatus.add(status);
			count++;
			if (count >= MAX_VALIDATION_ERRORS_PER_FILE) {
				break;
			}
		}

		IStatus[] children = multiStatus.getChildren();
		if (children.length > 0) {
			return multiStatus;
		}

		return Status.OK_STATUS;
	}

	private List<ConstraintStatus> validate(Document document, boolean useOptimizedMatcher, boolean usePrioritizer) {
		Resource eResource = document.eResource();
		if (eResource == null) {
			return Collections.emptyList();
		}

		URI uri = eResource.getURI();
		if (!uri.isPlatformResource()) {
			return Collections.emptyList();
		}

		return doValidate(document, uri, useOptimizedMatcher, usePrioritizer);
	}

	/**
	 * This method is protected to enable unit testing, but it must not be used elsewhere.
	 */
	protected List<ConstraintStatus> doValidate(Document document, URI uri, boolean useOptimizedMatcher,
			boolean usePrioritizer) {

		NatSpecPlugin.logInfo("Validating document at URI " + uri, null);
		MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);
		// ExpectationHelper expectationHelper = new ExpectationHelper();
		ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();

		List<ConstraintStatus> statusList = new ArrayList<ConstraintStatus>();
		IPatternMatchContext matchContext = createContext(uri);
		List<Sentence> sentences = document.getContents();
		for (Sentence sentence : sentences) {
			// ignore comments
			if (SentenceUtil.INSTANCE.isComment(sentence)) {
				continue;
			}

			// ignore empty sentences
			if (SentenceUtil.INSTANCE.isEmpty(sentence)) {
				continue;
			}

			boolean includeIncompleteMatches = false;
			boolean filterObsoleteDefaultMatches = true;
			List<ISyntaxPatternMatch<?>> completeMatches = matchService.match(sentence, matchContext,
					includeIncompleteMatches, filterObsoleteDefaultMatches, usePrioritizer);
			if (!completeMatches.isEmpty()) {
				// Found complete matches
				if (completeMatches.size() > 1) {
					List<ISyntaxPattern<? extends Object>> patterns = new ArrayList<ISyntaxPattern<? extends Object>>();
					for (ISyntaxPatternMatch<? extends Object> match : completeMatches) {
						ISyntaxPattern<? extends Object> pattern = match.getPattern();
						if (pattern.allowsOtherMatches()) {
							// For some languages, multiple matches are allowed.
							// In this case, the patterns must signal this. If
							// multiple matches are allowed, we ignore the match
							// to this pattern.
							continue;
						}
						patterns.add(pattern);
					}

					if (patterns.size() > 1) {
						String errorMessage = "Found multiple matching syntax patterns: " + patterns;
						statusList.add(
								new ConstraintStatus(this, document, errorMessage, Collections.singleton(sentence)));
					}
				}
			} else {
				// Found no complete matches. Must search again for incomplete ones to get expectation messages
				includeIncompleteMatches = filterObsoleteDefaultMatches;
				// List<ISyntaxPatternMatch<?>> incompleteMatches = matchService.match(sentence, matchContext,
				// includeIncompleteMatches, true, NatSpecPlugin.USE_PRIORITIZER);
				// List<ISyntaxPatternPart> expectedParts = expectationHelper.getExpectedParts(incompleteMatches);
				String errorMessage = errorMessageHelper.getErrorMessage(Collections.<ISyntaxPatternPart>emptyList());
				statusList.add(new ConstraintStatus(this, document, errorMessage, Collections.singleton(sentence)));
			}
		}
		return statusList;
	}

	protected IPatternMatchContext createContext(URI uri) {
		return PatternMatchContextFactory.INSTANCE.createPatternMatchContext(uri);
	}

	@Override
	public IConstraintDescriptor getDescriptor() {
		return new AbstractConstraintDescriptor() {

			@Override
			public boolean targetsTypeOf(EObject eObject) {
				return true;
			}

			@Override
			public boolean targetsEvent(Notification notification) {
				return false;
			}

			@Override
			public void removeCategory(Category category) {
			}

			@Override
			public boolean isLive() {
				return false;
			}

			@Override
			public boolean isBatch() {
				return true;
			}

			@Override
			public int getStatusCode() {
				return 0;
			}

			@Override
			public ConstraintSeverity getSeverity() {
				return ConstraintSeverity.ERROR;
			}

			@Override
			public String getPluginId() {
				return PACKAGE_NAME;
			}

			@Override
			public String getName() {
				return "NatSpec Constraints";
			}

			@Override
			public String getMessagePattern() {
				return "message"; // TODO
			}

			@Override
			public String getId() {
				return null;
			}

			@Override
			public EvaluationMode<?> getEvaluationMode() {
				return EvaluationMode.BATCH;
			}

			@Override
			public String getDescription() {
				return "description"; // TODO
			}

			@Override
			public Set<Category> getCategories() {
				return null;
			}

			@Override
			public String getBody() {
				return null;
			}

			@Override
			public void addCategory(Category category) {
			}
		};
	}
}
