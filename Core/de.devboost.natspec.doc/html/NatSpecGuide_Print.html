<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='http://fonts.googleapis.com/css?family=Titillium+Web:200,400' rel='stylesheet' type='text/css'/>
<link rel="stylesheet" href="NatSpec_Print.css" />
</head>
<body>
<h1 class="title">NatSpec User Guide </h1>
<h2>Outline</h2><a class="outline_section_reference" href="#1">1 Summary for Busy Managers</a><br/>
<a class="outline_section_reference" href="#2">2 Guide for Busy Requirements Engineers (and Customers)</a><br/>
<a class="outline_subsection_reference" href="#2.1">2.1 Creating a Natural Scenario Specification</a><br/>
<a class="outline_subsection_reference" href="#2.2">2.2 Initializing some Input Data</a><br/>
<a class="outline_subsection_reference" href="#2.3">2.3 Using an Application Service</a><br/>
<a class="outline_subsection_reference" href="#2.4">2.4 Checking Expectations on Output</a><br/>
<a class="outline_section_reference" href="#3">3 Guide for Busy Developers</a><br/>
<a class="outline_subsection_reference" href="#3.1">3.1 Making Sentences Executable using Test-Support Classes</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.1">3.1.1 Matching Lists of Words</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.2">3.1.2 Matching Boolean Parameters</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.3">3.1.3 Defining Optional Implicit Parameters</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.4">3.1.4 Enforcing Patterns for Text Syntax Patterns</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.5">3.1.5 Supported Primitive Types</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.6">3.1.6 Deriving Syntax from Method Names</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.7">3.1.7 Using NatSpec with JUnit</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.8">3.1.8 Creating a NatSpec Template that uses JUnit</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.9">3.1.9 Using the NatSpec JUnit Template to show sentences in JUnit view</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.10">3.1.10 Validating Expectations using JUnit</a><br/>
<a class="outline_subsubsection_reference" href="#3.1.11">3.1.11 Running NatSpec-based JUnit Tests</a><br/>
<a class="outline_subsection_reference" href="#3.2">3.2 Making Sentences Executable using Pattern Providers</a><br/>
<a class="outline_subsection_reference" href="#3.3">3.3 Dealing with Synonyms</a><br/>
<a class="outline_subsection_reference" href="#3.4">3.4 Documenting NatSpec Scenarios</a><br/>
<a class="outline_subsubsection_reference" href="#3.4.1">3.4.1 Using Comments to Document Sentences</a><br/>
<a class="outline_subsubsection_reference" href="#3.4.2">3.4.2 Using Comments to Define Markers in Scenarios</a><br/>
<a class="outline_subsubsection_reference" href="#3.4.3">3.4.3 Using Comments With Task Tags to Denote TODOs and FIXMEs</a><br/>
<a class="outline_subsection_reference" href="#3.5">3.5 Preserving the Implicit Contexts within a Specification</a><br/>
<a class="outline_subsection_reference" href="#3.6">3.6 Generating and Running JUnit Test Cases</a><br/>
<a class="outline_subsection_reference" href="#3.7">3.7 Setting the Target Folder for Generated Classes</a><br/>
<a class="outline_subsection_reference" href="#3.8">3.8 Initializing Infrastructure for JUnit Test Cases</a><br/>
<a class="outline_subsubsection_reference" href="#3.8.1">3.8.1 @MethodBody (required)</a><br/>
<a class="outline_subsubsection_reference" href="#3.8.2">3.8.2 @Imports (optional)</a><br/>
<a class="outline_section_reference" href="#4">4 NatSpec API Guide</a><br/>
<a class="outline_subsection_reference" href="#4.1">4.1 Pattern Provider API</a><br/>
<a class="outline_subsection_reference" href="#4.2">4.2 Synonym Provider API</a><br/>
<div style="page-break-after:always"></div><div style="page-break-after:always"></div><div style="page-break-after:always"></div><h2 id="1" class="section">1 Summary for Busy Managers</h2>
<p>
The goal of any software development process is to deliver software to customers that satisfies their needs. Transforming requirements into a shipped product is essentially the job that needs to be done. Requirements are often described in natural language. In modern software development, checking whether those requirements are met and implemented correctly is typically done by using unit tests. Such unit tests are usually described in a programming language like Java. The language gap between describing the requirements in natural language and implementing them as unit tests makes it difficult for customers and developers to communicate effectively. </p>
<p>
NatSpec bridges this gap. It provides means for the specification of requirements with your customers in natural language while enabling their execution as a unit test at the same time. Thus, NatSpec allows for writing readable, maintainable, and executable requirements and test specifications that are understood by customers and developers. An exemplary NatSpec specification is shown below: </p>
<div class="code">Create airplane Boeing-737-600<br/>Set totalSeats to 2<br/>Create flight LH-1234<br/>Set plane to Boeing-737-600<br/>Create passenger John Doe<br/>Book seat for John Doe at LH-1234<br/>Assume success<br/></div><p>
A specification like this is fully sufficient to generate a test case which ensures that the described scenario is implemented correctly by your software. Have you ever dreamed of a solution being that simple? </p>
<div style="page-break-after:always"></div><h2 id="2" class="section">2 Guide for Busy Requirements Engineers (and Customers)</h2>
It is your job to describe the functions a software is meant to deliver. Only if this is done in a very concrete way, developers will be able to realize them as expected. However, it is often hard to find a precise and yet readable way to give a requirements specification. NatSpec helps you to build precise scenario specifications that use natural language. There are no limitations on the structure or terminology. Furthermore, NatSpec specifications can be used by developers to validate the functions they implement against your expectations. <h3 id="2.1" class="subsection">2.1 Creating a Natural Scenario Specification</h3>
In NatSpec one can write tests in natural language in form of stories or scenarios. Each scenario is meant to describe a function of the application under development in terms of actions and expected behavior. Typically, a scenario involves creating some input data, invoking a few application services, and checking the actual results against expected results. To use NatSpec you should first create a new NatSpec project: <tt>File &gt; New &gt; Other... &gt; NatSpec Project</tt>&nbsp;. Then add a new test specification to the folder for end-user tests ( <tt>src-endusertests</tt>&nbsp;): <tt>File &gt; New &gt; Other... &gt; NatSpec Scenario File (.natspec)</tt>&nbsp;. <h3 id="2.2" class="subsection">2.2 Initializing some Input Data</h3>
As NatSpec uses natural language, initializing the input data in a scenario is as easy as writing a plain English sentence. For an exemplary application that deals with airplanes, flights and passengers to book air travels you could write something like: <div class="code">Create airplane Boeing-737-600<br/>Set totalSeats to 2<br/>Create flight LH-1234<br/>Set Airplane to Boeing-737-600<br/>Create passenger John Doe<br/></div><p>
There are no syntactic limitations on the sentence structures. There are also no specific keywords required. You can use plain English or any other language of your choice! </p>
<h3 id="2.3" class="subsection">2.3 Using an Application Service</h3>
Invoking an application service is equally easy: <div class="code">Book seat for John Doe at LH-1234<br/></div><h3 id="2.4" class="subsection">2.4 Checking Expectations on Output</h3>
To check the service results against your expectations you can write something like: <div class="code">Assume LH-1234 has passenger John Doe<br/></div><p>
That's it. Just write down scenarios capturing expected functionality. The test developers will map your text to actual test code later on and make sure that the respective functionality is automatically verified. </p>
<div style="page-break-after:always"></div><h2 id="3" class="section">3 Guide for Busy Developers</h2>
It is your job to implement the functionality that your customers require. However, it is hard to implement your customers' expectations if these are not precisely described or simply incomplete. NatSpec helps you to communicate with your customers in terms of exemplary scenarios. As it uses natural language, those scenarios can be easily understood both by yourself and by your clients. It will even help you to ask your customers for more detailed specifications and to resolve misunderstandings. NatSpec provides means to describe scenario specifications testing the functions you implement. Therefore, scenario specifications are mapped to JUnit tests in an easy and comprehensible way. NatSpec makes requirements engineering more precise. It reduces the effort you invest for building sensible test cases and gives test-driven development a kick start. <h3 id="3.1" class="subsection">3.1 Making Sentences Executable using Test-Support Classes</h3>
To make a sentence of a scenario specification executable, it is simply mapped to a Java method. This is done in so-called test-support classes. A test-support class is a simple Java class that contains test-support methods. Test-support methods describe how the syntax of a sentence in the natural specification is mapped to Java. These methods are identified by the annotation <tt>@TextSyntax</tt>&nbsp;. An example is shown in the following: <div class="code">public class TestSupport {<br/>&nbsp;&nbsp;&nbsp;&nbsp;private AirlineServices services;<br/>&nbsp;&nbsp;&nbsp;&nbsp;public TestSupport(AirlineServices services) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this.services = services;<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>&nbsp;&nbsp;&nbsp;&nbsp;@TextSyntax(&quot;Book seat for #1 at #2&quot;)<br/>&nbsp;&nbsp;&nbsp;&nbsp;public boolean bookSeat(Passenger passenger, Flight flight) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;boolean success = services.bookSeat(passenger, flight);<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;return success;<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>}<br/></div><p>
The method <tt>bookSeat</tt>&nbsp;makes the sentence </p>
<div class="code">Book seat for John Doe at LH-1234<br/></div><p>
executable. Typically, the application service (e.g., <tt>services.bookSeat(passenger, flight)</tt>&nbsp;) that is tested with a specific scenario is to be parameterized with some input data. Thus, it is necessary to clarify where and how such input data is represented in the natural specification. This is done in the syntax mapping: </p>
<div class="code">@TextSyntax(&quot;Book seat for #1 at #2&quot;)<br/></div><p>
It consists of static structures (words) and variable placeholders. The placeholders are indicated by a hashtag (#) followed by a number. This number refers to the index position of the method parameters for the according test support method. In our example #1 refers to parameter <tt>passenger</tt>&nbsp;and #2 refers to parameter <tt>flight</tt>&nbsp;. Within the test support method the meaning of the sentence is implemented using plain Java. To assign the same test support method to multiple sentences, the <tt>@TextSyntaxes</tt>&nbsp;annotation can be used, which can consist of multiple <tt>@TextSyntax</tt>&nbsp;annotations. For example, the following annotations map two sentences to one test support method. </p>
<div class="code">@TextSyntaxes({<br/>&nbsp;&nbsp;&nbsp;&nbsp;@TextSyntax(&quot;Book seat for #1 at #2&quot;),<br/>&nbsp;&nbsp;&nbsp;&nbsp;@TextSyntax(&quot;Book flight for #1 at #2&quot;),<br/>&nbsp;&nbsp;&nbsp;&nbsp;})<br/></div><p>
NatSpec accepts arbitrary many syntax patterns, but one must take care to make sure that these patterns do not match the same sentences. </p>
<h4 id="3.1.1" class="subsubsection">3.1.1 Matching Lists of Words</h4>
NatSpec supports to assign multiple words to a single parameter. This is automatically done, if the type of the respective parameter is 'java.util.List'. For example: <div class="code">@TextSyntax(&quot;Print #1&quot;)<br/>public void printNames(List&lt;String&gt; names) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;for (String name : names) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(name);<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>}<br/></div><p>
matches the sentence: </p>
<div class="code">Print John Jane Peter<br/></div><p>
To match a list of words, but use them as a single String argument, the annotation <tt>@Many</tt>&nbsp;can be used: </p>
<div class="code">@TextSyntax(&quot;Print #1&quot;)<br/>public void print(@Many String names) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(names);<br/>}<br/></div><p>
This does also match the sentence: </p>
<div class="code">Print John Jane Peter<br/></div><p>
but passes &quot;John Jane Peter&quot; as argument to <tt>print()</tt>&nbsp;. </p>
<h4 id="3.1.2" class="subsubsection">3.1.2 Matching Boolean Parameters</h4>
NatSpec supports boolean parameters, but these require special handling as it is not clear which text in a sentence represents the value 'true' and which represents 'false'. <p>
If one wants to represent 'true' by the presence of one word, the parameter name can be used to specify this word. For example, the following listing: </p>
<div class="code">@TextSyntax(&quot;Assume #1 failure&quot;)<br/>public void assume(boolean no) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(no ? &quot;Failure not expected.&quot; : &quot;Failure expected.&quot;);<br/>}<br/></div><p>
matches the following two sentences: </p>
<div class="code">Assume no failure<br/>Assume failure<br/></div><p>
For the first sentence the value 'true' is assigned to the parameter 'no'. For the second sentence that parameter is set to 'false'. </p>
<p>
Alternatively, one can explicitly specify the words that are used to represent 'true' and 'false' by using the @BooleanSyntax annotation: </p>
<div class="code">@TextSyntax(&quot;#1 authentication&quot;)<br/>public void selectAuthentication(@BooleanSyntax({&quot;Enable&quot;, &quot;Disable&quot;}) boolean enable) {<br/>System.out.println(&quot;Authentication is &quot; + (enable ? &quot;on&quot; : &quot;off&quot;));<br/>}<br/></div><p>
matches the following two sentences: </p>
<div class="code">Enable authentication<br/>Disable authentication<br/></div><h4 id="3.1.3" class="subsubsection">3.1.3 Defining Optional Implicit Parameters</h4>
NatSpec supports optional implicit parameters. Such parameters are optional in the sense that if no matching object in the context of the NatSpec script was found, null is used as argument for the parameter. <div class="code">@TextSyntax(&quot;Simulate flight #1&quot;)<br/>public void assume(Flight flight, @Optional FlightCrew crew) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;if (crew != null) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;flight.setCrew(crew);<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>&nbsp;&nbsp;&nbsp;&nbsp;flight.simulate();<br/>}<br/></div><h4 id="3.1.4" class="subsubsection">3.1.4 Enforcing Patterns for Text Syntax Patterns</h4>
NatSpec supports the enforcing of patterns that have to apply for all syntax patterns in the same test support class. This can be useful to ensure that all syntax patterns from one test support class follow a basic construction rule. Thus, they can easily be identified when used in a NatSpec specification. <p>
To define such construction rules you can use the @AllowedSyntaxPattern annotation. The value of the annotation is a regular expression which all syntax patterns must match. For example, the following annotation defines that only patterns which start with &quot;Assume&quot; can be defined in the respective test support class. </p>
<div class="code">@AllowedSyntaxPattern(&quot;Assume.*&quot;)<br/>public class MySupportClassWithAssumptions {<br/>&nbsp;&nbsp;&nbsp;&nbsp;@TextSyntax(&quot;Assume ticket is valid&quot;)<br/>&nbsp;&nbsp;&nbsp;&nbsp;public void assume(Ticket ticket) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;assertTrue(ticket.isValid());<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;assertEquals(0, ticket.getErrors());<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>}<br/></div><h4 id="3.1.5" class="subsubsection">3.1.5 Supported Primitive Types</h4>
NatSpec supports the following primitive types: <ul>
<li>String</li>
<li>int/Integer</li>
<li>long/Long</li>
<li>float/Float</li>
<li>double/Double</li>
<li>java.util.Date</li>
<li>java.time.LocalDate</li>
<li>java.time.LocalTime</li>
<li>java.time.LocalDateTime</li>
<li>java.math.BigDecimal</li>
</ul>
<h4 id="3.1.6" class="subsubsection">3.1.6 Deriving Syntax from Method Names</h4>
Alternatively to the annotation <tt>@TextSyntax</tt>&nbsp;, the annotation <tt>@NameBasedSyntax</tt>&nbsp;can be used. The syntax pattern for methods which are annotated with <tt>@NameBasedSyntax</tt>&nbsp;is derived from the method name. For example: <div class="code">@NameBasedSyntax<br/>public void print_and_toStdout(String text1, String text2) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(text1);<br/>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(text2);<br/>}<br/></div><p>
is equivalent to the following method: </p>
<div class="code">@TextSyntax(&quot;Print #1 and #2 to Stdout&quot;)<br/>public void print(String text1, String text2) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(text1);<br/>&nbsp;&nbsp;&nbsp;&nbsp;System.out.println(text2);<br/>}<br/></div><p>
The underscore indicate the places where parameters can occur in the sentence and the words are split based on the camel-case method name. </p>
<h4 id="3.1.7" class="subsubsection">3.1.7 Using NatSpec with JUnit</h4>
<h4 id="3.1.8" class="subsubsection">3.1.8 Creating a NatSpec Template that uses JUnit</h4>
To use NatSpec for the specification of JUnit tests, one must supply a NatSpec template (_NatSpecTemplate.java) that includes a method with an @Test annotation. This method must contain the @MethodBody placeholder to tell NatSpec to put the generated code into the method. For example, your template could look like this: <div class="code">import org.junit.Test;<br/>public class _NatSpecTemplate {<br/>&nbsp;&nbsp;&nbsp;&nbsp;protected MyTestSupport myTestSupport = new MyTestSupport();<br/>&nbsp;&nbsp;&nbsp;&nbsp;@Test<br/>&nbsp;&nbsp;&nbsp;&nbsp;public void runTest() throws Exception {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/* @MethodBody */<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>}<br/></div><h4 id="3.1.9" class="subsubsection">3.1.9 Using the NatSpec JUnit Template to show sentences in JUnit view</h4>
NatSpec provides a custom template that enables to show the individual sentences in the Eclipse JUnit view when running test cases. To use this template, the plug-in 'de.devboost.natspec.junit4.runner' must be added to the project dependencies. Once the dependency has been added, a template can be defined as follows: <div class="code">import org.junit.Test;<br/>import org.junit.runner.RunWith;<br/>import de.devboost.natspec.junit4.runner.NatSpecJUnit4Runner;<br/>import de.devboost.natspec.junit4.runner.NatSpecJUnit4Template;<br/>@RunWith(NatSpecJUnit4Runner.class)<br/>public class _NatSpecTemplate extends NatSpecJUnit4Template {<br/>&nbsp;&nbsp;&nbsp;&nbsp;protected MyTestSupport myTestSupport = new MyTestSupport();<br/>&nbsp;&nbsp;&nbsp;&nbsp;@Test<br/>&nbsp;&nbsp;&nbsp;&nbsp;public void runTest() throws Exception {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/* @MethodBody */<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>}<br/></div><p>
Note that the template must extend the NatSpecJUnit4Template class and run with the NatSpecJUnit4Runner. Only if both is declared, the Eclipse JUnit view will show the individual sentences as test steps. </p>
<h4 id="3.1.10" class="subsubsection">3.1.10 Validating Expectations using JUnit</h4>
To validate expectations expressed in natural-language specifications you can simply use JUnit methods in test-support methods. To check the expectation: <div class="code">Assume LH-1234 has passenger John Doe<br/></div><p>
you can write the following test support method: </p>
<div class="code">@TextSyntax(&quot;Assume #1 has passenger #2&quot;)<br/>public void hasPassenger(Flight flight, Passenger passenger) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;Assert.assertTrue(flight.hasPassenger(passenger));<br/>}<br/></div><h4 id="3.1.11" class="subsubsection">3.1.11 Running NatSpec-based JUnit Tests</h4>
Every NatSpec file that uses a template containing an @Test method can be launched as JUnit test. Just perform a right-click on the NatSpec file and select 'Run As &gt; JUnit Test'. <h3 id="3.2" class="subsection">3.2 Making Sentences Executable using Pattern Providers</h3>
For making natural specifications executable, test-support methods define patterns of sentences and their mapping to the Java code given in the method body. <p>
When dealing with large APIs that follow consistent conventions, it is often sensible to create such patterns programmatically. A common example are entity classes as used in persistence layers or for data transfer objects. These often follow the JavaBeans convention for accessing fields and common construction patterns like factories. It would cause lots of effort to manually implement test-support methods for all such entities. </p>
<p>
It is much more effective and consistent to define the mapping patterns for their getters, setters and constructors in a standardized way. To do so, you can implement a custom <tt>ISyntaxPatternProvider.</tt>&nbsp;For an example see the <tt>JavaCustomizationProvider</tt>&nbsp;in the project <tt>de.devboost.natspec.java.customization.</tt>&nbsp;It programmatically registers patterns for POJO entities following the JavaBeans conventions. </p>
<p>
Important note: Make sure you're implementing the pattern provider in an Eclipse plug-in project (i.e., an OSGi bundle). This is required to allow NatSpec to reload your pattern provider class dynamically without restarting your Eclipse instance. Thus you can make changes to the pattern provider code and instantly see the resulting changes. To get a more thorough understanding of how to implement pattern providers see </p>
<h3 id="3.3" class="subsection">3.3 Dealing with Synonyms</h3>
Natural languages are full of synonyms. Instead of writing <div class="code">Create airplane Boeing-737-600<br/></div><p>
you may also want to write </p>
<div class="code">Create plane Boeing-737-600<br/></div><p>
To support synonyms you just need to create a file called <tt>synonyms.txt</tt>&nbsp;in your test project and add lines of comma-separated synonyms. The following listing gives synonyms for <tt>airplane</tt>&nbsp;, <tt>passenger</tt>&nbsp;, and <tt>flight</tt>&nbsp;. </p>
<div class="code">airplanetype,airplane,plane,aircraft<br/>passenger,traveler<br/>flight,travel<br/></div><h3 id="3.4" class="subsection">3.4 Documenting NatSpec Scenarios</h3>
Even so we're using natural language, some scenarios or sentences need additional documentation. NatSpec provides means for documenting scenarios on different levels of granularity, ranging from documenting a single sentence to top-level scenario documentation. <h4 id="3.4.1" class="subsubsection">3.4.1 Using Comments to Document Sentences</h4>
Comments can be used to document sentences in NatSpec scenarios. The character sequence to denote standard comments is two forward slashes followed by the comment text. For an example, see the following specification: <div class="code">Create airplane Boeing-737-600<br/>// We need to set the total seats so that tickets can be issued<br/>Set totalSeats to 2<br/></div><h4 id="3.4.2" class="subsubsection">3.4.2 Using Comments to Define Markers in Scenarios</h4>
Markers in scenarios are a feature to define sections in scenarios. The character sequence to denote markers is three forward slashes followed by the marker text. The outline view can then be configured to only show those markers instead of entries for all sentences in the scenario. <div class="code">/// Initialization<br/>Create airplane Boeing-737-600<br/>// We need to set the total seats so that tickets can be issued<br/>Set totalSeats to 2<br/></div><h4 id="3.4.3" class="subsubsection">3.4.3 Using Comments With Task Tags to Denote TODOs and FIXMEs</h4>
Task tags, i.e., TODO and FIXME can also be used in standard comments to denote tasks that need to be listed in the Tasks view. <div class="code">Create airplane Boeing-737-600<br/>// TODO We need to initialize the airplane seats<br/></div><h3 id="3.5" class="subsection">3.5 Preserving the Implicit Contexts within a Specification</h3>
In natural language we typically use implicit contexts in a sequence of several sentences. For an example, see the following specification: <div class="code">Create airplane Boeing-737-600<br/>Set totalSeats to 2<br/></div><p>
The second sentence implicitly refers to the airplane created in the previous sentence. NatSpec does also support this kind of context preservation. For the second template you would define a test-support method like: </p>
<div class="code">@TextSyntax(&quot;Set totalSeats to #2&quot;)<br/>public void setTotalSeats(AirplaneType airplane, int seats) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;airplane.setTotalSeats(seats);<br/>}<br/></div><p>
As you can see, there is no placeholder bound to the first method parameter <tt>airplane</tt>&nbsp;. To determine the value for unbound parameters, NatSpec investigates the sentence context for matching objects. Therefore, it inspects all objects that were a result of the previous sentences in the specification and uses the latest result object with a matching type. In the following example NatSpec would use the airplane Boeing-737-700 as parameter for calling <tt>setTotalSeats()</tt>&nbsp;. </p>
<div class="code">Create airplane Boeing-737-600<br/>Create airplane Boeing-737-700<br/>Set totalSeats to 2<br/></div><p>
The result object of a sentence is determined by the value that is returned by the corresponding test-support method. The support method for the first two sentences looks as follows: </p>
<div class="code">@TextSyntax(&quot;Create airplane #1&quot;)<br/>public AirplaneType createAirplaneType(String typeName) {<br/>&nbsp;&nbsp;&nbsp;&nbsp;return new AirplaneType(typeName);<br/>}<br/></div><h3 id="3.6" class="subsection">3.6 Generating and Running JUnit Test Cases</h3>
NatSpec automatically generates JUnit test cases whenever you save a NatSpec specification file. Generation is based on the mappings you define using test-support classes and pattern providers. For every NatSpec file a corresponding Java class having the same name and being located in the same package will be generated. You can execute the test case like any other JUnit test: <tt>Run &gt; Run as &gt; JUnit Test</tt>&nbsp;<h3 id="3.7" class="subsection">3.7 Setting the Target Folder for Generated Classes</h3>
By default, NatSpec writes the generated classes to the same folder the NatSpec specification is contained in. This behavior can be adjusted by setting the NatSpec project property 'Output folder'. This property can be set by invoking the context menu for a project and selecting the 'Properties' menu item. The NatSpec property page can then be used to set either a relative or an absolute path (w.r.t. the project root) where to store the generated classes. <h3 id="3.8" class="subsection">3.8 Initializing Infrastructure for JUnit Test Cases</h3>
As test cases often require some infrastructure, e.g., to access the database layer or the business services to test, you can influence how test cases are generated using test-case templates. A test-case template is a Java class named <tt>_NatSpecTemplate.java</tt>&nbsp;(Note the underscore!) that you place next to your NatSpec specifications. Test-case templates can be placed in the same folder as the NatSpec file or any parent folder. The first template that is found when ascending the folder hierarchy is used for each NatSpec test. <p>
An exemplary test-case template is shown below: </p>
<div class="code">package com.yourdomain;<br/>import static org.junit.Assert.*;<br/>import org.junit.Before;<br/>import org.junit.Test;<br/>import de.devboost.natspec.examples.airline.InMemoryPersistenceContext;<br/>import de.devboost.natspec.examples.airline.services.AirlineServices;<br/>/* @Imports */<br/>public class _NatSpecTemplate {<br/>&nbsp;&nbsp;&nbsp;&nbsp;private AirlineServices services;<br/>&nbsp;&nbsp;&nbsp;&nbsp;private TestSupport testSupport;<br/>&nbsp;&nbsp;&nbsp;&nbsp;private InMemoryPersistenceContext persistenceContext;<br/>&nbsp;&nbsp;&nbsp;&nbsp;@Test<br/>&nbsp;&nbsp;&nbsp;&nbsp;public void executeScript() throws Exception {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/* @MethodBody */<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>&nbsp;&nbsp;&nbsp;&nbsp;@Before<br/>&nbsp;&nbsp;&nbsp;&nbsp;public void setUp() {<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;services = new AirlineServices();<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;testSupport = new TestSupport(services);<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;persistenceContext =<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;InMemoryPersistenceContext.getPersistenceContext();<br/>&nbsp;&nbsp;&nbsp;&nbsp;}<br/>}<br/></div><p>
As the test-case template is a Java class you can use plain Java code to initialize any infrastructure required for executing the test. You can also simply use JUnit annotations like <tt>@Before</tt>&nbsp;or <tt>@After</tt>&nbsp;for set up and tear down activities. </p>
<p>
The template also uses two placeholders to indicate a place in the template that will be replaced with generated code: </p>
<h4 id="3.8.1" class="subsubsection">3.8.1 @MethodBody (required)</h4>
The <tt>/* @MethodBody */</tt>&nbsp;placeholder will be replaced with the code generated from a specific NatSpec specification. <h4 id="3.8.2" class="subsubsection">3.8.2 @Imports (optional)</h4>
The <tt>/* @Imports */</tt>&nbsp;placeholder will be replaced with imports, which are generated from a specific NatSpec specification. If not provided, imports will be generated before the imports which are already present. <div style="page-break-after:always"></div><h2 id="4" class="section">4 NatSpec API Guide</h2>
NatSpec provides an API to allow expert users to programmatically create syntax patterns without the need for <tt>@TextSyntax</tt>&nbsp;annotations. Also, providers for synonyms can be registered with this API. The following sections cover details on the most important parts of this API. <h3 id="4.1" class="subsection">4.1 Pattern Provider API</h3>
To extend NatSpec dynamically with syntax patterns one can create a class that implements the interface <tt>ISyntaxPatternProvider</tt>&nbsp;. This class must reside in an OSGi bundle located in the current workspace. Whenever a change is applied to the class, NatSpec will automatically reload the OSGi bundle and instantiate a new instance of the class which will be asked to provide syntax pattern when necessary. <p>
To ask for syntax patterns, NatSpec calls the method <tt>getPatterns()</tt>&nbsp;on the provider class which returns a collections of patterns (i.e., instances of <tt>ISyntaxPattern</tt>&nbsp;). NatSpec passes the URI of the current specification file to <tt>getPatterns()</tt>&nbsp;in order to allow pattern providers to return different collection of patterns for different specifications. This can, for example, be used to consider the classpath of the project that contains the specification when computing syntax patterns. </p>
<p>
Syntax patterns can be realized by creating a class that implements <tt>ISyntaxPattern</tt>&nbsp;. We strongly encourage clients of the API to extend <tt>AbstractSyntaxPattern</tt>&nbsp;instead of implementing the interface only. </p>
<p>
A syntax pattern can match a sentence in a NatSpec specification and consists of syntax pattern parts which can match words in a sentence. The parts of a syntax pattern must be returned by its method <tt>getParts()</tt>&nbsp;. Syntax pattern parts can be simple static words (see class <tt>StaticWord</tt>&nbsp;) or match certain types of parameters (e.g., integers or dates). One can either implement custom syntax pattern parts or use existing ones provided by the NatSpec API. Most common syntax pattern parts are: </p>
<ul>
<li>StaticWord - match a single word case-insensitive or one of its synonyms</li>
<li>IntegerArgument - match a single integer number</li>
<li>DoubleArgument - match a single floating-point number</li>
<li>DateArgument - match a single date</li>
<li>ListParameter - match a list of arguments</li>
<li>ComplexParameter - match an instance of a specific Java class</li>
</ul>
<p>
Custom syntax pattern parts must implement <tt>ISyntaxPatternPart</tt>&nbsp;and respectively the method <tt>match(List&lt;Word&gt; words, IPatternMatchContext context)</tt>&nbsp;. By calling the <tt>match()</tt>&nbsp;method, NatSpec can decide whether a syntax pattern part matches a given list of words. If all parts of a syntax pattern match all words in a sentence, the sentence is considered as matched completely. </p>
<p>
When asked for a match, each syntax pattern part can return a custom match object (see <tt>ISyntaxPatternPartMatch</tt>&nbsp;) which stores the data that is relevant for the code generation. For example, class <tt>DateArgument</tt>&nbsp;returns a <tt>DateMatch</tt>&nbsp;object containing the actual date found in the sentence. Custom syntax pattern parts can either implement interface <tt>ISyntaxPatternPartMatch</tt>&nbsp;or extend class <tt>AbstractSyntaxPatternPartMatch</tt>&nbsp;. The latter method is recommended. </p>
<p>
To generate code for matched sentences NatSpec calls <tt>createUserData()</tt>&nbsp;on the syntax pattern object. If this method returns an object implementing <tt>ICodeFragment</tt>&nbsp;the code is obtained by calling <tt>generateSourceCode()</tt>&nbsp;. To compose the code, the syntax pattern can access the matched parts using the parameter <tt>match</tt>&nbsp;which implements <tt>ISyntaxPatternMatch</tt>&nbsp;and therefore gives access to all matched pattern parts. To easily get started with the pattern provider API one may also consider the example provider class <tt>JavaCustomizationProvider</tt>&nbsp;. This class is part of the NatSpec example workspace and contained in the project <tt>de.devboost.natspec.java.customization</tt>&nbsp;. </p>
<h3 id="4.2" class="subsection">4.2 Synonym Provider API</h3>
Extending NatSpec with custom synonym providers is similar to the implementation of custom pattern providers. One must create a class in an OSGi bundle located in the current workspace that implements <tt>ISynonymProvider</tt>&nbsp;. This class will be automatically instantiated and asked for synonyms by NatSpec if required. Similar to syntax patterns, synonyms can be specific to certain specifications (e.g., depending on the project that contains the specification). <div style="page-break-after:always"></div></body>
</html>
