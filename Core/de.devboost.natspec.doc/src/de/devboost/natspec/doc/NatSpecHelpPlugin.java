package de.devboost.natspec.doc;

import java.io.File;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.devboost.natspec.doc.gettingstarted.GettingStarted;
import de.devboost.natspec.doc.guide.NatSpecGuide;

public class NatSpecHelpPlugin extends AbstractUIPlugin {

	public static final String HTML_PATH = "html";
	public static final String HTML_GUIDE_PATH = HTML_PATH + File.separator + NatSpecGuide.class.getSimpleName() + ".html";
	public static final String HTML_GETTING_STARTED_PATH = HTML_PATH + File.separator + GettingStarted.class.getSimpleName() + ".html";
	
	private static NatSpecHelpPlugin plugin;
	public static String PLUGIN_PATH = null;

	private ResourceBundle resourceBundle;
	
	public NatSpecHelpPlugin() {
		super();
		plugin = this;
	}

	public void start(BundleContext context) throws Exception {
		super.start(context);
		PLUGIN_PATH = FileLocator.resolve(plugin.getBundle().getEntry(".")).toString();
	}

	public void stop(BundleContext context) throws Exception {
		super.stop(context);
	}

	public static NatSpecHelpPlugin getDefault() {
		return plugin;
	}

	public static String getResourceString(String key) {
		ResourceBundle bundle = NatSpecHelpPlugin.getDefault().getResourceBundle();
		try {
			return (bundle != null) ? bundle.getString(key) : key;
		} catch (MissingResourceException e) {
			return key;
		}
	}

	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}	
}
