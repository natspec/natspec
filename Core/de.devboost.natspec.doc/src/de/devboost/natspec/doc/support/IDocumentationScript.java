package de.devboost.natspec.doc.support;

import de.devboost.natspec.library.documentation.Documentation;

public interface IDocumentationScript {

	public void executeScript() throws Exception;

	public Documentation getDocumentation();
}
