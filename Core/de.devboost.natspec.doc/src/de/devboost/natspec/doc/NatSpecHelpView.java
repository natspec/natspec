package de.devboost.natspec.doc;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

public class NatSpecHelpView extends ViewPart {
	
	public static final String ID_HELP_VIEW = "de.devboost.natspec.doc.NatSpecHelpView";
	
	private Browser browser;
	
	public void createPartControl(Composite parent) {
		parent.setLayout(new FillLayout());
		try {
			browser = new Browser(parent, SWT.NONE);
			String url = NatSpecHelpPlugin.PLUGIN_PATH + NatSpecHelpPlugin.HTML_GETTING_STARTED_PATH;
			browser.setUrl(url);
		} catch (SWTError e) {
			return;
		}
	}
	
	public void setFocus() {
	}
	
	@Override
	public void dispose() {
		browser.dispose();
		super.dispose();
	}
}
