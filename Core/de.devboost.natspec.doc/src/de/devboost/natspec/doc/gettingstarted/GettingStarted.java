package de.devboost.natspec.doc.gettingstarted;

import de.devboost.natspec.doc.support.IDocumentationScript;
import de.devboost.natspec.library.documentation.Documentation;
import de.devboost.natspec.library.documentation.DocumentationSupport;

public class GettingStarted implements IDocumentationScript {
	
	protected DocumentationSupport documentationSupport = new DocumentationSupport();
	
	@SuppressWarnings("unused")
	public void executeScript() throws Exception {
		int dummy;
		// generated code will be inserted here
		// The code in this method is generated from: /de.devboost.natspec.doc/src/de/devboost/natspec/doc/gettingstarted/GettingStarted.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Documentation - Getting Started with NatSpec
		de.devboost.natspec.library.documentation.Documentation documentation_Getting_Started_with_NatSpec = documentationSupport.initDocumentation(java.util.Arrays.asList(new java.lang.String[] {"Getting", "Started", "with", "NatSpec"}));
		// Section - What is NatSpec?
		de.devboost.natspec.library.documentation.Section section_What_is_NatSpec_ = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"What", "is", "NatSpec?"}), documentation_Getting_Started_with_NatSpec);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_ = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_What_is_NatSpec_);
		// The goal of any software development process is to deliver software to customers
		de.devboost.natspec.library.documentation.Text text_The_goal_of_any_software_development_process_is_to_deliver_software_to_customers = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "goal", "of", "any", "software", "development", "process", "is", "to", "deliver", "software", "to", "customers"}), paragraph_);
		// that satisfies their needs. Transforming requirements into a shipped product is
		de.devboost.natspec.library.documentation.Text text_that_satisfies_their_needs__Transforming_requirements_into_a_shipped_product_is = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"that", "satisfies", "their", "needs.", "Transforming", "requirements", "into", "a", "shipped", "product", "is"}), paragraph_);
		// essentially the job that needs to be done.
		de.devboost.natspec.library.documentation.Text text_essentially_the_job_that_needs_to_be_done_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"essentially", "the", "job", "that", "needs", "to", "be", "done."}), paragraph_);
		// Requirements are often described in natural language. In modern software
		de.devboost.natspec.library.documentation.Text text_Requirements_are_often_described_in_natural_language__In_modern_software = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Requirements", "are", "often", "described", "in", "natural", "language.", "In", "modern", "software"}), paragraph_);
		// development, checking whether those requirements are met and implemented
		de.devboost.natspec.library.documentation.Text text_development__checking_whether_those_requirements_are_met_and_implemented = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"development,", "checking", "whether", "those", "requirements", "are", "met", "and", "implemented"}), paragraph_);
		// correctly is typically done by using unit tests. Such unit tests are usually
		de.devboost.natspec.library.documentation.Text text_correctly_is_typically_done_by_using_unit_tests__Such_unit_tests_are_usually = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"correctly", "is", "typically", "done", "by", "using", "unit", "tests.", "Such", "unit", "tests", "are", "usually"}), paragraph_);
		// described in a programming language like Java.
		de.devboost.natspec.library.documentation.Text text_described_in_a_programming_language_like_Java_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"described", "in", "a", "programming", "language", "like", "Java."}), paragraph_);
		// The language gap between describing the requirements in natural language and
		de.devboost.natspec.library.documentation.Text text_The_language_gap_between_describing_the_requirements_in_natural_language_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "language", "gap", "between", "describing", "the", "requirements", "in", "natural", "language", "and"}), paragraph_);
		// implementing them as unit tests makes it
		de.devboost.natspec.library.documentation.Text text_implementing_them_as_unit_tests_makes_it = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"implementing", "them", "as", "unit", "tests", "makes", "it"}), paragraph_);
		// difficult for customers and developers to communicate effectively.
		de.devboost.natspec.library.documentation.Text text_difficult_for_customers_and_developers_to_communicate_effectively_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"difficult", "for", "customers", "and", "developers", "to", "communicate", "effectively."}), paragraph_);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_0 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_What_is_NatSpec_);
		// NatSpec bridges this gap by allowing to specify requirements and use cases in natural
		de.devboost.natspec.library.documentation.Text text_NatSpec_bridges_this_gap_by_allowing_to_specify_requirements_and_use_cases_in_natural = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "bridges", "this", "gap", "by", "allowing", "to", "specify", "requirements", "and", "use", "cases", "in", "natural"}), paragraph_0);
		// language and generating unit tests checking for the specified functionality automatically.
		de.devboost.natspec.library.documentation.Text text_language_and_generating_unit_tests_checking_for_the_specified_functionality_automatically_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"language", "and", "generating", "unit", "tests", "checking", "for", "the", "specified", "functionality", "automatically."}), paragraph_0);
		// In the following, the usage of NatSpec is illustrated by a running example. Beforehand,
		de.devboost.natspec.library.documentation.Text text_In_the_following__the_usage_of_NatSpec_is_illustrated_by_a_running_example__Beforehand_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"In", "the", "following,", "the", "usage", "of", "NatSpec", "is", "illustrated", "by", "a", "running", "example.", "Beforehand,"}), paragraph_0);
		// it is shown how NatSpec can be installed and configured for its usage within the Eclipse IDE.
		de.devboost.natspec.library.documentation.Text text_it_is_shown_how_NatSpec_can_be_installed_and_configured_for_its_usage_within_the_Eclipse_IDE_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"it", "is", "shown", "how", "NatSpec", "can", "be", "installed", "and", "configured", "for", "its", "usage", "within", "the", "Eclipse", "IDE."}), paragraph_0);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_1 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_What_is_NatSpec_);
		// For further hints on what NatSpec is good for and how it can be used and customized, feel
		de.devboost.natspec.library.documentation.Text text_For_further_hints_on_what_NatSpec_is_good_for_and_how_it_can_be_used_and_customized__feel = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"For", "further", "hints", "on", "what", "NatSpec", "is", "good", "for", "and", "how", "it", "can", "be", "used", "and", "customized,", "feel"}), paragraph_1);
		// free to also consult NatSpec documentation.
		de.devboost.natspec.library.documentation.Text text_free_to_also_consult_NatSpec_documentation_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"free", "to", "also", "consult", "NatSpec", "documentation."}), paragraph_1);
		// Section - How to install NatSpec
		de.devboost.natspec.library.documentation.Section section_How_to_install_NatSpec = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"How", "to", "install", "NatSpec"}), documentation_Getting_Started_with_NatSpec);
		// This section explains how NatSpec is installed within a distribution of the Eclipse IDE.
		de.devboost.natspec.library.documentation.Text text_This_section_explains_how_NatSpec_is_installed_within_a_distribution_of_the_Eclipse_IDE_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"This", "section", "explains", "how", "NatSpec", "is", "installed", "within", "a", "distribution", "of", "the", "Eclipse", "IDE."}), section_How_to_install_NatSpec);
		// If you installed NatSpec already, you can skip this section.
		de.devboost.natspec.library.documentation.Text text_If_you_installed_NatSpec_already__you_can_skip_this_section_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "you", "installed", "NatSpec", "already,", "you", "can", "skip", "this", "section."}), section_How_to_install_NatSpec);
		// Subsection - Required Software to Run NatSpec
		de.devboost.natspec.library.documentation.Subsection subsection_Required_Software_to_Run_NatSpec = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Required", "Software", "to", "Run", "NatSpec"}), section_How_to_install_NatSpec);
		// As NatSpec is implemented as an extension (plugin) of the Eclipse IDE, Eclipse is required
		de.devboost.natspec.library.documentation.Text text_As_NatSpec_is_implemented_as_an_extension__plugin__of_the_Eclipse_IDE__Eclipse_is_required = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "NatSpec", "is", "implemented", "as", "an", "extension", "(plugin)", "of", "the", "Eclipse", "IDE,", "Eclipse", "is", "required"}), subsection_Required_Software_to_Run_NatSpec);
		// for the usage of NatSpec. You can download Eclipse from
		de.devboost.natspec.library.documentation.Text text_for_the_usage_of_NatSpec__You_can_download_Eclipse_from = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"for", "the", "usage", "of", "NatSpec.", "You", "can", "download", "Eclipse", "from"}), subsection_Required_Software_to_Run_NatSpec);
		// Link to http://www.eclipse.org/
		de.devboost.natspec.library.documentation.Link link_http___www_eclipse_org_ = documentationSupport.link("http://www.eclipse.org/", subsection_Required_Software_to_Run_NatSpec);
		// or
		de.devboost.natspec.library.documentation.Text text_or = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"or"}), subsection_Required_Software_to_Run_NatSpec);
		// Link to http://www.nat-spec.com/download.html with caption download a distribution having NatSpec installed
		de.devboost.natspec.library.documentation.Link link_download_a_distribution_having_NatSpec_installed_http___www_nat_spec_com_download_html = documentationSupport.link(java.util.Arrays.asList(new java.lang.String[] {"download", "a", "distribution", "having", "NatSpec", "installed"}), "http://www.nat-spec.com/download.html", subsection_Required_Software_to_Run_NatSpec);
		// .
		de.devboost.natspec.library.documentation.Text text__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), subsection_Required_Software_to_Run_NatSpec);
		// Feel free to use any Eclipse distribution you like. However, as NatSpec bases on Java and
		de.devboost.natspec.library.documentation.Text text_Feel_free_to_use_any_Eclipse_distribution_you_like__However__as_NatSpec_bases_on_Java_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Feel", "free", "to", "use", "any", "Eclipse", "distribution", "you", "like.", "However,", "as", "NatSpec", "bases", "on", "Java", "and"}), subsection_Required_Software_to_Run_NatSpec);
		// JUnit, make sure that your distribution includes at least the Java Development Tools (JDT).
		de.devboost.natspec.library.documentation.Text text_JUnit__make_sure_that_your_distribution_includes_at_least_the_Java_Development_Tools__JDT__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"JUnit,", "make", "sure", "that", "your", "distribution", "includes", "at", "least", "the", "Java", "Development", "Tools", "(JDT)."}), subsection_Required_Software_to_Run_NatSpec);
		// Subsection - Installing NatSpec in Eclipse
		de.devboost.natspec.library.documentation.Subsection subsection_Installing_NatSpec_in_Eclipse = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Installing", "NatSpec", "in", "Eclipse"}), section_How_to_install_NatSpec);
		// If you downloaded a complete Eclipse distribution including NatSpec from
		de.devboost.natspec.library.documentation.Text text_If_you_downloaded_a_complete_Eclipse_distribution_including_NatSpec_from = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "you", "downloaded", "a", "complete", "Eclipse", "distribution", "including", "NatSpec", "from"}), subsection_Installing_NatSpec_in_Eclipse);
		// Link to http://www.nat-spec.com/download.html
		de.devboost.natspec.library.documentation.Link link_http___www_nat_spec_com_download_html = documentationSupport.link("http://www.nat-spec.com/download.html", subsection_Installing_NatSpec_in_Eclipse);
		// , you don't need to install further software at all. However, most of the times you want to
		de.devboost.natspec.library.documentation.Text text___you_don_t_need_to_install_further_software_at_all__However__most_of_the_times_you_want_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "you", "don't", "need", "to", "install", "further", "software", "at", "all.", "However,", "most", "of", "the", "times", "you", "want", "to"}), subsection_Installing_NatSpec_in_Eclipse);
		// install NatSpec within your personalized, existing Eclipse IDE. Generally, two possibilities
		de.devboost.natspec.library.documentation.Text text_install_NatSpec_within_your_personalized__existing_Eclipse_IDE__Generally__two_possibilities = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"install", "NatSpec", "within", "your", "personalized,", "existing", "Eclipse", "IDE.", "Generally,", "two", "possibilities"}), subsection_Installing_NatSpec_in_Eclipse);
		// to install NatSpec into your Eclipse exist:
		de.devboost.natspec.library.documentation.Text text_to_install_NatSpec_into_your_Eclipse_exist_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"to", "install", "NatSpec", "into", "your", "Eclipse", "exist:"}), subsection_Installing_NatSpec_in_Eclipse);
		// Subsubsection - Installing NatSpec via Eclipse Market Place
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Installing_NatSpec_via_Eclipse_Market_Place = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Installing", "NatSpec", "via", "Eclipse", "Market", "Place"}), subsection_Installing_NatSpec_in_Eclipse);
		// Within Eclipse, select the menu option
		de.devboost.natspec.library.documentation.Text text_Within_Eclipse__select_the_menu_option = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Within", "Eclipse,", "select", "the", "menu", "option"}), subsubsection_Installing_NatSpec_via_Eclipse_Market_Place);
		// Code Help > Eclipse Marketplace...
		de.devboost.natspec.library.documentation.Code code_Help___Eclipse_Marketplace___ = documentationSupport.code(new java.lang.StringBuilder().append("Help").append(" ").append(">").append(" ").append("Eclipse").append(" ").append("Marketplace...").toString(), subsubsection_Installing_NatSpec_via_Eclipse_Market_Place);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_2 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Installing_NatSpec_via_Eclipse_Market_Place);
		// Enter
		de.devboost.natspec.library.documentation.Text text_Enter = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Enter"}), paragraph_2);
		// Code NatSpec
		de.devboost.natspec.library.documentation.Code code_NatSpec = documentationSupport.code(new java.lang.StringBuilder().append("NatSpec").toString(), paragraph_2);
		// into the search dialog and click on the
		de.devboost.natspec.library.documentation.Text text_into_the_search_dialog_and_click_on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"into", "the", "search", "dialog", "and", "click", "on", "the"}), paragraph_2);
		// Code Go
		de.devboost.natspec.library.documentation.Code code_Go = documentationSupport.code(new java.lang.StringBuilder().append("Go").toString(), paragraph_2);
		// button.
		de.devboost.natspec.library.documentation.Text text_button_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button."}), paragraph_2);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_3 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Installing_NatSpec_via_Eclipse_Market_Place);
		// NatSpec should appear in the search results:
		de.devboost.natspec.library.documentation.Text text_NatSpec_should_appear_in_the_search_results_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "should", "appear", "in", "the", "search", "results:"}), paragraph_3);
		// Image of Dialog to install NatSpec via Eclipse Marketplace at html/images/install_marketplace.png width 566 px
		de.devboost.natspec.library.documentation.Image image_Dialog_to_install_NatSpec_via_Eclipse_Marketplace_html_images_install_marketplace_png_566_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Dialog", "to", "install", "NatSpec", "via", "Eclipse", "Marketplace"}), "html/images/install_marketplace.png", "566", "px", subsubsection_Installing_NatSpec_via_Eclipse_Market_Place);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_4 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Installing_NatSpec_via_Eclipse_Market_Place);
		// Click on the
		de.devboost.natspec.library.documentation.Text text_Click_on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Click", "on", "the"}), paragraph_4);
		// Code Install
		de.devboost.natspec.library.documentation.Code code_Install = documentationSupport.code(new java.lang.StringBuilder().append("Install").toString(), paragraph_4);
		// button and follow the instructions to install NatSpec.
		de.devboost.natspec.library.documentation.Text text_button_and_follow_the_instructions_to_install_NatSpec_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button", "and", "follow", "the", "instructions", "to", "install", "NatSpec."}), paragraph_4);
		// Subsubsection - Installing NatSpec via NatSpec Update Site
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Installing_NatSpec_via_NatSpec_Update_Site = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Installing", "NatSpec", "via", "NatSpec", "Update", "Site"}), subsection_Installing_NatSpec_in_Eclipse);
		// Within Eclipse, select the menu option
		de.devboost.natspec.library.documentation.Text text_Within_Eclipse__select_the_menu_option0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Within", "Eclipse,", "select", "the", "menu", "option"}), subsubsection_Installing_NatSpec_via_NatSpec_Update_Site);
		// Code Help > Install New Software...
		de.devboost.natspec.library.documentation.Code code_Help___Install_New_Software___ = documentationSupport.code(new java.lang.StringBuilder().append("Help").append(" ").append(">").append(" ").append("Install").append(" ").append("New").append(" ").append("Software...").toString(), subsubsection_Installing_NatSpec_via_NatSpec_Update_Site);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_5 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Installing_NatSpec_via_NatSpec_Update_Site);
		// Enter the link to the NatSpec Update Site (
		de.devboost.natspec.library.documentation.Text text_Enter_the_link_to_the_NatSpec_Update_Site__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Enter", "the", "link", "to", "the", "NatSpec", "Update", "Site", "("}), paragraph_5);
		// Link to http://www.nat-spec.com/update/
		de.devboost.natspec.library.documentation.Link link_http___www_nat_spec_com_update_ = documentationSupport.link("http://www.nat-spec.com/update/", subsubsection_Installing_NatSpec_via_NatSpec_Update_Site);
		// ) into the URI text field and type
		de.devboost.natspec.library.documentation.Text text___into_the_URI_text_field_and_type = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {")", "into", "the", "URI", "text", "field", "and", "type"}), paragraph_5);
		// Code ENTER
		de.devboost.natspec.library.documentation.Code code_ENTER = documentationSupport.code(new java.lang.StringBuilder().append("ENTER").toString(), paragraph_5);
		// .
		de.devboost.natspec.library.documentation.Text text__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), paragraph_5);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_6 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Installing_NatSpec_via_NatSpec_Update_Site);
		// Select
		de.devboost.natspec.library.documentation.Text text_Select = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Select"}), paragraph_6);
		// Code NatSpec
		de.devboost.natspec.library.documentation.Code code_NatSpec0 = documentationSupport.code(new java.lang.StringBuilder().append("NatSpec").toString(), paragraph_6);
		// from the items to install, click on the
		de.devboost.natspec.library.documentation.Text text_from_the_items_to_install__click_on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"from", "the", "items", "to", "install,", "click", "on", "the"}), paragraph_6);
		// Code Next
		de.devboost.natspec.library.documentation.Code code_Next = documentationSupport.code(new java.lang.StringBuilder().append("Next").toString(), paragraph_6);
		// button and follow the instructions to install NatSpec:
		de.devboost.natspec.library.documentation.Text text_button_and_follow_the_instructions_to_install_NatSpec_0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button", "and", "follow", "the", "instructions", "to", "install", "NatSpec:"}), paragraph_6);
		// Image of Dialog to install NatSpec via NatSpec Update Site at html/images/install_updatesite.png width 661 px
		de.devboost.natspec.library.documentation.Image image_Dialog_to_install_NatSpec_via_NatSpec_Update_Site_html_images_install_updatesite_png_661_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Dialog", "to", "install", "NatSpec", "via", "NatSpec", "Update", "Site"}), "html/images/install_updatesite.png", "661", "px", subsubsection_Installing_NatSpec_via_NatSpec_Update_Site);
		// Subsection - Getting Your Personal NatSpec License
		de.devboost.natspec.library.documentation.Subsection subsection_Getting_Your_Personal_NatSpec_License = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Getting", "Your", "Personal", "NatSpec", "License"}), section_How_to_install_NatSpec);
		// To use NatSpec you will need a NatSpec license. Once you have installed NatSpec and restarted
		de.devboost.natspec.library.documentation.Text text_To_use_NatSpec_you_will_need_a_NatSpec_license__Once_you_have_installed_NatSpec_and_restarted = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "use", "NatSpec", "you", "will", "need", "a", "NatSpec", "license.", "Once", "you", "have", "installed", "NatSpec", "and", "restarted"}), subsection_Getting_Your_Personal_NatSpec_License);
		// your Eclipse distribution, a respective dialog will appear to enter a NatSpec license.
		de.devboost.natspec.library.documentation.Text text_your_Eclipse_distribution__a_respective_dialog_will_appear_to_enter_a_NatSpec_license_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"your", "Eclipse", "distribution,", "a", "respective", "dialog", "will", "appear", "to", "enter", "a", "NatSpec", "license."}), subsection_Getting_Your_Personal_NatSpec_License);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_7 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Getting_Your_Personal_NatSpec_License);
		// You can either apply for a trial license or buy a regular license. Students and research
		de.devboost.natspec.library.documentation.Text text_You_can_either_apply_for_a_trial_license_or_buy_a_regular_license__Students_and_research = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"You", "can", "either", "apply", "for", "a", "trial", "license", "or", "buy", "a", "regular", "license.", "Students", "and", "research"}), paragraph_7);
		// institutions can apply for a NatSpec license for free. Visit
		de.devboost.natspec.library.documentation.Text text_institutions_can_apply_for_a_NatSpec_license_for_free__Visit = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"institutions", "can", "apply", "for", "a", "NatSpec", "license", "for", "free.", "Visit"}), paragraph_7);
		// Link to http://www.nat-spec.com/buy.html with caption the NatSpec website
		de.devboost.natspec.library.documentation.Link link_the_NatSpec_website_http___www_nat_spec_com_buy_html = documentationSupport.link(java.util.Arrays.asList(new java.lang.String[] {"the", "NatSpec", "website"}), "http://www.nat-spec.com/buy.html", subsection_Getting_Your_Personal_NatSpec_License);
		// to learn more about NatSpec licenses.
		de.devboost.natspec.library.documentation.Text text_to_learn_more_about_NatSpec_licenses_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"to", "learn", "more", "about", "NatSpec", "licenses."}), paragraph_7);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_8 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Getting_Your_Personal_NatSpec_License);
		// To apply for a trial license you can use the opened dialog in Eclipse:
		de.devboost.natspec.library.documentation.Text text_To_apply_for_a_trial_license_you_can_use_the_opened_dialog_in_Eclipse_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "apply", "for", "a", "trial", "license", "you", "can", "use", "the", "opened", "dialog", "in", "Eclipse:"}), paragraph_8);
		// Select the option
		de.devboost.natspec.library.documentation.Text text_Select_the_option = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Select", "the", "option"}), paragraph_8);
		// Code Request free trial license
		de.devboost.natspec.library.documentation.Code code_Request_free_trial_license = documentationSupport.code(new java.lang.StringBuilder().append("Request").append(" ").append("free").append(" ").append("trial").append(" ").append("license").toString(), paragraph_8);
		// and click on the
		de.devboost.natspec.library.documentation.Text text_and_click_on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "click", "on", "the"}), paragraph_8);
		// Code Next
		de.devboost.natspec.library.documentation.Code code_Next0 = documentationSupport.code(new java.lang.StringBuilder().append("Next").toString(), paragraph_8);
		// button.
		de.devboost.natspec.library.documentation.Text text_button_0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button."}), paragraph_8);
		// Image of Dialog to request a NatSpec trial license (part 1) at html/images/license_dialog_1.png width 611 px
		de.devboost.natspec.library.documentation.Image image_Dialog_to_request_a_NatSpec_trial_license__part_1__html_images_license_dialog_1_png_611_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Dialog", "to", "request", "a", "NatSpec", "trial", "license", "(part", "1)"}), "html/images/license_dialog_1.png", "611", "px", subsection_Getting_Your_Personal_NatSpec_License);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_9 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Getting_Your_Personal_NatSpec_License);
		// Enter your name and your email address and click on the
		de.devboost.natspec.library.documentation.Text text_Enter_your_name_and_your_email_address_and_click_on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Enter", "your", "name", "and", "your", "email", "address", "and", "click", "on", "the"}), paragraph_9);
		// Code Next
		de.devboost.natspec.library.documentation.Code code_Next1 = documentationSupport.code(new java.lang.StringBuilder().append("Next").toString(), paragraph_9);
		// button.
		de.devboost.natspec.library.documentation.Text text_button_1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button."}), paragraph_9);
		// Image of Dialog to request a NatSpec trial license (part 2) at html/images/license_dialog_2.png width 611 px
		de.devboost.natspec.library.documentation.Image image_Dialog_to_request_a_NatSpec_trial_license__part_2__html_images_license_dialog_2_png_611_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Dialog", "to", "request", "a", "NatSpec", "trial", "license", "(part", "2)"}), "html/images/license_dialog_2.png", "611", "px", subsection_Getting_Your_Personal_NatSpec_License);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_10 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Getting_Your_Personal_NatSpec_License);
		// You should receive an email containing a trial license key you can enter into the following
		de.devboost.natspec.library.documentation.Text text_You_should_receive_an_email_containing_a_trial_license_key_you_can_enter_into_the_following = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"You", "should", "receive", "an", "email", "containing", "a", "trial", "license", "key", "you", "can", "enter", "into", "the", "following"}), paragraph_10);
		// screen:
		de.devboost.natspec.library.documentation.Text text_screen_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"screen:"}), paragraph_10);
		// Image of Dialog to enter NatSpec license at html/images/license_dialog_3.png width 611 px
		de.devboost.natspec.library.documentation.Image image_Dialog_to_enter_NatSpec_license_html_images_license_dialog_3_png_611_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Dialog", "to", "enter", "NatSpec", "license"}), "html/images/license_dialog_3.png", "611", "px", subsection_Getting_Your_Personal_NatSpec_License);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_11 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Getting_Your_Personal_NatSpec_License);
		// Proceed with the dialog by clicking on the
		de.devboost.natspec.library.documentation.Text text_Proceed_with_the_dialog_by_clicking_on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Proceed", "with", "the", "dialog", "by", "clicking", "on", "the"}), paragraph_11);
		// Code Next
		de.devboost.natspec.library.documentation.Code code_Next2 = documentationSupport.code(new java.lang.StringBuilder().append("Next").toString(), paragraph_11);
		// button. Now you should be able to use an try your NatSpec distribution.
		de.devboost.natspec.library.documentation.Text text_button__Now_you_should_be_able_to_use_an_try_your_NatSpec_distribution_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button.", "Now", "you", "should", "be", "able", "to", "use", "an", "try", "your", "NatSpec", "distribution."}), paragraph_11);
		// Section - A Running Example
		de.devboost.natspec.library.documentation.Section section_A_Running_Example = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"A", "Running", "Example"}), documentation_Getting_Started_with_NatSpec);
		// Similar to the NatSpec documentation, this tutorial uses an example scenario for an
		de.devboost.natspec.library.documentation.Text text_Similar_to_the_NatSpec_documentation__this_tutorial_uses_an_example_scenario_for_an = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Similar", "to", "the", "NatSpec", "documentation,", "this", "tutorial", "uses", "an", "example", "scenario", "for", "an"}), section_A_Running_Example);
		// application allowing the booking of airflight tickets. A simple test scenario for this
		de.devboost.natspec.library.documentation.Text text_application_allowing_the_booking_of_airflight_tickets__A_simple_test_scenario_for_this = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"application", "allowing", "the", "booking", "of", "airflight", "tickets.", "A", "simple", "test", "scenario", "for", "this"}), section_A_Running_Example);
		// application written in native language looks as follows:
		de.devboost.natspec.library.documentation.Text text_application_written_in_native_language_looks_as_follows_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"application", "written", "in", "native", "language", "looks", "as", "follows:"}), section_A_Running_Example);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_ = documentationSupport.beginListing(section_A_Running_Example);
		// Given a Passenger John Doe
		de.devboost.natspec.library.documentation.Text text_Given_a_Passenger_John_Doe = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Given", "a", "Passenger", "John", "Doe"}), listing_);
		// Given an Airplane Boeing-787
		de.devboost.natspec.library.documentation.Text text_Given_an_Airplane_Boeing_787 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Given", "an", "Airplane", "Boeing-787"}), listing_);
		// With 200 total seats
		de.devboost.natspec.library.documentation.Text text_With_200_total_seats = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"With", "200", "total", "seats"}), listing_);
		// Given a flight LH-1234
		de.devboost.natspec.library.documentation.Text text_Given_a_flight_LH_1234 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Given", "a", "flight", "LH-1234"}), listing_);
		// With Boeing-787 airplane
		de.devboost.natspec.library.documentation.Text text_With_Boeing_787_airplane = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"With", "Boeing-787", "airplane"}), listing_);
		// With 200 free seats
		de.devboost.natspec.library.documentation.Text text_With_200_free_seats = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"With", "200", "free", "seats"}), listing_);
		// Book seat for John Doe at LH-1234
		de.devboost.natspec.library.documentation.Text text_Book_seat_for_John_Doe_at_LH_1234 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Book", "seat", "for", "John", "Doe", "at", "LH-1234"}), listing_);
		// Assume success
		de.devboost.natspec.library.documentation.Text text_Assume_success = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Assume", "success"}), listing_);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_12 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_A_Running_Example);
		// This tutorial uses this example scenario to show how test scenarios, as well as respective
		de.devboost.natspec.library.documentation.Text text_This_tutorial_uses_this_example_scenario_to_show_how_test_scenarios__as_well_as_respective = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"This", "tutorial", "uses", "this", "example", "scenario", "to", "show", "how", "test", "scenarios,", "as", "well", "as", "respective"}), paragraph_12);
		// executable test cases are created and executed with NatSpec.
		de.devboost.natspec.library.documentation.Text text_executable_test_cases_are_created_and_executed_with_NatSpec_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"executable", "test", "cases", "are", "created", "and", "executed", "with", "NatSpec."}), paragraph_12);
		// Subsection - Setup of the Example Project
		de.devboost.natspec.library.documentation.Subsection subsection_Setup_of_the_Example_Project = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Setup", "of", "the", "Example", "Project"}), section_A_Running_Example);
		// To make our test scenario executable, we need an example flight booking application we can
		de.devboost.natspec.library.documentation.Text text_To_make_our_test_scenario_executable__we_need_an_example_flight_booking_application_we_can = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "make", "our", "test", "scenario", "executable,", "we", "need", "an", "example", "flight", "booking", "application", "we", "can"}), subsection_Setup_of_the_Example_Project);
		// test with our NatSpec scenarios. If you installed the complete Eclipse distribution including
		de.devboost.natspec.library.documentation.Text text_test_with_our_NatSpec_scenarios__If_you_installed_the_complete_Eclipse_distribution_including = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"test", "with", "our", "NatSpec", "scenarios.", "If", "you", "installed", "the", "complete", "Eclipse", "distribution", "including"}), subsection_Setup_of_the_Example_Project);
		// NatSpec, your workspace already will contain the required example project, including the
		de.devboost.natspec.library.documentation.Text text_NatSpec__your_workspace_already_will_contain_the_required_example_project__including_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec,", "your", "workspace", "already", "will", "contain", "the", "required", "example", "project,", "including", "the"}), subsection_Setup_of_the_Example_Project);
		// example application as well as some NatSpec test cases.
		de.devboost.natspec.library.documentation.Text text_example_application_as_well_as_some_NatSpec_test_cases_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"example", "application", "as", "well", "as", "some", "NatSpec", "test", "cases."}), subsection_Setup_of_the_Example_Project);
		// If you used another Eclipse distribution or if you deleted the example project already,
		de.devboost.natspec.library.documentation.Text text_If_you_used_another_Eclipse_distribution_or_if_you_deleted_the_example_project_already_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "you", "used", "another", "Eclipse", "distribution", "or", "if", "you", "deleted", "the", "example", "project", "already,"}), subsection_Setup_of_the_Example_Project);
		// you can create a new NatSpec example project using the menu option
		de.devboost.natspec.library.documentation.Text text_you_can_create_a_new_NatSpec_example_project_using_the_menu_option = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"you", "can", "create", "a", "new", "NatSpec", "example", "project", "using", "the", "menu", "option"}), subsection_Setup_of_the_Example_Project);
		// Code File > New > Other... > NatSpec Project
		de.devboost.natspec.library.documentation.Code code_File___New___Other______NatSpec_Project = documentationSupport.code(new java.lang.StringBuilder().append("File").append(" ").append(">").append(" ").append("New").append(" ").append(">").append(" ").append("Other...").append(" ").append(">").append(" ").append("NatSpec").append(" ").append("Project").toString(), subsection_Setup_of_the_Example_Project);
		// :
		de.devboost.natspec.library.documentation.Text text__1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {":"}), subsection_Setup_of_the_Example_Project);
		// Image of Dialog to create a new NatSpec Example Project (part 1) at html/images/new_project_1.png width 525 px
		de.devboost.natspec.library.documentation.Image image_Dialog_to_create_a_new_NatSpec_Example_Project__part_1__html_images_new_project_1_png_525_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Dialog", "to", "create", "a", "new", "NatSpec", "Example", "Project", "(part", "1)"}), "html/images/new_project_1.png", "525", "px", subsection_Setup_of_the_Example_Project);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_13 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Setup_of_the_Example_Project);
		// Enter the name of the project (e.g.,
		de.devboost.natspec.library.documentation.Text text_Enter_the_name_of_the_project__e_g__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Enter", "the", "name", "of", "the", "project", "(e.g.,"}), paragraph_13);
		// Code de.devboost.natspec.examples.airline
		de.devboost.natspec.library.documentation.Code code_de_devboost_natspec_examples_airline = documentationSupport.code(new java.lang.StringBuilder().append("de.devboost.natspec.examples.airline").toString(), paragraph_13);
		// ), and click on the
		de.devboost.natspec.library.documentation.Text text____and_click_on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"),", "and", "click", "on", "the"}), paragraph_13);
		// Code Finish
		de.devboost.natspec.library.documentation.Code code_Finish = documentationSupport.code(new java.lang.StringBuilder().append("Finish").toString(), paragraph_13);
		// button:
		de.devboost.natspec.library.documentation.Text text_button_2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button:"}), paragraph_13);
		// Image of Dialog to create a new NatSpec Example Project (part 2) at html/images/new_project_2.png width 525 px
		de.devboost.natspec.library.documentation.Image image_Dialog_to_create_a_new_NatSpec_Example_Project__part_2__html_images_new_project_2_png_525_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Dialog", "to", "create", "a", "new", "NatSpec", "Example", "Project", "(part", "2)"}), "html/images/new_project_2.png", "525", "px", subsection_Setup_of_the_Example_Project);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_14 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Setup_of_the_Example_Project);
		// Afterwards, the newly created project should look as follows:
		de.devboost.natspec.library.documentation.Text text_Afterwards__the_newly_created_project_should_look_as_follows_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Afterwards,", "the", "newly", "created", "project", "should", "look", "as", "follows:"}), paragraph_14);
		// Image of The created NatSpec Example Project at html/images/new_project_3.png width 334 px
		de.devboost.natspec.library.documentation.Image image_The_created_NatSpec_Example_Project_html_images_new_project_3_png_334_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "created", "NatSpec", "Example", "Project"}), "html/images/new_project_3.png", "334", "px", subsection_Setup_of_the_Example_Project);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_15 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Setup_of_the_Example_Project);
		// The example project contains the following packages:
		de.devboost.natspec.library.documentation.Text text_The_example_project_contains_the_following_packages_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "example", "project", "contains", "the", "following", "packages:"}), paragraph_15);
		// List
		de.devboost.natspec.library.documentation.List list_ = documentationSupport.addList(subsection_Setup_of_the_Example_Project);
		// * com.nat_spec.airline.example.persistence
		de.devboost.natspec.library.documentation.ListItem listItem_com_nat_spec_airline_example_persistence = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"com.nat_spec.airline.example.persistence"}), list_);
		// * com.nat_spec.airline.example.persistence.entities
		de.devboost.natspec.library.documentation.ListItem listItem_com_nat_spec_airline_example_persistence_entities = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"com.nat_spec.airline.example.persistence.entities"}), list_);
		// * com.nat_spec.airline.example.service
		de.devboost.natspec.library.documentation.ListItem listItem_com_nat_spec_airline_example_service = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"com.nat_spec.airline.example.service"}), list_);
		// * com.nat_spec.airline.example.servicevalidation
		de.devboost.natspec.library.documentation.ListItem listItem_com_nat_spec_airline_example_servicevalidation = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"com.nat_spec.airline.example.servicevalidation"}), list_);
		// * com.nat_spec.airline.example.test
		de.devboost.natspec.library.documentation.ListItem listItem_com_nat_spec_airline_example_test = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"com.nat_spec.airline.example.test"}), list_);
		// * com.nat_spec.airline.example.testscenarios
		de.devboost.natspec.library.documentation.ListItem listItem_com_nat_spec_airline_example_testscenarios = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"com.nat_spec.airline.example.testscenarios"}), list_);
		// * com.nat_spec.airline.example.testsupport
		de.devboost.natspec.library.documentation.ListItem listItem_com_nat_spec_airline_example_testsupport = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"com.nat_spec.airline.example.testsupport"}), list_);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_16 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Setup_of_the_Example_Project);
		// The packages
		de.devboost.natspec.library.documentation.Text text_The_packages = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "packages"}), paragraph_16);
		// Code com.nat_spec.airline.example.persistence
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_persistence = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.persistence").toString(), paragraph_16);
		// and
		de.devboost.natspec.library.documentation.Text text_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and"}), paragraph_16);
		// Code com.nat_spec.airline.example.persistence.entities
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_persistence_entities = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.persistence.entities").toString(), paragraph_16);
		// provide the persistence layer of the application under test and can be used for our
		de.devboost.natspec.library.documentation.Text text_provide_the_persistence_layer_of_the_application_under_test_and_can_be_used_for_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"provide", "the", "persistence", "layer", "of", "the", "application", "under", "test", "and", "can", "be", "used", "for", "our"}), paragraph_16);
		// example test scenario. Besides, the packages
		de.devboost.natspec.library.documentation.Text text_example_test_scenario__Besides__the_packages = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"example", "test", "scenario.", "Besides,", "the", "packages"}), paragraph_16);
		// Code com.nat_spec.airline.example.service
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_service = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.service").toString(), paragraph_16);
		// and
		de.devboost.natspec.library.documentation.Text text_and0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and"}), paragraph_16);
		// Code com.nat_spec.airline.example.servicevalidation
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_servicevalidation = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.servicevalidation").toString(), paragraph_16);
		// provide the service layer of the application under test for our example test scenario.
		de.devboost.natspec.library.documentation.Text text_provide_the_service_layer_of_the_application_under_test_for_our_example_test_scenario_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"provide", "the", "service", "layer", "of", "the", "application", "under", "test", "for", "our", "example", "test", "scenario."}), paragraph_16);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_17 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Setup_of_the_Example_Project);
		// The package
		de.devboost.natspec.library.documentation.Text text_The_package = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "package"}), paragraph_17);
		// Code com.nat_spec.airline.example.test
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_test = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.test").toString(), paragraph_17);
		// contains an example JUnit test showing how such a test would be written without using
		de.devboost.natspec.library.documentation.Text text_contains_an_example_JUnit_test_showing_how_such_a_test_would_be_written_without_using = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"contains", "an", "example", "JUnit", "test", "showing", "how", "such", "a", "test", "would", "be", "written", "without", "using"}), paragraph_17);
		// NatSpec. You can either delete or keep this package and test case. We will not further
		de.devboost.natspec.library.documentation.Text text_NatSpec__You_can_either_delete_or_keep_this_package_and_test_case__We_will_not_further = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec.", "You", "can", "either", "delete", "or", "keep", "this", "package", "and", "test", "case.", "We", "will", "not", "further"}), paragraph_17);
		// use them throughout this tutorial.
		de.devboost.natspec.library.documentation.Text text_use_them_throughout_this_tutorial_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"use", "them", "throughout", "this", "tutorial."}), paragraph_17);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_18 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Setup_of_the_Example_Project);
		// The packages
		de.devboost.natspec.library.documentation.Text text_The_packages0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "packages"}), paragraph_18);
		// Code com.nat_spec.airline.example.testscenarios
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_testscenarios = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.testscenarios").toString(), paragraph_18);
		// and
		de.devboost.natspec.library.documentation.Text text_and1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and"}), paragraph_18);
		// Code com.nat_spec.airline.example.testsupport
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_testsupport = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.testsupport").toString(), paragraph_18);
		// contain all the NatSpec magic. As we want to create the content of these packages during this
		de.devboost.natspec.library.documentation.Text text_contain_all_the_NatSpec_magic__As_we_want_to_create_the_content_of_these_packages_during_this = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"contain", "all", "the", "NatSpec", "magic.", "As", "we", "want", "to", "create", "the", "content", "of", "these", "packages", "during", "this"}), paragraph_18);
		// tutorial, you can delete all files from these three packages. After deleting the content
		de.devboost.natspec.library.documentation.Text text_tutorial__you_can_delete_all_files_from_these_three_packages__After_deleting_the_content = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"tutorial,", "you", "can", "delete", "all", "files", "from", "these", "three", "packages.", "After", "deleting", "the", "content"}), paragraph_18);
		// of these packages, the example project should look as follows:
		de.devboost.natspec.library.documentation.Text text_of_these_packages__the_example_project_should_look_as_follows_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "these", "packages,", "the", "example", "project", "should", "look", "as", "follows:"}), paragraph_18);
		// Image of The NatSpec Example Project after deleting the NatSpec test files at html/images/new_project_4.png width 334 px
		de.devboost.natspec.library.documentation.Image image_The_NatSpec_Example_Project_after_deleting_the_NatSpec_test_files_html_images_new_project_4_png_334_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "NatSpec", "Example", "Project", "after", "deleting", "the", "NatSpec", "test", "files"}), "html/images/new_project_4.png", "334", "px", subsection_Setup_of_the_Example_Project);
		// Subsection - Creating a NatSpec Scenario
		de.devboost.natspec.library.documentation.Subsection subsection_Creating_a_NatSpec_Scenario = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Creating", "a", "NatSpec", "Scenario"}), section_A_Running_Example);
		// Next, we want to create a new scenario. Thus, select the package
		de.devboost.natspec.library.documentation.Text text_Next__we_want_to_create_a_new_scenario__Thus__select_the_package = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Next,", "we", "want", "to", "create", "a", "new", "scenario.", "Thus,", "select", "the", "package"}), subsection_Creating_a_NatSpec_Scenario);
		// Code com.nat_spec.airline.example.testscenarios
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_testscenarios0 = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.testscenarios").toString(), subsection_Creating_a_NatSpec_Scenario);
		// in the project explorer and select the menu option
		de.devboost.natspec.library.documentation.Text text_in_the_project_explorer_and_select_the_menu_option = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "the", "project", "explorer", "and", "select", "the", "menu", "option"}), subsection_Creating_a_NatSpec_Scenario);
		// Code File > New > Other... > NatSpec Scenario File (.natspec):
		de.devboost.natspec.library.documentation.Code code_File___New___Other______NatSpec_Scenario_File___natspec__ = documentationSupport.code(new java.lang.StringBuilder().append("File").append(" ").append(">").append(" ").append("New").append(" ").append(">").append(" ").append("Other...").append(" ").append(">").append(" ").append("NatSpec").append(" ").append("Scenario").append(" ").append("File").append(" ").append("(.natspec):").toString(), subsection_Creating_a_NatSpec_Scenario);
		// Image of The dialog to create a new NatSpec Scenario (part 1) at html/images/new_scenario_1.png width 525 px
		de.devboost.natspec.library.documentation.Image image_The_dialog_to_create_a_new_NatSpec_Scenario__part_1__html_images_new_scenario_1_png_525_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "dialog", "to", "create", "a", "new", "NatSpec", "Scenario", "(part", "1)"}), "html/images/new_scenario_1.png", "525", "px", subsection_Creating_a_NatSpec_Scenario);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_19 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Scenario);
		// Enter the name of the Scenario (e.g.,
		de.devboost.natspec.library.documentation.Text text_Enter_the_name_of_the_Scenario__e_g__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Enter", "the", "name", "of", "the", "Scenario", "(e.g.,"}), paragraph_19);
		// Code BookSeatForPassenger.natspec
		de.devboost.natspec.library.documentation.Code code_BookSeatForPassenger_natspec = documentationSupport.code(new java.lang.StringBuilder().append("BookSeatForPassenger.natspec").toString(), paragraph_19);
		// ), and click on the
		de.devboost.natspec.library.documentation.Text text____and_click_on_the0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"),", "and", "click", "on", "the"}), paragraph_19);
		// Code Finish
		de.devboost.natspec.library.documentation.Code code_Finish0 = documentationSupport.code(new java.lang.StringBuilder().append("Finish").toString(), paragraph_19);
		// button:
		de.devboost.natspec.library.documentation.Text text_button_3 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button:"}), paragraph_19);
		// Image of The dialog to create a new NatSpec Scenario (part 2) at html/images/new_scenario_2.png width 637 px
		de.devboost.natspec.library.documentation.Image image_The_dialog_to_create_a_new_NatSpec_Scenario__part_2__html_images_new_scenario_2_png_637_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "dialog", "to", "create", "a", "new", "NatSpec", "Scenario", "(part", "2)"}), "html/images/new_scenario_2.png", "637", "px", subsection_Creating_a_NatSpec_Scenario);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_20 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Scenario);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_21 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Scenario);
		// Of course, you can also simply create a new file with the
		de.devboost.natspec.library.documentation.Text text_Of_course__you_can_also_simply_create_a_new_file_with_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Of", "course,", "you", "can", "also", "simply", "create", "a", "new", "file", "with", "the"}), paragraph_21);
		// Code .natspec
		de.devboost.natspec.library.documentation.Code code__natspec = documentationSupport.code(new java.lang.StringBuilder().append(".natspec").toString(), paragraph_21);
		// file extension to create a new NatSpec scenario instead of using the wizard.
		de.devboost.natspec.library.documentation.Text text_file_extension_to_create_a_new_NatSpec_scenario_instead_of_using_the_wizard_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"file", "extension", "to", "create", "a", "new", "NatSpec", "scenario", "instead", "of", "using", "the", "wizard."}), paragraph_21);
		// Afterwards, the project should contain the newly created scenario file:
		de.devboost.natspec.library.documentation.Text text_Afterwards__the_project_should_contain_the_newly_created_scenario_file_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Afterwards,", "the", "project", "should", "contain", "the", "newly", "created", "scenario", "file:"}), paragraph_21);
		// Image of The created NatSpec Scenario at html/images/new_scenario_3.png width 334 px
		de.devboost.natspec.library.documentation.Image image_The_created_NatSpec_Scenario_html_images_new_scenario_3_png_334_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "created", "NatSpec", "Scenario"}), "html/images/new_scenario_3.png", "334", "px", subsection_Creating_a_NatSpec_Scenario);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_22 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Scenario);
		// The package
		de.devboost.natspec.library.documentation.Text text_The_package0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "package"}), paragraph_22);
		// Code com.nat_spec.airline.example.testscenarios
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_testscenarios1 = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.testscenarios").toString(), paragraph_22);
		// contains the scenario file, as well as a corresponding Java class that contains the translated
		de.devboost.natspec.library.documentation.Text text_contains_the_scenario_file__as_well_as_a_corresponding_Java_class_that_contains_the_translated = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"contains", "the", "scenario", "file,", "as", "well", "as", "a", "corresponding", "Java", "class", "that", "contains", "the", "translated"}), paragraph_22);
		// JUnit test code for our NatSpec scenario. Currently, this class contains some compiling errors.
		de.devboost.natspec.library.documentation.Text text_JUnit_test_code_for_our_NatSpec_scenario__Currently__this_class_contains_some_compiling_errors_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"JUnit", "test", "code", "for", "our", "NatSpec", "scenario.", "Currently,", "this", "class", "contains", "some", "compiling", "errors."}), paragraph_22);
		// But don't worry, we will learn how to fix them in the following.
		de.devboost.natspec.library.documentation.Text text_But_don_t_worry__we_will_learn_how_to_fix_them_in_the_following_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"But", "don't", "worry,", "we", "will", "learn", "how", "to", "fix", "them", "in", "the", "following."}), paragraph_22);
		// Subsection - Creating a NatSpec Template
		de.devboost.natspec.library.documentation.Subsection subsection_Creating_a_NatSpec_Template = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Creating", "a", "NatSpec", "Template"}), section_A_Running_Example);
		// If the open the generated Java class, we see that it does not contain any Java code at all:
		de.devboost.natspec.library.documentation.Text text_If_the_open_the_generated_Java_class__we_see_that_it_does_not_contain_any_Java_code_at_all_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "the", "open", "the", "generated", "Java", "class,", "we", "see", "that", "it", "does", "not", "contain", "any", "Java", "code", "at", "all:"}), subsection_Creating_a_NatSpec_Template);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_0 = documentationSupport.beginListing(subsection_Creating_a_NatSpec_Template);
		// ERROR: No test case template found. Please provide
		de.devboost.natspec.library.documentation.Text text_ERROR__No_test_case_template_found__Please_provide = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"ERROR:", "No", "test", "case", "template", "found.", "Please", "provide"}), listing_0);
		// _NatSpecTemplate.java
		de.devboost.natspec.library.documentation.Text text__NatSpecTemplate_java = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"_NatSpecTemplate.java"}), listing_0);
		// in same folder or one of its parents.
		de.devboost.natspec.library.documentation.Text text_in_same_folder_or_one_of_its_parents_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "same", "folder", "or", "one", "of", "its", "parents."}), listing_0);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_23 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Template);
		// To let NatSpec know how our test scenarios shall be translated into executable Java code, we
		de.devboost.natspec.library.documentation.Text text_To_let_NatSpec_know_how_our_test_scenarios_shall_be_translated_into_executable_Java_code__we = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "let", "NatSpec", "know", "how", "our", "test", "scenarios", "shall", "be", "translated", "into", "executable", "Java", "code,", "we"}), paragraph_23);
		// first have to provide a NatSpec template that defines an enclosing frame for any generated
		de.devboost.natspec.library.documentation.Text text_first_have_to_provide_a_NatSpec_template_that_defines_an_enclosing_frame_for_any_generated = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"first", "have", "to", "provide", "a", "NatSpec", "template", "that", "defines", "an", "enclosing", "frame", "for", "any", "generated"}), paragraph_23);
		// test class. Thus, we create such a template now:
		de.devboost.natspec.library.documentation.Text text_test_class__Thus__we_create_such_a_template_now_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"test", "class.", "Thus,", "we", "create", "such", "a", "template", "now:"}), paragraph_23);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_24 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Template);
		// Select the package
		de.devboost.natspec.library.documentation.Text text_Select_the_package = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Select", "the", "package"}), paragraph_24);
		// Code com.nat_spec.airline.example.testscenarios
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_testscenarios2 = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.testscenarios").toString(), paragraph_24);
		// in the project explorer and select the menu option
		de.devboost.natspec.library.documentation.Text text_in_the_project_explorer_and_select_the_menu_option0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "the", "project", "explorer", "and", "select", "the", "menu", "option"}), paragraph_24);
		// Code File > New > Other... > NatSpec Template File (_NatSpecTemplate.java)
		de.devboost.natspec.library.documentation.Code code_File___New___Other______NatSpec_Template_File___NatSpecTemplate_java_ = documentationSupport.code(new java.lang.StringBuilder().append("File").append(" ").append(">").append(" ").append("New").append(" ").append(">").append(" ").append("Other...").append(" ").append(">").append(" ").append("NatSpec").append(" ").append("Template").append(" ").append("File").append(" ").append("(_NatSpecTemplate.java)").toString(), paragraph_24);
		// :
		de.devboost.natspec.library.documentation.Text text__2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {":"}), paragraph_24);
		// Image of The Wizard to create a new NatSpec Template at html/images/new_template_1.png width 525 px
		de.devboost.natspec.library.documentation.Image image_The_Wizard_to_create_a_new_NatSpec_Template_html_images_new_template_1_png_525_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "Wizard", "to", "create", "a", "new", "NatSpec", "Template"}), "html/images/new_template_1.png", "525", "px", subsection_Creating_a_NatSpec_Template);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_25 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Template);
		// Click on the
		de.devboost.natspec.library.documentation.Text text_Click_on_the0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Click", "on", "the"}), paragraph_25);
		// Code Next
		de.devboost.natspec.library.documentation.Code code_Next3 = documentationSupport.code(new java.lang.StringBuilder().append("Next").toString(), paragraph_25);
		// button and following, on the
		de.devboost.natspec.library.documentation.Text text_button_and_following__on_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button", "and", "following,", "on", "the"}), paragraph_25);
		// Code Finish
		de.devboost.natspec.library.documentation.Code code_Finish1 = documentationSupport.code(new java.lang.StringBuilder().append("Finish").toString(), paragraph_25);
		// button. The package
		de.devboost.natspec.library.documentation.Text text_button__The_package = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"button.", "The", "package"}), paragraph_25);
		// Code com.nat_spec.airline.example.testscenarios
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_testscenarios3 = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.testscenarios").toString(), paragraph_25);
		// should now contain a NatSpec template:
		de.devboost.natspec.library.documentation.Text text_should_now_contain_a_NatSpec_template_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"should", "now", "contain", "a", "NatSpec", "template:"}), paragraph_25);
		// Image of The project after creating a NatSpec Template at html/images/new_template_2.png width 334 px
		de.devboost.natspec.library.documentation.Image image_The_project_after_creating_a_NatSpec_Template_html_images_new_template_2_png_334_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "project", "after", "creating", "a", "NatSpec", "Template"}), "html/images/new_template_2.png", "334", "px", subsection_Creating_a_NatSpec_Template);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_26 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Template);
		// If you open the NatSpec Template class, you can see, that it looks like a JUnit test class but
		de.devboost.natspec.library.documentation.Text text_If_you_open_the_NatSpec_Template_class__you_can_see__that_it_looks_like_a_JUnit_test_class_but = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "you", "open", "the", "NatSpec", "Template", "class,", "you", "can", "see,", "that", "it", "looks", "like", "a", "JUnit", "test", "class", "but"}), paragraph_26);
		// contains several comments containing
		de.devboost.natspec.library.documentation.Text text_contains_several_comments_containing = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"contains", "several", "comments", "containing"}), paragraph_26);
		// Code TODO
		de.devboost.natspec.library.documentation.Code code_TODO = documentationSupport.code(new java.lang.StringBuilder().append("TODO").toString(), paragraph_26);
		// s:
		de.devboost.natspec.library.documentation.Text text_s_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"s:"}), paragraph_26);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_1 = documentationSupport.beginListing(subsection_Creating_a_NatSpec_Template);
		// package com.nat_spec.airline.example.test.scenarios;
		de.devboost.natspec.library.documentation.Text text_package_com_nat_spec_airline_example_test_scenarios_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"package", "com.nat_spec.airline.example.test.scenarios;"}), listing_1);
		// import org.junit.After;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_After_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.After;"}), listing_1);
		// import org.junit.Before;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_Before_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.Before;"}), listing_1);
		// import org.junit.Test;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_Test_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.Test;"}), listing_1);
		// ## TODO Import TestSupport classes.
		de.devboost.natspec.library.documentation.Text text____TODO_Import_TestSupport_classes_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "Import", "TestSupport", "classes."}), listing_1);
		// ## TODO Import classes to test and other test dependencies.
		de.devboost.natspec.library.documentation.Text text____TODO_Import_classes_to_test_and_other_test_dependencies_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "Import", "classes", "to", "test", "and", "other", "test", "dependencies."}), listing_1);
		// public class _NatSpecTemplate {
		de.devboost.natspec.library.documentation.Text text_public_class__NatSpecTemplate__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "class", "_NatSpecTemplate", "{"}), listing_1);
		// ## TODO Declare protected fields for test support classes
		de.devboost.natspec.library.documentation.Text text____TODO_Declare_protected_fields_for_test_support_classes = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "Declare", "protected", "fields", "for", "test", "support", "classes"}), listing_1);
		// ## TODO Declare fields for test context.
		de.devboost.natspec.library.documentation.Text text____TODO_Declare_fields_for_test_context_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "Declare", "fields", "for", "test", "context."}), listing_1);
		// @Test
		de.devboost.natspec.library.documentation.Text text__Test = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Test"}), listing_1);
		// public void executeScript() throws Exception {
		de.devboost.natspec.library.documentation.Text text_public_void_executeScript___throws_Exception__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "executeScript()", "throws", "Exception", "{"}), listing_1);
		// ## generated code will be inserted here
		de.devboost.natspec.library.documentation.Text text____generated_code_will_be_inserted_here = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "generated", "code", "will", "be", "inserted", "here"}), listing_1);
		// /* @MethodBody */
		de.devboost.natspec.library.documentation.Text text_____MethodBody___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "@MethodBody", "*/"}), listing_1);
		// }
		de.devboost.natspec.library.documentation.Text text__3 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_1);
		// @Before
		de.devboost.natspec.library.documentation.Text text__Before = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Before"}), listing_1);
		// public void setUp() {
		de.devboost.natspec.library.documentation.Text text_public_void_setUp____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "setUp()", "{"}), listing_1);
		// ## TODO Initialize fields for test support classes.
		de.devboost.natspec.library.documentation.Text text____TODO_Initialize_fields_for_test_support_classes_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "Initialize", "fields", "for", "test", "support", "classes."}), listing_1);
		// ## TODO Initialize fields for test context.
		de.devboost.natspec.library.documentation.Text text____TODO_Initialize_fields_for_test_context_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "Initialize", "fields", "for", "test", "context."}), listing_1);
		// }
		de.devboost.natspec.library.documentation.Text text__4 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_1);
		// @After
		de.devboost.natspec.library.documentation.Text text__After = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@After"}), listing_1);
		// public void shutdown() {
		de.devboost.natspec.library.documentation.Text text_public_void_shutdown____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "shutdown()", "{"}), listing_1);
		// ## TODO Shutdown text context.
		de.devboost.natspec.library.documentation.Text text____TODO_Shutdown_text_context_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "Shutdown", "text", "context."}), listing_1);
		// }
		de.devboost.natspec.library.documentation.Text text__5 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_1);
		// }
		de.devboost.natspec.library.documentation.Text text__6 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_1);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_27 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Template);
		// As the template class is used as a template for all test cases being generated, all fields and
		de.devboost.natspec.library.documentation.Text text_As_the_template_class_is_used_as_a_template_for_all_test_cases_being_generated__all_fields_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "the", "template", "class", "is", "used", "as", "a", "template", "for", "all", "test", "cases", "being", "generated,", "all", "fields", "and"}), paragraph_27);
		// methods being declared within the NatSpec Template will appear in all generated test cases.
		de.devboost.natspec.library.documentation.Text text_methods_being_declared_within_the_NatSpec_Template_will_appear_in_all_generated_test_cases_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"methods", "being", "declared", "within", "the", "NatSpec", "Template", "will", "appear", "in", "all", "generated", "test", "cases."}), paragraph_27);
		// Thus, you can declare a field delegating to the application under test, making it available
		de.devboost.natspec.library.documentation.Text text_Thus__you_can_declare_a_field_delegating_to_the_application_under_test__making_it_available = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Thus,", "you", "can", "declare", "a", "field", "delegating", "to", "the", "application", "under", "test,", "making", "it", "available"}), paragraph_27);
		// for all test cases based on this NatSpec template.
		de.devboost.natspec.library.documentation.Text text_for_all_test_cases_based_on_this_NatSpec_template_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"for", "all", "test", "cases", "based", "on", "this", "NatSpec", "template."}), paragraph_27);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_28 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Template);
		// The most important part of the NatSpec template is the
		de.devboost.natspec.library.documentation.Text text_The_most_important_part_of_the_NatSpec_template_is_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "most", "important", "part", "of", "the", "NatSpec", "template", "is", "the"}), paragraph_28);
		// Code executeScript()
		de.devboost.natspec.library.documentation.Code code_executeScript__ = documentationSupport.code(new java.lang.StringBuilder().append("executeScript()").toString(), paragraph_28);
		// method, that provides the envelope for the test code being generated for each NatSpec test
		de.devboost.natspec.library.documentation.Text text_method__that_provides_the_envelope_for_the_test_code_being_generated_for_each_NatSpec_test = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"method,", "that", "provides", "the", "envelope", "for", "the", "test", "code", "being", "generated", "for", "each", "NatSpec", "test"}), paragraph_28);
		// scenario. If you open the generated
		de.devboost.natspec.library.documentation.Text text_scenario__If_you_open_the_generated = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"scenario.", "If", "you", "open", "the", "generated"}), paragraph_28);
		// Code BookSeatForPassenger.java
		de.devboost.natspec.library.documentation.Code code_BookSeatForPassenger_java = documentationSupport.code(new java.lang.StringBuilder().append("BookSeatForPassenger.java").toString(), paragraph_28);
		// class for our test scenario, you can see that it looks exactly the same as the NatSpec
		de.devboost.natspec.library.documentation.Text text_class_for_our_test_scenario__you_can_see_that_it_looks_exactly_the_same_as_the_NatSpec = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class", "for", "our", "test", "scenario,", "you", "can", "see", "that", "it", "looks", "exactly", "the", "same", "as", "the", "NatSpec"}), paragraph_28);
		// template. The only difference is the
		de.devboost.natspec.library.documentation.Text text_template__The_only_difference_is_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"template.", "The", "only", "difference", "is", "the"}), paragraph_28);
		// Code executeScript()
		de.devboost.natspec.library.documentation.Code code_executeScript__0 = documentationSupport.code(new java.lang.StringBuilder().append("executeScript()").toString(), paragraph_28);
		// method:
		de.devboost.natspec.library.documentation.Text text_method_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"method:"}), paragraph_28);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_2 = documentationSupport.beginListing(subsection_Creating_a_NatSpec_Template);
		// @Test
		de.devboost.natspec.library.documentation.Text text__Test0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Test"}), listing_2);
		// public void executeScript() throws Exception {
		de.devboost.natspec.library.documentation.Text text_public_void_executeScript___throws_Exception__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "executeScript()", "throws", "Exception", "{"}), listing_2);
		// throw new java.lang.RuntimeException("NatSpec specification was empty. No code generated.");
		de.devboost.natspec.library.documentation.Text text_throw_new_java_lang_RuntimeException__NatSpec_specification_was_empty__No_code_generated____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"throw", "new", "java.lang.RuntimeException(\"NatSpec", "specification", "was", "empty.", "No", "code", "generated.\");"}), listing_2);
		// }
		de.devboost.natspec.library.documentation.Text text__7 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_2);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_29 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Creating_a_NatSpec_Template);
		// As the test scenario was empty, only a
		de.devboost.natspec.library.documentation.Text text_As_the_test_scenario_was_empty__only_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "the", "test", "scenario", "was", "empty,", "only", "a"}), paragraph_29);
		// Code RuntimeException
		de.devboost.natspec.library.documentation.Code code_RuntimeException = documentationSupport.code(new java.lang.StringBuilder().append("RuntimeException").toString(), paragraph_29);
		// has been generated, indicating that the test scenario used for code generation has been empty.
		de.devboost.natspec.library.documentation.Text text_has_been_generated__indicating_that_the_test_scenario_used_for_code_generation_has_been_empty_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"has", "been", "generated,", "indicating", "that", "the", "test", "scenario", "used", "for", "code", "generation", "has", "been", "empty."}), paragraph_29);
		// Subsection - Implementing a Test Scenario
		de.devboost.natspec.library.documentation.Subsection subsection_Implementing_a_Test_Scenario = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Implementing", "a", "Test", "Scenario"}), section_A_Running_Example);
		// So now its time to implement our test scenario. Thus, we enter the native language scenario
		de.devboost.natspec.library.documentation.Text text_So_now_its_time_to_implement_our_test_scenario__Thus__we_enter_the_native_language_scenario = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"So", "now", "its", "time", "to", "implement", "our", "test", "scenario.", "Thus,", "we", "enter", "the", "native", "language", "scenario"}), subsection_Implementing_a_Test_Scenario);
		// description into the scenario file:. Open the file with the NatSpec scenario editor (a simple
		de.devboost.natspec.library.documentation.Text text_description_into_the_scenario_file___Open_the_file_with_the_NatSpec_scenario_editor__a_simple = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"description", "into", "the", "scenario", "file:.", "Open", "the", "file", "with", "the", "NatSpec", "scenario", "editor", "(a", "simple"}), subsection_Implementing_a_Test_Scenario);
		// double click should work) and enter the scenario description. Afterwards, the editor should
		de.devboost.natspec.library.documentation.Text text_double_click_should_work__and_enter_the_scenario_description__Afterwards__the_editor_should = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"double", "click", "should", "work)", "and", "enter", "the", "scenario", "description.", "Afterwards,", "the", "editor", "should"}), subsection_Implementing_a_Test_Scenario);
		// look as follows:
		de.devboost.natspec.library.documentation.Text text_look_as_follows_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"look", "as", "follows:"}), subsection_Implementing_a_Test_Scenario);
		// Image of The NatSpec Scenario in the NatSpec editor at html/images/new_scenario_4.png width 472 px
		de.devboost.natspec.library.documentation.Image image_The_NatSpec_Scenario_in_the_NatSpec_editor_html_images_new_scenario_4_png_472_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "NatSpec", "Scenario", "in", "the", "NatSpec", "editor"}), "html/images/new_scenario_4.png", "472", "px", subsection_Implementing_a_Test_Scenario);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_30 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Implementing_a_Test_Scenario);
		// The entire text is marked as erroneous, as no mappings from the natural specification to
		de.devboost.natspec.library.documentation.Text text_The_entire_text_is_marked_as_erroneous__as_no_mappings_from_the_natural_specification_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "entire", "text", "is", "marked", "as", "erroneous,", "as", "no", "mappings", "from", "the", "natural", "specification", "to"}), paragraph_30);
		// executable Java code have been defined yet. Thus, let's uncomment most of the content and
		de.devboost.natspec.library.documentation.Text text_executable_Java_code_have_been_defined_yet__Thus__let_s_uncomment_most_of_the_content_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"executable", "Java", "code", "have", "been", "defined", "yet.", "Thus,", "let's", "uncomment", "most", "of", "the", "content", "and"}), paragraph_30);
		// provide a respective mapping for the remaining content to get an executable test and a green
		de.devboost.natspec.library.documentation.Text text_provide_a_respective_mapping_for_the_remaining_content_to_get_an_executable_test_and_a_green = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"provide", "a", "respective", "mapping", "for", "the", "remaining", "content", "to", "get", "an", "executable", "test", "and", "a", "green"}), paragraph_30);
		// JUnit bar.
		de.devboost.natspec.library.documentation.Text text_JUnit_bar_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"JUnit", "bar."}), paragraph_30);
		// Image of The uncommented NatSpec Scenario to focus on one issue at a time at html/images/new_scenario_5.png width 472 px
		de.devboost.natspec.library.documentation.Image image_The_uncommented_NatSpec_Scenario_to_focus_on_one_issue_at_a_time_html_images_new_scenario_5_png_472_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "uncommented", "NatSpec", "Scenario", "to", "focus", "on", "one", "issue", "at", "a", "time"}), "html/images/new_scenario_5.png", "472", "px", subsection_Implementing_a_Test_Scenario);
		// Subsection - Defining a Test Support Method
		de.devboost.natspec.library.documentation.Subsection subsection_Defining_a_Test_Support_Method = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Defining", "a", "Test", "Support", "Method"}), section_A_Running_Example);
		// To allow the execution of NatSpec scenarios defined in natural language, for each sentence
		de.devboost.natspec.library.documentation.Text text_To_allow_the_execution_of_NatSpec_scenarios_defined_in_natural_language__for_each_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "allow", "the", "execution", "of", "NatSpec", "scenarios", "defined", "in", "natural", "language,", "for", "each", "sentence"}), subsection_Defining_a_Test_Support_Method);
		// (or each line) of the NatSpec scenario, a test support method must exist, that maps the
		de.devboost.natspec.library.documentation.Text text__or_each_line__of_the_NatSpec_scenario__a_test_support_method_must_exist__that_maps_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"(or", "each", "line)", "of", "the", "NatSpec", "scenario,", "a", "test", "support", "method", "must", "exist,", "that", "maps", "the"}), subsection_Defining_a_Test_Support_Method);
		// respective sentence to executable Java code. Test support methods are declared by using
		de.devboost.natspec.library.documentation.Text text_respective_sentence_to_executable_Java_code__Test_support_methods_are_declared_by_using = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"respective", "sentence", "to", "executable", "Java", "code.", "Test", "support", "methods", "are", "declared", "by", "using"}), subsection_Defining_a_Test_Support_Method);
		// the annotation
		de.devboost.natspec.library.documentation.Text text_the_annotation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "annotation"}), subsection_Defining_a_Test_Support_Method);
		// Code @TextSyntax
		de.devboost.natspec.library.documentation.Code code__TextSyntax = documentationSupport.code(new java.lang.StringBuilder().append("@TextSyntax").toString(), subsection_Defining_a_Test_Support_Method);
		// (see the NatSpec documentation for further details) and are located within test support
		de.devboost.natspec.library.documentation.Text text__see_the_NatSpec_documentation_for_further_details__and_are_located_within_test_support = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"(see", "the", "NatSpec", "documentation", "for", "further", "details)", "and", "are", "located", "within", "test", "support"}), subsection_Defining_a_Test_Support_Method);
		// classes.
		de.devboost.natspec.library.documentation.Text text_classes_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"classes."}), subsection_Defining_a_Test_Support_Method);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_31 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method);
		// To declare a test support method, we first have to create an empty test support class.
		de.devboost.natspec.library.documentation.Text text_To_declare_a_test_support_method__we_first_have_to_create_an_empty_test_support_class_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "declare", "a", "test", "support", "method,", "we", "first", "have", "to", "create", "an", "empty", "test", "support", "class."}), paragraph_31);
		// Select the package
		de.devboost.natspec.library.documentation.Text text_Select_the_package0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Select", "the", "package"}), paragraph_31);
		// Code com.nat_spec.airline.example.testsupport
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_testsupport0 = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.testsupport").toString(), paragraph_31);
		// and create a new empty Java class called
		de.devboost.natspec.library.documentation.Text text_and_create_a_new_empty_Java_class_called = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "create", "a", "new", "empty", "Java", "class", "called"}), paragraph_31);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_31);
		// (
		de.devboost.natspec.library.documentation.Text text__8 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"("}), paragraph_31);
		// Code File > New > Class
		de.devboost.natspec.library.documentation.Code code_File___New___Class = documentationSupport.code(new java.lang.StringBuilder().append("File").append(" ").append(">").append(" ").append("New").append(" ").append(">").append(" ").append("Class").toString(), paragraph_31);
		// ).
		de.devboost.natspec.library.documentation.Text text___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {")."}), paragraph_31);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_32 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method);
		// To let NatSpec know of our new test support class, we have to add a respective link to our
		de.devboost.natspec.library.documentation.Text text_To_let_NatSpec_know_of_our_new_test_support_class__we_have_to_add_a_respective_link_to_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "let", "NatSpec", "know", "of", "our", "new", "test", "support", "class,", "we", "have", "to", "add", "a", "respective", "link", "to", "our"}), paragraph_32);
		// NatSpec template. Thus, open the
		de.devboost.natspec.library.documentation.Text text_NatSpec_template__Thus__open_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "template.", "Thus,", "open", "the"}), paragraph_32);
		// Code _NatSpecTemplate.java
		de.devboost.natspec.library.documentation.Code code__NatSpecTemplate_java = documentationSupport.code(new java.lang.StringBuilder().append("_NatSpecTemplate.java").toString(), paragraph_32);
		// and add the following field:
		de.devboost.natspec.library.documentation.Text text_and_add_the_following_field_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "add", "the", "following", "field:"}), paragraph_32);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_3 = documentationSupport.beginListing(subsection_Defining_a_Test_Support_Method);
		// public class _NatSpecTemplate {
		de.devboost.natspec.library.documentation.Text text_public_class__NatSpecTemplate__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "class", "_NatSpecTemplate", "{"}), listing_3);
		// protected TestSupport testSupport;
		de.devboost.natspec.library.documentation.Text text_protected_TestSupport_testSupport_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"protected", "TestSupport", "testSupport;"}), listing_3);
		// /* ... */
		de.devboost.natspec.library.documentation.Text text__________ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "...", "*/"}), listing_3);
		// @Before
		de.devboost.natspec.library.documentation.Text text__Before0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Before"}), listing_3);
		// public void setUp() {
		de.devboost.natspec.library.documentation.Text text_public_void_setUp____0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "setUp()", "{"}), listing_3);
		// testSupport = new TestSupport();
		de.devboost.natspec.library.documentation.Text text_testSupport___new_TestSupport___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"testSupport", "=", "new", "TestSupport();"}), listing_3);
		// /* ... */
		de.devboost.natspec.library.documentation.Text text__________0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "...", "*/"}), listing_3);
		// }
		de.devboost.natspec.library.documentation.Text text__9 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_3);
		// /* ... */
		de.devboost.natspec.library.documentation.Text text__________1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "...", "*/"}), listing_3);
		// }
		de.devboost.natspec.library.documentation.Text text__10 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_3);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_33 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method);
		// Make sure that you also initialize the
		de.devboost.natspec.library.documentation.Text text_Make_sure_that_you_also_initialize_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Make", "sure", "that", "you", "also", "initialize", "the"}), paragraph_33);
		// Code testSupport
		de.devboost.natspec.library.documentation.Code code_testSupport = documentationSupport.code(new java.lang.StringBuilder().append("testSupport").toString(), paragraph_33);
		// field within the
		de.devboost.natspec.library.documentation.Text text_field_within_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"field", "within", "the"}), paragraph_33);
		// Code setUp()
		de.devboost.natspec.library.documentation.Code code_setUp__ = documentationSupport.code(new java.lang.StringBuilder().append("setUp()").toString(), paragraph_33);
		// method. You can add multiple test support classes to the same NatSpec template.
		de.devboost.natspec.library.documentation.Text text_method__You_can_add_multiple_test_support_classes_to_the_same_NatSpec_template_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"method.", "You", "can", "add", "multiple", "test", "support", "classes", "to", "the", "same", "NatSpec", "template."}), paragraph_33);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_34 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method);
		// Now, let's define a new test support method for our first NatSpec sentence
		de.devboost.natspec.library.documentation.Text text_Now__let_s_define_a_new_test_support_method_for_our_first_NatSpec_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Now,", "let's", "define", "a", "new", "test", "support", "method", "for", "our", "first", "NatSpec", "sentence"}), paragraph_34);
		// Code Assume success
		de.devboost.natspec.library.documentation.Code code_Assume_success = documentationSupport.code(new java.lang.StringBuilder().append("Assume").append(" ").append("success").toString(), paragraph_34);
		// . Therefore, open the
		de.devboost.natspec.library.documentation.Text text___Therefore__open_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "Therefore,", "open", "the"}), paragraph_34);
		// Code TestSupport.java
		de.devboost.natspec.library.documentation.Code code_TestSupport_java = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport.java").toString(), paragraph_34);
		// file with the Java editor and add the following code to the respective class:
		de.devboost.natspec.library.documentation.Text text_file_with_the_Java_editor_and_add_the_following_code_to_the_respective_class_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"file", "with", "the", "Java", "editor", "and", "add", "the", "following", "code", "to", "the", "respective", "class:"}), paragraph_34);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_4 = documentationSupport.beginListing(subsection_Defining_a_Test_Support_Method);
		// boolean errorsOccured = false;
		de.devboost.natspec.library.documentation.Text text_boolean_errorsOccured___false_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"boolean", "errorsOccured", "=", "false;"}), listing_4);
		// @TextSyntax("Assume success")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Assume_success__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Assume", "success\")"}), listing_4);
		// public void assumeSuccess() {
		de.devboost.natspec.library.documentation.Text text_public_void_assumeSuccess____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "assumeSuccess()", "{"}), listing_4);
		// org.junit.Assert.assertFalse(errorsOccured);
		de.devboost.natspec.library.documentation.Text text_org_junit_Assert_assertFalse_errorsOccured__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"org.junit.Assert.assertFalse(errorsOccured);"}), listing_4);
		// }
		de.devboost.natspec.library.documentation.Text text__11 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_4);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_35 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method);
		// After saving the file, the error marker from our NatSpec scenario is gone:
		de.devboost.natspec.library.documentation.Text text_After_saving_the_file__the_error_marker_from_our_NatSpec_scenario_is_gone_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"After", "saving", "the", "file,", "the", "error", "marker", "from", "our", "NatSpec", "scenario", "is", "gone:"}), paragraph_35);
		// Image of The error marker is gone at html/images/new_scenario_6.png width 472 px
		de.devboost.natspec.library.documentation.Image image_The_error_marker_is_gone_html_images_new_scenario_6_png_472_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "error", "marker", "is", "gone"}), "html/images/new_scenario_6.png", "472", "px", subsection_Defining_a_Test_Support_Method);
		// Why is that? As the test support class now provides a mapping that can be matched to the
		de.devboost.natspec.library.documentation.Text text_Why_is_that__As_the_test_support_class_now_provides_a_mapping_that_can_be_matched_to_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Why", "is", "that?", "As", "the", "test", "support", "class", "now", "provides", "a", "mapping", "that", "can", "be", "matched", "to", "the"}), paragraph_35);
		// sentence in the NatSpec scenario, NatSpec knows which code shall be used to execute the
		de.devboost.natspec.library.documentation.Text text_sentence_in_the_NatSpec_scenario__NatSpec_knows_which_code_shall_be_used_to_execute_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sentence", "in", "the", "NatSpec", "scenario,", "NatSpec", "knows", "which", "code", "shall", "be", "used", "to", "execute", "the"}), paragraph_35);
		// respective test case. If you open the the generated
		de.devboost.natspec.library.documentation.Text text_respective_test_case__If_you_open_the_the_generated = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"respective", "test", "case.", "If", "you", "open", "the", "the", "generated"}), paragraph_35);
		// Code BookSeatForPassenger.java
		de.devboost.natspec.library.documentation.Code code_BookSeatForPassenger_java0 = documentationSupport.code(new java.lang.StringBuilder().append("BookSeatForPassenger.java").toString(), paragraph_35);
		// file, you can also see, that its
		de.devboost.natspec.library.documentation.Text text_file__you_can_also_see__that_its = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"file,", "you", "can", "also", "see,", "that", "its"}), paragraph_35);
		// Code executeScript()
		de.devboost.natspec.library.documentation.Code code_executeScript__1 = documentationSupport.code(new java.lang.StringBuilder().append("executeScript()").toString(), paragraph_35);
		// method now delegates to the respective test support method:
		de.devboost.natspec.library.documentation.Text text_method_now_delegates_to_the_respective_test_support_method_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"method", "now", "delegates", "to", "the", "respective", "test", "support", "method:"}), paragraph_35);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_5 = documentationSupport.beginListing(subsection_Defining_a_Test_Support_Method);
		// @Test
		de.devboost.natspec.library.documentation.Text text__Test1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Test"}), listing_5);
		// public void executeScript() throws Exception {
		de.devboost.natspec.library.documentation.Text text_public_void_executeScript___throws_Exception__1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "executeScript()", "throws", "Exception", "{"}), listing_5);
		// testSupport.assumeSuccess();
		de.devboost.natspec.library.documentation.Text text_testSupport_assumeSuccess___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"testSupport.assumeSuccess();"}), listing_5);
		// }
		de.devboost.natspec.library.documentation.Text text__12 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_5);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_36 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method);
		// Besides, if you execute the file as a JUnit test (
		de.devboost.natspec.library.documentation.Text text_Besides__if_you_execute_the_file_as_a_JUnit_test__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Besides,", "if", "you", "execute", "the", "file", "as", "a", "JUnit", "test", "("}), paragraph_36);
		// Code Run As > JUnit Test
		de.devboost.natspec.library.documentation.Code code_Run_As___JUnit_Test = documentationSupport.code(new java.lang.StringBuilder().append("Run").append(" ").append("As").append(" ").append(">").append(" ").append("JUnit").append(" ").append("Test").toString(), paragraph_36);
		// ), you will get a green JUnit bar, as the test case runs without any errors.
		de.devboost.natspec.library.documentation.Text text____you_will_get_a_green_JUnit_bar__as_the_test_case_runs_without_any_errors_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"),", "you", "will", "get", "a", "green", "JUnit", "bar,", "as", "the", "test", "case", "runs", "without", "any", "errors."}), paragraph_36);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_37 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method);
		// A few words to the test support method
		de.devboost.natspec.library.documentation.Text text_A_few_words_to_the_test_support_method = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"A", "few", "words", "to", "the", "test", "support", "method"}), paragraph_37);
		// Code assumeSuccess()
		de.devboost.natspec.library.documentation.Code code_assumeSuccess__ = documentationSupport.code(new java.lang.StringBuilder().append("assumeSuccess()").toString(), paragraph_37);
		// : The current implementation simply uses a local field within the test support class that
		de.devboost.natspec.library.documentation.Text text___The_current_implementation_simply_uses_a_local_field_within_the_test_support_class_that = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {":", "The", "current", "implementation", "simply", "uses", "a", "local", "field", "within", "the", "test", "support", "class", "that"}), paragraph_37);
		// indicates whether the execution of another sentence in the NatSpec scenario executed before
		de.devboost.natspec.library.documentation.Text text_indicates_whether_the_execution_of_another_sentence_in_the_NatSpec_scenario_executed_before = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"indicates", "whether", "the", "execution", "of", "another", "sentence", "in", "the", "NatSpec", "scenario", "executed", "before"}), paragraph_37);
		// lead to an execution error and, thus, setting the flag to
		de.devboost.natspec.library.documentation.Text text_lead_to_an_execution_error_and__thus__setting_the_flag_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"lead", "to", "an", "execution", "error", "and,", "thus,", "setting", "the", "flag", "to"}), paragraph_37);
		// Code true
		de.devboost.natspec.library.documentation.Code code_true = documentationSupport.code(new java.lang.StringBuilder().append("true").toString(), paragraph_37);
		// . As currently, our test case does not contain any other code than the assertion for no
		de.devboost.natspec.library.documentation.Text text___As_currently__our_test_case_does_not_contain_any_other_code_than_the_assertion_for_no = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "As", "currently,", "our", "test", "case", "does", "not", "contain", "any", "other", "code", "than", "the", "assertion", "for", "no"}), paragraph_37);
		// errors, its execution will always succeed. Thus, let's continue with more advanced sentences
		de.devboost.natspec.library.documentation.Text text_errors__its_execution_will_always_succeed__Thus__let_s_continue_with_more_advanced_sentences = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"errors,", "its", "execution", "will", "always", "succeed.", "Thus,", "let's", "continue", "with", "more", "advanced", "sentences"}), paragraph_37);
		// in our test scenario.
		de.devboost.natspec.library.documentation.Text text_in_our_test_scenario_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "our", "test", "scenario."}), paragraph_37);
		// Subsection - Connect to a Persistence Layer
		de.devboost.natspec.library.documentation.Subsection subsection_Connect_to_a_Persistence_Layer = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Connect", "to", "a", "Persistence", "Layer"}), section_A_Running_Example);
		// To realize a more sophisticated test scenarios, we need a connection to the application under
		de.devboost.natspec.library.documentation.Text text_To_realize_a_more_sophisticated_test_scenarios__we_need_a_connection_to_the_application_under = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "realize", "a", "more", "sophisticated", "test", "scenarios,", "we", "need", "a", "connection", "to", "the", "application", "under"}), subsection_Connect_to_a_Persistence_Layer);
		// tests or first, at least to its persistence layer that allows us to create and maintain
		de.devboost.natspec.library.documentation.Text text_tests_or_first__at_least_to_its_persistence_layer_that_allows_us_to_create_and_maintain = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"tests", "or", "first,", "at", "least", "to", "its", "persistence", "layer", "that", "allows", "us", "to", "create", "and", "maintain"}), subsection_Connect_to_a_Persistence_Layer);
		// entity objects.
		de.devboost.natspec.library.documentation.Text text_entity_objects_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"entity", "objects."}), subsection_Connect_to_a_Persistence_Layer);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_38 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Persistence_Layer);
		// Depending on the architecture of your application under test you may require
		de.devboost.natspec.library.documentation.Text text_Depending_on_the_architecture_of_your_application_under_test_you_may_require = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Depending", "on", "the", "architecture", "of", "your", "application", "under", "test", "you", "may", "require"}), paragraph_38);
		// connections to both, the persistence and the service layer, or only a connection to your
		de.devboost.natspec.library.documentation.Text text_connections_to_both__the_persistence_and_the_service_layer__or_only_a_connection_to_your = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"connections", "to", "both,", "the", "persistence", "and", "the", "service", "layer,", "or", "only", "a", "connection", "to", "your"}), paragraph_38);
		// service layer (if it encapsulates functionality to create and maintain entity objects). In
		de.devboost.natspec.library.documentation.Text text_service_layer__if_it_encapsulates_functionality_to_create_and_maintain_entity_objects___In = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"service", "layer", "(if", "it", "encapsulates", "functionality", "to", "create", "and", "maintain", "entity", "objects).", "In"}), paragraph_38);
		// this example case, the persistence layer is not encapsulated by the service layer and, thus,
		de.devboost.natspec.library.documentation.Text text_this_example_case__the_persistence_layer_is_not_encapsulated_by_the_service_layer_and__thus_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"this", "example", "case,", "the", "persistence", "layer", "is", "not", "encapsulated", "by", "the", "service", "layer", "and,", "thus,"}), paragraph_38);
		// we connect to the persistence layer directly.
		de.devboost.natspec.library.documentation.Text text_we_connect_to_the_persistence_layer_directly_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"we", "connect", "to", "the", "persistence", "layer", "directly."}), paragraph_38);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_39 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Persistence_Layer);
		// For the persistence layer we reuse the existing entity classes from the package
		de.devboost.natspec.library.documentation.Text text_For_the_persistence_layer_we_reuse_the_existing_entity_classes_from_the_package = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"For", "the", "persistence", "layer", "we", "reuse", "the", "existing", "entity", "classes", "from", "the", "package"}), paragraph_39);
		// Code com.nat_spec.airline.example.persistence.entity
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_persistence_entity = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.persistence.entity").toString(), paragraph_39);
		// (
		de.devboost.natspec.library.documentation.Text text__13 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"("}), paragraph_39);
		// Code AirplineType
		de.devboost.natspec.library.documentation.Code code_AirplineType = documentationSupport.code(new java.lang.StringBuilder().append("AirplineType").toString(), paragraph_39);
		// ,
		de.devboost.natspec.library.documentation.Text text__14 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {","}), paragraph_39);
		// Code Flight
		de.devboost.natspec.library.documentation.Code code_Flight = documentationSupport.code(new java.lang.StringBuilder().append("Flight").toString(), paragraph_39);
		// and
		de.devboost.natspec.library.documentation.Text text_and2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and"}), paragraph_39);
		// Code Passenger
		de.devboost.natspec.library.documentation.Code code_Passenger = documentationSupport.code(new java.lang.StringBuilder().append("Passenger").toString(), paragraph_39);
		// )
		de.devboost.natspec.library.documentation.Text text__15 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {")"}), paragraph_39);
		// and the class
		de.devboost.natspec.library.documentation.Text text_and_the_class = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "the", "class"}), paragraph_39);
		// Code InMemoryPersistenceContext
		de.devboost.natspec.library.documentation.Code code_InMemoryPersistenceContext = documentationSupport.code(new java.lang.StringBuilder().append("InMemoryPersistenceContext").toString(), paragraph_39);
		// from the package
		de.devboost.natspec.library.documentation.Text text_from_the_package = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"from", "the", "package"}), paragraph_39);
		// Code com.nat_spec.airline.example.persistence
		de.devboost.natspec.library.documentation.Code code_com_nat_spec_airline_example_persistence0 = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spec.airline.example.persistence").toString(), paragraph_39);
		// that represents a simple DAO object, storing the created entities in memory instead
		de.devboost.natspec.library.documentation.Text text_that_represents_a_simple_DAO_object__storing_the_created_entities_in_memory_instead = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"that", "represents", "a", "simple", "DAO", "object,", "storing", "the", "created", "entities", "in", "memory", "instead"}), paragraph_39);
		// of a real database (as used for real enterprise applications).
		de.devboost.natspec.library.documentation.Text text_of_a_real_database__as_used_for_real_enterprise_applications__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "a", "real", "database", "(as", "used", "for", "real", "enterprise", "applications)."}), paragraph_39);
		// Image of The Persistence Layer packages in the Project Explorer at html/images/persistence_project.png width 334 px
		de.devboost.natspec.library.documentation.Image image_The_Persistence_Layer_packages_in_the_Project_Explorer_html_images_persistence_project_png_334_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "Persistence", "Layer", "packages", "in", "the", "Project", "Explorer"}), "html/images/persistence_project.png", "334", "px", subsection_Connect_to_a_Persistence_Layer);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_40 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Persistence_Layer);
		// To access the persistence context from our test support methods, we have to create a
		de.devboost.natspec.library.documentation.Text text_To_access_the_persistence_context_from_our_test_support_methods__we_have_to_create_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "access", "the", "persistence", "context", "from", "our", "test", "support", "methods,", "we", "have", "to", "create", "a"}), paragraph_40);
		// respective reference from our
		de.devboost.natspec.library.documentation.Text text_respective_reference_from_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"respective", "reference", "from", "our"}), paragraph_40);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport0 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_40);
		// class. Thus, open the
		de.devboost.natspec.library.documentation.Text text_class__Thus__open_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class.", "Thus,", "open", "the"}), paragraph_40);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport1 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_40);
		// class, add a respective field and modify its constructor as shown in the following:
		de.devboost.natspec.library.documentation.Text text_class__add_a_respective_field_and_modify_its_constructor_as_shown_in_the_following_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class,", "add", "a", "respective", "field", "and", "modify", "its", "constructor", "as", "shown", "in", "the", "following:"}), paragraph_40);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_6 = documentationSupport.beginListing(subsection_Connect_to_a_Persistence_Layer);
		// /* ... */
		de.devboost.natspec.library.documentation.Text text__________2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "...", "*/"}), listing_6);
		// InMemoryPersistenceContext persistenceContext;
		de.devboost.natspec.library.documentation.Text text_InMemoryPersistenceContext_persistenceContext_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"InMemoryPersistenceContext", "persistenceContext;"}), listing_6);
		// /** Public constructor for test support class */
		de.devboost.natspec.library.documentation.Text text_____Public_constructor_for_test_support_class___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/**", "Public", "constructor", "for", "test", "support", "class", "*/"}), listing_6);
		// public TestSupport(InMemoryPersistenceContext persistenceContext) {
		de.devboost.natspec.library.documentation.Text text_public_TestSupport_InMemoryPersistenceContext_persistenceContext___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "TestSupport(InMemoryPersistenceContext", "persistenceContext)", "{"}), listing_6);
		// super();
		de.devboost.natspec.library.documentation.Text text_super___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"super();"}), listing_6);
		// this.persistenceContext = persistenceContext;
		de.devboost.natspec.library.documentation.Text text_this_persistenceContext___persistenceContext_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"this.persistenceContext", "=", "persistenceContext;"}), listing_6);
		// }
		de.devboost.natspec.library.documentation.Text text__16 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_6);
		// /* ... */
		de.devboost.natspec.library.documentation.Text text__________3 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "...", "*/"}), listing_6);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_41 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Persistence_Layer);
		// We're basically using a constructor parameter to pass the reference to the
		de.devboost.natspec.library.documentation.Text text_We_re_basically_using_a_constructor_parameter_to_pass_the_reference_to_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"We're", "basically", "using", "a", "constructor", "parameter", "to", "pass", "the", "reference", "to", "the"}), paragraph_41);
		// persistence context when creating a new instance of the
		de.devboost.natspec.library.documentation.Text text_persistence_context_when_creating_a_new_instance_of_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"persistence", "context", "when", "creating", "a", "new", "instance", "of", "the"}), paragraph_41);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport2 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_41);
		// class. Therefore, the
		de.devboost.natspec.library.documentation.Text text_class__Therefore__the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class.", "Therefore,", "the"}), paragraph_41);
		// Code _NatSpecTemplate
		de.devboost.natspec.library.documentation.Code code__NatSpecTemplate = documentationSupport.code(new java.lang.StringBuilder().append("_NatSpecTemplate").toString(), paragraph_41);
		// of our test project must pass a persistence context object when it creates the
		de.devboost.natspec.library.documentation.Text text_of_our_test_project_must_pass_a_persistence_context_object_when_it_creates_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "our", "test", "project", "must", "pass", "a", "persistence", "context", "object", "when", "it", "creates", "the"}), paragraph_41);
		// respective
		de.devboost.natspec.library.documentation.Text text_respective = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"respective"}), paragraph_41);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport3 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_41);
		// class for every test run.
		de.devboost.natspec.library.documentation.Text text_class_for_every_test_run_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class", "for", "every", "test", "run."}), paragraph_41);
		// Subsection - Defining a Test Support Method having Parameters
		de.devboost.natspec.library.documentation.Subsection subsection_Defining_a_Test_Support_Method_having_Parameters = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Defining", "a", "Test", "Support", "Method", "having", "Parameters"}), section_A_Running_Example);
		// Now, we can proceed with the implementation of test support methods and the implementation
		de.devboost.natspec.library.documentation.Text text_Now__we_can_proceed_with_the_implementation_of_test_support_methods_and_the_implementation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Now,", "we", "can", "proceed", "with", "the", "implementation", "of", "test", "support", "methods", "and", "the", "implementation"}), subsection_Defining_a_Test_Support_Method_having_Parameters);
		// of test scenario specified in natural language. First, we want to support the execution
		de.devboost.natspec.library.documentation.Text text_of_test_scenario_specified_in_natural_language__First__we_want_to_support_the_execution = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "test", "scenario", "specified", "in", "natural", "language.", "First,", "we", "want", "to", "support", "the", "execution"}), subsection_Defining_a_Test_Support_Method_having_Parameters);
		// of its first sentence
		de.devboost.natspec.library.documentation.Text text_of_its_first_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "its", "first", "sentence"}), subsection_Defining_a_Test_Support_Method_having_Parameters);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_7 = documentationSupport.beginListing(subsection_Defining_a_Test_Support_Method_having_Parameters);
		// Given a Passenger John Doe
		de.devboost.natspec.library.documentation.Text text_Given_a_Passenger_John_Doe0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Given", "a", "Passenger", "John", "Doe"}), listing_7);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_42 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method_having_Parameters);
		// Therefore, we require a test support method accepting this sentence. Although this could be
		de.devboost.natspec.library.documentation.Text text_Therefore__we_require_a_test_support_method_accepting_this_sentence__Although_this_could_be = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Therefore,", "we", "require", "a", "test", "support", "method", "accepting", "this", "sentence.", "Although", "this", "could", "be"}), paragraph_42);
		// easily done by using the annotation
		de.devboost.natspec.library.documentation.Text text_easily_done_by_using_the_annotation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"easily", "done", "by", "using", "the", "annotation"}), paragraph_42);
		// Code @TextSyntax("Given a Passenger John Doe")
		de.devboost.natspec.library.documentation.Code code__TextSyntax__Given_a_Passenger_John_Doe__ = documentationSupport.code(new java.lang.StringBuilder().append("@TextSyntax(\"Given").append(" ").append("a").append(" ").append("Passenger").append(" ").append("John").append(" ").append("Doe\")").toString(), paragraph_42);
		// , is it more sensible to create a respective parameterizable test support method that
		de.devboost.natspec.library.documentation.Text text___is_it_more_sensible_to_create_a_respective_parameterizable_test_support_method_that = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "is", "it", "more", "sensible", "to", "create", "a", "respective", "parameterizable", "test", "support", "method", "that"}), paragraph_42);
		// supports the creation of every passenger we like and not the creation of John Doe only. Thus,
		de.devboost.natspec.library.documentation.Text text_supports_the_creation_of_every_passenger_we_like_and_not_the_creation_of_John_Doe_only__Thus_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"supports", "the", "creation", "of", "every", "passenger", "we", "like", "and", "not", "the", "creation", "of", "John", "Doe", "only.", "Thus,"}), paragraph_42);
		// we add the following test support method to our
		de.devboost.natspec.library.documentation.Text text_we_add_the_following_test_support_method_to_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"we", "add", "the", "following", "test", "support", "method", "to", "our"}), paragraph_42);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport4 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_42);
		// class:
		de.devboost.natspec.library.documentation.Text text_class_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class:"}), paragraph_42);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_8 = documentationSupport.beginListing(subsection_Defining_a_Test_Support_Method_having_Parameters);
		// @TextSyntax("Given a Passenger #1 #2")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Given_a_Passenger__1__2__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Given", "a", "Passenger", "#1", "#2\")"}), listing_8);
		// public Passenger createPassenger(String firstName, String lastName) {
		de.devboost.natspec.library.documentation.Text text_public_Passenger_createPassenger_String_firstName__String_lastName___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "Passenger", "createPassenger(String", "firstName,", "String", "lastName)", "{"}), listing_8);
		// return persistenceContext.createPassenger(firstName, lastName);
		de.devboost.natspec.library.documentation.Text text_return_persistenceContext_createPassenger_firstName__lastName__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "persistenceContext.createPassenger(firstName,", "lastName);"}), listing_8);
		// }
		de.devboost.natspec.library.documentation.Text text__17 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_8);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_43 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method_having_Parameters);
		// The method takes a NatSpec sentence starting with the words
		de.devboost.natspec.library.documentation.Text text_The_method_takes_a_NatSpec_sentence_starting_with_the_words = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "method", "takes", "a", "NatSpec", "sentence", "starting", "with", "the", "words"}), paragraph_43);
		// Code Given a Passenger
		de.devboost.natspec.library.documentation.Code code_Given_a_Passenger = documentationSupport.code(new java.lang.StringBuilder().append("Given").append(" ").append("a").append(" ").append("Passenger").toString(), paragraph_43);
		// followed by two further
		de.devboost.natspec.library.documentation.Text text_followed_by_two_further = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"followed", "by", "two", "further"}), paragraph_43);
		// Code String
		de.devboost.natspec.library.documentation.Code code_String = documentationSupport.code(new java.lang.StringBuilder().append("String").toString(), paragraph_43);
		// s and matches these
		de.devboost.natspec.library.documentation.Text text_s_and_matches_these = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"s", "and", "matches", "these"}), paragraph_43);
		// Code String
		de.devboost.natspec.library.documentation.Code code_String0 = documentationSupport.code(new java.lang.StringBuilder().append("String").toString(), paragraph_43);
		// s to the first and last name of a passenger to be created. Instead of
		de.devboost.natspec.library.documentation.Text text_s_to_the_first_and_last_name_of_a_passenger_to_be_created__Instead_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"s", "to", "the", "first", "and", "last", "name", "of", "a", "passenger", "to", "be", "created.", "Instead", "of"}), paragraph_43);
		// Code String
		de.devboost.natspec.library.documentation.Code code_String1 = documentationSupport.code(new java.lang.StringBuilder().append("String").toString(), paragraph_43);
		// s, NatSpec parameters can also have other
		de.devboost.natspec.library.documentation.Text text_s__NatSpec_parameters_can_also_have_other = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"s,", "NatSpec", "parameters", "can", "also", "have", "other"}), paragraph_43);
		// primitive types such as
		de.devboost.natspec.library.documentation.Text text_primitive_types_such_as = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"primitive", "types", "such", "as"}), paragraph_43);
		// Code Integer
		de.devboost.natspec.library.documentation.Code code_Integer = documentationSupport.code(new java.lang.StringBuilder().append("Integer").toString(), paragraph_43);
		// , and
		de.devboost.natspec.library.documentation.Text text___and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "and"}), paragraph_43);
		// Code Double
		de.devboost.natspec.library.documentation.Code code_Double = documentationSupport.code(new java.lang.StringBuilder().append("Double").toString(), paragraph_43);
		// values. Besides,
		de.devboost.natspec.library.documentation.Text text_values__Besides_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"values.", "Besides,"}), paragraph_43);
		// Code java.util.Date
		de.devboost.natspec.library.documentation.Code code_java_util_Date = documentationSupport.code(new java.lang.StringBuilder().append("java.util.Date").toString(), paragraph_43);
		// s are supported. Inspect the NatSpec documentation for a list of all types supported
		de.devboost.natspec.library.documentation.Text text_s_are_supported__Inspect_the_NatSpec_documentation_for_a_list_of_all_types_supported = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"s", "are", "supported.", "Inspect", "the", "NatSpec", "documentation", "for", "a", "list", "of", "all", "types", "supported"}), paragraph_43);
		// within parameters of NatSpec templates.
		de.devboost.natspec.library.documentation.Text text_within_parameters_of_NatSpec_templates_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"within", "parameters", "of", "NatSpec", "templates."}), paragraph_43);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_44 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method_having_Parameters);
		// Now, we can create
		de.devboost.natspec.library.documentation.Text text_Now__we_can_create = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Now,", "we", "can", "create"}), paragraph_44);
		// Code Passenger
		de.devboost.natspec.library.documentation.Code code_Passenger0 = documentationSupport.code(new java.lang.StringBuilder().append("Passenger").toString(), paragraph_44);
		// entities by using NatSpec. Uncomment the first sentence of the NatSpec scenario:
		de.devboost.natspec.library.documentation.Text text_entities_by_using_NatSpec__Uncomment_the_first_sentence_of_the_NatSpec_scenario_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"entities", "by", "using", "NatSpec.", "Uncomment", "the", "first", "sentence", "of", "the", "NatSpec", "scenario:"}), paragraph_44);
		// Image of We can now create Passengers at html/images/create_passenger.png width 472 px
		de.devboost.natspec.library.documentation.Image image_We_can_now_create_Passengers_html_images_create_passenger_png_472_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"We", "can", "now", "create", "Passengers"}), "html/images/create_passenger.png", "472", "px", subsection_Defining_a_Test_Support_Method_having_Parameters);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_45 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Defining_a_Test_Support_Method_having_Parameters);
		// As you can see, the first sentence is now accepted by NatSpec and translated into executable
		de.devboost.natspec.library.documentation.Text text_As_you_can_see__the_first_sentence_is_now_accepted_by_NatSpec_and_translated_into_executable = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "you", "can", "see,", "the", "first", "sentence", "is", "now", "accepted", "by", "NatSpec", "and", "translated", "into", "executable"}), paragraph_45);
		// JUnit code. If you rerun the generated JUnit test, the JUnit bar should still remain green.
		de.devboost.natspec.library.documentation.Text text_JUnit_code__If_you_rerun_the_generated_JUnit_test__the_JUnit_bar_should_still_remain_green_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"JUnit", "code.", "If", "you", "rerun", "the", "generated", "JUnit", "test,", "the", "JUnit", "bar", "should", "still", "remain", "green."}), paragraph_45);
		// Subsection - Test Support Methods using Context Information
		de.devboost.natspec.library.documentation.Subsection subsection_Test_Support_Methods_using_Context_Information = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Test", "Support", "Methods", "using", "Context", "Information"}), section_A_Running_Example);
		// To allow the creation of Airplanes, and the acceptance of the following sentence in our
		de.devboost.natspec.library.documentation.Text text_To_allow_the_creation_of_Airplanes__and_the_acceptance_of_the_following_sentence_in_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "allow", "the", "creation", "of", "Airplanes,", "and", "the", "acceptance", "of", "the", "following", "sentence", "in", "our"}), subsection_Test_Support_Methods_using_Context_Information);
		// scenario
		de.devboost.natspec.library.documentation.Text text_scenario = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"scenario"}), subsection_Test_Support_Methods_using_Context_Information);
		// Code Given an Airplane Boeing-787
		de.devboost.natspec.library.documentation.Code code_Given_an_Airplane_Boeing_787 = documentationSupport.code(new java.lang.StringBuilder().append("Given").append(" ").append("an").append(" ").append("Airplane").append(" ").append("Boeing-787").toString(), subsection_Test_Support_Methods_using_Context_Information);
		// , we create a further test support method for the creation of Airplanes:
		de.devboost.natspec.library.documentation.Text text___we_create_a_further_test_support_method_for_the_creation_of_Airplanes_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "we", "create", "a", "further", "test", "support", "method", "for", "the", "creation", "of", "Airplanes:"}), subsection_Test_Support_Methods_using_Context_Information);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_9 = documentationSupport.beginListing(subsection_Test_Support_Methods_using_Context_Information);
		// @TextSyntax("Given an Airplane #1")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Given_an_Airplane__1__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Given", "an", "Airplane", "#1\")"}), listing_9);
		// public AirplaneType createAirplane(String name) {
		de.devboost.natspec.library.documentation.Text text_public_AirplaneType_createAirplane_String_name___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "AirplaneType", "createAirplane(String", "name)", "{"}), listing_9);
		// return persistenceContext.createAirplaneType(name);
		de.devboost.natspec.library.documentation.Text text_return_persistenceContext_createAirplaneType_name__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "persistenceContext.createAirplaneType(name);"}), listing_9);
		// }
		de.devboost.natspec.library.documentation.Text text__18 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_9);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_46 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Test_Support_Methods_using_Context_Information);
		// This works fine. However, the following sentence
		de.devboost.natspec.library.documentation.Text text_This_works_fine__However__the_following_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"This", "works", "fine.", "However,", "the", "following", "sentence"}), paragraph_46);
		// Code With 200 total seats
		de.devboost.natspec.library.documentation.Code code_With_200_total_seats = documentationSupport.code(new java.lang.StringBuilder().append("With").append(" ").append("200").append(" ").append("total").append(" ").append("seats").toString(), paragraph_46);
		// is more complicated: it uses implicit knowledge (or context information) that we created
		de.devboost.natspec.library.documentation.Text text_is_more_complicated__it_uses_implicit_knowledge__or_context_information__that_we_created = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"is", "more", "complicated:", "it", "uses", "implicit", "knowledge", "(or", "context", "information)", "that", "we", "created"}), paragraph_46);
		// an airplane beforehand. Not a problem for NatSpec! As the test support method to create
		de.devboost.natspec.library.documentation.Text text_an_airplane_beforehand__Not_a_problem_for_NatSpec__As_the_test_support_method_to_create = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"an", "airplane", "beforehand.", "Not", "a", "problem", "for", "NatSpec!", "As", "the", "test", "support", "method", "to", "create"}), paragraph_46);
		// a new airplane returns the respective
		de.devboost.natspec.library.documentation.Text text_a_new_airplane_returns_the_respective = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"a", "new", "airplane", "returns", "the", "respective"}), paragraph_46);
		// Code AirplaneType
		de.devboost.natspec.library.documentation.Code code_AirplaneType = documentationSupport.code(new java.lang.StringBuilder().append("AirplaneType").toString(), paragraph_46);
		// , NatSpec can reuse this
		de.devboost.natspec.library.documentation.Text text___NatSpec_can_reuse_this = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "NatSpec", "can", "reuse", "this"}), paragraph_46);
		// Code AirplaneType
		de.devboost.natspec.library.documentation.Code code_AirplaneType0 = documentationSupport.code(new java.lang.StringBuilder().append("AirplaneType").toString(), paragraph_46);
		// for all following scenario sentences requiring airplanes. The respective test method
		de.devboost.natspec.library.documentation.Text text_for_all_following_scenario_sentences_requiring_airplanes__The_respective_test_method = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"for", "all", "following", "scenario", "sentences", "requiring", "airplanes.", "The", "respective", "test", "method"}), paragraph_46);
		// only requires an additional parameter for the respective
		de.devboost.natspec.library.documentation.Text text_only_requires_an_additional_parameter_for_the_respective = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"only", "requires", "an", "additional", "parameter", "for", "the", "respective"}), paragraph_46);
		// Code AirplaneType
		de.devboost.natspec.library.documentation.Code code_AirplaneType1 = documentationSupport.code(new java.lang.StringBuilder().append("AirplaneType").toString(), paragraph_46);
		// :
		de.devboost.natspec.library.documentation.Text text__19 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {":"}), paragraph_46);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_10 = documentationSupport.beginListing(subsection_Test_Support_Methods_using_Context_Information);
		// @TextSyntax("With #1 total seats")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__With__1_total_seats__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"With", "#1", "total", "seats\")"}), listing_10);
		// public AirplaneType setTotalSeats(int seats, AirplaneType airplane) {
		de.devboost.natspec.library.documentation.Text text_public_AirplaneType_setTotalSeats_int_seats__AirplaneType_airplane___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "AirplaneType", "setTotalSeats(int", "seats,", "AirplaneType", "airplane)", "{"}), listing_10);
		// airplane.setTotalSeats(seats);
		de.devboost.natspec.library.documentation.Text text_airplane_setTotalSeats_seats__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"airplane.setTotalSeats(seats);"}), listing_10);
		// return airplane;
		de.devboost.natspec.library.documentation.Text text_return_airplane_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "airplane;"}), listing_10);
		// }
		de.devboost.natspec.library.documentation.Text text__20 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_10);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_47 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Test_Support_Methods_using_Context_Information);
		// This test support method takes an already created airplane and sets its total seats to
		de.devboost.natspec.library.documentation.Text text_This_test_support_method_takes_an_already_created_airplane_and_sets_its_total_seats_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"This", "test", "support", "method", "takes", "an", "already", "created", "airplane", "and", "sets", "its", "total", "seats", "to"}), paragraph_47);
		// the given value. If you inspect the generated JUnit code, you can see that NatSpec
		de.devboost.natspec.library.documentation.Text text_the_given_value__If_you_inspect_the_generated_JUnit_code__you_can_see_that_NatSpec = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "given", "value.", "If", "you", "inspect", "the", "generated", "JUnit", "code,", "you", "can", "see", "that", "NatSpec"}), paragraph_47);
		// automatically takes care of shipping the required context information from one test support
		de.devboost.natspec.library.documentation.Text text_automatically_takes_care_of_shipping_the_required_context_information_from_one_test_support = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"automatically", "takes", "care", "of", "shipping", "the", "required", "context", "information", "from", "one", "test", "support"}), paragraph_47);
		// method to another:
		de.devboost.natspec.library.documentation.Text text_method_to_another_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"method", "to", "another:"}), paragraph_47);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_11 = documentationSupport.beginListing(subsection_Test_Support_Methods_using_Context_Information);
		// @Test
		de.devboost.natspec.library.documentation.Text text__Test2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Test"}), listing_11);
		// public void executeScript() throws Exception {
		de.devboost.natspec.library.documentation.Text text_public_void_executeScript___throws_Exception__2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "executeScript()", "throws", "Exception", "{"}), listing_11);
		// de.devboost.natspec.examples.airline.entity.Passenger passenger_John_Doe = testSupport.createPassenger("John", "Doe");
		de.devboost.natspec.library.documentation.Text text_de_devboost_natspec_examples_airline_entity_Passenger_passenger_John_Doe___testSupport_createPassenger__John____Doe___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"de.devboost.natspec.examples.airline.entity.Passenger", "passenger_John_Doe", "=", "testSupport.createPassenger(\"John\",", "\"Doe\");"}), listing_11);
		// de.devboost.natspec.examples.airline.entity.AirplaneType airplaneType_Boeing_787 = testSupport.createAirplane("Boeing-787");
		de.devboost.natspec.library.documentation.Text text_de_devboost_natspec_examples_airline_entity_AirplaneType_airplaneType_Boeing_787___testSupport_createAirplane__Boeing_787___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"de.devboost.natspec.examples.airline.entity.AirplaneType", "airplaneType_Boeing_787", "=", "testSupport.createAirplane(\"Boeing-787\");"}), listing_11);
		// de.devboost.natspec.examples.airline.entity.AirplaneType airplaneType_200 = testSupport.setTotalSeats(200, airplaneType_Boeing_787);
		de.devboost.natspec.library.documentation.Text text_de_devboost_natspec_examples_airline_entity_AirplaneType_airplaneType_200___testSupport_setTotalSeats_200__airplaneType_Boeing_787__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"de.devboost.natspec.examples.airline.entity.AirplaneType", "airplaneType_200", "=", "testSupport.setTotalSeats(200,", "airplaneType_Boeing_787);"}), listing_11);
		// testSupport.assumeSuccess();
		de.devboost.natspec.library.documentation.Text text_testSupport_assumeSuccess___0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"testSupport.assumeSuccess();"}), listing_11);
		// }
		de.devboost.natspec.library.documentation.Text text__21 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_11);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_48 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Test_Support_Methods_using_Context_Information);
		// Besides, NatSpec takes also care that the required context information has been created at
		de.devboost.natspec.library.documentation.Text text_Besides__NatSpec_takes_also_care_that_the_required_context_information_has_been_created_at = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Besides,", "NatSpec", "takes", "also", "care", "that", "the", "required", "context", "information", "has", "been", "created", "at"}), paragraph_48);
		// all. If you uncomment the creation of the airplane and try to set its total seat without
		de.devboost.natspec.library.documentation.Text text_all__If_you_uncomment_the_creation_of_the_airplane_and_try_to_set_its_total_seat_without = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"all.", "If", "you", "uncomment", "the", "creation", "of", "the", "airplane", "and", "try", "to", "set", "its", "total", "seat", "without"}), paragraph_48);
		// creating the plane, NatSpec will not accept the parsing of your scenario:
		de.devboost.natspec.library.documentation.Text text_creating_the_plane__NatSpec_will_not_accept_the_parsing_of_your_scenario_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"creating", "the", "plane,", "NatSpec", "will", "not", "accept", "the", "parsing", "of", "your", "scenario:"}), paragraph_48);
		// Image of Seats cannot be set without creating an Airplane first at html/images/set_total_seats.png width 472 px
		de.devboost.natspec.library.documentation.Image image_Seats_cannot_be_set_without_creating_an_Airplane_first_html_images_set_total_seats_png_472_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Seats", "cannot", "be", "set", "without", "creating", "an", "Airplane", "first"}), "html/images/set_total_seats.png", "472", "px", subsection_Test_Support_Methods_using_Context_Information);
		// Subsection - Test Support Methods That Need Access To The Generated Test Class
		de.devboost.natspec.library.documentation.Subsection subsection_Test_Support_Methods_That_Need_Access_To_The_Generated_Test_Class = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Test", "Support", "Methods", "That", "Need", "Access", "To", "The", "Generated", "Test", "Class"}), section_A_Running_Example);
		// To allow test support methods to access the instance of the class that is generated for a concrete test, the @This
		de.devboost.natspec.library.documentation.Text text_To_allow_test_support_methods_to_access_the_instance_of_the_class_that_is_generated_for_a_concrete_test__the__This = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "allow", "test", "support", "methods", "to", "access", "the", "instance", "of", "the", "class", "that", "is", "generated", "for", "a", "concrete", "test,", "the", "@This"}), subsection_Test_Support_Methods_That_Need_Access_To_The_Generated_Test_Class);
		// annotation can be used.
		de.devboost.natspec.library.documentation.Text text_annotation_can_be_used_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"annotation", "can", "be", "used."}), subsection_Test_Support_Methods_That_Need_Access_To_The_Generated_Test_Class);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_12 = documentationSupport.beginListing(subsection_Test_Support_Methods_That_Need_Access_To_The_Generated_Test_Class);
		// @TextSyntax("Print test case class name")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Print_test_case_class_name__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Print", "test", "case", "class", "name\")"}), listing_12);
		// public void createAirplane(@This Object testInstance) {
		de.devboost.natspec.library.documentation.Text text_public_void_createAirplane__This_Object_testInstance___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "createAirplane(@This", "Object", "testInstance)", "{"}), listing_12);
		// System.out.println("Running test " + testInstance.getClass().getName());
		de.devboost.natspec.library.documentation.Text text_System_out_println__Running_test_____testInstance_getClass___getName____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(\"Running", "test", "\"", "+", "testInstance.getClass().getName());"}), listing_12);
		// }
		de.devboost.natspec.library.documentation.Text text__22 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_12);
		// Subsection - Test Support Methods with Complex Type Parameters
		de.devboost.natspec.library.documentation.Subsection subsection_Test_Support_Methods_with_Complex_Type_Parameters = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Test", "Support", "Methods", "with", "Complex", "Type", "Parameters"}), section_A_Running_Example);
		// Next, we provide a test support method to create a
		de.devboost.natspec.library.documentation.Text text_Next__we_provide_a_test_support_method_to_create_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Next,", "we", "provide", "a", "test", "support", "method", "to", "create", "a"}), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// Code Flight
		de.devboost.natspec.library.documentation.Code code_Flight0 = documentationSupport.code(new java.lang.StringBuilder().append("Flight").toString(), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// and one to set its airplane to an existing
		de.devboost.natspec.library.documentation.Text text_and_one_to_set_its_airplane_to_an_existing = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "one", "to", "set", "its", "airplane", "to", "an", "existing"}), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// Code AirplaneType
		de.devboost.natspec.library.documentation.Code code_AirplaneType2 = documentationSupport.code(new java.lang.StringBuilder().append("AirplaneType").toString(), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// . Whereas the first test support method is easy, the second one takes a parameter not being
		de.devboost.natspec.library.documentation.Text text___Whereas_the_first_test_support_method_is_easy__the_second_one_takes_a_parameter_not_being = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "Whereas", "the", "first", "test", "support", "method", "is", "easy,", "the", "second", "one", "takes", "a", "parameter", "not", "being"}), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// a primitive standard Java type, but a customized complex type instead, being the
		de.devboost.natspec.library.documentation.Text text_a_primitive_standard_Java_type__but_a_customized_complex_type_instead__being_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"a", "primitive", "standard", "Java", "type,", "but", "a", "customized", "complex", "type", "instead,", "being", "the"}), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// Code AirplaneType
		de.devboost.natspec.library.documentation.Code code_AirplaneType3 = documentationSupport.code(new java.lang.StringBuilder().append("AirplaneType").toString(), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// :
		de.devboost.natspec.library.documentation.Text text__23 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {":"}), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_13 = documentationSupport.beginListing(subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// @TextSyntax("Given a flight #1")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Given_a_flight__1__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Given", "a", "flight", "#1\")"}), listing_13);
		// public Flight createFlight(String name) {
		de.devboost.natspec.library.documentation.Text text_public_Flight_createFlight_String_name___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "Flight", "createFlight(String", "name)", "{"}), listing_13);
		// return persistenceContext.createFlight(name);
		de.devboost.natspec.library.documentation.Text text_return_persistenceContext_createFlight_name__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "persistenceContext.createFlight(name);"}), listing_13);
		// }
		de.devboost.natspec.library.documentation.Text text__24 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_13);
		// @TextSyntax("With #1 airplane")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__With__1_airplane__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"With", "#1", "airplane\")"}), listing_13);
		// public Flight setAirplane(AirplaneType airplane, Flight flight) {
		de.devboost.natspec.library.documentation.Text text_public_Flight_setAirplane_AirplaneType_airplane__Flight_flight___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "Flight", "setAirplane(AirplaneType", "airplane,", "Flight", "flight)", "{"}), listing_13);
		// flight.setAirplane(airplane);
		de.devboost.natspec.library.documentation.Text text_flight_setAirplane_airplane__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"flight.setAirplane(airplane);"}), listing_13);
		// return flight;
		de.devboost.natspec.library.documentation.Text text_return_flight_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "flight;"}), listing_13);
		// }
		de.devboost.natspec.library.documentation.Text text__25 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_13);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_49 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// NatSpec can handle such parameters as well. As you can see, the
		de.devboost.natspec.library.documentation.Text text_NatSpec_can_handle_such_parameters_as_well__As_you_can_see__the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "can", "handle", "such", "parameters", "as", "well.", "As", "you", "can", "see,", "the"}), paragraph_49);
		// Code AirplaneType
		de.devboost.natspec.library.documentation.Code code_AirplaneType4 = documentationSupport.code(new java.lang.StringBuilder().append("AirplaneType").toString(), paragraph_49);
		// used in the respective sentence is highlighted within the NatSpec scenario editor:
		de.devboost.natspec.library.documentation.Text text_used_in_the_respective_sentence_is_highlighted_within_the_NatSpec_scenario_editor_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"used", "in", "the", "respective", "sentence", "is", "highlighted", "within", "the", "NatSpec", "scenario", "editor:"}), paragraph_49);
		// Image of Usage of Complex Parameters in Sentences such as an AirplaneType at html/images/set_airplane_type.png width 472 px
		de.devboost.natspec.library.documentation.Image image_Usage_of_Complex_Parameters_in_Sentences_such_as_an_AirplaneType_html_images_set_airplane_type_png_472_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"Usage", "of", "Complex", "Parameters", "in", "Sentences", "such", "as", "an", "AirplaneType"}), "html/images/set_airplane_type.png", "472", "px", subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_50 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// Besides, NatSpec also investigates which of the
		de.devboost.natspec.library.documentation.Text text_Besides__NatSpec_also_investigates_which_of_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Besides,", "NatSpec", "also", "investigates", "which", "of", "the"}), paragraph_50);
		// Code AirplaneType
		de.devboost.natspec.library.documentation.Code code_AirplaneType5 = documentationSupport.code(new java.lang.StringBuilder().append("AirplaneType").toString(), paragraph_50);
		// s created beforehand is meant to be used in the context of this sentence. If you would create
		de.devboost.natspec.library.documentation.Text text_s_created_beforehand_is_meant_to_be_used_in_the_context_of_this_sentence__If_you_would_create = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"s", "created", "beforehand", "is", "meant", "to", "be", "used", "in", "the", "context", "of", "this", "sentence.", "If", "you", "would", "create"}), paragraph_50);
		// a second airplane using the sentence
		de.devboost.natspec.library.documentation.Text text_a_second_airplane_using_the_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"a", "second", "airplane", "using", "the", "sentence"}), paragraph_50);
		// Code Given an Airplane Boeing-797
		de.devboost.natspec.library.documentation.Code code_Given_an_Airplane_Boeing_797 = documentationSupport.code(new java.lang.StringBuilder().append("Given").append(" ").append("an").append(" ").append("Airplane").append(" ").append("Boeing-797").toString(), paragraph_50);
		// , NatSpec would comprehend to use the airplane
		de.devboost.natspec.library.documentation.Text text___NatSpec_would_comprehend_to_use_the_airplane = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "NatSpec", "would", "comprehend", "to", "use", "the", "airplane"}), paragraph_50);
		// Code Boeing-787
		de.devboost.natspec.library.documentation.Code code_Boeing_787 = documentationSupport.code(new java.lang.StringBuilder().append("Boeing-787").toString(), paragraph_50);
		// instead of
		de.devboost.natspec.library.documentation.Text text_instead_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"instead", "of"}), paragraph_50);
		// Code Boeing-797
		de.devboost.natspec.library.documentation.Code code_Boeing_797 = documentationSupport.code(new java.lang.StringBuilder().append("Boeing-797").toString(), paragraph_50);
		// for the creation of the flight
		de.devboost.natspec.library.documentation.Text text_for_the_creation_of_the_flight = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"for", "the", "creation", "of", "the", "flight"}), paragraph_50);
		// Code LH-1234
		de.devboost.natspec.library.documentation.Code code_LH_1234 = documentationSupport.code(new java.lang.StringBuilder().append("LH-1234").toString(), paragraph_50);
		// .
		de.devboost.natspec.library.documentation.Text text__26 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), paragraph_50);
		// For completeness reasons, to set the free seats of flight
		de.devboost.natspec.library.documentation.Text text_For_completeness_reasons__to_set_the_free_seats_of_flight = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"For", "completeness", "reasons,", "to", "set", "the", "free", "seats", "of", "flight"}), paragraph_50);
		// CODE LH-1234
		de.devboost.natspec.library.documentation.Code code_LH_12340 = documentationSupport.code(new java.lang.StringBuilder().append("LH-1234").toString(), paragraph_50);
		// , the following test support method can be provided:
		de.devboost.natspec.library.documentation.Text text___the_following_test_support_method_can_be_provided_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "the", "following", "test", "support", "method", "can", "be", "provided:"}), paragraph_50);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_14 = documentationSupport.beginListing(subsection_Test_Support_Methods_with_Complex_Type_Parameters);
		// @TextSyntax("With #1 free seats")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__With__1_free_seats__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"With", "#1", "free", "seats\")"}), listing_14);
		// public Flight setFreeSeats(int seats, Flight flight) {
		de.devboost.natspec.library.documentation.Text text_public_Flight_setFreeSeats_int_seats__Flight_flight___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "Flight", "setFreeSeats(int", "seats,", "Flight", "flight)", "{"}), listing_14);
		// flight.setFreeSeats(seats);
		de.devboost.natspec.library.documentation.Text text_flight_setFreeSeats_seats__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"flight.setFreeSeats(seats);"}), listing_14);
		// return flight;
		de.devboost.natspec.library.documentation.Text text_return_flight_0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "flight;"}), listing_14);
		// }
		de.devboost.natspec.library.documentation.Text text__27 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_14);
		// Subsection - Connect to a Service Layer
		de.devboost.natspec.library.documentation.Subsection subsection_Connect_to_a_Service_Layer = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Connect", "to", "a", "Service", "Layer"}), section_A_Running_Example);
		// So far, we have only tested our persistence layer and our data access object. Now, it is
		de.devboost.natspec.library.documentation.Text text_So_far__we_have_only_tested_our_persistence_layer_and_our_data_access_object__Now__it_is = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"So", "far,", "we", "have", "only", "tested", "our", "persistence", "layer", "and", "our", "data", "access", "object.", "Now,", "it", "is"}), subsection_Connect_to_a_Service_Layer);
		// time to test the service layer which is the heart of our enterprise application. Similar
		de.devboost.natspec.library.documentation.Text text_time_to_test_the_service_layer_which_is_the_heart_of_our_enterprise_application__Similar = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"time", "to", "test", "the", "service", "layer", "which", "is", "the", "heart", "of", "our", "enterprise", "application.", "Similar"}), subsection_Connect_to_a_Service_Layer);
		// to the data access object, we have to connect our service layer to our
		de.devboost.natspec.library.documentation.Text text_to_the_data_access_object__we_have_to_connect_our_service_layer_to_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"to", "the", "data", "access", "object,", "we", "have", "to", "connect", "our", "service", "layer", "to", "our"}), subsection_Connect_to_a_Service_Layer);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport5 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), subsection_Connect_to_a_Service_Layer);
		// class to allow its usage within test support methods.
		de.devboost.natspec.library.documentation.Text text_class_to_allow_its_usage_within_test_support_methods_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class", "to", "allow", "its", "usage", "within", "test", "support", "methods."}), subsection_Connect_to_a_Service_Layer);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_51 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// Now, we reuse the service implementation from the package
		de.devboost.natspec.library.documentation.Text text_Now__we_reuse_the_service_implementation_from_the_package = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Now,", "we", "reuse", "the", "service", "implementation", "from", "the", "package"}), paragraph_51);
		// Code com.nat_spect.airline.example.service
		de.devboost.natspec.library.documentation.Code code_com_nat_spect_airline_example_service = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spect.airline.example.service").toString(), paragraph_51);
		// :
		de.devboost.natspec.library.documentation.Text text__28 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {":"}), paragraph_51);
		// Image of The Service Layer in the Package Explorer at html/images/service_project.png width 334 px
		de.devboost.natspec.library.documentation.Image image_The_Service_Layer_in_the_Package_Explorer_html_images_service_project_png_334_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "Service", "Layer", "in", "the", "Package", "Explorer"}), "html/images/service_project.png", "334", "px", subsection_Connect_to_a_Service_Layer);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_52 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// The package
		de.devboost.natspec.library.documentation.Text text_The_package1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "package"}), paragraph_52);
		// Code com.nat_spect.airline.example.service
		de.devboost.natspec.library.documentation.Code code_com_nat_spect_airline_example_service0 = documentationSupport.code(new java.lang.StringBuilder().append("com.nat_spect.airline.example.service").toString(), paragraph_52);
		// contains the service class
		de.devboost.natspec.library.documentation.Text text_contains_the_service_class = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"contains", "the", "service", "class"}), paragraph_52);
		// Code AirlineService
		de.devboost.natspec.library.documentation.Code code_AirlineService = documentationSupport.code(new java.lang.StringBuilder().append("AirlineService").toString(), paragraph_52);
		// providing services for the booking of seats in flights and the class
		de.devboost.natspec.library.documentation.Text text_providing_services_for_the_booking_of_seats_in_flights_and_the_class = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"providing", "services", "for", "the", "booking", "of", "seats", "in", "flights", "and", "the", "class"}), paragraph_52);
		// Code OperationStatus
		de.devboost.natspec.library.documentation.Code code_OperationStatus = documentationSupport.code(new java.lang.StringBuilder().append("OperationStatus").toString(), paragraph_52);
		// that is used as return type of the service methods, indicating successful or failed service
		de.devboost.natspec.library.documentation.Text text_that_is_used_as_return_type_of_the_service_methods__indicating_successful_or_failed_service = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"that", "is", "used", "as", "return", "type", "of", "the", "service", "methods,", "indicating", "successful", "or", "failed", "service"}), paragraph_52);
		// invocations and possibly, respective error messages.
		de.devboost.natspec.library.documentation.Text text_invocations_and_possibly__respective_error_messages_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"invocations", "and", "possibly,", "respective", "error", "messages."}), paragraph_52);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_53 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// To integrate the service layer into our
		de.devboost.natspec.library.documentation.Text text_To_integrate_the_service_layer_into_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "integrate", "the", "service", "layer", "into", "our"}), paragraph_53);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport6 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_53);
		// class, let's again integrate the service's setup into our
		de.devboost.natspec.library.documentation.Text text_class__let_s_again_integrate_the_service_s_setup_into_our = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class,", "let's", "again", "integrate", "the", "service's", "setup", "into", "our"}), paragraph_53);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport7 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_53);
		// class:
		de.devboost.natspec.library.documentation.Text text_class_0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class:"}), paragraph_53);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_15 = documentationSupport.beginListing(subsection_Connect_to_a_Service_Layer);
		// /* ... */
		de.devboost.natspec.library.documentation.Text text__________4 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "...", "*/"}), listing_15);
		// AirlineService service;
		de.devboost.natspec.library.documentation.Text text_AirlineService_service_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"AirlineService", "service;"}), listing_15);
		// /**
		de.devboost.natspec.library.documentation.Text text____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/**"}), listing_15);
		// * Public constructor for test support class
		de.devboost.natspec.library.documentation.ListItem listItem_Public_constructor_for_test_support_class = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"Public", "constructor", "for", "test", "support", "class"}), list_);
		// */
		de.devboost.natspec.library.documentation.Text text___0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"*/"}), listing_15);
		// public TestSupport() {
		de.devboost.natspec.library.documentation.Text text_public_TestSupport____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "TestSupport()", "{"}), listing_15);
		// super();
		de.devboost.natspec.library.documentation.Text text_super___0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"super();"}), listing_15);
		// persistenceContext = InMemoryPersistenceContext.getPersistenceContext();
		de.devboost.natspec.library.documentation.Text text_persistenceContext___InMemoryPersistenceContext_getPersistenceContext___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"persistenceContext", "=", "InMemoryPersistenceContext.getPersistenceContext();"}), listing_15);
		// service = new AirlineService();
		de.devboost.natspec.library.documentation.Text text_service___new_AirlineService___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"service", "=", "new", "AirlineService();"}), listing_15);
		// }
		de.devboost.natspec.library.documentation.Text text__29 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_15);
		// /* ... */
		de.devboost.natspec.library.documentation.Text text__________5 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "...", "*/"}), listing_15);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_54 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// Again, you could also integrate the service setup into the
		de.devboost.natspec.library.documentation.Text text_Again__you_could_also_integrate_the_service_setup_into_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Again,", "you", "could", "also", "integrate", "the", "service", "setup", "into", "the"}), paragraph_54);
		// Code _NatSpecTemplate
		de.devboost.natspec.library.documentation.Code code__NatSpecTemplate0 = documentationSupport.code(new java.lang.StringBuilder().append("_NatSpecTemplate").toString(), paragraph_54);
		// instead of the
		de.devboost.natspec.library.documentation.Text text_instead_of_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"instead", "of", "the"}), paragraph_54);
		// Code TestSupport
		de.devboost.natspec.library.documentation.Code code_TestSupport8 = documentationSupport.code(new java.lang.StringBuilder().append("TestSupport").toString(), paragraph_54);
		// class.
		de.devboost.natspec.library.documentation.Text text_class_1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"class."}), paragraph_54);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_55 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// Now, we can implement the test support method for our final unsupported scenario sentence
		de.devboost.natspec.library.documentation.Text text_Now__we_can_implement_the_test_support_method_for_our_final_unsupported_scenario_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Now,", "we", "can", "implement", "the", "test", "support", "method", "for", "our", "final", "unsupported", "scenario", "sentence"}), paragraph_55);
		// to book a seat for a flight:
		de.devboost.natspec.library.documentation.Text text_to_book_a_seat_for_a_flight_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"to", "book", "a", "seat", "for", "a", "flight:"}), paragraph_55);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_16 = documentationSupport.beginListing(subsection_Connect_to_a_Service_Layer);
		// @TextSyntax("Book seat for #1 at #2")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Book_seat_for__1_at__2__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Book", "seat", "for", "#1", "at", "#2\")"}), listing_16);
		// public OperationStatus bookSeat(Passenger passenger, Flight flight) {
		de.devboost.natspec.library.documentation.Text text_public_OperationStatus_bookSeat_Passenger_passenger__Flight_flight___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "OperationStatus", "bookSeat(Passenger", "passenger,", "Flight", "flight)", "{"}), listing_16);
		// return service.bookSeat(passenger, flight);
		de.devboost.natspec.library.documentation.Text text_return_service_bookSeat_passenger__flight__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "service.bookSeat(passenger,", "flight);"}), listing_16);
		// }
		de.devboost.natspec.library.documentation.Text text__30 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_16);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_56 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// Besides, we can now modify the assertion check for no failures, as the
		de.devboost.natspec.library.documentation.Text text_Besides__we_can_now_modify_the_assertion_check_for_no_failures__as_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Besides,", "we", "can", "now", "modify", "the", "assertion", "check", "for", "no", "failures,", "as", "the"}), paragraph_56);
		// Code OperationStatus
		de.devboost.natspec.library.documentation.Code code_OperationStatus0 = documentationSupport.code(new java.lang.StringBuilder().append("OperationStatus").toString(), paragraph_56);
		// returned by the service method invocation can now be used for the assertion:
		de.devboost.natspec.library.documentation.Text text_returned_by_the_service_method_invocation_can_now_be_used_for_the_assertion_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"returned", "by", "the", "service", "method", "invocation", "can", "now", "be", "used", "for", "the", "assertion:"}), paragraph_56);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_17 = documentationSupport.beginListing(subsection_Connect_to_a_Service_Layer);
		// @TextSyntax("Assume success")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Assume_success__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Assume", "success\")"}), listing_17);
		// public void assumeSuccess(OperationStatus status) {
		de.devboost.natspec.library.documentation.Text text_public_void_assumeSuccess_OperationStatus_status___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "assumeSuccess(OperationStatus", "status)", "{"}), listing_17);
		// org.junit.Assert.assertTrue(status.isValid());
		de.devboost.natspec.library.documentation.Text text_org_junit_Assert_assertTrue_status_isValid____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"org.junit.Assert.assertTrue(status.isValid());"}), listing_17);
		// }
		de.devboost.natspec.library.documentation.Text text__31 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_17);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_57 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// That's all. Our complete scenario specified in natural language is now accepted by NatSpec
		de.devboost.natspec.library.documentation.Text text_That_s_all__Our_complete_scenario_specified_in_natural_language_is_now_accepted_by_NatSpec = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"That's", "all.", "Our", "complete", "scenario", "specified", "in", "natural", "language", "is", "now", "accepted", "by", "NatSpec"}), paragraph_57);
		// and generated into executable JUnit code:
		de.devboost.natspec.library.documentation.Text text_and_generated_into_executable_JUnit_code_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "generated", "into", "executable", "JUnit", "code:"}), paragraph_57);
		// Image of The complete Scenario as accepted by NatSpec at html/images/complete_scenario.png width 472 px
		de.devboost.natspec.library.documentation.Image image_The_complete_Scenario_as_accepted_by_NatSpec_html_images_complete_scenario_png_472_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "complete", "Scenario", "as", "accepted", "by", "NatSpec"}), "html/images/complete_scenario.png", "472", "px", subsection_Connect_to_a_Service_Layer);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_58 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Connect_to_a_Service_Layer);
		// If you rerun the JUnit test, you should still receive a green JUnit bar. Feel free to define
		de.devboost.natspec.library.documentation.Text text_If_you_rerun_the_JUnit_test__you_should_still_receive_a_green_JUnit_bar__Feel_free_to_define = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "you", "rerun", "the", "JUnit", "test,", "you", "should", "still", "receive", "a", "green", "JUnit", "bar.", "Feel", "free", "to", "define"}), paragraph_58);
		// further scenarios and play around with further test support methods to test the service layer.
		de.devboost.natspec.library.documentation.Text text_further_scenarios_and_play_around_with_further_test_support_methods_to_test_the_service_layer_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"further", "scenarios", "and", "play", "around", "with", "further", "test", "support", "methods", "to", "test", "the", "service", "layer."}), paragraph_58);
		// You can also inspect the NatSpec documentation to learn, what else is possible and can be
		de.devboost.natspec.library.documentation.Text text_You_can_also_inspect_the_NatSpec_documentation_to_learn__what_else_is_possible_and_can_be = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"You", "can", "also", "inspect", "the", "NatSpec", "documentation", "to", "learn,", "what", "else", "is", "possible", "and", "can", "be"}), paragraph_58);
		// done using NatSpec.
		de.devboost.natspec.library.documentation.Text text_done_using_NatSpec_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"done", "using", "NatSpec."}), paragraph_58);
		// Section - Some Further Hints
		de.devboost.natspec.library.documentation.Section section_Some_Further_Hints = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"Some", "Further", "Hints"}), documentation_Getting_Started_with_NatSpec);
		// For the daily use of NatSpec you might also know some further hints and tricks that ease the
		de.devboost.natspec.library.documentation.Text text_For_the_daily_use_of_NatSpec_you_might_also_know_some_further_hints_and_tricks_that_ease_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"For", "the", "daily", "use", "of", "NatSpec", "you", "might", "also", "know", "some", "further", "hints", "and", "tricks", "that", "ease", "the"}), section_Some_Further_Hints);
		// usage of NatSpec.
		de.devboost.natspec.library.documentation.Text text_usage_of_NatSpec_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"usage", "of", "NatSpec."}), section_Some_Further_Hints);
		// Subsection - Syntax Patterns View
		de.devboost.natspec.library.documentation.Subsection subsection_Syntax_Patterns_View = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Syntax", "Patterns", "View"}), section_Some_Further_Hints);
		// NatSpec contains a view giving an overview over all existing test support methods and the
		de.devboost.natspec.library.documentation.Text text_NatSpec_contains_a_view_giving_an_overview_over_all_existing_test_support_methods_and_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "contains", "a", "view", "giving", "an", "overview", "over", "all", "existing", "test", "support", "methods", "and", "the"}), subsection_Syntax_Patterns_View);
		// respective defined syntax patterns. You can open the view by using the menu option
		de.devboost.natspec.library.documentation.Text text_respective_defined_syntax_patterns__You_can_open_the_view_by_using_the_menu_option = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"respective", "defined", "syntax", "patterns.", "You", "can", "open", "the", "view", "by", "using", "the", "menu", "option"}), subsection_Syntax_Patterns_View);
		// Code Window > Show View > Other... > NatSpec > Syntax Patterns
		de.devboost.natspec.library.documentation.Code code_Window___Show_View___Other______NatSpec___Syntax_Patterns = documentationSupport.code(new java.lang.StringBuilder().append("Window").append(" ").append(">").append(" ").append("Show").append(" ").append("View").append(" ").append(">").append(" ").append("Other...").append(" ").append(">").append(" ").append("NatSpec").append(" ").append(">").append(" ").append("Syntax").append(" ").append("Patterns").toString(), subsection_Syntax_Patterns_View);
		// :
		de.devboost.natspec.library.documentation.Text text__32 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {":"}), subsection_Syntax_Patterns_View);
		// Image of The Syntax Patterns View at html/images/syntax_patterns_view.png width 752 px
		de.devboost.natspec.library.documentation.Image image_The_Syntax_Patterns_View_html_images_syntax_patterns_view_png_752_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "Syntax", "Patterns", "View"}), "html/images/syntax_patterns_view.png", "752", "px", subsection_Syntax_Patterns_View);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_59 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Syntax_Patterns_View);
		// The view enlists all existing patterns including their parameter types, as well as the
		de.devboost.natspec.library.documentation.Text text_The_view_enlists_all_existing_patterns_including_their_parameter_types__as_well_as_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "view", "enlists", "all", "existing", "patterns", "including", "their", "parameter", "types,", "as", "well", "as", "the"}), paragraph_59);
		// context in which they can be used. A double-click on one of the patterns opens
		de.devboost.natspec.library.documentation.Text text_context_in_which_they_can_be_used__A_double_click_on_one_of_the_patterns_opens = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"context", "in", "which", "they", "can", "be", "used.", "A", "double-click", "on", "one", "of", "the", "patterns", "opens"}), paragraph_59);
		// the respective method that is associated with the pattern.
		de.devboost.natspec.library.documentation.Text text_the_respective_method_that_is_associated_with_the_pattern_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "respective", "method", "that", "is", "associated", "with", "the", "pattern."}), paragraph_59);
		// Subsection - NatSpec Help
		de.devboost.natspec.library.documentation.Subsection subsection_NatSpec_Help = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"NatSpec", "Help"}), section_Some_Further_Hints);
		// During the usage of NatSpec it can be useful to have its documentation directly available
		de.devboost.natspec.library.documentation.Text text_During_the_usage_of_NatSpec_it_can_be_useful_to_have_its_documentation_directly_available = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"During", "the", "usage", "of", "NatSpec", "it", "can", "be", "useful", "to", "have", "its", "documentation", "directly", "available"}), subsection_NatSpec_Help);
		// within Eclipse. Thus, a NatSpec Help view exists that provides the NatSpec documentation at
		de.devboost.natspec.library.documentation.Text text_within_Eclipse__Thus__a_NatSpec_Help_view_exists_that_provides_the_NatSpec_documentation_at = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"within", "Eclipse.", "Thus,", "a", "NatSpec", "Help", "view", "exists", "that", "provides", "the", "NatSpec", "documentation", "at"}), subsection_NatSpec_Help);
		// any time you need it.
		de.devboost.natspec.library.documentation.Text text_any_time_you_need_it_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"any", "time", "you", "need", "it."}), subsection_NatSpec_Help);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_60 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_NatSpec_Help);
		// The NatSpec help can be opened by using the menu option
		de.devboost.natspec.library.documentation.Text text_The_NatSpec_help_can_be_opened_by_using_the_menu_option = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "NatSpec", "help", "can", "be", "opened", "by", "using", "the", "menu", "option"}), paragraph_60);
		// Code Window > Show View > Other... > NatSpec > NatSpec Help
		de.devboost.natspec.library.documentation.Code code_Window___Show_View___Other______NatSpec___NatSpec_Help = documentationSupport.code(new java.lang.StringBuilder().append("Window").append(" ").append(">").append(" ").append("Show").append(" ").append("View").append(" ").append(">").append(" ").append("Other...").append(" ").append(">").append(" ").append("NatSpec").append(" ").append(">").append(" ").append("NatSpec").append(" ").append("Help").toString(), paragraph_60);
		// Image of The NatSpec Help View at html/images/help_view.png width 765 px
		de.devboost.natspec.library.documentation.Image image_The_NatSpec_Help_View_html_images_help_view_png_765_px = documentationSupport.image(java.util.Arrays.asList(new java.lang.String[] {"The", "NatSpec", "Help", "View"}), "html/images/help_view.png", "765", "px", subsection_NatSpec_Help);
		// Section - Summary
		de.devboost.natspec.library.documentation.Section section_Summary = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"Summary"}), documentation_Getting_Started_with_NatSpec);
		// This tutorial showed, how NatSpec can be installed in Eclipse and how NatSpec can be
		de.devboost.natspec.library.documentation.Text text_This_tutorial_showed__how_NatSpec_can_be_installed_in_Eclipse_and_how_NatSpec_can_be = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"This", "tutorial", "showed,", "how", "NatSpec", "can", "be", "installed", "in", "Eclipse", "and", "how", "NatSpec", "can", "be"}), section_Summary);
		// applied to specify acceptance and functional tests in natural language. It was shown, how
		de.devboost.natspec.library.documentation.Text text_applied_to_specify_acceptance_and_functional_tests_in_natural_language__It_was_shown__how = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"applied", "to", "specify", "acceptance", "and", "functional", "tests", "in", "natural", "language.", "It", "was", "shown,", "how"}), section_Summary);
		// test support methods are defined to allow the generation of executable JUnit tests out
		de.devboost.natspec.library.documentation.Text text_test_support_methods_are_defined_to_allow_the_generation_of_executable_JUnit_tests_out = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"test", "support", "methods", "are", "defined", "to", "allow", "the", "generation", "of", "executable", "JUnit", "tests", "out"}), section_Summary);
		// of natural language sentences and how persistence and service layers can be integrated
		de.devboost.natspec.library.documentation.Text text_of_natural_language_sentences_and_how_persistence_and_service_layers_can_be_integrated = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "natural", "language", "sentences", "and", "how", "persistence", "and", "service", "layers", "can", "be", "integrated"}), section_Summary);
		// into these test support methods.
		de.devboost.natspec.library.documentation.Text text_into_these_test_support_methods_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"into", "these", "test", "support", "methods."}), section_Summary);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_61 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_Summary);
		// But NatSpec can do even more. You can defined synonyms to be used within your syntax patters,
		de.devboost.natspec.library.documentation.Text text_But_NatSpec_can_do_even_more__You_can_defined_synonyms_to_be_used_within_your_syntax_patters_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"But", "NatSpec", "can", "do", "even", "more.", "You", "can", "defined", "synonyms", "to", "be", "used", "within", "your", "syntax", "patters,"}), paragraph_61);
		// or you can provide syntax patterns programmatically, without the use of NatSpec annotations.
		de.devboost.natspec.library.documentation.Text text_or_you_can_provide_syntax_patterns_programmatically__without_the_use_of_NatSpec_annotations_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"or", "you", "can", "provide", "syntax", "patterns", "programmatically,", "without", "the", "use", "of", "NatSpec", "annotations."}), paragraph_61);
		// Feel free to explore the NatSpec documentation for all functions and features supported
		de.devboost.natspec.library.documentation.Text text_Feel_free_to_explore_the_NatSpec_documentation_for_all_functions_and_features_supported = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Feel", "free", "to", "explore", "the", "NatSpec", "documentation", "for", "all", "functions", "and", "features", "supported"}), paragraph_61);
		// by NatSpec.
		de.devboost.natspec.library.documentation.Text text_by_NatSpec_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"by", "NatSpec."}), paragraph_61);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_62 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_Summary);
		// Besides, your feedback is highly appreciated in order to improve NatSpec. Thus, feel free
		de.devboost.natspec.library.documentation.Text text_Besides__your_feedback_is_highly_appreciated_in_order_to_improve_NatSpec__Thus__feel_free = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Besides,", "your", "feedback", "is", "highly", "appreciated", "in", "order", "to", "improve", "NatSpec.", "Thus,", "feel", "free"}), paragraph_62);
		// to suggest new features, report encountered problems or simply share your NatSpec experience
		de.devboost.natspec.library.documentation.Text text_to_suggest_new_features__report_encountered_problems_or_simply_share_your_NatSpec_experience = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"to", "suggest", "new", "features,", "report", "encountered", "problems", "or", "simply", "share", "your", "NatSpec", "experience"}), paragraph_62);
		// with us. Just
		de.devboost.natspec.library.documentation.Text text_with_us__Just = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"with", "us.", "Just"}), paragraph_62);
		// Link to mailto:support@devboost.de with caption send an e-mail to the NatSpec support team
		de.devboost.natspec.library.documentation.Link link_send_an_e_mail_to_the_NatSpec_support_team_mailto_support_devboost_de = documentationSupport.link(java.util.Arrays.asList(new java.lang.String[] {"send", "an", "e-mail", "to", "the", "NatSpec", "support", "team"}), "mailto:support@devboost.de", section_Summary);
		// . We love to hear from you.
		de.devboost.natspec.library.documentation.Text text___We_love_to_hear_from_you_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "We", "love", "to", "hear", "from", "you."}), paragraph_62);
		
	}

	@Override
	public Documentation getDocumentation() {
		return documentationSupport.getDocumentation();
	}
}
