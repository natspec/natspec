package de.devboost.natspec.doc;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.devboost.natspec.doc.gettingstarted.GettingStarted;
import de.devboost.natspec.doc.guide.NatSpecGuide;
import de.devboost.natspec.doc.support.IDocumentationScript;
import de.devboost.natspec.library.documentation.Documentation;
import de.devboost.natspec.library.documentation.DocumentationGenerator;
import de.devboost.natspec.library.documentation.HTMLValidator;

public class NatSpecDocumentationGenerator {

	private static final String CSS_FILE = "NatSpec";

	public static void main(String[] args) throws Exception {
		new NatSpecDocumentationGenerator().run();
	}

	private void run() throws Exception {
		// Generate both printable and browser version
		generateAndSaveHTML("");
		generateAndSaveHTML("_Print");
	}

	private void generateAndSaveHTML(String suffix) throws IOException, Exception {
		generateAndSaveHTML(new NatSpecGuide(), false, suffix);
		generateAndSaveHTML(new GettingStarted(), true, suffix);
	}

	private void generateAndSaveHTML(IDocumentationScript script,
			final boolean clean, String suffix) throws Exception, IOException {
		
		script.executeScript();
		Documentation documentation = script.getDocumentation();
		DocumentationGenerator generator = new DocumentationGenerator() {
			
			protected String getClassificationHTML() {
				return "";
			}
			
			@Override
			protected boolean deleteIfExists(File file) {
				// We do want to clean directories only the first time.
				if (clean) {
					return super.deleteIfExists(file);
				} else {
					return true;
				}
			}
		};

		String cssFile = CSS_FILE + suffix + ".css";
		String html = generator.getDocumentationAsString(documentation, cssFile);
		boolean isValidXML = new HTMLValidator().isValidXML(html);
		if (!isValidXML) {
			throw new RuntimeException("Generated HTML is invalid.");
		}

		String htmlFileName = script.getClass().getSimpleName() + suffix + ".html";
		String htmlFilePath = NatSpecHelpPlugin.HTML_PATH + File.separator + htmlFileName;
		File htmlFile = new File(htmlFilePath);
		FileWriter writer = new FileWriter(htmlFile);
		writer.write(html);
		writer.close();
	}
}
