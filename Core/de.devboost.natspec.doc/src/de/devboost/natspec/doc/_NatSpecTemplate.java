package de.devboost.natspec.doc;

import de.devboost.natspec.doc.support.IDocumentationScript;
import de.devboost.natspec.library.documentation.Documentation;
import de.devboost.natspec.library.documentation.DocumentationSupport;

public class _NatSpecTemplate implements IDocumentationScript {
	
	protected DocumentationSupport documentationSupport = new DocumentationSupport();
	
	@SuppressWarnings("unused")
	public void executeScript() throws Exception {
		int dummy;
		// generated code will be inserted here
		/* @MethodBody */
	}

	@Override
	public Documentation getDocumentation() {
		return documentationSupport.getDocumentation();
	}
}
