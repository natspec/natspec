package de.devboost.natspec.doc.guide;

import de.devboost.natspec.doc.support.IDocumentationScript;
import de.devboost.natspec.library.documentation.Documentation;
import de.devboost.natspec.library.documentation.DocumentationSupport;

public class NatSpecGuide implements IDocumentationScript {
	
	protected DocumentationSupport documentationSupport = new DocumentationSupport();
	
	@SuppressWarnings("unused")
	public void executeScript() throws Exception {
		int dummy;
		// generated code will be inserted here
		// The code in this method is generated from: /de.devboost.natspec.doc/src/de/devboost/natspec/doc/guide/NatSpecGuide.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Documentation - NatSpec User Guide
		de.devboost.natspec.library.documentation.Documentation documentation_NatSpec_User_Guide = documentationSupport.initDocumentation(java.util.Arrays.asList(new java.lang.String[] {"NatSpec", "User", "Guide"}));
		// Section - Summary for Busy Managers
		de.devboost.natspec.library.documentation.Section section_Summary_for_Busy_Managers = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"Summary", "for", "Busy", "Managers"}), documentation_NatSpec_User_Guide);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_ = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_Summary_for_Busy_Managers);
		// The goal of any software development process is to deliver software to customers
		de.devboost.natspec.library.documentation.Text text_The_goal_of_any_software_development_process_is_to_deliver_software_to_customers = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "goal", "of", "any", "software", "development", "process", "is", "to", "deliver", "software", "to", "customers"}), paragraph_);
		// that satisfies their needs. Transforming requirements into a shipped product is
		de.devboost.natspec.library.documentation.Text text_that_satisfies_their_needs__Transforming_requirements_into_a_shipped_product_is = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"that", "satisfies", "their", "needs.", "Transforming", "requirements", "into", "a", "shipped", "product", "is"}), paragraph_);
		// essentially the job that needs to be done.
		de.devboost.natspec.library.documentation.Text text_essentially_the_job_that_needs_to_be_done_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"essentially", "the", "job", "that", "needs", "to", "be", "done."}), paragraph_);
		// Requirements are often described in natural language. In modern software
		de.devboost.natspec.library.documentation.Text text_Requirements_are_often_described_in_natural_language__In_modern_software = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Requirements", "are", "often", "described", "in", "natural", "language.", "In", "modern", "software"}), paragraph_);
		// development, checking whether those requirements are met and implemented
		de.devboost.natspec.library.documentation.Text text_development__checking_whether_those_requirements_are_met_and_implemented = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"development,", "checking", "whether", "those", "requirements", "are", "met", "and", "implemented"}), paragraph_);
		// correctly is typically done by using unit tests. Such unit tests are usually
		de.devboost.natspec.library.documentation.Text text_correctly_is_typically_done_by_using_unit_tests__Such_unit_tests_are_usually = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"correctly", "is", "typically", "done", "by", "using", "unit", "tests.", "Such", "unit", "tests", "are", "usually"}), paragraph_);
		// described in a programming language like Java.
		de.devboost.natspec.library.documentation.Text text_described_in_a_programming_language_like_Java_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"described", "in", "a", "programming", "language", "like", "Java."}), paragraph_);
		// The language gap between describing the requirements in natural language and
		de.devboost.natspec.library.documentation.Text text_The_language_gap_between_describing_the_requirements_in_natural_language_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "language", "gap", "between", "describing", "the", "requirements", "in", "natural", "language", "and"}), paragraph_);
		// implementing them as unit tests makes it
		de.devboost.natspec.library.documentation.Text text_implementing_them_as_unit_tests_makes_it = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"implementing", "them", "as", "unit", "tests", "makes", "it"}), paragraph_);
		// difficult for customers and developers to communicate effectively.
		de.devboost.natspec.library.documentation.Text text_difficult_for_customers_and_developers_to_communicate_effectively_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"difficult", "for", "customers", "and", "developers", "to", "communicate", "effectively."}), paragraph_);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_0 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_Summary_for_Busy_Managers);
		// NatSpec bridges this gap. It provides means for the specification of
		de.devboost.natspec.library.documentation.Text text_NatSpec_bridges_this_gap__It_provides_means_for_the_specification_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "bridges", "this", "gap.", "It", "provides", "means", "for", "the", "specification", "of"}), paragraph_0);
		// requirements with your customers in natural language while enabling their
		de.devboost.natspec.library.documentation.Text text_requirements_with_your_customers_in_natural_language_while_enabling_their = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"requirements", "with", "your", "customers", "in", "natural", "language", "while", "enabling", "their"}), paragraph_0);
		// execution as a unit test at the same time. Thus, NatSpec allows for writing
		de.devboost.natspec.library.documentation.Text text_execution_as_a_unit_test_at_the_same_time__Thus__NatSpec_allows_for_writing = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"execution", "as", "a", "unit", "test", "at", "the", "same", "time.", "Thus,", "NatSpec", "allows", "for", "writing"}), paragraph_0);
		// readable, maintainable, and executable requirements and test specifications
		de.devboost.natspec.library.documentation.Text text_readable__maintainable__and_executable_requirements_and_test_specifications = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"readable,", "maintainable,", "and", "executable", "requirements", "and", "test", "specifications"}), paragraph_0);
		// that are understood by customers and developers.
		de.devboost.natspec.library.documentation.Text text_that_are_understood_by_customers_and_developers_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"that", "are", "understood", "by", "customers", "and", "developers."}), paragraph_0);
		// An exemplary NatSpec specification is shown below:
		de.devboost.natspec.library.documentation.Text text_An_exemplary_NatSpec_specification_is_shown_below_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"An", "exemplary", "NatSpec", "specification", "is", "shown", "below:"}), paragraph_0);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_ = documentationSupport.beginListing(section_Summary_for_Busy_Managers);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_600 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_);
		// Set totalSeats to 2
		de.devboost.natspec.library.documentation.Text text_Set_totalSeats_to_2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "totalSeats", "to", "2"}), listing_);
		// Create flight LH-1234
		de.devboost.natspec.library.documentation.Text text_Create_flight_LH_1234 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "flight", "LH-1234"}), listing_);
		// Set plane to Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Set_plane_to_Boeing_737_600 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "plane", "to", "Boeing-737-600"}), listing_);
		// Create passenger John Doe
		de.devboost.natspec.library.documentation.Text text_Create_passenger_John_Doe = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "passenger", "John", "Doe"}), listing_);
		// Book seat for John Doe at LH-1234
		de.devboost.natspec.library.documentation.Text text_Book_seat_for_John_Doe_at_LH_1234 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Book", "seat", "for", "John", "Doe", "at", "LH-1234"}), listing_);
		// Assume success
		de.devboost.natspec.library.documentation.Text text_Assume_success = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Assume", "success"}), listing_);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_1 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), section_Summary_for_Busy_Managers);
		// A specification like this is fully sufficient to generate a test case which ensures
		de.devboost.natspec.library.documentation.Text text_A_specification_like_this_is_fully_sufficient_to_generate_a_test_case_which_ensures = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"A", "specification", "like", "this", "is", "fully", "sufficient", "to", "generate", "a", "test", "case", "which", "ensures"}), paragraph_1);
		// that the described scenario is implemented correctly by your
		de.devboost.natspec.library.documentation.Text text_that_the_described_scenario_is_implemented_correctly_by_your = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"that", "the", "described", "scenario", "is", "implemented", "correctly", "by", "your"}), paragraph_1);
		// software. Have you ever dreamed of a solution being that simple?
		de.devboost.natspec.library.documentation.Text text_software__Have_you_ever_dreamed_of_a_solution_being_that_simple_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"software.", "Have", "you", "ever", "dreamed", "of", "a", "solution", "being", "that", "simple?"}), paragraph_1);
		// Section - Guide for Busy Requirements Engineers (and Customers)
		de.devboost.natspec.library.documentation.Section section_Guide_for_Busy_Requirements_Engineers__and_Customers_ = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"Guide", "for", "Busy", "Requirements", "Engineers", "(and", "Customers)"}), documentation_NatSpec_User_Guide);
		// It is your job to describe the functions a software is meant to deliver.
		de.devboost.natspec.library.documentation.Text text_It_is_your_job_to_describe_the_functions_a_software_is_meant_to_deliver_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"It", "is", "your", "job", "to", "describe", "the", "functions", "a", "software", "is", "meant", "to", "deliver."}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// Only if this is done in a very concrete way, developers will be able to realize them as expected.
		de.devboost.natspec.library.documentation.Text text_Only_if_this_is_done_in_a_very_concrete_way__developers_will_be_able_to_realize_them_as_expected_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Only", "if", "this", "is", "done", "in", "a", "very", "concrete", "way,", "developers", "will", "be", "able", "to", "realize", "them", "as", "expected."}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// However, it is often hard to find a precise and yet readable way to give a
		de.devboost.natspec.library.documentation.Text text_However__it_is_often_hard_to_find_a_precise_and_yet_readable_way_to_give_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"However,", "it", "is", "often", "hard", "to", "find", "a", "precise", "and", "yet", "readable", "way", "to", "give", "a"}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// requirements specification.
		de.devboost.natspec.library.documentation.Text text_requirements_specification_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"requirements", "specification."}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// NatSpec helps you to build precise scenario specifications that use
		de.devboost.natspec.library.documentation.Text text_NatSpec_helps_you_to_build_precise_scenario_specifications_that_use = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "helps", "you", "to", "build", "precise", "scenario", "specifications", "that", "use"}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// natural language. There are no limitations on the structure or terminology.
		de.devboost.natspec.library.documentation.Text text_natural_language__There_are_no_limitations_on_the_structure_or_terminology_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"natural", "language.", "There", "are", "no", "limitations", "on", "the", "structure", "or", "terminology."}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// Furthermore, NatSpec specifications can be used by developers to validate
		de.devboost.natspec.library.documentation.Text text_Furthermore__NatSpec_specifications_can_be_used_by_developers_to_validate = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Furthermore,", "NatSpec", "specifications", "can", "be", "used", "by", "developers", "to", "validate"}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// the functions they implement against your expectations.
		de.devboost.natspec.library.documentation.Text text_the_functions_they_implement_against_your_expectations_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "functions", "they", "implement", "against", "your", "expectations."}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// Subsection - Creating a Natural Scenario Specification
		de.devboost.natspec.library.documentation.Subsection subsection_Creating_a_Natural_Scenario_Specification = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Creating", "a", "Natural", "Scenario", "Specification"}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// In NatSpec one can write tests in natural language in form of stories or
		de.devboost.natspec.library.documentation.Text text_In_NatSpec_one_can_write_tests_in_natural_language_in_form_of_stories_or = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"In", "NatSpec", "one", "can", "write", "tests", "in", "natural", "language", "in", "form", "of", "stories", "or"}), subsection_Creating_a_Natural_Scenario_Specification);
		// scenarios.
		de.devboost.natspec.library.documentation.Text text_scenarios_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"scenarios."}), subsection_Creating_a_Natural_Scenario_Specification);
		// Each scenario is meant to describe a function of the application under
		de.devboost.natspec.library.documentation.Text text_Each_scenario_is_meant_to_describe_a_function_of_the_application_under = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Each", "scenario", "is", "meant", "to", "describe", "a", "function", "of", "the", "application", "under"}), subsection_Creating_a_Natural_Scenario_Specification);
		// development in terms of actions and expected behavior. Typically, a scenario
		de.devboost.natspec.library.documentation.Text text_development_in_terms_of_actions_and_expected_behavior__Typically__a_scenario = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"development", "in", "terms", "of", "actions", "and", "expected", "behavior.", "Typically,", "a", "scenario"}), subsection_Creating_a_Natural_Scenario_Specification);
		// involves creating some input data, invoking a few application services, and
		de.devboost.natspec.library.documentation.Text text_involves_creating_some_input_data__invoking_a_few_application_services__and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"involves", "creating", "some", "input", "data,", "invoking", "a", "few", "application", "services,", "and"}), subsection_Creating_a_Natural_Scenario_Specification);
		// checking the actual results against expected results.
		de.devboost.natspec.library.documentation.Text text_checking_the_actual_results_against_expected_results_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"checking", "the", "actual", "results", "against", "expected", "results."}), subsection_Creating_a_Natural_Scenario_Specification);
		// To use NatSpec you should first create a new NatSpec project:
		de.devboost.natspec.library.documentation.Text text_To_use_NatSpec_you_should_first_create_a_new_NatSpec_project_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "use", "NatSpec", "you", "should", "first", "create", "a", "new", "NatSpec", "project:"}), subsection_Creating_a_Natural_Scenario_Specification);
		// Code File > New > Other... > NatSpec Project
		de.devboost.natspec.library.documentation.Code code_File___New___Other______NatSpec_Project = documentationSupport.code(new java.lang.StringBuilder().append("File").append(" ").append(">").append(" ").append("New").append(" ").append(">").append(" ").append("Other...").append(" ").append(">").append(" ").append("NatSpec").append(" ").append("Project").toString(), subsection_Creating_a_Natural_Scenario_Specification);
		// .
		de.devboost.natspec.library.documentation.Text text__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), subsection_Creating_a_Natural_Scenario_Specification);
		// Then add a new test specification to the folder for end-user tests (
		de.devboost.natspec.library.documentation.Text text_Then_add_a_new_test_specification_to_the_folder_for_end_user_tests__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Then", "add", "a", "new", "test", "specification", "to", "the", "folder", "for", "end-user", "tests", "("}), subsection_Creating_a_Natural_Scenario_Specification);
		// Code src-endusertests
		de.devboost.natspec.library.documentation.Code code_src_endusertests = documentationSupport.code(new java.lang.StringBuilder().append("src-endusertests").toString(), subsection_Creating_a_Natural_Scenario_Specification);
		// ):
		de.devboost.natspec.library.documentation.Text text___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"):"}), subsection_Creating_a_Natural_Scenario_Specification);
		// Code File > New > Other... > NatSpec Scenario File (.natspec)
		de.devboost.natspec.library.documentation.Code code_File___New___Other______NatSpec_Scenario_File___natspec_ = documentationSupport.code(new java.lang.StringBuilder().append("File").append(" ").append(">").append(" ").append("New").append(" ").append(">").append(" ").append("Other...").append(" ").append(">").append(" ").append("NatSpec").append(" ").append("Scenario").append(" ").append("File").append(" ").append("(.natspec)").toString(), subsection_Creating_a_Natural_Scenario_Specification);
		// .
		de.devboost.natspec.library.documentation.Text text__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), subsection_Creating_a_Natural_Scenario_Specification);
		// Subsection - Initializing some Input Data
		de.devboost.natspec.library.documentation.Subsection subsection_Initializing_some_Input_Data = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Initializing", "some", "Input", "Data"}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// As NatSpec uses natural language, initializing the input data in a
		de.devboost.natspec.library.documentation.Text text_As_NatSpec_uses_natural_language__initializing_the_input_data_in_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "NatSpec", "uses", "natural", "language,", "initializing", "the", "input", "data", "in", "a"}), subsection_Initializing_some_Input_Data);
		// scenario is as easy as writing a plain English sentence. For an exemplary
		de.devboost.natspec.library.documentation.Text text_scenario_is_as_easy_as_writing_a_plain_English_sentence__For_an_exemplary = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"scenario", "is", "as", "easy", "as", "writing", "a", "plain", "English", "sentence.", "For", "an", "exemplary"}), subsection_Initializing_some_Input_Data);
		// application that deals with airplanes, flights and passengers to book air
		de.devboost.natspec.library.documentation.Text text_application_that_deals_with_airplanes__flights_and_passengers_to_book_air = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"application", "that", "deals", "with", "airplanes,", "flights", "and", "passengers", "to", "book", "air"}), subsection_Initializing_some_Input_Data);
		// travels you could write something like:
		de.devboost.natspec.library.documentation.Text text_travels_you_could_write_something_like_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"travels", "you", "could", "write", "something", "like:"}), subsection_Initializing_some_Input_Data);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_0 = documentationSupport.beginListing(subsection_Initializing_some_Input_Data);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_6000 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_0);
		// Set totalSeats to 2
		de.devboost.natspec.library.documentation.Text text_Set_totalSeats_to_20 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "totalSeats", "to", "2"}), listing_0);
		// Create flight LH-1234
		de.devboost.natspec.library.documentation.Text text_Create_flight_LH_12340 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "flight", "LH-1234"}), listing_0);
		// Set Airplane to Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Set_Airplane_to_Boeing_737_600 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "Airplane", "to", "Boeing-737-600"}), listing_0);
		// Create passenger John Doe
		de.devboost.natspec.library.documentation.Text text_Create_passenger_John_Doe0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "passenger", "John", "Doe"}), listing_0);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_2 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Initializing_some_Input_Data);
		// There are no syntactic limitations on the sentence structures. There are also
		de.devboost.natspec.library.documentation.Text text_There_are_no_syntactic_limitations_on_the_sentence_structures__There_are_also = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"There", "are", "no", "syntactic", "limitations", "on", "the", "sentence", "structures.", "There", "are", "also"}), paragraph_2);
		// no specific keywords required. You can use plain English or any other language
		de.devboost.natspec.library.documentation.Text text_no_specific_keywords_required__You_can_use_plain_English_or_any_other_language = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"no", "specific", "keywords", "required.", "You", "can", "use", "plain", "English", "or", "any", "other", "language"}), paragraph_2);
		// of your choice!
		de.devboost.natspec.library.documentation.Text text_of_your_choice_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "your", "choice!"}), paragraph_2);
		// Subsection - Using an Application Service
		de.devboost.natspec.library.documentation.Subsection subsection_Using_an_Application_Service = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Using", "an", "Application", "Service"}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// Invoking an application service is equally easy:
		de.devboost.natspec.library.documentation.Text text_Invoking_an_application_service_is_equally_easy_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Invoking", "an", "application", "service", "is", "equally", "easy:"}), subsection_Using_an_Application_Service);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_1 = documentationSupport.beginListing(subsection_Using_an_Application_Service);
		// Book seat for John Doe at LH-1234
		de.devboost.natspec.library.documentation.Text text_Book_seat_for_John_Doe_at_LH_12340 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Book", "seat", "for", "John", "Doe", "at", "LH-1234"}), listing_1);
		// Subsection - Checking Expectations on Output
		de.devboost.natspec.library.documentation.Subsection subsection_Checking_Expectations_on_Output = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Checking", "Expectations", "on", "Output"}), section_Guide_for_Busy_Requirements_Engineers__and_Customers_);
		// To check the service results against your expectations you can write
		de.devboost.natspec.library.documentation.Text text_To_check_the_service_results_against_your_expectations_you_can_write = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "check", "the", "service", "results", "against", "your", "expectations", "you", "can", "write"}), subsection_Checking_Expectations_on_Output);
		// something like:
		de.devboost.natspec.library.documentation.Text text_something_like_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"something", "like:"}), subsection_Checking_Expectations_on_Output);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_2 = documentationSupport.beginListing(subsection_Checking_Expectations_on_Output);
		// Assume LH-1234 has passenger John Doe
		de.devboost.natspec.library.documentation.Text text_Assume_LH_1234_has_passenger_John_Doe = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Assume", "LH-1234", "has", "passenger", "John", "Doe"}), listing_2);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_3 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Checking_Expectations_on_Output);
		// That's it. Just write down scenarios capturing expected functionality. The
		de.devboost.natspec.library.documentation.Text text_That_s_it__Just_write_down_scenarios_capturing_expected_functionality__The = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"That's", "it.", "Just", "write", "down", "scenarios", "capturing", "expected", "functionality.", "The"}), paragraph_3);
		// test developers will map your text to actual test code later on and make sure
		de.devboost.natspec.library.documentation.Text text_test_developers_will_map_your_text_to_actual_test_code_later_on_and_make_sure = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"test", "developers", "will", "map", "your", "text", "to", "actual", "test", "code", "later", "on", "and", "make", "sure"}), paragraph_3);
		// that the respective functionality is automatically verified.
		de.devboost.natspec.library.documentation.Text text_that_the_respective_functionality_is_automatically_verified_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"that", "the", "respective", "functionality", "is", "automatically", "verified."}), paragraph_3);
		// Section - Guide for Busy Developers
		de.devboost.natspec.library.documentation.Section section_Guide_for_Busy_Developers = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"Guide", "for", "Busy", "Developers"}), documentation_NatSpec_User_Guide);
		// It is your job to implement the functionality that your customers require.
		de.devboost.natspec.library.documentation.Text text_It_is_your_job_to_implement_the_functionality_that_your_customers_require_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"It", "is", "your", "job", "to", "implement", "the", "functionality", "that", "your", "customers", "require."}), section_Guide_for_Busy_Developers);
		// However, it is hard to implement your customers' expectations if these are not
		de.devboost.natspec.library.documentation.Text text_However__it_is_hard_to_implement_your_customers__expectations_if_these_are_not = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"However,", "it", "is", "hard", "to", "implement", "your", "customers'", "expectations", "if", "these", "are", "not"}), section_Guide_for_Busy_Developers);
		// precisely described or simply incomplete.
		de.devboost.natspec.library.documentation.Text text_precisely_described_or_simply_incomplete_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"precisely", "described", "or", "simply", "incomplete."}), section_Guide_for_Busy_Developers);
		// NatSpec helps you to communicate with your customers in terms of
		de.devboost.natspec.library.documentation.Text text_NatSpec_helps_you_to_communicate_with_your_customers_in_terms_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "helps", "you", "to", "communicate", "with", "your", "customers", "in", "terms", "of"}), section_Guide_for_Busy_Developers);
		// exemplary scenarios. As it uses natural language, those scenarios can be easily
		de.devboost.natspec.library.documentation.Text text_exemplary_scenarios__As_it_uses_natural_language__those_scenarios_can_be_easily = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"exemplary", "scenarios.", "As", "it", "uses", "natural", "language,", "those", "scenarios", "can", "be", "easily"}), section_Guide_for_Busy_Developers);
		// understood both by yourself and by your clients. It will even help you to ask your
		de.devboost.natspec.library.documentation.Text text_understood_both_by_yourself_and_by_your_clients__It_will_even_help_you_to_ask_your = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"understood", "both", "by", "yourself", "and", "by", "your", "clients.", "It", "will", "even", "help", "you", "to", "ask", "your"}), section_Guide_for_Busy_Developers);
		// customers for more detailed specifications and to resolve misunderstandings.
		de.devboost.natspec.library.documentation.Text text_customers_for_more_detailed_specifications_and_to_resolve_misunderstandings_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"customers", "for", "more", "detailed", "specifications", "and", "to", "resolve", "misunderstandings."}), section_Guide_for_Busy_Developers);
		// NatSpec provides means to describe scenario specifications testing the
		de.devboost.natspec.library.documentation.Text text_NatSpec_provides_means_to_describe_scenario_specifications_testing_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "provides", "means", "to", "describe", "scenario", "specifications", "testing", "the"}), section_Guide_for_Busy_Developers);
		// functions you implement. Therefore, scenario specifications are mapped to
		de.devboost.natspec.library.documentation.Text text_functions_you_implement__Therefore__scenario_specifications_are_mapped_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"functions", "you", "implement.", "Therefore,", "scenario", "specifications", "are", "mapped", "to"}), section_Guide_for_Busy_Developers);
		// JUnit tests in an easy and comprehensible way.
		de.devboost.natspec.library.documentation.Text text_JUnit_tests_in_an_easy_and_comprehensible_way_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"JUnit", "tests", "in", "an", "easy", "and", "comprehensible", "way."}), section_Guide_for_Busy_Developers);
		// NatSpec makes requirements engineering more precise. It reduces the
		de.devboost.natspec.library.documentation.Text text_NatSpec_makes_requirements_engineering_more_precise__It_reduces_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "makes", "requirements", "engineering", "more", "precise.", "It", "reduces", "the"}), section_Guide_for_Busy_Developers);
		// effort you invest for building sensible test cases and gives test-driven
		de.devboost.natspec.library.documentation.Text text_effort_you_invest_for_building_sensible_test_cases_and_gives_test_driven = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"effort", "you", "invest", "for", "building", "sensible", "test", "cases", "and", "gives", "test-driven"}), section_Guide_for_Busy_Developers);
		// development a kick start.
		de.devboost.natspec.library.documentation.Text text_development_a_kick_start_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"development", "a", "kick", "start."}), section_Guide_for_Busy_Developers);
		// Subsection - Making Sentences Executable using Test-Support Classes
		de.devboost.natspec.library.documentation.Subsection subsection_Making_Sentences_Executable_using_Test_Support_Classes = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Making", "Sentences", "Executable", "using", "Test-Support", "Classes"}), section_Guide_for_Busy_Developers);
		// To make a sentence of a scenario specification executable, it is simply
		de.devboost.natspec.library.documentation.Text text_To_make_a_sentence_of_a_scenario_specification_executable__it_is_simply = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "make", "a", "sentence", "of", "a", "scenario", "specification", "executable,", "it", "is", "simply"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// mapped to a Java method. This is done in so-called test-support classes. A
		de.devboost.natspec.library.documentation.Text text_mapped_to_a_Java_method__This_is_done_in_so_called_test_support_classes__A = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"mapped", "to", "a", "Java", "method.", "This", "is", "done", "in", "so-called", "test-support", "classes.", "A"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// test-support class is a simple Java class that contains test-support methods.
		de.devboost.natspec.library.documentation.Text text_test_support_class_is_a_simple_Java_class_that_contains_test_support_methods_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"test-support", "class", "is", "a", "simple", "Java", "class", "that", "contains", "test-support", "methods."}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// Test-support methods describe how the syntax of a sentence in the natural
		de.devboost.natspec.library.documentation.Text text_Test_support_methods_describe_how_the_syntax_of_a_sentence_in_the_natural = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Test-support", "methods", "describe", "how", "the", "syntax", "of", "a", "sentence", "in", "the", "natural"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// specification is mapped to Java. These methods are identified by the annotation
		de.devboost.natspec.library.documentation.Text text_specification_is_mapped_to_Java__These_methods_are_identified_by_the_annotation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"specification", "is", "mapped", "to", "Java.", "These", "methods", "are", "identified", "by", "the", "annotation"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// Code @TextSyntax
		de.devboost.natspec.library.documentation.Code code__TextSyntax = documentationSupport.code(new java.lang.StringBuilder().append("@TextSyntax").toString(), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// .
		de.devboost.natspec.library.documentation.Text text__1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// An example is shown in the following:
		de.devboost.natspec.library.documentation.Text text_An_example_is_shown_in_the_following_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"An", "example", "is", "shown", "in", "the", "following:"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_3 = documentationSupport.beginListing(subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// public class TestSupport {
		de.devboost.natspec.library.documentation.Text text_public_class_TestSupport__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "class", "TestSupport", "{"}), listing_3);
		// private AirlineServices services;
		de.devboost.natspec.library.documentation.Text text_private_AirlineServices_services_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"private", "AirlineServices", "services;"}), listing_3);
		// public TestSupport(AirlineServices services) {
		de.devboost.natspec.library.documentation.Text text_public_TestSupport_AirlineServices_services___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "TestSupport(AirlineServices", "services)", "{"}), listing_3);
		// this.services = services;
		de.devboost.natspec.library.documentation.Text text_this_services___services_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"this.services", "=", "services;"}), listing_3);
		// }
		de.devboost.natspec.library.documentation.Text text__2 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_3);
		// @TextSyntax("Book seat for #1 at #2")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Book_seat_for__1_at__2__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Book", "seat", "for", "#1", "at", "#2\")"}), listing_3);
		// public boolean bookSeat(Passenger passenger, Flight flight) {
		de.devboost.natspec.library.documentation.Text text_public_boolean_bookSeat_Passenger_passenger__Flight_flight___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "boolean", "bookSeat(Passenger", "passenger,", "Flight", "flight)", "{"}), listing_3);
		// boolean success = services.bookSeat(passenger, flight);
		de.devboost.natspec.library.documentation.Text text_boolean_success___services_bookSeat_passenger__flight__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"boolean", "success", "=", "services.bookSeat(passenger,", "flight);"}), listing_3);
		// return success;
		de.devboost.natspec.library.documentation.Text text_return_success_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "success;"}), listing_3);
		// }
		de.devboost.natspec.library.documentation.Text text__3 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_3);
		// }
		de.devboost.natspec.library.documentation.Text text__4 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_3);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_4 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// The method
		de.devboost.natspec.library.documentation.Text text_The_method = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "method"}), paragraph_4);
		// Code bookSeat
		de.devboost.natspec.library.documentation.Code code_bookSeat = documentationSupport.code(new java.lang.StringBuilder().append("bookSeat").toString(), paragraph_4);
		// makes the sentence
		de.devboost.natspec.library.documentation.Text text_makes_the_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"makes", "the", "sentence"}), paragraph_4);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_4 = documentationSupport.beginListing(subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// Book seat for John Doe at LH-1234
		de.devboost.natspec.library.documentation.Text text_Book_seat_for_John_Doe_at_LH_12341 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Book", "seat", "for", "John", "Doe", "at", "LH-1234"}), listing_4);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_5 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// executable.
		de.devboost.natspec.library.documentation.Text text_executable_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"executable."}), paragraph_5);
		// Typically, the application service (e.g.,
		de.devboost.natspec.library.documentation.Text text_Typically__the_application_service__e_g__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Typically,", "the", "application", "service", "(e.g.,"}), paragraph_5);
		// Code services.bookSeat(passenger, flight)
		de.devboost.natspec.library.documentation.Code code_services_bookSeat_passenger__flight_ = documentationSupport.code(new java.lang.StringBuilder().append("services.bookSeat(passenger,").append(" ").append("flight)").toString(), paragraph_5);
		// ) that is tested with a specific scenario is to be parameterized with some input
		de.devboost.natspec.library.documentation.Text text___that_is_tested_with_a_specific_scenario_is_to_be_parameterized_with_some_input = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {")", "that", "is", "tested", "with", "a", "specific", "scenario", "is", "to", "be", "parameterized", "with", "some", "input"}), paragraph_5);
		// data. Thus, it is necessary to clarify where and how such input data is
		de.devboost.natspec.library.documentation.Text text_data__Thus__it_is_necessary_to_clarify_where_and_how_such_input_data_is = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"data.", "Thus,", "it", "is", "necessary", "to", "clarify", "where", "and", "how", "such", "input", "data", "is"}), paragraph_5);
		// represented in the natural specification. This is done in the syntax mapping:
		de.devboost.natspec.library.documentation.Text text_represented_in_the_natural_specification__This_is_done_in_the_syntax_mapping_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"represented", "in", "the", "natural", "specification.", "This", "is", "done", "in", "the", "syntax", "mapping:"}), paragraph_5);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_5 = documentationSupport.beginListing(subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// @TextSyntax("Book seat for #1 at #2")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Book_seat_for__1_at__2__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Book", "seat", "for", "#1", "at", "#2\")"}), listing_5);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_6 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// It consists of static structures (words) and variable placeholders. The
		de.devboost.natspec.library.documentation.Text text_It_consists_of_static_structures__words__and_variable_placeholders__The = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"It", "consists", "of", "static", "structures", "(words)", "and", "variable", "placeholders.", "The"}), paragraph_6);
		// placeholders are indicated by a hashtag (#) followed by a number. This number
		de.devboost.natspec.library.documentation.Text text_placeholders_are_indicated_by_a_hashtag_____followed_by_a_number__This_number = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"placeholders", "are", "indicated", "by", "a", "hashtag", "(#)", "followed", "by", "a", "number.", "This", "number"}), paragraph_6);
		// refers to the index position of the method parameters for the according test
		de.devboost.natspec.library.documentation.Text text_refers_to_the_index_position_of_the_method_parameters_for_the_according_test = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"refers", "to", "the", "index", "position", "of", "the", "method", "parameters", "for", "the", "according", "test"}), paragraph_6);
		// support method. In our example #1 refers to parameter
		de.devboost.natspec.library.documentation.Text text_support_method__In_our_example__1_refers_to_parameter = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"support", "method.", "In", "our", "example", "#1", "refers", "to", "parameter"}), paragraph_6);
		// Code passenger
		de.devboost.natspec.library.documentation.Code code_passenger = documentationSupport.code(new java.lang.StringBuilder().append("passenger").toString(), paragraph_6);
		// and #2 refers to parameter
		de.devboost.natspec.library.documentation.Text text_and__2_refers_to_parameter = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "#2", "refers", "to", "parameter"}), paragraph_6);
		// Code flight
		de.devboost.natspec.library.documentation.Code code_flight = documentationSupport.code(new java.lang.StringBuilder().append("flight").toString(), paragraph_6);
		// . Within the test support method the meaning of the sentence is implemented
		de.devboost.natspec.library.documentation.Text text___Within_the_test_support_method_the_meaning_of_the_sentence_is_implemented = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "Within", "the", "test", "support", "method", "the", "meaning", "of", "the", "sentence", "is", "implemented"}), paragraph_6);
		// using plain Java.
		de.devboost.natspec.library.documentation.Text text_using_plain_Java_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"using", "plain", "Java."}), paragraph_6);
		// To assign the same test support method to multiple sentences, the
		de.devboost.natspec.library.documentation.Text text_To_assign_the_same_test_support_method_to_multiple_sentences__the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "assign", "the", "same", "test", "support", "method", "to", "multiple", "sentences,", "the"}), paragraph_6);
		// Code @TextSyntaxes
		de.devboost.natspec.library.documentation.Code code__TextSyntaxes = documentationSupport.code(new java.lang.StringBuilder().append("@TextSyntaxes").toString(), paragraph_6);
		// annotation can be used, which can consist of multiple
		de.devboost.natspec.library.documentation.Text text_annotation_can_be_used__which_can_consist_of_multiple = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"annotation", "can", "be", "used,", "which", "can", "consist", "of", "multiple"}), paragraph_6);
		// Code @TextSyntax
		de.devboost.natspec.library.documentation.Code code__TextSyntax0 = documentationSupport.code(new java.lang.StringBuilder().append("@TextSyntax").toString(), paragraph_6);
		// annotations. For example, the following annotations map two sentences to one
		de.devboost.natspec.library.documentation.Text text_annotations__For_example__the_following_annotations_map_two_sentences_to_one = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"annotations.", "For", "example,", "the", "following", "annotations", "map", "two", "sentences", "to", "one"}), paragraph_6);
		// test support method.
		de.devboost.natspec.library.documentation.Text text_test_support_method_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"test", "support", "method."}), paragraph_6);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_6 = documentationSupport.beginListing(subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// @TextSyntaxes({
		de.devboost.natspec.library.documentation.Text text__TextSyntaxes__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntaxes({"}), listing_6);
		// @TextSyntax("Book seat for #1 at #2"),
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Book_seat_for__1_at__2___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Book", "seat", "for", "#1", "at", "#2\"),"}), listing_6);
		// @TextSyntax("Book flight for #1 at #2"),
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Book_flight_for__1_at__2___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Book", "flight", "for", "#1", "at", "#2\"),"}), listing_6);
		// })
		de.devboost.natspec.library.documentation.Text text___0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"})"}), listing_6);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_7 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// NatSpec accepts arbitrary many syntax patterns, but one must take care to make
		de.devboost.natspec.library.documentation.Text text_NatSpec_accepts_arbitrary_many_syntax_patterns__but_one_must_take_care_to_make = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "accepts", "arbitrary", "many", "syntax", "patterns,", "but", "one", "must", "take", "care", "to", "make"}), paragraph_7);
		// sure that these patterns do not match the same sentences.
		de.devboost.natspec.library.documentation.Text text_sure_that_these_patterns_do_not_match_the_same_sentences_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sure", "that", "these", "patterns", "do", "not", "match", "the", "same", "sentences."}), paragraph_7);
		// Subsubsection - Matching Lists of Words
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Matching_Lists_of_Words = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Matching", "Lists", "of", "Words"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// NatSpec supports to assign multiple words to a single parameter. This is
		de.devboost.natspec.library.documentation.Text text_NatSpec_supports_to_assign_multiple_words_to_a_single_parameter__This_is = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "supports", "to", "assign", "multiple", "words", "to", "a", "single", "parameter.", "This", "is"}), subsubsection_Matching_Lists_of_Words);
		// automatically done, if the type of the respective parameter is
		de.devboost.natspec.library.documentation.Text text_automatically_done__if_the_type_of_the_respective_parameter_is = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"automatically", "done,", "if", "the", "type", "of", "the", "respective", "parameter", "is"}), subsubsection_Matching_Lists_of_Words);
		// 'java.util.List'. For example:
		de.devboost.natspec.library.documentation.Text text__java_util_List___For_example_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"'java.util.List'.", "For", "example:"}), subsubsection_Matching_Lists_of_Words);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_7 = documentationSupport.beginListing(subsubsection_Matching_Lists_of_Words);
		// @TextSyntax("Print #1")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Print__1__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Print", "#1\")"}), listing_7);
		// public void printNames(List<String> names) {
		de.devboost.natspec.library.documentation.Text text_public_void_printNames_List_String__names___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "printNames(List<String>", "names)", "{"}), listing_7);
		// for (String name : names) {
		de.devboost.natspec.library.documentation.Text text_for__String_name___names___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"for", "(String", "name", ":", "names)", "{"}), listing_7);
		// System.out.println(name);
		de.devboost.natspec.library.documentation.Text text_System_out_println_name__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(name);"}), listing_7);
		// }
		de.devboost.natspec.library.documentation.Text text__5 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_7);
		// }
		de.devboost.natspec.library.documentation.Text text__6 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_7);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_8 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Lists_of_Words);
		// matches the sentence:
		de.devboost.natspec.library.documentation.Text text_matches_the_sentence_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"matches", "the", "sentence:"}), paragraph_8);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_8 = documentationSupport.beginListing(subsubsection_Matching_Lists_of_Words);
		// Print John Jane Peter
		de.devboost.natspec.library.documentation.Text text_Print_John_Jane_Peter = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Print", "John", "Jane", "Peter"}), listing_8);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_9 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Lists_of_Words);
		// To match a list of words, but use them as a single String argument, the
		de.devboost.natspec.library.documentation.Text text_To_match_a_list_of_words__but_use_them_as_a_single_String_argument__the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "match", "a", "list", "of", "words,", "but", "use", "them", "as", "a", "single", "String", "argument,", "the"}), paragraph_9);
		// annotation
		de.devboost.natspec.library.documentation.Text text_annotation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"annotation"}), paragraph_9);
		// Code @Many
		de.devboost.natspec.library.documentation.Code code__Many = documentationSupport.code(new java.lang.StringBuilder().append("@Many").toString(), paragraph_9);
		// can be used:
		de.devboost.natspec.library.documentation.Text text_can_be_used_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"can", "be", "used:"}), paragraph_9);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_9 = documentationSupport.beginListing(subsubsection_Matching_Lists_of_Words);
		// @TextSyntax("Print #1")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Print__1__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Print", "#1\")"}), listing_9);
		// public void print(@Many String names) {
		de.devboost.natspec.library.documentation.Text text_public_void_print__Many_String_names___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "print(@Many", "String", "names)", "{"}), listing_9);
		// System.out.println(names);
		de.devboost.natspec.library.documentation.Text text_System_out_println_names__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(names);"}), listing_9);
		// }
		de.devboost.natspec.library.documentation.Text text__7 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_9);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_10 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Lists_of_Words);
		// This does also match the sentence:
		de.devboost.natspec.library.documentation.Text text_This_does_also_match_the_sentence_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"This", "does", "also", "match", "the", "sentence:"}), paragraph_10);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_10 = documentationSupport.beginListing(subsubsection_Matching_Lists_of_Words);
		// Print John Jane Peter
		de.devboost.natspec.library.documentation.Text text_Print_John_Jane_Peter0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Print", "John", "Jane", "Peter"}), listing_10);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_11 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Lists_of_Words);
		// but passes "John Jane Peter" as argument to
		de.devboost.natspec.library.documentation.Text text_but_passes__John_Jane_Peter__as_argument_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"but", "passes", "\"John", "Jane", "Peter\"", "as", "argument", "to"}), paragraph_11);
		// Code print()
		de.devboost.natspec.library.documentation.Code code_print__ = documentationSupport.code(new java.lang.StringBuilder().append("print()").toString(), paragraph_11);
		// .
		de.devboost.natspec.library.documentation.Text text__8 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), paragraph_11);
		// Subsubsection - Matching Boolean Parameters
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Matching_Boolean_Parameters = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Matching", "Boolean", "Parameters"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// NatSpec supports boolean parameters, but these require special handling as it
		de.devboost.natspec.library.documentation.Text text_NatSpec_supports_boolean_parameters__but_these_require_special_handling_as_it = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "supports", "boolean", "parameters,", "but", "these", "require", "special", "handling", "as", "it"}), subsubsection_Matching_Boolean_Parameters);
		// is not clear which text in a sentence represents the value 'true' and
		de.devboost.natspec.library.documentation.Text text_is_not_clear_which_text_in_a_sentence_represents_the_value__true__and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"is", "not", "clear", "which", "text", "in", "a", "sentence", "represents", "the", "value", "'true'", "and"}), subsubsection_Matching_Boolean_Parameters);
		// which represents 'false'.
		de.devboost.natspec.library.documentation.Text text_which_represents__false__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"which", "represents", "'false'."}), subsubsection_Matching_Boolean_Parameters);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_12 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Boolean_Parameters);
		// If one wants to represent 'true' by the presence of one word, the parameter name
		de.devboost.natspec.library.documentation.Text text_If_one_wants_to_represent__true__by_the_presence_of_one_word__the_parameter_name = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "one", "wants", "to", "represent", "'true'", "by", "the", "presence", "of", "one", "word,", "the", "parameter", "name"}), paragraph_12);
		// can be used to specify this word. For example, the following listing:
		de.devboost.natspec.library.documentation.Text text_can_be_used_to_specify_this_word__For_example__the_following_listing_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"can", "be", "used", "to", "specify", "this", "word.", "For", "example,", "the", "following", "listing:"}), paragraph_12);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_11 = documentationSupport.beginListing(subsubsection_Matching_Boolean_Parameters);
		// @TextSyntax("Assume #1 failure")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Assume__1_failure__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Assume", "#1", "failure\")"}), listing_11);
		// public void assume(boolean no) {
		de.devboost.natspec.library.documentation.Text text_public_void_assume_boolean_no___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "assume(boolean", "no)", "{"}), listing_11);
		// System.out.println(no ? "Failure not expected." : "Failure expected.");
		de.devboost.natspec.library.documentation.Text text_System_out_println_no____Failure_not_expected______Failure_expected____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(no", "?", "\"Failure", "not", "expected.\"", ":", "\"Failure", "expected.\");"}), listing_11);
		// }
		de.devboost.natspec.library.documentation.Text text__9 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_11);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_13 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Boolean_Parameters);
		// matches the following two sentences:
		de.devboost.natspec.library.documentation.Text text_matches_the_following_two_sentences_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"matches", "the", "following", "two", "sentences:"}), paragraph_13);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_12 = documentationSupport.beginListing(subsubsection_Matching_Boolean_Parameters);
		// Assume no failure
		de.devboost.natspec.library.documentation.Text text_Assume_no_failure = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Assume", "no", "failure"}), listing_12);
		// Assume failure
		de.devboost.natspec.library.documentation.Text text_Assume_failure = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Assume", "failure"}), listing_12);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_14 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Boolean_Parameters);
		// For the first sentence the value 'true' is assigned to the parameter 'no'. For
		de.devboost.natspec.library.documentation.Text text_For_the_first_sentence_the_value__true__is_assigned_to_the_parameter__no___For = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"For", "the", "first", "sentence", "the", "value", "'true'", "is", "assigned", "to", "the", "parameter", "'no'.", "For"}), paragraph_14);
		// the second sentence that parameter is set to 'false'.
		de.devboost.natspec.library.documentation.Text text_the_second_sentence_that_parameter_is_set_to__false__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "second", "sentence", "that", "parameter", "is", "set", "to", "'false'."}), paragraph_14);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_15 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Boolean_Parameters);
		// Alternatively, one can explicitly specify the words that are used to
		de.devboost.natspec.library.documentation.Text text_Alternatively__one_can_explicitly_specify_the_words_that_are_used_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Alternatively,", "one", "can", "explicitly", "specify", "the", "words", "that", "are", "used", "to"}), paragraph_15);
		// represent 'true' and 'false' by using the @BooleanSyntax annotation:
		de.devboost.natspec.library.documentation.Text text_represent__true__and__false__by_using_the__BooleanSyntax_annotation_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"represent", "'true'", "and", "'false'", "by", "using", "the", "@BooleanSyntax", "annotation:"}), paragraph_15);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_13 = documentationSupport.beginListing(subsubsection_Matching_Boolean_Parameters);
		// @TextSyntax("#1 authentication")
		de.devboost.natspec.library.documentation.Text text__TextSyntax___1_authentication__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"#1", "authentication\")"}), listing_13);
		// public void selectAuthentication(@BooleanSyntax({"Enable", "Disable"}) boolean enable) {
		de.devboost.natspec.library.documentation.Text text_public_void_selectAuthentication__BooleanSyntax___Enable____Disable____boolean_enable___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "selectAuthentication(@BooleanSyntax({\"Enable\",", "\"Disable\"})", "boolean", "enable)", "{"}), listing_13);
		// System.out.println("Authentication is " + (enable ? "on" : "off"));
		de.devboost.natspec.library.documentation.Text text_System_out_println__Authentication_is______enable____on_____off____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(\"Authentication", "is", "\"", "+", "(enable", "?", "\"on\"", ":", "\"off\"));"}), listing_13);
		// }
		de.devboost.natspec.library.documentation.Text text__10 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_13);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_16 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Matching_Boolean_Parameters);
		// matches the following two sentences:
		de.devboost.natspec.library.documentation.Text text_matches_the_following_two_sentences_0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"matches", "the", "following", "two", "sentences:"}), paragraph_16);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_14 = documentationSupport.beginListing(subsubsection_Matching_Boolean_Parameters);
		// Enable authentication
		de.devboost.natspec.library.documentation.Text text_Enable_authentication = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Enable", "authentication"}), listing_14);
		// Disable authentication
		de.devboost.natspec.library.documentation.Text text_Disable_authentication = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Disable", "authentication"}), listing_14);
		// Subsubsection - Defining Optional Implicit Parameters
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Defining_Optional_Implicit_Parameters = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Defining", "Optional", "Implicit", "Parameters"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// NatSpec supports optional implicit parameters. Such parameters are optional in the sense that if no matching object in
		de.devboost.natspec.library.documentation.Text text_NatSpec_supports_optional_implicit_parameters__Such_parameters_are_optional_in_the_sense_that_if_no_matching_object_in = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "supports", "optional", "implicit", "parameters.", "Such", "parameters", "are", "optional", "in", "the", "sense", "that", "if", "no", "matching", "object", "in"}), subsubsection_Defining_Optional_Implicit_Parameters);
		// the context of the NatSpec script was found, null is used as argument for the parameter.
		de.devboost.natspec.library.documentation.Text text_the_context_of_the_NatSpec_script_was_found__null_is_used_as_argument_for_the_parameter_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "context", "of", "the", "NatSpec", "script", "was", "found,", "null", "is", "used", "as", "argument", "for", "the", "parameter."}), subsubsection_Defining_Optional_Implicit_Parameters);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_15 = documentationSupport.beginListing(subsubsection_Defining_Optional_Implicit_Parameters);
		// @TextSyntax("Simulate flight #1")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Simulate_flight__1__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Simulate", "flight", "#1\")"}), listing_15);
		// public void assume(Flight flight, @Optional FlightCrew crew) {
		de.devboost.natspec.library.documentation.Text text_public_void_assume_Flight_flight___Optional_FlightCrew_crew___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "assume(Flight", "flight,", "@Optional", "FlightCrew", "crew)", "{"}), listing_15);
		// if (crew != null) {
		de.devboost.natspec.library.documentation.Text text_if__crew____null___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"if", "(crew", "!=", "null)", "{"}), listing_15);
		// flight.setCrew(crew);
		de.devboost.natspec.library.documentation.Text text_flight_setCrew_crew__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"flight.setCrew(crew);"}), listing_15);
		// }
		de.devboost.natspec.library.documentation.Text text__11 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_15);
		// flight.simulate();
		de.devboost.natspec.library.documentation.Text text_flight_simulate___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"flight.simulate();"}), listing_15);
		// }
		de.devboost.natspec.library.documentation.Text text__12 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_15);
		// Subsubsection - Enforcing Patterns for Text Syntax Patterns
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Enforcing_Patterns_for_Text_Syntax_Patterns = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Enforcing", "Patterns", "for", "Text", "Syntax", "Patterns"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// NatSpec supports the enforcing of patterns that have to apply for all syntax patterns in the same test support class.
		de.devboost.natspec.library.documentation.Text text_NatSpec_supports_the_enforcing_of_patterns_that_have_to_apply_for_all_syntax_patterns_in_the_same_test_support_class_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "supports", "the", "enforcing", "of", "patterns", "that", "have", "to", "apply", "for", "all", "syntax", "patterns", "in", "the", "same", "test", "support", "class."}), subsubsection_Enforcing_Patterns_for_Text_Syntax_Patterns);
		// This can be useful to ensure that all syntax patterns from one test support class follow a basic construction
		de.devboost.natspec.library.documentation.Text text_This_can_be_useful_to_ensure_that_all_syntax_patterns_from_one_test_support_class_follow_a_basic_construction = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"This", "can", "be", "useful", "to", "ensure", "that", "all", "syntax", "patterns", "from", "one", "test", "support", "class", "follow", "a", "basic", "construction"}), subsubsection_Enforcing_Patterns_for_Text_Syntax_Patterns);
		// rule. Thus, they can easily be identified when used in a NatSpec specification.
		de.devboost.natspec.library.documentation.Text text_rule__Thus__they_can_easily_be_identified_when_used_in_a_NatSpec_specification_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"rule.", "Thus,", "they", "can", "easily", "be", "identified", "when", "used", "in", "a", "NatSpec", "specification."}), subsubsection_Enforcing_Patterns_for_Text_Syntax_Patterns);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_17 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Enforcing_Patterns_for_Text_Syntax_Patterns);
		// To define such construction rules you can use the @AllowedSyntaxPattern annotation. The value of the annotation
		de.devboost.natspec.library.documentation.Text text_To_define_such_construction_rules_you_can_use_the__AllowedSyntaxPattern_annotation__The_value_of_the_annotation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "define", "such", "construction", "rules", "you", "can", "use", "the", "@AllowedSyntaxPattern", "annotation.", "The", "value", "of", "the", "annotation"}), paragraph_17);
		// is a regular expression which all syntax patterns must match. For example, the following annotation defines that
		de.devboost.natspec.library.documentation.Text text_is_a_regular_expression_which_all_syntax_patterns_must_match__For_example__the_following_annotation_defines_that = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"is", "a", "regular", "expression", "which", "all", "syntax", "patterns", "must", "match.", "For", "example,", "the", "following", "annotation", "defines", "that"}), paragraph_17);
		// only patterns which start with "Assume" can be defined in the respective test support class.
		de.devboost.natspec.library.documentation.Text text_only_patterns_which_start_with__Assume__can_be_defined_in_the_respective_test_support_class_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"only", "patterns", "which", "start", "with", "\"Assume\"", "can", "be", "defined", "in", "the", "respective", "test", "support", "class."}), paragraph_17);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_16 = documentationSupport.beginListing(subsubsection_Enforcing_Patterns_for_Text_Syntax_Patterns);
		// @AllowedSyntaxPattern("Assume.*")
		de.devboost.natspec.library.documentation.Text text__AllowedSyntaxPattern__Assume____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@AllowedSyntaxPattern(\"Assume.*\")"}), listing_16);
		// public class MySupportClassWithAssumptions {
		de.devboost.natspec.library.documentation.Text text_public_class_MySupportClassWithAssumptions__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "class", "MySupportClassWithAssumptions", "{"}), listing_16);
		// @TextSyntax("Assume ticket is valid")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Assume_ticket_is_valid__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Assume", "ticket", "is", "valid\")"}), listing_16);
		// public void assume(Ticket ticket) {
		de.devboost.natspec.library.documentation.Text text_public_void_assume_Ticket_ticket___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "assume(Ticket", "ticket)", "{"}), listing_16);
		// assertTrue(ticket.isValid());
		de.devboost.natspec.library.documentation.Text text_assertTrue_ticket_isValid____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"assertTrue(ticket.isValid());"}), listing_16);
		// assertEquals(0, ticket.getErrors());
		de.devboost.natspec.library.documentation.Text text_assertEquals_0__ticket_getErrors____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"assertEquals(0,", "ticket.getErrors());"}), listing_16);
		// }
		de.devboost.natspec.library.documentation.Text text__13 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_16);
		// }
		de.devboost.natspec.library.documentation.Text text__14 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_16);
		// Subsubsection - Supported Primitive Types
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Supported_Primitive_Types = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Supported", "Primitive", "Types"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// NatSpec supports the following primitive types:
		de.devboost.natspec.library.documentation.Text text_NatSpec_supports_the_following_primitive_types_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "supports", "the", "following", "primitive", "types:"}), subsubsection_Supported_Primitive_Types);
		// List
		de.devboost.natspec.library.documentation.List list_ = documentationSupport.addList(subsubsection_Supported_Primitive_Types);
		// * String
		de.devboost.natspec.library.documentation.ListItem listItem_String = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"String"}), list_);
		// * int/Integer
		de.devboost.natspec.library.documentation.ListItem listItem_int_Integer = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"int/Integer"}), list_);
		// * long/Long
		de.devboost.natspec.library.documentation.ListItem listItem_long_Long = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"long/Long"}), list_);
		// * float/Float
		de.devboost.natspec.library.documentation.ListItem listItem_float_Float = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"float/Float"}), list_);
		// * double/Double
		de.devboost.natspec.library.documentation.ListItem listItem_double_Double = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"double/Double"}), list_);
		// * java.util.Date
		de.devboost.natspec.library.documentation.ListItem listItem_java_util_Date = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"java.util.Date"}), list_);
		// * java.time.LocalDate
		de.devboost.natspec.library.documentation.ListItem listItem_java_time_LocalDate = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"java.time.LocalDate"}), list_);
		// * java.time.LocalTime
		de.devboost.natspec.library.documentation.ListItem listItem_java_time_LocalTime = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"java.time.LocalTime"}), list_);
		// * java.time.LocalDateTime
		de.devboost.natspec.library.documentation.ListItem listItem_java_time_LocalDateTime = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"java.time.LocalDateTime"}), list_);
		// * java.math.BigDecimal
		de.devboost.natspec.library.documentation.ListItem listItem_java_math_BigDecimal = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"java.math.BigDecimal"}), list_);
		// Subsubsection - Deriving Syntax from Method Names
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Deriving_Syntax_from_Method_Names = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Deriving", "Syntax", "from", "Method", "Names"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// Alternatively to the annotation
		de.devboost.natspec.library.documentation.Text text_Alternatively_to_the_annotation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Alternatively", "to", "the", "annotation"}), subsubsection_Deriving_Syntax_from_Method_Names);
		// Code @TextSyntax
		de.devboost.natspec.library.documentation.Code code__TextSyntax1 = documentationSupport.code(new java.lang.StringBuilder().append("@TextSyntax").toString(), subsubsection_Deriving_Syntax_from_Method_Names);
		// , the annotation
		de.devboost.natspec.library.documentation.Text text___the_annotation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "the", "annotation"}), subsubsection_Deriving_Syntax_from_Method_Names);
		// Code @NameBasedSyntax
		de.devboost.natspec.library.documentation.Code code__NameBasedSyntax = documentationSupport.code(new java.lang.StringBuilder().append("@NameBasedSyntax").toString(), subsubsection_Deriving_Syntax_from_Method_Names);
		// can be used. The syntax pattern for methods which are annotated with
		de.devboost.natspec.library.documentation.Text text_can_be_used__The_syntax_pattern_for_methods_which_are_annotated_with = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"can", "be", "used.", "The", "syntax", "pattern", "for", "methods", "which", "are", "annotated", "with"}), subsubsection_Deriving_Syntax_from_Method_Names);
		// Code @NameBasedSyntax
		de.devboost.natspec.library.documentation.Code code__NameBasedSyntax0 = documentationSupport.code(new java.lang.StringBuilder().append("@NameBasedSyntax").toString(), subsubsection_Deriving_Syntax_from_Method_Names);
		// is derived from the method name. For example:
		de.devboost.natspec.library.documentation.Text text_is_derived_from_the_method_name__For_example_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"is", "derived", "from", "the", "method", "name.", "For", "example:"}), subsubsection_Deriving_Syntax_from_Method_Names);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_17 = documentationSupport.beginListing(subsubsection_Deriving_Syntax_from_Method_Names);
		// @NameBasedSyntax
		de.devboost.natspec.library.documentation.Text text__NameBasedSyntax = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@NameBasedSyntax"}), listing_17);
		// public void print_and_toStdout(String text1, String text2) {
		de.devboost.natspec.library.documentation.Text text_public_void_print_and_toStdout_String_text1__String_text2___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "print_and_toStdout(String", "text1,", "String", "text2)", "{"}), listing_17);
		// System.out.println(text1);
		de.devboost.natspec.library.documentation.Text text_System_out_println_text1__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(text1);"}), listing_17);
		// System.out.println(text2);
		de.devboost.natspec.library.documentation.Text text_System_out_println_text2__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(text2);"}), listing_17);
		// }
		de.devboost.natspec.library.documentation.Text text__15 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_17);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_18 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Deriving_Syntax_from_Method_Names);
		// is equivalent to the following method:
		de.devboost.natspec.library.documentation.Text text_is_equivalent_to_the_following_method_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"is", "equivalent", "to", "the", "following", "method:"}), paragraph_18);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_18 = documentationSupport.beginListing(subsubsection_Deriving_Syntax_from_Method_Names);
		// @TextSyntax("Print #1 and #2 to Stdout")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Print__1_and__2_to_Stdout__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Print", "#1", "and", "#2", "to", "Stdout\")"}), listing_18);
		// public void print(String text1, String text2) {
		de.devboost.natspec.library.documentation.Text text_public_void_print_String_text1__String_text2___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "print(String", "text1,", "String", "text2)", "{"}), listing_18);
		// System.out.println(text1);
		de.devboost.natspec.library.documentation.Text text_System_out_println_text1__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(text1);"}), listing_18);
		// System.out.println(text2);
		de.devboost.natspec.library.documentation.Text text_System_out_println_text2__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"System.out.println(text2);"}), listing_18);
		// }
		de.devboost.natspec.library.documentation.Text text__16 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_18);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_19 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Deriving_Syntax_from_Method_Names);
		// The underscore indicate the places where parameters can occur in the sentence
		de.devboost.natspec.library.documentation.Text text_The_underscore_indicate_the_places_where_parameters_can_occur_in_the_sentence = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "underscore", "indicate", "the", "places", "where", "parameters", "can", "occur", "in", "the", "sentence"}), paragraph_19);
		// and the words are split based on the camel-case method name.
		de.devboost.natspec.library.documentation.Text text_and_the_words_are_split_based_on_the_camel_case_method_name_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "the", "words", "are", "split", "based", "on", "the", "camel-case", "method", "name."}), paragraph_19);
		// Subsubsection - Using NatSpec with JUnit
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Using_NatSpec_with_JUnit = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Using", "NatSpec", "with", "JUnit"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// Subsubsection - Creating a NatSpec Template that uses JUnit
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Creating_a_NatSpec_Template_that_uses_JUnit = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Creating", "a", "NatSpec", "Template", "that", "uses", "JUnit"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// To use NatSpec for the specification of JUnit tests, one must supply a NatSpec
		de.devboost.natspec.library.documentation.Text text_To_use_NatSpec_for_the_specification_of_JUnit_tests__one_must_supply_a_NatSpec = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "use", "NatSpec", "for", "the", "specification", "of", "JUnit", "tests,", "one", "must", "supply", "a", "NatSpec"}), subsubsection_Creating_a_NatSpec_Template_that_uses_JUnit);
		// template (_NatSpecTemplate.java) that includes a method with an @Test
		de.devboost.natspec.library.documentation.Text text_template___NatSpecTemplate_java__that_includes_a_method_with_an__Test = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"template", "(_NatSpecTemplate.java)", "that", "includes", "a", "method", "with", "an", "@Test"}), subsubsection_Creating_a_NatSpec_Template_that_uses_JUnit);
		// annotation. This method must contain the @MethodBody placeholder to tell NatSpec
		de.devboost.natspec.library.documentation.Text text_annotation__This_method_must_contain_the__MethodBody_placeholder_to_tell_NatSpec = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"annotation.", "This", "method", "must", "contain", "the", "@MethodBody", "placeholder", "to", "tell", "NatSpec"}), subsubsection_Creating_a_NatSpec_Template_that_uses_JUnit);
		// to put the generated code into the method. For example, your template could look
		de.devboost.natspec.library.documentation.Text text_to_put_the_generated_code_into_the_method__For_example__your_template_could_look = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"to", "put", "the", "generated", "code", "into", "the", "method.", "For", "example,", "your", "template", "could", "look"}), subsubsection_Creating_a_NatSpec_Template_that_uses_JUnit);
		// like this:
		de.devboost.natspec.library.documentation.Text text_like_this_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"like", "this:"}), subsubsection_Creating_a_NatSpec_Template_that_uses_JUnit);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_19 = documentationSupport.beginListing(subsubsection_Creating_a_NatSpec_Template_that_uses_JUnit);
		// import org.junit.Test;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_Test_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.Test;"}), listing_19);
		// public class _NatSpecTemplate {
		de.devboost.natspec.library.documentation.Text text_public_class__NatSpecTemplate__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "class", "_NatSpecTemplate", "{"}), listing_19);
		// protected MyTestSupport myTestSupport = new MyTestSupport();
		de.devboost.natspec.library.documentation.Text text_protected_MyTestSupport_myTestSupport___new_MyTestSupport___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"protected", "MyTestSupport", "myTestSupport", "=", "new", "MyTestSupport();"}), listing_19);
		// @Test
		de.devboost.natspec.library.documentation.Text text__Test = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Test"}), listing_19);
		// public void runTest() throws Exception {
		de.devboost.natspec.library.documentation.Text text_public_void_runTest___throws_Exception__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "runTest()", "throws", "Exception", "{"}), listing_19);
		// /* @MethodBody */
		de.devboost.natspec.library.documentation.Text text_____MethodBody___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "@MethodBody", "*/"}), listing_19);
		// }
		de.devboost.natspec.library.documentation.Text text__17 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_19);
		// }
		de.devboost.natspec.library.documentation.Text text__18 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_19);
		// Subsubsection - Using the NatSpec JUnit Template to show sentences in JUnit view
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Using", "the", "NatSpec", "JUnit", "Template", "to", "show", "sentences", "in", "JUnit", "view"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// NatSpec provides a custom template that enables to show the individual sentences
		de.devboost.natspec.library.documentation.Text text_NatSpec_provides_a_custom_template_that_enables_to_show_the_individual_sentences = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "provides", "a", "custom", "template", "that", "enables", "to", "show", "the", "individual", "sentences"}), subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view);
		// in the Eclipse JUnit view when running test cases. To use this template, the
		de.devboost.natspec.library.documentation.Text text_in_the_Eclipse_JUnit_view_when_running_test_cases__To_use_this_template__the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "the", "Eclipse", "JUnit", "view", "when", "running", "test", "cases.", "To", "use", "this", "template,", "the"}), subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view);
		// plug-in 'de.devboost.natspec.junit4.runner' must be added to the project
		de.devboost.natspec.library.documentation.Text text_plug_in__de_devboost_natspec_junit4_runner__must_be_added_to_the_project = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"plug-in", "'de.devboost.natspec.junit4.runner'", "must", "be", "added", "to", "the", "project"}), subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view);
		// dependencies. Once the dependency has been added, a template can be defined as
		de.devboost.natspec.library.documentation.Text text_dependencies__Once_the_dependency_has_been_added__a_template_can_be_defined_as = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"dependencies.", "Once", "the", "dependency", "has", "been", "added,", "a", "template", "can", "be", "defined", "as"}), subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view);
		// follows:
		de.devboost.natspec.library.documentation.Text text_follows_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"follows:"}), subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_20 = documentationSupport.beginListing(subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view);
		// import org.junit.Test;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_Test_0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.Test;"}), listing_20);
		// import org.junit.runner.RunWith;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_runner_RunWith_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.runner.RunWith;"}), listing_20);
		// import de.devboost.natspec.junit4.runner.NatSpecJUnit4Runner;
		de.devboost.natspec.library.documentation.Text text_import_de_devboost_natspec_junit4_runner_NatSpecJUnit4Runner_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "de.devboost.natspec.junit4.runner.NatSpecJUnit4Runner;"}), listing_20);
		// import de.devboost.natspec.junit4.runner.NatSpecJUnit4Template;
		de.devboost.natspec.library.documentation.Text text_import_de_devboost_natspec_junit4_runner_NatSpecJUnit4Template_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "de.devboost.natspec.junit4.runner.NatSpecJUnit4Template;"}), listing_20);
		// @RunWith(NatSpecJUnit4Runner.class)
		de.devboost.natspec.library.documentation.Text text__RunWith_NatSpecJUnit4Runner_class_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@RunWith(NatSpecJUnit4Runner.class)"}), listing_20);
		// public class _NatSpecTemplate extends NatSpecJUnit4Template {
		de.devboost.natspec.library.documentation.Text text_public_class__NatSpecTemplate_extends_NatSpecJUnit4Template__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "class", "_NatSpecTemplate", "extends", "NatSpecJUnit4Template", "{"}), listing_20);
		// protected MyTestSupport myTestSupport = new MyTestSupport();
		de.devboost.natspec.library.documentation.Text text_protected_MyTestSupport_myTestSupport___new_MyTestSupport___0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"protected", "MyTestSupport", "myTestSupport", "=", "new", "MyTestSupport();"}), listing_20);
		// @Test
		de.devboost.natspec.library.documentation.Text text__Test0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Test"}), listing_20);
		// public void runTest() throws Exception {
		de.devboost.natspec.library.documentation.Text text_public_void_runTest___throws_Exception__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "runTest()", "throws", "Exception", "{"}), listing_20);
		// /* @MethodBody */
		de.devboost.natspec.library.documentation.Text text_____MethodBody___0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "@MethodBody", "*/"}), listing_20);
		// }
		de.devboost.natspec.library.documentation.Text text__19 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_20);
		// }
		de.devboost.natspec.library.documentation.Text text__20 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_20);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_20 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Using_the_NatSpec_JUnit_Template_to_show_sentences_in_JUnit_view);
		// Note that the template must extend the NatSpecJUnit4Template class and run with
		de.devboost.natspec.library.documentation.Text text_Note_that_the_template_must_extend_the_NatSpecJUnit4Template_class_and_run_with = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Note", "that", "the", "template", "must", "extend", "the", "NatSpecJUnit4Template", "class", "and", "run", "with"}), paragraph_20);
		// the NatSpecJUnit4Runner. Only if both is declared, the Eclipse JUnit view will
		de.devboost.natspec.library.documentation.Text text_the_NatSpecJUnit4Runner__Only_if_both_is_declared__the_Eclipse_JUnit_view_will = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "NatSpecJUnit4Runner.", "Only", "if", "both", "is", "declared,", "the", "Eclipse", "JUnit", "view", "will"}), paragraph_20);
		// show the individual sentences as test steps.
		de.devboost.natspec.library.documentation.Text text_show_the_individual_sentences_as_test_steps_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"show", "the", "individual", "sentences", "as", "test", "steps."}), paragraph_20);
		// Subsubsection - Validating Expectations using JUnit
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Validating_Expectations_using_JUnit = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Validating", "Expectations", "using", "JUnit"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// To validate expectations expressed in natural-language specifications you can
		de.devboost.natspec.library.documentation.Text text_To_validate_expectations_expressed_in_natural_language_specifications_you_can = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "validate", "expectations", "expressed", "in", "natural-language", "specifications", "you", "can"}), subsubsection_Validating_Expectations_using_JUnit);
		// simply use JUnit methods in test-support methods. To check the expectation:
		de.devboost.natspec.library.documentation.Text text_simply_use_JUnit_methods_in_test_support_methods__To_check_the_expectation_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"simply", "use", "JUnit", "methods", "in", "test-support", "methods.", "To", "check", "the", "expectation:"}), subsubsection_Validating_Expectations_using_JUnit);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_21 = documentationSupport.beginListing(subsubsection_Validating_Expectations_using_JUnit);
		// Assume LH-1234 has passenger John Doe
		de.devboost.natspec.library.documentation.Text text_Assume_LH_1234_has_passenger_John_Doe0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Assume", "LH-1234", "has", "passenger", "John", "Doe"}), listing_21);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_21 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsubsection_Validating_Expectations_using_JUnit);
		// you can write the following test support method:
		de.devboost.natspec.library.documentation.Text text_you_can_write_the_following_test_support_method_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"you", "can", "write", "the", "following", "test", "support", "method:"}), paragraph_21);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_22 = documentationSupport.beginListing(subsubsection_Validating_Expectations_using_JUnit);
		// @TextSyntax("Assume #1 has passenger #2")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Assume__1_has_passenger__2__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Assume", "#1", "has", "passenger", "#2\")"}), listing_22);
		// public void hasPassenger(Flight flight, Passenger passenger) {
		de.devboost.natspec.library.documentation.Text text_public_void_hasPassenger_Flight_flight__Passenger_passenger___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "hasPassenger(Flight", "flight,", "Passenger", "passenger)", "{"}), listing_22);
		// Assert.assertTrue(flight.hasPassenger(passenger));
		de.devboost.natspec.library.documentation.Text text_Assert_assertTrue_flight_hasPassenger_passenger___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Assert.assertTrue(flight.hasPassenger(passenger));"}), listing_22);
		// }
		de.devboost.natspec.library.documentation.Text text__21 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_22);
		// Subsubsection - Running NatSpec-based JUnit Tests
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Running_NatSpec_based_JUnit_Tests = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Running", "NatSpec-based", "JUnit", "Tests"}), subsection_Making_Sentences_Executable_using_Test_Support_Classes);
		// Every NatSpec file that uses a template containing an @Test method can be
		de.devboost.natspec.library.documentation.Text text_Every_NatSpec_file_that_uses_a_template_containing_an__Test_method_can_be = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Every", "NatSpec", "file", "that", "uses", "a", "template", "containing", "an", "@Test", "method", "can", "be"}), subsubsection_Running_NatSpec_based_JUnit_Tests);
		// launched as JUnit test. Just perform a right-click on the NatSpec file and
		de.devboost.natspec.library.documentation.Text text_launched_as_JUnit_test__Just_perform_a_right_click_on_the_NatSpec_file_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"launched", "as", "JUnit", "test.", "Just", "perform", "a", "right-click", "on", "the", "NatSpec", "file", "and"}), subsubsection_Running_NatSpec_based_JUnit_Tests);
		// select 'Run As > JUnit Test'.
		de.devboost.natspec.library.documentation.Text text_select__Run_As___JUnit_Test__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"select", "'Run", "As", ">", "JUnit", "Test'."}), subsubsection_Running_NatSpec_based_JUnit_Tests);
		// Subsection - Making Sentences Executable using Pattern Providers
		de.devboost.natspec.library.documentation.Subsection subsection_Making_Sentences_Executable_using_Pattern_Providers = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Making", "Sentences", "Executable", "using", "Pattern", "Providers"}), section_Guide_for_Busy_Developers);
		// For making natural specifications executable, test-support methods define
		de.devboost.natspec.library.documentation.Text text_For_making_natural_specifications_executable__test_support_methods_define = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"For", "making", "natural", "specifications", "executable,", "test-support", "methods", "define"}), subsection_Making_Sentences_Executable_using_Pattern_Providers);
		// patterns of sentences and their mapping to the Java code given in the method
		de.devboost.natspec.library.documentation.Text text_patterns_of_sentences_and_their_mapping_to_the_Java_code_given_in_the_method = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"patterns", "of", "sentences", "and", "their", "mapping", "to", "the", "Java", "code", "given", "in", "the", "method"}), subsection_Making_Sentences_Executable_using_Pattern_Providers);
		// body.
		de.devboost.natspec.library.documentation.Text text_body_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"body."}), subsection_Making_Sentences_Executable_using_Pattern_Providers);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_22 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Making_Sentences_Executable_using_Pattern_Providers);
		// When dealing with large APIs that follow consistent conventions, it is often
		de.devboost.natspec.library.documentation.Text text_When_dealing_with_large_APIs_that_follow_consistent_conventions__it_is_often = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"When", "dealing", "with", "large", "APIs", "that", "follow", "consistent", "conventions,", "it", "is", "often"}), paragraph_22);
		// sensible to create such patterns programmatically. A common example are entity
		de.devboost.natspec.library.documentation.Text text_sensible_to_create_such_patterns_programmatically__A_common_example_are_entity = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sensible", "to", "create", "such", "patterns", "programmatically.", "A", "common", "example", "are", "entity"}), paragraph_22);
		// classes as used in persistence layers or for data transfer objects. These
		de.devboost.natspec.library.documentation.Text text_classes_as_used_in_persistence_layers_or_for_data_transfer_objects__These = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"classes", "as", "used", "in", "persistence", "layers", "or", "for", "data", "transfer", "objects.", "These"}), paragraph_22);
		// often follow the JavaBeans convention for accessing fields and common
		de.devboost.natspec.library.documentation.Text text_often_follow_the_JavaBeans_convention_for_accessing_fields_and_common = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"often", "follow", "the", "JavaBeans", "convention", "for", "accessing", "fields", "and", "common"}), paragraph_22);
		// construction patterns like factories. It would cause lots of effort to manually
		de.devboost.natspec.library.documentation.Text text_construction_patterns_like_factories__It_would_cause_lots_of_effort_to_manually = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"construction", "patterns", "like", "factories.", "It", "would", "cause", "lots", "of", "effort", "to", "manually"}), paragraph_22);
		// implement test-support methods for all such entities.
		de.devboost.natspec.library.documentation.Text text_implement_test_support_methods_for_all_such_entities_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"implement", "test-support", "methods", "for", "all", "such", "entities."}), paragraph_22);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_23 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Making_Sentences_Executable_using_Pattern_Providers);
		// It is much more effective and consistent to define the mapping patterns for
		de.devboost.natspec.library.documentation.Text text_It_is_much_more_effective_and_consistent_to_define_the_mapping_patterns_for = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"It", "is", "much", "more", "effective", "and", "consistent", "to", "define", "the", "mapping", "patterns", "for"}), paragraph_23);
		// their getters, setters and constructors in a standardized way.
		de.devboost.natspec.library.documentation.Text text_their_getters__setters_and_constructors_in_a_standardized_way_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"their", "getters,", "setters", "and", "constructors", "in", "a", "standardized", "way."}), paragraph_23);
		// To do so, you can implement a custom
		de.devboost.natspec.library.documentation.Text text_To_do_so__you_can_implement_a_custom = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "do", "so,", "you", "can", "implement", "a", "custom"}), paragraph_23);
		// Code ISyntaxPatternProvider.
		de.devboost.natspec.library.documentation.Code code_ISyntaxPatternProvider_ = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPatternProvider.").toString(), paragraph_23);
		// For an example see the
		de.devboost.natspec.library.documentation.Text text_For_an_example_see_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"For", "an", "example", "see", "the"}), paragraph_23);
		// Code JavaCustomizationProvider
		de.devboost.natspec.library.documentation.Code code_JavaCustomizationProvider = documentationSupport.code(new java.lang.StringBuilder().append("JavaCustomizationProvider").toString(), paragraph_23);
		// in the project
		de.devboost.natspec.library.documentation.Text text_in_the_project = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "the", "project"}), paragraph_23);
		// Code de.devboost.natspec.java.customization.
		de.devboost.natspec.library.documentation.Code code_de_devboost_natspec_java_customization_ = documentationSupport.code(new java.lang.StringBuilder().append("de.devboost.natspec.java.customization.").toString(), paragraph_23);
		// It programmatically registers patterns for POJO entities following the JavaBeans
		de.devboost.natspec.library.documentation.Text text_It_programmatically_registers_patterns_for_POJO_entities_following_the_JavaBeans = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"It", "programmatically", "registers", "patterns", "for", "POJO", "entities", "following", "the", "JavaBeans"}), paragraph_23);
		// conventions.
		de.devboost.natspec.library.documentation.Text text_conventions_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"conventions."}), paragraph_23);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_24 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Making_Sentences_Executable_using_Pattern_Providers);
		// Important note: Make sure you're implementing the pattern provider in an Eclipse
		de.devboost.natspec.library.documentation.Text text_Important_note__Make_sure_you_re_implementing_the_pattern_provider_in_an_Eclipse = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Important", "note:", "Make", "sure", "you're", "implementing", "the", "pattern", "provider", "in", "an", "Eclipse"}), paragraph_24);
		// plug-in project (i.e., an OSGi bundle). This is required to allow NatSpec to
		de.devboost.natspec.library.documentation.Text text_plug_in_project__i_e___an_OSGi_bundle___This_is_required_to_allow_NatSpec_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"plug-in", "project", "(i.e.,", "an", "OSGi", "bundle).", "This", "is", "required", "to", "allow", "NatSpec", "to"}), paragraph_24);
		// reload your pattern provider class dynamically without restarting your Eclipse
		de.devboost.natspec.library.documentation.Text text_reload_your_pattern_provider_class_dynamically_without_restarting_your_Eclipse = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"reload", "your", "pattern", "provider", "class", "dynamically", "without", "restarting", "your", "Eclipse"}), paragraph_24);
		// instance. Thus you can make changes to the pattern provider code and instantly
		de.devboost.natspec.library.documentation.Text text_instance__Thus_you_can_make_changes_to_the_pattern_provider_code_and_instantly = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"instance.", "Thus", "you", "can", "make", "changes", "to", "the", "pattern", "provider", "code", "and", "instantly"}), paragraph_24);
		// see the resulting changes.
		de.devboost.natspec.library.documentation.Text text_see_the_resulting_changes_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"see", "the", "resulting", "changes."}), paragraph_24);
		// To get a more thorough understanding of how to implement pattern providers see
		de.devboost.natspec.library.documentation.Text text_To_get_a_more_thorough_understanding_of_how_to_implement_pattern_providers_see = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "get", "a", "more", "thorough", "understanding", "of", "how", "to", "implement", "pattern", "providers", "see"}), paragraph_24);
		// Subsection - Dealing with Synonyms
		de.devboost.natspec.library.documentation.Subsection subsection_Dealing_with_Synonyms = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Dealing", "with", "Synonyms"}), section_Guide_for_Busy_Developers);
		// Natural languages are full of synonyms. Instead of writing
		de.devboost.natspec.library.documentation.Text text_Natural_languages_are_full_of_synonyms__Instead_of_writing = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Natural", "languages", "are", "full", "of", "synonyms.", "Instead", "of", "writing"}), subsection_Dealing_with_Synonyms);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_23 = documentationSupport.beginListing(subsection_Dealing_with_Synonyms);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_6001 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_23);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_25 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Dealing_with_Synonyms);
		// you may also want to write
		de.devboost.natspec.library.documentation.Text text_you_may_also_want_to_write = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"you", "may", "also", "want", "to", "write"}), paragraph_25);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_24 = documentationSupport.beginListing(subsection_Dealing_with_Synonyms);
		// Create plane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_plane_Boeing_737_600 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "plane", "Boeing-737-600"}), listing_24);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_26 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Dealing_with_Synonyms);
		// To support synonyms you just need to create a file called
		de.devboost.natspec.library.documentation.Text text_To_support_synonyms_you_just_need_to_create_a_file_called = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "support", "synonyms", "you", "just", "need", "to", "create", "a", "file", "called"}), paragraph_26);
		// Code synonyms.txt
		de.devboost.natspec.library.documentation.Code code_synonyms_txt = documentationSupport.code(new java.lang.StringBuilder().append("synonyms.txt").toString(), paragraph_26);
		// in your test project and add lines of comma-separated synonyms. The following
		de.devboost.natspec.library.documentation.Text text_in_your_test_project_and_add_lines_of_comma_separated_synonyms__The_following = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "your", "test", "project", "and", "add", "lines", "of", "comma-separated", "synonyms.", "The", "following"}), paragraph_26);
		// listing gives synonyms for
		de.devboost.natspec.library.documentation.Text text_listing_gives_synonyms_for = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"listing", "gives", "synonyms", "for"}), paragraph_26);
		// Code airplane
		de.devboost.natspec.library.documentation.Code code_airplane = documentationSupport.code(new java.lang.StringBuilder().append("airplane").toString(), paragraph_26);
		// ,
		de.devboost.natspec.library.documentation.Text text__22 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {","}), paragraph_26);
		// Code passenger
		de.devboost.natspec.library.documentation.Code code_passenger0 = documentationSupport.code(new java.lang.StringBuilder().append("passenger").toString(), paragraph_26);
		// , and
		de.devboost.natspec.library.documentation.Text text___and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {",", "and"}), paragraph_26);
		// Code flight
		de.devboost.natspec.library.documentation.Code code_flight0 = documentationSupport.code(new java.lang.StringBuilder().append("flight").toString(), paragraph_26);
		// .
		de.devboost.natspec.library.documentation.Text text__23 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), paragraph_26);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_25 = documentationSupport.beginListing(subsection_Dealing_with_Synonyms);
		// airplanetype,airplane,plane,aircraft
		de.devboost.natspec.library.documentation.Text text_airplanetype_airplane_plane_aircraft = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"airplanetype,airplane,plane,aircraft"}), listing_25);
		// passenger,traveler
		de.devboost.natspec.library.documentation.Text text_passenger_traveler = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"passenger,traveler"}), listing_25);
		// flight,travel
		de.devboost.natspec.library.documentation.Text text_flight_travel = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"flight,travel"}), listing_25);
		// Subsection - Documenting NatSpec Scenarios
		de.devboost.natspec.library.documentation.Subsection subsection_Documenting_NatSpec_Scenarios = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Documenting", "NatSpec", "Scenarios"}), section_Guide_for_Busy_Developers);
		// Even so we're using natural language, some scenarios or sentences need
		de.devboost.natspec.library.documentation.Text text_Even_so_we_re_using_natural_language__some_scenarios_or_sentences_need = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Even", "so", "we're", "using", "natural", "language,", "some", "scenarios", "or", "sentences", "need"}), subsection_Documenting_NatSpec_Scenarios);
		// additional documentation. NatSpec provides means for documenting scenarios on
		de.devboost.natspec.library.documentation.Text text_additional_documentation__NatSpec_provides_means_for_documenting_scenarios_on = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"additional", "documentation.", "NatSpec", "provides", "means", "for", "documenting", "scenarios", "on"}), subsection_Documenting_NatSpec_Scenarios);
		// different levels of granularity, ranging from documenting a single
		de.devboost.natspec.library.documentation.Text text_different_levels_of_granularity__ranging_from_documenting_a_single = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"different", "levels", "of", "granularity,", "ranging", "from", "documenting", "a", "single"}), subsection_Documenting_NatSpec_Scenarios);
		// sentence to top-level scenario documentation.
		de.devboost.natspec.library.documentation.Text text_sentence_to_top_level_scenario_documentation_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sentence", "to", "top-level", "scenario", "documentation."}), subsection_Documenting_NatSpec_Scenarios);
		// Subsubsection - Using Comments to Document Sentences
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Using_Comments_to_Document_Sentences = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Using", "Comments", "to", "Document", "Sentences"}), subsection_Documenting_NatSpec_Scenarios);
		// Comments can be used to document sentences in NatSpec scenarios. The character
		de.devboost.natspec.library.documentation.Text text_Comments_can_be_used_to_document_sentences_in_NatSpec_scenarios__The_character = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Comments", "can", "be", "used", "to", "document", "sentences", "in", "NatSpec", "scenarios.", "The", "character"}), subsubsection_Using_Comments_to_Document_Sentences);
		// sequence to denote standard comments is two forward slashes followed by the
		de.devboost.natspec.library.documentation.Text text_sequence_to_denote_standard_comments_is_two_forward_slashes_followed_by_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sequence", "to", "denote", "standard", "comments", "is", "two", "forward", "slashes", "followed", "by", "the"}), subsubsection_Using_Comments_to_Document_Sentences);
		// comment text. For an example, see the following specification:
		de.devboost.natspec.library.documentation.Text text_comment_text__For_an_example__see_the_following_specification_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"comment", "text.", "For", "an", "example,", "see", "the", "following", "specification:"}), subsubsection_Using_Comments_to_Document_Sentences);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_26 = documentationSupport.beginListing(subsubsection_Using_Comments_to_Document_Sentences);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_6002 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_26);
		// ## We need to set the total seats so that tickets can be issued
		de.devboost.natspec.library.documentation.Text text____We_need_to_set_the_total_seats_so_that_tickets_can_be_issued = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "We", "need", "to", "set", "the", "total", "seats", "so", "that", "tickets", "can", "be", "issued"}), listing_26);
		// Set totalSeats to 2
		de.devboost.natspec.library.documentation.Text text_Set_totalSeats_to_21 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "totalSeats", "to", "2"}), listing_26);
		// Subsubsection - Using Comments to Define Markers in Scenarios
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Using_Comments_to_Define_Markers_in_Scenarios = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Using", "Comments", "to", "Define", "Markers", "in", "Scenarios"}), subsection_Documenting_NatSpec_Scenarios);
		// Markers in scenarios are a feature to define sections in scenarios.
		de.devboost.natspec.library.documentation.Text text_Markers_in_scenarios_are_a_feature_to_define_sections_in_scenarios_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Markers", "in", "scenarios", "are", "a", "feature", "to", "define", "sections", "in", "scenarios."}), subsubsection_Using_Comments_to_Define_Markers_in_Scenarios);
		// The character sequence to denote markers is three forward slashes followed by
		de.devboost.natspec.library.documentation.Text text_The_character_sequence_to_denote_markers_is_three_forward_slashes_followed_by = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "character", "sequence", "to", "denote", "markers", "is", "three", "forward", "slashes", "followed", "by"}), subsubsection_Using_Comments_to_Define_Markers_in_Scenarios);
		// the marker text. The outline view can then be configured to only show those
		de.devboost.natspec.library.documentation.Text text_the_marker_text__The_outline_view_can_then_be_configured_to_only_show_those = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "marker", "text.", "The", "outline", "view", "can", "then", "be", "configured", "to", "only", "show", "those"}), subsubsection_Using_Comments_to_Define_Markers_in_Scenarios);
		// markers instead of entries for all sentences in the scenario.
		de.devboost.natspec.library.documentation.Text text_markers_instead_of_entries_for_all_sentences_in_the_scenario_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"markers", "instead", "of", "entries", "for", "all", "sentences", "in", "the", "scenario."}), subsubsection_Using_Comments_to_Define_Markers_in_Scenarios);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_27 = documentationSupport.beginListing(subsubsection_Using_Comments_to_Define_Markers_in_Scenarios);
		// ### Initialization
		de.devboost.natspec.library.documentation.Text text_____Initialization = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"###", "Initialization"}), listing_27);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_6003 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_27);
		// ## We need to set the total seats so that tickets can be issued
		de.devboost.natspec.library.documentation.Text text____We_need_to_set_the_total_seats_so_that_tickets_can_be_issued0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "We", "need", "to", "set", "the", "total", "seats", "so", "that", "tickets", "can", "be", "issued"}), listing_27);
		// Set totalSeats to 2
		de.devboost.natspec.library.documentation.Text text_Set_totalSeats_to_22 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "totalSeats", "to", "2"}), listing_27);
		// Subsubsection - Using Comments With Task Tags to Denote TODOs and FIXMEs
		de.devboost.natspec.library.documentation.Subsubsection subsubsection_Using_Comments_With_Task_Tags_to_Denote_TODOs_and_FIXMEs = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"Using", "Comments", "With", "Task", "Tags", "to", "Denote", "TODOs", "and", "FIXMEs"}), subsection_Documenting_NatSpec_Scenarios);
		// Task tags, i.e., TODO and FIXME can also be used in standard comments to denote
		de.devboost.natspec.library.documentation.Text text_Task_tags__i_e___TODO_and_FIXME_can_also_be_used_in_standard_comments_to_denote = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Task", "tags,", "i.e.,", "TODO", "and", "FIXME", "can", "also", "be", "used", "in", "standard", "comments", "to", "denote"}), subsubsection_Using_Comments_With_Task_Tags_to_Denote_TODOs_and_FIXMEs);
		// tasks that need to be listed in the Tasks view.
		de.devboost.natspec.library.documentation.Text text_tasks_that_need_to_be_listed_in_the_Tasks_view_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"tasks", "that", "need", "to", "be", "listed", "in", "the", "Tasks", "view."}), subsubsection_Using_Comments_With_Task_Tags_to_Denote_TODOs_and_FIXMEs);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_28 = documentationSupport.beginListing(subsubsection_Using_Comments_With_Task_Tags_to_Denote_TODOs_and_FIXMEs);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_6004 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_28);
		// ## TODO We need to initialize the airplane seats
		de.devboost.natspec.library.documentation.Text text____TODO_We_need_to_initialize_the_airplane_seats = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"##", "TODO", "We", "need", "to", "initialize", "the", "airplane", "seats"}), listing_28);
		// Subsection - Preserving the Implicit Contexts within a Specification
		de.devboost.natspec.library.documentation.Subsection subsection_Preserving_the_Implicit_Contexts_within_a_Specification = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Preserving", "the", "Implicit", "Contexts", "within", "a", "Specification"}), section_Guide_for_Busy_Developers);
		// In natural language we typically use implicit contexts in a sequence of
		de.devboost.natspec.library.documentation.Text text_In_natural_language_we_typically_use_implicit_contexts_in_a_sequence_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"In", "natural", "language", "we", "typically", "use", "implicit", "contexts", "in", "a", "sequence", "of"}), subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// several sentences. For an example, see the following specification:
		de.devboost.natspec.library.documentation.Text text_several_sentences__For_an_example__see_the_following_specification_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"several", "sentences.", "For", "an", "example,", "see", "the", "following", "specification:"}), subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_29 = documentationSupport.beginListing(subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_6005 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_29);
		// Set totalSeats to 2
		de.devboost.natspec.library.documentation.Text text_Set_totalSeats_to_23 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "totalSeats", "to", "2"}), listing_29);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_27 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// The second sentence implicitly refers to the airplane created in the previous
		de.devboost.natspec.library.documentation.Text text_The_second_sentence_implicitly_refers_to_the_airplane_created_in_the_previous = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "second", "sentence", "implicitly", "refers", "to", "the", "airplane", "created", "in", "the", "previous"}), paragraph_27);
		// sentence. NatSpec does also support this kind of context preservation. For
		de.devboost.natspec.library.documentation.Text text_sentence__NatSpec_does_also_support_this_kind_of_context_preservation__For = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sentence.", "NatSpec", "does", "also", "support", "this", "kind", "of", "context", "preservation.", "For"}), paragraph_27);
		// the second template you would define a test-support method like:
		de.devboost.natspec.library.documentation.Text text_the_second_template_you_would_define_a_test_support_method_like_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "second", "template", "you", "would", "define", "a", "test-support", "method", "like:"}), paragraph_27);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_30 = documentationSupport.beginListing(subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// @TextSyntax("Set totalSeats to #2")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Set_totalSeats_to__2__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Set", "totalSeats", "to", "#2\")"}), listing_30);
		// public void setTotalSeats(AirplaneType airplane, int seats) {
		de.devboost.natspec.library.documentation.Text text_public_void_setTotalSeats_AirplaneType_airplane__int_seats___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "setTotalSeats(AirplaneType", "airplane,", "int", "seats)", "{"}), listing_30);
		// airplane.setTotalSeats(seats);
		de.devboost.natspec.library.documentation.Text text_airplane_setTotalSeats_seats__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"airplane.setTotalSeats(seats);"}), listing_30);
		// }
		de.devboost.natspec.library.documentation.Text text__24 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_30);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_28 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// As you can see, there is no placeholder bound to the first method parameter
		de.devboost.natspec.library.documentation.Text text_As_you_can_see__there_is_no_placeholder_bound_to_the_first_method_parameter = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "you", "can", "see,", "there", "is", "no", "placeholder", "bound", "to", "the", "first", "method", "parameter"}), paragraph_28);
		// Code airplane
		de.devboost.natspec.library.documentation.Code code_airplane0 = documentationSupport.code(new java.lang.StringBuilder().append("airplane").toString(), paragraph_28);
		// . To determine the value for unbound parameters,
		de.devboost.natspec.library.documentation.Text text___To_determine_the_value_for_unbound_parameters_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "To", "determine", "the", "value", "for", "unbound", "parameters,"}), paragraph_28);
		// NatSpec investigates the sentence context for matching objects.
		de.devboost.natspec.library.documentation.Text text_NatSpec_investigates_the_sentence_context_for_matching_objects_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "investigates", "the", "sentence", "context", "for", "matching", "objects."}), paragraph_28);
		// Therefore, it inspects all objects that were a result of the previous
		de.devboost.natspec.library.documentation.Text text_Therefore__it_inspects_all_objects_that_were_a_result_of_the_previous = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Therefore,", "it", "inspects", "all", "objects", "that", "were", "a", "result", "of", "the", "previous"}), paragraph_28);
		// sentences in the specification and uses the latest result object with a
		de.devboost.natspec.library.documentation.Text text_sentences_in_the_specification_and_uses_the_latest_result_object_with_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sentences", "in", "the", "specification", "and", "uses", "the", "latest", "result", "object", "with", "a"}), paragraph_28);
		// matching type.
		de.devboost.natspec.library.documentation.Text text_matching_type_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"matching", "type."}), paragraph_28);
		// In the following example NatSpec would use the airplane Boeing-737-700 as
		de.devboost.natspec.library.documentation.Text text_In_the_following_example_NatSpec_would_use_the_airplane_Boeing_737_700_as = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"In", "the", "following", "example", "NatSpec", "would", "use", "the", "airplane", "Boeing-737-700", "as"}), paragraph_28);
		// parameter for calling
		de.devboost.natspec.library.documentation.Text text_parameter_for_calling = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"parameter", "for", "calling"}), paragraph_28);
		// Code setTotalSeats()
		de.devboost.natspec.library.documentation.Code code_setTotalSeats__ = documentationSupport.code(new java.lang.StringBuilder().append("setTotalSeats()").toString(), paragraph_28);
		// .
		de.devboost.natspec.library.documentation.Text text__25 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), paragraph_28);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_31 = documentationSupport.beginListing(subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// Create airplane Boeing-737-600
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_6006 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-600"}), listing_31);
		// Create airplane Boeing-737-700
		de.devboost.natspec.library.documentation.Text text_Create_airplane_Boeing_737_700 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Create", "airplane", "Boeing-737-700"}), listing_31);
		// Set totalSeats to 2
		de.devboost.natspec.library.documentation.Text text_Set_totalSeats_to_24 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Set", "totalSeats", "to", "2"}), listing_31);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_29 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// The result object of a sentence is determined by the value that is returned
		de.devboost.natspec.library.documentation.Text text_The_result_object_of_a_sentence_is_determined_by_the_value_that_is_returned = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "result", "object", "of", "a", "sentence", "is", "determined", "by", "the", "value", "that", "is", "returned"}), paragraph_29);
		// by the corresponding test-support method. The support method for the
		de.devboost.natspec.library.documentation.Text text_by_the_corresponding_test_support_method__The_support_method_for_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"by", "the", "corresponding", "test-support", "method.", "The", "support", "method", "for", "the"}), paragraph_29);
		// first two sentences looks as follows:
		de.devboost.natspec.library.documentation.Text text_first_two_sentences_looks_as_follows_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"first", "two", "sentences", "looks", "as", "follows:"}), paragraph_29);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_32 = documentationSupport.beginListing(subsection_Preserving_the_Implicit_Contexts_within_a_Specification);
		// @TextSyntax("Create airplane #1")
		de.devboost.natspec.library.documentation.Text text__TextSyntax__Create_airplane__1__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@TextSyntax(\"Create", "airplane", "#1\")"}), listing_32);
		// public AirplaneType createAirplaneType(String typeName) {
		de.devboost.natspec.library.documentation.Text text_public_AirplaneType_createAirplaneType_String_typeName___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "AirplaneType", "createAirplaneType(String", "typeName)", "{"}), listing_32);
		// return new AirplaneType(typeName);
		de.devboost.natspec.library.documentation.Text text_return_new_AirplaneType_typeName__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"return", "new", "AirplaneType(typeName);"}), listing_32);
		// }
		de.devboost.natspec.library.documentation.Text text__26 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_32);
		// Subsection - Generating and Running JUnit Test Cases
		de.devboost.natspec.library.documentation.Subsection subsection_Generating_and_Running_JUnit_Test_Cases = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Generating", "and", "Running", "JUnit", "Test", "Cases"}), section_Guide_for_Busy_Developers);
		// NatSpec automatically generates JUnit test cases whenever you save
		de.devboost.natspec.library.documentation.Text text_NatSpec_automatically_generates_JUnit_test_cases_whenever_you_save = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "automatically", "generates", "JUnit", "test", "cases", "whenever", "you", "save"}), subsection_Generating_and_Running_JUnit_Test_Cases);
		// a NatSpec specification file.
		de.devboost.natspec.library.documentation.Text text_a_NatSpec_specification_file_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"a", "NatSpec", "specification", "file."}), subsection_Generating_and_Running_JUnit_Test_Cases);
		// Generation is based on the mappings you define using test-support classes and
		de.devboost.natspec.library.documentation.Text text_Generation_is_based_on_the_mappings_you_define_using_test_support_classes_and = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Generation", "is", "based", "on", "the", "mappings", "you", "define", "using", "test-support", "classes", "and"}), subsection_Generating_and_Running_JUnit_Test_Cases);
		// pattern providers. For every NatSpec file a corresponding Java class having the same
		de.devboost.natspec.library.documentation.Text text_pattern_providers__For_every_NatSpec_file_a_corresponding_Java_class_having_the_same = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"pattern", "providers.", "For", "every", "NatSpec", "file", "a", "corresponding", "Java", "class", "having", "the", "same"}), subsection_Generating_and_Running_JUnit_Test_Cases);
		// name and being located in the same package will
		de.devboost.natspec.library.documentation.Text text_name_and_being_located_in_the_same_package_will = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"name", "and", "being", "located", "in", "the", "same", "package", "will"}), subsection_Generating_and_Running_JUnit_Test_Cases);
		// be generated.
		de.devboost.natspec.library.documentation.Text text_be_generated_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"be", "generated."}), subsection_Generating_and_Running_JUnit_Test_Cases);
		// You can execute the test case like any other JUnit test:
		de.devboost.natspec.library.documentation.Text text_You_can_execute_the_test_case_like_any_other_JUnit_test_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"You", "can", "execute", "the", "test", "case", "like", "any", "other", "JUnit", "test:"}), subsection_Generating_and_Running_JUnit_Test_Cases);
		// Code Run > Run as > JUnit Test
		de.devboost.natspec.library.documentation.Code code_Run___Run_as___JUnit_Test = documentationSupport.code(new java.lang.StringBuilder().append("Run").append(" ").append(">").append(" ").append("Run").append(" ").append("as").append(" ").append(">").append(" ").append("JUnit").append(" ").append("Test").toString(), subsection_Generating_and_Running_JUnit_Test_Cases);
		// Subsection - Setting the Target Folder for Generated Classes
		de.devboost.natspec.library.documentation.Subsection subsection_Setting_the_Target_Folder_for_Generated_Classes = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Setting", "the", "Target", "Folder", "for", "Generated", "Classes"}), section_Guide_for_Busy_Developers);
		// By default, NatSpec writes the generated classes to the same folder the NatSpec
		de.devboost.natspec.library.documentation.Text text_By_default__NatSpec_writes_the_generated_classes_to_the_same_folder_the_NatSpec = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"By", "default,", "NatSpec", "writes", "the", "generated", "classes", "to", "the", "same", "folder", "the", "NatSpec"}), subsection_Setting_the_Target_Folder_for_Generated_Classes);
		// specification is contained in. This behavior can be adjusted by setting the
		de.devboost.natspec.library.documentation.Text text_specification_is_contained_in__This_behavior_can_be_adjusted_by_setting_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"specification", "is", "contained", "in.", "This", "behavior", "can", "be", "adjusted", "by", "setting", "the"}), subsection_Setting_the_Target_Folder_for_Generated_Classes);
		// NatSpec project property 'Output folder'. This property can be set by invoking
		de.devboost.natspec.library.documentation.Text text_NatSpec_project_property__Output_folder___This_property_can_be_set_by_invoking = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "project", "property", "'Output", "folder'.", "This", "property", "can", "be", "set", "by", "invoking"}), subsection_Setting_the_Target_Folder_for_Generated_Classes);
		// the context menu for a project and selecting the 'Properties' menu item. The
		de.devboost.natspec.library.documentation.Text text_the_context_menu_for_a_project_and_selecting_the__Properties__menu_item__The = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "context", "menu", "for", "a", "project", "and", "selecting", "the", "'Properties'", "menu", "item.", "The"}), subsection_Setting_the_Target_Folder_for_Generated_Classes);
		// NatSpec property page can then be used to set either a relative or an absolute
		de.devboost.natspec.library.documentation.Text text_NatSpec_property_page_can_then_be_used_to_set_either_a_relative_or_an_absolute = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "property", "page", "can", "then", "be", "used", "to", "set", "either", "a", "relative", "or", "an", "absolute"}), subsection_Setting_the_Target_Folder_for_Generated_Classes);
		// path (w.r.t. the project root) where to store the generated classes.
		de.devboost.natspec.library.documentation.Text text_path__w_r_t__the_project_root__where_to_store_the_generated_classes_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"path", "(w.r.t.", "the", "project", "root)", "where", "to", "store", "the", "generated", "classes."}), subsection_Setting_the_Target_Folder_for_Generated_Classes);
		// Subsection - Initializing Infrastructure for JUnit Test Cases
		de.devboost.natspec.library.documentation.Subsection subsection_Initializing_Infrastructure_for_JUnit_Test_Cases = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Initializing", "Infrastructure", "for", "JUnit", "Test", "Cases"}), section_Guide_for_Busy_Developers);
		// As test cases often require some infrastructure, e.g., to access the
		de.devboost.natspec.library.documentation.Text text_As_test_cases_often_require_some_infrastructure__e_g___to_access_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "test", "cases", "often", "require", "some", "infrastructure,", "e.g.,", "to", "access", "the"}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// database layer or the business services to test, you can influence how test
		de.devboost.natspec.library.documentation.Text text_database_layer_or_the_business_services_to_test__you_can_influence_how_test = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"database", "layer", "or", "the", "business", "services", "to", "test,", "you", "can", "influence", "how", "test"}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// cases are generated using test-case templates.
		de.devboost.natspec.library.documentation.Text text_cases_are_generated_using_test_case_templates_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"cases", "are", "generated", "using", "test-case", "templates."}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// A test-case template is a Java class named
		de.devboost.natspec.library.documentation.Text text_A_test_case_template_is_a_Java_class_named = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"A", "test-case", "template", "is", "a", "Java", "class", "named"}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// Code _NatSpecTemplate.java
		de.devboost.natspec.library.documentation.Code code__NatSpecTemplate_java = documentationSupport.code(new java.lang.StringBuilder().append("_NatSpecTemplate.java").toString(), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// (Note the underscore!) that you place next to your NatSpec specifications.
		de.devboost.natspec.library.documentation.Text text__Note_the_underscore___that_you_place_next_to_your_NatSpec_specifications_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"(Note", "the", "underscore!)", "that", "you", "place", "next", "to", "your", "NatSpec", "specifications."}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// Test-case templates can be placed in the same folder as the NatSpec file or any
		de.devboost.natspec.library.documentation.Text text_Test_case_templates_can_be_placed_in_the_same_folder_as_the_NatSpec_file_or_any = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Test-case", "templates", "can", "be", "placed", "in", "the", "same", "folder", "as", "the", "NatSpec", "file", "or", "any"}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// parent folder. The first template that is found when ascending the folder
		de.devboost.natspec.library.documentation.Text text_parent_folder__The_first_template_that_is_found_when_ascending_the_folder = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"parent", "folder.", "The", "first", "template", "that", "is", "found", "when", "ascending", "the", "folder"}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// hierarchy is used for each NatSpec test.
		de.devboost.natspec.library.documentation.Text text_hierarchy_is_used_for_each_NatSpec_test_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"hierarchy", "is", "used", "for", "each", "NatSpec", "test."}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_30 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// An exemplary test-case template is shown below:
		de.devboost.natspec.library.documentation.Text text_An_exemplary_test_case_template_is_shown_below_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"An", "exemplary", "test-case", "template", "is", "shown", "below:"}), paragraph_30);
		// Listing
		de.devboost.natspec.library.documentation.Listing listing_33 = documentationSupport.beginListing(subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// package com.yourdomain;
		de.devboost.natspec.library.documentation.Text text_package_com_yourdomain_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"package", "com.yourdomain;"}), listing_33);
		// import static org.junit.Assert.*;
		de.devboost.natspec.library.documentation.Text text_import_static_org_junit_Assert___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "static", "org.junit.Assert.*;"}), listing_33);
		// import org.junit.Before;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_Before_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.Before;"}), listing_33);
		// import org.junit.Test;
		de.devboost.natspec.library.documentation.Text text_import_org_junit_Test_1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "org.junit.Test;"}), listing_33);
		// import de.devboost.natspec.examples.airline.InMemoryPersistenceContext;
		de.devboost.natspec.library.documentation.Text text_import_de_devboost_natspec_examples_airline_InMemoryPersistenceContext_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "de.devboost.natspec.examples.airline.InMemoryPersistenceContext;"}), listing_33);
		// import de.devboost.natspec.examples.airline.services.AirlineServices;
		de.devboost.natspec.library.documentation.Text text_import_de_devboost_natspec_examples_airline_services_AirlineServices_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"import", "de.devboost.natspec.examples.airline.services.AirlineServices;"}), listing_33);
		// /* @Imports */
		de.devboost.natspec.library.documentation.Text text_____Imports___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "@Imports", "*/"}), listing_33);
		// public class _NatSpecTemplate {
		de.devboost.natspec.library.documentation.Text text_public_class__NatSpecTemplate__0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "class", "_NatSpecTemplate", "{"}), listing_33);
		// private AirlineServices services;
		de.devboost.natspec.library.documentation.Text text_private_AirlineServices_services_0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"private", "AirlineServices", "services;"}), listing_33);
		// private TestSupport testSupport;
		de.devboost.natspec.library.documentation.Text text_private_TestSupport_testSupport_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"private", "TestSupport", "testSupport;"}), listing_33);
		// private InMemoryPersistenceContext persistenceContext;
		de.devboost.natspec.library.documentation.Text text_private_InMemoryPersistenceContext_persistenceContext_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"private", "InMemoryPersistenceContext", "persistenceContext;"}), listing_33);
		// @Test
		de.devboost.natspec.library.documentation.Text text__Test1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Test"}), listing_33);
		// public void executeScript() throws Exception {
		de.devboost.natspec.library.documentation.Text text_public_void_executeScript___throws_Exception__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "executeScript()", "throws", "Exception", "{"}), listing_33);
		// /* @MethodBody */
		de.devboost.natspec.library.documentation.Text text_____MethodBody___1 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"/*", "@MethodBody", "*/"}), listing_33);
		// }
		de.devboost.natspec.library.documentation.Text text__27 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_33);
		// @Before
		de.devboost.natspec.library.documentation.Text text__Before = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"@Before"}), listing_33);
		// public void setUp() {
		de.devboost.natspec.library.documentation.Text text_public_void_setUp____ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"public", "void", "setUp()", "{"}), listing_33);
		// services = new AirlineServices();
		de.devboost.natspec.library.documentation.Text text_services___new_AirlineServices___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"services", "=", "new", "AirlineServices();"}), listing_33);
		// testSupport = new TestSupport(services);
		de.devboost.natspec.library.documentation.Text text_testSupport___new_TestSupport_services__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"testSupport", "=", "new", "TestSupport(services);"}), listing_33);
		// persistenceContext =
		de.devboost.natspec.library.documentation.Text text_persistenceContext__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"persistenceContext", "="}), listing_33);
		//  InMemoryPersistenceContext.getPersistenceContext();
		de.devboost.natspec.library.documentation.Text text__InMemoryPersistenceContext_getPersistenceContext___ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"", "InMemoryPersistenceContext.getPersistenceContext();"}), listing_33);
		// }
		de.devboost.natspec.library.documentation.Text text__28 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_33);
		// }
		de.devboost.natspec.library.documentation.Text text__29 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"}"}), listing_33);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_31 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// As the test-case template is a Java class you can use plain Java code to
		de.devboost.natspec.library.documentation.Text text_As_the_test_case_template_is_a_Java_class_you_can_use_plain_Java_code_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"As", "the", "test-case", "template", "is", "a", "Java", "class", "you", "can", "use", "plain", "Java", "code", "to"}), paragraph_31);
		// initialize any infrastructure required for executing the test. You can
		de.devboost.natspec.library.documentation.Text text_initialize_any_infrastructure_required_for_executing_the_test__You_can = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"initialize", "any", "infrastructure", "required", "for", "executing", "the", "test.", "You", "can"}), paragraph_31);
		// also simply use JUnit annotations like
		de.devboost.natspec.library.documentation.Text text_also_simply_use_JUnit_annotations_like = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"also", "simply", "use", "JUnit", "annotations", "like"}), paragraph_31);
		// Code @Before
		de.devboost.natspec.library.documentation.Code code__Before = documentationSupport.code(new java.lang.StringBuilder().append("@Before").toString(), paragraph_31);
		// or
		de.devboost.natspec.library.documentation.Text text_or = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"or"}), paragraph_31);
		// Code @After
		de.devboost.natspec.library.documentation.Code code__After = documentationSupport.code(new java.lang.StringBuilder().append("@After").toString(), paragraph_31);
		// for set up and tear down activities.
		de.devboost.natspec.library.documentation.Text text_for_set_up_and_tear_down_activities_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"for", "set", "up", "and", "tear", "down", "activities."}), paragraph_31);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_32 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// The template also uses two placeholders to indicate a place in the template that will be replaced with generated code:
		de.devboost.natspec.library.documentation.Text text_The_template_also_uses_two_placeholders_to_indicate_a_place_in_the_template_that_will_be_replaced_with_generated_code_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The", "template", "also", "uses", "two", "placeholders", "to", "indicate", "a", "place", "in", "the", "template", "that", "will", "be", "replaced", "with", "generated", "code:"}), paragraph_32);
		// Subsubsection - @MethodBody (required)
		de.devboost.natspec.library.documentation.Subsubsection subsubsection__MethodBody__required_ = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"@MethodBody", "(required)"}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// The
		de.devboost.natspec.library.documentation.Text text_The = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The"}), subsubsection__MethodBody__required_);
		// Code /* @MethodBody */
		de.devboost.natspec.library.documentation.Code code_____MethodBody___ = documentationSupport.code(new java.lang.StringBuilder().append("/*").append(" ").append("@MethodBody").append(" ").append("*/").toString(), subsubsection__MethodBody__required_);
		// placeholder will be replaced with the code generated from a specific NatSpec specification.
		de.devboost.natspec.library.documentation.Text text_placeholder_will_be_replaced_with_the_code_generated_from_a_specific_NatSpec_specification_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"placeholder", "will", "be", "replaced", "with", "the", "code", "generated", "from", "a", "specific", "NatSpec", "specification."}), subsubsection__MethodBody__required_);
		// Subsubsection - @Imports (optional)
		de.devboost.natspec.library.documentation.Subsubsection subsubsection__Imports__optional_ = documentationSupport.addSubsubsection(java.util.Arrays.asList(new java.lang.String[] {"@Imports", "(optional)"}), subsection_Initializing_Infrastructure_for_JUnit_Test_Cases);
		// The
		de.devboost.natspec.library.documentation.Text text_The0 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"The"}), subsubsection__Imports__optional_);
		// Code /* @Imports */
		de.devboost.natspec.library.documentation.Code code_____Imports___ = documentationSupport.code(new java.lang.StringBuilder().append("/*").append(" ").append("@Imports").append(" ").append("*/").toString(), subsubsection__Imports__optional_);
		// placeholder will be replaced with imports, which are generated from a specific NatSpec specification.
		de.devboost.natspec.library.documentation.Text text_placeholder_will_be_replaced_with_imports__which_are_generated_from_a_specific_NatSpec_specification_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"placeholder", "will", "be", "replaced", "with", "imports,", "which", "are", "generated", "from", "a", "specific", "NatSpec", "specification."}), subsubsection__Imports__optional_);
		// If not provided, imports will be generated before the imports which are already present.
		de.devboost.natspec.library.documentation.Text text_If_not_provided__imports_will_be_generated_before_the_imports_which_are_already_present_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"If", "not", "provided,", "imports", "will", "be", "generated", "before", "the", "imports", "which", "are", "already", "present."}), subsubsection__Imports__optional_);
		// Section - NatSpec API Guide
		de.devboost.natspec.library.documentation.Section section_NatSpec_API_Guide = documentationSupport.addSection(java.util.Arrays.asList(new java.lang.String[] {"NatSpec", "API", "Guide"}), documentation_NatSpec_User_Guide);
		// NatSpec provides an API to allow expert users to programmatically create syntax
		de.devboost.natspec.library.documentation.Text text_NatSpec_provides_an_API_to_allow_expert_users_to_programmatically_create_syntax = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"NatSpec", "provides", "an", "API", "to", "allow", "expert", "users", "to", "programmatically", "create", "syntax"}), section_NatSpec_API_Guide);
		// patterns without the need for
		de.devboost.natspec.library.documentation.Text text_patterns_without_the_need_for = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"patterns", "without", "the", "need", "for"}), section_NatSpec_API_Guide);
		// Code @TextSyntax
		de.devboost.natspec.library.documentation.Code code__TextSyntax2 = documentationSupport.code(new java.lang.StringBuilder().append("@TextSyntax").toString(), section_NatSpec_API_Guide);
		// annotations. Also, providers
		de.devboost.natspec.library.documentation.Text text_annotations__Also__providers = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"annotations.", "Also,", "providers"}), section_NatSpec_API_Guide);
		// for synonyms can be registered with this API. The following sections cover
		de.devboost.natspec.library.documentation.Text text_for_synonyms_can_be_registered_with_this_API__The_following_sections_cover = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"for", "synonyms", "can", "be", "registered", "with", "this", "API.", "The", "following", "sections", "cover"}), section_NatSpec_API_Guide);
		// details on the most important parts of this API.
		de.devboost.natspec.library.documentation.Text text_details_on_the_most_important_parts_of_this_API_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"details", "on", "the", "most", "important", "parts", "of", "this", "API."}), section_NatSpec_API_Guide);
		// Subsection - Pattern Provider API
		de.devboost.natspec.library.documentation.Subsection subsection_Pattern_Provider_API = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Pattern", "Provider", "API"}), section_NatSpec_API_Guide);
		// To extend NatSpec dynamically with syntax patterns one can create a class that
		de.devboost.natspec.library.documentation.Text text_To_extend_NatSpec_dynamically_with_syntax_patterns_one_can_create_a_class_that = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "extend", "NatSpec", "dynamically", "with", "syntax", "patterns", "one", "can", "create", "a", "class", "that"}), subsection_Pattern_Provider_API);
		// implements the interface
		de.devboost.natspec.library.documentation.Text text_implements_the_interface = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"implements", "the", "interface"}), subsection_Pattern_Provider_API);
		// Code ISyntaxPatternProvider
		de.devboost.natspec.library.documentation.Code code_ISyntaxPatternProvider = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPatternProvider").toString(), subsection_Pattern_Provider_API);
		// . This class must reside
		de.devboost.natspec.library.documentation.Text text___This_class_must_reside = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "This", "class", "must", "reside"}), subsection_Pattern_Provider_API);
		// in an OSGi bundle located in the current workspace. Whenever a change is applied
		de.devboost.natspec.library.documentation.Text text_in_an_OSGi_bundle_located_in_the_current_workspace__Whenever_a_change_is_applied = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "an", "OSGi", "bundle", "located", "in", "the", "current", "workspace.", "Whenever", "a", "change", "is", "applied"}), subsection_Pattern_Provider_API);
		// to the class, NatSpec will automatically reload the OSGi bundle and instantiate
		de.devboost.natspec.library.documentation.Text text_to_the_class__NatSpec_will_automatically_reload_the_OSGi_bundle_and_instantiate = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"to", "the", "class,", "NatSpec", "will", "automatically", "reload", "the", "OSGi", "bundle", "and", "instantiate"}), subsection_Pattern_Provider_API);
		// a new instance of the class which will be asked to provide syntax pattern when
		de.devboost.natspec.library.documentation.Text text_a_new_instance_of_the_class_which_will_be_asked_to_provide_syntax_pattern_when = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"a", "new", "instance", "of", "the", "class", "which", "will", "be", "asked", "to", "provide", "syntax", "pattern", "when"}), subsection_Pattern_Provider_API);
		// necessary.
		de.devboost.natspec.library.documentation.Text text_necessary_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"necessary."}), subsection_Pattern_Provider_API);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_33 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Pattern_Provider_API);
		// To ask for syntax patterns, NatSpec calls the method
		de.devboost.natspec.library.documentation.Text text_To_ask_for_syntax_patterns__NatSpec_calls_the_method = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "ask", "for", "syntax", "patterns,", "NatSpec", "calls", "the", "method"}), paragraph_33);
		// Code getPatterns()
		de.devboost.natspec.library.documentation.Code code_getPatterns__ = documentationSupport.code(new java.lang.StringBuilder().append("getPatterns()").toString(), paragraph_33);
		// on the provider class which returns a collections of patterns (i.e., instances
		de.devboost.natspec.library.documentation.Text text_on_the_provider_class_which_returns_a_collections_of_patterns__i_e___instances = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"on", "the", "provider", "class", "which", "returns", "a", "collections", "of", "patterns", "(i.e.,", "instances"}), paragraph_33);
		// of
		de.devboost.natspec.library.documentation.Text text_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of"}), paragraph_33);
		// Code ISyntaxPattern
		de.devboost.natspec.library.documentation.Code code_ISyntaxPattern = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPattern").toString(), paragraph_33);
		// ). NatSpec passes the URI of the current specification
		de.devboost.natspec.library.documentation.Text text____NatSpec_passes_the_URI_of_the_current_specification = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {").", "NatSpec", "passes", "the", "URI", "of", "the", "current", "specification"}), paragraph_33);
		// file to
		de.devboost.natspec.library.documentation.Text text_file_to = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"file", "to"}), paragraph_33);
		// Code getPatterns()
		de.devboost.natspec.library.documentation.Code code_getPatterns__0 = documentationSupport.code(new java.lang.StringBuilder().append("getPatterns()").toString(), paragraph_33);
		// in order to allow pattern providers to return
		de.devboost.natspec.library.documentation.Text text_in_order_to_allow_pattern_providers_to_return = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"in", "order", "to", "allow", "pattern", "providers", "to", "return"}), paragraph_33);
		// different collection of patterns for different specifications. This can, for
		de.devboost.natspec.library.documentation.Text text_different_collection_of_patterns_for_different_specifications__This_can__for = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"different", "collection", "of", "patterns", "for", "different", "specifications.", "This", "can,", "for"}), paragraph_33);
		// example, be used to consider the classpath of the project that contains the
		de.devboost.natspec.library.documentation.Text text_example__be_used_to_consider_the_classpath_of_the_project_that_contains_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"example,", "be", "used", "to", "consider", "the", "classpath", "of", "the", "project", "that", "contains", "the"}), paragraph_33);
		// specification when computing syntax patterns.
		de.devboost.natspec.library.documentation.Text text_specification_when_computing_syntax_patterns_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"specification", "when", "computing", "syntax", "patterns."}), paragraph_33);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_34 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Pattern_Provider_API);
		// Syntax patterns can be realized by creating a class that implements
		de.devboost.natspec.library.documentation.Text text_Syntax_patterns_can_be_realized_by_creating_a_class_that_implements = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Syntax", "patterns", "can", "be", "realized", "by", "creating", "a", "class", "that", "implements"}), paragraph_34);
		// Code ISyntaxPattern
		de.devboost.natspec.library.documentation.Code code_ISyntaxPattern0 = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPattern").toString(), paragraph_34);
		// . We strongly encourage clients of the API to extend
		de.devboost.natspec.library.documentation.Text text___We_strongly_encourage_clients_of_the_API_to_extend = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "We", "strongly", "encourage", "clients", "of", "the", "API", "to", "extend"}), paragraph_34);
		// Code AbstractSyntaxPattern
		de.devboost.natspec.library.documentation.Code code_AbstractSyntaxPattern = documentationSupport.code(new java.lang.StringBuilder().append("AbstractSyntaxPattern").toString(), paragraph_34);
		// instead of implementing the interface only.
		de.devboost.natspec.library.documentation.Text text_instead_of_implementing_the_interface_only_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"instead", "of", "implementing", "the", "interface", "only."}), paragraph_34);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_35 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Pattern_Provider_API);
		// A syntax pattern can match a sentence in a NatSpec specification and consists of
		de.devboost.natspec.library.documentation.Text text_A_syntax_pattern_can_match_a_sentence_in_a_NatSpec_specification_and_consists_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"A", "syntax", "pattern", "can", "match", "a", "sentence", "in", "a", "NatSpec", "specification", "and", "consists", "of"}), paragraph_35);
		// syntax pattern parts which can match words in a sentence. The parts of a syntax
		de.devboost.natspec.library.documentation.Text text_syntax_pattern_parts_which_can_match_words_in_a_sentence__The_parts_of_a_syntax = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"syntax", "pattern", "parts", "which", "can", "match", "words", "in", "a", "sentence.", "The", "parts", "of", "a", "syntax"}), paragraph_35);
		// pattern must be returned by its method
		de.devboost.natspec.library.documentation.Text text_pattern_must_be_returned_by_its_method = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"pattern", "must", "be", "returned", "by", "its", "method"}), paragraph_35);
		// Code getParts()
		de.devboost.natspec.library.documentation.Code code_getParts__ = documentationSupport.code(new java.lang.StringBuilder().append("getParts()").toString(), paragraph_35);
		// . Syntax pattern parts
		de.devboost.natspec.library.documentation.Text text___Syntax_pattern_parts = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "Syntax", "pattern", "parts"}), paragraph_35);
		// can be simple static words (see class
		de.devboost.natspec.library.documentation.Text text_can_be_simple_static_words__see_class = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"can", "be", "simple", "static", "words", "(see", "class"}), paragraph_35);
		// Code StaticWord
		de.devboost.natspec.library.documentation.Code code_StaticWord = documentationSupport.code(new java.lang.StringBuilder().append("StaticWord").toString(), paragraph_35);
		// ) or match certain
		de.devboost.natspec.library.documentation.Text text___or_match_certain = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {")", "or", "match", "certain"}), paragraph_35);
		// types of parameters (e.g., integers or dates). One can either implement custom
		de.devboost.natspec.library.documentation.Text text_types_of_parameters__e_g___integers_or_dates___One_can_either_implement_custom = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"types", "of", "parameters", "(e.g.,", "integers", "or", "dates).", "One", "can", "either", "implement", "custom"}), paragraph_35);
		// syntax pattern parts or use existing ones provided by the NatSpec API. Most
		de.devboost.natspec.library.documentation.Text text_syntax_pattern_parts_or_use_existing_ones_provided_by_the_NatSpec_API__Most = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"syntax", "pattern", "parts", "or", "use", "existing", "ones", "provided", "by", "the", "NatSpec", "API.", "Most"}), paragraph_35);
		// common syntax pattern parts are:
		de.devboost.natspec.library.documentation.Text text_common_syntax_pattern_parts_are_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"common", "syntax", "pattern", "parts", "are:"}), paragraph_35);
		// List
		de.devboost.natspec.library.documentation.List list_0 = documentationSupport.addList(subsection_Pattern_Provider_API);
		// * StaticWord - match a single word case-insensitive or one of its synonyms
		de.devboost.natspec.library.documentation.ListItem listItem_StaticWord___match_a_single_word_case_insensitive_or_one_of_its_synonyms = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"StaticWord", "-", "match", "a", "single", "word", "case-insensitive", "or", "one", "of", "its", "synonyms"}), list_0);
		// * IntegerArgument - match a single integer number
		de.devboost.natspec.library.documentation.ListItem listItem_IntegerArgument___match_a_single_integer_number = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"IntegerArgument", "-", "match", "a", "single", "integer", "number"}), list_0);
		// * DoubleArgument - match a single floating-point number
		de.devboost.natspec.library.documentation.ListItem listItem_DoubleArgument___match_a_single_floating_point_number = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"DoubleArgument", "-", "match", "a", "single", "floating-point", "number"}), list_0);
		// * DateArgument - match a single date
		de.devboost.natspec.library.documentation.ListItem listItem_DateArgument___match_a_single_date = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"DateArgument", "-", "match", "a", "single", "date"}), list_0);
		// * ListParameter - match a list of arguments
		de.devboost.natspec.library.documentation.ListItem listItem_ListParameter___match_a_list_of_arguments = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"ListParameter", "-", "match", "a", "list", "of", "arguments"}), list_0);
		// * ComplexParameter - match an instance of a specific Java class
		de.devboost.natspec.library.documentation.ListItem listItem_ComplexParameter___match_an_instance_of_a_specific_Java_class = documentationSupport.addListItem(java.util.Arrays.asList(new java.lang.String[] {"ComplexParameter", "-", "match", "an", "instance", "of", "a", "specific", "Java", "class"}), list_0);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_36 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Pattern_Provider_API);
		// Custom syntax pattern parts must implement
		de.devboost.natspec.library.documentation.Text text_Custom_syntax_pattern_parts_must_implement = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Custom", "syntax", "pattern", "parts", "must", "implement"}), paragraph_36);
		// Code ISyntaxPatternPart
		de.devboost.natspec.library.documentation.Code code_ISyntaxPatternPart = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPatternPart").toString(), paragraph_36);
		// and respectively the method
		de.devboost.natspec.library.documentation.Text text_and_respectively_the_method = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "respectively", "the", "method"}), paragraph_36);
		// Code match(List<Word> words, IPatternMatchContext context)
		de.devboost.natspec.library.documentation.Code code_match_List_Word__words__IPatternMatchContext_context_ = documentationSupport.code(new java.lang.StringBuilder().append("match(List<Word>").append(" ").append("words,").append(" ").append("IPatternMatchContext").append(" ").append("context)").toString(), paragraph_36);
		// . By calling the
		de.devboost.natspec.library.documentation.Text text___By_calling_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "By", "calling", "the"}), paragraph_36);
		// Code match()
		de.devboost.natspec.library.documentation.Code code_match__ = documentationSupport.code(new java.lang.StringBuilder().append("match()").toString(), paragraph_36);
		// method, NatSpec can decide whether a
		de.devboost.natspec.library.documentation.Text text_method__NatSpec_can_decide_whether_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"method,", "NatSpec", "can", "decide", "whether", "a"}), paragraph_36);
		// syntax pattern part matches a given list of words. If all parts of a syntax
		de.devboost.natspec.library.documentation.Text text_syntax_pattern_part_matches_a_given_list_of_words__If_all_parts_of_a_syntax = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"syntax", "pattern", "part", "matches", "a", "given", "list", "of", "words.", "If", "all", "parts", "of", "a", "syntax"}), paragraph_36);
		// pattern match all words in a sentence, the sentence is considered as matched
		de.devboost.natspec.library.documentation.Text text_pattern_match_all_words_in_a_sentence__the_sentence_is_considered_as_matched = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"pattern", "match", "all", "words", "in", "a", "sentence,", "the", "sentence", "is", "considered", "as", "matched"}), paragraph_36);
		// completely.
		de.devboost.natspec.library.documentation.Text text_completely_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"completely."}), paragraph_36);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_37 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Pattern_Provider_API);
		// When asked for a match, each syntax pattern part can return a custom match
		de.devboost.natspec.library.documentation.Text text_When_asked_for_a_match__each_syntax_pattern_part_can_return_a_custom_match = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"When", "asked", "for", "a", "match,", "each", "syntax", "pattern", "part", "can", "return", "a", "custom", "match"}), paragraph_37);
		// object (see
		de.devboost.natspec.library.documentation.Text text_object__see = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"object", "(see"}), paragraph_37);
		// Code ISyntaxPatternPartMatch
		de.devboost.natspec.library.documentation.Code code_ISyntaxPatternPartMatch = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPatternPartMatch").toString(), paragraph_37);
		// ) which stores the data that is
		de.devboost.natspec.library.documentation.Text text___which_stores_the_data_that_is = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {")", "which", "stores", "the", "data", "that", "is"}), paragraph_37);
		// relevant for the code generation. For example, class
		de.devboost.natspec.library.documentation.Text text_relevant_for_the_code_generation__For_example__class = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"relevant", "for", "the", "code", "generation.", "For", "example,", "class"}), paragraph_37);
		// Code DateArgument
		de.devboost.natspec.library.documentation.Code code_DateArgument = documentationSupport.code(new java.lang.StringBuilder().append("DateArgument").toString(), paragraph_37);
		// returns a
		de.devboost.natspec.library.documentation.Text text_returns_a = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"returns", "a"}), paragraph_37);
		// Code DateMatch
		de.devboost.natspec.library.documentation.Code code_DateMatch = documentationSupport.code(new java.lang.StringBuilder().append("DateMatch").toString(), paragraph_37);
		// object containing the actual date found in the
		de.devboost.natspec.library.documentation.Text text_object_containing_the_actual_date_found_in_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"object", "containing", "the", "actual", "date", "found", "in", "the"}), paragraph_37);
		// sentence. Custom syntax pattern parts can either implement interface
		de.devboost.natspec.library.documentation.Text text_sentence__Custom_syntax_pattern_parts_can_either_implement_interface = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"sentence.", "Custom", "syntax", "pattern", "parts", "can", "either", "implement", "interface"}), paragraph_37);
		// Code ISyntaxPatternPartMatch
		de.devboost.natspec.library.documentation.Code code_ISyntaxPatternPartMatch0 = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPatternPartMatch").toString(), paragraph_37);
		// or extend class
		de.devboost.natspec.library.documentation.Text text_or_extend_class = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"or", "extend", "class"}), paragraph_37);
		// Code AbstractSyntaxPatternPartMatch
		de.devboost.natspec.library.documentation.Code code_AbstractSyntaxPatternPartMatch = documentationSupport.code(new java.lang.StringBuilder().append("AbstractSyntaxPatternPartMatch").toString(), paragraph_37);
		// . The latter method is recommended.
		de.devboost.natspec.library.documentation.Text text___The_latter_method_is_recommended_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "The", "latter", "method", "is", "recommended."}), paragraph_37);
		// Paragraph
		de.devboost.natspec.library.documentation.Paragraph paragraph_38 = documentationSupport.createParagraphWithHeading(java.util.Arrays.asList(new java.lang.String[] {}), subsection_Pattern_Provider_API);
		// To generate code for matched sentences NatSpec calls
		de.devboost.natspec.library.documentation.Text text_To_generate_code_for_matched_sentences_NatSpec_calls = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "generate", "code", "for", "matched", "sentences", "NatSpec", "calls"}), paragraph_38);
		// Code createUserData()
		de.devboost.natspec.library.documentation.Code code_createUserData__ = documentationSupport.code(new java.lang.StringBuilder().append("createUserData()").toString(), paragraph_38);
		// on the syntax pattern object. If this method returns an object implementing
		de.devboost.natspec.library.documentation.Text text_on_the_syntax_pattern_object__If_this_method_returns_an_object_implementing = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"on", "the", "syntax", "pattern", "object.", "If", "this", "method", "returns", "an", "object", "implementing"}), paragraph_38);
		// Code ICodeFragment
		de.devboost.natspec.library.documentation.Code code_ICodeFragment = documentationSupport.code(new java.lang.StringBuilder().append("ICodeFragment").toString(), paragraph_38);
		// the code is obtained by calling
		de.devboost.natspec.library.documentation.Text text_the_code_is_obtained_by_calling = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "code", "is", "obtained", "by", "calling"}), paragraph_38);
		// Code generateSourceCode()
		de.devboost.natspec.library.documentation.Code code_generateSourceCode__ = documentationSupport.code(new java.lang.StringBuilder().append("generateSourceCode()").toString(), paragraph_38);
		// . To compose the code, the syntax pattern can
		de.devboost.natspec.library.documentation.Text text___To_compose_the_code__the_syntax_pattern_can = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "To", "compose", "the", "code,", "the", "syntax", "pattern", "can"}), paragraph_38);
		// access the matched parts using the parameter
		de.devboost.natspec.library.documentation.Text text_access_the_matched_parts_using_the_parameter = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"access", "the", "matched", "parts", "using", "the", "parameter"}), paragraph_38);
		// Code match
		de.devboost.natspec.library.documentation.Code code_match = documentationSupport.code(new java.lang.StringBuilder().append("match").toString(), paragraph_38);
		// which implements
		de.devboost.natspec.library.documentation.Text text_which_implements = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"which", "implements"}), paragraph_38);
		// Code ISyntaxPatternMatch
		de.devboost.natspec.library.documentation.Code code_ISyntaxPatternMatch = documentationSupport.code(new java.lang.StringBuilder().append("ISyntaxPatternMatch").toString(), paragraph_38);
		// and therefore gives access to all matched pattern parts.
		de.devboost.natspec.library.documentation.Text text_and_therefore_gives_access_to_all_matched_pattern_parts_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"and", "therefore", "gives", "access", "to", "all", "matched", "pattern", "parts."}), paragraph_38);
		// To easily get started with the pattern provider API one may also consider the
		de.devboost.natspec.library.documentation.Text text_To_easily_get_started_with_the_pattern_provider_API_one_may_also_consider_the = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"To", "easily", "get", "started", "with", "the", "pattern", "provider", "API", "one", "may", "also", "consider", "the"}), paragraph_38);
		// example provider class
		de.devboost.natspec.library.documentation.Text text_example_provider_class = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"example", "provider", "class"}), paragraph_38);
		// Code JavaCustomizationProvider
		de.devboost.natspec.library.documentation.Code code_JavaCustomizationProvider0 = documentationSupport.code(new java.lang.StringBuilder().append("JavaCustomizationProvider").toString(), paragraph_38);
		// . This class is part of
		de.devboost.natspec.library.documentation.Text text___This_class_is_part_of = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "This", "class", "is", "part", "of"}), paragraph_38);
		// the NatSpec example workspace and contained in the project
		de.devboost.natspec.library.documentation.Text text_the_NatSpec_example_workspace_and_contained_in_the_project = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "NatSpec", "example", "workspace", "and", "contained", "in", "the", "project"}), paragraph_38);
		// Code de.devboost.natspec.java.customization
		de.devboost.natspec.library.documentation.Code code_de_devboost_natspec_java_customization = documentationSupport.code(new java.lang.StringBuilder().append("de.devboost.natspec.java.customization").toString(), paragraph_38);
		// .
		de.devboost.natspec.library.documentation.Text text__30 = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"."}), paragraph_38);
		// Subsection - Synonym Provider API
		de.devboost.natspec.library.documentation.Subsection subsection_Synonym_Provider_API = documentationSupport.addSubsection(java.util.Arrays.asList(new java.lang.String[] {"Synonym", "Provider", "API"}), section_NatSpec_API_Guide);
		// Extending NatSpec with custom synonym providers is similar to the implementation
		de.devboost.natspec.library.documentation.Text text_Extending_NatSpec_with_custom_synonym_providers_is_similar_to_the_implementation = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"Extending", "NatSpec", "with", "custom", "synonym", "providers", "is", "similar", "to", "the", "implementation"}), subsection_Synonym_Provider_API);
		// of custom pattern providers.
		de.devboost.natspec.library.documentation.Text text_of_custom_pattern_providers_ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"of", "custom", "pattern", "providers."}), subsection_Synonym_Provider_API);
		// One must create a class in an OSGi bundle located in the current workspace that
		de.devboost.natspec.library.documentation.Text text_One_must_create_a_class_in_an_OSGi_bundle_located_in_the_current_workspace_that = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"One", "must", "create", "a", "class", "in", "an", "OSGi", "bundle", "located", "in", "the", "current", "workspace", "that"}), subsection_Synonym_Provider_API);
		// implements
		de.devboost.natspec.library.documentation.Text text_implements = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"implements"}), subsection_Synonym_Provider_API);
		// Code ISynonymProvider
		de.devboost.natspec.library.documentation.Code code_ISynonymProvider = documentationSupport.code(new java.lang.StringBuilder().append("ISynonymProvider").toString(), subsection_Synonym_Provider_API);
		// . This class will be automatically
		de.devboost.natspec.library.documentation.Text text___This_class_will_be_automatically = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {".", "This", "class", "will", "be", "automatically"}), subsection_Synonym_Provider_API);
		// instantiated and asked for synonyms by NatSpec if required. Similar to syntax
		de.devboost.natspec.library.documentation.Text text_instantiated_and_asked_for_synonyms_by_NatSpec_if_required__Similar_to_syntax = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"instantiated", "and", "asked", "for", "synonyms", "by", "NatSpec", "if", "required.", "Similar", "to", "syntax"}), subsection_Synonym_Provider_API);
		// patterns, synonyms can be specific to certain specifications (e.g., depending on
		de.devboost.natspec.library.documentation.Text text_patterns__synonyms_can_be_specific_to_certain_specifications__e_g___depending_on = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"patterns,", "synonyms", "can", "be", "specific", "to", "certain", "specifications", "(e.g.,", "depending", "on"}), subsection_Synonym_Provider_API);
		// the project that contains the specification).
		de.devboost.natspec.library.documentation.Text text_the_project_that_contains_the_specification__ = documentationSupport.createPlainContents(java.util.Arrays.asList(new String[] {"the", "project", "that", "contains", "the", "specification)."}), subsection_Synonym_Provider_API);
		
	}

	@Override
	public Documentation getDocumentation() {
		return documentationSupport.getDocumentation();
	}
}
