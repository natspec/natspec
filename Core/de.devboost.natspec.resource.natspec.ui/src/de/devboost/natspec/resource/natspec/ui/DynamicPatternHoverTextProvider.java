package de.devboost.natspec.resource.natspec.ui;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;

public class DynamicPatternHoverTextProvider extends AbstractHoverTextProvider {

	@Override
		protected String getHoverText(EObject object, List<ISyntaxPatternMatch<? extends Object>> matches,
				String projectName, Sentence sentence) {
			
			String text = null;
			if (sentence != object) {
				return null;
			}
			
			for (ISyntaxPatternMatch<? extends Object> match : matches) {
				ISyntaxPattern<? extends Object> pattern = match.getPattern();
				String documentation = pattern.getDocumentation();
				if (documentation == null) {
					continue;
				}
				if (text == null) {
					text = "";
				}
				
				text += "<br><br>"+documentation;
			}

			return text;
		}

}
