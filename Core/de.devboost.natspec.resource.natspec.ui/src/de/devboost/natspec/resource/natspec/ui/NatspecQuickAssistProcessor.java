package de.devboost.natspec.resource.natspec.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.quickassist.IQuickAssistInvocationContext;
import org.eclipse.jface.text.quickassist.IQuickAssistProcessor;
import org.eclipse.jface.text.source.Annotation;

import de.devboost.natspec.resource.natspec.INatspecResourceProvider;

public class NatspecQuickAssistProcessor implements IQuickAssistProcessor {
	
	// TODO This is a somewhat dirty implementation of a registry for
	// quick assist processors
	public final static Set<IExtendedNatspecQuickAssistProcessor> ADDITIONAL_PROCESSORS = new LinkedHashSet<IExtendedNatspecQuickAssistProcessor>();
	
	private INatspecResourceProvider resourceProvider;
	private INatspecAnnotationModelProvider annotationModelProvider;
	
	public NatspecQuickAssistProcessor(INatspecResourceProvider resourceProvider, INatspecAnnotationModelProvider annotationModelProvider) {
		super();
		this.resourceProvider = resourceProvider;
		this.annotationModelProvider = annotationModelProvider;
	}
	
	public boolean canAssist(IQuickAssistInvocationContext invocationContext) {
		for (IExtendedNatspecQuickAssistProcessor additionalProcessor : ADDITIONAL_PROCESSORS) {
			additionalProcessor.setResourceProvider(resourceProvider);
			additionalProcessor.setAnnotationModelProvider(annotationModelProvider);
			if (additionalProcessor.canAssist(invocationContext)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean canFix(Annotation annotation) {
		for (IExtendedNatspecQuickAssistProcessor additionalProcessor : ADDITIONAL_PROCESSORS) {
			additionalProcessor.setResourceProvider(resourceProvider);
			additionalProcessor.setAnnotationModelProvider(annotationModelProvider);
			if (additionalProcessor.canFix(annotation)) {
				return true;
			}
		}
		return false;
	}
	
	public ICompletionProposal[] computeQuickAssistProposals(IQuickAssistInvocationContext invocationContext) {
		List<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
		for (IExtendedNatspecQuickAssistProcessor additionalProcessor : ADDITIONAL_PROCESSORS) {
			additionalProcessor.setResourceProvider(resourceProvider);
			additionalProcessor.setAnnotationModelProvider(annotationModelProvider);
			ICompletionProposal[] subProposals = additionalProcessor.computeQuickAssistProposals(invocationContext);
			if (subProposals == null) {
				continue;
			}
			proposals.addAll(Arrays.asList(subProposals));
		}
		
		if (proposals.isEmpty()) {
			return null;
		}
		
		return proposals.toArray(new ICompletionProposal[proposals.size()]);
	}

	public String getErrorMessage() {
		return null;
	}
}
