package de.devboost.natspec.resource.natspec.ui;

import org.eclipse.jface.text.quickassist.IQuickAssistProcessor;

import de.devboost.natspec.resource.natspec.INatspecResourceProvider;

/**
 * This interface must be implemented by classes that extent the set of quick
 * assist proposals for NatSpec.
 */
public interface IExtendedNatspecQuickAssistProcessor extends IQuickAssistProcessor {

	public void setResourceProvider(INatspecResourceProvider resourceProvider);

	public void setAnnotationModelProvider(
			INatspecAnnotationModelProvider annotationModelProvider);
}
