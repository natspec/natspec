package de.devboost.natspec.resource.natspec.ui;

import org.eclipse.swt.graphics.Color;

public class TextAttributeKey {

	private final Color foregroundColor;
	private final Color backgroundColor;
	private final int style;
	
	public TextAttributeKey(Color foregroundColor, Color backgroundColor,
			int style) {
		super();
		this.foregroundColor = foregroundColor;
		this.backgroundColor = backgroundColor;
		this.style = style;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((backgroundColor == null) ? 0 : backgroundColor.hashCode());
		result = prime * result
				+ ((foregroundColor == null) ? 0 : foregroundColor.hashCode());
		result = prime * result + style;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextAttributeKey other = (TextAttributeKey) obj;
		if (backgroundColor == null) {
			if (other.backgroundColor != null)
				return false;
		} else if (!backgroundColor.equals(other.backgroundColor))
			return false;
		if (foregroundColor == null) {
			if (other.foregroundColor != null)
				return false;
		} else if (!foregroundColor.equals(other.foregroundColor))
			return false;
		if (style != other.style)
			return false;
		return true;
	}
}
