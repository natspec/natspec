package de.devboost.natspec.resource.natspec.ui;

import de.devboost.natspec.resource.natspec.mopp.NatspecTextToken;

public class CustomTextToken extends NatspecTextToken {

	private final Class<?> clazz;
	private Boolean bold;
	private Boolean italic;
	private Boolean underline;
	private Boolean strikethrough;
	private int[] colorAsRGB;

	public CustomTextToken(Class<?> clazz, String name, String text, int offset, int length,
			int line, int column, boolean canBeUsedForSyntaxHighlighting) {
		super(name, text, offset, length, line, column, canBeUsedForSyntaxHighlighting);
		this.clazz = clazz;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public Boolean getBold() {
		return bold;
	}

	public void setBold(boolean bold) {
		this.bold = bold;
	}

	public Boolean getItalic() {
		return italic;
	}

	public void setItalic(boolean italic) {
		this.italic = italic;
	}

	public Boolean getUnderline() {
		return underline;
	}

	public void setUnderline(boolean underline) {
		this.underline = underline;
	}

	public Boolean getStrikethrough() {
		return strikethrough;
	}

	public void setStrikethrough(boolean strikethrough) {
		this.strikethrough = strikethrough;
	}

	public int[] getColorAsRGB() {
		return colorAsRGB;
	}

	public void setColorAsRGB(int[] colorAsRGB) {
		this.colorAsRGB = colorAsRGB;
	}
}
