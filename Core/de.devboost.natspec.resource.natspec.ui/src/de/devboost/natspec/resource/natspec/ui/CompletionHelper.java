package de.devboost.natspec.resource.natspec.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.swt.graphics.Image;

import com.google.common.base.CaseFormat;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.resource.natspec.mopp.NatspecExpectedTerminal;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;

public class CompletionHelper {

	private static final Pattern WHITESPACE_REGEX = Pattern.compile("^\\s+");
	
	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;
	
	public CompletionHelper(boolean useOptimizedMatcher, boolean usePrioritizer) {
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	public Set<String> computeCompletionProposals(List<ISyntaxPattern<?>> patterns, List<Sentence> sentences,
			IPatternMatchContext context) {

		if (sentences.isEmpty()) {
			return new LinkedHashSet<>();
		}

		Sentence lastSentence = sentences.get(sentences.size() - 1);
		String text = lastSentence.getText();
		String camelCaseExpanded = expandCamelCase(text);
		Set<String> proposals = new LinkedHashSet<>();
		Set<String> lowerCaseProposals = new LinkedHashSet<>();
		
		PatternMatchContextFactory contextFactory = PatternMatchContextFactory.INSTANCE;
		URI contextURI = context.getContextURI();
		
		boolean matchFuzzy = false;
		IPatternMatchContext freshContext = contextFactory.createPatternMatchContext(contextURI, null, matchFuzzy);
		freshContext.addObjectsToContext(context.getObjectsInContext());
		List<String> computedProposals = computeProposals(patterns, freshContext, sentences, lastSentence);
		updateProposalSets(computedProposals, proposals, lowerCaseProposals);
		
		matchFuzzy = true;
		freshContext = contextFactory.createPatternMatchContext(contextURI, null, matchFuzzy);
		freshContext.addObjectsToContext(context.getObjectsInContext());
		sentences.remove(lastSentence);
		Sentence newLastSentence = SentenceUtil.INSTANCE.createSentence(camelCaseExpanded);
		sentences.add(newLastSentence);
		
		computedProposals = computeProposals(patterns, freshContext, sentences, newLastSentence);
		updateProposalSets(computedProposals, proposals, lowerCaseProposals);
		
		// Restore last sentence
		sentences.remove(newLastSentence);
		sentences.add(lastSentence);
		
		return proposals;
	}

	private void updateProposalSets(List<String> computedProposals, Set<String> proposals,
			Set<String> lowerCaseProposals) {
		
		for (String computedProposal : computedProposals) {
			String lowerCaseProposal = computedProposal.toLowerCase();
			if (lowerCaseProposals.contains(lowerCaseProposal)) {
				continue;
			}

			lowerCaseProposals.add(lowerCaseProposal);
			proposals.add(computedProposal);
		}
	}

	/**
	 * Transforms the given text such that the last word is converted from camel-case to individual words. For example,
	 * <tt>My Input ABCde</tt> is transformed to <tt>My Input A B Cde</tt>.
	 */
	private String expandCamelCase(String text) {
		StringBuilder expanded = new StringBuilder();

		String[] words = text.split(" ");
		int wordCount = words.length;
		for (int i = 0; i < wordCount; i++) {
			boolean isLastPart = i == wordCount - 1;
			if (isLastPart) {
				String lowerUnderscore = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, words[i]);
				String[] lastWordParts = lowerUnderscore.split("_");
				for (String lastWordPart : lastWordParts) {
					expanded.append(lastWordPart);
					expanded.append(" ");
				}
			} else {
				expanded.append(words[i]);
				expanded.append(" ");
			}
		}

		return expanded.toString();
	}

	private List<String> computeProposals(List<ISyntaxPattern<?>> patterns, IPatternMatchContext context,
			List<Sentence> sentences, Sentence lastSentence) {
		
		List<ISyntaxPatternMatch<?>> allMatches = new ArrayList<ISyntaxPatternMatch<?>>();
		MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);
		for (int i = 0; i < sentences.size() - 1; i++) {
			Sentence previousSentence = sentences.get(i);
			// ignore comments
			if (SentenceUtil.INSTANCE.isComment(previousSentence)) {
				continue;
			}
			
			// ignore empty sentences
			if (SentenceUtil.INSTANCE.isEmpty(previousSentence)) {
				continue;
			}
			
			// Here we do not prioritize matches because we need to get all proposals
			boolean prioritizeMatches = false;
			boolean includeIncompleteMatches = false;
			boolean filterObsoleteDefaultMatches = true;
			List<ISyntaxPatternMatch<?>> matches = matchService.match(previousSentence, context,
					includeIncompleteMatches, filterObsoleteDefaultMatches, prioritizeMatches, patterns);
			allMatches.addAll(matches);
		}

		List<String> result = new ArrayList<>();
		for (ISyntaxPattern<?> pattern : patterns) {
			computeCompletionProposals(result, lastSentence, pattern, context, allMatches);
		}

		return result;
	}

	private void computeCompletionProposals(List<String> result, Sentence lastSentence, ISyntaxPattern<?> pattern,
			IPatternMatchContext context, List<ISyntaxPatternMatch<? extends Object>> previousMatches) {

		MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);
		Collection<ISyntaxPattern<?>> patterns = Collections.<ISyntaxPattern<?>>singletonList(pattern);
		boolean includeIncompleteMatches = true;
		boolean filterObsoleteDefaultMatches = true;
		// Here we do not prioritize matches because we need to get all proposals
		boolean prioritizeMatches = false;
		List<ISyntaxPatternMatch<? extends Object>> allMatches = matchService.match(lastSentence, context,
				includeIncompleteMatches, filterObsoleteDefaultMatches, prioritizeMatches, patterns);
		String remainingSentence = lastSentence.getText();
		
		for (ISyntaxPatternMatch<? extends Object> match : allMatches) {
			// The CompletionProposalComputer carries state and must therefore be recreated for each match
			CompletionProposalComputer computer = new CompletionProposalComputer();
			computer.process(remainingSentence, match, context, previousMatches);
			boolean isComplete = computer.isComplete();
			List<String> proposals = computer.getProposals();
			for (String proposal : proposals) {
				if (isComplete && !proposal.trim().isEmpty()) {
					result.add(proposal.trim());
				}
			}
		}
	}

	public List<NatspecCompletionProposal> getCompletionProposals(IPatternMatchContext context,
			List<ISyntaxPattern<?>> patterns, List<Sentence> sentences) {

		if (sentences.isEmpty()) {
			return Collections.emptyList();
		}

		Set<String> completionProposalSet = computeCompletionProposals(patterns, sentences, context);
		// Convert set to a list to allow sorting
		List<String> completionProposalList = new ArrayList<>(completionProposalSet);
		Collections.sort(completionProposalList);

		Sentence lastSentence = sentences.get(sentences.size() - 1);
		return deriveCompletionProposals(completionProposalList, lastSentence);
	}

	private List<NatspecCompletionProposal> deriveCompletionProposals(List<String> proposalTexts,
			Sentence currentSentence) {
		
		String sentenceText = currentSentence.getText();

		// TODO The image is located in a plug-in which might not be available
		String imageURI = "platform:/plugin/de.devboost.natspec.testscripting.ui/icons/pattern.png";
		Image image = NatspecImageProvider.INSTANCE.getImage(imageURI);

		NatspecExpectedTerminal expectedTerminal = null;
		// do not replace leading whitespace when inserting completion
		String prefix = WHITESPACE_REGEX.matcher(sentenceText).replaceAll("");
		boolean matchesPrefix = true;
		EStructuralFeature stucturalFeature = null;
		EObject container = null;

		List<NatspecCompletionProposal> proposals = new LinkedList<NatspecCompletionProposal>();
		for (String proposalText : proposalTexts) {
			String displayString = proposalText;
			String insertString = proposalText;
			// TODO we could make inserting a bit more clever here.
			NatspecCompletionProposal natSpecProposal = new NatspecCompletionProposal(expectedTerminal, insertString,
					prefix, matchesPrefix, stucturalFeature, container, image, displayString);
			proposals.add(natSpecProposal);
		}

		return proposals;
	}
}
