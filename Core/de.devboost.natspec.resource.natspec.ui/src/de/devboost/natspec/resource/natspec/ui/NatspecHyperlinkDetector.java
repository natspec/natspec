/**
 * <copyright>
 * </copyright>
 *
 * 
 */
package de.devboost.natspec.resource.natspec.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;

import de.devboost.natspec.resource.natspec.INatspecTextResource;

/**
 * This class return links for all recognized sentences.
 */
public class NatspecHyperlinkDetector implements IHyperlinkDetector {
	
	public final static Set<IExtendedHyperlinkDetector> ADDITIONAL_DETECTORS = new LinkedHashSet<IExtendedHyperlinkDetector>();

	private INatspecTextResource textResource;
	
	/**
	 * Creates a hyperlink detector.
	 * 
	 * @param resource the resource to use for calculating the locations.
	 */
	public NatspecHyperlinkDetector(Resource resource) {
		textResource = (INatspecTextResource) resource;
	}
	
	public IHyperlink[] detectHyperlinks(ITextViewer textViewer, IRegion region, boolean canShowMultipleHyperlinks) {
		List<IHyperlink> links = new ArrayList<IHyperlink>();
		for (IExtendedHyperlinkDetector detector : ADDITIONAL_DETECTORS) {
			detector.setResource(textResource);
			IHyperlink[] subLinks = detector.detectHyperlinks(textViewer, region, canShowMultipleHyperlinks);
			links.addAll(Arrays.asList(subLinks));
		}
		if (links.isEmpty()) {
			return null;
		}
		return links.toArray(new IHyperlink[links.size()]);
	}
	
}
