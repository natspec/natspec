package de.devboost.natspec.resource.natspec.ui;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.completion.IMultiCompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;

public class CompletionProposalComputer {

	private static final String UNKNOWN = "<?>";
	
	private boolean isComplete;
	private boolean isFirstNonMatchedPart;
	private CompletionProposalSet proposalSet;
	private String remainingSentence;
	private List<ISyntaxPatternPart> parts;

	/**
	 * Processes the sentence and computes proposals based on the given match and context. This method must be called
	 * before {@link #getProposals()} and before {@link #isComplete()}.
	 */
	public void process(String sentenceToComplete, ISyntaxPatternMatch<? extends Object> match,
			IPatternMatchContext context, List<ISyntaxPatternMatch<? extends Object>> previousMatches) {

		// Reset result fields
		isComplete = false;
		isFirstNonMatchedPart = true;

		// Prepare state
		proposalSet = new CompletionProposalSet();
		remainingSentence = sentenceToComplete;

		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();

		ISyntaxPattern<? extends Object> pattern = match.getPattern();
		parts = pattern.getParts();

		for (ISyntaxPatternPart part : parts) {
			boolean stop = compute(context, partsToMatchesMap, part, pattern, previousMatches);
			if (stop) {
				break;
			}
		}
	}

	private boolean compute(IPatternMatchContext context,
			Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap, ISyntaxPatternPart part,
			ISyntaxPattern<? extends Object> pattern, List<ISyntaxPatternMatch<? extends Object>> previousMatches) {

		if (partsToMatchesMap.containsKey(part)) {
			ISyntaxPatternPartMatch partMatch = partsToMatchesMap.get(part);
			List<Word> matchedWords = partMatch.getMatchedWords();
			String matchedWordsString = explodeText(matchedWords);
			String textToAppend = matchedWordsString;
			// If a static word was matched fuzzy we need to append the actual static word instead of the text that was
			// matched.
			if (part instanceof StaticWord) {
				StaticWord staticWord = (StaticWord) part;
				String staticWordText = staticWord.getText();
				if (staticWordText.length() != matchedWordsString.length()) {
					textToAppend = staticWordText;
				}
			}

			append(Collections.singleton(textToAppend), true);
			// TODO Maybe we should use substring() instead?
			remainingSentence = remainingSentence.replaceFirst(matchedWordsString, "");
		}

		if (remainingSentence.trim().isEmpty()) {
			isComplete = true;
		}

		if (!partsToMatchesMap.containsKey(part)) {
			if (part instanceof ImplicitParameter) {
				return true;
			}
			
			Set<String> completionProposalParts = getCompletionProposalParts(context, part, pattern, previousMatches);
			if (completionProposalParts == null) {
				return true;
			}

			if (isComplete) {
				// matches consumed complete sentence
				// we started from an empty sentence
				append(completionProposalParts, false);
			}

			if (!isComplete && isFirstNonMatchedPart) {
				isFirstNonMatchedPart = false;

				Set<String> lastParts = new LinkedHashSet<String>();
				for (String completionProposalPart : completionProposalParts) {
					if (completionProposalPart.toLowerCase().startsWith(
							remainingSentence.trim().toLowerCase())) {

						// Last word in sentence is part of completion proposal
						lastParts.add(completionProposalPart);
					}
				}

				if (!lastParts.isEmpty()) {
					remainingSentence = "";
					isComplete = true;
					append(lastParts, false);
				}
			}
		}
		return false;
	}

	private Set<String> getCompletionProposalParts(IPatternMatchContext context, ISyntaxPatternPart part,
			ISyntaxPattern<? extends Object> pattern, List<ISyntaxPatternMatch<? extends Object>> previousMatches) {

		Set<String> proposalParts = new LinkedHashSet<String>();
		for (ISyntaxPatternMatch<? extends Object> previousMatch : previousMatches) {
			if (previousMatch.getPattern() != pattern) {
				continue;
			}
			ISyntaxPatternPartMatch previousPartMatch = previousMatch.getPartsToMatchesMap().get(part);
			if (previousPartMatch == null) {
				continue;
			}
			List<Word> matchedWords = previousPartMatch.getMatchedWords();
			String previousWords = WordUtil.INSTANCE.toString(matchedWords);
			proposalParts.add(previousWords);
		}
		
		if (part instanceof ICompletable) {
			ICompletable completable = (ICompletable) part;
			String proposal = completable.getCompletionProposal(context);
			if (proposal != null) {
				proposalParts.add(proposal);
			}
		} else if (part instanceof IMultiCompletable) {
			IMultiCompletable multiCompletable = (IMultiCompletable) part;
			Set<String> proposals = multiCompletable.getCompletionProposals(context);
			if (proposals != null) {
				proposalParts.addAll(proposals);
			}
		}
		
		if (proposalParts.isEmpty()) {
			proposalParts.add(UNKNOWN);
		}
		
		return proposalParts;
	}

	private void append(Set<String> text, boolean skipEmptyStrings) {
		if (text == null) {
			return;
		}
		if (text.isEmpty()) {
			return;
		}
		proposalSet.append(text, skipEmptyStrings);
	}

	private String explodeText(List<Word> matchedWords) {
		StringBuilder explosion = new StringBuilder();
		for (Word word : matchedWords) {
			explosion.append(word.getText());
			if (matchedWords.indexOf(word) < matchedWords.size() - 1) {
				explosion.append(" ");
			}
		}
		return explosion.toString();
	}

	public boolean isComplete() {
		return isComplete;
	}

	public List<String> getProposals() {
		List<String> proposals = proposalSet.getProposals();
		// If there are fewer proposal parts than pattern parts, we must add a space because the user wants to continue
		// typing after the last proposed word even if we can give him further proposals.
		if (proposalSet.getParts() < getCompletableParts()) {
			for (int i = 0; i < proposals.size(); i++) {
				String proposal = proposals.get(i);
				if (proposal.endsWith(" ")) {
					continue;
				}
				proposals.set(i, proposal + " ");
			}
		}
		
		return proposals;
	}

	private int getCompletableParts() {
		int count = 0;
		for (ISyntaxPatternPart part : parts) {
			if (part instanceof ICompletable) {
				count++;
			} else if (part instanceof IMultiCompletable) {
				count++;
			}
		}
		return count;
	}
}
