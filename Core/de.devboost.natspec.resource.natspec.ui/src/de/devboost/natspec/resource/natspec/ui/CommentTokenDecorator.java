package de.devboost.natspec.resource.natspec.ui;

/**
 * This is just a dummy class used to decorate comments in token styling.
 */
public class CommentTokenDecorator {
	
	public final static CommentTokenDecorator INSTANCE = new CommentTokenDecorator();

	private CommentTokenDecorator() {
	}
}
