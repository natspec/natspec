package de.devboost.natspec.resource.natspec.ui;

import java.util.LinkedHashMap;
import java.util.Map;

import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.resource.natspec.INatspecTokenStyle;
import de.devboost.natspec.resource.natspec.mopp.NatspecTokenStyle;

public class TokenStyleProvider {
	
	public final static TokenStyleProvider PROVIDER = new TokenStyleProvider();
	
	private Map<Class<?>, INatspecTokenStyle> styleMap = new LinkedHashMap<Class<?>, INatspecTokenStyle>();
	
	private TokenStyleProvider() {
		int[] color = new int[] {0x80, 0x00, 0x55};
		int[] backgroundColor = null;
		INatspecTokenStyle style = new NatspecTokenStyle(color, backgroundColor, true, false, false, false);
		styleMap.put(StaticWord.class, style);
	}
	
	public INatspecTokenStyle getStyle(Class<?> tokenClass) {
		if (tokenClass == null) {
			return null;
		}
		return styleMap.get(tokenClass);
	}

	public void addStyle(Class<?> clazz, INatspecTokenStyle style) {
		styleMap.put(clazz, style);
	}
}
