package de.devboost.natspec.resource.natspec.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.resource.natspec.INatspecHoverTextProvider;

public class NatspecHoverTextProvider implements INatspecHoverTextProvider {
	
	public final static List<INatspecHoverTextProvider> ADDITIONAL_PROVIDERS = new ArrayList<INatspecHoverTextProvider>();
	
	public String getHoverText(EObject container, EObject referencedObject) {
		return getHoverText(container);
	}
	
	public String getHoverText(EObject object) {
		if (object instanceof Sentence) {
			Sentence sentence = (Sentence) object;
			for (INatspecHoverTextProvider otherProvider : ADDITIONAL_PROVIDERS) {
				String hoverText = otherProvider.getHoverText(object);
				if (hoverText != null && !hoverText.isEmpty()) {
					return hoverText;
				}
			}

			String text = SentenceUtil.INSTANCE.getText(sentence);
			// TODO Escape HTML
			if (SentenceUtil.INSTANCE.isOutlineMarker(sentence)) {
				return "<strong>Outline Marker:</strong>&nbsp;" + text;
			} else if (SentenceUtil.INSTANCE.isComment(sentence)) {
				return "<strong>Comment:</strong>&nbsp;" + text;
			} else {
				return "<strong>Sentence:</strong>&nbsp;" + text;
			}
		}
		
		
		return "";
	}
}
