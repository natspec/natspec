package de.devboost.natspec.resource.natspec.ui;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.resource.natspec.INatspecHoverTextProvider;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;

public abstract class AbstractHoverTextProvider implements INatspecHoverTextProvider {

	public AbstractHoverTextProvider() {
		super();
	}

	protected abstract String getHoverText(EObject object, List<ISyntaxPatternMatch<? extends Object>> matches, String projectName, Sentence sentence);

	@Override
	public String getHoverText(EObject container, EObject referencedObject) {
		return getHoverText(container);
	}

	@Override
	public String getHoverText(EObject object) {
		//FIXME mseifert Check if the use of the constants is right here,
		// or if we should explain and use explicitly true, false here as before
		MatchService matchService = new MatchService(NatSpecPlugin.USE_TREE_BASED_MATCHER,
				NatSpecPlugin.USE_PRIORITIZER);
		Resource resource = object.eResource();
		URI uri = resource.getURI();
		IFile file = new NatspecEclipseProxy().getFileForURI(uri);
		IProject project = file.getProject();
		String projectName = project.getName();
	
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(uri);
		List<EObject> contents = resource.getContents();
		if (contents.isEmpty()) {
			return "";
		}
	
		EObject root = contents.get(0);
		if (!(root instanceof Document)) {
			return "";
		}
	
		Document document = (Document) root;
		List<Sentence> sentences = document.getContents();
		for (Sentence sentence : sentences) {
			String hoverText = getHoverText(object, matchService, context, projectName, sentence);
			if (hoverText != null) {
				return hoverText;
			}
		}
	
		return "";
	}

	private String getHoverText(EObject object, MatchService matchService, IPatternMatchContext context, String projectName, Sentence sentence) {
		List<ISyntaxPatternMatch<? extends Object>> matches = matchService.match(sentence, context);
		return getHoverText(object, matches, projectName, sentence);
	}

}