package de.devboost.natspec.resource.natspec.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * A {@link CompletionProposalSet} represents a set of proposals. The set is
 * derived from a list of alternatives for each part of the proposal. The
 * alternatives are represented as sets of strings. For example, the list [Hello
 * (World,Buddy)] yields the alternatives &quot;Hello World&quot; and
 * &quot;Hello Buddy&quot;.
 */
public class CompletionProposalSet {

	private List<Set<String>> proposalPartSets = new ArrayList<Set<String>>();
	
	public void append(Set<String> alternatives, boolean skipEmptyStrings) {
		Set<String> nonEmptyAlternatives = new LinkedHashSet<String>();
		for (String alternative : alternatives) {
			if (skipEmptyStrings && alternative.trim().isEmpty()) {
				continue;
			}
			nonEmptyAlternatives.add(alternative);
		}
		
		if (nonEmptyAlternatives.isEmpty()) {
			return;
		}
		
		proposalPartSets.add(nonEmptyAlternatives);
	}

	public List<String> getProposals() {
		return doGetProposals(proposalPartSets);
	}

	// TODO We might want to restrict this method to return a maximum number of
	// strings because we compute the Cartesian product of all sets in the list.
	// This can easily grow very large.
	private List<String> doGetProposals(List<Set<String>> proposalPartSets) {
		if (proposalPartSets.isEmpty()) {
			return Arrays.asList("");
		}
		
		Set<String> firstSet = proposalPartSets.get(0);
		if (proposalPartSets.size() == 1) {
			List<String> result = new ArrayList<String>();
			for (String part : firstSet) {
				result.add(part);
			}
			return result;
		} else {
			List<Set<String>> tail = proposalPartSets.subList(1, proposalPartSets.size());
			List<String> tailParts = doGetProposals(tail);
			List<String> result = new ArrayList<String>();
			for (String part : firstSet) {
				for (String tailPart : tailParts) {
					if (tailPart.startsWith(" ")) {
						result.add(part + tailPart);
					} else {
						result.add(part + " " + tailPart);
					}
				}
			}
			return result;
		}
	}

	public int getParts() {
		return proposalPartSets.size();
	}
}
