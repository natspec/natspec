package de.devboost.natspec.resource.natspec.ui;

import org.eclipse.core.resources.IMarker;
import org.eclipse.jface.text.quickassist.IQuickFixableAnnotation;
import org.eclipse.ui.texteditor.MarkerAnnotation;

public class NatspecMarkerAnnotation extends MarkerAnnotation implements IQuickFixableAnnotation {
	
	public NatspecMarkerAnnotation(IMarker marker) {
		super(marker);
	}
	
	public void setQuickFixable(boolean state) {
		// do nothing
	}
	
	public boolean isQuickFixableStateSet() {
		return true;
	}
	
	public boolean isQuickFixable() {
		// every wrong sentence can be fixed
		return true;
	}
}
