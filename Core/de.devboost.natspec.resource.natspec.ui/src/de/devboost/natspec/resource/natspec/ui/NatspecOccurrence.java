package de.devboost.natspec.resource.natspec.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.source.projection.ProjectionViewer;

import de.devboost.natspec.resource.natspec.INatspecTextResource;

/**
 * This class finds the positions to highlight and adds them to the document.
 */
// This class was overridden to disable occurrence highlighting.
public class NatspecOccurrence {
	
	public final static String OCCURRENCE_ANNOTATION_ID = "de.devboost.natspec.resource.natspec.ui.occurences";
	public final static String DECLARATION_ANNOTATION_ID = "de.devboost.natspec.resource.natspec.ui.occurences.declaration";
	
	/**
	 * Creates the Occurrence class to find position to highlight.
	 * 
	 * @param textResource the text resource for location
	 * @param sourceViewer the source viewer for the text
	 * @param tokenScanner the token scanner helps to find the searched tokens
	 */
	public NatspecOccurrence(INatspecTextResource textResource, ProjectionViewer sourceViewer) {
		super();
	}
	
	/**
	 * 
	 * @return the eObject at the current cursor position.
	 */
	public EObject getEObjectAtCurrentPosition() {
		return null;
	}
	
	/**
	 * Returns the token text at the caret.
	 * 
	 * @return the token text
	 */
	public String getTokenText() {
		return "";
	}
	
	/**
	 * Does nothing.
	 */
	public void updateOccurrenceAnnotations() {
	}
	
	/**
	 * Resets the token region to enable remove highlighting if the text is changing.
	 */
	public void resetTokenRegion() {
		// do nothing
	}
}
