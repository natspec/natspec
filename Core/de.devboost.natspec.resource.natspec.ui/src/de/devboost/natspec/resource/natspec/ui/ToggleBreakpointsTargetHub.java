package de.devboost.natspec.resource.natspec.ui;

import java.util.Set;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.ui.actions.IToggleBreakpointsTarget;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchPart;

/**
 * The {@link ToggleBreakpointsTargetHub} delegates all methods calls to a set
 * of {@link IToggleBreakpointsTarget}s.
 */
public class ToggleBreakpointsTargetHub implements IToggleBreakpointsTarget {

	private final Set<IToggleBreakpointsTarget> targets;

	public ToggleBreakpointsTargetHub(
			Set<IToggleBreakpointsTarget> targets) {
		super();
		this.targets = targets;
	}

	@Override
	public void toggleLineBreakpoints(IWorkbenchPart part, ISelection selection)
			throws CoreException {
		for (IToggleBreakpointsTarget target : targets) {
			target.toggleLineBreakpoints(part, selection);
		}
	}

	@Override
	public boolean canToggleLineBreakpoints(IWorkbenchPart part,
			ISelection selection) {

		for (IToggleBreakpointsTarget target : targets) {
			if (target.canToggleLineBreakpoints(part, selection)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void toggleMethodBreakpoints(IWorkbenchPart part,
			ISelection selection) throws CoreException {

		for (IToggleBreakpointsTarget target : targets) {
			target.toggleMethodBreakpoints(part, selection);
		}
	}

	@Override
	public boolean canToggleMethodBreakpoints(IWorkbenchPart part,
			ISelection selection) {

		for (IToggleBreakpointsTarget target : targets) {
			if (target.canToggleMethodBreakpoints(part, selection)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void toggleWatchpoints(IWorkbenchPart part, ISelection selection)
			throws CoreException {
		for (IToggleBreakpointsTarget target : targets) {
			target.toggleWatchpoints(part, selection);
		}
	}

	@Override
	public boolean canToggleWatchpoints(IWorkbenchPart part,
			ISelection selection) {
		for (IToggleBreakpointsTarget target : targets) {
			if (target.canToggleLineBreakpoints(part, selection)) {
				return true;
			}
		}
		return false;
	}
}
