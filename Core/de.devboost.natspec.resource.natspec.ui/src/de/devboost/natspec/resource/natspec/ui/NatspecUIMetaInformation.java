package de.devboost.natspec.resource.natspec.ui;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.ui.actions.IToggleBreakpointsTarget;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.resource.natspec.INatspecHoverTextProvider;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;

public class NatspecUIMetaInformation extends NatspecMetaInformation {
	
	private static final NatspecEclipseProxy NATSPEC_ECLIPSE_PROXY = new NatspecEclipseProxy();
	public final static Set<IToggleBreakpointsTarget> REGISTERED_TOGGLE_BREAKPOINT_TARGETS = new LinkedHashSet<IToggleBreakpointsTarget>();

	public INatspecTokenScanner createTokenScanner(INatspecTextResource resource, NatspecColorManager colorManager) {
		IFile file = NATSPEC_ECLIPSE_PROXY.getFileForResource(resource);
		// return custom token scanner
		String charset = null;
		try {
			charset = file.getCharset();
		} catch (CoreException e) {
			NatspecUIPlugin.logError(
					"Can't determine charset for file " + file, e);
		}
		
		return new CustomTokenScanner(resource.getURI(), charset, colorManager,
				NatSpecPlugin.USE_TREE_BASED_MATCHER,
				NatSpecPlugin.USE_PRIORITIZER);
	}
	
	public INatspecHoverTextProvider getHoverTextProvider() {
		return new NatspecHoverTextProvider();
	}
	
	public NatspecImageProvider getImageProvider() {
		return NatspecImageProvider.INSTANCE;
	}
	
	public NatspecColorManager createColorManager() {
		return new NatspecColorManager();
	}
	
	public NatspecCodeCompletionHelper createCodeCompletionHelper() {
		return new NatspecCodeCompletionHelper();
	}
	
	@SuppressWarnings("rawtypes")
	public Object createResourceAdapter(Object target, Class adapterType, IResource resource) {
		Set<IToggleBreakpointsTarget> targets = Collections.unmodifiableSet(REGISTERED_TOGGLE_BREAKPOINT_TARGETS);
		return new ToggleBreakpointsTargetHub(targets);
	}
}
