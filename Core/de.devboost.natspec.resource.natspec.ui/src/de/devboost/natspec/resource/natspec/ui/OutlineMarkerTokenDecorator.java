package de.devboost.natspec.resource.natspec.ui;

/**
 * This is just a dummy class used to decorate outline markers in token styling.
 */
public class OutlineMarkerTokenDecorator {

	public final static OutlineMarkerTokenDecorator INSTANCE = new OutlineMarkerTokenDecorator();

	private OutlineMarkerTokenDecorator() {
	}
}
