package de.devboost.natspec.resource.natspec.ui;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import de.devboost.natspec.Document;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;

public class NatspecOutlinePageShowOutlineMarkersOnlyAction extends AbstractNatspecOutlinePageAction {
	
	public NatspecOutlinePageShowOutlineMarkersOnlyAction(NatspecOutlinePageTreeViewer treeViewer) {
		super(treeViewer, "Show markers only", IAction.AS_CHECK_BOX);
		initialize("platform:/plugin/de.devboost.natspec.edit/icons/full/obj16/outline_marker.gif");
	}
	
	public void runInternal(final boolean on) {
		getTreeViewer().setFilters(new ViewerFilter[] { new ViewerFilter() {
			
			@Override
			public boolean select(Viewer viewer, Object parentElement, Object element) {
				if (!on) {
					return true;
				}
				
				if (element instanceof Document) {
					return true;
				}
				
				if (element instanceof Sentence) {
					Sentence sentence = (Sentence) element;
					return SentenceUtil.INSTANCE.isOutlineMarker(sentence);
				}
				
				return false;
			}
		}});
		
		getTreeViewer().refresh();
		getTreeViewer().expandAll();
	}
}
