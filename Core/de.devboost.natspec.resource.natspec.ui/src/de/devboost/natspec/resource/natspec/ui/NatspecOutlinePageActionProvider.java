package de.devboost.natspec.resource.natspec.ui;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.IAction;

public class NatspecOutlinePageActionProvider {
	
	public List<IAction> getActions(NatspecOutlinePageTreeViewer treeViewer) {
		List<IAction> defaultActions = new ArrayList<IAction>();
		defaultActions.add(new NatspecOutlinePageLinkWithEditorAction(treeViewer));
		defaultActions.add(new NatspecOutlinePageShowOutlineMarkersOnlyAction(treeViewer));
		defaultActions.add(new NatspecOutlinePageLexicalSortingAction(treeViewer));
		return defaultActions;
	}
}
