package de.devboost.natspec.resource.natspec.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.Resource.Factory.Registry;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;

import de.devboost.natspec.Document;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.Style;
import de.devboost.natspec.resource.natspec.INatspecLocationMap;
import de.devboost.natspec.resource.natspec.INatspecOptions;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.INatspecTokenStyle;
import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;
import de.devboost.natspec.resource.natspec.mopp.NatspecResourceFactory;
import de.devboost.natspec.resource.natspec.mopp.NatspecTokenStyle;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;

/**
 * The {@link CustomTokenScanner} is a special implementation of the {@link INatspecTokenScanner} interface. It is
 * required because syntax highlighting for NatSpec documents requires to match the syntax patterns against the document
 * as NatSpec does not differentiate token types when parsing documents.
 */
public class CustomTokenScanner implements INatspecTokenScanner {

	private static final NatspecMetaInformation NATSPEC_META_INFORMATION = new NatspecMetaInformation();
	private static final String WHITESPACE_TOKEN_NAME = "WHITESPACE";
	private static final String COMMENT_TOKEN_NAME = "COMMENT";

	private final URI contextURI;
	private final String charset;
	private final NatspecColorManager colorManager;
	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;

	private LinkedList<CustomTextToken> tokens;
	private CustomTextToken currentToken;

	public CustomTokenScanner(URI contextURI, String charset,
			NatspecColorManager colorManager, boolean useOptimizedMatcher,
			boolean usePrioritizer) {

		super();
		this.contextURI = contextURI;
		this.charset = charset;
		this.colorManager = colorManager;
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	/**
	 * Sets the document and a range which shall be scanned by this token scanner. The current implementation performs
	 * the scanning right away when this method is called.
	 */
	@Override
	public void setRange(IDocument document, int offset, int length) {
		this.tokens = new LinkedList<CustomTextToken>();
		String text = document.get();
		// Convert text to bytes using the encoding/charset of the file that
		// contains the text.
		byte[] bytes;
		try {
			if (charset == null) {
				bytes = text.getBytes();
			} else {
				bytes = text.getBytes(charset);
			}
		} catch (UnsupportedEncodingException e) {
			NatspecUIPlugin.logError(e.getMessage(), e);
			return;
		}

		Resource resource = loadResource(bytes, charset);
		if (resource == null) {
			return;
		}

		INatspecTextResource natSpecResource = (INatspecTextResource) resource;
		scan(offset, length, natSpecResource);
	}

	private Resource loadResource(byte[] content, String charset) {
		String extension = NATSPEC_META_INFORMATION.getSyntaxName();
		ResourceSet resourceSet = new ResourceSetImpl();
		Registry resourceFactoryRegistry = resourceSet.getResourceFactoryRegistry();
		Map<String, Object> extensionToFactoryMap = resourceFactoryRegistry.getExtensionToFactoryMap();
		extensionToFactoryMap.put(extension, new NatspecResourceFactory());

		URI uri = URI.createURI("temp." + extension);
		Resource resource = resourceSet.createResource(uri);
		if (resource == null) {
			return null;
		}

		Map<Object, Object> loadOptions = new LinkedHashMap<Object, Object>();
		if (charset != null) {
			loadOptions.put(INatspecOptions.OPTION_ENCODING, charset);
		}
		ByteArrayInputStream inputStream = new ByteArrayInputStream(content);
		try {
			resource.load(inputStream, loadOptions);
		} catch (IOException ioe) {
			NatspecUIPlugin.logError(ioe.getMessage(), ioe);
			return null;
		}
		return resource;
	}

	private void scan(int offset, int length, INatspecTextResource resource) {
		IPatternMatchContext context = createContext();

		List<CustomTextToken> newTokens = new ArrayList<CustomTextToken>();

		INatspecLocationMap locationMap = resource.getLocationMap();
		List<EObject> contents = resource.getContents();
		if (!contents.isEmpty()) {
			EObject eObject = contents.get(0);
			if (eObject instanceof Document) {
				Document document = (Document) eObject;
				List<Sentence> sentences = document.getContents();
				int sentenceCount = sentences.size();
				for (int i = 0; i < sentenceCount; i++) {
					Sentence sentence = sentences.get(i);
					scanSentence(context, locationMap, sentence, newTokens);
				}
			}
		}

		updateTokens(offset, length, newTokens);
	}

	/**
	 * Iterates over the given list of (new) tokens, skips the tokens that are not contained in the given region (i.e.,
	 * between offset and offset + length), fills the gaps between the tokens with whitespace tokens and add all
	 * resulting tokens to {@link #tokens}.
	 * 
	 * @param offset
	 *            the offset of the document where the scanning process started
	 * @param length
	 *            the length of the region in the document that was scanned
	 * @param newTokens
	 *            a new list of tokens that was obtained by scanning the document
	 */
	private void updateTokens(int offset, int length,
			List<CustomTextToken> newTokens) {

		// fill holes between tokens with whitespace tokens
		int currentOffset = offset;
		for (CustomTextToken token : newTokens) {
			int tokenOffset = token.getOffset();
			int tokenLength = token.getLength();
			if (tokenOffset + tokenLength < offset) {
				// skip this token. it is before the requested range
				continue;
			}
			if (tokenOffset > offset + length) {
				// skip this token. it is after the requested range.
				continue;
			}
			// token is in range
			if (tokenOffset > currentOffset) {
				// create whitespace token
				this.tokens.add(createWhitespaceToken(currentOffset, tokenOffset - currentOffset, token.getLine()));
			}
			this.tokens.add(token);
			currentOffset = tokenOffset + tokenLength;
		}

		if (this.tokens.isEmpty()) {
			this.tokens.add(createWhitespaceToken(offset, length, -1));
		}
	}

	/**
	 * Scans (i.e., matches) the given sentence and adds the resulting tokens to 'tokens'.
	 * 
	 * @param context
	 *            the context used for matching the document
	 * @param locationMap
	 *            the location map of the resource containing the sentence
	 * @param sentence
	 *            the sentence to match
	 * @param tokens
	 *            a result list where found tokens are added to
	 */
	private void scanSentence(IPatternMatchContext context,
			INatspecLocationMap locationMap, Sentence sentence,
			List<CustomTextToken> tokens) {

		int sentenceCharStart = locationMap.getCharStart(sentence);
		int line = locationMap.getLine(sentence);

		// Handle comments and outline markers
		if (SentenceUtil.INSTANCE.isComment(sentence)) {
			String text = sentence.getText();
			int length = text.length();
			Class<?> decorator = CommentTokenDecorator.class;
			if (SentenceUtil.INSTANCE.isOutlineMarker(sentence)) {
				decorator = OutlineMarkerTokenDecorator.class;
			}
			
			CustomTextToken newToken = new CustomTextToken(decorator, COMMENT_TOKEN_NAME, text, sentenceCharStart,
					length, line, 0, true);
			tokens.add(newToken);
			return;
		}

		// Match sentence
		MatchService matchService = new MatchService(useOptimizedMatcher,
				usePrioritizer);
		List<ISyntaxPatternMatch<? extends Object>> matches = matchService
				.match(sentence, context);
		if (matches.isEmpty()) {
			// If no match is found, no highlighting is applied
			return;
		}

		// Derive tokens from matched parts
		ISyntaxPatternMatch<? extends Object> match = matches.get(0);
		ISyntaxPattern<? extends Object> pattern = match.getPattern();
		Collection<ISyntaxPatternPart> parts = pattern.getParts();
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
		for (ISyntaxPatternPart part : parts) {
			ISyntaxPatternPartMatch partMatch = partsToMatchesMap.get(part);
			scanPartMatch(part, partMatch, sentenceCharStart, line, tokens);
		}
	}

	/**
	 * Scans the match for the given parts and add the resulting token to 'tokens'.
	 * 
	 * @param part
	 *            the matched {@link ISyntaxPatternPart}
	 * @param partMatch
	 *            the match that was found for 'part'
	 * @param sentenceCharStart
	 *            the offset of the sentence containing the partMatch
	 * @param line
	 *            the line of the sentence containing the partMatch
	 * @param tokens
	 *            a result list where the found token is added to
	 */
	private void scanPartMatch(ISyntaxPatternPart part,
			ISyntaxPatternPartMatch partMatch, int sentenceCharStart, int line,
			List<CustomTextToken> tokens) {

		List<Word> words = partMatch.getMatchedWords();
		if (words == null) {
			return;
		}

		boolean canBeUsedForSyntaxHighlighting = true;
		Class<? extends ISyntaxPatternPart> partClass = part.getClass();
		String name = partClass.getName();
		Style style = part.getStyle();
		for (Word word : words) {
			String wordText = word.getText();
			int wordColumn = word.getOffset();
			int wordOffset = sentenceCharStart + word.getOffset();
			int wordLength = wordText.length();
			CustomTextToken wordToken = new CustomTextToken(partClass,
					name, wordText, wordOffset, wordLength, line, wordColumn,
					canBeUsedForSyntaxHighlighting);
			if (style != null) {
				wordToken.setBold(style.isBold());
				wordToken.setItalic(style.isItalic());
				wordToken.setStrikethrough(style.isStrikethrough());
				wordToken.setUnderline(style.isUnderline());
				int[] color = style.getColor();
				if (color != null) {
					wordToken.setColorAsRGB(color);
				}
			}
			tokens.add(wordToken);
		}
	}

	protected IPatternMatchContext createContext() {
		PatternMatchContextFactory factory = PatternMatchContextFactory.INSTANCE;
		return factory.createPatternMatchContext(contextURI);
	}

	private CustomTextToken createWhitespaceToken(int offset, int length,
			int line) {

		String name = WHITESPACE_TOKEN_NAME;
		String text = null;
		int column = -1;
		boolean canBeUsedForSyntaxHighlighting = false;
		return new CustomTextToken(null, name, text, offset, length, line,
				column, canBeUsedForSyntaxHighlighting);
	}

	@Override
	public IToken nextToken() {
		if (tokens.isEmpty()) {
			return Token.EOF;
		}

		currentToken = tokens.removeFirst();

		TextAttribute textAttribute = null;
		Class<?> tokenClass = currentToken.getClazz();
		// use style provider
		INatspecTokenStyle style = TokenStyleProvider.PROVIDER.getStyle(tokenClass);
		if (style != null) {
			if (style instanceof NatspecTokenStyle) {
				NatspecTokenStyle copy = new NatspecTokenStyle(style);
				copyCurrentTokenStyle(currentToken, copy);
				textAttribute = getTextAttribute(copy);
			}
		}

		return new Token(textAttribute);
	}

	private void copyCurrentTokenStyle(CustomTextToken source, NatspecTokenStyle target) {
		Boolean bold = source.getBold();
		if (bold != null) {
			target.setBold(bold);
		}

		Boolean italic = source.getItalic();
		if (italic != null) {
			target.setItalic(italic);
		}

		Boolean underline = source.getUnderline();
		if (underline != null) {
			target.setUnderline(underline);
		}

		Boolean strikethrough = source.getStrikethrough();
		if (strikethrough != null) {
			target.setStrikethrough(strikethrough);
		}

		int[] colorAsRGB = source.getColorAsRGB();
		if (colorAsRGB != null) {
			target.setColorAsRGB(colorAsRGB);
		}
	}

	private TextAttribute getTextAttribute(INatspecTokenStyle tokeStyle) {
		int[] foregroundColorArray = tokeStyle.getColorAsRGB();
		Color foregroundColor = getColor(foregroundColorArray);

		int[] backgroundColorArray = tokeStyle.getBackgroundColorAsRGB();
		Color backgroundColor = getColor(backgroundColorArray);

		int style = getStyleAsInt(tokeStyle);
		return new TextAttribute(foregroundColor, backgroundColor, style);
	}

	private int getStyleAsInt(INatspecTokenStyle tokeStyle) {
		int style = SWT.NORMAL;
		if (tokeStyle.isBold()) {
			style = style | SWT.BOLD;
		}
		if (tokeStyle.isItalic()) {
			style = style | SWT.ITALIC;
		}
		if (tokeStyle.isStrikethrough()) {
			style = style | TextAttribute.STRIKETHROUGH;
		}
		if (tokeStyle.isUnderline()) {
			style = style | TextAttribute.UNDERLINE;
		}
		return style;
	}

	private Color getColor(int[] rgbArray) {
		// We create a local copy of the 'colorManager' field to avoid
		// concurrency issues.
		NatspecColorManager localColorManager = colorManager;
		if (localColorManager == null) {
			return null;
		}
		if (rgbArray == null) {
			return null;
		}

		int red = rgbArray[0];
		int green = rgbArray[1];
		int blue = rgbArray[2];
		RGB rgb = new RGB(red, green, blue);
		return localColorManager.getColor(rgb);
	}

	@Override
	public int getTokenOffset() {
		return currentToken.getOffset();
	}

	@Override
	public int getTokenLength() {
		return currentToken.getLength();
	}

	@Override
	public String getTokenText() {
		return currentToken.getText();
	}
}
