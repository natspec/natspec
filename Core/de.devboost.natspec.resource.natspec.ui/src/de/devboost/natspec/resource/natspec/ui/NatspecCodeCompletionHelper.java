package de.devboost.natspec.resource.natspec.ui;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.INatspecLocationMap;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;

/**
 * A CodeCompletionHelper can be used to derive completion proposals for partial documents. It runs the parser generated
 * by EMFText in a special mode (i.e., the rememberExpectedElements mode). Based on the elements that are expected by
 * the parser for different regions in the document, valid proposals are computed.
 */
public class NatspecCodeCompletionHelper {

	private static final String FEATURE_NAME = "'Code completion'";

	/**
	 * Computes a set of proposals for the given document assuming the cursor is at 'cursorOffset'. The proposals are
	 * derived using the given set of patterns.
	 * 
	 * If there is no valid NatSpec license available, this methods returns an empty array of proposals.
	 * 
	 * @param originalResource
	 *            the resource that contains the text
	 * @param content
	 *            the documents text content
	 * @param cursorOffset
	 *            the cursor position in the text
	 * @param context
	 *            the context for matching the patterns
	 * @param patterns
	 *            the patterns that shall be used to derive completion proposals for
	 * 
	 * @return an array of valid completion proposals
	 */
	public NatspecCompletionProposal[] computeCompletionProposals(INatspecTextResource originalResource, String content,
			int cursorOffset, IPatternMatchContext context, List<ISyntaxPattern<?>> patterns) {

		if (!NatSpecPlugin.hasValidLicense()) {
			// no license, no code completion
			ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();
			errorMessageHelper.showErrorMessage(FEATURE_NAME);
			return new NatspecCompletionProposal[0];
		}

		return doComputeCompletionProposals(originalResource, content, cursorOffset, context, patterns,
				NatSpecPlugin.USE_TREE_BASED_MATCHER, NatSpecPlugin.USE_PRIORITIZER);
	}

	/**
	 * This method is equivalent to {@link #computeCompletionProposals(INatspecTextResource, String, int)}, but does not
	 * check for a valid license.
	 */
	public NatspecCompletionProposal[] doComputeCompletionProposals(INatspecTextResource originalResource,
			String content, int cursorOffset, IPatternMatchContext context, List<ISyntaxPattern<?>> patterns,
			boolean useOptimizedMatcher, boolean usePrioritizer) {

		int previousLinebreak = content.lastIndexOf("\n", cursorOffset - 1) + 1;
		if (previousLinebreak == -1) {
			previousLinebreak = 0;
		}
		int nextLinebreak = content.indexOf("\n", cursorOffset);
		if (nextLinebreak == -1) {
			nextLinebreak = content.length();
		}

		final INatspecLocationMap locationMap = originalResource.getLocationMap();

		List<EObject> elementsAt = locationMap.getElementsBetween(0, previousLinebreak);
		List<Sentence> sentencesForProposalComputation = new LinkedList<Sentence>();
		for (EObject eObject : elementsAt) {
			if (eObject instanceof Sentence) {
				sentencesForProposalComputation.add((Sentence) eObject);
			}
		}
		Collections.sort(sentencesForProposalComputation, new Comparator<Sentence>() {

			@Override
			public int compare(Sentence s1, Sentence s2) {
				return locationMap.getCharStart(s1) - locationMap.getCharStart(s2);
			}
		});

		String partialSentenceText = content.substring(previousLinebreak, cursorOffset);
		Sentence partialSentence = NatspecFactory.eINSTANCE.createSentence();
		partialSentence.setText(partialSentenceText);
		sentencesForProposalComputation.add(partialSentence);
		CompletionHelper completionHelper = new CompletionHelper(useOptimizedMatcher, usePrioritizer);
		List<NatspecCompletionProposal> proposals = completionHelper.getCompletionProposals(context, patterns,
				sentencesForProposalComputation);
		NatspecCompletionProposal[] proposalResults = new NatspecCompletionProposal[proposals.size()];
		proposalResults = proposals.toArray(proposalResults);
		return proposalResults;
	}

	public NatspecCompletionProposal[] computeCompletionProposals(INatspecTextResource resource, String testText,
			int length) {

		URI uri = resource.getURI();

		List<ISyntaxPattern<?>> patterns = SyntaxPatternRegistry.REGISTRY.getAllSyntaxPatterns(uri);

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(uri);

		return computeCompletionProposals(resource, testText, length, context, patterns);
	}
}
