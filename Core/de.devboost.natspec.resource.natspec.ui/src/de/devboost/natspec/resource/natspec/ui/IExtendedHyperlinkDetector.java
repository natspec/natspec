package de.devboost.natspec.resource.natspec.ui;

import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;

import de.devboost.natspec.resource.natspec.INatspecTextResource;

public interface IExtendedHyperlinkDetector extends IHyperlinkDetector {

	public void setResource(INatspecTextResource textResource);

}
