package de.devboost.natspec.resource.natspec.ui;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import de.devboost.license.eclipse.ui.StartupListener;

public class ErrorMessageHelper {
	
	private static final int NO_ID = 1;

	// We do not run this method in a UI job although it does only work when
	// run in the UI thread to make sure that the error message is only shown
	// when the resource changes was triggered by a UI action (e.g., when the
	// user saves a NatSpec file).
	public void showErrorMessage(String feature) {
		Display current = Display.getCurrent();
		if (current == null) {
			return;
		}
		Shell shell = current.getActiveShell();
		if (shell == null) {
			return;
		}
		MessageDialog dialog = new MessageDialog(
				shell,
				"No license found",
				null,
				"You accessed the "
						+ feature
						+ " feature of NatSpec.\n"
						+ "Without a valid license major NatSpec features (e.g., code generation) are disabled.\n"
						+ "Do you want to enter a new license?",
				MessageDialog.ERROR, new String[] { IDialogConstants.YES_LABEL,
						IDialogConstants.NO_LABEL }, 0);
		int returnCode = dialog.open();
		if (returnCode == NO_ID) {
			return;
		}
		
		// open license dialog
		new StartupListener().showConfigurationDialogIfRequired();
	}
}
