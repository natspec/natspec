package de.devboost.natspec.test;

import java.util.Collection;

public interface ITestDataProvider3 {

	public Collection<Object[]> getTestData(boolean p1, boolean p2, boolean p3);
}
