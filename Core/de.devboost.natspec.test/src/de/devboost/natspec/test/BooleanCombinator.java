package de.devboost.natspec.test;

import java.util.ArrayList;
import java.util.Collection;

public class BooleanCombinator {

	public Collection<Object[]> createBooleanCombinations() {
		Collection<Object[]> data = new ArrayList<Object[]>();
		data.add(new Object[] {false, true});
		data.add(new Object[] {false, false});
		data.add(new Object[] {true, true});
		data.add(new Object[] {true, false});
		return data;
	}

	public Collection<Object[]> createBooleanCombinations(ITestDataProvider2 provider) {
		Collection<Object[]> data = new ArrayList<Object[]>();
		data.addAll(provider.getTestData(false, true));
		data.addAll(provider.getTestData(false, false));
		data.addAll(provider.getTestData(true, true));
		data.addAll(provider.getTestData(true, false));
		return data;
	}

	public Collection<Object[]> createBooleanCombinations(ITestDataProvider3 provider) {
		Collection<Object[]> testData = new ArrayList<Object[]>();
		testData.addAll(provider.getTestData(false, false, true));
		testData.addAll(provider.getTestData(false, false, false));
		testData.addAll(provider.getTestData(false, true, true));
		testData.addAll(provider.getTestData(false, true, false));
		testData.addAll(provider.getTestData(true, false, true));
		testData.addAll(provider.getTestData(true, false, false));
		testData.addAll(provider.getTestData(true, true, true));
		testData.addAll(provider.getTestData(true, true, false));
		return testData;
	}
}
