package de.devboost.natspec.test;

import java.util.Collection;

public interface ITestDataProvider2 {

	public Collection<Object[]> getTestData(boolean p1, boolean p2);

}
