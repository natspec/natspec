package de.devboost.natspec.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;

@RunWith(Parameterized.class)
public class SentenceUtilTest {

	private final String input;
	private final String expectedOutput;

	public SentenceUtilTest(String input, String expectedOutput) {
		this.input = input;
		this.expectedOutput = expectedOutput;
	}

	@Test
	public void testGetText() {
		Sentence sentence = NatspecFactory.eINSTANCE.createSentence();
		sentence.setText(input);
		String output = SentenceUtil.INSTANCE.getText(sentence);
		assertThat(output, is(equalTo(expectedOutput)));
	}
	
	@Parameters
	public static Collection<Object[]> getTestData() {
		Collection<Object[]> testData = new ArrayList<Object[]>();
		
		testData.add(new Object[] {"Normal Sentence", "Normal Sentence"});
		testData.add(new Object[] {"  Normal Sentence With Space  ", "Normal Sentence With Space"});
		
		testData.add(new Object[] {"// Comment", "Comment"});
		testData.add(new Object[] {"\t// Comment With Tab", "Comment With Tab"});
		
		testData.add(new Object[] {"/// Outline Marker", "Outline Marker"});
		testData.add(new Object[] {"\t/// Outline Marker With Tab", "Outline Marker With Tab"});
		
		return testData;
	}
}
