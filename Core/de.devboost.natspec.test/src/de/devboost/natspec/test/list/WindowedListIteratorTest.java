package de.devboost.natspec.test.list;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import org.junit.Test;

import de.devboost.natspec.internal.matching.list.WindowedList;
import de.devboost.natspec.internal.matching.list.WindowedListIterator;

/**
 * Test cases for the {@link WindowedListIterator}.
 * 
 * @author Claas Wilke
 */
public class WindowedListIteratorTest {

	private static List<String> emptyList = new WindowedList<String>(
			new ArrayList<String>());
	private static List<String> listA = new WindowedList<String>(
			Arrays.asList(new String[] { "a" }));
	private static List<String> listAB = new WindowedList<String>(
			Arrays.asList(new String[] { "a", "b" }));
	private static List<String> listABC = new WindowedList<String>(
			Arrays.asList(new String[] { "a", "b", "c" }));
	private static List<String> listB = new WindowedList<String>(
			Arrays.asList(new String[] { "b" }));
	private static List<String> listBC = new WindowedList<String>(
			Arrays.asList(new String[] { "b", "c" }));
	private static List<String> listC = new WindowedList<String>(
			Arrays.asList(new String[] { "c" }));

	@Test(expected = UnsupportedOperationException.class)
	public void testAdd() {
		ListIterator<String> itABC = listABC.listIterator();
		itABC.add("a");
	}

	@Test
	public void testHasNext() {
		ListIterator<String> emptyIt = emptyList.listIterator();
		ListIterator<String> itA = listA.listIterator();
		ListIterator<String> itAB = listAB.listIterator();
		ListIterator<String> itABC = listABC.listIterator();
		ListIterator<String> itB = listB.listIterator();
		ListIterator<String> itBC = listBC.listIterator();
		ListIterator<String> itC = listC.listIterator();

		assertFalse(emptyIt.hasNext());
		assertTrue(itA.hasNext());
		assertTrue(itAB.hasNext());
		assertTrue(itABC.hasNext());
		assertTrue(itB.hasNext());
		assertTrue(itBC.hasNext());
		assertTrue(itC.hasNext());
	}

	@Test
	public void testHasPrevious() {
		ListIterator<String> itA = listA.listIterator();
		assertFalse(itA.hasPrevious());

		itA.next();
		assertTrue(itA.hasPrevious());
	}

	@Test
	public void testNext() {
		ListIterator<String> itA = listA.listIterator();
		assertEquals("a", itA.next());

		ListIterator<String> itAB = listAB.listIterator();
		assertEquals("a", itAB.next());
		assertEquals("b", itAB.next());

	}

	@Test
	public void testNextIndex() {
		ListIterator<String> itAB = listAB.listIterator();
		assertEquals(0, itAB.nextIndex());

		itAB.next();
		assertEquals(1, itAB.nextIndex());

		itAB.next();
		assertEquals(2, itAB.nextIndex());
	}

	@Test(expected = NoSuchElementException.class)
	public void testNextNegative() {
		ListIterator<String> emptyIt = emptyList.listIterator();
		emptyIt.next();
	}

	@Test
	public void testPrevious() {
		ListIterator<String> itABC = listABC.listIterator(3);

		assertEquals("c", itABC.previous());
		assertEquals("b", itABC.previous());
		assertEquals("a", itABC.previous());
	}

	@Test
	public void testPreviousIndex() {
		ListIterator<String> itABC = listABC.listIterator(3);
		assertEquals(2, itABC.previousIndex());

		itABC.previous();
		assertEquals(1, itABC.previousIndex());

		itABC.previous();
		assertEquals(0, itABC.previousIndex());

		itABC.previous();
		assertEquals(-1, itABC.previousIndex());
	}

	@Test(expected = NoSuchElementException.class)
	public void testPreviousNegative01() {
		ListIterator<String> emptyIt = emptyList.listIterator();
		emptyIt.previous();
	}

	@Test(expected = NoSuchElementException.class)
	public void testPreviousNegative02() {
		ListIterator<String> itA = listA.listIterator();
		itA.previous();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testRemove() {
		ListIterator<String> itABC = listABC.listIterator();
		itABC.remove();
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testSet() {
		ListIterator<String> itABC = listABC.listIterator();
		itABC.set("a");
	}
}
