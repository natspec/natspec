package de.devboost.natspec.test.list;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import org.junit.Test;

import de.devboost.natspec.internal.matching.list.WindowedList;

/**
 * Test cases for the {@link WindowedList}.
 * 
 * @author Claas Wilke
 */
public class WindowedListTest {

	private static WindowedList<String> emptyList = new WindowedList<String>(
			new ArrayList<String>());
	private static WindowedList<String> listA = new WindowedList<String>(
			Arrays.asList(new String[] { "a" }));
	private static WindowedList<String> listAB = new WindowedList<String>(
			Arrays.asList(new String[] { "a", "b" }));
	private static WindowedList<String> listABBA = new WindowedList<String>(
			Arrays.asList(new String[] { "a", "b", "b", "a" }));
	private static WindowedList<String> listABC = new WindowedList<String>(
			Arrays.asList(new String[] { "a", "b", "c" }));
	private static WindowedList<String> listB = new WindowedList<String>(
			Arrays.asList(new String[] { "b" }));
	private static WindowedList<String> listBC = new WindowedList<String>(
			Arrays.asList(new String[] { "b", "c" }));
	private static WindowedList<String> listC = new WindowedList<String>(
			Arrays.asList(new String[] { "c" }));

	@Test(expected = UnsupportedOperationException.class)
	public void testAdd01() {
		emptyList.add("");
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testAdd02() {
		emptyList.add(0, "");
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testAddAll01() {
		emptyList.addAll(emptyList);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testAddAll02() {
		emptyList.addAll(0, emptyList);
	}

	@Test
	public void testContains() {
		assertTrue(listAB.contains("a"));
		assertTrue(listAB.contains("b"));
		assertFalse(listAB.contains("c"));
	}

	@Test
	public void testContainsAll() {
		assertTrue(listAB.containsAll(listA));
		assertTrue(listAB.containsAll(listAB));
		assertTrue(listAB.containsAll(listB));
		assertTrue(listAB.containsAll(listABBA));
		assertFalse(listAB.containsAll(listABC));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testClear() {
		emptyList.clear();
	}

	@Test
	public void testGet() {
		String elem;

		elem = listABC.get(0);
		assertEquals("a", elem);

		elem = listABC.get(1);
		assertEquals("b", elem);

		elem = listABC.get(2);
		assertEquals("c", elem);

		WindowedList<String> list = new WindowedList<String>(
				Arrays.asList(new String[] { "a", "b", "c" }));
		list.window(1, 2);
		elem = list.get(0);
		assertEquals("b", elem);

		elem = list.get(1);
		assertEquals("c", elem);

		list.window(0, 1);
		elem = list.get(0);
		assertEquals("b", elem);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetNegative01() {
		emptyList.get(-1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetNegative02() {
		emptyList.get(0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testGetNegative03() {
		listABC.get(3);
	}

	@Test
	public void testIncreaseFrontWindow() {
		WindowedList<String> list;

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));

		list.window(2, 1);
		assertEquals(listC, list);

		list.increaseFrontWindow(1);
		assertEquals(listBC, list);

		list.increaseFrontWindow(1);
		assertEquals(listABC, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));

		list.window(2, 1);
		assertEquals(listC, list);

		list.increaseFrontWindow(2);
		assertEquals(listABC, list);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testIncreaseFrontWindowNegative01() {
		emptyList.increaseFrontWindow(1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIncreaseFrontWindowNegative02() {
		emptyList.increaseFrontWindow(-1);
	}

	@Test
	public void testIndexOf() {
		assertEquals(0, listABC.indexOf("a"));
		assertEquals(1, listABC.indexOf("b"));
		assertEquals(2, listABC.indexOf("c"));
		assertEquals(-1, listABC.indexOf("d"));

		assertEquals(0, listABBA.indexOf("a"));
		assertEquals(1, listABBA.indexOf("b"));
	}

	@Test
	public void testIsEmpty() {
		assertTrue(emptyList.isEmpty());
		assertFalse(listA.isEmpty());
		assertFalse(listAB.isEmpty());
	}

	@Test
	public void testIterator() {
		assertNotNull(emptyList.iterator());
	}

	@Test
	public void testLastIndexOf() {
		assertEquals(0, listABC.lastIndexOf("a"));
		assertEquals(1, listABC.lastIndexOf("b"));
		assertEquals(2, listABC.lastIndexOf("c"));
		assertEquals(-1, listABC.lastIndexOf("d"));

		assertEquals(3, listABBA.lastIndexOf("a"));
		assertEquals(2, listABBA.lastIndexOf("b"));
	}

	@Test
	public void testListIterator01() {
		ListIterator<String> it;

		it = emptyList.listIterator();
		assertNotNull(it);

		it = listABC.listIterator();
		assertNotNull(it);
		assertEquals("a", it.next());
		assertEquals("b", it.next());
		assertEquals("c", it.next());
		assertFalse(it.hasNext());

		WindowedList<String> list;
		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(0, 2);
		it = list.listIterator();
		assertNotNull(it);
		assertEquals("a", it.next());
		assertEquals("b", it.next());
		assertFalse(it.hasNext());

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(1, 2);
		it = list.listIterator();
		assertNotNull(it);
		assertEquals("b", it.next());
		assertEquals("c", it.next());
		assertFalse(it.hasNext());
	}

	@Test
	public void testListIterator02() {
		ListIterator<String> it;

		it = emptyList.listIterator(0);
		assertNotNull(it);

		it = listABC.listIterator(1);
		assertNotNull(it);
		assertEquals("b", it.next());
		assertEquals("c", it.next());
		assertFalse(it.hasNext());

		WindowedList<String> list;
		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(0, 2);
		it = list.listIterator(1);
		assertNotNull(it);
		assertEquals("b", it.next());
		assertFalse(it.hasNext());

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(1, 2);
		it = list.listIterator(1);
		assertNotNull(it);
		assertEquals("c", it.next());
		assertFalse(it.hasNext());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testRemove01() {
		emptyList.remove("");
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testRemove02() {
		emptyList.remove(0);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testRemoveAll() {
		emptyList.removeAll(emptyList);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testRetainAll() {
		emptyList.retainAll(emptyList);
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testSet() {
		emptyList.set(0, "a");
	}

	@Test
	public void testSize() {
		assertEquals(0, emptyList.size());
		assertEquals(1, listA.size());
		assertEquals(2, listAB.size());
	}

	@Test
	public void testSublist() {
		List<String> result;

		result = emptyList.subList(0, 0);
		assertEquals(emptyList, result);

		result = listA.subList(0, 0);
		assertEquals(emptyList, result);

		result = listA.subList(0, 1);
		assertEquals(listA, result);

		result = listAB.subList(0, 0);
		assertEquals(emptyList, result);

		result = listAB.subList(0, 1);
		assertEquals(listA, result);

		result = listAB.subList(0, 2);
		assertEquals(listAB, result);

		result = listAB.subList(1, 2);
		assertEquals(listB, result);

		result = listABC.subList(0, 0);
		assertEquals(emptyList, result);

		result = listABC.subList(0, 1);
		assertEquals(listA, result);

		result = listABC.subList(0, 2);
		assertEquals(listAB, result);

		result = listABC.subList(0, 3);
		assertEquals(listABC, result);

		result = listABC.subList(1, 2);
		assertEquals(listB, result);

		result = listABC.subList(1, 3);
		assertEquals(listBC, result);

		result = listABC.subList(2, 3);
		assertEquals(listC, result);

		/* Test sublist of sublist. */
		result = listABC.subList(0, 2);
		result = result.subList(1, 2);
		assertEquals(listB, result);

		result = listABC.subList(1, 3);
		result = result.subList(0, 1);
		assertEquals(listB, result);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSublistNegative01() {
		emptyList.subList(-1, 0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSublistNegative02() {
		emptyList.subList(1, 1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSublistNegative03() {
		emptyList.subList(0, -1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSublistNegative04() {
		emptyList.subList(0, 1);
	}

	@Test
	public void testToArray() {
		assertArrayEquals(new String[] {}, emptyList.toArray());
		assertArrayEquals(new String[] { "a" }, listA.toArray());

		WindowedList<String> list = new WindowedList<String>(
				Arrays.asList(new String[] { "a", "b", "c" }));
		list.window(0, 2);
		assertArrayEquals(new String[] { "a", "b" }, list.toArray());

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(1, 2);
		assertArrayEquals(new String[] { "b", "c" }, list.toArray());

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(1, 2);
		list.window(0, 1);
		assertArrayEquals(new String[] { "b" }, list.toArray());
	}

	@Test
	public void testWindow() {
		WindowedList<String> list;

		list = new WindowedList<String>(new ArrayList<String>());
		list.window(0, 0);
		assertEquals(WindowedListTest.emptyList, emptyList);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a" }));
		list.window(0, 0);
		assertEquals(WindowedListTest.emptyList, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a" }));
		list.window(0, 1);
		assertEquals(WindowedListTest.listA, list);

		list = new WindowedList<String>(
				Arrays.asList(new String[] { "a", "b" }));
		list.window(0, 0);
		assertEquals(WindowedListTest.emptyList, list);

		list = new WindowedList<String>(
				Arrays.asList(new String[] { "a", "b" }));
		list.window(0, 1);
		assertEquals(WindowedListTest.listA, list);

		list = new WindowedList<String>(
				Arrays.asList(new String[] { "a", "b" }));
		list.window(0, 2);
		assertEquals(WindowedListTest.listAB, list);

		list = new WindowedList<String>(
				Arrays.asList(new String[] { "a", "b" }));
		list.window(1, 1);
		assertEquals(WindowedListTest.listB, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(0, 0);
		assertEquals(WindowedListTest.emptyList, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(0, 1);
		assertEquals(WindowedListTest.listA, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(0, 2);
		assertEquals(WindowedListTest.listAB, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(0, 3);
		assertEquals(WindowedListTest.listABC, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(1, 1);
		assertEquals(listB, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(1, 2);
		assertEquals(listBC, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(2, 1);
		assertEquals(listC, list);

		/* Test sublist of sublist. */
		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(0, 2);
		list.window(1, 1);
		assertEquals(listB, list);

		list = new WindowedList<String>(Arrays.asList(new String[] { "a", "b",
				"c" }));
		list.window(1, 2);
		list.window(0, 1);
		assertEquals(listB, list);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testWindowNegative01() {
		emptyList.window(-1, 0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testWindowNegative02() {
		emptyList.window(1, 0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testWindowNegative03() {
		emptyList.window(0, -1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testWindowNegative04() {
		emptyList.window(0, 1);
	}
}
