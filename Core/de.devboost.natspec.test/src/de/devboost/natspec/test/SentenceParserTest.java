package de.devboost.natspec.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.Word;
import de.devboost.natspec.parsing.ISentenceParser;
import de.devboost.natspec.parsing.SentenceParserFactory;

public class SentenceParserTest {

	@Test
	public void testSplitting() {
		ISentenceParser parser = new SentenceParserFactory().createSentenceParser();
		Sentence sentence = NatspecFactory.eINSTANCE.createSentence();
		sentence.setText("|---- ------------------------------------------------------------------------------ ----|");
		List<Word> split = parser.split(sentence);
		assertEquals(3, split.size());
	}
	
	/**
	 * This is a test case to reproduce NATSPEC-324 (Tabs at end of line are not
	 * automatically omitted (sentence does not match)).
	 */
	@Test
	public void testTabAtEnd() {
		ISentenceParser parser = new SentenceParserFactory().createSentenceParser();
		Sentence sentence = NatspecFactory.eINSTANCE.createSentence();
		sentence.setText("some text with tab at the end\t");
		List<Word> split = parser.split(sentence);
		assertEquals(7, split.size());
		assertEquals("end", split.get(6).getText());
	}
}
