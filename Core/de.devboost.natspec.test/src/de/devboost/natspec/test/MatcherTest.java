package de.devboost.natspec.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.patterns.parts.StaticWord;

@RunWith(Parameterized.class)
public class MatcherTest extends AbstractNatSpecTestCase {

	public MatcherTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}
	
	@Test
	public void testSimpleMatching1() {
		SyntaxPatternMock<Object> pattern = createABasicCommandPattern();
		assertMatch(pattern, "", false);
	}

	@Test
	public void testSimpleMatching2() {
		SyntaxPatternMock<Object> pattern = createABasicCommandPattern();
		assertMatch(pattern, "A basic command", true);
	}

	@Test
	public void testSimpleMatching3() {
		SyntaxPatternMock<Object> pattern = createABasicCommandPattern();
		assertMatch(pattern, "A simple command", false);
	}

	@Test
	public void testSimpleMatching4() {
		SyntaxPatternMock<Object> pattern = createABasicCommandPattern();
		assertMatch(pattern, "A command", false);
	}

	@Test
	public void testSimpleMatching5() {
		SyntaxPatternMock<Object> pattern = createABasicCommandPattern();
		assertMatch(pattern, "A command with more words", false);
	}

	private SyntaxPatternMock<Object> createABasicCommandPattern() {
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("A"));
		pattern.addPart(new StaticWord("basic"));
		pattern.addPart(new StaticWord("command"));
		return pattern;
	}
	
	@Test
	public void testSingleArgument() {
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("A"));
		pattern.addPart(new StaticWord("command"));
		pattern.addPart(new StaticWord("with"));
		pattern.addPart(new StaticWord("argument"));
		pattern.addPart(new IntegerArgument());
		
		assertMatch(pattern, "", false);
		// argument is missing: failure
		assertMatch(pattern, "A command with argument", false);
		// argument is given: success
		assertMatch(pattern, "A command with argument 1", true);
		// too many arguments are given: failure
		assertMatch(pattern, "A command with argument 1 2", false);
	}

	@Test
	public void testSubsetPatterns() {
		SyntaxPatternMock<Object> pattern1 = new SyntaxPatternMock<Object>();
		pattern1.addPart(new StaticWord("A"));
		pattern1.addPart(new StaticWord("command"));
		pattern1.addPart(new IntegerArgument());
		
		SyntaxPatternMock<Object> pattern2 = new SyntaxPatternMock<Object>();
		pattern2.addPart(new StaticWord("A"));
		pattern2.addPart(new StaticWord("command"));
		pattern2.addPart(new IntegerArgument());
		pattern2.addPart(new StaticWord("with"));
		pattern2.addPart(new StaticWord("more"));
		
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		patterns.add(pattern1);
		patterns.add(pattern2);
		
		assertMatch(patterns, "", false);
		assertMatch(patterns, "A command 1", true);
		assertMatch(patterns, "A command 1 with", false);
		assertMatch(patterns, "A command 1 with more", true);
	}

	@Test
	public void testMatchingWhitespaceOnlySentence() {
		SyntaxPatternMock<Object> pattern = createABasicCommandPattern();
		
		assertMatch(pattern, " ", false);
		assertMatch(pattern, "\t", false);
	}
}
