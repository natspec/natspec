package de.devboost.natspec.test;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.Word;
import de.devboost.natspec.internal.matching.tree.TreeBasedSyntaxPatternMatcher;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternMatcher;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.matching.SyntaxPatternMatcherFactory;
import de.devboost.natspec.parsing.ISentenceParser;
import de.devboost.natspec.parsing.SentenceParserFactory;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;

public abstract class AbstractNatSpecTestCase {
	
	private ISynonymProvider localSynonymProvider;
	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;
	
	public AbstractNatSpecTestCase(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	protected ISyntaxPatternMatcher createMatcher(boolean useOptimizedMatcher) {
		return SyntaxPatternMatcherFactory.INSTANCE.createSyntaxPatternMatcher(useOptimizedMatcher);
	}

	@Parameters(name = "Optimized matcher: {0} - Prioritizer: {1}")
	public static Collection<Object[]> getTestData() throws UnsupportedTypeException {
		return new BooleanCombinator().createBooleanCombinations();
	}
	
	@Before
	public void setUp() {
		resetMatcher();
	}

	protected void resetMatcher() {
		// We must reset the state of the TreeBasedSyntaxPatternMatcher to make
		// sure that the test runs do not interfere with each other.
		ISyntaxPatternMatcher matcher = SyntaxPatternMatcherFactory.INSTANCE.createSyntaxPatternMatcher(useOptimizedMatcher());
		if (matcher instanceof TreeBasedSyntaxPatternMatcher) {
			TreeBasedSyntaxPatternMatcher patternMatcher = (TreeBasedSyntaxPatternMatcher) matcher;
			patternMatcher.reset();
		}
	}

	public boolean useOptimizedMatcher() {
		return useOptimizedMatcher;
	}

	public boolean usePrioritizer() {
		return usePrioritizer;
	}

	protected ISyntaxPatternMatch<? extends Object> assertMatch(List<ISyntaxPattern<?>> patterns, String text, boolean matchExpected) {
		IPatternMatchContext context = new PatternMatchContextMock(null, localSynonymProvider);
		return assertMatch(patterns, text, matchExpected, context);
	}
	
	protected ISyntaxPatternMatch<? extends Object> assertMatch(ISyntaxPattern<?> pattern, String text, boolean matchExpected) {
		IPatternMatchContext context = new PatternMatchContextMock(null, localSynonymProvider);
		return assertMatch(pattern, text, matchExpected, context);
	}
	
	protected ISyntaxPatternMatch<? extends Object> assertMatch(ISyntaxPattern<?> pattern, String text, boolean matchExpected, IPatternMatchContext context) {
		MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);
		List<ISyntaxPatternMatch<? extends Object>> allMatches = getAllMatches(pattern, text, context);
		List<ISyntaxPatternMatch<? extends Object>> completeMatches = matchService.filterCompleteMatches(allMatches);
		if (matchExpected) {
			assertEquals("There must be one match ([ "+pattern+" ] "+ text +").", 1, completeMatches.size());
			return completeMatches.get(0);
		} else {
			assertEquals("There must not be a match.", 0, completeMatches.size());
			return null;
		}
	}
	
	protected ISyntaxPatternMatch<? extends Object> assertMatch(List<ISyntaxPattern<?>> patterns, String text, boolean matchExpected, IPatternMatchContext context) {
		MatchService matchService = new MatchService(useOptimizedMatcher,
				usePrioritizer);
		List<ISyntaxPatternMatch<? extends Object>> allMatches = getAllMatches(patterns, text, context);
		List<ISyntaxPatternMatch<? extends Object>> completeMatches = matchService.filterCompleteMatches(allMatches);
		if (matchExpected) {
			assertEquals("There must be one match.", 1, completeMatches.size());
			return completeMatches.get(0);
		} else {
			assertEquals("There must not be a match.", 0, completeMatches.size());
			return null;
		}
	}
	
	protected List<ISyntaxPatternMatch<? extends Object>> getAllMatches(
			ISyntaxPattern<? extends Object> pattern, 
			String text, 
			IPatternMatchContext context) {
		
		List<ISyntaxPattern<? extends Object>> patterns = Collections.<ISyntaxPattern<? extends Object>>singletonList(pattern);
		return getAllMatches(patterns, text, context);
	}

	protected List<ISyntaxPatternMatch<? extends Object>> getAllMatches(
			List<ISyntaxPattern<? extends Object>> patterns, 
			String text, 
			IPatternMatchContext context) {
		
		Sentence sentence = NatspecFactory.eINSTANCE.createSentence();
		sentence.setText(text);
		
		ISentenceParser parser = new SentenceParserFactory().createSentenceParser();
		List<Word> words = parser.split(sentence);

		ISyntaxPatternMatcher matcher = createMatcher(useOptimizedMatcher);
		List<ISyntaxPatternMatch<? extends Object>> allMatches = matcher.match(words, patterns, context, true);
		return allMatches;
	}

	public void setLocalSynonymProvider(ISynonymProvider localSynonymProvider) {
		this.localSynonymProvider = localSynonymProvider;
	}

	protected void tearDown() {
		localSynonymProvider = null;
	}
}
