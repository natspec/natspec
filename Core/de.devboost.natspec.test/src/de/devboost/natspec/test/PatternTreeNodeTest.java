package de.devboost.natspec.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import de.devboost.natspec.internal.matching.tree.SyntaxPatternPartHashKey;
import de.devboost.natspec.internal.matching.tree.PatternTreeNode;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.StaticWord;

// FIXME 2.3 Remove export for internal package in de.devboost.natspec
public class PatternTreeNodeTest {

	@Test
	public void testNodeSplitting() {
		
		SyntaxPatternMock<Object> pattern1 = new SyntaxPatternMock<Object>();
		StaticWord wordHello = new StaticWord("Hello");
		pattern1.addPart(wordHello);
		pattern1.addPart(new StaticWord("world!"));
		
		SyntaxPatternMock<Object> pattern2 = new SyntaxPatternMock<Object>();
		// We do intentionally not reuse 'wordHello' because we want to check
		// whether the matcher can handle different objects that are equal.
		pattern2.addPart(new StaticWord("Hello"));
		pattern2.addPart(new StaticWord("funny"));
		pattern2.addPart(new StaticWord("forest!"));

		List<ISyntaxPattern<Object>> patterns = new ArrayList<ISyntaxPattern<Object>>();
		patterns.add(pattern1);
		patterns.add(pattern2);
		
		PatternTreeNode<Object> node = new PatternTreeNode<Object>(patterns);
		assertEquals(2, count(node.getPatterns()));
		node.split();
		assertEquals("Patterns must not be kept after splitting, but pushed to child nodes.", 0, count(node.getPatterns()));
		
		Map<SyntaxPatternPartHashKey, PatternTreeNode<Object>> childMap = node.getChildMap();
		assertEquals(1, childMap.size());
		SyntaxPatternPartHashKey helloWorldKey = new SyntaxPatternPartHashKey(wordHello);
		assertTrue(childMap.containsKey(helloWorldKey));
		
		PatternTreeNode<Object> child = childMap.get(helloWorldKey);
		assertEquals(2, count(child.getPatterns()));
	}

	private int count(Iterator<ISyntaxPattern<Object>> patterns) {
		int count = 0;
		while (patterns.hasNext()) {
			patterns.next();
			count++;
		}
		return count;
	}
}
