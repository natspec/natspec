package de.devboost.natspec.test;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.matching.AbstractPatternMatchContext;

public class PatternMatchContextMock extends AbstractPatternMatchContext {

	public PatternMatchContextMock(URI contextURI) {
		super(contextURI);
	}

	public PatternMatchContextMock(URI contextURI,
			ISynonymProvider localSynonymProvider) {
		super(contextURI, localSynonymProvider);
	}

	@Override
	public Object getLastObjectByType(String qualifiedTypeName) {
		// TODO Auto-generated method stub
		return null;
	}
}
