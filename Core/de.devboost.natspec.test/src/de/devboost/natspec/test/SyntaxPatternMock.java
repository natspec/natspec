package de.devboost.natspec.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.SyntaxPatternMatchFactory;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.testscripting.patterns.IParameterIndexProvider;

public class SyntaxPatternMock<T> implements ISyntaxPattern<Object>, IParameterIndexProvider {

	private List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();


	
	@Override
	public List<ISyntaxPatternPart> getParts() {
		return parts;
	}

	public void addPart(ISyntaxPatternPart part) {
		this.parts.add(part);
	}

	@Override
	public ISyntaxPatternMatch<Object> createPatternMatch(
			IPatternMatchContext context) {
		return new SyntaxPatternMatchFactory().createSyntaxPatternMatch(this, context);
	}

	@Override
	public Object createUserData(ISyntaxPatternMatch<Object> match) {
		// do nothing
		return null;
	}
	
	@Override
	public String toString() {
		return parts.toString();
	}

	@Override
	public boolean allowsOtherMatches() {
		return false;
	}
	
	@Override
	public Map<ISyntaxPatternPart, Integer> getPartToParameterIndexMap() {
		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		
		Integer index = 0;
		for (ISyntaxPatternPart part : parts) {
			if (part instanceof StaticWord) {
				continue;
			}
			partToParameterIndexMap.put(part, index);
			index++;
		}
		return Collections.unmodifiableMap(partToParameterIndexMap);
	}

	@Override
	public String getDocumentation() {
		return null;
	}
}
