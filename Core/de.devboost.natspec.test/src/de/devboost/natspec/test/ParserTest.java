package de.devboost.natspec.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;

import de.devboost.natspec.Document;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.Word;
import de.devboost.natspec.resource.natspec.util.NatspecResourceUtil;

public class ParserTest {

	@Test
	public void testParsingOneSentence() {
		Document content = NatspecResourceUtil.getResourceContent("A simple sentence.");
		assertNotNull(content);
		List<Sentence> contents = content.getContents();
		assertEquals(1, contents.size());
		Sentence sentence = contents.get(0);
		List<Word> words = sentence.getWords();
		System.out.println("ParserTest.testParsing() words = " + words);
		assertEquals(3, words.size());
	}
		
	@Test
	public void testParsingTwoSentences() {
		Document content = NatspecResourceUtil.getResourceContent("A first sentence.\nAnd a second one.");
		List<Sentence> contents = content.getContents();
		assertNotNull(content);
		assertEquals(2, contents.size());
		assertEquals(3, contents.get(0).getWords().size());
		assertEquals(4, contents.get(1).getWords().size());
	}
}
