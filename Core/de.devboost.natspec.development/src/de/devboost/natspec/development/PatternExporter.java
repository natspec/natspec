package de.devboost.natspec.development;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

import com.thoughtworks.xstream.XStream;

import de.devboost.natspec.patterns.ISyntaxPattern;

/**
 * Helper class for exporting a XML representation of patterns to files.
 * 
 * @author cwende
 */
public class PatternExporter {

	/** 
	 * Exports the given list of syntax patterns to the given file.
	 * 
	 * @param patterns the patterns to export
	 * @param file the target file
	 * 
	 * @throws IOException if something goes wrong
	 */
	public void writePatternsToFile(List<ISyntaxPattern<?>> patterns, File file)
			throws IOException {
		
		XStream xStream = new XStream();
		
		OutputStream out = new FileOutputStream(file);
		ObjectOutputStream stream = xStream.createObjectOutputStream(out);
		stream.writeObject(patterns);
		stream.close();
		out.close();
	}
}
