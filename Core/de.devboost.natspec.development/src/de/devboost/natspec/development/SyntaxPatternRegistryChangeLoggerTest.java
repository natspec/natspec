package de.devboost.natspec.development;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.junit.Test;

public class SyntaxPatternRegistryChangeLoggerTest {
	
	@Test
	public void testStackReporting() throws IOException {
		SyntaxPatternRegistryChangeLogger syntaxPatternRegistryChangeLogger = new SyntaxPatternRegistryChangeLogger();
		syntaxPatternRegistryChangeLogger.doEarlyStartup(true);
		syntaxPatternRegistryChangeLogger.notifyPatternRequested("test.uri");
		String accessLog = readFile(syntaxPatternRegistryChangeLogger.getAccessorLogFilePath());
		
		assertTrue(accessLog.contains("de.devboost.natspec.development.SyntaxPatternRegistryChangeLogger - notifyPatternRequested"));
		assertTrue(accessLog.contains("de.devboost.natspec.development.SyntaxPatternRegistryChangeLoggerTest - testStackReporting"));
	}
	
	private String readFile(String pathname) throws IOException {

	    File file = new File(pathname);
	    StringBuilder fileContents = new StringBuilder((int) file.length());
	    Scanner scanner = new Scanner(file);
	    String lineSeparator = System.getProperty("line.separator");

	    try {
	        while (scanner.hasNextLine()) {        
	            fileContents.append(scanner.nextLine() + lineSeparator);
	        }
	        return fileContents.toString();
	    } finally {
	        scanner.close();
	    }
	}
}
