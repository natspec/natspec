package de.devboost.natspec.development;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.eclipse.ui.IStartup;

import de.devboost.essentials.FileUtils;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.registries.IPatternRequestedListener;
import de.devboost.natspec.registries.IRegistryListener;
import de.devboost.natspec.registries.SyntaxPatternRegistry;

/**
 * Helper class to introspect the patterns that are available at a given time of
 * accessing the registry.
 * 
 * @author cwende
 */
// TODO Write exceptions to Eclipse log instead of System.out
public class SyntaxPatternRegistryChangeLogger implements IStartup,
		IPatternRequestedListener, IRegistryListener {

	private final static String FLAG_EXTENSION = "flag";
	private final static String ENABLE_LOGGING_FILE = "enablelogging."
			+ FLAG_EXTENSION;
	private final static String DATE_FORMAT = "yyyy-MM-dd_HHmmss_SSS";
	private final static SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(
			DATE_FORMAT);
	private final static PatternExporter patternExporter = new PatternExporter();

	private final static Logger accessLogger = Logger.getLogger(SyntaxPatternRegistryChangeLogger.class.getName());

	private List<ISyntaxPattern<?>> currentPatterns;
	private boolean currentPatternsExported;
	private String loggingPath;

	@Override
	public void earlyStartup() {
		File loggingDir = new File(getLoggingPath());
		File enableLoggingFile = new File(loggingDir, ENABLE_LOGGING_FILE);
		boolean enableLogging = enableLoggingFile.exists();
		doEarlyStartup(enableLogging);
	}

	public void doEarlyStartup(boolean enableLogging) {
		if (!enableLogging) {
			return;
		}
		SyntaxPatternRegistry.REGISTRY.addRequestListener(this);
		SyntaxPatternRegistry.REGISTRY.addListener(this);
		try {
			FileHandler fileHandler = new FileHandler(getAccessorLogFilePath());
			SimpleFormatter formatter = new SimpleFormatter();
			fileHandler.setFormatter(formatter);

			accessLogger.addHandler(fileHandler);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		accessLogger.info(getClass().getName() + " listening to access in "
				+ SyntaxPatternRegistry.class.getName());
	}

	protected String getAccessorLogFilePath() {
		return getLoggingPath()
				+ "syntax_pattern_access.txt";
	}

	@Override
	public void notifyPatternRequested(String uri) {
		if (!currentPatternsExported) {
			String filename = getLoggingPath() + getCurrentTime()
					+ "_all_patterns.xml";
			File file = new File(filename);
			try {
				patternExporter.writePatternsToFile(currentPatterns, file);
				accessLogger.info("Exported Patterns to "
						+ file.getAbsolutePath());
			} catch (Exception e) {
				accessLogger.severe("Pattern export failed. " + e.toString());
			}
		}
		StringBuffer stack = printAccessorFromStacktrace();
		accessLogger.info("Pattern requested for " + uri + stack.toString());

	}

	private StringBuffer printAccessorFromStacktrace() {
		StringBuffer stack = new StringBuffer();
		stack.append("\nfrom: \n");
		try {
			throw new Exception();
		} catch (Exception e) {
			StackTraceElement[] stackTrace = e.getStackTrace();
			int i = 3;
			for (StackTraceElement stackTraceElement : stackTrace) {
				if (i<1) {
					continue;
				}
				i--;
				stack.append("\t"+stackTraceElement.getClassName() + " - "
						+ stackTraceElement.getMethodName() + ":"
						+ stackTraceElement.getLineNumber() + "\n");
			}
		}
		return stack;
	}

	private String getLoggingPath() {
		if (loggingPath == null) {
			String homeDirectory = System.getProperty("user.home");
			if (homeDirectory == null || homeDirectory.isEmpty()) {
				return "./";
			}
			loggingPath = homeDirectory + File.separator + ".natSpec"
					+ File.separator;
			File loggingDirectory = new File(loggingPath);
			if (loggingDirectory.exists()) {
				Set<String> excludedExtensions = Collections
						.singleton(FLAG_EXTENSION);
				new FileUtils().clearContents(loggingDirectory, null,
						excludedExtensions);
			}
			if (!loggingDirectory.exists()) {
				loggingDirectory.mkdirs();
			}
		}
		return loggingPath;
	}

	private String getCurrentTime() {
		return DATE_FORMATTER.format(new Date());
	}

	@Override
	public void notifyChanged() {
		accessLogger.info("Pattern list updated.");
		currentPatterns = SyntaxPatternRegistry.REGISTRY
				.getAllSyntaxPatterns(null);
		currentPatternsExported = false;
	}
}
