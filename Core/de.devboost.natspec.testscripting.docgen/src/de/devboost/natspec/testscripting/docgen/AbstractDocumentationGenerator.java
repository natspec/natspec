package de.devboost.natspec.testscripting.docgen;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;

public abstract class AbstractDocumentationGenerator {

	public void generateDocumentation(List<ISyntaxPattern<?>> patterns) {
		// create groups of patterns (one for each complex parameter type)
		Multimap<String, ISyntaxPattern<?>> typeNameToPatternMap = getPatternsGroups(patterns);
		
		// sort types alphabetically
		List<String> typeNames = new ArrayList<String>(); 
		typeNames.addAll(typeNameToPatternMap.keySet());
		Collections.sort(typeNames);
		
		// create documentation for patterns associated with types
		for (String type : typeNames) {
			beginType(type);
			Collection<ISyntaxPattern<?>> patternsForType = typeNameToPatternMap.get(type);
			for (ISyntaxPattern<?> pattern : patternsForType) {
				beginPattern(pattern);
			}
			endType(type);
		}
		// create documentation for patterns that are not related to a type
		for (ISyntaxPattern<?> pattern : patterns) {
			if (!typeNameToPatternMap.values().contains(pattern)) {
				beginType(null);
				beginPattern(pattern);
				endType(null);
			}
		}
	}

	protected abstract void beginType(String type);
	protected abstract void endType(String type);
	protected abstract void beginPattern(ISyntaxPattern<?> pattern);

	private Multimap<String, ISyntaxPattern<?>> getPatternsGroups(
			List<ISyntaxPattern<?>> patterns) {
		Multimap<String, ISyntaxPattern<?>> typeNameToPatternMap = HashMultimap.create();
		for (ISyntaxPattern<?> pattern : patterns) {
			List<ISyntaxPatternPart> parts = pattern.getParts();
			for (ISyntaxPatternPart part : parts) {
				if (part instanceof ComplexParameter) {
					ComplexParameter complexParameter = (ComplexParameter) part;
					String typeName = complexParameter.getQualifiedTypeName();
					typeNameToPatternMap.put(typeName, pattern);
				}
			}
		}
		return typeNameToPatternMap;
	}
}
