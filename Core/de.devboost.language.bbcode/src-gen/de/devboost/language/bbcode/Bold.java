/**
 */
package de.devboost.language.bbcode;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bold</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.devboost.language.bbcode.BbcodePackage#getBold()
 * @model
 * @generated
 */
public interface Bold extends CompositeStyle {
} // Bold
