/**
 */
package de.devboost.language.bbcode;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Italic</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.devboost.language.bbcode.BbcodePackage#getItalic()
 * @model
 * @generated
 */
public interface Italic extends CompositeStyle {
} // Italic
