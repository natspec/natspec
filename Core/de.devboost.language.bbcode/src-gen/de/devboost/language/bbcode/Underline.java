/**
 */
package de.devboost.language.bbcode;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Underline</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.devboost.language.bbcode.BbcodePackage#getUnderline()
 * @model
 * @generated
 */
public interface Underline extends CompositeStyle {
} // Underline
