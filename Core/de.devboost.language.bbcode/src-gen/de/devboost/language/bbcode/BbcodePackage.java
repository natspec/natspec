/**
 */
package de.devboost.language.bbcode;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.devboost.language.bbcode.BbcodeFactory
 * @model kind="package"
 * @generated
 */
public interface BbcodePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "bbcode";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://devboost.de/languages/bbcode";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "bbcode";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BbcodePackage eINSTANCE = de.devboost.language.bbcode.impl.BbcodePackageImpl.init();

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.BBCodeTextImpl <em>BB Code Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.BBCodeTextImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getBBCodeText()
	 * @generated
	 */
	int BB_CODE_TEXT = 0;

	/**
	 * The feature id for the '<em><b>Style Parts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BB_CODE_TEXT__STYLE_PARTS = 0;

	/**
	 * The number of structural features of the '<em>BB Code Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BB_CODE_TEXT_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Get Text</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BB_CODE_TEXT___GET_TEXT = 0;

	/**
	 * The operation id for the '<em>Get Invalid Color Styles</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BB_CODE_TEXT___GET_INVALID_COLOR_STYLES = 1;

	/**
	 * The number of operations of the '<em>BB Code Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BB_CODE_TEXT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.StyleImpl <em>Style</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.StyleImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getStyle()
	 * @generated
	 */
	int STYLE = 1;

	/**
	 * The number of structural features of the '<em>Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STYLE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STYLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.CompositeStyleImpl <em>Composite Style</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.CompositeStyleImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getCompositeStyle()
	 * @generated
	 */
	int COMPOSITE_STYLE = 2;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STYLE__CHILDREN = STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Composite Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STYLE_FEATURE_COUNT = STYLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Composite Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_STYLE_OPERATION_COUNT = STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.EmptyStyleImpl <em>Empty Style</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.EmptyStyleImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getEmptyStyle()
	 * @generated
	 */
	int EMPTY_STYLE = 3;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_STYLE__TEXT = STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Empty Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_STYLE_FEATURE_COUNT = STYLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Empty Style</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EMPTY_STYLE_OPERATION_COUNT = STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.BoldImpl <em>Bold</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.BoldImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getBold()
	 * @generated
	 */
	int BOLD = 4;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOLD__CHILDREN = COMPOSITE_STYLE__CHILDREN;

	/**
	 * The number of structural features of the '<em>Bold</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOLD_FEATURE_COUNT = COMPOSITE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Bold</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOLD_OPERATION_COUNT = COMPOSITE_STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.ItalicImpl <em>Italic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.ItalicImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getItalic()
	 * @generated
	 */
	int ITALIC = 5;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITALIC__CHILDREN = COMPOSITE_STYLE__CHILDREN;

	/**
	 * The number of structural features of the '<em>Italic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITALIC_FEATURE_COUNT = COMPOSITE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Italic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITALIC_OPERATION_COUNT = COMPOSITE_STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.StrikethroughImpl <em>Strikethrough</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.StrikethroughImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getStrikethrough()
	 * @generated
	 */
	int STRIKETHROUGH = 6;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRIKETHROUGH__CHILDREN = COMPOSITE_STYLE__CHILDREN;

	/**
	 * The number of structural features of the '<em>Strikethrough</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRIKETHROUGH_FEATURE_COUNT = COMPOSITE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Strikethrough</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRIKETHROUGH_OPERATION_COUNT = COMPOSITE_STYLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.UnderlineImpl <em>Underline</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.UnderlineImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getUnderline()
	 * @generated
	 */
	int UNDERLINE = 7;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDERLINE__CHILDREN = COMPOSITE_STYLE__CHILDREN;

	/**
	 * The number of structural features of the '<em>Underline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDERLINE_FEATURE_COUNT = COMPOSITE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Underline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNDERLINE_OPERATION_COUNT = COMPOSITE_STYLE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link de.devboost.language.bbcode.impl.ColorImpl <em>Color</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.devboost.language.bbcode.impl.ColorImpl
	 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getColor()
	 * @generated
	 */
	int COLOR = 8;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR__CHILDREN = COMPOSITE_STYLE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR__VALUE = COMPOSITE_STYLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR_FEATURE_COUNT = COMPOSITE_STYLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Red</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR___GET_RED = COMPOSITE_STYLE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Green</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR___GET_GREEN = COMPOSITE_STYLE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Blue</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR___GET_BLUE = COMPOSITE_STYLE_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR___IS_VALID = COMPOSITE_STYLE_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Color</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLOR_OPERATION_COUNT = COMPOSITE_STYLE_OPERATION_COUNT + 4;


	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.BBCodeText <em>BB Code Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BB Code Text</em>'.
	 * @see de.devboost.language.bbcode.BBCodeText
	 * @generated
	 */
	EClass getBBCodeText();

	/**
	 * Returns the meta object for the containment reference list '{@link de.devboost.language.bbcode.BBCodeText#getStyleParts <em>Style Parts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Style Parts</em>'.
	 * @see de.devboost.language.bbcode.BBCodeText#getStyleParts()
	 * @see #getBBCodeText()
	 * @generated
	 */
	EReference getBBCodeText_StyleParts();

	/**
	 * Returns the meta object for the '{@link de.devboost.language.bbcode.BBCodeText#getText() <em>Get Text</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Text</em>' operation.
	 * @see de.devboost.language.bbcode.BBCodeText#getText()
	 * @generated
	 */
	EOperation getBBCodeText__GetText();

	/**
	 * Returns the meta object for the '{@link de.devboost.language.bbcode.BBCodeText#getInvalidColorStyles() <em>Get Invalid Color Styles</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Invalid Color Styles</em>' operation.
	 * @see de.devboost.language.bbcode.BBCodeText#getInvalidColorStyles()
	 * @generated
	 */
	EOperation getBBCodeText__GetInvalidColorStyles();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.Style <em>Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Style</em>'.
	 * @see de.devboost.language.bbcode.Style
	 * @generated
	 */
	EClass getStyle();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.CompositeStyle <em>Composite Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Style</em>'.
	 * @see de.devboost.language.bbcode.CompositeStyle
	 * @generated
	 */
	EClass getCompositeStyle();

	/**
	 * Returns the meta object for the containment reference list '{@link de.devboost.language.bbcode.CompositeStyle#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see de.devboost.language.bbcode.CompositeStyle#getChildren()
	 * @see #getCompositeStyle()
	 * @generated
	 */
	EReference getCompositeStyle_Children();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.EmptyStyle <em>Empty Style</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Empty Style</em>'.
	 * @see de.devboost.language.bbcode.EmptyStyle
	 * @generated
	 */
	EClass getEmptyStyle();

	/**
	 * Returns the meta object for the attribute '{@link de.devboost.language.bbcode.EmptyStyle#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see de.devboost.language.bbcode.EmptyStyle#getText()
	 * @see #getEmptyStyle()
	 * @generated
	 */
	EAttribute getEmptyStyle_Text();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.Bold <em>Bold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bold</em>'.
	 * @see de.devboost.language.bbcode.Bold
	 * @generated
	 */
	EClass getBold();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.Italic <em>Italic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Italic</em>'.
	 * @see de.devboost.language.bbcode.Italic
	 * @generated
	 */
	EClass getItalic();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.Strikethrough <em>Strikethrough</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Strikethrough</em>'.
	 * @see de.devboost.language.bbcode.Strikethrough
	 * @generated
	 */
	EClass getStrikethrough();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.Underline <em>Underline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Underline</em>'.
	 * @see de.devboost.language.bbcode.Underline
	 * @generated
	 */
	EClass getUnderline();

	/**
	 * Returns the meta object for class '{@link de.devboost.language.bbcode.Color <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Color</em>'.
	 * @see de.devboost.language.bbcode.Color
	 * @generated
	 */
	EClass getColor();

	/**
	 * Returns the meta object for the attribute '{@link de.devboost.language.bbcode.Color#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.devboost.language.bbcode.Color#getValue()
	 * @see #getColor()
	 * @generated
	 */
	EAttribute getColor_Value();

	/**
	 * Returns the meta object for the '{@link de.devboost.language.bbcode.Color#getRed() <em>Get Red</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Red</em>' operation.
	 * @see de.devboost.language.bbcode.Color#getRed()
	 * @generated
	 */
	EOperation getColor__GetRed();

	/**
	 * Returns the meta object for the '{@link de.devboost.language.bbcode.Color#getGreen() <em>Get Green</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Green</em>' operation.
	 * @see de.devboost.language.bbcode.Color#getGreen()
	 * @generated
	 */
	EOperation getColor__GetGreen();

	/**
	 * Returns the meta object for the '{@link de.devboost.language.bbcode.Color#getBlue() <em>Get Blue</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Blue</em>' operation.
	 * @see de.devboost.language.bbcode.Color#getBlue()
	 * @generated
	 */
	EOperation getColor__GetBlue();

	/**
	 * Returns the meta object for the '{@link de.devboost.language.bbcode.Color#isValid() <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see de.devboost.language.bbcode.Color#isValid()
	 * @generated
	 */
	EOperation getColor__IsValid();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BbcodeFactory getBbcodeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.BBCodeTextImpl <em>BB Code Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.BBCodeTextImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getBBCodeText()
		 * @generated
		 */
		EClass BB_CODE_TEXT = eINSTANCE.getBBCodeText();

		/**
		 * The meta object literal for the '<em><b>Style Parts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BB_CODE_TEXT__STYLE_PARTS = eINSTANCE.getBBCodeText_StyleParts();

		/**
		 * The meta object literal for the '<em><b>Get Text</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BB_CODE_TEXT___GET_TEXT = eINSTANCE.getBBCodeText__GetText();

		/**
		 * The meta object literal for the '<em><b>Get Invalid Color Styles</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BB_CODE_TEXT___GET_INVALID_COLOR_STYLES = eINSTANCE.getBBCodeText__GetInvalidColorStyles();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.StyleImpl <em>Style</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.StyleImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getStyle()
		 * @generated
		 */
		EClass STYLE = eINSTANCE.getStyle();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.CompositeStyleImpl <em>Composite Style</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.CompositeStyleImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getCompositeStyle()
		 * @generated
		 */
		EClass COMPOSITE_STYLE = eINSTANCE.getCompositeStyle();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_STYLE__CHILDREN = eINSTANCE.getCompositeStyle_Children();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.EmptyStyleImpl <em>Empty Style</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.EmptyStyleImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getEmptyStyle()
		 * @generated
		 */
		EClass EMPTY_STYLE = eINSTANCE.getEmptyStyle();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EMPTY_STYLE__TEXT = eINSTANCE.getEmptyStyle_Text();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.BoldImpl <em>Bold</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.BoldImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getBold()
		 * @generated
		 */
		EClass BOLD = eINSTANCE.getBold();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.ItalicImpl <em>Italic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.ItalicImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getItalic()
		 * @generated
		 */
		EClass ITALIC = eINSTANCE.getItalic();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.StrikethroughImpl <em>Strikethrough</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.StrikethroughImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getStrikethrough()
		 * @generated
		 */
		EClass STRIKETHROUGH = eINSTANCE.getStrikethrough();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.UnderlineImpl <em>Underline</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.UnderlineImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getUnderline()
		 * @generated
		 */
		EClass UNDERLINE = eINSTANCE.getUnderline();

		/**
		 * The meta object literal for the '{@link de.devboost.language.bbcode.impl.ColorImpl <em>Color</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.devboost.language.bbcode.impl.ColorImpl
		 * @see de.devboost.language.bbcode.impl.BbcodePackageImpl#getColor()
		 * @generated
		 */
		EClass COLOR = eINSTANCE.getColor();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COLOR__VALUE = eINSTANCE.getColor_Value();

		/**
		 * The meta object literal for the '<em><b>Get Red</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COLOR___GET_RED = eINSTANCE.getColor__GetRed();

		/**
		 * The meta object literal for the '<em><b>Get Green</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COLOR___GET_GREEN = eINSTANCE.getColor__GetGreen();

		/**
		 * The meta object literal for the '<em><b>Get Blue</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COLOR___GET_BLUE = eINSTANCE.getColor__GetBlue();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COLOR___IS_VALID = eINSTANCE.getColor__IsValid();

	}

} //BbcodePackage
