/**
 */
package de.devboost.language.bbcode.impl;

import de.devboost.language.bbcode.BbcodePackage;
import de.devboost.language.bbcode.Bold;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bold</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class BoldImpl extends CompositeStyleImpl implements Bold {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BoldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BbcodePackage.Literals.BOLD;
	}

} //BoldImpl
