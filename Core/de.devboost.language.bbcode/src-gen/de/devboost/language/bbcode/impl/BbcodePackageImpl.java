/**
 */
package de.devboost.language.bbcode.impl;

import de.devboost.language.bbcode.BBCodeText;
import de.devboost.language.bbcode.BbcodeFactory;
import de.devboost.language.bbcode.BbcodePackage;
import de.devboost.language.bbcode.Bold;
import de.devboost.language.bbcode.Color;
import de.devboost.language.bbcode.CompositeStyle;
import de.devboost.language.bbcode.EmptyStyle;
import de.devboost.language.bbcode.Italic;
import de.devboost.language.bbcode.Strikethrough;
import de.devboost.language.bbcode.Style;
import de.devboost.language.bbcode.Underline;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BbcodePackageImpl extends EPackageImpl implements BbcodePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bbCodeTextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass styleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeStyleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass emptyStyleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass boldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass italicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass strikethroughEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass underlineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass colorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.devboost.language.bbcode.BbcodePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BbcodePackageImpl() {
		super(eNS_URI, BbcodeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BbcodePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BbcodePackage init() {
		if (isInited) return (BbcodePackage)EPackage.Registry.INSTANCE.getEPackage(BbcodePackage.eNS_URI);

		// Obtain or create and register package
		BbcodePackageImpl theBbcodePackage = (BbcodePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BbcodePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BbcodePackageImpl());

		isInited = true;

		// Create package meta-data objects
		theBbcodePackage.createPackageContents();

		// Initialize created meta-data
		theBbcodePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBbcodePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BbcodePackage.eNS_URI, theBbcodePackage);
		return theBbcodePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBBCodeText() {
		return bbCodeTextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBBCodeText_StyleParts() {
		return (EReference)bbCodeTextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBBCodeText__GetText() {
		return bbCodeTextEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBBCodeText__GetInvalidColorStyles() {
		return bbCodeTextEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStyle() {
		return styleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeStyle() {
		return compositeStyleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeStyle_Children() {
		return (EReference)compositeStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEmptyStyle() {
		return emptyStyleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEmptyStyle_Text() {
		return (EAttribute)emptyStyleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBold() {
		return boldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getItalic() {
		return italicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStrikethrough() {
		return strikethroughEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnderline() {
		return underlineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getColor() {
		return colorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColor_Value() {
		return (EAttribute)colorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getColor__GetRed() {
		return colorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getColor__GetGreen() {
		return colorEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getColor__GetBlue() {
		return colorEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getColor__IsValid() {
		return colorEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BbcodeFactory getBbcodeFactory() {
		return (BbcodeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		bbCodeTextEClass = createEClass(BB_CODE_TEXT);
		createEReference(bbCodeTextEClass, BB_CODE_TEXT__STYLE_PARTS);
		createEOperation(bbCodeTextEClass, BB_CODE_TEXT___GET_TEXT);
		createEOperation(bbCodeTextEClass, BB_CODE_TEXT___GET_INVALID_COLOR_STYLES);

		styleEClass = createEClass(STYLE);

		compositeStyleEClass = createEClass(COMPOSITE_STYLE);
		createEReference(compositeStyleEClass, COMPOSITE_STYLE__CHILDREN);

		emptyStyleEClass = createEClass(EMPTY_STYLE);
		createEAttribute(emptyStyleEClass, EMPTY_STYLE__TEXT);

		boldEClass = createEClass(BOLD);

		italicEClass = createEClass(ITALIC);

		strikethroughEClass = createEClass(STRIKETHROUGH);

		underlineEClass = createEClass(UNDERLINE);

		colorEClass = createEClass(COLOR);
		createEAttribute(colorEClass, COLOR__VALUE);
		createEOperation(colorEClass, COLOR___GET_RED);
		createEOperation(colorEClass, COLOR___GET_GREEN);
		createEOperation(colorEClass, COLOR___GET_BLUE);
		createEOperation(colorEClass, COLOR___IS_VALID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		compositeStyleEClass.getESuperTypes().add(this.getStyle());
		emptyStyleEClass.getESuperTypes().add(this.getStyle());
		boldEClass.getESuperTypes().add(this.getCompositeStyle());
		italicEClass.getESuperTypes().add(this.getCompositeStyle());
		strikethroughEClass.getESuperTypes().add(this.getCompositeStyle());
		underlineEClass.getESuperTypes().add(this.getCompositeStyle());
		colorEClass.getESuperTypes().add(this.getCompositeStyle());

		// Initialize classes, features, and operations; add parameters
		initEClass(bbCodeTextEClass, BBCodeText.class, "BBCodeText", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBBCodeText_StyleParts(), this.getStyle(), null, "styleParts", null, 1, -1, BBCodeText.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getBBCodeText__GetText(), ecorePackage.getEString(), "getText", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getBBCodeText__GetInvalidColorStyles(), this.getColor(), "getInvalidColorStyles", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(styleEClass, Style.class, "Style", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(compositeStyleEClass, CompositeStyle.class, "CompositeStyle", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCompositeStyle_Children(), this.getStyle(), null, "children", null, 0, -1, CompositeStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(emptyStyleEClass, EmptyStyle.class, "EmptyStyle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEmptyStyle_Text(), ecorePackage.getEString(), "text", null, 1, 1, EmptyStyle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(boldEClass, Bold.class, "Bold", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(italicEClass, Italic.class, "Italic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(strikethroughEClass, Strikethrough.class, "Strikethrough", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(underlineEClass, Underline.class, "Underline", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(colorEClass, Color.class, "Color", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getColor_Value(), ecorePackage.getEString(), "value", null, 1, 1, Color.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getColor__GetRed(), ecorePackage.getEInt(), "getRed", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getColor__GetGreen(), ecorePackage.getEInt(), "getGreen", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getColor__GetBlue(), ecorePackage.getEInt(), "getBlue", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getColor__IsValid(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //BbcodePackageImpl
