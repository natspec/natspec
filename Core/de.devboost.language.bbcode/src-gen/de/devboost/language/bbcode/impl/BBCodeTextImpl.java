/**
 */
package de.devboost.language.bbcode.impl;

import de.devboost.language.bbcode.BBCodeText;
import de.devboost.language.bbcode.BbcodePackage;
import de.devboost.language.bbcode.Color;
import de.devboost.language.bbcode.Style;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BB Code Text</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.devboost.language.bbcode.impl.BBCodeTextImpl#getStyleParts <em>Style Parts</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BBCodeTextImpl extends MinimalEObjectImpl.Container implements BBCodeText {
	/**
	 * The cached value of the '{@link #getStyleParts() <em>Style Parts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStyleParts()
	 * @generated
	 * @ordered
	 */
	protected EList<Style> styleParts;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BBCodeTextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BbcodePackage.Literals.BB_CODE_TEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Style> getStyleParts() {
		if (styleParts == null) {
			styleParts = new EObjectContainmentEList<Style>(Style.class, this, BbcodePackage.BB_CODE_TEXT__STYLE_PARTS);
		}
		return styleParts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Color> getInvalidColorStyles() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BbcodePackage.BB_CODE_TEXT__STYLE_PARTS:
				return ((InternalEList<?>)getStyleParts()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BbcodePackage.BB_CODE_TEXT__STYLE_PARTS:
				return getStyleParts();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BbcodePackage.BB_CODE_TEXT__STYLE_PARTS:
				getStyleParts().clear();
				getStyleParts().addAll((Collection<? extends Style>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BbcodePackage.BB_CODE_TEXT__STYLE_PARTS:
				getStyleParts().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BbcodePackage.BB_CODE_TEXT__STYLE_PARTS:
				return styleParts != null && !styleParts.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BbcodePackage.BB_CODE_TEXT___GET_TEXT:
				return getText();
			case BbcodePackage.BB_CODE_TEXT___GET_INVALID_COLOR_STYLES:
				return getInvalidColorStyles();
		}
		return super.eInvoke(operationID, arguments);
	}

} //BBCodeTextImpl
