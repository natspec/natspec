/**
 */
package de.devboost.language.bbcode.impl;

import de.devboost.language.bbcode.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;

import de.devboost.language.bbcode.BBCodeText;
import de.devboost.language.bbcode.BbcodeFactory;
import de.devboost.language.bbcode.BbcodePackage;
import de.devboost.language.bbcode.Bold;
import de.devboost.language.bbcode.EmptyStyle;
import de.devboost.language.bbcode.Italic;
import de.devboost.language.bbcode.Strikethrough;
import de.devboost.language.bbcode.Underline;
import de.devboost.language.bbcode.custom.BBCodeTextCustom;
import de.devboost.language.bbcode.custom.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BbcodeFactoryImpl extends EFactoryImpl implements BbcodeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static BbcodeFactory init() {
		try {
			BbcodeFactory theBbcodeFactory = (BbcodeFactory)EPackage.Registry.INSTANCE.getEFactory(BbcodePackage.eNS_URI);
			if (theBbcodeFactory != null) {
				return theBbcodeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new BbcodeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BbcodeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case BbcodePackage.BB_CODE_TEXT: return createBBCodeText();
			case BbcodePackage.EMPTY_STYLE: return createEmptyStyle();
			case BbcodePackage.BOLD: return createBold();
			case BbcodePackage.ITALIC: return createItalic();
			case BbcodePackage.STRIKETHROUGH: return createStrikethrough();
			case BbcodePackage.UNDERLINE: return createUnderline();
			case BbcodePackage.COLOR: return createColor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BBCodeText createBBCodeText() {
		BBCodeTextImpl bbCodeText = new BBCodeTextCustom();
		return bbCodeText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmptyStyle createEmptyStyle() {
		EmptyStyleImpl emptyStyle = new EmptyStyleImpl();
		return emptyStyle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Bold createBold() {
		BoldImpl bold = new BoldImpl();
		return bold;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Italic createItalic() {
		ItalicImpl italic = new ItalicImpl();
		return italic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Strikethrough createStrikethrough() {
		StrikethroughImpl strikethrough = new StrikethroughImpl();
		return strikethrough;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Underline createUnderline() {
		UnderlineImpl underline = new UnderlineImpl();
		return underline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Color createColor() {
		ColorImpl color = new ColorCustom();
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BbcodePackage getBbcodePackage() {
		return (BbcodePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static BbcodePackage getPackage() {
		return BbcodePackage.eINSTANCE;
	}

} //BbcodeFactoryImpl
