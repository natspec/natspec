/**
 */
package de.devboost.language.bbcode.impl;

import de.devboost.language.bbcode.BbcodePackage;
import de.devboost.language.bbcode.Strikethrough;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Strikethrough</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class StrikethroughImpl extends CompositeStyleImpl implements Strikethrough {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StrikethroughImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BbcodePackage.Literals.STRIKETHROUGH;
	}

} //StrikethroughImpl
