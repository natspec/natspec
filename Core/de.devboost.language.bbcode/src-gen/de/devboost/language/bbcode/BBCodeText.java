/**
 */
package de.devboost.language.bbcode;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BB Code Text</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.devboost.language.bbcode.BBCodeText#getStyleParts <em>Style Parts</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.devboost.language.bbcode.BbcodePackage#getBBCodeText()
 * @model
 * @generated
 */
public interface BBCodeText extends EObject {
	/**
	 * Returns the value of the '<em><b>Style Parts</b></em>' containment reference list.
	 * The list contents are of type {@link de.devboost.language.bbcode.Style}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Style Parts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Style Parts</em>' containment reference list.
	 * @see de.devboost.language.bbcode.BbcodePackage#getBBCodeText_StyleParts()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Style> getStyleParts();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getText();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	EList<Color> getInvalidColorStyles();

} // BBCodeText
