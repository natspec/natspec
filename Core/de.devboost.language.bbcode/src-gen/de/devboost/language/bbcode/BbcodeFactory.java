/**
 */
package de.devboost.language.bbcode;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.devboost.language.bbcode.BbcodePackage
 * @generated
 */
public interface BbcodeFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BbcodeFactory eINSTANCE = de.devboost.language.bbcode.impl.BbcodeFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>BB Code Text</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BB Code Text</em>'.
	 * @generated
	 */
	BBCodeText createBBCodeText();

	/**
	 * Returns a new object of class '<em>Empty Style</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Empty Style</em>'.
	 * @generated
	 */
	EmptyStyle createEmptyStyle();

	/**
	 * Returns a new object of class '<em>Bold</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bold</em>'.
	 * @generated
	 */
	Bold createBold();

	/**
	 * Returns a new object of class '<em>Italic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Italic</em>'.
	 * @generated
	 */
	Italic createItalic();

	/**
	 * Returns a new object of class '<em>Strikethrough</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Strikethrough</em>'.
	 * @generated
	 */
	Strikethrough createStrikethrough();

	/**
	 * Returns a new object of class '<em>Underline</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Underline</em>'.
	 * @generated
	 */
	Underline createUnderline();

	/**
	 * Returns a new object of class '<em>Color</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Color</em>'.
	 * @generated
	 */
	Color createColor();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	BbcodePackage getBbcodePackage();

} //BbcodeFactory
