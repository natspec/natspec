/**
 */
package de.devboost.language.bbcode;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Strikethrough</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.devboost.language.bbcode.BbcodePackage#getStrikethrough()
 * @model
 * @generated
 */
public interface Strikethrough extends CompositeStyle {
} // Strikethrough
