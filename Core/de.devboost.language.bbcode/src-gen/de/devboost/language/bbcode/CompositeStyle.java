/**
 */
package de.devboost.language.bbcode;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Style</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.devboost.language.bbcode.CompositeStyle#getChildren <em>Children</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.devboost.language.bbcode.BbcodePackage#getCompositeStyle()
 * @model abstract="true"
 * @generated
 */
public interface CompositeStyle extends Style {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link de.devboost.language.bbcode.Style}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see de.devboost.language.bbcode.BbcodePackage#getCompositeStyle_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<Style> getChildren();

} // CompositeStyle
