/**
 */
package de.devboost.language.bbcode;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Style</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.devboost.language.bbcode.BbcodePackage#getStyle()
 * @model abstract="true"
 * @generated
 */
public interface Style extends EObject {
} // Style
