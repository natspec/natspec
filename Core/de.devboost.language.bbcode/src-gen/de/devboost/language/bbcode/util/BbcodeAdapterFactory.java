/**
 */
package de.devboost.language.bbcode.util;

import de.devboost.language.bbcode.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.devboost.language.bbcode.BbcodePackage
 * @generated
 */
public class BbcodeAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BbcodePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BbcodeAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = BbcodePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BbcodeSwitch<Adapter> modelSwitch =
		new BbcodeSwitch<Adapter>() {
			@Override
			public Adapter caseBBCodeText(BBCodeText object) {
				return createBBCodeTextAdapter();
			}
			@Override
			public Adapter caseStyle(Style object) {
				return createStyleAdapter();
			}
			@Override
			public Adapter caseCompositeStyle(CompositeStyle object) {
				return createCompositeStyleAdapter();
			}
			@Override
			public Adapter caseEmptyStyle(EmptyStyle object) {
				return createEmptyStyleAdapter();
			}
			@Override
			public Adapter caseBold(Bold object) {
				return createBoldAdapter();
			}
			@Override
			public Adapter caseItalic(Italic object) {
				return createItalicAdapter();
			}
			@Override
			public Adapter caseStrikethrough(Strikethrough object) {
				return createStrikethroughAdapter();
			}
			@Override
			public Adapter caseUnderline(Underline object) {
				return createUnderlineAdapter();
			}
			@Override
			public Adapter caseColor(Color object) {
				return createColorAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.BBCodeText <em>BB Code Text</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.BBCodeText
	 * @generated
	 */
	public Adapter createBBCodeTextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.Style <em>Style</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.Style
	 * @generated
	 */
	public Adapter createStyleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.CompositeStyle <em>Composite Style</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.CompositeStyle
	 * @generated
	 */
	public Adapter createCompositeStyleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.EmptyStyle <em>Empty Style</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.EmptyStyle
	 * @generated
	 */
	public Adapter createEmptyStyleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.Bold <em>Bold</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.Bold
	 * @generated
	 */
	public Adapter createBoldAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.Italic <em>Italic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.Italic
	 * @generated
	 */
	public Adapter createItalicAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.Strikethrough <em>Strikethrough</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.Strikethrough
	 * @generated
	 */
	public Adapter createStrikethroughAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.Underline <em>Underline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.Underline
	 * @generated
	 */
	public Adapter createUnderlineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.devboost.language.bbcode.Color <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.devboost.language.bbcode.Color
	 * @generated
	 */
	public Adapter createColorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //BbcodeAdapterFactory
