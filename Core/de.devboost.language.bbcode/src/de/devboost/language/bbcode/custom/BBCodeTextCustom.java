package de.devboost.language.bbcode.custom;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import com.google.common.base.Joiner;

import de.devboost.language.bbcode.Color;
import de.devboost.language.bbcode.EmptyStyle;
import de.devboost.language.bbcode.impl.BBCodeTextImpl;

public class BBCodeTextCustom extends BBCodeTextImpl {
	
	@Override
	public String getText() {
		List<String> parts = new ArrayList<String>();
		TreeIterator<EObject> iterator = eAllContents();
		while (iterator.hasNext()) {
			EObject next = (EObject) iterator.next();
			if(next instanceof EmptyStyle){
				EmptyStyle emptyStyle = (EmptyStyle) next;
				parts.add(emptyStyle.getText());
			}
		}
		
		return Joiner.on(' ').skipNulls().join(parts);
	}
	
	@Override
	public EList<Color> getInvalidColorStyles() {
		EList<Color> invalidColorStyles = ECollections.newBasicEList();
		TreeIterator<EObject> iterator = eAllContents();
		while (iterator.hasNext()) {
			EObject next = (EObject) iterator.next();
			if(next instanceof Color){
				Color color = (Color) next;
				if(!color.isValid()){
					invalidColorStyles.add(color);
				}
			}
		}
		return invalidColorStyles;
	}
}
