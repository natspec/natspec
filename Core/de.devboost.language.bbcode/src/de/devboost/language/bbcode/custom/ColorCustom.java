package de.devboost.language.bbcode.custom;

import de.devboost.language.bbcode.impl.ColorImpl;

public class ColorCustom extends ColorImpl {

	private int red = 0;
	private int green = 0;
	private int blue = 0;
	private boolean valid;

	private void extractColors() {
		String colorValueHexString = getValue().replace("#", "");
		if(!((getValue().length() == 7) && (colorValueHexString.length() == 6))){
			valid = false;
			return;
		}
		int colorValueHexInt;
		try {
			colorValueHexInt = Integer.parseInt(colorValueHexString, 16);
		} catch (NumberFormatException e) {
			valid = false;
			return;
		}

		red = (colorValueHexInt & 0xFF0000) >> 16;
		green = (colorValueHexInt & 0xFF00) >> 8;
		blue = (colorValueHexInt & 0xFF);
		
		valid = true;
	}

	@Override
	public int getRed() {
		return this.red;
	}

	@Override
	public int getGreen() {
		return this.green;
	}

	@Override
	public int getBlue() {
		return this.blue;
	}

	@Override
	public boolean isValid() {
		return this.valid;
	}

	@Override
	public void setValue(String newValue) {
		if (newValue == null || newValue.isEmpty()) {
			valid = false;
			return;
		}

		if (!(newValue.startsWith("#"))) {

			String colorString = KnownColors.INSTANCE.getColor(newValue);
			if (colorString == null) {
				this.valid = false;
			} else {
				newValue = "#" + colorString;
			}
		}

		super.setValue(newValue);
		extractColors();
	}

}