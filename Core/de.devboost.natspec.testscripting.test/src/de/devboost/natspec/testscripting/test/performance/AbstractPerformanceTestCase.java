package de.devboost.natspec.testscripting.test.performance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.junit.Before;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;
import de.devboost.natspec.testscripting.patterns.AbstractSyntaxPattern;

public abstract class AbstractPerformanceTestCase {

	private final class SyntaxPatternMock extends AbstractSyntaxPattern<Object> {
		
		private final List<ISyntaxPatternPart> parts;

		private SyntaxPatternMock(int index) {
			super(null);
			this.parts = new ArrayList<ISyntaxPatternPart>();
			this.parts.add(new StaticWord("foo" + index));
		}

		@Override
		public List<ISyntaxPatternPart> getParts() {
			return parts;
		}

		@Override
		public Object createUserData(ISyntaxPatternMatch<Object> match) {
			return null;
		}

		@Override
		public int computeHashCode() {
			// TODO Is this correct?
			return 0;
		}
	}

	@Before
	public void setup() {
		new NatspecMetaInformation().registerResourceFactory();
	}
	
	protected void registerPatterns(int patternCount) {
		final Collection<ISyntaxPattern<? extends Object>> patterns = createPatterns(patternCount);

		SyntaxPatternRegistry registry = SyntaxPatternRegistry.REGISTRY;
		registry.add(new ISyntaxPatternProvider() {
			
			@Override
			public Collection<ISyntaxPattern<? extends Object>> getPatterns(URI uri) {
				return patterns;
			}
		});
	}

	protected Collection<ISyntaxPattern<? extends Object>> createPatterns(
			int patternCount) {
		final Collection<ISyntaxPattern<? extends Object>> patterns = new ArrayList<ISyntaxPattern<? extends Object>>(patternCount);
		for (int i = 0; i < patternCount; i++) {
			patterns.add(new SyntaxPatternMock(i));
		}
		return patterns;
	}
}
