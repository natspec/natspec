package de.devboost.natspec.testscripting.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import de.devboost.natspec.testscripting.NameHelper;

public class NameHelperTest {

	@Test
	public void testGetSimpleName() {
		NameHelper nameHelper = NameHelper.INSTANCE;
		assertEquals("List", nameHelper.getSimpleName("java.util.List"));
		assertEquals("List", nameHelper.getSimpleName("java.util.List<java.util.String>"));
	}

	@Test
	public void testGetTypeArguments() {
		assertTypeArguments("java.util.List<java.util.String>", "java.util.String");
		assertTypeArguments("java.util.Map<String,Object>", "String", "Object");
		assertTypeArguments("java.util.Map<String, Object>", "String", "Object");
		assertTypeArguments("java.util.Map<String, java.util.List<java.util.String>>", "String", "java.util.List<java.util.String>");
		assertTypeArguments("Map<String, Map<String, Object>", "String", "Map<String, Object");
	}

	private void assertTypeArguments(String qualifiedName,
			String... expectedArguments) {

		NameHelper nameHelper = NameHelper.INSTANCE;
		List<String> typeArguments = nameHelper.getTypeArguments(qualifiedName);
		System.out.println("type arguments: " + typeArguments);
		assertEquals("Wrong number of type arguments", expectedArguments.length, typeArguments.size());
		for (String expectedArgument : expectedArguments) {
			assertTrue("Missing type argument " + expectedArgument, typeArguments.contains(expectedArgument));
		}
	}
}
