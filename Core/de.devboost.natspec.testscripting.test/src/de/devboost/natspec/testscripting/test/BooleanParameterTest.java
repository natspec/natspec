package de.devboost.natspec.testscripting.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.patterns.parts.BooleanMatch;
import de.devboost.natspec.testscripting.patterns.parts.BooleanParameter;

@RunWith(Parameterized.class)
public class BooleanParameterTest extends AbstractNatSpecTestCase {

	public BooleanParameterTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testBooleanParameterWithTrueFalseSet() {
		BooleanParameter part = new BooleanParameter("on", "off");

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Turn"));
		pattern.addPart(part);
		
		assertMatch(pattern, "", false);
		assertMatch(pattern, "Turn", false);
		assertBooleanMatch(pattern, "Turn on", true, part);
		assertBooleanMatch(pattern, "Turn off", false, part);
		assertMatch(pattern, "Turn foo", false);
	}

	@Test
	public void testBooleanParameterAtEndWithEmptyFalse() {
		BooleanParameter part = new BooleanParameter("on", "");

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Turn"));
		pattern.addPart(part);
		
		assertMatch(pattern, "", false);
		assertBooleanMatch(pattern, "Turn", false, part);
		assertBooleanMatch(pattern, "Turn on", true, part);
		assertMatch(pattern, "Turn off", false);
		assertMatch(pattern, "Turn foo", false);
	}

	@Test
	public void testBooleanParameterInBetweenWithEmptyFalse() {
		BooleanParameter part = new BooleanParameter("no", "");

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Assert"));
		pattern.addPart(part);
		pattern.addPart(new StaticWord("failure"));
		
		assertMatch(pattern, "", false);
		assertMatch(pattern, "Assert", false);
		assertMatch(pattern, "Assert no", false);
		assertBooleanMatch(pattern, "Assert no failure", true, part);
		assertBooleanMatch(pattern, "Assert failure", false, part);
	}

	@Test
	public void testBooleanParameterAtEndWithEmptyTrue() {
		BooleanParameter part = new BooleanParameter("", "off");

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Turn"));
		pattern.addPart(part);
		
		assertMatch(pattern, "", false);
		assertBooleanMatch(pattern, "Turn", true, part);
		assertMatch(pattern, "Turn on", false);
		assertBooleanMatch(pattern, "Turn off", false, part);
		assertMatch(pattern, "Turn foo", false);
	}

	@Test
	public void testBooleanParameterInBetweenWithEmptyTrue() {
		BooleanParameter part = new BooleanParameter("", "no");

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Assert"));
		pattern.addPart(part);
		pattern.addPart(new StaticWord("success"));
		
		assertMatch(pattern, "", false);
		assertMatch(pattern, "Assert", false);
		assertMatch(pattern, "Assert no", false);
		assertBooleanMatch(pattern, "Assert no success", false, part);
		assertBooleanMatch(pattern, "Assert success", true, part);
	}

	private void assertBooleanMatch(ISyntaxPattern<?> pattern, String input,
			boolean expectedValue, BooleanParameter booleanParameter) {

		boolean matchExpected = true;
		ISyntaxPatternMatch<?> match = assertMatch(pattern, input,
				matchExpected);
		Object partMatch = match.getPartsToMatchesMap().get(booleanParameter);
		assertTrue("Wrong type of match: " + partMatch.getClass().getSimpleName(), partMatch instanceof BooleanMatch);
		boolean value = ((BooleanMatch) partMatch).getValue();
		assertEquals("Wrong value.", expectedValue, value);
	}
}
