package de.devboost.natspec.testscripting.test.context;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContext;
import de.devboost.natspec.testscripting.patterns.AbstractClass;
import de.devboost.natspec.testscripting.patterns.IClass;

public class PatternMatchContextTest {
	
	private static class TypeMock extends AbstractClass {

		private final String qualifiedTypeName;
		private final IClass[] superTypes;

		public TypeMock(String qualifiedTypeName, IClass... superTypes) {
			super();
			this.qualifiedTypeName = qualifiedTypeName;
			this.superTypes = superTypes;
		}
		
		@Override
		public List<? extends IClass> getSuperTypes() {
			return Arrays.asList(superTypes);
		}

		@Override
		public String getQualifiedName() {
			return qualifiedTypeName;
		}

		@Override
		public String getQualifiedNameWithTypeArguments() {
			return getQualifiedName();
		}

		@Override
		public List<? extends IClass> getTypeArguments() {
			return Collections.emptyList();
		}

		@Override
		public Set<String> getAllSuperTypes() {
			Set<String> allSuperTypes = new LinkedHashSet<String>();
			allSuperTypes.add(qualifiedTypeName);
			for (IClass superType : superTypes) {
				allSuperTypes.addAll(superType.getAllSuperTypes());
			}
			return allSuperTypes;
		}
	}

	@Test
	public void testTypeLookup() {
		
		PatternMatchContext context = new PatternMatchContext(null);

		IClass typeA = new TypeMock("A");
		// Type B extends A
		IClass typeB = new TypeMock("B", typeA);
		Object object1 = new ObjectCreation(context, typeA, Collections.<String>emptyList());
		Object object2 = new ObjectCreation(context, typeB, Collections.<String>emptyList());
		context.addObjectToContext(object1);
		context.addObjectToContext(object2);
		
		// Since object2 is an instance of type B, which inherits from type A,
		// we expect object2 here (it was added last to the context)
		assertSame(object2, context.getLastObjectByType("A"));
	}

	/**
	 * A test that checks whether the last added object is correctly recognized
	 * by the {@link PatternMatchContext} class.
	 */
	@Test
	public void testIsLastObject() {
		
		PatternMatchContext context = new PatternMatchContext(null);

		IClass typeA = new TypeMock("A");
		IClass typeB = new TypeMock("B");
		
		Object objectA1 = new ObjectCreation(context, typeA, Collections.<String>emptyList());
		Object objectA2 = new ObjectCreation(context, typeA, Collections.<String>emptyList());
		Object objectB1 = new ObjectCreation(context, typeB, Collections.<String>emptyList());
		
		context.addObjectToContext(objectA1);
		context.addObjectToContext(objectB1);
		context.addObjectToContext(objectA2);
		
		// A2 was that last object of type A that was inserted
		assertSame(objectA2, context.getLastObjectByType("A"));
		// B1 was that last object of type B that was inserted
		assertSame(objectB1, context.getLastObjectByType("B"));
		
		assertFalse(context.isLast(objectA1));
		assertFalse(context.isLast(objectB1));
		assertTrue(context.isLast(objectA2));
	}
}
