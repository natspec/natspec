package de.devboost.natspec.testscripting.test.parts;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.PatternMatchContext;
import de.devboost.natspec.testscripting.patterns.parts.BooleanParameter;

@RunWith(Parameterized.class)
public class BooleanParameterTest {

	private final String sentence;
	private final String trueValue;
	private final String falseValue;
	private final boolean matchExpected;

	public BooleanParameterTest(String sentence, String trueValue, String falseValue, boolean matchExpected) {
		this.sentence = sentence;
		this.trueValue = trueValue;
		this.falseValue = falseValue;
		this.matchExpected = matchExpected;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		Collection<Object[]> data = new ArrayList<Object[]>();
		// Basic positive examples
		data.add(new Object[] { "yes", "yes", "no", true });
		data.add(new Object[] { "no", "yes", "no", true });
		data.add(new Object[] { "yes with more words", "yes", "no", true });
		data.add(new Object[] { "no with more words", "yes", "no", true });
		// Basic negative examples
		data.add(new Object[] { "unknown", "yes", "no", false });
		// Multi-word examples
		data.add(new Object[] { "yes indeed", "yes indeed", "no", true });
		data.add(new Object[] { "no indeed", "yes", "no indeed", true });
		data.add(new Object[] { "yes", "yes indeed", "no", false });
		data.add(new Object[] { "no", "yes", "no indeed", false });
		// Empty examples
		data.add(new Object[] { "yes", "yes", "", true });
		data.add(new Object[] { "something", "yes", "", true });
		data.add(new Object[] { "no", "", "no", true });
		data.add(new Object[] { "something", "", "no", true });
		// Test with synonyms
		data.add(new Object[] { "sure", "yes", "no", true });
		data.add(new Object[] { "sure buddy", "yes", "no", true });
		data.add(new Object[] { "nope", "yes", "no", true });
		data.add(new Object[] { "nope buddy", "yes", "no", true });
		return data;
	}

	@Test
	public void testMatching() {
		BooleanParameter booleanParameter = new BooleanParameter(trueValue, falseValue);
		List<Word> words = getWords();
		ISynonymProvider localSynonymProvider = new ISynonymProvider() {
			
			@Override
			public Set<Set<String>> getSynonyms(URI contextURI) {
				Set<String> synonyms1 = new LinkedHashSet<String>();
				synonyms1.add("yes");
				synonyms1.add("sure");
				Set<String> synonyms2 = new LinkedHashSet<String>();
				synonyms2.add("no");
				synonyms2.add("nope");
				
				Set<Set<String>> synonyms = new LinkedHashSet<Set<String>>();
				synonyms.add(synonyms1);
				synonyms.add(synonyms2);
				return synonyms;
			}
		};
		IPatternMatchContext context = new PatternMatchContext(null, localSynonymProvider);
		
		ISyntaxPatternPartMatch match = booleanParameter.match(words, context);
		if (matchExpected) {
			assertNotNull("Match expected", match);
		} else {
			assertNull("No match expected", match);
		}
	}

	private List<Word> getWords() {
		List<Word> words = new ArrayList<Word>();
		String[] parts = sentence.split(" ");
		for (String part : parts) {
			Word word = NatspecFactory.eINSTANCE.createWord();
			word.setText(part);
			words.add(word);
		}
		return words;
	}
}
