package de.devboost.natspec.testscripting.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

@RunWith(Parameterized.class)
public class ExtendedMatcherTest extends AbstractNatSpecTestCase {

	public ExtendedMatcherTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testSubsetPatterns() {
		SyntaxPatternMock<Object> pattern1 = new SyntaxPatternMock<Object>();
		pattern1.addPart(new StaticWord("Login"));
		pattern1.addPart(new StringParameter());
		
		SyntaxPatternMock<Object> pattern2 = new SyntaxPatternMock<Object>();
		pattern2.addPart(new StaticWord("Login"));
		pattern2.addPart(new StringParameter());
		pattern2.addPart(new StaticWord("at"));
		
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		patterns.add(pattern2);
		patterns.add(pattern1);
		
		assertMatch(patterns, "", false);
		assertMatch(patterns, "Login joe", true);
		assertMatch(patterns, "Login joe at", true);
	}
}
