package de.devboost.natspec.testscripting.test.performance;

import java.util.Collection;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;

@RunWith(Parameterized.class)
public class MatchServicePerformanceTest extends AbstractPerformanceTestCase {

	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;
	
	public MatchServicePerformanceTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		return new BooleanCombinator().createBooleanCombinations();
	}

	@Test
	@Category(Runtime.class)
	@Ignore("Disabled because the test slows down the build (until we've figured out a feasible way to run performance tests).")
	public void testPerformance() {

		int patternCount = 10000;
		registerPatterns(patternCount);
		
		MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);
		Sentence sentence = NatspecFactory.eINSTANCE.createSentence();
		sentence.setText("This is a rather long sentence about nothing");
		
		PatternMatchContextFactory contextFactory = PatternMatchContextFactory.INSTANCE;
		IPatternMatchContext context = contextFactory.createPatternMatchContext(null);
		int matcherRuns = 10000;
		for (int i = 0; i < matcherRuns; i++) {
			matchService.match(sentence, context);
		}
	}
}
