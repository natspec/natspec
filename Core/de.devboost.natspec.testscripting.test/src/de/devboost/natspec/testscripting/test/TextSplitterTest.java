package de.devboost.natspec.testscripting.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import de.devboost.natspec.testscripting.util.Line;
import de.devboost.natspec.testscripting.util.TextSplitter;

public class TextSplitterTest {

	private TextSplitter splitter = TextSplitter.INSTANCE;

	@Test
	public void testSplitting1() {
		List<Line> lines = splitter.split("abc");
		assertEquals(1, lines.size());
		assertLine(lines.get(0), "abc", 0, 0);
	}

	@Test
	public void testSplitting2() {
		List<Line> lines = splitter.split("abc\ndef");
		assertEquals(2, lines.size());
		assertLine(lines.get(0), "abc", 0, 0);
		assertLine(lines.get(1), "def", 1, 4);
	}

	@Test
	public void testSplitting3() {
		List<Line> lines = splitter.split("abc\r\ndef");
		assertEquals(2, lines.size());
		assertLine(lines.get(0), "abc", 0, 0);
		assertLine(lines.get(1), "def", 1, 5);
	}

	@Test
	public void testSplitting4() {
		List<Line> lines = splitter.split("abc\ndef\n");
		assertEquals(2, lines.size());
		assertLine(lines.get(0), "abc", 0, 0);
		assertLine(lines.get(1), "def", 1, 4);
	}

	@Test
	public void testSplitting5() {
		List<Line> lines = splitter.split("\nabc\ndef\n");
		assertEquals(3, lines.size());
		assertLine(lines.get(0), "", 0, 0);
		assertLine(lines.get(1), "abc", 1, 1);
		assertLine(lines.get(2), "def", 2, 5);
	}

	@Test
	public void testSplitting6() {
		List<Line> lines = splitter.split("abc\n\ndef");
		assertEquals(3, lines.size());
		assertLine(lines.get(0), "abc", 0, 0);
		assertLine(lines.get(1), "", 1, 4);
		assertLine(lines.get(2), "def", 2, 5);
	}

	private void assertLine(Line line, String text, int lineNumber, int offset) {
		assertEquals("Wrong text", text, line.getText());
		assertEquals("Wrong line number", lineNumber, line.getPosition().getLine());
		assertEquals("Wrong offset", offset, line.getPosition().getOffset());
	}
}
