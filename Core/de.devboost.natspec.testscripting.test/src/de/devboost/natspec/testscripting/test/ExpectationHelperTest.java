package de.devboost.natspec.testscripting.test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.ExpectationHelper;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.CommonType;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;

@RunWith(Parameterized.class)
public class ExpectationHelperTest extends AbstractNatSpecTestCase {

	public ExpectationHelperTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testExpectations() {
		
		String type = String.class.getName();

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new ImplicitParameter(type));
		pattern.addPart(new StaticWord("Get"));
		pattern.addPart(new StaticWord("some"));
		StaticWord beer = new StaticWord("beer");
		pattern.addPart(beer);
		
		String text = "Get some";
		
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		CommonType type2 = new CommonType(String.class.getName());
		List<String> identifiers = Arrays.asList(new String[] {"wine"});
		context.addObjectToContext(new ObjectCreation(context, type2, identifiers));
		
		List<ISyntaxPatternMatch<? extends Object>> allMatches = getAllMatches(pattern, text, context);
		for (ISyntaxPatternMatch<? extends Object> match : allMatches) {
			System.out.println(match);
		}
		List<ISyntaxPatternPart> parts = new ExpectationHelper().getExpectedParts(allMatches);
		
		assertEquals(1, parts.size());
		assertEquals(beer, parts.get(0));
	}
}
