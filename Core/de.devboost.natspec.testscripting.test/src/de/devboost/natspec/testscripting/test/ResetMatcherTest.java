package de.devboost.natspec.testscripting.test;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.internal.matching.tree.TreeBasedSyntaxPatternMatcher;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.DefaultSentenceMatchParameter;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;

/**
 * This test reproduces a problem that was caused by the
 * {@link TreeBasedSyntaxPatternMatcher} because it has a state which was not
 * correctly reset when a set of empty patterns was matched after matching
 * against a non-empty set of patterns.
 */
@RunWith(Parameterized.class)
public class ResetMatcherTest extends AbstractNatSpecTestCase {

	public ResetMatcherTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testReset() {

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new DefaultSentenceMatchParameter());

		String text = "Sentence one";

		assertMatches(
				Collections
						.<ISyntaxPattern<? extends Object>> singletonList(pattern),
				text, 1);
		assertMatches(
				Collections
						.<ISyntaxPattern<? extends Object>> emptyList(),
				text, 0);
	}

	private void assertMatches(List<ISyntaxPattern<? extends Object>> patterns,
			String text, int expectedMatches) {
		
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		List<ISyntaxPatternMatch<? extends Object>> allMatches = getAllMatches(
				patterns, text, context);
		for (ISyntaxPatternMatch<? extends Object> match : allMatches) {
			System.out.println(match);
		}
		assertEquals(expectedMatches, allMatches.size());
	}
	
	protected void resetMatcher() {
		// We do intentionally override this method to NOT reset the matcher
	}
}
