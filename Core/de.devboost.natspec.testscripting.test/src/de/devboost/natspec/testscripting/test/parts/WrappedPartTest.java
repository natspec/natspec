package de.devboost.natspec.testscripting.test.parts;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.patterns.parts.DateParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;
import de.devboost.natspec.testscripting.patterns.parts.WrappedPart;

public class WrappedPartTest {

	@Test
	public void testIssueNATSPEC215() {
		// To check that NATSPEC-215 is fixed, it is sufficient to perform the
		// matching. If a NPE is throw JUnit will complain.
		match("(abc)", "(", ")");
		match("abc", "(", ")");
	}

	@Test
	public void testIssueNATSPEC230() {
		assertNoMatch(match("(abc)", "(", "):"));
		assertMatch(match("(abc):", "(", "):"));
	}
	
	@Test
	public void testBrokenNatSpec() {
		assertMatch(match("abc,", "", ","));
		assertMatch(match("abc, de", "", ","));
	}
	
	@Test
	public void testIssueNATSPEC362() {
		assertMatch(match("(18.02.2015)", "(", ")", new DateParameter()));
		assertMatch(match("(18.02.2015 12:12)", "(", ")", new DateParameter()));
		assertMatch(match("(18.02.2015 12:12:12)", "(", ")", new DateParameter()));
	}

	@Test
	public void testIssueNATSPEC373() {
		// Check that the exception is not present anymore
		match("\" joe\"", "\"", "\"", new StringParameter());
		match("\"joe \"", "\"", "\"", new StringParameter());
		// FIXME NATSPEC-373 Enable this test if its ok to add testscripting java as dependency
		//match("\" joe\"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class)));
		//match("\"joe \"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class)));
		
		// FIXME NATSPEC-373 Enable this test if we decided if this should match or not
		//assertMatch(match("\" joe\"", "\"", "\"", new StringParameter()));
		//assertMatch(match("\"joe \"", "\"", "\"", new StringParameter()));
		//assertMatch(match("\"joe john max\"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class))));
		//assertMatch(match("\" joe\"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class))));
		//assertMatch(match("\"joe \"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class))));
	}
	
	private void assertMatch(ISyntaxPatternPartMatch match) {
		assertNotNull(match);
		assertTrue(match.hasMatched());
	}

	private void assertNoMatch(ISyntaxPatternPartMatch match) {
		assertNull(match);
	}

	protected ISyntaxPatternPartMatch match(String input, String prefix, String suffix) {
		ISyntaxPatternPart delegate = new StringParameter();
		return match(input, prefix, suffix, delegate);
	}
	
	protected ISyntaxPatternPartMatch match(String input, String prefix, String suffix, ISyntaxPatternPart delegate) {
		WrappedPart wrappedPart = new WrappedPart(delegate, prefix, suffix);
		
		List<Word> words = new ArrayList<Word>();
		String[] stringWords = input.split(" ");
		for (String stringWord : stringWords) {
			Word word = NatspecFactory.eINSTANCE.createWord();
			word.setText(stringWord);
			words.add(word);
		}
		
		return wrappedPart.match(words, null);
	}
}
