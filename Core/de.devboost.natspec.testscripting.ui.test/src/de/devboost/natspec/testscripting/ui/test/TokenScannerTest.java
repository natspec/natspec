package de.devboost.natspec.testscripting.ui.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.resource.natspec.ui.CustomTokenScanner;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.testscripting.patterns.AbstractSyntaxPattern;

@RunWith(Parameterized.class)
public class TokenScannerTest extends AbstractNatSpecTestCase {

	private final class TestPattern extends AbstractSyntaxPattern<Object> {

		private List<ISyntaxPatternPart> parts;
		
		public TestPattern() {
			super(null);
			parts = new ArrayList<ISyntaxPatternPart>();
			parts.add(new StaticWord("A"));
			parts.add(new StaticWord("command"));
		}
		
		@Override
		public List<ISyntaxPatternPart> getParts() {
			return parts;
		}

		@Override
		public Object createUserData(ISyntaxPatternMatch<Object> match) {
			// do nothing
			return null;
		}
	}

	public TokenScannerTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Parameters(name = "{index} : useOptimizedMatcher={0}, usePrioritizer={1}")
	public static Collection<Object[]> getTestData() {
		return new BooleanCombinator().createBooleanCombinations();
	}

	@Test
	public void testScanning() {
		ISyntaxPattern<? extends Object> pattern = new TestPattern();
		List<ISyntaxPattern<? extends Object>> patterns = new ArrayList<ISyntaxPattern<? extends Object>>();
		patterns.add(pattern);
		
		String input = "A command";
		TokenScannerTestHelper helper = new TokenScannerTestHelper(null,
				useOptimizedMatcher(), usePrioritizer(), patterns);
		List<IToken> tokens = helper.getTokens(input);
		// two normal tokens and one whitespace token
		assertEquals(3, tokens.size());
		
		List<Integer> offsets = getTokenOffsets(input, patterns);
		assertEquals(3, offsets.size());
		assertEquals(0, offsets.get(0).intValue());
		// whitespace token starts at offset 1
		assertEquals(1, offsets.get(1).intValue());
		// token 'command' starts at offset 2
		assertEquals(2, offsets.get(2).intValue());
		
		List<Integer> lengths = getTokenLengths(input, patterns);
		assertEquals(3, lengths.size());
		assertEquals(1, lengths.get(0).intValue());
		assertEquals(1, lengths.get(1).intValue());
		// token 'command' starts at offset 2
		assertEquals(7, lengths.get(2).intValue());

		input = "A command\nA command";
		offsets = getTokenOffsets(input, patterns);
		assertEquals(7, offsets.size());
		assertEquals(0, offsets.get(0).intValue());
		assertEquals(1, offsets.get(1).intValue());
		assertEquals(2, offsets.get(2).intValue());
		assertEquals(9, offsets.get(3).intValue());
		assertEquals(10, offsets.get(4).intValue());
		assertEquals(11, offsets.get(5).intValue());
		assertEquals(12, offsets.get(6).intValue());
		
		lengths = getTokenLengths(input, patterns);
		assertEquals(7, lengths.size());
		assertEquals(1, lengths.get(0).intValue());
		assertEquals(1, lengths.get(1).intValue());
		assertEquals(7, lengths.get(2).intValue());
		assertEquals(1, lengths.get(3).intValue());
		assertEquals(1, lengths.get(4).intValue());
		assertEquals(1, lengths.get(5).intValue());
		assertEquals(7, lengths.get(6).intValue());

		lengths = getTokenLengths(input, 10, input.length(), patterns);
		assertEquals(3, lengths.size());
		assertEquals(1, lengths.get(0).intValue());
		assertEquals(1, lengths.get(1).intValue());
		assertEquals(7, lengths.get(2).intValue());

		input = "Something invalid";
		offsets = getTokenOffsets(input, patterns);
		assertEquals(1, offsets.size());
		assertEquals(0, offsets.get(0).intValue());

		lengths = getTokenLengths(input, 10, input.length(), patterns);
		assertEquals(1, lengths.size());
		assertEquals(input.length(), lengths.get(0).intValue());
	}

	private List<Integer> getTokenOffsets(String input, List<ISyntaxPattern<? extends Object>> patterns) {
		CustomTokenScanner scanner = new FixedPatternTokenScanner(useOptimizedMatcher(), usePrioritizer(), patterns);
		scanner.setRange(new DocumentMock(input), 0, input.length());
		
		List<Integer> offsets = new ArrayList<Integer>();
		while (scanner.nextToken() != Token.EOF) {
			offsets.add(scanner.getTokenOffset());
		}
		return offsets;
	}
	
	private List<Integer> getTokenLengths(String input, List<ISyntaxPattern<? extends Object>> patterns) {
		return getTokenLengths(input, 0, input.length(), patterns);
	}

	private List<Integer> getTokenLengths(String input, int start, int end, List<ISyntaxPattern<? extends Object>> patterns) {
		CustomTokenScanner scanner = new FixedPatternTokenScanner(useOptimizedMatcher(), usePrioritizer(), patterns);
		scanner.setRange(new DocumentMock(input), start, end);
		
		List<Integer> lengths = new ArrayList<Integer>();
		while (scanner.nextToken() != Token.EOF) {
			lengths.add(scanner.getTokenLength());
		}
		return lengths;
	}
}
