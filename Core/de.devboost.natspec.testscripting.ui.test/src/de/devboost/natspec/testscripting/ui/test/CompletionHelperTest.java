package de.devboost.natspec.testscripting.ui.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.resource.natspec.ui.CompletionHelper;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.CommonType;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

@RunWith(Parameterized.class)
public class CompletionHelperTest {

	private final static String qualifiedTypeName = "TypeA";

	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;

	public CompletionHelperTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		return new BooleanCombinator().createBooleanCombinations();
	}

	@Test
	public void testStaticWordCompletion1() {
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Hello"));
		patterns.add(pattern);

		List<String> expectedProposals = new ArrayList<String>();
		expectedProposals.add("Hello");

		assertProposals(patterns, expectedProposals);
	}

	@Test
	public void testStaticWordCompletion2() {
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Hello"));
		patterns.add(pattern);

		List<String> expectedProposals = new ArrayList<String>();
		expectedProposals.add("Hello");

		assertProposals(patterns, expectedProposals, Collections.<String> emptyList(), "H");

		// We do not expect proposals after an 'X'
		expectedProposals.clear();
		assertProposals(patterns, expectedProposals, Collections.<String> emptyList(), "X");
	}

	@Test
	public void testTwoStaticWordCompletion() {
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Hello"));
		pattern.addPart(new StaticWord("World"));
		patterns.add(pattern);

		List<String> expectedProposals = new ArrayList<String>();
		expectedProposals.add("Hello World");

		assertProposals(patterns, expectedProposals);
	}

	@Test
	public void testComplexParameterCompletion() {

		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		SyntaxPatternMock<Object> createPattern = createCreateObjectPattern("Object1");
		SyntaxPatternMock<Object> usePattern = createUseObjectPattern();

		patterns.add(createPattern);
		patterns.add(usePattern);

		List<String> expectedProposals = new ArrayList<String>();
		expectedProposals.add("Create Object1");
		expectedProposals.add("Use <?>");

		assertProposals(patterns, expectedProposals);
	}

	@Test
	public void testComplexParameterCompletion2() {

		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		SyntaxPatternMock<Object> usePattern = createUseObjectPattern();

		patterns.add(createCreateObjectPattern("Object1"));
		patterns.add(createCreateObjectPattern("Object2"));
		patterns.add(usePattern);

		List<String> expectedProposals = new ArrayList<String>();
		expectedProposals.add("Create Object1");
		expectedProposals.add("Create Object2");
		expectedProposals.add("Use Object1");
		expectedProposals.add("Use Object2");

		List<String> sentences = new ArrayList<String>();
		sentences.add("Create Object1");
		sentences.add("Create Object2");

		assertProposals(patterns, expectedProposals, sentences, "");
	}

	@Test
	public void testImplicitParameterCompletion2() {
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new ImplicitParameter("java.lang.String"));
		pattern.addPart(new StaticWord("ImplicitHello"));

		patterns.add(pattern);

		List<String> expectedProposals = new ArrayList<String>();

		assertProposals(patterns, expectedProposals, Collections.<String> emptyList(), "");

		expectedProposals.add("ImplicitHello");
		assertProposals(patterns, expectedProposals, Collections.<String> emptyList(), "",
				new String[] { "TestString" });
	}

	@Test
	public void testArgumentReuseForPropsals() {

		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();

		patterns.add(createSayStringArgumentPattern());

		List<String> expectedProposals = new ArrayList<String>();
		expectedProposals.add("Say Hello");
		expectedProposals.add("Say something");

		List<String> sentences = new ArrayList<String>();
		sentences.add("Say Hello");

		assertProposals(patterns, expectedProposals, sentences, "");
	}

	@Test
	public void testCamelCaseCompletion() {

		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();

		patterns.add(createStaticWordPattern("Say", "Hello"));

		List<String> expectedProposals = new ArrayList<String>();
		expectedProposals.add("Say Hello");

		List<String> sentences = Collections.emptyList();

		assertProposals(patterns, expectedProposals, sentences, "");
		assertProposals(patterns, expectedProposals, sentences, "S");
		assertProposals(patterns, expectedProposals, sentences, "SH");
		assertProposals(patterns, expectedProposals, sentences, "SaH");
		assertProposals(patterns, expectedProposals, sentences, "SaHe");
		assertProposals(patterns, expectedProposals, sentences, "Say H");
		assertProposals(patterns, expectedProposals, sentences, "Say He");
		assertProposals(patterns, expectedProposals, sentences, "Say Hello");
	}

	private ISyntaxPattern<?> createStaticWordPattern(String... words) {
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		for (String word : words) {
			pattern.addPart(new StaticWord(word));
		}
		return pattern;
	}

	private ISyntaxPattern<?> createSayStringArgumentPattern() {
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Say"));
		pattern.addPart(new StringParameter("something"));
		return pattern;
	}

	private SyntaxPatternMock<Object> createUseObjectPattern() {
		SyntaxPatternMock<Object> useObjectPattern = new SyntaxPatternMock<Object>();
		useObjectPattern.addPart(new StaticWord("Use"));
		useObjectPattern.addPart(new ComplexParameter(qualifiedTypeName));
		return useObjectPattern;
	}

	private SyntaxPatternMock<Object> createCreateObjectPattern(final String objectName) {
		SyntaxPatternMock<Object> createObjectPattern = new SyntaxPatternMock<Object>() {

			@Override
			public Object createUserData(ISyntaxPatternMatch<Object> match) {
				IPatternMatchContext context = match.getContext();
				List<String> identifiers = Arrays.asList(new String[] { objectName });
				context.addObjectToContext(new ObjectCreation(context, new CommonType(qualifiedTypeName), identifiers));
				return super.createUserData(match);
			}
		};
		createObjectPattern.addPart(new StaticWord("Create"));
		createObjectPattern.addPart(new StaticWord(objectName));
		return createObjectPattern;
	}

	private void assertProposals(List<ISyntaxPattern<?>> patterns, List<String> expectedProposals) {
		assertProposals(patterns, expectedProposals, Collections.<String> emptyList(), "");
	}

	private void assertProposals(List<ISyntaxPattern<?>> patterns, List<String> expectedProposals,
			List<String> sentences, String lastSentence) {

		assertProposals(patterns, expectedProposals, sentences, lastSentence, null);
	}

	private void assertProposals(List<ISyntaxPattern<?>> patterns, List<String> expectedProposals,
			List<String> sentences, String lastSentence, Object[] contextObjects) {

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);

		if (contextObjects != null) {
			for (Object object : contextObjects) {
				context.addObjectToContext(new ObjectCreation(context, new CommonType(String.class.getName()),
						Arrays.asList(new String[] { object.toString() })));
			}
		}

		List<Sentence> sentenceList = new ArrayList<Sentence>();
		for (String sentenceText : sentences) {
			addSentence(sentenceList, sentenceText);
		}

		addSentence(sentenceList, lastSentence);

		CompletionHelper completionHelper = new CompletionHelper(useOptimizedMatcher, usePrioritizer);
		Set<String> proposals = completionHelper.computeCompletionProposals(patterns, sentenceList, context);
		assertNotNull(proposals);
		
		System.out.println("Proposals are: " + proposals);
		
		assertEquals("Wrong number of proposals.", expectedProposals.size(), proposals.size());
		for (String expected : expectedProposals) {
			assertTrue("Missing proposal '" + expected + "' from " + proposals, proposals.contains(expected));
		}
	}

	private void addSentence(List<Sentence> sentenceList, String text) {
		Sentence sentence = NatspecFactory.eINSTANCE.createSentence();
		sentence.setText(text);
		sentenceList.add(sentence);
	}
}
