package de.devboost.natspec.testscripting.ui.test;

import java.util.Collection;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.resource.natspec.ui.CustomTokenScanner;

public class FixedPatternTokenScanner extends CustomTokenScanner {
	
	private final Collection<? extends ISyntaxPattern<? extends Object>> patterns;

	public FixedPatternTokenScanner(boolean useOptimizedMatcher,
			boolean usePrioritizer,
			Collection<? extends ISyntaxPattern<? extends Object>> patterns) {
		
		super(null, null, null, useOptimizedMatcher, usePrioritizer);
		this.patterns = patterns;
	}

	@Override
	protected IPatternMatchContext createContext() {
		IPatternMatchContext context = super.createContext();
		context.getPatternsInContext().addAll(patterns);
		return context;
	}
}