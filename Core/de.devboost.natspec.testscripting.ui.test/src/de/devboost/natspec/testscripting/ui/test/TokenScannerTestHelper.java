package de.devboost.natspec.testscripting.ui.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.resource.natspec.ui.CustomTokenScanner;

public class TokenScannerTestHelper {

	private final CustomTokenScanner scanner;
	
	public TokenScannerTestHelper(
			URI uri,
			boolean useOptimizedMatcher,
			boolean usePrioritizer,
			final Collection<? extends ISyntaxPattern<? extends Object>> patterns) {

		super();
		this.scanner = new FixedPatternTokenScanner(useOptimizedMatcher,
				usePrioritizer, patterns);
	}

	public List<IToken> getTokens(String input) {
		
		DocumentMock document = new DocumentMock(input);

		scanner.setRange(document, 0, input.length());
		
		List<IToken> tokens = new ArrayList<IToken>();
		IToken nextToken;
		while ((nextToken = scanner.nextToken()) != Token.EOF) {
			tokens.add(nextToken);
		}
		return tokens;
	}
}
