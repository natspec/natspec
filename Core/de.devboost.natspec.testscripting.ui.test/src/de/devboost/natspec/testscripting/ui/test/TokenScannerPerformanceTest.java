package de.devboost.natspec.testscripting.ui.test;

import static org.junit.Assert.assertFalse;

import java.util.Collection;
import java.util.List;

import org.eclipse.jface.text.rules.IToken;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.testscripting.test.performance.AbstractPerformanceTestCase;

@RunWith(Parameterized.class)
public class TokenScannerPerformanceTest extends AbstractPerformanceTestCase {

	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;

	public TokenScannerPerformanceTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		return new BooleanCombinator().createBooleanCombinations();
	}

	@Test
	@Category(Runtime.class)
	public void testPerformance() {

		int patternCount = 10000;
		Collection<ISyntaxPattern<? extends Object>> patterns = createPatterns(patternCount);

		StringBuilder input = new StringBuilder();
		int sentences = 500;
		for (int i = 0; i < sentences; i++) {
			input.append("This is a rather long sentence with about 10 words\n");
		}
		String inputText = input.toString();

		TokenScannerTestHelper tokenScannerTestHelper = new TokenScannerTestHelper(
				null, useOptimizedMatcher, usePrioritizer, patterns);
		int scans = 10;
		for (int i = 0; i < scans; i++) {
			List<IToken> tokens = tokenScannerTestHelper.getTokens(inputText);
			assertFalse(tokens.isEmpty());
		}
	}
}
