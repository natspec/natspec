package de.devboost.natspec.testscripting.ui.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.shared.eclipse.jface.text.RegionOverlapHelper;

// TODO Move this test
@RunWith(Parameterized.class)
public class RegionOverlapTest {
	
	private int s1;
	private int e1;
	private int s2;
	private int e2;
	private boolean expectOverlap;
	
	public RegionOverlapTest(int s1, int e1, int s2, int e2,
			boolean expectOverlap) {
		super();
		this.s1 = s1;
		this.e1 = e1;
		this.s2 = s2;
		this.e2 = e2;
		this.expectOverlap = expectOverlap;
	}
	
	@Parameters
	public static Collection<Object[]> getData() {
		Collection<Object[]> data = new ArrayList<Object[]>();
		data.add(new Object[] {10, 12, 11, 13, true});
		data.add(new Object[] {10, 12, 13, 14, false});
		data.add(new Object[] {13, 14, 10, 12, false});
		data.add(new Object[] {9, 14, 10, 12, true});
		data.add(new Object[] {10, 13, 9, 14, true});
		return data;
	}

	@Test
	public void testOverlap() {
		assertOverlap(s1, e1, s2, e2, expectOverlap);
	}

	private void assertOverlap(int s1, int e1, int s2, int e2, boolean expectOverlap) {
		IRegion r1 = new Region(s1, e1 - s1);
		IRegion r2 = new Region(s2, e2 - s2);
		assertEquals(r1 + " and " + r2 + " must" + (expectOverlap ? "" : " not") + " overlap", expectOverlap, new RegionOverlapHelper().overlaps(r1, r2));
	}
}
