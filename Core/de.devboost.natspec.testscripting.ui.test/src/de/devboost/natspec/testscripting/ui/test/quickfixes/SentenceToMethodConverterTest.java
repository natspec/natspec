package de.devboost.natspec.testscripting.ui.test.quickfixes;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.parsing.ISentenceParser;
import de.devboost.natspec.parsing.SentenceParserFactory;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.test.ITestDataProvider2;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.java.quickfixes.IDerivedMethod;
import de.devboost.natspec.testscripting.java.quickfixes.SentenceToMethodConverter;

@RunWith(Parameterized.class)
public class SentenceToMethodConverterTest {

	private final String sentence;
	private final String[] objectIdentifiersInContext;
	private final String objectTypeInContext;
	private final String expectedAnnotation;
	private final String expectedMethodName;
	private final String expectedParameters;
	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;

	public SentenceToMethodConverterTest(String sentence,
			String[] objectIdentifiersInContext, String objectTypeInContext,
			String expectedAnnotation, String expectedMethodName,
			String expectedParameters, boolean useOptimizedMatcher,
			boolean usePrioritizer) {

		super();
		this.sentence = sentence;
		this.objectIdentifiersInContext = objectIdentifiersInContext;
		this.objectTypeInContext = objectTypeInContext;
		this.expectedAnnotation = expectedAnnotation;
		this.expectedMethodName = expectedMethodName;
		this.expectedParameters = expectedParameters;
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		ITestDataProvider2 provider = new ITestDataProvider2() {
			
			@Override
			public Collection<Object[]> getTestData(boolean useOptimizedMatcher, boolean usePrioritizer) {
				return SentenceToMethodConverterTest.getTestData(useOptimizedMatcher, usePrioritizer);
			}
		};
		return new BooleanCombinator().createBooleanCombinations(provider);
	}
	
	@Parameters
	public static Collection<Object[]> getTestData(boolean useOptimizedMatcher,
			boolean usePrioritizer) {

		Collection<Object[]> data = new ArrayList<Object[]>();

		// test detection of sentence without parameters
		data.add(new Object[] { "Some normal text", new String[0], null,
				"Some normal text", "someNormalText", "", useOptimizedMatcher,
				usePrioritizer });

		// test detection of int parameter
		data.add(new Object[] { "Some 2", new String[0], null, "Some #1",
				"some", "int p", useOptimizedMatcher, usePrioritizer });

		// test detection of float/double parameter
		data.add(new Object[] { "Some 2.0", new String[0], null, "Some #1",
				"some", "double p", useOptimizedMatcher, usePrioritizer });

		// test detection of String parameter (identified by underscore)
		data.add(new Object[] { "Some _text", new String[0], null, "Some #1",
				"some", "String _text", useOptimizedMatcher, usePrioritizer });

		// test detection of String parameter (identified by dot)
		data.add(new Object[] { "Some a.b", new String[0], null, "Some #1",
				"some", "String ab", useOptimizedMatcher, usePrioritizer });

		// test wrong detection of String parameter (dot at end of sentence)
		data.add(new Object[] { "Some text.", new String[0], null,
				"Some text.", "someText", "", useOptimizedMatcher,
				usePrioritizer });

		// test correct parameter name for String (identified by 1,1)
		data.add(new Object[] { "Some 1,1", new String[0], null, "Some #1",
				"some", "String _11", useOptimizedMatcher, usePrioritizer });

		// test correct method name for String (identified by 1,1)
		data.add(new Object[] { "1Some", new String[0], null, "#1",
				"_", "String _1Some", useOptimizedMatcher, usePrioritizer });

		// test detection of multiple String parameters with same name
		data.add(new Object[] { "Some _text _text", new String[0], null,
				"Some #1 #2", "some", "String _text, String _text2",
				useOptimizedMatcher, usePrioritizer });

		// test detection of String parameter (identified by camel case word)
		data.add(new Object[] { "Some camelCaseWord", new String[0], null,
				"Some #1", "some", "String camelCaseWord", useOptimizedMatcher,
				usePrioritizer });

		// test detection of String parameter (identified by number in word)
		data.add(new Object[] { "Some word1", new String[0], null, "Some #1",
				"some", "String word1", useOptimizedMatcher, usePrioritizer });

		// test detection of Object parameter (available in context)
		data.add(new Object[] { "Some defined object",
				new String[] { "defined", "object" }, Object.class.getName(),
				"Some #1", "some", "Object object", useOptimizedMatcher,
				usePrioritizer });

		// test detection of Exception parameter (available in context)
		data.add(new Object[] { "Some defined object",
				new String[] { "defined", "object" },
				Exception.class.getName(), "Some #1", "some",
				"Exception exception", useOptimizedMatcher, usePrioritizer });

		// test detection of custom type parameter (available in context)
		data.add(new Object[] { "Some defined object",
				new String[] { "defined", "object" }, "com.example.MyType",
				"Some #1", "some", "com.example.MyType myType",
				useOptimizedMatcher, usePrioritizer });

		// test detection of generic custom type parameter (available in
		// context)
		data.add(new Object[] { "Some defined object",
				new String[] { "defined", "object" },
				"com.example.MyGenericType<String>", "Some #1", "some",
				"com.example.MyGenericType<String> myGenericType",
				useOptimizedMatcher, usePrioritizer });

		data.add(new Object[] { "Assert stock CSV with 2 lines on FTP server",
				new String[0], null,
				"Assert stock #1 with #2 lines on #3 server",
				"assertStockWithLinesOnServer",
				"String csv, int p, String ftp", useOptimizedMatcher,
				usePrioritizer });

		// regression for IndexOutOfBoundsException
		data.add(new Object[] {
				"Assert 1 logged LOGIN event for user admin",
				new String[] { "admin", "test", "pick", "Pick", "Terminal", "1" },
				"AuthenticationResult",
				"Assert #1 logged #2 event for user admin",
				"assertLoggedEventForUserAdmin", "int p, String login",
				useOptimizedMatcher, usePrioritizer });

		// test detection of all upper-case words as string parameters
		data.add(new Object[] { "Assert UPPERCASE string", new String[0], null,
				"Assert #1 string", "assertString", "String uppercase",
				useOptimizedMatcher, usePrioritizer });
		
		// test detection of date parameter without time
		data.add(new Object[] { "Assert date is 01.01.2014", new String[0], null,
				"Assert date is #1", "assertDateIs", "java.util.Date date",
				useOptimizedMatcher, usePrioritizer });

		// test detection of date parameter with time
		data.add(new Object[] { "Assert date is 01.01.2014 12:00", new String[0], null,
				"Assert date is #1", "assertDateIs", "java.util.Date date",
				useOptimizedMatcher, usePrioritizer });

		// test dash at start of sentence
		data.add(new Object[] { "- some text", new String[0], null,
				"- some text", "someText", "",
				useOptimizedMatcher, usePrioritizer });

		// test sentence without valid Java identifier characters
		data.add(new Object[] { "-", new String[0], null,
				"-", "_", "",
				useOptimizedMatcher, usePrioritizer });

		// test sentence that contains java keyword
		data.add(new Object[] { "- test: false", new String[0], null,
				"- #1 #2", "_", "String test, boolean p",
				useOptimizedMatcher, usePrioritizer });

		// another test sentence that contains a java keyword
		data.add(new Object[] { "static", new String[0], null,
				"static", "_static", "",
				useOptimizedMatcher, usePrioritizer });

		// NATSPEC-328 test that parameters are never capitalized
		data.add(new Object[] {
				"Assert warehouse with identifier Warehouse1 and name Warehouse1 is marked as inactive",
				new String[0],
				null,
				"Assert warehouse with identifier #1 and name #2 is marked as inactive",
				"assertWarehouseWithIdentifierAndNameIsMarkedAsInactive",
				"String warehouse1, String warehouse12", useOptimizedMatcher,
				usePrioritizer });

		return data;
	}

	@Test
	public void testConversion() {
		SentenceToMethodConverter converter = new SentenceToMethodConverter(useOptimizedMatcher, usePrioritizer);
		ISentenceParser sentenceParser = new SentenceParserFactory().createSentenceParser();
		Sentence sentenceObject = NatspecFactory.eINSTANCE.createSentence();
		sentenceObject.setText(sentence);
		List<Word> words = sentenceParser.split(sentenceObject);
		List<String> wordsAsStrings = WordUtil.INSTANCE.toStringList(words);
		final List<Object> objectsInContext = new ArrayList<Object>();
		PatternMatchContextFactory contextFactory = PatternMatchContextFactory.INSTANCE;
		IPatternMatchContext context = contextFactory.createPatternMatchContext(null);
		if (objectTypeInContext != null) {
			String qualifiedTypeName = objectTypeInContext;
			List<String> identifiers = Arrays
					.asList(objectIdentifiersInContext);
			objectsInContext.add(new ObjectCreation(context, new JavaType(
					qualifiedTypeName), identifiers));
		}

		IPatternMatchContext patternMatchContext = new IPatternMatchContext() {

			@Override
			public List<Object> getObjectsInContext() {
				return objectsInContext;
			}

			@Override
			public ISynonymProvider getLocalSynonymProvider() {
				return null;
			}

			@Override
			public URI getContextURI() {
				return null;
			}

			@Override
			public void addObjectToContext(Object object) {
				throw new UnsupportedOperationException();
			}

			@Override
			public void addObjectsToContext(List<Object> objects) {
				throw new UnsupportedOperationException();
			}
			
			@Override
			public Collection<ISyntaxPattern<? extends Object>> getPatternsInContext() {
				throw new UnsupportedOperationException();
			}

			@Override
			public Set<String> getUsedVariableNames() {
				throw new UnsupportedOperationException();
			}

			@Override
			public Object getLastObjectByType(String qualifiedTypesString) {
				throw new UnsupportedOperationException();
			}

			@Override
			public String getTemplateFieldByType(String qualifiedTypeName) {
				throw new UnsupportedOperationException();
			}

			@Override
			public void setTemplateFieldNameToTypeMap(
					Map<String, String> templateFieldMap) {
				throw new UnsupportedOperationException();
			}

			@Override
			public boolean isLast(Object object) {
				throw new UnsupportedOperationException();
			}

			@Override
			public Map<String, String> getTemplateFieldTypeToNameMap() {
				throw new UnsupportedOperationException();
			}

			@Override
			public boolean matchFuzzy() {
				return false;
			}
		};
		IDerivedMethod derivedMethod = converter.deriveMethod(wordsAsStrings, patternMatchContext);

		String code = derivedMethod.getCode();
		System.out.println("Actual code   >>>" + code + "<<<");

		String expectedCode = "@TextSyntax(\""
				+ expectedAnnotation
				+ "\")\npublic void "
				+ expectedMethodName
				+ "("
				+ expectedParameters
				+ ") {\nthrow new UnsupportedOperationException(\"Not implemented yet.\");\n}";
		System.out.println("Expected code >>>" + expectedCode + "<<<");

		assertEquals("Unexpected code", expectedCode, code);
	}
}
