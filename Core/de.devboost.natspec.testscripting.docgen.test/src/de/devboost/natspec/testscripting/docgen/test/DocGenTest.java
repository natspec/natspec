package de.devboost.natspec.testscripting.docgen.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;

public class DocGenTest {

	@Test
	public void testUntypedPattern() {
		SyntaxPatternMock<?> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("DoIt"));
		String expected = "begin untyped begin [StaticWord [DoIt]] end untyped";

		assertDocumentation(pattern, expected);
	}

	@Test
	public void testTypedPattern() {
		SyntaxPatternMock<?> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("DoIt"));
		pattern.addPart(new ComplexParameter("Type"));
		String expected = "begin Type begin [StaticWord [DoIt], ComplexParameter [Type]] end Type";

		assertDocumentation(pattern, expected);
	}

	private void assertDocumentation(SyntaxPatternMock<?> pattern,
			String expected) {
		List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();
		patterns.add(pattern);

		TestDocGenerator generator = new TestDocGenerator();
		generator.generateDocumentation(patterns);
		String result = generator.getResult();
		assertEquals(expected, result);
	}
}
