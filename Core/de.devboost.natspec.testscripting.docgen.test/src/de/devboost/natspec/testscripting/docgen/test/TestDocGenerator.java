package de.devboost.natspec.testscripting.docgen.test;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.docgen.AbstractDocumentationGenerator;

public class TestDocGenerator extends AbstractDocumentationGenerator {
	
	private StringBuilder result = new StringBuilder();

	@Override
	protected void beginType(String type) {
		result.append("begin " + getName(type));
	}

	@Override
	protected void endType(String type) {
		result.append("end " + getName(type));
	}

	private String getName(String type) {
		if (type == null) {
			type = "untyped";
		}
		return type + " ";
	}

	@Override
	protected void beginPattern(ISyntaxPattern<?> pattern) {
		result.append("begin " + pattern + " ");
	}

	public String getResult() {
		return result.toString().trim();
	}
}
