package de.devboost.natspec.testscripting.python.builder;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import com.google.common.base.CaseFormat;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.resource.natspec.INatspecBuilder;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.resource.natspec.util.NatspecStreamUtil;
import de.devboost.natspec.testscripting.IProperties;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.builder.CodeGenerationContext;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.python.PythonNatSpecConstants;
import de.devboost.natspec.testscripting.python.patterns.PythonMultipleMatchCodeGenerator;

public class PythonTestScriptBuilder implements INatspecBuilder {

	private static final String ORG_PYTHON_PYDEV_PYTHON_NATURE = "org.python.pydev.pythonNature";

	private static final String ERROR_MESSAGE = "ERROR: No test case template found. Please provide \n"
			+ PythonNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME + "\n" + " in same folder or one of its parents.\n";

	public static final String GENERATION_SOURCE_STRING = "\"\"\"\n The code in this method is generated from: ";

	@Override
	public IStatus build(NatspecResource resource, IProgressMonitor monitor) {
		// register resource to listen for changes
		TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
		if (plugin == null) {
			return Status.OK_STATUS;
		}
		plugin.addTestScript(resource);

		if (NatSpecPlugin.hasValidLicense()) {
			return generateCodeForScenario(resource);
		} else {
			// add warning to error log
			TestConnectorPlugin.logWarning("Can't generate NatSpec code without a valid license key.", null);
			return Status.OK_STATUS;
		}
	}

	private IStatus generateCodeForScenario(NatspecResource resource) {
		MatchService matchService = new MatchService(NatSpecPlugin.USE_TREE_BASED_MATCHER,
				NatSpecPlugin.USE_PRIORITIZER);
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(resource.getURI());
		ICodeGenerationContext codeGenContext = new CodeGenerationContext();

		// Normally the validation makes sure that multiple matches to the same
		// sentence cause an error. During code generation we would therefore
		// not expect multiple matches. If there are multiple matches
		// nonetheless, we add code for all matches.
		codeGenContext.addCode(GENERATION_SOURCE_STRING + resource.getURI().toPlatformString(false) + "\n");
		codeGenContext.addCode(
				" Never change this method or any contents of this file, all local changes will we overwritten.\n\"\"\"\n");
		// TODO: ibauer refactor this
		// iterate over the whole resource to find sentences
		Iterator<EObject> allContents = resource.getAllContents();
		while (allContents.hasNext()) {
			EObject eObject = (EObject) allContents.next();
			if (eObject instanceof Sentence) {
				Sentence sentence = (Sentence) eObject;
				// TODO This code can also be found in the NatspecValidator
				// where in addition to skipping comments, empty lines are
				// ignore. We must merge this code to avoid duplication.
				SentenceUtil sentenceUtil = SentenceUtil.INSTANCE;
				if (sentenceUtil.isComment(sentence)) {
					continue;
				}
				// ignore empty sentences
				if (sentenceUtil.isEmpty(sentence)) {
					continue;
				}

				List<ISyntaxPatternMatch<?>> matches = matchService.match(sentence, context);
				if (containsCompleteMatch(matches)) {
					List<ISyntaxPatternMatch<? extends Object>> completeMatches = matchService
							.filterCompleteMatches(matches);

					if (completeMatches.size() > 1) {
						new PythonMultipleMatchCodeGenerator().generateSourceCode(completeMatches, codeGenContext);
					} else {
						Object userData = completeMatches.get(0).getUserData();
						if (userData instanceof ICodeFragment) {
							ICodeFragment fragment = (ICodeFragment) userData;
							fragment.generateSourceCode(codeGenContext);
						} else {
							codeGenContext.addCode("# sentence not matched " + "\n");
						}
					}

				}
			}
		}

		return writeGeneratedCodeToFile(resource, codeGenContext);
	}

	private boolean containsCompleteMatch(List<ISyntaxPatternMatch<?>> matches) {
		for (ISyntaxPatternMatch<?> match : matches) {
			if (match.isComplete()) {
				return true;
			}
		}
		return false;
	}

	private IStatus writeGeneratedCodeToFile(NatspecResource resource, ICodeGenerationContext codeGenContext) {
		// Write code to a file
		IFile file = new NatspecEclipseProxy().getFileForResource(resource);

		// in python files are lower snake case
		String className = resource.getURI().trimFileExtension().lastSegment();
		className = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, className);
		className = StringUtils.INSTANCE.toFirstUpper(className);

		IProject project = file.getProject();
		IPath outputFolderPath = file.getParent().getProjectRelativePath();
		try {
			String outputPath = project.getPersistentProperty(IProperties.QUALIFIED_PATH_PROPERTY);
			if (outputPath != null) {
				if (outputPath.startsWith("/")) {
					outputFolderPath = project.getFile(new Path(outputPath)).getProjectRelativePath();
				} else {
					outputFolderPath = outputFolderPath.append(outputPath);
				}
			}
		} catch (CoreException e) {
			// ignore. output path is null, which is default.
		}

		String fileName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, className);
		IFile outputFile = project
				.getFile(outputFolderPath.append(fileName).addFileExtension(PythonNatSpecConstants.PYTHON_EXTENSION));

		String classCode = getTestCaseCode(resource, null, className, codeGenContext.getImports(),
				codeGenContext.getCode());
		String charset = null;
		try {
			charset = file.getCharset();
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while determining encoding of test script.", e);
		}
		saveSafe(classCode, outputFile, fileName, charset);
		touchModifiedFile(outputFile, fileName);

		try {
			outputFile.refreshLocal(1, new NullProgressMonitor());
			outputFile.getParent().refreshLocal(2, new NullProgressMonitor());
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while refreshing generated test case.", e);
		}

		return Status.OK_STATUS;
	}

	private String getTestCaseCode(Resource resource, String packageName, String className, Set<String> imports,
			String generatedStatements) {

		IFile file = new NatspecEclipseProxy().getFileForResource(resource);
		IFile templateFile = new PythonTemplateFileFinder().findTemplateFile(file);
		if (templateFile == null) {
			return ERROR_MESSAGE;
		}

		StringBuilder importStatements = new StringBuilder();
		for (String importStatement : imports) {
			importStatements.append(importStatement + "\n");
		}

		InputStream templateStream = null;
		try {
			templateStream = templateFile.getContents();
			String templateContent = NatspecStreamUtil.getContent(templateStream);

			PythonTemplateInstantiator templateInstantiator = new PythonTemplateInstantiator(
					new PythonTemplateFileFinder());

			String generatedMethods = "";
			return templateInstantiator.instantiateTemplate(templateContent, packageName, importStatements.toString(),
					className, generatedStatements, "", generatedMethods);

		} catch (IOException e) {
			TestConnectorPlugin.logError("Exception while reading test case template.", e);
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while reading test case template.", e);
		} finally {
			try {
				if (templateStream != null) {
					templateStream.close();
				}
			} catch (IOException e) {
				// Ignore
			}
		}
		return ERROR_MESSAGE;
	}

	private void saveSafe(String code, IFile file, String className, String encoding) {
		try {
			save(file, code, className, encoding);
		} catch (IOException e) {
			TestConnectorPlugin.logError("Exception while saving test class.", e);
		}
	}

	private void touchModifiedFile(IFile file, String className) {
		try {
			IContainer parent = file.getParent();
			if (parent instanceof IFolder) {
				IFolder folder = (IFolder) parent;
				IFile pythonFile = folder.getFile(className + "." + PythonNatSpecConstants.PYTHON_EXTENSION);
				if (pythonFile != null && pythonFile.exists()) {
					pythonFile.touch(new NullProgressMonitor());
				}
			}
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while touching generated test class.", e);
		}
	}

	private void save(IFile file, String testCode, String className, String encoding)
			throws FileNotFoundException, IOException {
		byte[] bytes;
		if (encoding == null) {
			bytes = testCode.getBytes();
		} else {
			bytes = testCode.getBytes(encoding);
		}

		InputStream in = new ByteArrayInputStream(bytes);
		File parentFile = file.getRawLocation().toFile().getParentFile();
		parentFile.mkdirs();
		String parentPath = parentFile.toString();
		File targetFile = new File(parentPath, className + ".py");
		OutputStream out = new FileOutputStream(targetFile);
		NatspecStreamUtil.copy(in, out);
		out.close();
	}

	@Override
	public boolean isBuildingNeeded(URI uri) {
		// Only return true if project has Python nature
		IFile file = new NatspecEclipseProxy().getFileForURI(uri);
		if (file == null) {
			return false;
		}

		IProject project = file.getProject();
		try {
			IProjectNature nature = project.getNature(ORG_PYTHON_PYDEV_PYTHON_NATURE);
			if (nature != null) {
				return true;
			}
		} catch (CoreException e) {
			// Ignore this
		}
		return false;
	}

	@Override
	public IStatus handleDeletion(URI uri, IProgressMonitor monitor) {
		return Status.OK_STATUS;
	}
}
