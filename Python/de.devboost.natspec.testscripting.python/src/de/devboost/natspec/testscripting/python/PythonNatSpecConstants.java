package de.devboost.natspec.testscripting.python;

public interface PythonNatSpecConstants {

	public static final String PYTHON_EXTENSION = "py";
	
	// FIXME Use lower case letter
	public static final String TEST_CASE_TEMPLATE_CLASS_NAME = "_NatSpecTemplate";
	
	public static final String TEST_CASE_TEMPLATE_FILE_NAME = "_NatSpecTemplate." + PYTHON_EXTENSION;
	
	public static final String TEXT_SYNTAX_NAME = "TextSyntax";

}
