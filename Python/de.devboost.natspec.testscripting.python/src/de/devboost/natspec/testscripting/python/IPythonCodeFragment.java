package de.devboost.natspec.testscripting.python;

import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;

public interface IPythonCodeFragment extends ICodeFragment {

	public void generateSourceCode(ICodeGenerationContext context,
			boolean isMultipleMatch, int position);
}
