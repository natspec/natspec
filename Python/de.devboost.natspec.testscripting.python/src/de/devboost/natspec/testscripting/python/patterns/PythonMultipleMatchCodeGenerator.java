package de.devboost.natspec.testscripting.python.patterns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.testscripting.builder.CodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.python.IPythonCodeFragment;

public class PythonMultipleMatchCodeGenerator {

	// TODO documentate this

	public void generateSourceCode(List<ISyntaxPatternMatch<? extends Object>> matches,
			ICodeGenerationContext context) {

		Map<String, List<ISyntaxPatternMatch<? extends Object>>> groupedMatches = new HashMap<String, List<ISyntaxPatternMatch<? extends Object>>>();

		// Use fresh context instead of the global one, because we do not want
		// to generate code here.
		groupedMatches = groupMatchesByArguments(matches, new CodeGenerationContext());

		for (List<ISyntaxPatternMatch<? extends Object>> matchList : groupedMatches.values()) {
			Iterator<ISyntaxPatternMatch<? extends Object>> matchIterator = matchList.iterator();
			int pos = 0;
			while (matchIterator.hasNext()) {
				ISyntaxPatternMatch<? extends Object> match = matchIterator.next();
				if (!matchIterator.hasNext())
					pos = -1;

				Object userData = match.getUserData();
				if (userData instanceof IPythonCodeFragment) {
					IPythonCodeFragment fragment = (IPythonCodeFragment) userData;
					fragment.generateSourceCode(context, matchList.size() > 1, pos);
				} else {
					context.addCode("# sentence not matched " + "\n");
				}

				pos++;
			}
		}
	}

	private Map<String, List<ISyntaxPatternMatch<? extends Object>>> groupMatchesByArguments(
			List<ISyntaxPatternMatch<? extends Object>> matches, ICodeGenerationContext context) {

		Map<String, List<ISyntaxPatternMatch<? extends Object>>> groupedMatches = new LinkedHashMap<String, List<ISyntaxPatternMatch<? extends Object>>>();
		for (ISyntaxPatternMatch<? extends Object> match : matches) {
			Object userData = match.getUserData();
			if (userData instanceof PythonDynamicSyntaxCodeGenerator) {
				PythonDynamicSyntaxCodeGenerator fragment = (PythonDynamicSyntaxCodeGenerator) userData;
				List<String> argumentList = new ArrayList<String>();
				fragment.generateArguments(argumentList, context);

				String arguments = StringUtils.INSTANCE.explode(argumentList, ", ");

				if (!groupedMatches.containsKey(arguments)) {
					groupedMatches.put(arguments, new ArrayList<ISyntaxPatternMatch<? extends Object>>());
				}
				groupedMatches.get(arguments).add(match);
			}
		}
		return groupedMatches;
	}
}
