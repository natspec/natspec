package de.devboost.natspec.testscripting.python.visitors;

/*
 * Exception for the ReturnVisitor to signalize that the return is found.
 */
public class ReturnFound extends Exception {
	private static final long serialVersionUID = 2511405878233081629L;

	public ReturnFound() {
		super();
	}
}
