package de.devboost.natspec.testscripting.python.builder;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;

import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.builder.AbstractResourceChangeListener;
import de.devboost.natspec.testscripting.python.PythonNatSpecConstants;

/**
 * The {@link PythonTestTemplateChangeListener} listens to changes that are
 * applied to test case templates. If a change is applied, all scripts are
 * revalidated and their code is regenerated.
 */
public class PythonTestTemplateChangeListener extends
		AbstractResourceChangeListener {

	@Override
	public void handleResourceChange(IResourceDelta delta, int eventType) {
		IResource resource = delta.getResource();
		if (resource == null) {
			return;
		}
		String name = resource.getName();

		if (PythonNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME
				.equals(name)) {
			TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
			if (plugin == null) {
				return;
			}

			// Validate all scripts that are located in the same directory or
			// in sub folders
			IContainer parent = resource.getParent();
			plugin.triggerScriptValidation(parent);
		}
	}

	@Override
	protected void handleMarkerDeltas(IMarkerDelta[] markerDeltas) {
		// Do nothing.
	}
}
