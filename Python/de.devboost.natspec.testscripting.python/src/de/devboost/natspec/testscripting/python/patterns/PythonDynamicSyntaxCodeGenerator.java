package de.devboost.natspec.testscripting.python.patterns;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.CaseFormat;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;
import de.devboost.natspec.testscripting.python.IPythonCodeFragment;
import de.devboost.natspec.testscripting.python.PythonCodeGeneratorFactory;

public class PythonDynamicSyntaxCodeGenerator extends AbstractCodeGenerator
		implements IPythonCodeFragment {

	public static final PrefixBasedCommentGenerator COMMENT_GENERATOR = new PrefixBasedCommentGenerator("#");
	
	private final String qualifiedTypeName;
	private final String methodName;
	private final Map<Integer, ISyntaxPatternPartMatch> argumentIndexToArgumentMatchMap;
	private final String returnType;
	private final String returnVariableName;

	public PythonDynamicSyntaxCodeGenerator(
			ISyntaxPatternMatch<?> match,
			String typeName,
			String methodName,
			String returnType,
			Map<Integer, ISyntaxPatternPartMatch> argumentIndexToArgumentMatchMap,
			String returnVariableName) {

		super(match, new PythonCodeGeneratorFactory(), COMMENT_GENERATOR);
		this.qualifiedTypeName = typeName;
		this.methodName = methodName;
		this.returnType = returnType;
		this.argumentIndexToArgumentMatchMap = argumentIndexToArgumentMatchMap;
		this.returnVariableName = returnVariableName;
	}
	
	@Override
	public void generateSourceCode(ICodeGenerationContext context) {
		this.generateSourceCode(context, false, 0);
	}
	
	@Override
	public void generateSourceCode(ICodeGenerationContext context,
			boolean isMultipleMatch, int position) {

		List<String> argumentList = new ArrayList<String>();
		generateArguments(argumentList, context);
		
		String arguments = StringUtils.INSTANCE.explode(argumentList, ", ");
		
		String returnAssignment = "";
		String returnVariable = " returnValue" + context.getNextCounter();
		if (returnVariableName != null) {
			returnVariable = returnVariableName;
		}
		if (returnType != null) {
			returnAssignment = returnVariable + " = ";
		}

		String supportClassName = NameHelper.INSTANCE
				.getSimpleName(qualifiedTypeName);
		// In python we don't use camelcase (PEP8 standard)
		// FIXME ibauer Use real field name instead
		String snakeCaseName = CaseFormat.UPPER_CAMEL.to(
				CaseFormat.LOWER_UNDERSCORE, supportClassName);
		String instanceName = "self." + snakeCaseName;
		
		if (isMultipleMatch) {
			context.addImport("from natspec_utils.match import multipleMatch as _");
			if (position == 0) {
				context.addCode(getComment());
				context.addCode(returnAssignment + "_(\n");
			}
			context.addCode("\t" + instanceName + "." + methodName + ",\n"); //TODO: don't use tab here, it will brake your python code
			if (position == -1) {
				context.addCode("\t" + arguments);//TODO: don't use tab here, it will brake your python code
				context.addCode("\n)\n\n");//TODO: don't use tab here, it will brake your python code
			}
		} else {
			context.addCode(getComment());
			context.addCode(returnAssignment + instanceName + "." + methodName
					+ "(");
			context.addCode(arguments);
			context.addCode(")\n\n");
		}
	}

	public void generateArguments(List<String> arguments,
			ICodeGenerationContext context) {

		Set<Integer> indices = argumentIndexToArgumentMatchMap.keySet();
		List<Integer> list = new ArrayList<Integer>(indices);

		Collections.sort(list);
		for (int argumentIndex : list) {
			ISyntaxPatternPartMatch argumentMatch = argumentIndexToArgumentMatchMap
					.get(argumentIndex);

			String argumentCode = getCode(argumentMatch, context);
			arguments.add(argumentCode);
		}
	}
}
