package de.devboost.natspec.testscripting.python;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.text.IDocument;
import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.shared_core.model.ISimpleNode;
import org.python.pydev.shared_core.parsing.IParserObserver;
import org.python.pydev.shared_core.string.StringUtils;

import de.devboost.natspec.testscripting.python.visitors.ClassDefVisitor;
import de.devboost.natspec.testscripting.python.visitors.TemplateImportsVisitor;
import de.devboost.natspec.testscripting.python.visitors.TemplateVisitor;

public class PyDevObserver implements IParserObserver {

	@Override
	public void parserChanged(ISimpleNode root, IAdaptable adaptable,
			IDocument document, long generatedOnStamp) {

		SimpleNode ast = (SimpleNode) root;

		if (!(adaptable instanceof IFile)) {
			return;
		}
		IFile file = (IFile) adaptable;

		String[] path = file.getFullPath().segments();
		String fileName = path[path.length-1];
		fileName = fileName.replace("."+PythonNatSpecConstants.PYTHON_EXTENSION, "");
		path[path.length-1] = fileName;
		
		final String moduleName = StringUtils.join(".", path);

		final String projectName = getProjectName(adaptable);
		if (projectName == null) {
			return;
		}

		PythonConnectorPlugin plugin = PythonConnectorPlugin.getInstance();
		if (plugin == null) {
			// this can happen during Eclipse shutdown
			return;
		}

		final PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider = plugin
				.getDynamicSyntaxPatternProvider();
		if (dynamicSyntaxPatternProvider == null) {
			return;
		}

		searchSyntaxPatterns(adaptable, ast, moduleName, projectName,
				dynamicSyntaxPatternProvider);
		
		searchTemplateFields(ast, projectName, dynamicSyntaxPatternProvider,
				file);

		
	}

	private void searchTemplateFields(SimpleNode ast, String projectName,
			PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider,
			IFile file) {

		String fileName = file.getName();
		if (!fileName
				.equals(PythonNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME)) {
			return;
		}

		try {
			
			Map<String, String> imports = new LinkedHashMap<String, String>();

			TemplateImportsVisitor importsVisitor = new TemplateImportsVisitor(
					projectName, imports);

			TemplateVisitor templateVisitor = new TemplateVisitor(
					dynamicSyntaxPatternProvider, file, imports);
			
			ast.accept(importsVisitor);
			ast.accept(templateVisitor);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void searchSyntaxPatterns(IAdaptable file, SimpleNode ast,
			String moduleName, String projectName,
			PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider) {

		try {
			ClassDefVisitor classDefVisitor = new ClassDefVisitor(
					dynamicSyntaxPatternProvider, file, projectName, moduleName);
			ast.accept(classDefVisitor);
		} catch (Exception e) {
			// FIXME (ilja) Handle this
		}
	}

	private String getProjectName(IAdaptable adaptable) {
		String projectName = null;
		if (adaptable instanceof IFile) {
			IFile file = (IFile) adaptable;
			IProject project = file.getProject();
			projectName = project.getName();
		}
		return projectName;
	}

	@Override
	public void parserError(Throwable arg0, IAdaptable arg1, IDocument arg2) {
	}
}
