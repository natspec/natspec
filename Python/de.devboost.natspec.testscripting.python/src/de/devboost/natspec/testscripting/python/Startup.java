package de.devboost.natspec.testscripting.python;

import org.eclipse.ui.IStartup;

import de.devboost.natspec.resource.natspec.NatspecBuilderRegistry;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.python.builder.PythonTestScriptBuilder;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		TestConnectorPlugin.getInstance();

		// register test class builder
		NatspecBuilderRegistry.REGISTRY
				.registerBuilder(new PythonTestScriptBuilder());
	}

}
