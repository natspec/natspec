package de.devboost.natspec.testscripting.python.visitors;

import java.util.List;
import java.util.Map;

import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.parser.jython.ast.FunctionDef;
import org.python.pydev.parser.jython.ast.VisitorBase;

import de.devboost.natspec.testscripting.Field;

public class TemplateMethodVisitor extends VisitorBase {

	private final Map<String, String> imports;
	private final List<Field> fields;

	public TemplateMethodVisitor(Map<String, String> imports, List<Field> fields) {
		super();
		this.imports = imports;
		this.fields = fields;
	}

	@Override
	public Object visitFunctionDef(FunctionDef node) throws Exception {
		node.accept(new TemplateAssignVisitor(imports, fields));
		return null;
	}

	@Override
	protected Object unhandled_node(SimpleNode node) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void traverse(SimpleNode node) throws Exception {
		node.traverse(this);
	}
}
