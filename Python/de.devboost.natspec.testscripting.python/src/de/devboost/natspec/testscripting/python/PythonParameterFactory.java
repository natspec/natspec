package de.devboost.natspec.testscripting.python;

import java.util.List;

import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DoubleArgument;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.IParameter;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.DateParameter;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;
import de.devboost.natspec.testscripting.patterns.parts.ListParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;
import de.devboost.natspec.testscripting.python.patterns.PythonType;
import de.devboost.natspec.testscripting.python.patterns.parts.DynamicTypeParameter;

public class PythonParameterFactory implements IParameterFactory {
	
	@Override
	public ISyntaxPatternPart createParameter(IParameter parameter)
			throws UnsupportedTypeException {
		return createParameter(parameter.getType());
	}
	
	@Override
	public ISyntaxPatternPart createParameter(IClass type)
			throws UnsupportedTypeException {
		
		String qualifiedName = type.getQualifiedName();
		List<? extends IClass> typeArguments = type.getTypeArguments();
		return createParameter(qualifiedName, typeArguments);
	}

	private ISyntaxPatternPart createParameter(String qualifiedTypeName, 
			List<? extends IClass> typeArguments) throws UnsupportedTypeException {
		
		String realName = NameHelper.INSTANCE.getNameWithoutTypeArguments(qualifiedTypeName);
		
		if (isDynamicType(realName)) {
			return new DynamicTypeParameter();
		} else if (isInteger(realName)) {
			return new IntegerArgument();
		} else if (isFloat(realName)) {
			return new DoubleArgument();
		} else if (isString(realName)) {
			return new StringParameter();
		} else if (isList(realName)) {
			if (typeArguments.size() == 0) {
				IClass listElementType = new PythonType("*");
				return new ListParameter(createParameter(listElementType), listElementType);
			} else if (typeArguments.size() == 1) {
				IClass listElementType = typeArguments.get(0);
				return new ListParameter(createParameter(listElementType), listElementType);
			} else {
				throw new UnsupportedTypeException("Unsupported list type: "+ realName + " [type arguments=" + typeArguments + "]");
			}
		} else if (isObject(realName)) {
			// TODO Other parameters can also be converted to 'Object'. how
			// can we decide which one to use?
			// TODO Why do we create StringParameters for Object parameters?
			return new StringParameter();
		} else if (isDate(realName)) {
			return new DateParameter();
		} else {
			// we have a type that is not built in
			return new ComplexParameter(realName);
		}
	}

	@Override
	public boolean isDefaultSyntaxPattern(String[] parts,
			List<? extends IParameter> parameters) {
		
		// The default syntax pattern must have one part
		if (parts.length != 1) {
			return false;
		}
		
		// The default syntax pattern must equal '#1'
		if (!"#1".equals(parts[0].trim())) {
			return false;
		}
		
		// The first must be a list...
		IClass parameterType = parameters.get(0).getType();
		if (!isList(parameterType.getQualifiedName())) {
			return false;
		}

		// ...of type String
		List<? extends IClass> typeArguments = parameterType.getTypeArguments();
		if (typeArguments.size() != 1) {
			return false;
		}
		
		IClass typeArgument = typeArguments.get(0);
		if (!isString(typeArgument.getQualifiedName())) {
			return false;
		}
		return true;
		
	}
	
	private boolean isDate(String typeName) {
		//FIXME this is not correct, we have to differ betwen datetime and date
		return "datetime.date".equals(typeName);
	}

	private boolean isObject(String typeName) {
		return Object.class.getName().equals(typeName);
	}

	private boolean isList(String typeName) {
		return "list".equals(typeName);
	}

	private boolean isString(String typeName) {
		return "types.StringTypes".equals(typeName)
				|| "StringTypes".equals(typeName)
				|| "str".equalsIgnoreCase(typeName);
	}

	private boolean isFloat(String typeName) {
		return "types.FloatType".equals(typeName)
				|| "FloatType".equals(typeName) || "float".equals(typeName);
	}

	private boolean isInteger(String typeName) {
		return "int".equals(typeName) || "IntType".equals(typeName)
				|| "types.IntType".equals(typeName);
	}

	private boolean isDynamicType(String typeName) {
		return "*".equals(typeName);
	}

	@Override
	public ISyntaxPatternPart createParameter(IParameter parameter,
			boolean implicit) throws UnsupportedTypeException {

		String qualifiedType = parameter.getType().getQualifiedName();
		return new ImplicitParameter(qualifiedType);
	}

	@Override
	public ISyntaxPatternPart createParameter(IClass type, boolean implicit)
			throws UnsupportedTypeException {

		String qualifiedType = type.getQualifiedName();
		return new ImplicitParameter(qualifiedType);
	}

	@Override
	public boolean isDefaultSyntaxPatternWithMany(List<? extends IParameter> parameters) {
		// FIXME ibauer
		return false;
	}
}
