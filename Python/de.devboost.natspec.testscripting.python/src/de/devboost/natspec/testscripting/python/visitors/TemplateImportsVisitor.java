package de.devboost.natspec.testscripting.python.visitors;

import java.util.Map;

import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.parser.jython.ast.Import;
import org.python.pydev.parser.jython.ast.ImportFrom;
import org.python.pydev.parser.jython.ast.NameTokType;
import org.python.pydev.parser.jython.ast.VisitorBase;
import org.python.pydev.parser.jython.ast.aliasType;
import org.python.pydev.parser.visitors.NodeUtils;

public class TemplateImportsVisitor extends VisitorBase {

	private final Map<String, String> imports;
	private final String projectName;

	public TemplateImportsVisitor(String projectName,
			Map<String, String> imports) {
		this.imports = imports;
		this.projectName = projectName;
	}

	@Override
	public Object visitImport(Import node) throws Exception {
		aliasType[] importedNames = node.names;
		for (aliasType importedName : importedNames) {
			NameTokType as = importedName.asname;
			String name = NodeUtils.getNameFromNameTok(importedName.name);

			String key = name;
			String value = name;

			if (as != null) {
				key = NodeUtils.getNameFromNameTok(as);
			}
			imports.put(key, value);
		}
		return null;
	}

	@Override
	public Object visitImportFrom(ImportFrom node) throws Exception {
		aliasType[] importedNames = node.names;
		String module = NodeUtils.getNameFromNameTok(node.module);
		for (aliasType importedName : importedNames) {
			NameTokType as = importedName.asname;
			String name = NodeUtils.getNameFromNameTok(importedName.name);

			String key;
			String value;

			if (as == null) {
				key = name;
				value = projectName + "." + module + "." + name;
			} else {
				key = NodeUtils.getNameFromNameTok(as);
				value = projectName + "." + module + "." + name;
			}
			imports.put(key, value);
		}
		return null;
	}

	@Override
	protected Object unhandled_node(SimpleNode node) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void traverse(SimpleNode node) throws Exception {
		node.traverse(this);
	}
}
