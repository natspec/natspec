package de.devboost.natspec.testscripting.python.patterns.parts;

import java.util.Arrays;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DoubleArgument;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.patterns.parts.styling.Style;
import de.devboost.natspec.testscripting.patterns.parts.DateParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

// TODO ilja document this
public class DynamicTypeParameter implements ISyntaxPatternPart, IComparable,
		ICompletable {

	private static final int HASH_CODE = DynamicTypeParameter.class.getName().hashCode();

	private List<? extends ISyntaxPatternPart> matchesToTry = Arrays
			.asList(new ISyntaxPatternPart[] { new IntegerArgument(),
					new DoubleArgument(), new DateParameter(),
					new StringParameter() });
	
	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		return "*";
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words,
			IPatternMatchContext context) {
		
		ISyntaxPatternPartMatch matchedWords;
		for (ISyntaxPatternPart matchToTry : this.matchesToTry) {
			matchedWords = matchToTry.match(words, context);
			if (matchedWords != null) {
				return matchedWords;
			}
		}

		return null;
	}

	@Override
	public boolean isEqualTo(Object object) {
		return object != null && object instanceof DynamicTypeParameter;
	}

	@Override
	public int computeHashCode() {
		return HASH_CODE;
	}

	@Override
	public String toSimpleString() {
		return "<DynamicType>";
	}

	@Override
	public void setStyle(Style style) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Style getStyle() {
		// TODO ibauer implement this
		return null;
	}
}
