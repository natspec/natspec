package de.devboost.natspec.testscripting.python.visitors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.parser.jython.ast.ClassDef;
import org.python.pydev.parser.jython.ast.FunctionDef;
import org.python.pydev.parser.jython.ast.VisitorBase;
import org.python.pydev.parser.visitors.NodeUtils;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.IPatternRegistration;
import de.devboost.natspec.testscripting.PatternRegistration;
import de.devboost.natspec.testscripting.python.PythonDynamicSyntaxPatternProvider;

public class ClassDefVisitor extends VisitorBase {
	
	private final IAdaptable adaptable;
	private final List<FunctionDef> methodsWithTextSyntax;
	private final String projectName;
	private final String moduleName;
	private final PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider;

	public ClassDefVisitor(
			PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider,
			IAdaptable adaptable, String projectName, String moduleName) {
		
		super();
		
		this.dynamicSyntaxPatternProvider = dynamicSyntaxPatternProvider;
		this.adaptable = adaptable;
		this.projectName = projectName;
		this.moduleName = moduleName;
		
		this.methodsWithTextSyntax = new ArrayList<FunctionDef>();
	}

	@Override
	public Object visitClassDef(ClassDef node) throws Exception {

		final String className = NodeUtils.getNameFromNameTok(node.name);
		final List<ISyntaxPattern<?>> patterns = new ArrayList<ISyntaxPattern<?>>();

		try {
			FunctionDefVisitor functionDefVisitor = new FunctionDefVisitor(
					patterns, projectName, moduleName, className,
					methodsWithTextSyntax);
			node.accept(functionDefVisitor);
		} catch (Exception e) {
			// TODO (ilja) Handle this
			System.out.println(e);
		}

		if (patterns.isEmpty()) {
			return null;
		}
		
		// FIXME Check type of 'adaptable'
		IFile file = (IFile) adaptable;
		String typename = moduleName + "." + className;
		IPatternRegistration registration = new PatternRegistration(file, typename, patterns);
		List<IPatternRegistration> registrations = Collections.singletonList(registration);
		dynamicSyntaxPatternProvider.register(registrations);
		
		// TODO Is this correct?
		return null;
	}

	@Override
	public void traverse(SimpleNode node) throws Exception {
		node.traverse(this);
	}

	@Override
	protected Object unhandled_node(SimpleNode node) throws Exception {
		return null;
	}
}
