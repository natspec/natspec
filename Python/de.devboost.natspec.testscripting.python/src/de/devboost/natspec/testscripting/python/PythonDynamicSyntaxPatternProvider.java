package de.devboost.natspec.testscripting.python;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.patterns.AbstractDynamicSyntaxPatternProvider;
import de.devboost.natspec.testscripting.python.builder.PythonTemplateFileFinder;

public class PythonDynamicSyntaxPatternProvider extends
		AbstractDynamicSyntaxPatternProvider {

	private final PythonFieldTypeCache fieldTypeCache = new PythonFieldTypeCache();
	
	@Override
	protected List<ISyntaxPattern<?>> doGetPatterns(IFile natspecFile) {
		IFile templateFile = new PythonTemplateFileFinder().findTemplateFile(natspecFile);
		Collection<String> allFieldTypes = fieldTypeCache.getAllFieldTypes(templateFile);
		if (allFieldTypes == null) {
			return null;
		}
		
		List<ISyntaxPattern<?>> patterns = new LinkedList<ISyntaxPattern<?>>();
		
		Map<String, List<ISyntaxPattern<?>>> testSupportClassToPatternsMap = getTestSupportClassToPatternsMap();
		for (String fieldType : allFieldTypes) {
			List<ISyntaxPattern<?>> pattern = testSupportClassToPatternsMap.get(fieldType);
			if (pattern == null) {
				continue;
			}
			patterns.addAll(pattern);
		}
		return patterns;
	}

	@Override
	public boolean registerPatterns(String qualifiedTypeName,
			List<ISyntaxPattern<?>> newPatterns) {
		return super.registerPatterns(qualifiedTypeName, newPatterns);
	}
	
	public PythonFieldTypeCache getFieldTypeCache() {
		return fieldTypeCache;
	}
}
