package de.devboost.natspec.testscripting.python;

import org.eclipse.core.resources.IFile;

import de.devboost.natspec.testscripting.AbstractGeneratedFileFinder;
import de.devboost.natspec.testscripting.patterns.ICommentGenerator;
import de.devboost.natspec.testscripting.python.patterns.PythonDynamicSyntaxCodeGenerator;

public class GeneratedPythonFileFinder extends AbstractGeneratedFileFinder {

	@Override
	protected String getExtensionForGeneratedFiles() {
		return PythonNatSpecConstants.PYTHON_EXTENSION;
	}

	@Override
	protected ICommentGenerator getCommentGenerator() {
		return PythonDynamicSyntaxCodeGenerator.COMMENT_GENERATOR;
	}

	@Override
	protected String getGeneratedFileNameWithoutExtension(IFile natspecFile) {
		// FIXME ibauer Removing the file extension is not enough. One must also escaped all characters which are not
		// valid Python identifiers.
		return removeExtension(natspecFile);
	}

	private String removeExtension(IFile natspecFile) {
		String filename = natspecFile.getName();
		String fileExtension = natspecFile.getFileExtension();
		return filename.substring(0, filename.length() - fileExtension.length() - 1);
	}
}
