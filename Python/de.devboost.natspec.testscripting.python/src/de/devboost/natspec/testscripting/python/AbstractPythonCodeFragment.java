package de.devboost.natspec.testscripting.python;

import de.devboost.natspec.testscripting.patterns.AbstractCodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;

public abstract class AbstractPythonCodeFragment extends AbstractCodeFragment
		implements IPythonCodeFragment {

	@Override
	public void generateSourceCode(ICodeGenerationContext context,
			boolean isMultipleMatch, int position) {
		
		generateSourceCode(context);
	}
}
