package de.devboost.natspec.testscripting.python;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.python.pydev.core.IGrammarVersionProvider;
import org.python.pydev.core.MisconfigurationException;
import org.python.pydev.parser.PyParser;
import org.python.pydev.shared_core.callbacks.ICallback0;

public class PyDevBuilderVisitor extends org.python.pydev.builder.PyDevBuilderVisitor {

	@Override
	public void visitChangedResource(IResource resource,
			ICallback0<IDocument> document, IProgressMonitor monitor) {
		PyParser pyParser = new PyParser(new IGrammarVersionProvider() {
			@Override
			public int getGrammarVersion() throws MisconfigurationException {
				return IGrammarVersionProvider.GRAMMAR_PYTHON_VERSION_2_7;
			}
		});
		pyParser.setDocument(document.call(), resource);
		pyParser.reparseDocument();
	}

	@Override
	public void visitRemovedResource(IResource resource,
			ICallback0<IDocument> document, IProgressMonitor monitor) {
		// TODO Auto-generated method stub
		
	}

}
