package de.devboost.natspec.testscripting.python;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import de.devboost.natspec.testscripting.AbstractWorkspaceJob;

public class RegisterPythonProvidersJob extends AbstractWorkspaceJob {

	public RegisterPythonProvidersJob() {
		super("Register Python Syntax Pattern Providers");
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		PythonConnectorPlugin pythonConnectorPlugin = PythonConnectorPlugin
				.getInstance();
		if (pythonConnectorPlugin != null) {
			pythonConnectorPlugin.registerProviders();
		} else {
			PythonConnectorPlugin.logWarning(getClass().getSimpleName()
					+ ".run() plugin is null", null);
		}
		return Status.OK_STATUS;
	}

}
