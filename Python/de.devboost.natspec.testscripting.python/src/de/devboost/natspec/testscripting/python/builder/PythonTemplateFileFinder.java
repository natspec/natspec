package de.devboost.natspec.testscripting.python.builder;

import de.devboost.natspec.testscripting.AbstractTemplateFileFinder;
import de.devboost.natspec.testscripting.python.PythonNatSpecConstants;

public class PythonTemplateFileFinder extends AbstractTemplateFileFinder {

	public PythonTemplateFileFinder() {
		super(PythonNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME,
				PythonNatSpecConstants.PYTHON_EXTENSION);
	}

}
