package de.devboost.natspec.testscripting.python;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.patterns.parts.DefaultSentencePartMatch;
import de.devboost.natspec.patterns.parts.DoubleMatch;
import de.devboost.natspec.patterns.parts.FloatMatch;
import de.devboost.natspec.testscripting.patterns.AbstractCodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.ICodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.parts.AbstractMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.parts.BooleanMatch;
import de.devboost.natspec.testscripting.patterns.parts.DateMatch;
import de.devboost.natspec.testscripting.patterns.parts.ListMatch;
import de.devboost.natspec.testscripting.patterns.parts.ManyMatch;
import de.devboost.natspec.testscripting.patterns.parts.ObjectMatch;
import de.devboost.natspec.testscripting.patterns.parts.StringMatch;
import de.devboost.natspec.testscripting.python.patterns.PythonDynamicSyntaxCodeGenerator;

public class PythonCodeGeneratorFactory extends AbstractMatchCodeGeneratorFactory implements ICodeGeneratorFactory<PythonDynamicSyntaxCodeGenerator> {

	@Override
	public PythonDynamicSyntaxCodeGenerator createCodeGenerator(
			ISyntaxPatternMatch<PythonDynamicSyntaxCodeGenerator> match,
			String qualifiedTypeName, String methodName,
			String[] parameterTypes,
			String qualifiedReturnType,
			Map<Integer, ISyntaxPatternPartMatch> argumentMatches,
			String returnVariableName, boolean parameterSyntax) {
		return new PythonDynamicSyntaxCodeGenerator(match, qualifiedTypeName, methodName, qualifiedReturnType, argumentMatches, returnVariableName);
	}
	
	@Override
	public ICodeFragment createGenerator(final BooleanMatch match) {
		// FIXME ibauer
		throw new RuntimeException("Boolean arguments are not supported for Python (yet).");
	}

	@Override
	public ICodeFragment createGeneratorForUnknown(
			final ISyntaxPatternPartMatch match) {
		return new AbstractCodeFragment() {
			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode("# TODO unknown match: " + match);
			}
		};
	}
	
	@Override
	public ICodeFragment createGenerator(final DateMatch match) {
		return new AbstractPythonCodeFragment() {
			
			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				// pythons fromtimestamp get time in seconds, java is
				// milliseconds, so devide by 1000
				context.addImport("import datetime");
				context.addCode("datetime.datetime.fromtimestamp("
						+ match.getDate().getTime() / 1000 + "L)");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final StringMatch match) {
		return new AbstractPythonCodeFragment() {
			
			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				// TODO Escape string value
				context.addImport("from natspec_utils.stringutils import stringToUnicode as u");
				context.addCode("u(\"" + match.getValue() + "\")");
			}
		};
	}
	
	@Override
	public ICodeFragment createGenerator(final ListMatch match) {
		return new AbstractPythonCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				List<ISyntaxPatternPartMatch> elementMatches = match
						.getElementMatches();
				context.addCode("[");
				Iterator<ISyntaxPatternPartMatch> iterator = elementMatches
						.iterator();
				while (iterator.hasNext()) {
					ISyntaxPatternPartMatch elementMatch = iterator.next();
					ICodeFragment generator = createGenerator(elementMatch);
					generator.generateSourceCode(context);
					if (iterator.hasNext()) {
						context.addCode(", ");
					}
				}
				context.addCode("]");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final ObjectMatch match) {
		// FIXME ibauer implement this
		return null;
	}

	@Override
	public ICodeFragment createGenerator(final DoubleMatch match) {
		return new AbstractPythonCodeFragment() {
			
			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(Double.toString(match.getValue()));
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final FloatMatch match) {
		return new AbstractPythonCodeFragment() {
			
			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(Float.toString(match.getValue()));
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final DefaultSentencePartMatch match) {
		return new AbstractPythonCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				List<Word> words = match.getMatchedWords();
				List<String> matchedStrings = WordUtil.INSTANCE
						.toStringList(words);
				context.addCode("[");
				Iterator<String> iterator = matchedStrings.iterator();
				while (iterator.hasNext()) {
					String word = iterator.next();
					//TODO ibauer maybe we need escaping here?
					context.addCode("'"+word+"'");
					if (iterator.hasNext()) {
						context.addCode(", ");
					}
				}
				context.addCode("]");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(ManyMatch match) {
		throw new RuntimeException("The @Many annotation is not supported for Python.");
	}
}
