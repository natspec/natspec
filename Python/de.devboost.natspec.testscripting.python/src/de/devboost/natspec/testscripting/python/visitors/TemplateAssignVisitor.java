package de.devboost.natspec.testscripting.python.visitors;

import java.util.List;
import java.util.Map;

import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.parser.jython.ast.Assign;
import org.python.pydev.parser.jython.ast.Attribute;
import org.python.pydev.parser.jython.ast.Call;
import org.python.pydev.parser.jython.ast.Name;
import org.python.pydev.parser.jython.ast.VisitorBase;
import org.python.pydev.parser.jython.ast.exprType;
import org.python.pydev.parser.visitors.NodeUtils;

import de.devboost.natspec.testscripting.Field;

public class TemplateAssignVisitor extends VisitorBase {

	private final Map<String, String> imports;
	private final List<Field> fields;

	public TemplateAssignVisitor(Map<String, String> imports, List<Field> fields) {
		super();
		this.imports = imports;
		this.fields = fields;
	}

	@Override
	public Object visitAssign(Assign node) throws Exception {

		String name = null;
		exprType[] targets = node.targets;
		boolean foundField = false;
		for (exprType target : targets) {
			boolean isNotAttribute = !(target instanceof Attribute);
			if (isNotAttribute) {
				continue;
			}
			Attribute attribute = (Attribute) target;
			boolean isNotName = !(attribute.value instanceof Name);
			if (isNotName) {
				continue;
			}
			Name nameNode = (Name) attribute.value;
			name = nameNode.id;
			boolean isNotSelf = !name.equals("self");
			if (isNotSelf) {
				continue;
			}
			foundField = true;
			break;
		}
		if (!foundField) {
			return null;
		}
		
		exprType value = node.value;
		boolean isNotCall = !(value instanceof Call);
		if (isNotCall) {
			return null;
		}
		Call call = (Call) value;

		String fieldType;
		String key;

		if (call.func instanceof Name) {
			key = ((Name) call.func).id;
			fieldType = "";
		} else if (call.func instanceof Attribute) {
			Attribute attribute = (Attribute) call.func;
			String attr = NodeUtils.getNameFromNameTok(attribute.attr);
			fieldType = attr;
			key = ((Name) attribute.value).id;
		} else {
			return null;
		}

		String module = imports.get(key);
		if (!fieldType.isEmpty()) {
			module += "." + fieldType;
		}

		fields.add(new Field(key, module));

		return null;
	}

	@Override
	protected Object unhandled_node(SimpleNode node) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void traverse(SimpleNode node) throws Exception {
		node.traverse(this);
	}
}
