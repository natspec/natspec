package de.devboost.natspec.testscripting.python.builder;

import java.util.regex.Pattern;

import de.devboost.natspec.testscripting.AbstractTemplateFileFinder;
import de.devboost.natspec.testscripting.builder.AbstractTemplateInstantiator;
import de.devboost.natspec.testscripting.python.PythonNatSpecConstants;

// TODO The replacement logic in this class will not work for arbitrary Python
// template classes. To have a clean implementation we must use a minimal Python
// parser/lexer.
public class PythonTemplateInstantiator extends AbstractTemplateInstantiator {

	private final static Pattern REGEX_IMPORTS_PLACEHOLDER = Pattern.compile("\"\"\" @Imports \"\"\"");
	private final static Pattern REGEX_METHOD_BODY_PLACEHOLDER = Pattern.compile("\"\"\" @MethodBody \"\"\"");

	public PythonTemplateInstantiator(
			AbstractTemplateFileFinder templateFileFinder) {
		super(templateFileFinder);
	}

	/**
	 * addImports search for an @Imports placeholder. If not present we add
	 * imports at the beginning of the file.
	 */
	@Override
	public String addImports(String importStatements, String result) {
		if (importStatements.isEmpty()) {
			return result;
		}

		// check if @Imports is used, if not add imports to start of the file
		int index = getIndexOf(result, REGEX_IMPORTS_PLACEHOLDER);
		if (index < 0) {
			return importStatements + "\n" + result;
		}
		result = replace(result, REGEX_IMPORTS_PLACEHOLDER, importStatements);
		return result;
	}

	/**
	 * Does nothing (i.e., return result as it is).
	 */
	@Override
	public String replacePackageName(String packageName, String result) {
		return result;
	}

	@Override
	public String replaceMethodBody(String generatedStatements, String result) {
		result = replace(result, REGEX_METHOD_BODY_PLACEHOLDER,
				generatedStatements);
		return result;
	}

	@Override
	public String replaceClassName(String className, String result) {
		String templateClassName = PythonNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME
				.substring(
						0,
						PythonNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME
								.lastIndexOf("."
										+ PythonNatSpecConstants.PYTHON_EXTENSION));
		result = replaceAll(result, templateClassName, className);
		// TODO This is not exact. There might be more whitespace characters

		return result;
	}

	@Override
	public String addMethods(String generatedMethods, String result) {
		// There is currently no support to add methods to Python NatSpec
		// templates.
		return result;
	}

	@Override
	protected String doIndentation(String templateContent, String replacement,
			int index) {
		
		// FIXME 2.3 ibauer also replace tabs?
		int spaces = countSpacesBeforeIndex(templateContent, index);
		String spacesString = getCharNTimesString(spaces, " ");
		String replacementWithSpaces = replacement.replace("\n", "\n"
				+ spacesString);
		return replacementWithSpaces;
	}
}
