package de.devboost.natspec.testscripting.python;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.devboost.natspec.registries.SynonymProviderRegistry;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;
import de.devboost.natspec.testscripting.IRevalidationVisitor;
import de.devboost.natspec.testscripting.RevalidateScriptsListener;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.python.builder.PythonTestTemplateChangeListener;
import de.devboost.natspec.testscripting.synonyms.PlainTextFileSynonymProvider;

public class PythonConnectorPlugin extends AbstractUIPlugin {

	private static final String PLUGIN_ID = PythonConnectorPlugin.class
			.getPackage().getName();

	private static PythonConnectorPlugin instance;

	private Set<NatspecResource> resources = new LinkedHashSet<NatspecResource>();

	private final PlainTextFileSynonymProvider synonymProvider = new PlainTextFileSynonymProvider();

	private PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider;
	private final IRevalidationVisitor revalidationListener = new PythonRevalidationVisitor();


	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		instance = this;

		RevalidateScriptsListener listener = new RevalidateScriptsListener();
		SyntaxPatternRegistry.REGISTRY.addListener(listener);
		SynonymProviderRegistry.REGISTRY.addListener(listener);
		
		TestConnectorPlugin testConnectorPlugin = TestConnectorPlugin
				.getInstance();
		if (testConnectorPlugin != null) {
			testConnectorPlugin.addRevalidationVisitor(revalidationListener);
		}
		
		// FIXME Use cache to store list of file that provide patterns between
		// Eclipse sessions (see CompositeCompilationHandler)
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();

		root.accept(new IResourceVisitor() {
			
			@Override
			public boolean visit(IResource resource) throws CoreException {
				boolean isPythonFile = isPythonFile(resource);
				if (isPythonFile) {
					IFile file = (IFile) resource;
					
					file.touch(new NullProgressMonitor());
				}
				return true; 
			}

			private boolean isPythonFile(IResource resource) {
				String extension = resource.getFileExtension();
				return PythonNatSpecConstants.PYTHON_EXTENSION.equals(extension);
			}
		});
		
		Job job = new RegisterPythonProvidersJob();
		job.schedule();
	}

	public void registerProviders() {

		dynamicSyntaxPatternProvider = new PythonDynamicSyntaxPatternProvider();
		SyntaxPatternRegistry.REGISTRY.add(dynamicSyntaxPatternProvider);

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		workspace.addResourceChangeListener(
				new PythonTestTemplateChangeListener(),
				IResourceChangeEvent.POST_CHANGE);
	}

	public PlainTextFileSynonymProvider getSynonymProvider() {
		return synonymProvider;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		// we must do this at the very end, since the tearDown() method requires
		// access to the plug-in instance (to save its state to the workspace
		// metadata folder)
		instance = null;
		super.stop(context);
	}

	public static PythonConnectorPlugin getInstance() {
		return instance;
	}

	public void addTestScript(NatspecResource resource) {
		resources.add(resource);
	}

	public Set<NatspecResource> getTestScripts() {
		return resources;
	}

	public PythonDynamicSyntaxPatternProvider getDynamicSyntaxPatternProvider() {
		return dynamicSyntaxPatternProvider;
	}

	// FIXME 2.3 Use PluginLogger instead
	/**
	 * Helper method for error logging.
	 * 
	 * @param message
	 *            the error message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	public static IStatus logError(String message, Throwable throwable) {
		return log(IStatus.ERROR, message, throwable);
	}

	/**
	 * Helper method for logging informations.
	 * 
	 * @param message
	 *            the information message to log
	 * @param throwable
	 *            the exception that describes the information in detail (can be
	 *            null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logInfo(String message, Throwable throwable) {
		return log(IStatus.INFO, message, throwable);
	}

	/**
	 * Helper method for logging warnings.
	 * 
	 * @param message
	 *            the warning message to log
	 * @param throwable
	 *            the exception that describes the warning in detail (can be
	 *            null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logWarning(String message, Throwable throwable) {
		return log(IStatus.WARNING, message, throwable);
	}

	/**
	 * Helper method for logging.
	 * 
	 * @param type
	 *            the type of the message to log
	 * @param message
	 *            the message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	protected static IStatus log(int type, String message, Throwable throwable) {
		IStatus status;
		if (throwable != null) {
			status = new Status(type, PythonConnectorPlugin.PLUGIN_ID, 0,
					message, throwable);
		} else {
			status = new Status(type, PythonConnectorPlugin.PLUGIN_ID, message);
		}
		final PythonConnectorPlugin pluginInstance = PythonConnectorPlugin
				.getInstance();
		if (pluginInstance == null) {
			System.err.println(message);
			if (throwable != null) {
				throwable.printStackTrace();
			}
		} else {
			pluginInstance.getLog().log(status);
		}
		return status;
	}

}
