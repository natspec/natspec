package de.devboost.natspec.testscripting.python;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;

import de.devboost.natspec.testscripting.Field;
import de.devboost.natspec.testscripting.IFieldCache;

/**
 *A Python-specific implementation of the {@link IFieldCache} interface.
 */
public class PythonFieldTypeCache implements IFieldCache {

	/**
	 * A map from NatSpec template files to a set of all fields in the template
	 * class.
	 */
	private Map<IFile, Set<Field>> fileToFieldsMap = new LinkedHashMap<IFile, Set<Field>>();

	/**
	 * Returns the types of all fields declared in the class contained in the
	 * given file and all super classes (direct and indirect) of this class.
	 */
	@Override
	public Collection<String> getAllFieldTypes(IFile templateFile) {
		return getFieldNameToTypeMap(templateFile).values();
	}

	/**
	 * Clears all data stored in the cache for the given file (i.e., the types
	 * of its fields and the file containing its super class).
	 */
	// FIXME ibauer check whether this method is called at the right point in time
	@Override
	public void clearCache(IFile file) {
		fileToFieldsMap.remove(file);
	}
	
	/**
	 * Adds the given field type to the set of types that are registered for the
	 * given Python template file.
	 * 
	 * @param file a Python template file
	 * @param fieldType the type to add
	 */
	public void addField(IFile file, Field field) {
		if (field==null) {
			return;
		}
		boolean containsKey = fileToFieldsMap.containsKey(file);
		if (!containsKey) {
			fileToFieldsMap.put(file, new LinkedHashSet<Field>());
		}
		
		Set<Field> fields = fileToFieldsMap.get(file);
		fields.add(field);
	}

	@Override
	public Map<String, String> getFieldNameToTypeMap(IFile templateFile) {
		Set<Field> fields = fileToFieldsMap.get(templateFile);
		Map<String, String> fieldNameToTypeMap = new LinkedHashMap<String, String>();
		if (fields==null) {
			return fieldNameToTypeMap;
		}
		for (Field field : fields) {
			fieldNameToTypeMap.put(field.getName(), field.getType());
		}
		return fieldNameToTypeMap;
	}
}
