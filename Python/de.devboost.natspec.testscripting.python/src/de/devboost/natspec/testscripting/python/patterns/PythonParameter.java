package de.devboost.natspec.testscripting.python.patterns;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.testscripting.patterns.IAnnotation;
import de.devboost.natspec.testscripting.patterns.IParameter;

public class PythonParameter implements IParameter {

	private final String name;
	private final PythonType type;

	public PythonParameter(String name, PythonType type) {
		super();
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public PythonType getType() {
		return type;
	}

	@Override
	public List<IAnnotation> getAnnotations() {
		return Collections.emptyList();
	}
}
