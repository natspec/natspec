package de.devboost.natspec.testscripting.python.visitors;

import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.parser.jython.ast.VisitorBase;


public class ReturnVisitor extends VisitorBase {

	@Override
	public Object visitReturn(org.python.pydev.parser.jython.ast.Return node) throws ReturnFound {
		if (node.value!=null) {
			throw new ReturnFound();
		}
		return null;
	};
	
	@Override
	public void traverse(SimpleNode node) throws Exception {
		node.traverse(this);
	}

	@Override
	protected Object unhandled_node(SimpleNode node) throws Exception {
		return null;
	}
	

}
