package de.devboost.natspec.testscripting.python.patterns;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.devboost.natspec.testscripting.patterns.AbstractClass;
import de.devboost.natspec.testscripting.patterns.IClass;

public class PythonType extends AbstractClass {

	private final String qualifiedName;
	private final Set<String> allSuperTypes;
	private final List<? extends IClass> typeArguments;
	
	public PythonType(String qualifiedName) {
		this(qualifiedName, Collections.<IClass>emptyList());
	}
	
	public PythonType(String qualifiedName, List<? extends IClass> typeArguments) {
		super();
		this.qualifiedName = qualifiedName;
		this.allSuperTypes = new LinkedHashSet<String>();
		this.allSuperTypes.add(qualifiedName);
		this.typeArguments = typeArguments;
	}

	@Override
	public String getQualifiedName() {
		return this.qualifiedName;
	}

	@Override
	public String getQualifiedNameWithTypeArguments() {
		return this.qualifiedName;
	}

	@Override
	public List<? extends IClass> getTypeArguments() {
		return typeArguments;
	}

	@Override
	public List<? extends IClass> getSuperTypes() {
		return Collections.emptyList();
	}

	@Override
	public Set<String> getAllSuperTypes() {
		return allSuperTypes;
	}
}
