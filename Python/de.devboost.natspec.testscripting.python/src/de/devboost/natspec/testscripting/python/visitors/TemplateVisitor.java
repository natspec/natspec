package de.devboost.natspec.testscripting.python.visitors;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.parser.jython.ast.ClassDef;
import org.python.pydev.parser.jython.ast.VisitorBase;
import org.python.pydev.parser.visitors.NodeUtils;

import de.devboost.natspec.testscripting.Field;
import de.devboost.natspec.testscripting.python.PythonDynamicSyntaxPatternProvider;
import de.devboost.natspec.testscripting.python.PythonFieldTypeCache;
import de.devboost.natspec.testscripting.python.PythonNatSpecConstants;

public class TemplateVisitor extends VisitorBase {

	private final PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider;
	private final IFile file;
	private final Map<String, String> imports;
	
	public TemplateVisitor(
			PythonDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider,
			IFile file, Map<String, String> imports) {
		super();
		this.dynamicSyntaxPatternProvider = dynamicSyntaxPatternProvider;
		this.file = file;
		this.imports = imports;
	}

	@Override
	public Object visitClassDef(ClassDef node) throws Exception {
		final String className = NodeUtils.getNameFromNameTok(node.name);
		List<Field> fields = new LinkedList<Field>();
		if (className.equals(PythonNatSpecConstants.TEST_CASE_TEMPLATE_CLASS_NAME)) {
			TemplateMethodVisitor initVisitor = new TemplateMethodVisitor(imports, fields);
			node.accept(initVisitor);
		}
		
		PythonFieldTypeCache fieldTypeCache = dynamicSyntaxPatternProvider.getFieldTypeCache();
		if (fieldTypeCache == null) {
			return null;
		}
		fieldTypeCache.clearCache(file);
		for (Field field : fields) {
			fieldTypeCache.addField(file, field);
		}
		
		return null;
	}
	
	@Override
	protected Object unhandled_node(SimpleNode node) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void traverse(SimpleNode node) throws Exception {
		node.traverse(this);
	}
}
