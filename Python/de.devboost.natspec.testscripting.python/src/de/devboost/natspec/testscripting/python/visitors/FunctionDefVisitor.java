package de.devboost.natspec.testscripting.python.visitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.python.pydev.parser.jython.SimpleNode;
import org.python.pydev.parser.jython.ast.FunctionDef;
import org.python.pydev.parser.jython.ast.Name;
import org.python.pydev.parser.jython.ast.Str;
import org.python.pydev.parser.jython.ast.VisitorBase;
import org.python.pydev.parser.jython.ast.decoratorsType;
import org.python.pydev.parser.jython.ast.exprType;
import org.python.pydev.parser.visitors.NodeUtils;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.patterns.NullValidationCallback;
import de.devboost.natspec.testscripting.patterns.PatternParser;
import de.devboost.natspec.testscripting.python.PythonCodeGeneratorFactory;
import de.devboost.natspec.testscripting.python.PythonNatSpecConstants;
import de.devboost.natspec.testscripting.python.PythonParameterFactory;
import de.devboost.natspec.testscripting.python.patterns.PythonDynamicSyntaxCodeGenerator;
import de.devboost.natspec.testscripting.python.patterns.PythonParameter;
import de.devboost.natspec.testscripting.python.patterns.PythonType;

public class FunctionDefVisitor extends VisitorBase {

	private final PatternParser<PythonDynamicSyntaxCodeGenerator> patternParser = new PatternParser<PythonDynamicSyntaxCodeGenerator>(
			new PythonParameterFactory(), new PythonCodeGeneratorFactory());

	private final List<ISyntaxPattern<?>> patterns;
	private final String projectName;
	private final String moduleName;
	private final String className;
	private final List<FunctionDef> methodsWithTextSyntax;

	public FunctionDefVisitor(List<ISyntaxPattern<?>> patterns,
			String projectName, String moduleName, String className,
			List<FunctionDef> methodsWithTextSyntax) {

		this.patterns = patterns;
		this.projectName = projectName;
		this.moduleName = moduleName;
		this.className = className;
		this.methodsWithTextSyntax = methodsWithTextSyntax;
	}

	@Override
	public Object visitFunctionDef(FunctionDef node) throws Exception {
		if (node.decs != null) {
			process(node);
		}
		node.traverse(this);
		return null;
	}

	private void process(FunctionDef node) {
		for (decoratorsType dec : node.decs) {
			Name decoratorsName = (Name) dec.func;

			boolean isTextSyntax = PythonNatSpecConstants.TEXT_SYNTAX_NAME
					.equals(decoratorsName.id);
			if (!isTextSyntax) {
				return;
			}

			String methodName = NodeUtils.getNameFromNameTok(node.name);
			List<PythonParameter> args = new ArrayList<PythonParameter>();

			List<String> patternSpecifications = new LinkedList<String>();

			if (dec.args[0] instanceof Str) {
				String pattern = ((Str) dec.args[0]).s;
				patternSpecifications.add(pattern);
			}
			if (dec.args[0] instanceof org.python.pydev.parser.jython.ast.List) {
				org.python.pydev.parser.jython.ast.List castedPatterns = (org.python.pydev.parser.jython.ast.List) dec.args[0];
				for (exprType pattern : castedPatterns.elts) {
					if (pattern instanceof Str) {
						String castedPattern = ((Str) pattern).s;
						patternSpecifications.add(castedPattern);
					}
				}
			}

			Map<String, SimpleNode> typeListMap = new HashMap<String, SimpleNode>();
			try {
				typeListMap.put(
						NodeUtils.getNameFromNameTok(dec.keywords[0].arg),
						dec.keywords[0].value);
				typeListMap.put(
						NodeUtils.getNameFromNameTok(dec.keywords[1].arg),
						dec.keywords[1].value);
			} catch (ArrayIndexOutOfBoundsException e) {
			}

			// TODO (ilja) remove this
			System.out.println(typeListMap);
			System.out.println(node.args);

			// TODO ibauer implement function annotations
			// TODO ibauer implement kwargs

			for (int i = 0; i < node.args.args.length; i++) {
				SimpleNode arg = node.args.args[i];
				if (!((Name) arg).id.equals("self")) {

					// default parameter type is DynamicTypeParameter
					String paramType = "*";

					List<PythonType> typeArgumentsTypes = new LinkedList<PythonType>();
					try {
						org.python.pydev.parser.jython.ast.List typeList = (org.python.pydev.parser.jython.ast.List) typeListMap
								.get("types");
						SimpleNode parameter = typeList.elts[i - 1];
						// check for type parameter as string or imported
						// class
						if (parameter instanceof Str) {
							paramType = ((Str) parameter).s;
						} else if (parameter instanceof Name) {
							paramType = ((Name) parameter).id;
						}

						try {
							String[] typeArguments = paramType.split("<");
							paramType = typeArguments[0];
							typeArguments = typeArguments[1].split(">");
							typeArgumentsTypes.add(new PythonType(
									typeArguments[0]));
						} catch (Exception e) {

						}
					} catch (Exception e) {
					}
					PythonType argumentType = new PythonType(paramType,
							typeArgumentsTypes);
					// TODO ibauer Set parameter name
					args.add(new PythonParameter(null, argumentType));
				}

			}

			boolean hasReturn = false;
			try {
				node.accept(new ReturnVisitor());
			} catch (ReturnFound e) {
				hasReturn = true;
			} catch (Exception e) {
				// ignore this
			}

			PythonType returnType = null;

			if (hasReturn) {
				if (typeListMap.containsKey("return_type")) {
					returnType = new PythonType(
							((Str) typeListMap.get("return_type")).s);
				} else {
					returnType = new PythonType("*");
				}
			}

			for (String patternSpecification : patternSpecifications) {
				ISyntaxPattern<?> pattern = patternParser.parse(projectName,
						moduleName + "." + className, methodName,
						patternSpecification, args, returnType,
						NullValidationCallback.INSTANCE, null, false, false); // TODO ibauer Add
														// PythonValidationCallback
				patterns.add(pattern);
			}
			methodsWithTextSyntax.add(node);
		}
	}

	@Override
	public void traverse(SimpleNode node) throws Exception {
		node.traverse(this);
	}

	@Override
	protected Object unhandled_node(SimpleNode node) throws Exception {
		return null;
	}
}
