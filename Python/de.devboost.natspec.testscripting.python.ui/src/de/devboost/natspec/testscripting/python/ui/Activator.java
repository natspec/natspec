package de.devboost.natspec.testscripting.python.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.resource.natspec.ui.NatspecHyperlinkDetector;
import de.devboost.natspec.testscripting.python.ui.links.GeneratedFileLinkDetector;
import de.devboost.natspec.testscripting.python.ui.links.TestSupportMethodLinkDetector;

public class Activator extends AbstractUIPlugin {

	public static final String PLUGIN_ID = Activator.class.getPackage()
			.getName();

	// The shared instance
	private static Activator plugin;

	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;

		// add the hyper link processor
		NatspecHyperlinkDetector.ADDITIONAL_DETECTORS
				.add(new TestSupportMethodLinkDetector(
						NatSpecPlugin.USE_TREE_BASED_MATCHER,
						NatSpecPlugin.USE_PRIORITIZER));
		NatspecHyperlinkDetector.ADDITIONAL_DETECTORS
				.add(new GeneratedFileLinkDetector());
	}

	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	public static Activator getDefault() {
		return plugin;
	}

}
