package de.devboost.natspec.testscripting.python.ui.links;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.resource.natspec.INatspecLocationMap;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.ui.ErrorMessageHelper;
import de.devboost.natspec.resource.natspec.ui.IExtendedHyperlinkDetector;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.python.PythonCodeGeneratorFactory;
import de.devboost.shared.eclipse.jface.text.RegionOverlapHelper;

public class TestSupportMethodLinkDetector implements
		IExtendedHyperlinkDetector {

	private static final String FEATURE_NAME = "'Hyperlink'";
	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;

	private INatspecTextResource resource;

	public TestSupportMethodLinkDetector(boolean useOptimizedMatcher,
			boolean usePrioritizer) {

		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	@Override
	public IHyperlink[] detectHyperlinks(ITextViewer textViewer,
			IRegion region, boolean canShowMultipleHyperlinks) {

		if (!NatSpecPlugin.hasValidLicense()) {
			// no license, no hyper links
			ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();
			errorMessageHelper.showErrorMessage(FEATURE_NAME);

			// no license, no hyper links
			// FIX by CW do not: return new IHyperlink[0];
			// org.eclipse.jface.text.hyperlink.HyperlinkManager does not allow
			// this, returning null is better.
			return null;
		}

		List<IHyperlink> links = new ArrayList<IHyperlink>();
		for (EObject root : resource.getContents()) {
			if (root instanceof Document) {
				Document document = (Document) root;
				handleDocument(document, region, links);
			}
		}
		return links.toArray(new IHyperlink[links.size()]);
	}

	private void handleDocument(Document document, IRegion region,
			List<IHyperlink> links) {
		URI uri = resource.getURI();
		PatternMatchContextFactory contextFactory = PatternMatchContextFactory.INSTANCE;
		IPatternMatchContext matchContext = contextFactory
				.createPatternMatchContext(uri);

		List<Sentence> sentences = document.getContents();
		for (Sentence sentence : sentences) {
			// ignore comments
			if (SentenceUtil.INSTANCE.isComment(sentence)) {
				continue;
			}
			MatchService matchService = new MatchService(useOptimizedMatcher,
					usePrioritizer);
			List<ISyntaxPatternMatch<?>> matches = matchService.match(sentence,
					matchContext);
			for (ISyntaxPatternMatch<?> match : matches) {
				ISyntaxPattern<?> pattern = match.getPattern();
				if (pattern instanceof DynamicSyntaxPattern) {
					DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
					createLink(dynamicSyntaxPattern, sentence, region, links);
				}
			}
		}
	}

	private void createLink(DynamicSyntaxPattern<?> pattern, Sentence sentence,
			IRegion region, List<IHyperlink> links) {
		
		// Do only add links which point to Python methods here
		if (!(pattern.getCodeGeneratorFactory() instanceof PythonCodeGeneratorFactory)) {
			return;
		}
		
		INatspecLocationMap locationMap = resource.getLocationMap();
		int offset = locationMap.getCharStart(sentence);
		int length = locationMap.getCharEnd(sentence) - offset + 1;

		IRegion regionOfSentence = new Region(offset, length);
		RegionOverlapHelper regionOverlapHelper = new RegionOverlapHelper();
		if (regionOverlapHelper.overlaps(region, regionOfSentence)) {
			// overlap
			links.add(new TestSupportMethodLink(regionOfSentence, pattern));
		}
	}

	@Override
	public void setResource(INatspecTextResource textResource) {
		this.resource = textResource;
	}
}
