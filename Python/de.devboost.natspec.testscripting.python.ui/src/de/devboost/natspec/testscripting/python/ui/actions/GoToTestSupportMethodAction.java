package de.devboost.natspec.testscripting.python.ui.actions;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.python.pydev.core.ICodeCompletionASTManager;
import org.python.pydev.core.IModule;
import org.python.pydev.core.IToken;
import org.python.pydev.core.structure.CompletionRecursionException;
import org.python.pydev.editor.PyEdit;
import org.python.pydev.editor.model.ItemPointer;
import org.python.pydev.plugin.nature.PythonNature;
import org.python.pydev.shared_core.structure.Location;

import com.python.pydev.refactoring.actions.PyGoToDefinition;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;

public class GoToTestSupportMethodAction {

	private final ISyntaxPattern<?> pattern;

	public GoToTestSupportMethodAction(ISyntaxPattern<?> pattern) {
		super();
		this.pattern = pattern;
	}

	public void execute() {
		if (pattern instanceof DynamicSyntaxPattern) {
			DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
			execute(dynamicSyntaxPattern);
		}
	}

	protected void execute(DynamicSyntaxPattern<?> pattern) {
		String typeName = pattern.getQualifiedTypeName();
		String methodName = pattern.getMethodName();

		String projectName = pattern.getProjectName();
		if (projectName == null) {
			return;
		}

		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject(projectName);
		if (project == null) {
			return;
		}

		PythonNature nature = PythonNature.getPythonNature(project);
		if (nature == null) {
			return;
		}
		ICodeCompletionASTManager astManager = nature.getAstManager();
		if (astManager == null) {
			return;
		}

		typeName = removeProjectNameFromTypeName(typeName, projectName);
		String moduleName = getModuleName(typeName);
		String className = getClassName(typeName);

		IModule module = astManager.getModule(moduleName, nature, false);
		IToken foundMethod = null;
		try {
			foundMethod = astManager.getRepInModule(module, className + "."
					+ methodName, nature);
		} catch (CompletionRecursionException e1) {
			e1.printStackTrace();
		}
		if (foundMethod == null) {
			return;
		}

		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getShell();
		if (shell == null) {
			return;
		}

		int lineDefinition = foundMethod.getLineDefinition();
		int colDefinition = foundMethod.getColDefinition();

		Location startLocation = new Location(lineDefinition, colDefinition);
		ItemPointer itemPointer = new ItemPointer(module.getFile(),
				startLocation, startLocation);
		
		PyGoToDefinition.openDefinition(new ItemPointer[] { itemPointer },
				new PyEdit(), shell);
	}

	protected String removeProjectNameFromTypeName(String typeName,
			String projectName) {
		typeName = typeName.replace(projectName + ".", "");
		return typeName;
	}

	private String getModuleName(String typeName) {
		String[] typeParts = typeName.split("\\.");
		typeParts[typeParts.length - 1] = "";
		String moduleName = StringUtils.join(typeParts, ".");
		moduleName = StringUtils.removeEnd(moduleName, ".");
		return moduleName;
	}

	private String getClassName(String typeName) {
		String[] typeParts = typeName.split("\\.");
		String className = typeParts[typeParts.length - 1];
		return className;
	}
}
