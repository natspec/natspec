package de.devboost.natspec.testscripting.python.ui.links;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.python.ui.actions.GoToTestSupportMethodAction;

public class TestSupportMethodLink implements IHyperlink {

	private final IRegion region;
	private final DynamicSyntaxPattern<?> pattern;

	public TestSupportMethodLink(IRegion region, DynamicSyntaxPattern<?> pattern) {
		super();
		this.region = region;
		this.pattern = pattern;
	}

	@Override
	public IRegion getHyperlinkRegion() {
		return region;
	}

	@Override
	public String getTypeLabel() {
		return null;
	}

	@Override
	public String getHyperlinkText() {
		String typeName = pattern.getQualifiedTypeName();
		String methodName = pattern.getMethodName();
		return "Go to " + typeName + "." + methodName;
	}

	@Override
	public void open() {
		GoToTestSupportMethodAction action = new GoToTestSupportMethodAction(pattern);
		action.execute();
	}
}
