package de.devboost.natspec.testscripting.python.ui.links;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import de.devboost.natspec.testscripting.python.ui.actions.GoToGeneratedFileAction;

public class GeneratedFileLink implements IHyperlink {

	private IRegion region;
	private IFile generatedFile;
	private int lineNumber;

	public GeneratedFileLink(IRegion region,
			IFile generatedFile, int lineNumber) {
		super();
		this.region = region;
		this.generatedFile = generatedFile;
		this.lineNumber = lineNumber;
	}

	@Override
	public IRegion getHyperlinkRegion() {
		return region;
	}

	@Override
	public String getTypeLabel() {
		return null;
	}

	@Override
	public String getHyperlinkText() {
		return "Show Sentence in Generated File";
	}

	@Override
	public void open() {
		GoToGeneratedFileAction action = new GoToGeneratedFileAction(generatedFile, lineNumber);
		action.execute();
	}
}
