package de.devboost.natspec.testscripting.python.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.python.pydev.editor.PyEdit;
import org.python.pydev.editor.model.ItemPointer;
import org.python.pydev.shared_core.structure.Location;

import com.python.pydev.refactoring.actions.PyGoToDefinition;

public class GoToGeneratedFileAction {

	private final IFile generatedFile;
	private final int lineNumber;

	public GoToGeneratedFileAction(IFile generatedFile, int lineNumber) {
		super();
		this.generatedFile = generatedFile;
		this.lineNumber = lineNumber;
	}

	public void execute() {
		//FIXME: 2.3 implement this
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getShell();
		if (shell == null) {
			return;
		}
		Location startLocation = new Location(lineNumber, 0);
		ItemPointer itemPointer = new ItemPointer(generatedFile,
				startLocation, startLocation);
		
		PyGoToDefinition.openDefinition(new ItemPointer[] { itemPointer },
				new PyEdit(), shell);
	}
}
