package de.devboost.natspec.testscripting.python.ui.links;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.resource.natspec.INatspecLocationMap;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.ui.ErrorMessageHelper;
import de.devboost.natspec.resource.natspec.ui.IExtendedHyperlinkDetector;
import de.devboost.natspec.testscripting.python.GeneratedPythonFileFinder;
import de.devboost.natspec.testscripting.util.Position;
import de.devboost.shared.eclipse.jface.text.RegionOverlapHelper;

public class GeneratedFileLinkDetector implements IExtendedHyperlinkDetector {

	private static final String FEATURE_NAME = "'Hyperlink'";
	private INatspecTextResource resource;

	@Override
	public IHyperlink[] detectHyperlinks(ITextViewer textViewer,
			IRegion region, boolean canShowMultipleHyperlinks) {

		if (!NatSpecPlugin.hasValidLicense()) {
			// no license, no hyper links
			ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();
			errorMessageHelper.showErrorMessage(FEATURE_NAME);
			return null;
		}

		List<IHyperlink> links = new ArrayList<IHyperlink>();
		for (EObject root : resource.getContents()) {
			if (root instanceof Document) {
				Document document = (Document) root;
				handleDocument(document, region, links);
			}
		}
		return links.toArray(new IHyperlink[links.size()]);
	}

	private void handleDocument(Document document, IRegion region,
			List<IHyperlink> links) {
		GeneratedPythonFileFinder generatedPythonFileFinder = new GeneratedPythonFileFinder();
		IFile generatedFile = generatedPythonFileFinder
				.getGeneratedFileForSpecification(resource);

		if (!generatedFile.exists()) {
			return;
		}
		
		List<Sentence> sentences = document.getContents();
		INatspecLocationMap locationMap = resource.getLocationMap();

		for (Sentence sentence : sentences) {
			// ignore comments
			if (SentenceUtil.INSTANCE.isComment(sentence)) {
				continue;
			}
			int lineInSpec = locationMap.getLine(sentence);
			int offsetInSpec = locationMap.getCharStart(sentence);
			Position positionInSpec = new Position(lineInSpec - 1, offsetInSpec);
			Position positionInCode = generatedPythonFileFinder
					.findSentenceInCode(resource, generatedFile, positionInSpec);

			if (positionInCode.isValid()) {
				int lineInCode = positionInCode.getLine();
				createLink(sentence, region, generatedFile, lineInCode,
						links);
			}
		}
	}

	private void createLink(Sentence sentence, IRegion region,
			IFile generatedFile, int lineNumber, List<IHyperlink> links) {
		INatspecLocationMap locationMap = resource.getLocationMap();
		int offset = locationMap.getCharStart(sentence);
		int length = locationMap.getCharEnd(sentence) - offset + 1;

		IRegion regionOfSentence = new Region(offset, length);
		RegionOverlapHelper regionOverlapHelper = new RegionOverlapHelper();
		if (regionOverlapHelper.overlaps(region, regionOfSentence)) {
			// overlap
			links.add(new GeneratedFileLink(regionOfSentence, generatedFile,
					lineNumber));
		}
	}

	@Override
	public void setResource(INatspecTextResource textResource) {
		this.resource = textResource;
	}
}
