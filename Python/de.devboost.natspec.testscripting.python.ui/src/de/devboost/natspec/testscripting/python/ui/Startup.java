package de.devboost.natspec.testscripting.python.ui;

import org.eclipse.ui.IStartup;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.python.PythonCodeGeneratorFactory;
import de.devboost.natspec.testscripting.python.ui.actions.GoToTestSupportMethodAction;
import de.devboost.natspec.testscripting.ui.views.IPatternClickHandler;
import de.devboost.natspec.testscripting.ui.views.SyntaxPatternView;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		TestConnectorPlugin.getInstance();
		// This triggers the activation of this bundle, which causes some side
		// effects (e.g., registration of listeners).
		Activator.getDefault();
		
		registerBreakPointAdapter();
		registerSyntaxViewClickHandler();
	}

	private void registerSyntaxViewClickHandler() {
		SyntaxPatternView.CLICK_HANDLERS.add(new IPatternClickHandler() {
			
			@Override
			public boolean handle(ISyntaxPattern<?> pattern) {
				
				if (pattern instanceof DynamicSyntaxPattern) {
					DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
					if (!(dynamicSyntaxPattern.getCodeGeneratorFactory() instanceof PythonCodeGeneratorFactory)) {
						return false;
					}
					new GoToTestSupportMethodAction(dynamicSyntaxPattern).execute();
					return true;
				}
				
				// Can't handle this kind of pattern. Maybe someone else can.
				return false;
			}
		});
	}

	private void registerBreakPointAdapter() {
		
	}
}
