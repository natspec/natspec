name = NatSpec4Python
updatesite = de.devboost.natspec.python.updatesite
feature = de.devboost.natspec.python
associatesites = http://download.eclipse.org/releases/kepler,http://www.devboost.de/pydev-mirror/
eclipsemirror = 176.9.79.208

type/win64   = http://devboost.de/eclipse-mirror/downloads/drops4/R-4.3.2-201402211700/eclipse-SDK-4.3.2-win32-x86_64.zip
type/win32   = http://devboost.de/eclipse-mirror/downloads/drops4/R-4.3.2-201402211700/eclipse-SDK-4.3.2-win32.zip
type/osx     = http://devboost.de/eclipse-mirror/downloads/drops4/R-4.3.2-201402211700/eclipse-SDK-4.3.2-macosx-cocoa-x86_64.tar.gz
type/linux   = http://devboost.de/eclipse-mirror/downloads/drops4/R-4.3.2-201402211700/eclipse-SDK-4.3.2-linux-gtk.tar.gz
type/linux64 = http://devboost.de/eclipse-mirror/downloads/drops4/R-4.3.2-201402211700/eclipse-SDK-4.3.2-linux-gtk-x86_64.tar.gz

platform/plugin.xml/startupForegroundColor = 707173
platform/plugin.xml/startupMessageRect = 8,251,320,20
platform/plugin.xml/startupProgressRect = 7,276,438,10
