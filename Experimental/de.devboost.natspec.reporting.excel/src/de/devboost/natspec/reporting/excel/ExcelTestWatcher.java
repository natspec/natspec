package de.devboost.natspec.reporting.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.internal.AssumptionViolatedException;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

// FIXME this is just a quick demo hack. Refactor this class !!!
public class ExcelTestWatcher extends TestWatcher {

	private static final int TEST_NAME = 0;
	private static final int TEST_SUCCEEDED = 1;
	private static final int TEST_SKIPPED = 2;

	private static final int TEST_FAIL = 3;
	private static final int TEST_RUN = 4;
	private static final int LAST_RUN = 5;
	private static final int FAIL_RATE = 6;
	
	private AbstractTestWithExcelReporting test;

	public ExcelTestWatcher(File reportingFile) {

	}

	public ExcelTestWatcher(
			AbstractTestWithExcelReporting abstractTestWithExcelReporting) {
		this.test = abstractTestWithExcelReporting;
	}

	@Override
	protected void failed(Throwable e, Description description) {
		reportToFile(getTestIdentifier(description), 0, 0, 1);
	}

	@Override
	protected void skipped(AssumptionViolatedException e,
			Description description) {
		reportToFile(getTestIdentifier(description), 0, 1, 0);

	}

	@Override
	protected void succeeded(Description description) {
		reportToFile(getTestIdentifier(description), 1, 0, 0);
	}

	private void reportToFile(String testName, int sucessPlus, int skipPlus,
			int failPlus) {
		FileInputStream file;
		XSSFWorkbook workbook;
		try {
			File reportingFile = test.getReportingFile();
			if (reportingFile.exists()) {
				file = new FileInputStream(reportingFile);

				// Get the workbook instance for XLS file
				workbook = new XSSFWorkbook(file);

				file.close();
			} else {
				workbook = new XSSFWorkbook();
				XSSFSheet sheet = workbook.createSheet();
				XSSFRow headerRow = sheet.createRow(0);
				XSSFCell testNameHeader = headerRow.createCell(TEST_NAME);
				testNameHeader.setCellValue("Test Name");
				XSSFCell testRunsHeader = headerRow.createCell(TEST_RUN);
				testRunsHeader.setCellValue("Total Runs");
				XSSFCell testSuceedHeader = headerRow
						.createCell(TEST_SUCCEEDED);
				testSuceedHeader.setCellValue("Succees");
				XSSFCell testFailHeader = headerRow.createCell(TEST_FAIL);
				testFailHeader.setCellValue("Fail");
				XSSFCell testSkipHeader = headerRow.createCell(TEST_SKIPPED);
				testSkipHeader.setCellValue("Skip");
				XSSFCell testLastRunHeader = headerRow.createCell(LAST_RUN);
				testLastRunHeader.setCellValue("Last Run");
				XSSFCell testFailRate = headerRow.createCell(FAIL_RATE);
				testFailRate.setCellValue("Fail Rate");
			}

			XSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.rowIterator();
			Row testRow = null;
			while (rowIterator.hasNext()) {
				Row row = (Row) rowIterator.next();
				if (testName
						.equals(row.getCell(TEST_NAME).getStringCellValue())) {
					testRow = row;
					break;
				}
			}

			if (testRow == null) {
				testRow = sheet.createRow(sheet.getLastRowNum() + 1);
				testRow.createCell(TEST_NAME).setCellValue(testName);
				testRow.createCell(TEST_SUCCEEDED).setCellValue(0);
				testRow.createCell(TEST_FAIL).setCellValue(0);
				testRow.createCell(TEST_SKIPPED).setCellValue(0);
				testRow.createCell(TEST_RUN).setCellValue(0);
				testRow.createCell(TEST_RUN);
			}

			double currentSuccess = 0;
			double currentFail = 0;
			double currentTotal = 0;
			double currentSkip = 0;

			currentSuccess = testRow.getCell(TEST_SUCCEEDED)
					.getNumericCellValue();
			currentFail = testRow.getCell(TEST_FAIL).getNumericCellValue();
			currentTotal = testRow.getCell(TEST_RUN).getNumericCellValue();
			currentSkip = testRow.getCell(TEST_SKIPPED).getNumericCellValue();

			testRow.getCell(TEST_SUCCEEDED).setCellValue(
					currentSuccess + sucessPlus);
			testRow.getCell(TEST_FAIL).setCellValue(currentFail + failPlus);
			testRow.getCell(TEST_SKIPPED).setCellValue(currentSkip + skipPlus);
			testRow.getCell(TEST_RUN).setCellValue(currentTotal + 1);
			testRow.getCell(FAIL_RATE).setCellValue((currentFail + failPlus) / (currentTotal + 1));

			
			if (sucessPlus == 1) {
				testRow.getCell(LAST_RUN).setCellValue("SUCCESS");
			}
			if (failPlus == 1) {
				testRow.getCell(LAST_RUN).setCellValue("FAIL");
			}
			if (skipPlus == 1) {
				testRow.getCell(LAST_RUN).setCellValue("SKIP");
			}
			
			FileOutputStream out = new FileOutputStream(reportingFile);
			workbook.write(out);
			out.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String getTestIdentifier(Description description) {
		return description.getClassName();
	}
}