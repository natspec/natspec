package de.devboost.natspec.reporting.excel;

import java.io.File;

import org.junit.Rule;

public class AbstractTestWithExcelReporting {
	
	@Rule
	public ExcelTestWatcher excelTestWatcher = new ExcelTestWatcher(this);
	private File reportingFile;

	public AbstractTestWithExcelReporting(File reportingFile) {
		this.reportingFile = reportingFile;
	}
	
	public File getReportingFile() {
		return reportingFile;
	}
}
