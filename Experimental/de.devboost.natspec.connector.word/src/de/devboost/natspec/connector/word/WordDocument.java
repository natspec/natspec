package de.devboost.natspec.connector.word;

import java.util.Map;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import de.devboost.natspec.Document;
import de.devboost.natspec.Sentence;

/**
 * A {@link WordDocument} connects the NatSpec {@link Document} that was extracted from a Microsoft Word file with the
 * original contents of this file. This connection (mapping) is required to apply changes to the Word file after
 * evaluating the NatSpec sentences (e.g., to add information about the evaluation result to the Word document).
 */
public class WordDocument {

	/**
	 * The extracted NatSpec document.
	 */
	private final Document natSpecDocument;
	
	/**
	 * The name of the original Word file.
	 */
	private final String fileName;
	
	/**
	 * The object model extracted from the Word file.
	 */
	private final XWPFDocument xwpfDocument;
	
	/**
	 * A mapping from the extracted sentences to the runs where they were found. 
	 */
	private final Map<Sentence, XWPFRun> sentenceToRunMap;

	public WordDocument(Document natSpecDocument, String fileName, XWPFDocument xwpfDocument,
			Map<Sentence, XWPFRun> sentenceToRunMap) {
		
		this.natSpecDocument = natSpecDocument;
		this.fileName = fileName;
		this.xwpfDocument = xwpfDocument;
		this.sentenceToRunMap = sentenceToRunMap;
	}

	public Document getNatSpecDocument() {
		return natSpecDocument;
	}

	public String getFileName() {
		return fileName;
	}

	public XWPFDocument getXwpfDocument() {
		return xwpfDocument;
	}

	public Map<Sentence, XWPFRun> getSentenceToRunMap() {
		return sentenceToRunMap;
	}
}
