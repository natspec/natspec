package de.devboost.natspec.connector.word;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xwpf.usermodel.BodyElementType;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTRPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHighlightColor.Enum;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;

/**
 * The {@link AbstractNatSpecWordExtractor} can be used to extract sentences from MS Word documents. To use this class,
 * it must be sub classed.
 */
public abstract class AbstractNatSpecWordExtractor {

	/**
	 * Loads a MS Word document from the given stream and returns a high-level representation of the document that
	 * contains the extracted sentences.
	 * 
	 * @param inputStream
	 *            the stream to read from
	 * @param filename
	 *            the name of the original MS Word file
	 * @return the extracted sentences
	 * @throws IOException
	 *             if the stream cannot be read
	 * @throws OpenXML4JException
	 *             if the document is corrupt
	 */
	public WordDocument loadWordFile(InputStream inputStream, String filename) throws IOException, OpenXML4JException {

		Document natSpecDocument = NatspecFactory.eINSTANCE.createDocument();
		XWPFDocument xwpfDocument = new XWPFDocument(inputStream);

		Map<Sentence, XWPFRun> sentenceToRunMap = new LinkedHashMap<Sentence, XWPFRun>();

		List<IBodyElement> bodyElements = xwpfDocument.getBodyElements();

		for (IBodyElement bodyElement : bodyElements) {
			if (BodyElementType.PARAGRAPH.equals(bodyElement.getElementType())) {
				XWPFParagraph paragraph = (XWPFParagraph) bodyElement;
				List<XWPFRun> runs = paragraph.getRuns();
				StringBuilder text = new StringBuilder();
				int runCount = runs.size();
				XWPFRun xwpfRun = null;
				for (int i = 0; i < runCount; i++) {
					xwpfRun = runs.get(i);
					if (isSentence(xwpfRun)) {
						String textOfRun = xwpfRun.toString();
						text.append(textOfRun);
					} else {
						text = createNatSpecSentence(natSpecDocument, sentenceToRunMap, text, xwpfRun);
					}
				}
				createNatSpecSentence(natSpecDocument, sentenceToRunMap, text, xwpfRun);
			}
		}

		return new WordDocument(natSpecDocument, filename, xwpfDocument, sentenceToRunMap);
	}

	private StringBuilder createNatSpecSentence(Document natSpecDocument, Map<Sentence, XWPFRun> sentenceToRunMap,
			StringBuilder text, XWPFRun xwpfRun) {

		if (xwpfRun == null) {
			return text;
		}

		if (text.length() <= 0) {
			return text;
		}

		Sentence newSentence = NatspecFactory.eINSTANCE.createSentence();
		newSentence.setText(text.toString());
		natSpecDocument.getContents().add(newSentence);
		sentenceToRunMap.put(newSentence, xwpfRun);
		return new StringBuilder();
	}

	/**
	 * This is a template method that must be implemented by concrete sub classes to decide whether the given run shall
	 * be interpreted as an executable sentence.
	 * 
	 * @return <code>true</code> if the sentence shall be executed, otherwise <code>false</code>
	 */
	protected abstract boolean isSentence(XWPFRun xwpfRun);

	/**
	 * Returns <code>true</code> is the given run is highlighted with the given color.
	 * 
	 * @param xwpfRun
	 *            the run to check
	 * @param color
	 *            the expected color
	 * @return <code>true</code> is the run is highlighted with the color, otherwise <code>false</code>
	 */
	protected boolean hasHighlightColor(XWPFRun xwpfRun, Enum color) {
		CTRPr rpr = xwpfRun.getCTR().getRPr();
		if (rpr != null && rpr.getHighlight() != null) {
			Enum highlightColor = rpr.getHighlight().getVal();
			if (highlightColor.equals(color)) {
				return true;
			}
		}
		return false;
	}
}
