package de.devboost.natspec.reporting.excel.test;

import static org.junit.Assert.assertTrue;
import de.devboost.natspec.annotations.TextSyntax;

public class TestSupport {

	@TextSyntax("Do #1 fail")
	public void doSomeRandomFailingOrNot(boolean not) {
		assertTrue(not);
	}

}
