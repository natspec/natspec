package de.devboost.natspec.reporting.excel.test;

import java.io.File;

import org.junit.Test;

import de.devboost.natspec.reporting.excel.AbstractTestWithExcelReporting;

public class TestWatcherScenario3 extends AbstractTestWithExcelReporting {

	public TestWatcherScenario3() {
		super(new File("./testReport.xlsx"));
		// TODO Auto-generated constructor stub
	}

	TestSupport testSupport = new TestSupport();
	
	@Test
	public void executeScript() throws Exception {
		// The code in this method is generated from: /de.devboost.natspec.reporting.excel.test/src/de/devboost/natspec/reporting/excel/test/TestWatcherScenario3.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Do fail
		testSupport.doSomeRandomFailingOrNot(false);
		
	}

}