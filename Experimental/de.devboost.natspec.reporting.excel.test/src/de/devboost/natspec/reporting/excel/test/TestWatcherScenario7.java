package de.devboost.natspec.reporting.excel.test;

import java.io.File;

import org.junit.Test;

import de.devboost.natspec.reporting.excel.AbstractTestWithExcelReporting;

public class TestWatcherScenario7 extends AbstractTestWithExcelReporting {

	public TestWatcherScenario7() {
		super(new File("./testReport.xlsx"));
		// TODO Auto-generated constructor stub
	}

	TestSupport testSupport = new TestSupport();
	
	@Test
	public void executeScript() throws Exception {
		// The code in this method is generated from: /de.devboost.natspec.reporting.excel.test/src/de/devboost/natspec/reporting/excel/test/TestWatcherScenario7.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Do not fail
		testSupport.doSomeRandomFailingOrNot(true);
		
	}

}