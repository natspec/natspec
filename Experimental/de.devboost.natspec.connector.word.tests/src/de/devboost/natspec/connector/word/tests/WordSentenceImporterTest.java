package de.devboost.natspec.connector.word.tests;

import org.junit.Test;

import de.devboost.natspec.connector.word.tests.support.NatSpecWordConnectorTestSupport;

public class WordSentenceImporterTest {

	protected NatSpecWordConnectorTestSupport natSpecWordConnectorTestSupport = new NatSpecWordConnectorTestSupport(WordSentenceImporterTest.class);

	@Test
	@SuppressWarnings("unused")
	public void executeScript() throws Exception {
		// The code in this method is generated from: /de.devboost.natspec.connector.word.tests/src/de/devboost/natspec/connector/word/tests/WordSentenceImporterTest.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Load example word file example.docx
		de.devboost.natspec.connector.word.WordDocument wordDocument_example_docx = natSpecWordConnectorTestSupport.loadExampleWordFile("example.docx");
		// Assert that sentence Please extract this Hello World sentence was extracted
		natSpecWordConnectorTestSupport.assertThatSentence_WasExtracted(new java.lang.StringBuilder().append("Please").append(" ").append("extract").append(" ").append("this").append(" ").append("Hello").append(" ").append("World").append(" ").append("sentence").toString(), wordDocument_example_docx);
		// Assert that sentence Don't extract this sentence was not extracted
		natSpecWordConnectorTestSupport.assertThat_eWasNotExtracted(new java.lang.StringBuilder().append("Don't").append(" ").append("extract").append(" ").append("this").append(" ").append("sentence").toString(), wordDocument_example_docx);
		
		int suppressUnusedWaring;
	}

}