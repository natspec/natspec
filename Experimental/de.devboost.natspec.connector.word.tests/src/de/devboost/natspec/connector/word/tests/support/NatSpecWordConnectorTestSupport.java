package de.devboost.natspec.connector.word.tests.support;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STHighlightColor;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.annotations.Many;
import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.connector.word.AbstractNatSpecWordExtractor;
import de.devboost.natspec.connector.word.WordDocument;

public class NatSpecWordConnectorTestSupport {

	private final AbstractNatSpecWordExtractor natSpecWordExtractor = new AbstractNatSpecWordExtractor() {

		@Override
		protected boolean isSentence(XWPFRun xwpfRun) {
			return hasHighlightColor(xwpfRun, STHighlightColor.YELLOW);
		}
	};
	
	private final Class<?> contextClass;

	public NatSpecWordConnectorTestSupport(Class<?> contextClass) {
		this.contextClass = contextClass;
	}

	@TextSyntax("Load example word file #1")
	public WordDocument loadExampleWordFile(String exampledoc) throws IOException, OpenXML4JException {
		InputStream resource = contextClass.getResourceAsStream(exampledoc);
		assertNotNull("Could not load test ressource", resource);
		WordDocument document = natSpecWordExtractor.loadWordFile(resource, exampledoc);
		return document;
	}

	@TextSyntax("Assert that sentence #1 was extracted")
	public void assertThatSentence_WasExtracted(@Many String expectedSentence, WordDocument document) {
		assertNotNull("Document not extracted.", document);
		List<Sentence> contents = document.getNatSpecDocument().getContents();
		for (Sentence sentence : contents) {
			if (sentence.getText().equals(expectedSentence)) {
				return;
			}
		}
		fail("Expected sentence not found: " + expectedSentence);
	}

	@TextSyntax("Assert that sentence #1 was not extracted")
	public void assertThat_eWasNotExtracted(@Many String expectedSentence, WordDocument document) {
		List<Sentence> contents = document.getNatSpecDocument().getContents();
		for (Sentence sentence : contents) {
			if (sentence.getText().equals(expectedSentence)) {
				fail("Not expected sentence found: " + expectedSentence);
			}
		}
	}
}
