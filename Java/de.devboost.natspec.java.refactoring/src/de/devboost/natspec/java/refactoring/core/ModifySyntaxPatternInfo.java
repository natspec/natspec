package de.devboost.natspec.java.refactoring.core;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jdt.core.dom.StringLiteral;

import de.devboost.natspec.testscripting.java.jdt.JDTJavaModelExtractor;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.patterns.IParameter;

public class ModifySyntaxPatternInfo {

	private int offset;
	private String newPattern;
	private String oldPattern;
	private IFile sourceFile;
	private Annotation selectedAnnotations;
	private IType[] types;
	private IMethod method;
	private int selectionStart;
	private int selectionLength;

	public int getOffset() {
		return offset;
	}

	public void setOffset(final int offset) {
		this.offset = offset;
	}

	public String getNewPattern() {
		return newPattern;
	}

	public void setNewPattern(final String newPattern) {
		this.newPattern = newPattern;
	}

	public String getOldPattern() {
		return oldPattern;
	}

	public void setOldPattern(final String oldPattern) {
		this.oldPattern = oldPattern;
	}

	public IFile getSourceFile() {
		return sourceFile;
	}

	public void setSourceFile(final IFile sourceFile) {
		this.sourceFile = sourceFile;
	}

	public void setSelectedAnnotation(Annotation selectedAnnotations) {
		this.selectedAnnotations = selectedAnnotations;
	}

	public Annotation getSelectedAnnotation() {
		return selectedAnnotations;
	}

	public String getSyntaxPattern() {
		StringLiteral astNode = getSyntaxPatternStringLiteralAstNode();
		if (astNode == null) {
			return null;
		}

		return astNode.getLiteralValue();
	}

	public int getSyntaxPatternOffset() {
		StringLiteral astNode = getSyntaxPatternStringLiteralAstNode();
		if (astNode == null) {
			return -1;
		}

		return astNode.getStartPosition();
	}

	public StringLiteral getSyntaxPatternStringLiteralAstNode() {
		Annotation annotation = getSelectedAnnotation();
		if (annotation instanceof SingleMemberAnnotation) {
			SingleMemberAnnotation singleMemberAnnotation = (SingleMemberAnnotation) annotation;
			Expression expression = singleMemberAnnotation.getValue();
			if (expression instanceof StringLiteral) {
				StringLiteral stringLiteral = (StringLiteral) expression;
				return stringLiteral;
			}
		}
		return null;
	}

	public boolean isPattern(String typeName, String methodName, List<? extends IParameter> parameters) {
		Annotation annotation = getSelectedAnnotation();
		if (annotation == null) {
			return false;
		}

		MethodDeclaration method = findContainingMethod(annotation);
		if (isMethod(method, methodName) && isType(typeName) && parametersMatch(method, parameters)) {
			return true;
		}

		return false;
	}

	private boolean parametersMatch(MethodDeclaration methodDeclaration, List<? extends IParameter> otherParameters) {
		IResource resource = null;
		List<JavaParameter> parameters = new JDTJavaModelExtractor().convertParameters(resource, methodDeclaration);
		if (parameters.size() != otherParameters.size()) {
			return false;
		}
		for (int i = 0; i < otherParameters.size(); i++) {
			IParameter parameter = parameters.get(i);
			IParameter otherParameter = otherParameters.get(i);
			String type = parameter.getType().getQualifiedNameWithTypeArguments();
			String otherType = otherParameter.getType().getQualifiedNameWithTypeArguments();
			if (!type.equals(otherType)) {
				return false;
			}
		}

		return true;
	}

	private boolean isType(String typeName) {
		if (types == null) {
			return false;
		}
		for (IType type : types) {
			String fullyQualifiedName = type.getFullyQualifiedName();
			if (typeName.equals(fullyQualifiedName)) {
				return true;
			}
		}
		return false;
	}

	private boolean isMethod(MethodDeclaration methodDeclaration, String methodName) {
		if (methodDeclaration == null) {
			return false;
		}

		if (methodName.equals(methodDeclaration.getName().toString())) {
			return true;
		}

		return false;
	}

	private MethodDeclaration findContainingMethod(ASTNode node) {
		if (node == null) {
			return null;
		}
		if (node instanceof MethodDeclaration) {
			MethodDeclaration methodDeclaration = (MethodDeclaration) node;
			return methodDeclaration;
		}

		return findContainingMethod(node.getParent());
	}

	public void setSelectedTypes(IType[] types) {
		this.types = types;
	}

	public void setSelectedMethod(IMethod method) {
		this.method = method;
	}

	public IMethod getSelectedMethod() {
		return method;
	}

	public void setSelectionStart(int selectionStart) {
		this.selectionStart = selectionStart;
	}

	public void setSelectionLength(int selectionLength) {
		this.selectionLength = selectionLength;
	}

	public int getSelectionStart() {
		return selectionStart;
	}

	public int getSelectionLength() {
		return selectionLength;
	}
}
