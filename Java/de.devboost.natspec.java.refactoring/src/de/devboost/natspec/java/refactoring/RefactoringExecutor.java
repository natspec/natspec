package de.devboost.natspec.java.refactoring;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Joiner;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.patterns.IParameterIndexProvider;

public class RefactoringExecutor {

	/**
	 * Applies a refactoring to the given match that modifies the sentence that was matched such that it conforms to the
	 * given target syntax pattern. The target pattern can be given in the same syntax that is used to specify syntax 
	 * patterns in the @TextSyntax annotation. The new sentence is returned.
	 */
	public String applyRefactoring(String targetPattern, ISyntaxPatternMatch<? extends Object> match) {

		ISyntaxPattern<? extends Object> pattern = match.getPattern();
		String resultSentence = targetPattern;
		if (pattern instanceof IParameterIndexProvider) {
			IParameterIndexProvider dynamicSyntaxPattern = (IParameterIndexProvider) pattern;
			Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = dynamicSyntaxPattern
					.getPartToParameterIndexMap();
			Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
			Set<ISyntaxPatternPart> syntaxPatternParts = partsToMatchesMap.keySet();
			for (ISyntaxPatternPart syntaxPatternPart : syntaxPatternParts) {
				ISyntaxPatternPartMatch syntaxPatternPartMatch = partsToMatchesMap.get(syntaxPatternPart);
				Integer index = partToParameterIndexMap.get(syntaxPatternPart);

				// not a valid syntax parameter
				if (index == null) {
					continue;
				}
				
				List<Word> matchedWords = syntaxPatternPartMatch.getMatchedWords();
				String matchedSentenceFragment = concat(matchedWords);
				resultSentence = resultSentence.replaceFirst("#" + (index + 1), matchedSentenceFragment);
			}
		}

		return resultSentence;
	}

	private String concat(List<Word> words) {
		List<String> wordsAsStrings = new ArrayList<String>();
		for (Word word : words) {
			String text = word.getText();
			wordsAsStrings.add(text);
		}
		
		String result = Joiner.on(" ").join(wordsAsStrings);
		return result;
	}
}
