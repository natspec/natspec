package de.devboost.natspec.java.refactoring.ui.wizards;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.ltk.ui.refactoring.UserInputWizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import de.devboost.natspec.java.refactoring.Activator;
import de.devboost.natspec.java.refactoring.core.ModifySyntaxPatternInfo;

public class ModifySyntaxPatternInputPage extends UserInputWizardPage {

	private static final String DS_KEY = ModifySyntaxPatternInputPage.class.getName();

	private final ModifySyntaxPatternInfo info;

	private IDialogSettings dialogSettings;
	private Text newPatternTextField;

	public ModifySyntaxPatternInputPage(final ModifySyntaxPatternInfo info) {
		super(ModifySyntaxPatternInputPage.class.getName());
		this.info = info;
		initDialogSettings();
	}

	// interface methods of UserInputWizardPage
	// /////////////////////////////////////////

	public void createControl(final Composite parent) {
		Composite composite = createRootComposite(parent);
		setControl(composite);

		createNewPatternTextField(composite);
		validate();
	}

	private Composite createRootComposite(final Composite parent) {
		Composite result = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout(2, false);
		gridLayout.marginWidth = 10;
		gridLayout.marginHeight = 10;
		result.setLayout(gridLayout);
		initializeDialogUnits(result);
		Dialog.applyDialogFont(result);
		return result;
	}

	private void createNewPatternTextField(Composite composite) {
		String text = info.getSyntaxPattern();
		int selectionStart = info.getSelectionStart() - 1;
		if (selectionStart < 0) {
			selectionStart = 0;
		}
		int selectionEnd = selectionStart + info.getSelectionLength();
		if (selectionEnd > text.length()) {
			selectionEnd = text.length();
		}

		newPatternTextField = new Text(composite, SWT.BORDER);
		newPatternTextField.setText(text);
		newPatternTextField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		newPatternTextField.setSelection(selectionStart, selectionEnd);
		newPatternTextField.addKeyListener(new KeyAdapter() {
			
			public void keyReleased(final KeyEvent e) {
				info.setNewPattern(newPatternTextField.getText());
				validate();
			}
		});
	}

	private void initDialogSettings() {
		IDialogSettings ds = Activator.getDefault().getDialogSettings();
		dialogSettings = ds.getSection(DS_KEY);
		if (dialogSettings == null) {
			dialogSettings = ds.addNewSection(DS_KEY);
		}
	}

	private void validate() {
		String text = newPatternTextField.getText();
		setPageComplete(text.length() > 0 && !text.equals(info.getOldPattern()));
	}
}
