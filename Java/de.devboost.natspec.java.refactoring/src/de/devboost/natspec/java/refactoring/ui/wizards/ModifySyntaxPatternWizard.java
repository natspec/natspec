package de.devboost.natspec.java.refactoring.ui.wizards;

import org.eclipse.ltk.ui.refactoring.RefactoringWizard;

import de.devboost.natspec.java.refactoring.core.ModifySyntaxPatternInfo;
import de.devboost.natspec.java.refactoring.core.ModifySyntaxPatternRefactoring;

public class ModifySyntaxPatternWizard extends RefactoringWizard {

	private final ModifySyntaxPatternInfo info;

	public ModifySyntaxPatternWizard(ModifySyntaxPatternRefactoring refactoring, ModifySyntaxPatternInfo info) {
		super(refactoring, DIALOG_BASED_USER_INTERFACE);
		this.info = info;
	}

	@Override
	protected void addUserInputPages() {
		setDefaultPageTitle(getRefactoring().getName());
		addPage(new ModifySyntaxPatternInputPage(info));
	}
}
