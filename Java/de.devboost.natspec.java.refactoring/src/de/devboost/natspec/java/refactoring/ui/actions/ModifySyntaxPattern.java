package de.devboost.natspec.java.refactoring.ui.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.ui.refactoring.RefactoringWizardOpenOperation;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.texteditor.ITextEditor;

import de.devboost.natspec.java.refactoring.Activator;
import de.devboost.natspec.java.refactoring.core.ModifySyntaxPatternDelegate;
import de.devboost.natspec.java.refactoring.core.ModifySyntaxPatternInfo;
import de.devboost.natspec.java.refactoring.core.ModifySyntaxPatternProcessor;
import de.devboost.natspec.java.refactoring.core.ModifySyntaxPatternRefactoring;
import de.devboost.natspec.java.refactoring.ui.wizards.ModifySyntaxPatternWizard;
import de.devboost.natspec.testscripting.java.jdt.ConcreteJDTUtility;
import de.devboost.natspec.testscripting.java.jdt.ParserHelper;

public class ModifySyntaxPattern implements IEditorActionDelegate {

	private final static String MSG_REFACTORING_REFUSED_TITLE = "Refactoring not applicable";
	private final static String MSG_REFACTORING_REFUSED = "Modify Syntax Pattern Refactoring cannot be applied.";

	private ISelection selection;
	private IEditorPart targetEditor;
	private boolean onJavaFile;

	private ModifySyntaxPatternInfo info = new ModifySyntaxPatternInfo();

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		this.targetEditor = targetEditor;
		this.onJavaFile = false;
		
		IFile file = getFile();
		if (file == null) {
			return;
		}
		
		String fileExtension = file.getFileExtension();
		if (ModifySyntaxPatternDelegate.EXT_JAVA.equals(fileExtension)) {
			this.onJavaFile = true;
		}
	}

	public void run(final IAction action) {
		if (!onJavaFile) {
			refuse();
		} else {
			if (selection != null) {
				if (selection instanceof ITextSelection) {
					applySelection((ITextSelection) selection);
					if (saveAll()) {
						openWizard();
					}
				} else {
					Activator.logInfo("Selection has unknown type: " + selection.getClass().getName() + ". Can't start refactoring.", null);
				}
			} else {
				Activator.logInfo("No selection found. Can't start refactoring.", null);
			}
		}
	}

	public void selectionChanged(final IAction action, final ISelection selection) {
		this.selection = selection;
	}

	private void applySelection(final ITextSelection textSelection) {
		info = new ModifySyntaxPatternInfo();
		
		IFile file = getFile();
		if (file == null) {
			return;
		}
		
		int position = textSelection.getOffset();
		int length = textSelection.getLength();
		Activator.logInfo("Selection starts at " + position + "(lenght="+ length + ").", null);

		ICompilationUnit compilationUnit = ConcreteJDTUtility.INSTANCE.getCompilationUnit(file);
		if (compilationUnit == null) {
			return;
		}
		
		CompilationUnit parsedUnit = new ParserHelper().parse(compilationUnit);
		IType[] types = null;
		IMethod method = null;
		try {
			types = compilationUnit.getTypes();
			IJavaElement elementAtPosition = compilationUnit.getElementAt(position);
			if (elementAtPosition instanceof IMethod) {
				method = (IMethod) elementAtPosition;
			}
		} catch (JavaModelException e) {
			// Ignore
		}

		Annotation selectedAnnotation = findSelectedAnnotation(parsedUnit, position, length);
		info.setSelectedAnnotation(selectedAnnotation);
		int syntaxPatternOffset = info.getSyntaxPatternOffset();
		int selectionStart = position - syntaxPatternOffset;
		info.setSelectionStart(selectionStart);
		info.setSelectionLength(length);
		info.setSelectedTypes(types);
		info.setSelectedMethod(method);
		
		info.setOldPattern(info.getSyntaxPattern());
		info.setNewPattern(info.getSyntaxPattern());
		info.setOffset(textSelection.getOffset());
		info.setSourceFile(getFile());
	}

	private Annotation findSelectedAnnotation(ASTNode node, final int position, final int length) {
		final List<Annotation> annotations = new ArrayList<Annotation>();
		
		ASTVisitor visitor = new ASTVisitor() {
			
			@Override
			public boolean visit(SingleMemberAnnotation node) {
				if (covers(node, position, length)) {
					annotations.add(node);
				}
				return super.visit(node);
			}
		};
		node.accept(visitor);
		
		if (annotations.isEmpty()) {
			return null;
		}
		
		Iterator<Annotation> iterator = annotations.iterator();
		while (iterator.hasNext()) {
			Annotation annotation = iterator.next();
			boolean coversAnotherAnnotation = false;
			for (Annotation otherAnnotation : annotations) {
				if (annotation == otherAnnotation) {
					continue;
				}
				
				if (covers(annotation, otherAnnotation.getStartPosition(), otherAnnotation.getLength())) {
					coversAnotherAnnotation = true;
					break;
				}
			}
			if (coversAnotherAnnotation) {
				iterator.remove();
			}
		}

		if (annotations.isEmpty()) {
			return null;
		}

		return annotations.get(0);
	}

	private boolean covers(ASTNode node, int position, int length) {
		int nodePosition = node.getStartPosition();
		if (nodePosition <= position) {
			int nodeLength = node.getLength();
			int nodeEnd = nodePosition + nodeLength;
			int end = position + length;
			if (nodeEnd >= end) {
				// node covers position + length
				return true;
			}
		}

		return false;
	}

	private void refuse() {
		String title = MSG_REFACTORING_REFUSED_TITLE;
		String message = MSG_REFACTORING_REFUSED;
		MessageDialog.openInformation(getShell(), title, message);
	}

	private static boolean saveAll() {
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		return IDE.saveAllEditors(new IResource[] { workspaceRoot }, false);
	}

	private void openWizard() {
		RefactoringProcessor processor = new ModifySyntaxPatternProcessor(info);
		ModifySyntaxPatternRefactoring ref = new ModifySyntaxPatternRefactoring(processor);
		ModifySyntaxPatternWizard wizard = new ModifySyntaxPatternWizard(ref, info);
		RefactoringWizardOpenOperation op = new RefactoringWizardOpenOperation(wizard);
		try {
			String titleForFailedChecks = MSG_REFACTORING_REFUSED_TITLE;
			op.run(getShell(), titleForFailedChecks);
		} catch (final InterruptedException irex) {
			// operation was cancelled
		}
	}

	private Shell getShell() {
		Shell result = null;
		if (targetEditor != null) {
			result = targetEditor.getSite().getShell();
		} else {
			result = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		}
		return result;
	}

	private IFile getFile() {
		IFile result = null;
		if (targetEditor instanceof ITextEditor) {
			ITextEditor editor = (ITextEditor) targetEditor;
			IEditorInput editorInput = editor.getEditorInput();
			if (editorInput instanceof IFileEditorInput) {
				IFileEditorInput fileEditorInput = (IFileEditorInput) editorInput;
				result = fileEditorInput.getFile();
			}
		}
		return result;
	}
}
