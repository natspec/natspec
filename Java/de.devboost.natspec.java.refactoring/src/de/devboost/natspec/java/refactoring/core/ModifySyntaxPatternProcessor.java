package de.devboost.natspec.java.refactoring.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.RefactoringParticipant;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.ltk.core.refactoring.participants.SharableParticipants;

public class ModifySyntaxPatternProcessor extends RefactoringProcessor {

	private static final String PROCESSOR_NAME = "Modify Syntax Pattern";

	private final ModifySyntaxPatternInfo info;
	private final ModifySyntaxPatternDelegate delegate;

	public ModifySyntaxPatternProcessor(ModifySyntaxPatternInfo info) {
		this.info = info;
		this.delegate = new ModifySyntaxPatternDelegate(info);
	}

	public Object[] getElements() {
		// usually, this would be some element object in the object model on which
		// we work (e.g. a Java element if we were in the Java Model); in this case
		// we have only the property name
		return new Object[] { info.getOldPattern() };
	}

	public String getIdentifier() {
		return getClass().getName();
	}

	public String getProcessorName() {
		return PROCESSOR_NAME;
	}

	public boolean isApplicable() throws CoreException {
		return true;
	}

	public RefactoringStatus checkInitialConditions(IProgressMonitor pm) {
		return delegate.checkInitialConditions();
	}

	public RefactoringStatus checkFinalConditions(IProgressMonitor pm, CheckConditionsContext context) {
		return delegate.checkFinalConditions(pm, context);
	}

	public Change createChange(IProgressMonitor pm) {
		CompositeChange result = new CompositeChange(getProcessorName());
		delegate.createChange(pm, result);
		return result;
	}

	public RefactoringParticipant[] loadParticipants(final RefactoringStatus status,
			final SharableParticipants sharedParticipants) {
		// This would be the place to load the participants via the
		// ParticipantManager and decide which of them are allowed to participate.
		return new RefactoringParticipant[0];
	}
}
