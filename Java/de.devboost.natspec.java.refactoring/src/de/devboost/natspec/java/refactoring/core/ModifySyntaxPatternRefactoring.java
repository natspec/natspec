package de.devboost.natspec.java.refactoring.core;

import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;

public class ModifySyntaxPatternRefactoring extends ProcessorBasedRefactoring {

	private final RefactoringProcessor processor;

	public ModifySyntaxPatternRefactoring(RefactoringProcessor processor) {
		super(processor);
		this.processor = processor;
	}

	public RefactoringProcessor getProcessor() {
		return processor;
	}
}
