package de.devboost.natspec.java.refactoring.core;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.TextSyntax;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.ltk.core.refactoring.Change;
import org.eclipse.ltk.core.refactoring.CompositeChange;
import org.eclipse.ltk.core.refactoring.RefactoringStatus;
import org.eclipse.ltk.core.refactoring.TextFileChange;
import org.eclipse.ltk.core.refactoring.participants.CheckConditionsContext;
import org.eclipse.ltk.core.refactoring.participants.IConditionChecker;
import org.eclipse.ltk.core.refactoring.participants.ValidateEditChecker;
import org.eclipse.text.edits.MultiTextEdit;
import org.eclipse.text.edits.ReplaceEdit;

import de.devboost.natspec.Document;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.java.refactoring.RefactoringExecutor;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.resource.natspec.INatspecLocationMap;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.jdt.ConcreteJDTUtility;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.IParameter;

public class ModifySyntaxPatternDelegate {

	public static final String EXT_JAVA = "java"; //$NON-NLS-1$

	private static final String EXT_NATSPEC = "natspec";

	private static final String MSG_SELECT_TEXT_SYNTAX_ANNOTATION_TO_REFACTOR = "Please select a @TextSyntax annotation to refactor a syntax pattern.";
	private static final String MSG_NO_FILE_SELECTED = "No file selected for refactoring.";
	private static final String MSG_FILE_IS_READONLY = "File is read-only. Can't refactor.";
	private static final String MSG_COLLECTING_CHANGES = "Collecting changes";
	private static final String MSG_CHECKING = "Checking";

	private final ModifySyntaxPatternInfo info;
	private final List<IFile> natSpecFiles = new ArrayList<IFile>();

	public ModifySyntaxPatternDelegate(ModifySyntaxPatternInfo info) {
		this.info = info;
	}

	public RefactoringStatus checkInitialConditions() {
		RefactoringStatus result = new RefactoringStatus();
		IFile sourceFile = info.getSourceFile();
		if (sourceFile == null || !sourceFile.exists()) {
			result.addFatalError(MSG_NO_FILE_SELECTED);
		} else if (info.getSourceFile().isReadOnly()) {
			result.addFatalError(MSG_FILE_IS_READONLY);
		} else if (info.getSelectedAnnotation() == null) {
			result.addFatalError(MSG_SELECT_TEXT_SYNTAX_ANNOTATION_TO_REFACTOR);
			// Check type of selected annotation
		} else if (!isTextSyntax(info.getSelectedAnnotation())) {
			result.addFatalError(MSG_SELECT_TEXT_SYNTAX_ANNOTATION_TO_REFACTOR);
		} else if (info.getSelectedMethod() == null) {
			result.addFatalError(MSG_SELECT_TEXT_SYNTAX_ANNOTATION_TO_REFACTOR);
		}
		return result;
	}

	private boolean isTextSyntax(Annotation annotation) {
		String name = annotation.getTypeName().toString();
		return TextSyntax.class.getSimpleName().equals(name);
	}

	public RefactoringStatus checkFinalConditions(final IProgressMonitor pm, final CheckConditionsContext ctxt) {
		RefactoringStatus result = new RefactoringStatus();
		pm.beginTask(MSG_CHECKING, 100);
		// do something long-running here: traverse the entire workspace to look for all *.natspec files
		IContainer rootContainer = ResourcesPlugin.getWorkspace().getRoot();
		search(rootContainer, result);
		pm.worked(50);

		if (ctxt != null) {
			IFile[] files = new IFile[natSpecFiles.size()];
			natSpecFiles.toArray(files);
			IConditionChecker checker = ctxt.getChecker(ValidateEditChecker.class);
			ValidateEditChecker editChecker = (ValidateEditChecker) checker;
			editChecker.addFiles(files);
		}

		pm.done();
		return result;
	}

	public void createChange(IProgressMonitor pm, CompositeChange rootChange) {
		try {
			pm.beginTask(MSG_COLLECTING_CHANGES, 100);
			rootChange.addAll(createModifyNatSpecFilesChange());
			pm.worked(90);
			rootChange.add(createModifyAnnotationChange());
			pm.worked(10);
		} finally {
			pm.done();
		}
	}

	private Change createModifyAnnotationChange() {
		IFile file = info.getSourceFile();

		TextFileChange result = new TextFileChange(file.getName(), file);
		result.setSaveMode(TextFileChange.FORCE_SAVE);
		
		// a file change contains a tree of edits, first add the root of them
		MultiTextEdit fileChangeRootEdit = new MultiTextEdit();
		result.setEdit(fileChangeRootEdit);

		StringLiteral astNode = info.getSyntaxPatternStringLiteralAstNode();
		// edit object for the text replacement in the file, this is the only child
		ReplaceEdit edit = new ReplaceEdit(astNode.getStartPosition() + 1, astNode.getLength() - 2,
				info.getNewPattern());
		fileChangeRootEdit.addChild(edit);
		return result;
	}

	private Change[] createModifyNatSpecFilesChange() {
		RefactoringExecutor refactoringExecutor = new RefactoringExecutor();

		List<Change> result = new ArrayList<Change>();

		for (IFile natSpecFile : natSpecFiles) {
			String targetPattern = info.getNewPattern();
			boolean useOptimizedMatcher = NatSpecPlugin.USE_TREE_BASED_MATCHER;
			boolean usePrioritizer = NatSpecPlugin.USE_PRIORITIZER;

			Resource resource = new NatspecEclipseProxy().getResource(natSpecFile);
			IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(resource
					.getURI());

			TextFileChange tfc = new TextFileChange(natSpecFile.getName(), natSpecFile);
			tfc.setSaveMode(TextFileChange.FORCE_SAVE);
			
			MultiTextEdit fileChangeRootEdit = new MultiTextEdit();
			tfc.setEdit(fileChangeRootEdit);

			MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);
			List<EObject> contents = resource.getContents();
			for (EObject content : contents) {
				if (content instanceof Document) {
					Document document = (Document) content;
					List<Sentence> sentences = document.getContents();
					for (Sentence sentence : sentences) {
						List<ISyntaxPatternMatch<? extends Object>> matches = matchService.match(sentence, context);
						for (ISyntaxPatternMatch<? extends Object> match : matches) {
							ISyntaxPattern<?> pattern = match.getPattern();
							if (pattern instanceof DynamicSyntaxPattern<?>) {
								DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
								String typeName = dynamicSyntaxPattern.getQualifiedTypeName();
								String methodName = dynamicSyntaxPattern.getMethodName();
								List<? extends IParameter> parameters = dynamicSyntaxPattern.getParameters();
								if (info.isPattern(typeName, methodName, parameters)) {
									String newText = refactoringExecutor.applyRefactoring(targetPattern, match);

									INatspecLocationMap locationMap = getLocationMap(sentence);
									int offset = locationMap.getCharStart(sentence);
									int length = locationMap.getCharEnd(sentence) - offset + 1;
									if (offset < 0) {
										continue;
									}
									
									ReplaceEdit edit = new ReplaceEdit(offset, length, newText);
									fileChangeRootEdit.addChild(edit);
								}
							}
						}
					}
				}
			}

			if (fileChangeRootEdit.getChildrenSize() > 0) {
				result.add(tfc);
			}
		}

		return (Change[]) result.toArray(new Change[result.size()]);
	}

	private INatspecLocationMap getLocationMap(Sentence sentence) {
		Resource resource = sentence.eResource();
		if (resource instanceof NatspecResource) {
			NatspecResource natspecResource = (NatspecResource) resource;
			INatspecLocationMap locationMap = natspecResource.getLocationMap();
			return locationMap;
		}

		return null;
	}

	private boolean isToRefactor(final IFile file) {
		return EXT_NATSPEC.equals(file.getFileExtension());
	}

	private void search(final IContainer rootContainer, final RefactoringStatus status) {
		try {
			if (rootContainer instanceof IProject) {
				IProject project = (IProject) rootContainer;
				if (!project.isOpen()) {
					return;
				}
			}
			IResource[] members = rootContainer.members();
			for (int i = 0; i < members.length; i++) {
				IResource nextMember = members[i];
				if (nextMember instanceof IContainer) {
					IPath fullPath = nextMember.getFullPath();
					IProject project = nextMember.getProject();
					// Exclude bin folders based on the Java project configuration (output folder)
					boolean isOutputFolder = ConcreteJDTUtility.INSTANCE.isOutputFolder(project, fullPath);
					if (isOutputFolder) {
						continue;
					}
					search((IContainer) nextMember, status);
				} else {
					IFile file = (IFile) nextMember;
					handleFile(file, status);
				}
			}
		} catch (final CoreException cex) {
			status.addFatalError(cex.getMessage());
		}
	}

	private void handleFile(final IFile file, final RefactoringStatus status) {
		if (isToRefactor(file)) {
			natSpecFiles.add(file);
		}
	}
}
