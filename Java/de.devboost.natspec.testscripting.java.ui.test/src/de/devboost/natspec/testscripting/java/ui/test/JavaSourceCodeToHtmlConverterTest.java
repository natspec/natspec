package de.devboost.natspec.testscripting.java.ui.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.devboost.natspec.testscripting.java.ui.hover.JavaSourceCodeToHtmlConverter;

public class JavaSourceCodeToHtmlConverterTest {

	@Test
	public void testMethodToHtml() {
		assertHtml("@Annotation\n\tpublic void m() {\n\t}", "<pre>@Annotation<br/><b>public</b> <b>void</b> m() {<br/>}<br/></pre>");
	}

	@Test
	public void testMethodWithEmptyLineToHtml() {
		assertHtml("@Annotation\n\tpublic void m() {\n\n\t}", "<pre>@Annotation<br/><b>public</b> <b>void</b> m() {<br/><br/>}<br/></pre>");
	}

	private void assertHtml(String input, String expectedOutput) {
		String actualOutput = JavaSourceCodeToHtmlConverter.INSTANCE.toHtml(input);
		assertEquals("Wrong HTML output", expectedOutput, actualOutput);
	}
}
