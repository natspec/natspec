package de.devboost.natspec.testscripting.java.ui.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.core.IJavaProject;
import org.junit.Test;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.resource.natspec.INatspecQuickFix;
import de.devboost.natspec.testscripting.java.jdt.test.JavaProjectMock;
import de.devboost.natspec.testscripting.java.jdt.test.TypeMock;
import de.devboost.natspec.testscripting.java.ui.quickfixes.TestSupportQuickAssistProcessor;

public class QuickFixTest {

	private class AccessibleTestSupportQuickFixProcessor extends TestSupportQuickAssistProcessor {

		public List<INatspecQuickFix> getQuickFixes(Collection<String> allFieldTypes, Set<Sentence> sentencesToFix,
				IJavaProject javaProject, URI uri) {

			return super.getQuickFixes(allFieldTypes, sentencesToFix, javaProject, uri);
		}
	}

	@Test
	public void testNoQuickFixForBinaryType() {
		TypeMock type = createType("TestType", true);
		assertQuickFixes(0, type);
	}

	@Test
	public void testQuickFixForSourceType() {
		TypeMock type = createType("TestType", false);
		assertQuickFixes(1, type);
	}

	@Test
	public void testNoDuplicateQuickFixes() {
		TypeMock type1 = createType("TestType", false);
		TypeMock type2 = createType("TestType", false);
		assertQuickFixes(1, type1, type2);
	}

	private void assertQuickFixes(int expectedFixes, TypeMock... types) {
		AccessibleTestSupportQuickFixProcessor processor = new AccessibleTestSupportQuickFixProcessor();
		Collection<String> allFieldTypes = new ArrayList<String>();
		JavaProjectMock javaProject = new JavaProjectMock();
		for (TypeMock typeMock : types) {
			String typeName = typeMock.getFullyQualifiedName();
			allFieldTypes.add(typeName);
			javaProject.addType(typeName, typeMock);
		}
		
		Set<Sentence> sentencesToFix = Collections.singleton(NatspecFactory.eINSTANCE.createSentence());
		
		URI uri = null;
		List<INatspecQuickFix> quickFixes = processor.getQuickFixes(allFieldTypes, sentencesToFix, javaProject, uri);
		assertEquals("Wrong number of quick fixes.", expectedFixes, quickFixes.size());
	}

	private TypeMock createType(String typeName, boolean useBinaryType) {
		TypeMock typeMock = new TypeMock(typeName);
		typeMock.setBinary(useBinaryType);
		typeMock.setPath(new Path("path/to/" + typeName));
		return typeMock;
	}
}
