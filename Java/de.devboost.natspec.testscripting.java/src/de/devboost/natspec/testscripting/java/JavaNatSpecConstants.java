package de.devboost.natspec.testscripting.java;

public interface JavaNatSpecConstants {

	public static final String JAVA_EXTENSION = "java";

	public static final String TEST_CASE_TEMPLATE_FILE_NAME = "_NatSpecTemplate.java";
}
