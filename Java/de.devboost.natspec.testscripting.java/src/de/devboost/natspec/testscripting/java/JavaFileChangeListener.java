package de.devboost.natspec.testscripting.java;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.model.IBreakpoint;

import de.devboost.natspec.testscripting.builder.AbstractResourceChangeListener;
import de.devboost.natspec.testscripting.debug.IDebugConstants;

public class JavaFileChangeListener extends AbstractResourceChangeListener {

	@Override
	protected void handleMarkerDeltas(IMarkerDelta[] markerDeltas) {
		for (IMarkerDelta markerDelta : markerDeltas) {
			IResource resource = markerDelta.getResource();
			if (resource instanceof IFile) {
				IFile file = (IFile) resource;
				if ("java".equals(file.getFileExtension())) {
					if (IResourceDelta.REMOVED == markerDelta.getKind()) {
						Map<String, Object> attributes = markerDelta.getAttributes();
						Object id = attributes.get(IBreakpoint.ID);
						// TODO Find constant for this
						if ("org.eclipse.jdt.debug".equals(id)) {
							// Java breakpoint was removed. Figure out which
							// corresponding NatSpec breakpoint needs to be
							// removed.
							Object natspecResource = attributes.get(IDebugConstants.CORRESSPONDING_RESOURCE);
							Object markerID = attributes.get(IDebugConstants.CORRESSPONDING_MARKER_ID);
							if (natspecResource instanceof String && markerID instanceof String) {
								remove((String) natspecResource, (String) markerID, resource);
							}
						}
					}
				}
			}
		}
	}

	private void remove(String pathString, String markerIDString, IResource resource) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IPath path = new Path(pathString);
		IFile file = root.getFile(path);
		if (!file.exists()) {
			return;
		}
		IMarker marker = file.getMarker(Long.parseLong(markerIDString));
		if (!marker.exists()) {
			return;
		}
		try {
			delete(marker, resource);
		} catch (CoreException e) {
			System.out.println("JavaFileChangeListener.remove() " + e.getMessage());
			// Ignore this
		}
	}

	private void delete(final IMarker marker, IResource resource) throws CoreException {
		Job deleteMarkerJob = new Job("Deleting NatSpec breakpoint") {
			
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					marker.delete();
				} catch (CoreException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return Status.OK_STATUS;
			}
		};
		deleteMarkerJob.schedule();
	}

	@Override
	protected void handleResourceChange(IResourceDelta delta, int eventType) {
		// Do nothing.
	}
}
