/**
 * This package contains a simplified model of the Java language covering only
 * classifiers, methods, parameters, annotations and types. The aim of this 
 * model is to decouple the extraction of this information from source code and
 * the conversion of annotations to syntax patterns. Source code analyzers 
 * (e.g., the NatSpec JDT integration) create a model of the source code using
 * the classes in this package. From this model, the 
 * {@link de.devboost.natspec.testscripting.java.extract.JavaModelPatternExtractor}
 * can then extract syntax patterns.
 */
package de.devboost.natspec.testscripting.java.model;