package de.devboost.natspec.testscripting.java.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

public class EnumMatch extends AbstractSyntaxPatternPartMatch {

	private final String enumClassName;
	private final String value;

	public EnumMatch(List<Word> words, String enumClassName, String value) {
		super(words);
		this.enumClassName = enumClassName;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getEnumClassName() {
		return enumClassName;
	}
}
