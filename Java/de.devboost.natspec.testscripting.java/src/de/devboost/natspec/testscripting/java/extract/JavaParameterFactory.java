package de.devboost.natspec.testscripting.java.extract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.devboost.natspec.annotations.BooleanSyntax;
import de.devboost.natspec.annotations.Many;
import de.devboost.natspec.annotations.Optional;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DoubleArgument;
import de.devboost.natspec.patterns.parts.FloatArgument;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaEnum;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.java.patterns.parts.BigDecimalArgument;
import de.devboost.natspec.testscripting.java.patterns.parts.EnumParameter;
import de.devboost.natspec.testscripting.java.patterns.parts.LocalDateParameter;
import de.devboost.natspec.testscripting.java.patterns.parts.LocalDateTimeParameter;
import de.devboost.natspec.testscripting.java.patterns.parts.LocalTimeParameter;
import de.devboost.natspec.testscripting.patterns.IAnnotation;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.IParameter;
import de.devboost.natspec.testscripting.patterns.IValidationCallback;
import de.devboost.natspec.testscripting.patterns.parts.BooleanParameter;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.DateParameter;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;
import de.devboost.natspec.testscripting.patterns.parts.ListParameter;
import de.devboost.natspec.testscripting.patterns.parts.ManyParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

/**
 * The {@link JavaParameterFactory} can be used to create syntax pattern parts that match Java types. It therefore
 * provides a default mapping between Java types and {@link ISyntaxPatternPart}s.
 */
public class JavaParameterFactory implements IParameterFactory {

	private static final JavaAnnotationHelper JAVA_ANNOTATION_HELPER = JavaAnnotationHelper.INSTANCE;

	@Override
	public ISyntaxPatternPart createParameter(IParameter parameter) throws UnsupportedTypeException {
		boolean implicit = false;
		return createParameter(parameter, implicit);
	}

	@Override
	public ISyntaxPatternPart createParameter(IParameter parameter, boolean implicit) throws UnsupportedTypeException {
		if (implicit) {
			return createImplicitParameter(parameter);
		} else {
			return createExplicitParameter(parameter);
		}
	}

	/**
	 * Returns all annotations of the given parameter that have no effect if the parameter is implicit (i.e., not
	 * associated with a place holder).
	 * 
	 * @param parameter the method parameter for which the annotations need to be filtered
	 */
	private List<IAnnotation> getUselessAnnotationsForImplicitParamaters(IParameter parameter) {
		List<IAnnotation> uselessAnnotations = new ArrayList<IAnnotation>(2);

		List<IAnnotation> annotations = parameter.getAnnotations();
		for (IAnnotation annotation : annotations) {
			if (JAVA_ANNOTATION_HELPER.isBooleanSyntaxAnnotation(annotation)) {
				uselessAnnotations.add(annotation);
			} else if (JAVA_ANNOTATION_HELPER.isManySyntaxAnnotation(annotation)) {
				uselessAnnotations.add(annotation);
			}
		}

		return uselessAnnotations;
	}

	@Override
	public ISyntaxPatternPart createParameter(IClass type) throws UnsupportedTypeException {
		String parameterName = null;
		boolean implicit = false;
		boolean optional = false;
		return createParameter(type, parameterName, implicit, optional);
	}

	@Override
	public ISyntaxPatternPart createParameter(IClass type, boolean implicit) throws UnsupportedTypeException {
		String parameterName = null;
		boolean optional = false;
		return createParameter(type, parameterName, implicit, optional);
	}

	private ISyntaxPatternPart createParameter(IClass type, String parameterName, boolean implicit, boolean optional)
			throws UnsupportedTypeException {

		if (implicit) {
			return createImplicitParameter(type, optional, false);
		} else {
			List<? extends IClass> typeArguments = type.getTypeArguments();
			List<IAnnotation> annotations = Collections.<IAnnotation> emptyList();
			return createExplicitParameter(type, parameterName, typeArguments, annotations);
		}
	}

	private ISyntaxPatternPart createImplicitParameter(IParameter parameter) throws UnsupportedTypeException {
		boolean optional = JAVA_ANNOTATION_HELPER.hasOptionalAnnotation(parameter.getAnnotations());
		boolean isThis = JAVA_ANNOTATION_HELPER.hasThisAnnotation(parameter.getAnnotations());

		List<IAnnotation> parameterAnnotations = getUselessAnnotationsForImplicitParamaters(parameter);
		for (IAnnotation annotation : parameterAnnotations) {
			if (annotation instanceof JavaAnnotation) {
				JavaAnnotation javaAnnotation = (JavaAnnotation) annotation;
				IValidationCallback validationCallback = javaAnnotation.getValidationResultCallback();
				String annotationTypeName = javaAnnotation.getAnnotationTypeName();
				String message = "Using @" + annotationTypeName + " for implicit parameters has no effect";
				validationCallback.addWarning(message);
			}
		}

		IClass type = parameter.getType();
		String qualifiedName = type.getQualifiedName();
		String realTypeName = NameHelper.INSTANCE.getNameWithoutTypeArguments(qualifiedName);
		checkUsageOfOptionalAnnotation(realTypeName, parameter.getAnnotations(), true);
		// FIXME checkUsageOfThisAnnotation()

		return createImplicitParameter(type, optional, isThis);
	}

	private ISyntaxPatternPart createImplicitParameter(IClass type, boolean optional, boolean isThis) {
		String qualifiedName = type.getQualifiedName();
		boolean considerLastObjectOnly = false;
		return new ImplicitParameter(qualifiedName, considerLastObjectOnly, optional, isThis);
	}

	/**
	 * Use {@link #createParameter(JavaType)} instead.
	 */
	@Deprecated
	public ISyntaxPatternPart createParameter(String qualifiedTypeName) {
		try {
			JavaType type = new JavaType(qualifiedTypeName);
			List<IClass> typeArguments = Collections.<IClass> emptyList();
			List<IAnnotation> annotations = Collections.<IAnnotation> emptyList();
			return createExplicitParameter(type, null, typeArguments, annotations);
		} catch (UnsupportedTypeException e) {
			return null;
		}
	}

	/**
	 * Creates a {@link ISyntaxPatternPart} that matches the given parameter, assuming the parameter is explicit (i.e.,
	 * it corresponds to a place holder in the text syntax specification).
	 * 
	 * @param parameter
	 *            the method parameter to create the {@link ISyntaxPatternPart} for
	 * @return the created part
	 * @throws UnsupportedTypeException
	 *             if the type of the parameter is not supported
	 */
	private ISyntaxPatternPart createExplicitParameter(IParameter parameter) throws UnsupportedTypeException {

		IClass type = parameter.getType();
		String name = parameter.getName();
		List<? extends IClass> typeArguments = type.getTypeArguments();
		List<IAnnotation> annotations = parameter.getAnnotations();

		return createExplicitParameter(type, name, typeArguments, annotations);
	}

	/**
	 * Creates a {@link ISyntaxPatternPart} that matches the given type, assuming the parameter which has that type is
	 * explicit (i.e., it corresponds to a place holder in the text syntax specification).
	 * 
	 * @param type
	 *            the type of the method parameter to create the {@link ISyntaxPatternPart} for
	 * @param parameterName
	 *            the name of the method parameter to create the {@link ISyntaxPatternPart} for
	 * @param typeArguments
	 *            the type arguments of the method parameter (if the parameter has a generic type)
	 * @param annotations
	 *            the annotations of the method parameter
	 * @return the created part
	 * @throws UnsupportedTypeException
	 *             if the type of the parameter is not supported
	 */
	private ISyntaxPatternPart createExplicitParameter(IClass type, String parameterName,
			List<? extends IClass> typeArguments, List<IAnnotation> annotations) throws UnsupportedTypeException {

		String qualifiedTypeName = type.getQualifiedName();
		String qualifiedTypeNameWithoutTypeArguments = NameHelper.INSTANCE
				.getNameWithoutTypeArguments(qualifiedTypeName);

		JavaAnnotation booleanSyntaxAnnotation = JAVA_ANNOTATION_HELPER.findBooleanSyntaxAnnotation(annotations);
		boolean hasBooleanSyntaxAnnotation = booleanSyntaxAnnotation != null;

		JavaAnnotation isManyAnnotation = JAVA_ANNOTATION_HELPER.findManyAnnotation(annotations);

		checkUsageOfOptionalAnnotation(qualifiedTypeNameWithoutTypeArguments, annotations, false);

		if (!JavaTypeHelper.INSTANCE.isBoolean(qualifiedTypeNameWithoutTypeArguments) && hasBooleanSyntaxAnnotation) {
			// Add warning if there is a @BooleanSyntax annotation, but the parameter is not a boolean
			IValidationCallback validationCallback = booleanSyntaxAnnotation.getValidationResultCallback();
			String message = "@" + BooleanSyntax.class.getSimpleName() + " must not be used for non-boolean parameters";
			validationCallback.addError(message);
			throw new UnsupportedTypeException(message, true);
		} else if (isManyAnnotation != null) {
			if (String.class.getName().equals(type.getQualifiedName())) {
				boolean implicit = false;
				return new ManyParameter(createParameter(type, parameterName, implicit, false), type);
			} else {
				// Attach warning if @Many is used for parameters with different type.
				IValidationCallback validationCallback = isManyAnnotation.getValidationResultCallback();
				String message = "The @" + Many.class.getSimpleName()
						+ " annotation must be used for String parameters only.";
				validationCallback.addError(message);
				throw new UnsupportedTypeException(message, true);
			}
		} else if (JavaTypeHelper.INSTANCE.isInteger(qualifiedTypeNameWithoutTypeArguments)) {
			return new IntegerArgument();
		} else if (JavaTypeHelper.INSTANCE.isDouble(qualifiedTypeNameWithoutTypeArguments)) {
			return new DoubleArgument();
		} else if (JavaTypeHelper.INSTANCE.isFloat(qualifiedTypeNameWithoutTypeArguments)) {
			return new FloatArgument();
		} else if (JavaTypeHelper.INSTANCE.isBigDecimal(qualifiedTypeNameWithoutTypeArguments)) {
			return new BigDecimalArgument();
		} else if (JavaTypeHelper.INSTANCE.isString(qualifiedTypeNameWithoutTypeArguments)) {
			return new StringParameter(parameterName);
		} else if (JavaTypeHelper.INSTANCE.isBoolean(qualifiedTypeNameWithoutTypeArguments) && parameterName != null) {
			if (!hasBooleanSyntaxAnnotation) {
				// If there is no @BooleanSyntax annotation, we use the parameter name as value for 'true' and the empty
				// string for 'false'.
				String trueValue = parameterName;
				return new BooleanParameter(trueValue, "");
			} else {
				List<String> annotationValues = JAVA_ANNOTATION_HELPER.getValues(booleanSyntaxAnnotation,
						JavaAnnotation.ANNOTATION_PROPERTY_NAME);
				if (annotationValues.size() == 2) {
					String trueValue = annotationValues.get(0);
					String falseValue = annotationValues.get(1);
					return new BooleanParameter(trueValue, falseValue);
				} else {
					// Signal error
					IValidationCallback validationCallback = booleanSyntaxAnnotation.getValidationResultCallback();
					String message = "Unsupported number of arguments (" + annotationValues.size() + ") for @"
							+ BooleanSyntax.class.getSimpleName();
					validationCallback.addError(message);
					throw new UnsupportedTypeException(message, true);
				}
			}
		} else if (JavaTypeHelper.INSTANCE.isList(qualifiedTypeNameWithoutTypeArguments)) {
			if (typeArguments.size() == 0) {
				IClass listElementType = new JavaType(Object.class);
				boolean implicit = false;
				boolean optional = false;
				return new ListParameter(createParameter(listElementType, parameterName, implicit, optional),
						listElementType);
			} else if (typeArguments.size() == 1) {
				IClass listElementType = typeArguments.get(0);
				boolean implicit = false;
				boolean optional = false;
				return new ListParameter(createParameter(listElementType, parameterName, implicit, optional),
						listElementType);
			} else {
				throw new UnsupportedTypeException("Unsupported list type: " + qualifiedTypeNameWithoutTypeArguments
						+ " [type arguments=" + typeArguments + "]");
			}
		} else if (JavaTypeHelper.INSTANCE.isObject(qualifiedTypeNameWithoutTypeArguments)) {
			// TODO Other parameters can also be converted to 'Object'. how
			// can we decide which one to use?
			// TODO Why do we create StringParameters for Object parameters?
			return new StringParameter();
		} else if (JavaTypeHelper.INSTANCE.isDate(qualifiedTypeNameWithoutTypeArguments)) {
			return new DateParameter();
		} else if (JavaTypeHelper.INSTANCE.isLocalTime(qualifiedTypeNameWithoutTypeArguments)) {
			return new LocalTimeParameter();
		} else if (JavaTypeHelper.INSTANCE.isLocalDate(qualifiedTypeNameWithoutTypeArguments)) {
			return new LocalDateParameter();
		} else if (JavaTypeHelper.INSTANCE.isLocalDateTime(qualifiedTypeNameWithoutTypeArguments)) {
			return new LocalDateTimeParameter();
		} else if (type instanceof JavaEnum) {
			JavaEnum javaEnum = (JavaEnum) type;
			return new EnumParameter(qualifiedTypeName, javaEnum.getLiterals());
		} else {
			// we have a type that is not built in
			return new ComplexParameter(qualifiedTypeNameWithoutTypeArguments);
		}
	}

	/**
	 * Checks whether the @Optional annotation is used correctly for the given parameter (if it is used at all).
	 * 
	 * @param parameterTypeName
	 *            the type of the method parameter
	 * @param annotations
	 *            the annotation attached to the method parameter
	 * @param implicit
	 *            <code>true</code> if the parameter is implicit (i.e., there is no place holder that corresponds to the
	 *            parameter)
	 * @throws UnsupportedTypeException
	 *             if the @Optional annotation is used for a parameter with a primitive type
	 */
	private void checkUsageOfOptionalAnnotation(String parameterTypeName, List<IAnnotation> annotations,
			boolean implicit) throws UnsupportedTypeException {

		JavaAnnotation optionalAnnotation = JAVA_ANNOTATION_HELPER.findAnnotation(annotations, Optional.class);
		boolean optional = optionalAnnotation != null;

		String optionalSimpleClassName = "@" + Optional.class.getSimpleName();

		if (!implicit && optional) {
			IValidationCallback validationCallback = optionalAnnotation.getValidationResultCallback();
			String message = optionalSimpleClassName + " must be used for implicit parameters only";
			validationCallback.addError(message);
		}

		if (optional && JavaTypeHelper.INSTANCE.isPrimitive(parameterTypeName)) {
			IValidationCallback validationCallback = optionalAnnotation.getValidationResultCallback();
			String message = optionalSimpleClassName + " must not be used for parameters with primitive type";
			validationCallback.addError(message);

			// We have already attached an error, but we still throw the exception to make sure the faulty parameter is
			// ignored.
			boolean handled = true;
			throw new UnsupportedTypeException(message, handled);
		}
	}

	@Override
	public boolean isDefaultSyntaxPattern(String[] parts, List<? extends IParameter> parameters) {

		// The default syntax pattern must have one part
		if (parts.length != 1) {
			return false;
		}

		// The default syntax pattern must equal '#1'
		if (!"#1".equals(parts[0].trim())) {
			return false;
		}

		if (!JavaParameterHelper.INSTANCE.hasOneStringListParameter(parameters)) {
			return true;
		}

		if (!JavaParameterHelper.INSTANCE.hasOneAtManyStringParameter(parameters)) {
			return true;
		}

		return false;
	}

	@Override
	public boolean isDefaultSyntaxPatternWithMany(List<? extends IParameter> parameters) {
		return JavaParameterHelper.INSTANCE.hasOneAtManyStringParameter(parameters);
	}
}
