package de.devboost.natspec.testscripting.java;

import java.util.regex.Pattern;

import de.devboost.natspec.testscripting.builder.AbstractTemplateInstantiator;
import de.devboost.natspec.testscripting.builder.ITemplateInstantiator;

// TODO The replacement logic in this class will not work for arbitrary Java
// template classes. To have a clean implementation we must use a minimal Java
// lexer that processes package declarations, imports and class declarations.
public class JavaTemplateInstantiator extends AbstractTemplateInstantiator implements ITemplateInstantiator {

	private final static Pattern REGEX_PROTECTED_CLASS = Pattern.compile("protected class");
	private final static Pattern REGEX_PUBLIC_CLASS = Pattern.compile("public class");
	private final static Pattern REGEX_IMPORT = Pattern.compile("import");
	private final static Pattern REGEX_PACKAGE_DECLARATION = Pattern.compile("package [^;]*;");
	// This corresponds to /* @Imports */
	private final static Pattern REGEX_IMPORTS_PLACEHOLDER = Pattern.compile("/\\* @Imports \\*/");
	private final static Pattern REGEX_METHOD_BODY_PLACEHOLDER = Pattern.compile("/\\* @MethodBody \\*/");
	private final static Pattern REGEX_PARAMETRIZATION_PLACEHOLDER = Pattern.compile("/\\* @ParameterizationCode \\*/");

	public JavaTemplateInstantiator() {
		super(JavaTemplateFileFinder.INSTANCE);
	}
	
	@Override
	public String replaceClassName(String className, String result) {
		String fileExtension = "." + JavaNatSpecConstants.JAVA_EXTENSION;
		String templateClassName = JavaNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME
				.substring(0, JavaNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME
						.lastIndexOf(fileExtension));
		result = replaceAll(result, templateClassName, className);
		// TODO This is not exact. There might be more whitespace characters

		return result;
	}

	@Override
	public String replacePackageName(String packageName, String result) {
		boolean templateContainsPackageDeclaration = getIndexOf(result,
				REGEX_PACKAGE_DECLARATION) >= 0;
		if (templateContainsPackageDeclaration) {
			result = replace(result, REGEX_PACKAGE_DECLARATION, "package "
					+ packageName + ";");
		} else {
			boolean templateContainsImport = getIndexOf(result, REGEX_IMPORT) >= 0;
			if (templateContainsImport) {
				result = replace(result, REGEX_IMPORT, "package "
						+ packageName + ";\n\n" + REGEX_IMPORT);
			} else {
				for (Pattern keyword : new Pattern[] { REGEX_PUBLIC_CLASS,
						REGEX_PROTECTED_CLASS }) {
					result = replace(result, keyword, "package " + packageName
							+ ";\n\n" + keyword);
				}
			}
		}
		return result;
	}

	/**
	 * Searches for an @Imports placeholder. If one is found, the placeholder is
	 * replaced with the new imports. If not present, we search for existing
	 * imports in the file and insert the new import statements before these.
	 */
	@Override
	public String addImports(String importStatements, String result) {
		if (importStatements.isEmpty()) {
			return result;
		}
		
		// check if @Imports is used, if not add to end of imports
		int index = getIndexOf(result, REGEX_IMPORTS_PLACEHOLDER);
		if (index < 0) {
			boolean templateContainsImport = getIndexOf(result,
					REGEX_IMPORT) >= 0;
			if (templateContainsImport) {
				result = replace(result, REGEX_IMPORT, importStatements
						+ "\nimport");
			} else {
				for (Pattern keyword : new Pattern[] { REGEX_PUBLIC_CLASS,
						REGEX_PROTECTED_CLASS }) {
					result = replace(result, keyword, importStatements + "\n"
							+ keyword);
				}
			}
		} else {
			result = replace(result, REGEX_IMPORTS_PLACEHOLDER, importStatements);
		}
				
		return result;
	}
	
	@Override
	public String replaceMethodBody(String generatedStatements, String result) {
		result = replace(result, REGEX_METHOD_BODY_PLACEHOLDER, generatedStatements);
		return result;
	}
	
	protected String replaceParametrizationCode(String generatedStatements, String result) {
		result = replace(result, REGEX_PARAMETRIZATION_PLACEHOLDER, generatedStatements);
		return result;
	}

	@Override
	public String addMethods(String generatedMethods, String result) {
		int lastClosingBracket = result.lastIndexOf("}");
		if (lastClosingBracket < 0) {
			// Template does not contain a '}'. Can't add methods.
			return result;
		}
		
		String head = result.substring(0, lastClosingBracket);
		String tail = result.substring(lastClosingBracket, result.length());
		result = head + generatedMethods + tail;
		return result;
	}

	@Override
	protected String doIndentation(String templateContent, String replacement,
			int index) {
		
		int tabs = countTabsBeforeIndex(templateContent, index);
		String tabsString = getCharNTimesString(tabs, "\t");
		String replacementWithTabs = replacement.replace("\n", "\n"
				+ tabsString);
		return replacementWithTabs;
	}
}
