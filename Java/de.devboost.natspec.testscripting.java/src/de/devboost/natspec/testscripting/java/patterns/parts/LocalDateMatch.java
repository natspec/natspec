package de.devboost.natspec.testscripting.java.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

public class LocalDateMatch extends AbstractSyntaxPatternPartMatch {

	private final String value;

	public LocalDateMatch(List<Word> words, String value) {
		super(words);
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
