package de.devboost.natspec.testscripting.java.model;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.testscripting.patterns.IValidationCallback;

/**
 * A {@link JavaMethod} is an AST element that represents methods in Java
 * classes. Every {@link JavaMethod} can be annotated (using
 * {@link JavaAnnotation}s) and has a list of {@link JavaParameter}s.
 */
public class JavaMethod {

	private final String name;
	private final List<JavaAnnotation> annotations = new ArrayList<JavaAnnotation>();
	private final List<JavaParameter> parameters = new ArrayList<JavaParameter>();
	
	private JavaModifier modifier;
	private JavaType returnType;
	private IValidationCallback validationCallback;

	/**
	 * Creates a new {@link JavaMethod} with the given name.
	 */
	public JavaMethod(String name, IValidationCallback validationCallback) {
		super();
		this.name = name;
		this.validationCallback = validationCallback;
	}

	public String getName() {
		return name;
	}

	public List<JavaAnnotation> getAnnotations() {
		return annotations;
	}

	public List<JavaParameter> getParameters() {
		return parameters;
	}

	public JavaType getReturnType() {
		return returnType;
	}

	public void setReturnType(JavaType returnType) {
		this.returnType = returnType;
	}

	public void setModifier(JavaModifier modifier) {
		this.modifier = modifier;
	}

	public JavaModifier getModifier() {
		return modifier;
	}

	public IValidationCallback getValidationResultCallback() {
		return validationCallback;
	}
}
