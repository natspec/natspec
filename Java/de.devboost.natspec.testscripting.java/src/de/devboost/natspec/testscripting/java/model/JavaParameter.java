package de.devboost.natspec.testscripting.java.model;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.testscripting.patterns.IAnnotation;
import de.devboost.natspec.testscripting.patterns.IParameter;
import de.devboost.natspec.util.ComparisonHelper;

/**
 * A {@link JavaParameter} is an AST element that represents Java method
 * parameters. Every {@link JavaParameter} is identified by a name and a type.
 */
public class JavaParameter implements IParameter, IComparable {

	private final String name;
	private final JavaType type;
	private final List<IAnnotation> annotations;

	public JavaParameter(String name, JavaType type, List<JavaAnnotation> annotations) {
		super();
		this.name = name;
		this.type = type;
		this.annotations = new ArrayList<IAnnotation>(annotations.size());
		this.annotations.addAll(annotations);
	}

	public String getName() {
		return name;
	}

	public JavaType getType() {
		return type;
	}

	public List<IAnnotation> getAnnotations() {
		return annotations;
	}
	
	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JavaParameter other = (JavaParameter) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!ComparisonHelper.INSTANCE.isEqualTo(type, other.type)) {
			return false;
		}
		return true;
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
}
