package de.devboost.natspec.testscripting.java.quickfixes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.Document;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameterMatcher;
import de.devboost.natspec.testscripting.patterns.parts.DateParameter;

/**
 * The {@link SentenceToMethodConverter} can be used to derive a test support
 * method from an existing (unmatched) sentence in a NatSpec document. It 
 * applies several heuristics to decide which words in the sentence correspond
 * to static syntax and which are most probably parameters.
 */
public class SentenceToMethodConverter {
	
	private static final String JAVA_LANG_PACKAGE = "java.lang.";
	private static final Pattern JAVA_LANG_CLASS_PATTERN = Pattern.compile(JAVA_LANG_PACKAGE + "[a-zA-Z]+");
	
	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;
	
	public SentenceToMethodConverter(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	/**
	 * Derives a Java NatSpec test support method for the given sentence.
	 * 
	 * @param sentence the sentence the new method must match
	 * @param uri the URI of the document containing the sentence. We do not use
	 * sentence.eResource() because the sentence may be detached from a concrete
	 * resource.
	 * 
	 * @return a method or <code>null</code> if no method can be derived
	 */
	public IDerivedMethod deriveMethod(Sentence sentence, URI uri) {
		EObject container = sentence.eContainer();
		if (container instanceof Document) {
			Document document = (Document) container;
			List<Sentence> contents = document.getContents();
			int index = contents.indexOf(sentence);
			
			PatternMatchContextFactory contextFactory = PatternMatchContextFactory.INSTANCE;
			IPatternMatchContext context = contextFactory.createPatternMatchContext(uri);
			MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);
			// Match sentences before
			for (int i = 0; i < index; i++) {
				Sentence nextSentence = contents.get(i);
				matchService.match(nextSentence, context);
			}
			
			List<String> words = WordUtil.INSTANCE.toStringList(sentence.getWords());
			return deriveMethod(words, context);
		}
		
		return null;
	}

	public IDerivedMethod deriveMethod(List<String> words, IPatternMatchContext patternMatchContext) {

		List<DerivedMethodPart> parts = deriveParts(words, patternMatchContext);
		final String name = getMethodName(parts);
		final String code = getCodeForMethod(parts);
		final List<JavaType> parameterTypes = getParameterTypes(parts);

		return new IDerivedMethod() {
			
			@Override
			public String getName() {
				return name;
			}
			
			@Override
			public String getCode() {
				return code;
			}

			@Override
			public List<JavaType> getParameterTypes() {
				return parameterTypes;
			}
		};
	}

	private String getCodeForMethod(List<DerivedMethodPart> parts) {
		// compose method name, annotation and parameters from the parts
		String annotation = getAnnotation(parts);
		String methodName = getMethodName(parts);
		String parameters = getParameters(parts);
		
		// compose complete code for method
		StringBuilder methodCode = new StringBuilder();
		methodCode.append("@TextSyntax(\"");
		methodCode.append(annotation);
		methodCode.append("\")\n");
		methodCode.append("public void ");
		methodCode.append(methodName);
		methodCode.append("(");
		methodCode.append(parameters);
		methodCode.append(") {\n");
		methodCode.append("throw new UnsupportedOperationException(\"Not implemented yet.\");\n");
		methodCode.append("}");
		return methodCode.toString();
	}

	private List<DerivedMethodPart> deriveParts(List<String> words, IPatternMatchContext context) {
		ParameterNameHelper parameterNameHelper = new ParameterNameHelper();
		int parameterIndex = 1;
		List<DerivedMethodPart> parts = new ArrayList<DerivedMethodPart>();
		int wordCount = words.size();
		for (int i = 0; i < wordCount; i++) {
			String word = words.get(i);
			boolean isLastWord = words.indexOf(word) == wordCount - 1;
			int matching = matchesObjectInContext(words.subList(i, wordCount), parts, parameterIndex, context);
			if (matching > 0) {
				i += matching - 1;
				parameterIndex++;
				continue;
			}
			
			if (isInteger(word)) {
				String parameterName = parameterNameHelper.getNewParameterName(parts, "p");
				parts.add(new DerivedMethodPart("#" + parameterIndex, "", "int", parameterName));
				parameterIndex++;
				continue;
			}
			
			if (isBoolean(word)) {
				String parameterName = parameterNameHelper.getNewParameterName(parts, "p");
				parts.add(new DerivedMethodPart("#" + parameterIndex, "", "boolean", parameterName));
				parameterIndex++;
				continue;
			}
			
			if (isFloatOrDouble(word)) {
				String parameterName = parameterNameHelper.getNewParameterName(parts, "p");
				parts.add(new DerivedMethodPart("#" + parameterIndex, "", "double", parameterName));
				parameterIndex++;
				continue;
			}
			
			int wordsUsableAsDate = isDate(words.subList(i, words.size()));
			if (wordsUsableAsDate > 0) {
				String parameterName = parameterNameHelper.getNewParameterName(parts, "date");
				parts.add(new DerivedMethodPart("#" + parameterIndex, "", "java.util.Date", parameterName));
				i += wordsUsableAsDate - 1;
				parameterIndex++;
				continue;
			}
			
			if (isString(word, isLastWord)) {
				String parameterName = getCleanWord(word);
				parameterName = parameterNameHelper.getNewParameterName(parts, parameterName);
				parts.add(new DerivedMethodPart("#" + parameterIndex, "", "String", parameterName));
				parameterIndex++;
				continue;
			}
			
			parts.add(new DerivedMethodPart(word, word, "", ""));
		}
		return parts;
	}

	private int matchesObjectInContext(List<String> words,
			List<DerivedMethodPart> parts, int parameterIndex,
			IPatternMatchContext context) {
		
		List<Object> objectsInContext = context.getObjectsInContext();
		for (Object objectInContext : objectsInContext) {
			if (objectInContext instanceof ObjectCreation) {
				ObjectCreation objectCreation = (ObjectCreation) objectInContext;
				List<String> identifiers = objectCreation.getIdentifiers();
				int matching = ComplexParameterMatcher.INSTANCE.matchStrings(words, identifiers, context, null);
				if (matching > 0) {
					IClass javaType = objectCreation.getObjectType();
					String qualifiedType = javaType.getQualifiedName();
					if (JAVA_LANG_CLASS_PATTERN.matcher(qualifiedType).matches()) {
						qualifiedType = qualifiedType.substring(JAVA_LANG_PACKAGE.length());
					}
					String simpleTypeName = objectCreation.getSimpleTypeName();
					String parameterName = StringUtils.INSTANCE.toFirstLower(simpleTypeName);
					parameterName = new ParameterNameHelper().getNewParameterName(parts, parameterName);
					parts.add(new DerivedMethodPart("#" + parameterIndex, "", qualifiedType, parameterName));
					return matching;
				}
			}
		}
		
		return 0;
	}

	/**
	 * Checks whether the first or the first two words of the given list can be used as date parameter. Returns the
	 * number of words that can be used. If the words cannot be used as data parameters, zero is returned.
	 */
	private int isDate(List<String> words) {
		if (words.isEmpty()) {
			return 0;
		}
		
		if (words.size() > 1) {
			String word = words.get(0) + " " + words.get(1);
			if (matchesDateFormat(word, DateParameter.DATE_TIME_FORMAT_PATTERN)) {
				return 2;
			}
		}
		
		if (matchesDateFormat(words.get(0), DateParameter.DATE_FORMAT_PATTERN)) {
			return 1;
		}
		
		return 0;
	}

	private boolean matchesDateFormat(String word, String pattern) {
		SimpleDateFormat dateFormat = DateParameter.createDateFormat(pattern);
		try {
			dateFormat.parse(word);
			return true;
		} catch (ParseException e) {
			return false;
		}
	}

	private boolean isString(String text, boolean isLastWord) {
		if (text.contains("_") ||
			text.contains(":")) {
			return true;
		}
		if (text.contains(".")) {
			if (!text.endsWith(".") || !isLastWord) {
				return true;
			}
		}
		if (isCamelCase(text)) {
			return true;
		}
		if (isUpperCase(text)) {
			return true;
		}
		for (int i = 0; i <= 9; i++) {
			if (text.contains(Integer.toString(i))) {
				return true;
			}
		}
		return false;
	}

	private boolean isCamelCase(String text) {
		boolean foundLower = false;
		for (int i = 0; i < text.length(); i++) {
			char nextChar = text.charAt(i);
			if (Character.isLowerCase(nextChar)) {
				foundLower = true;
				continue;
			}
			if (Character.isUpperCase(nextChar) && foundLower) {
				return true;
			}
		}
		return false;
	}

	private boolean isUpperCase(String text) {
		for (int i = 0; i < text.length(); i++) {
			char nextChar = text.charAt(i);
			if (!Character.isUpperCase(nextChar)) {
				return false;
			}
		}
		return true;
	}

	private boolean isFloatOrDouble(String text) {
		try {
			Float.parseFloat(text);
			return true;
		} catch (NumberFormatException e) {
			// ignore
		}
		try {
			Double.parseDouble(text);
			return true;
		} catch (NumberFormatException e) {
			// ignore
		}
		return false;
	}

	private boolean isInteger(String text) {
		try {
			Integer.parseInt(text);
			return true;
		} catch (NumberFormatException e) {
			// ignore
		}
		return false;
	}

	private boolean isBoolean(String text) {
		return Boolean.TRUE.toString().equalsIgnoreCase(text) || Boolean.FALSE.toString().equalsIgnoreCase(text);
	}

	private String getAnnotation(List<DerivedMethodPart> parts) {
		StringBuilder annotation = new StringBuilder();
		for (DerivedMethodPart part : parts) {
			String annotationPart = part.getAnnotationPart();
			if (annotationPart.length() <= 0) {
				continue;
			}
			
			annotation.append(annotationPart);
			if (parts.indexOf(part) < parts.size() - 1) {
				// is not last
				annotation.append(" ");
			}
		}
		return annotation.toString();
	}
		
	private String getMethodName(List<DerivedMethodPart> parts) {
		StringBuilder methodName = new StringBuilder();
		for (DerivedMethodPart part : parts) {
			String cleanWord = getCleanWord(part.getMethodNamePart());
			if (cleanWord.length() <= 0) {
				continue;
			}
			
			if (parts.indexOf(part) == 0) {
				// is first
				methodName.append(StringUtils.INSTANCE.toFirstLower(cleanWord));
			} else {
				methodName.append(StringUtils.INSTANCE.toFirstUpper(cleanWord));
			}
		}
		
		ParameterNameHelper nameHelper = new ParameterNameHelper();
		String name = nameHelper.getValidIdentifier(methodName.toString());
		name = StringUtils.INSTANCE.toFirstLower(name);
		if (nameHelper.isJavaKeyword(name)) {
			return "_" + name;
		}
		
		return name;
	}

	private String getParameters(List<DerivedMethodPart> parts) {
		StringBuilder parameters = new StringBuilder();
		List<DerivedMethodPart> nonEmptyParts = new ArrayList<DerivedMethodPart>();
		for (DerivedMethodPart part : parts) {
			String parameterTypePart = part.getParameterTypePart();
			if (parameterTypePart.length() <= 0) {
				continue;
			}
			nonEmptyParts.add(part);
		}			
		
		for (DerivedMethodPart part : nonEmptyParts) {
			String parameterNamePart = part.getParameterNamePart();
			String parameterTypePart = part.getParameterTypePart();
			parameters.append(parameterTypePart);
			parameters.append(" ");
			parameters.append(parameterNamePart);
			if (nonEmptyParts.indexOf(part) < nonEmptyParts.size() - 1) {
				// is not last
				parameters.append(", ");
			}
		}
		return parameters.toString();
	}

	private List<JavaType> getParameterTypes(List<DerivedMethodPart> parts) {
		List<JavaType> types = new ArrayList<JavaType>();
		for (DerivedMethodPart part : parts) {
			String parameterTypePart = part.getParameterTypePart();
			if (parameterTypePart.length() <= 0) {
				continue;
			}
			
			types.add(new JavaType(parameterTypePart));
		}
		return types;
	}

	private String getCleanWord(String word) {
		StringBuilder cleanWord = new StringBuilder();
		for (int i = 0; i < word.length(); i++) {
			char nextChar = word.charAt(i);
			if (Character.isJavaIdentifierPart(nextChar)) {
				cleanWord.append(nextChar);
			}
		}
		return cleanWord.toString();
	}
}
