package de.devboost.natspec.testscripting.java;

import de.devboost.natspec.testscripting.AbstractTemplateFileFinder;

public class JavaTemplateFileFinder extends AbstractTemplateFileFinder {
	
	public final static JavaTemplateFileFinder INSTANCE = new JavaTemplateFileFinder();
	
	private JavaTemplateFileFinder() {
		super(JavaNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME,
				JavaNatSpecConstants.JAVA_EXTENSION);
	}
}
