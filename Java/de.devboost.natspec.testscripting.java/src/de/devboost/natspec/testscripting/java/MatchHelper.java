package de.devboost.natspec.testscripting.java;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import de.devboost.natspec.Document;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.resource.natspec.INatspecTextResource;

public class MatchHelper {
	
	public final static MatchHelper INSTANCE = new MatchHelper();

	private MatchHelper() {
	}

	/**
	 * Iterates over all sentences contained in the given resource and collects the matches that were stored by the
	 * previous matching process (triggered by the validation of the resource) in {@link Sentence#getMatches()}.
	 * 
	 * @param resource
	 *            the resource from which to collect matches
	 * @param templateFieldNameToTypeMap
	 *            the map from field names to field types of the NatSpec template that applies to the resource
	 * @return all matches found by the validation of the resource
	 */
	public List<ISyntaxPatternMatch<?>> getMatches(INatspecTextResource resource,
			Map<String, String> templateFieldNameToTypeMap) {

		// We only need to update the context once as it is the same for all sentences in the resource.
		boolean updatedContext = false;
		
		List<ISyntaxPatternMatch<?>> matches = new ArrayList<ISyntaxPatternMatch<?>>();
		List<EObject> contents = resource.getContents();
		for (EObject content : contents) {
			if (content instanceof Document) {
				Document document = (Document) content;
				List<Sentence> sentences = document.getContents();
				for (Sentence sentence : sentences) {
					List<Object> sentenceMatches = sentence.getMatches();
					for (Object sentenceMatch : sentenceMatches) {
						if (sentenceMatch instanceof ISyntaxPatternMatch<?>) {
							ISyntaxPatternMatch<?> syntaxPatternMatch = (ISyntaxPatternMatch<?>) sentenceMatch;
							matches.add(syntaxPatternMatch);
							
							if (!updatedContext) {
								IPatternMatchContext context = syntaxPatternMatch.getContext();
								context.setTemplateFieldNameToTypeMap(templateFieldNameToTypeMap);
								updatedContext = true;
							}
						}
					}
				}
			}
		}

		return matches;
	}
}
