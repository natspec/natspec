package de.devboost.natspec.testscripting.java.extract;

import java.util.List;

import de.devboost.natspec.testscripting.patterns.IAnnotation;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.IParameter;

public class JavaParameterHelper {

	public final static JavaParameterHelper INSTANCE = new JavaParameterHelper();
	
	private JavaParameterHelper() {
	}

	public boolean hasOneStringListParameter(List<? extends IParameter> parameters) {
		if (parameters.isEmpty()) {
			return false;
		}
		
		// The first must be a list...
		IClass parameterType = parameters.get(0).getType();
		if (!JavaTypeHelper.INSTANCE.isList(parameterType.getQualifiedName())) {
			return false;
		}

		// ...of type String
		List<? extends IClass> typeArguments = parameterType.getTypeArguments();
		if (typeArguments.size() != 1) {
			return false;
		}
		
		IClass typeArgument = typeArguments.get(0);
		if (!JavaTypeHelper.INSTANCE.isString(typeArgument.getQualifiedName())) {
			return false;
		}

		return true;
	}

	public boolean hasOneAtManyStringParameter(List<? extends IParameter> parameters) {
		if (parameters.isEmpty()) {
			return false;
		}
		
		IParameter firstParameter = parameters.get(0);
		IClass parameterType = firstParameter.getType();
		if (!JavaTypeHelper.INSTANCE.isString(parameterType.getQualifiedName())) {
			return false;
		}
		
		List<IAnnotation> annotations = firstParameter.getAnnotations();
		if (annotations.isEmpty()) {
			return false;
		}
		
		IAnnotation annotation = annotations.get(0);
		if (!JavaAnnotationHelper.INSTANCE.isManySyntaxAnnotation(annotation)) {
			return false;
		}

		return true;
	}
}
