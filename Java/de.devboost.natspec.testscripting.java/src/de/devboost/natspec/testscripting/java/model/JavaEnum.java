package de.devboost.natspec.testscripting.java.model;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.testscripting.patterns.IClass;

public class JavaEnum extends JavaType implements IComparable {

	private Set<String> literals;

	public JavaEnum(String qualifiedName, Set<String> literals) {
		super(qualifiedName);
		this.literals = literals;
	}

	@Override
	public List<? extends IClass> getSuperTypes() {
		return Collections.emptyList();
	}

	@Override
	public List<? extends IClass> getTypeArguments() {
		return Collections.emptyList();
	}

	@Override
	public Set<String> getAllSuperTypes() {
		return Collections.emptySet();
	}
	
	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((literals == null) ? 0 : literals.hashCode());
		return result;
	}
	
	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JavaEnum other = (JavaEnum) obj;
		if (literals == null) {
			if (other.literals != null)
				return false;
		} else if (!literals.equals(other.literals))
			return false;
		return super.isEqualTo(obj);
	}

	public Set<String> getLiterals() {
		return literals;
	}
}
