package de.devboost.natspec.testscripting.java;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringEscapeUtils;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.patterns.parts.DefaultSentencePartMatch;
import de.devboost.natspec.patterns.parts.DoubleMatch;
import de.devboost.natspec.patterns.parts.FloatMatch;
import de.devboost.natspec.testscripting.java.patterns.parts.BigDecimalMatch;
import de.devboost.natspec.testscripting.java.patterns.parts.EnumMatch;
import de.devboost.natspec.testscripting.java.patterns.parts.LocalDateMatch;
import de.devboost.natspec.testscripting.java.patterns.parts.LocalDateTimeMatch;
import de.devboost.natspec.testscripting.java.patterns.parts.LocalTimeMatch;
import de.devboost.natspec.testscripting.patterns.AbstractCodeFragment;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.parts.AbstractMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.parts.BooleanMatch;
import de.devboost.natspec.testscripting.patterns.parts.DateMatch;
import de.devboost.natspec.testscripting.patterns.parts.ListMatch;
import de.devboost.natspec.testscripting.patterns.parts.ManyMatch;
import de.devboost.natspec.testscripting.patterns.parts.ObjectMatch;
import de.devboost.natspec.testscripting.patterns.parts.StringMatch;

public class JavaMatchCodeGeneratorFactory extends AbstractMatchCodeGeneratorFactory {

	@Override
	public ICodeFragment createGenerator(final ISyntaxPatternPartMatch match) {
		if (match instanceof BigDecimalMatch) {
			BigDecimalMatch bigDecimalMatch = (BigDecimalMatch) match;
			return createGenerator(bigDecimalMatch);
		}
		if (match instanceof EnumMatch) {
			EnumMatch enumMatch = (EnumMatch) match;
			return createGenerator(enumMatch);
		}
		if (match instanceof LocalTimeMatch) {
			LocalTimeMatch localTimeMatch = (LocalTimeMatch) match;
			return createGenerator(localTimeMatch);
		}
		if (match instanceof LocalDateMatch) {
			LocalDateMatch localDateMatch = (LocalDateMatch) match;
			return createGenerator(localDateMatch);
		}
		if (match instanceof LocalDateTimeMatch) {
			LocalDateTimeMatch localDateTimeMatch = (LocalDateTimeMatch) match;
			return createGenerator(localDateTimeMatch);
		}

		return super.createGenerator(match);
	}

	@Override
	public ICodeFragment createGenerator(final StringMatch match) {
		return new AbstractCodeFragment() {
			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				String value = match.getValue();
				value = StringEscapeUtils.unescapeJava(value);
				value = escapeStringLiteral(value);
				context.addCode("\"" + value + "\"");
			}
		};
	}

	private String escapeStringLiteral(String text) {
		return StringEscapeUtils.escapeJava(text);
	}

	@Override
	public ICodeFragment createGenerator(final DateMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode("new " + Date.class.getName() + "(" + match.getDate().getTime() + "L)");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final ListMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				List<ISyntaxPatternPartMatch> elementMatches = match.getElementMatches();
				IClass listElementType = match.getListElementType();
				String elementType = listElementType.getQualifiedName();
				context.addCode("java.util.Arrays.asList(new " + elementType + "[] {");
				Iterator<ISyntaxPatternPartMatch> iterator = elementMatches.iterator();
				while (iterator.hasNext()) {
					ISyntaxPatternPartMatch elementMatch = iterator.next();
					ICodeFragment generator = createGenerator(elementMatch);
					generator.generateSourceCode(context);
					if (iterator.hasNext()) {
						context.addCode(", ");
					}
				}
				context.addCode("})");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final ManyMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				List<ISyntaxPatternPartMatch> elementMatches = match.getElementMatches();
				context.addCode("new java.lang.StringBuilder()");
				Iterator<ISyntaxPatternPartMatch> iterator = elementMatches.iterator();
				while (iterator.hasNext()) {
					ISyntaxPatternPartMatch elementMatch = iterator.next();
					ICodeFragment generator = createGenerator(elementMatch);
					context.addCode(".append(");
					generator.generateSourceCode(context);
					context.addCode(")");
					if (iterator.hasNext()) {
						context.addCode(".append(\" \")");
					}
				}
				context.addCode(".toString()");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final ObjectMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(match.getObjectCreation().getVariableName());
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final DoubleMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(Double.toString(match.getValue()));
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final FloatMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(Float.toString(match.getValue()) + "f");
			}
		};
	}

	public ICodeFragment createGenerator(final BigDecimalMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode("new " + BigDecimal.class.getName() + "(\"" + match.getValue().toString() + "\")");
			}
		};
	}

	public ICodeFragment createGenerator(final LocalTimeMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode("java.time.LocalTime.parse(\"" + match.getValue().toString() + "\")");
			}
		};
	}

	public ICodeFragment createGenerator(final LocalDateMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode("java.time.LocalDate.parse(\"" + match.getValue().toString() + "\")");
			}
		};
	}

	public ICodeFragment createGenerator(final LocalDateTimeMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode("java.time.LocalDateTime.parse(\"" + match.getValue().toString() + "\")");
			}
		};
	}

	@Override
	public ICodeFragment createGenerator(final DefaultSentencePartMatch match) {
		if (match.isMany()) {

			return new AbstractCodeFragment() {

				@Override
				public void generateSourceCode(ICodeGenerationContext context) {
					context.addCode("new java.lang.StringBuilder()");
					List<Word> matchedWords = match.getMatchedWords();
					List<String> matchedStrings = WordUtil.INSTANCE.toStringList(matchedWords);
					Iterator<String> iterator = matchedStrings.iterator();
					while (iterator.hasNext()) {
						String next = iterator.next();
						next = escapeStringLiteral(next);
						context.addCode(".append(");
						context.addCode("\"" + next + "\"");
						context.addCode(")");
						if (iterator.hasNext()) {
							context.addCode(".append(\" \")");
						}
					}
					context.addCode(".toString()");
				}
			};
		} else {
			return new AbstractCodeFragment() {

				@Override
				public void generateSourceCode(ICodeGenerationContext context) {
					context.addCode("java.util.Arrays.asList(new String[] {");
					List<Word> matchedWords = match.getMatchedWords();
					List<String> matchedStrings = WordUtil.INSTANCE.toStringList(matchedWords);
					Iterator<String> iterator = matchedStrings.iterator();
					while (iterator.hasNext()) {
						String next = iterator.next();
						next = escapeStringLiteral(next);

						context.addCode("\"" + next + "\"");
						if (iterator.hasNext()) {
							context.addCode(", ");
						}
					}
					context.addCode("})");
				}
			};
		}
	}

	@Override
	public ICodeFragment createGenerator(final BooleanMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(Boolean.toString(match.getValue()));
			}
		};
	}

	public ICodeFragment createGenerator(final EnumMatch match) {
		return new AbstractCodeFragment() {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				context.addCode(match.getEnumClassName() + ".valueOf(\"" + match.getValue() + "\")");
			}
		};
	}
}
