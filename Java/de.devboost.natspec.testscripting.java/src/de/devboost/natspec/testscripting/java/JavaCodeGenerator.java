package de.devboost.natspec.testscripting.java;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.devboost.essentials.StringUtils;
import de.devboost.natspec.Word;
import de.devboost.natspec.annotations.NameBasedSyntax;
import de.devboost.natspec.annotations.StyledTextSyntax;
import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;

/**
 * The {@link JavaCodeGenerator} is responsible to generate code for one sentence that was matched to a Java method
 * annotated with {@link TextSyntax}, {@link TextSyntaxes}, {@link NameBasedSyntax} or {@link StyledTextSyntax}.
 */
public class JavaCodeGenerator extends AbstractCodeGenerator {

	private static final StringUtils STRING_UTILS = StringUtils.INSTANCE;

	public static final PrefixBasedCommentGenerator JAVA_COMMENT_GENERATOR = new PrefixBasedCommentGenerator("//");

	private final String qualifiedTypeName;
	private final String methodName;
	private final String[] parameterTypes;
	private final Map<Integer, ISyntaxPatternPartMatch> argumentIndexToArgumentMatchMap;
	private final String qualifiedReturnTypeName;
	private final String returnVariableName;

	private boolean parameterizationCode;

	/**
	 * Creates a new {@link JavaCodeGenerator} for a specific match of a sentence to a Java method.
	 * 
	 * @param match
	 *            the matched sentence
	 * @param qualifiedTypeName
	 *            the qualified name of the type containing the method
	 * @param methodName
	 *            the name of the method
	 * @param qualifiedReturnTypeName
	 *            the qualified name of the method's return type
	 * @param argumentIndexToArgumentMatchMap
	 *            a map from the methods arguments indexes to the parts to the sentence that matched the argument
	 * @param returnVariableName
	 *            the name of the variable the result of the method must be assigned to
	 */
	public JavaCodeGenerator(ISyntaxPatternMatch<?> match, String qualifiedTypeName, String methodName,
			String[] parameterTypes, String qualifiedReturnTypeName,
			Map<Integer, ISyntaxPatternPartMatch> argumentIndexToArgumentMatchMap, String returnVariableName,
			boolean parameterizationCode) {

		super(match, new JavaMatchCodeGeneratorFactory(), JAVA_COMMENT_GENERATOR);
		this.qualifiedTypeName = qualifiedTypeName;
		this.methodName = methodName;
		this.parameterTypes = parameterTypes;
		this.qualifiedReturnTypeName = qualifiedReturnTypeName;
		this.argumentIndexToArgumentMatchMap = argumentIndexToArgumentMatchMap;
		this.returnVariableName = returnVariableName;
		this.parameterizationCode = parameterizationCode;
	}

	@Override
	public boolean isParameterizationCode() {
		return parameterizationCode;
	}

	/**
	 * Returns a Java code fragment that calls the matched method with the appropriate arguments and assigns it to
	 * {@link #returnVariableName}.
	 */
	@Override
	public void generateSourceCode(ICodeGenerationContext context) {
		List<String> argumentList = new ArrayList<String>();

		Set<Integer> indices = argumentIndexToArgumentMatchMap.keySet();
		List<Integer> list = new ArrayList<Integer>(indices);
		Collections.sort(list);
		for (int argumentIndex : list) {
			ISyntaxPatternPartMatch argumentMatch = argumentIndexToArgumentMatchMap.get(argumentIndex);
			String argumentCode = getCode(argumentMatch);
			argumentList.add(argumentCode);
		}
		String arguments = STRING_UTILS.explode(argumentList, ", ");
		String returnAssignment = "";
		String returnVariable = " returnValue" + context.getNextCounter();
		if (returnVariableName != null) {
			returnVariable = " " + returnVariableName;
		}
		if (qualifiedReturnTypeName != null) {
			returnAssignment = qualifiedReturnTypeName + returnVariable + " = ";
		}

		ISyntaxPatternMatch<?> match = getMatch();
		IPatternMatchContext patternMatchContext = match.getContext();
		String instanceName = patternMatchContext.getTemplateFieldByType(qualifiedTypeName);
		if (instanceName == null) {
			instanceName = "supportClassFieldNotAvailable";
		}
		// String simpleTypeName = NameHelper.INSTANCE.getSimpleName(qualifiedTypeName);
		// String instanceName = STRING_UTILS.toFirstLower(simpleTypeName);

		String comment = getComment();
		context.addCode(comment);
		if (context.isLoggingEnabled()) {
			List<Word> words = getMatch().getWords();
			context.addCode("log(\"" + JavaEscaper.INSTANCE.escape(words) + "\");\n");
		}
		context.addCode(returnAssignment + instanceName + "." + methodName + "(");
		context.addCode(arguments);
		context.addCode(");\n");
	}

	public String getQualifiedTypeName() {
		return qualifiedTypeName;
	}

	public String getMethodName() {
		return methodName;
	}

	public String[] getParameterTypes() {
		return parameterTypes;
	}
}
