package de.devboost.natspec.testscripting.java;

import java.util.List;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.testscripting.builder.CodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;

/**
 * The {@link JavaBodyGenerator} is responsible for generating the code that calls the methods which back the sentences
 * in a NatSpec document. In contrast to the {@link JavaCodeGenerator} it does not generate code for a single matched
 * sentence, but for a list of matches. Thus, its output is a list of statements. These statements are inserted in the
 * NatSpec template to replace the &#64;MethodBody place holder.
 */
public class JavaBodyGenerator {

	public static final String GENERATION_SOURCE_STRING = "The code in this method is generated from: ";
	
	public final static JavaBodyGenerator INSTANCE = new JavaBodyGenerator();

	protected JavaBodyGenerator() {
	}

	public ICodeGenerationContext generateBody(INatspecTextResource resource,
			List<ISyntaxPatternMatch<?>> matches, boolean enableLogging, boolean generateParameterizationCode) {

		ICodeGenerationContext codeGenContext = new CodeGenerationContext();
		codeGenContext.setLoggingEnabled(enableLogging);

		// Normally the validation makes sure that multiple matches for the same sentence cause an error. During code
		// generation we would therefore not expect multiple matches. If there are multiple matches nonetheless, we add
		// code for all matches.
		URI uri = resource.getURI();
		String resourceURI = uri.toPlatformString(false);
		addHeaderComments(codeGenContext, resourceURI);

		PrefixBasedCommentGenerator commentGenerator = JavaCodeGenerator.JAVA_COMMENT_GENERATOR;

		// Generate code to execute sentences
		for (ISyntaxPatternMatch<?> match : matches) {
			Object userData = match.getUserData();
			if (userData instanceof ICodeFragment) {
				ICodeFragment fragment = (ICodeFragment) userData;
				boolean isParameterizationCode = fragment.isParameterizationCode();
				if (generateParameterizationCode && isParameterizationCode) {
					fragment.generateSourceCode(codeGenContext);
				}
				if (!generateParameterizationCode && !isParameterizationCode) {
					fragment.generateSourceCode(codeGenContext);
				}
			} else {
				String comment = commentGenerator.getComment("Sentence not matched" + "\n");
				codeGenContext.addCode(comment);
			}
		}
		
		if (matches.isEmpty()) {
			codeGenContext
					.addCode("throw new java.lang.RuntimeException(\"NatSpec specification was empty. No code generated.\");\n");
		} else if (!resource.getErrors().isEmpty()) {
			codeGenContext
					.addCode("throw new java.lang.RuntimeException(\"NatSpec specification contains errors. Generated code is incomplete.\");\n");
		}
		
		return codeGenContext;
	}

	private void addHeaderComments(ICodeGenerationContext codeGenContext, String resourceURI) {

		PrefixBasedCommentGenerator commentGenerator = JavaCodeGenerator.JAVA_COMMENT_GENERATOR;

		String comment1 = commentGenerator.getComment(GENERATION_SOURCE_STRING + resourceURI);

		String comment2 = commentGenerator
				.getComment("Never change this method or any contents of this file, all local changes will be overwritten.");

		String comment3 = commentGenerator.getComment("Change " + JavaNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME
				+ " instead.\n");

		codeGenContext.addCode(comment1);
		codeGenContext.addCode(comment2);
		codeGenContext.addCode(comment3);
	}
}
