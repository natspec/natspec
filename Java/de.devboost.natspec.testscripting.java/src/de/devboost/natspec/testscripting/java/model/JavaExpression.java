package de.devboost.natspec.testscripting.java.model;

/**
 * A {@link JavaExpression} is an abstract AST element that acts as common super
 * type for all kinds of expressions.
 */
public abstract class JavaExpression {
}
