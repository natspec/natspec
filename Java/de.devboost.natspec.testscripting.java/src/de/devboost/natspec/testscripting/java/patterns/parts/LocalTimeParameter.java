package de.devboost.natspec.testscripting.java.patterns.parts;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.ICompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

public class LocalTimeParameter extends AbstractStyleable implements ISyntaxPatternPart, ICompletable {
	
	public final static String TIME_REGEX = "[0-9][0-9]:[0-9][0-9](:[0-9][0-9](\\.([0-9])?([0-9])?([0-9])?([0-9])?([0-9])?([0-9])?([0-9])?([0-9])?([0-9])?)?)?";
	public final static Pattern PATTERN = Pattern.compile(TIME_REGEX);

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		if (words.isEmpty()) {
			return null;
		}

		Word first = words.get(0);
		String text = first.getText();
		if (text == null) {
			return null;
		}
		
		Matcher matcher = PATTERN.matcher(text);
		if (matcher.matches()) {
			return new LocalTimeMatch(Collections.singletonList(first), text);
		}
		
		return null;
	}

	@Override
	public String getCompletionProposal(IPatternMatchContext context) {
		return "12:00";
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	@Override
	public String toSimpleString() {
		return "<LocalTime>";
	}

	@Override
	public boolean isEqualTo(Object object) {
		// Since this class does not have fields, all objects are equal.
		if(object == null || !(object instanceof LocalTimeParameter)){
			return false;
		}
		return super.isEqualTo(object);
	}

	@Override
	public int computeHashCode() {
		return 0;
	}
}
