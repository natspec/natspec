package de.devboost.natspec.testscripting.java.extract;

import de.devboost.natspec.testscripting.patterns.IValidationCallback;

public class SyntaxSpecification {

	private final String text;
	private final IValidationCallback validationCallback;
	private final boolean parameterSyntax;
	
	@Deprecated
	public SyntaxSpecification(String text, IValidationCallback validationCallback) {
		this(text, validationCallback, false);
	}

	public SyntaxSpecification(String text, IValidationCallback validationCallback, boolean parameterSyntax) {
		this.text = text;
		this.validationCallback = validationCallback;
		this.parameterSyntax = parameterSyntax;
	}

	public String getText() {
		return text;
	}

	public IValidationCallback getValidationCallback() {
		return validationCallback;
	}

	public boolean isParameterSyntax() {
		return parameterSyntax;
	}
}
