package de.devboost.natspec.testscripting.java.model;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.testscripting.patterns.IAnnotation;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.IValidationCallback;

/**
 * A {@link JavaAnnotation} is an AST element that represents annotations (i.e.,
 * annotation instances; not annotation types) for {@link JavaMethod}s.
 */
public class JavaAnnotation extends JavaExpression implements IAnnotation {

	/**
	 * The name of the property that is used by single value annotations. See
	 * JLS 9.6.1. (Annotation Type Elements).
	 */
	public final static String ANNOTATION_PROPERTY_NAME = "value";

	private final JavaType annotationType;
	private final List<JavaAssignment> assignments = new ArrayList<JavaAssignment>(1);
	// This field is transient to avoid its serialization when patterns are
	// serialized with XStream
	private final transient IValidationCallback validationResultCallback;

	public JavaAnnotation(JavaType annotationType,
			IValidationCallback validationResultCallback) {
		super();
		this.annotationType = annotationType;
		this.validationResultCallback = validationResultCallback;
	}

	public String getAnnotationTypeName() {
		return annotationType.getQualifiedName();
	}

	public List<JavaAssignment> getAssignments() {
		return assignments;
	}
	
	public IValidationCallback getValidationResultCallback() {
		return validationResultCallback;
	}

	@Override
	public IClass getAnnotationType() {
		return annotationType;
	}
}
