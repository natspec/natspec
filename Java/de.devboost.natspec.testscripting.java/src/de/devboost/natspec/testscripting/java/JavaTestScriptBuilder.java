package de.devboost.natspec.testscripting.java;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;

import com.google.common.io.ByteStreams;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.essentials.StringUtils;
import de.devboost.natspec.Document;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.Word;
import de.devboost.natspec.interfaces.INatSpecDefinitionHandler;
import de.devboost.natspec.interfaces.ISelfDescribingNatSpecTemplate;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.resource.natspec.INatspecBuilder;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.IProperties;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.builder.ITemplateInstantiator;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;

public abstract class JavaTestScriptBuilder implements INatspecBuilder {

	private static final NullProgressMonitor NULL_PROGRESS_MONITOR = new NullProgressMonitor();

	private static final NatspecEclipseProxy NATSPEC_ECLIPSE_PROXY = new NatspecEclipseProxy();

	private static final String ERROR_MESSAGE = "ERROR: No test case template found. Please provide \n"
			+ JavaNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME + "\n" + " in same folder or one of its parents.\n";

	private static final String JAVA_NATURE = "org.eclipse.jdt.core.javanature";

	private static final JDTUtility JDT_UTILITY = new JDTUtility() {

		@Override
		protected void logWarning(String message, Exception e) {
			JavaTestScriptingPlugin.logWarning(message, e);
		}
	};

	@Override
	public IStatus build(NatspecResource resource, IProgressMonitor monitor) {
		// Do nothing it progress is cancelled (e.g., by user who got sick of waiting).
		if (monitor.isCanceled()) {
			return Status.CANCEL_STATUS;
		}

		// register resource to listen for changes
		TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
		if (plugin == null) {
			return Status.OK_STATUS;
		}
		plugin.addTestScript(resource);

		if (NatSpecPlugin.hasValidLicense()) {
			return generateClassForScenario(resource);
		} else {
			// add warning to error log
			TestConnectorPlugin.logWarning("Can't generate NatSpec code without a valid license key.", null);
			return Status.OK_STATUS;
		}
	}

	private IStatus generateClassForScenario(NatspecResource resource) {

		IFile templateFile = getTemplateFile(resource);
		boolean loggingEnabled = false;
		boolean selfDescriptionEnabled = false;
		if (templateFile != null) {
			loggingEnabled = extendsNatspecLogger(templateFile);
			selfDescriptionEnabled = isSelfDescribingTemplate(templateFile);
		}
		Map<String, String> templateFieldTypeToNameMap = getFieldMap(templateFile);

		List<ISyntaxPatternMatch<?>> matches = MatchHelper.INSTANCE.getMatches(resource, templateFieldTypeToNameMap);
		ICodeGenerationContext bodyCodeGenContext = generateBody(resource, matches, loggingEnabled);
		String generatedBodyCode = bodyCodeGenContext.getCode();

		ICodeGenerationContext parameterizationCodeGenContext = generateParameterization(resource, matches);
		String generatedParameterizationCode = parameterizationCodeGenContext.getCode();

		Set<String> imports = new LinkedHashSet<String>();
		imports.addAll(bodyCodeGenContext.getImports());
		imports.addAll(parameterizationCodeGenContext.getImports());

		// Write code to a file
		IFile file = NATSPEC_ECLIPSE_PROXY.getFileForResource(resource);
		URI uri = resource.getURI();
		String filenameWithoutExtension = URI.decode(uri.trimFileExtension().lastSegment());
		saveMatches(matches, filenameWithoutExtension);
		String className = JavaClassNameProvider.INSTANCE.getClassName(filenameWithoutExtension);
		IPath outputFolderPath = getOutputFolderPath(file);

		IPath outputFilePath = outputFolderPath.append(className).addFileExtension(JavaNatSpecConstants.JAVA_EXTENSION);
		IProject project = file.getProject();
		IFile outputFile = project.getFile(outputFilePath);
		String packageName = JDT_UTILITY.getPackageName(outputFile);
		String classCode = getTestCaseCode(resource, templateFile, packageName, className, imports, generatedBodyCode,
				generatedParameterizationCode, selfDescriptionEnabled);
		String charset = getCharset(file);
		saveSafe(classCode, outputFile, charset);

		postGenerateClassForScenario(resource, file, outputFile);

		return Status.OK_STATUS;
	}

	private void saveMatches(List<ISyntaxPatternMatch<?>> matches, String filenameWithoutExtension) {
		if (System.getenv("SAVE_NATSPEC_MATCHES") == null) {
			return;
		}
		
		StringBuilder result = new StringBuilder();
		for (ISyntaxPatternMatch<?> match : matches) {
			if (!match.isComplete()) {
				continue;
			}
			
			result.append("<sentence>\n");
			ISyntaxPattern<?> pattern = match.getPattern();
			Map<?, ?> partToParameterIndexMap = Collections.emptyMap();
			List<?> parameters = new ArrayList<>();
			if (pattern instanceof DynamicSyntaxPattern) {
				DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
				partToParameterIndexMap = dynamicSyntaxPattern.getPartToParameterIndexMap();
				parameters = dynamicSyntaxPattern.getParameters();
			}
			
			Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
			for (Entry<ISyntaxPatternPart, ISyntaxPatternPartMatch> entry : partsToMatchesMap.entrySet()) {
				ISyntaxPatternPart part = entry.getKey();
				Integer index = (Integer) partToParameterIndexMap.get(part);
				String parameterName = null;
				if (index != null) {
					if (index < parameters.size()) {
						Object parameter = parameters.get(index);
						if (parameter instanceof JavaParameter) {
							JavaParameter javaParameter = (JavaParameter) parameter;
							parameterName = javaParameter.getName();
						}
					}
				}
				
				ISyntaxPatternPartMatch partMatch = entry.getValue();
				result.append("\t<part_match>\n");
				List<Word> matchedWords = partMatch.getMatchedWords();
				List<String> parts = WordUtil.INSTANCE.toStringList(matchedWords);
				String allWords = StringUtils.INSTANCE.explode(parts, " ");
				result.append("\t\t<words>");
				result.append(allWords);
				result.append("</words>\n");
				result.append("\t\t<type>");
				result.append(part.getClass().getSimpleName());
				result.append("</type>\n");
				if (parameterName != null) {
					result.append("\t\t<parameter_name>");
					result.append(parameterName);
					result.append("</parameter_name>\n");
				}
				result.append("\t</part_match>\n");
			}
			result.append("</sentence>\n");
		}
		
		try (FileOutputStream fos = new FileOutputStream(System.getProperty("user.home") + File.separator + filenameWithoutExtension + "-natspec-matches.xml")) {
			fos.write(result.toString().getBytes(StandardCharsets.UTF_8));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ICodeGenerationContext generateBody(NatspecResource resource, List<ISyntaxPatternMatch<?>> matches,
			boolean loggingEnabled) {

		JavaBodyGenerator bodyGenerator = JavaBodyGenerator.INSTANCE;
		boolean generateParameterizationCode = false;
		ICodeGenerationContext bodyCodeGenContext = bodyGenerator.generateBody(resource, matches, loggingEnabled, generateParameterizationCode);
		return bodyCodeGenContext;
	}

	private ICodeGenerationContext generateParameterization(NatspecResource resource,
			List<ISyntaxPatternMatch<?>> matches) {
		
		JavaBodyGenerator bodyGenerator = JavaBodyGenerator.INSTANCE;
		boolean generateParameterizationCode = true;
		boolean loggingEnabled = false;
		ICodeGenerationContext bodyCodeGenContext = bodyGenerator.generateBody(resource, matches, loggingEnabled, generateParameterizationCode);
		return bodyCodeGenContext;
	}

	private IPath getOutputFolderPath(IFile file) {
		IProject project = file.getProject();
		IContainer parent = file.getParent();
		IPath outputFolderPath = parent.getProjectRelativePath();
		try {
			String outputPath = project.getPersistentProperty(IProperties.QUALIFIED_PATH_PROPERTY);
			if (outputPath != null) {
				if (outputPath.startsWith("/")) {
					outputFolderPath = project.getFile(new Path(outputPath)).getProjectRelativePath();
				} else {
					outputFolderPath = outputFolderPath.append(outputPath);
				}
			}
		} catch (CoreException e) {
			// ignore. output path is null, which is default.
		}

		return outputFolderPath;
	}

	private String getCharset(IFile file) {
		String charset = null;
		try {
			charset = file.getCharset();
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while determining encoding of NatSpec specification.", e);
		}

		return charset;
	}

	protected abstract Map<String, String> getFieldMap(IFile templateFile);

	protected abstract void postGenerateClassForScenario(NatspecResource resource, IFile file, IFile outputFile);

	private String getTestCaseCode(Resource resource, IFile templateFile, String packageName, String className,
			Set<String> imports, String generatedStatements, String generatedParameterCode,
			boolean selfDescriptionEnabled) {

		if (templateFile == null) {
			return ERROR_MESSAGE;
		}

		String generatedMethods = generateMethods(resource, selfDescriptionEnabled, imports);

		StringBuilder importStatements = new StringBuilder();
		for (String importedClass : imports) {
			importStatements.append("import " + importedClass + ";\n");
		}

		InputStream templateStream = null;
		try {
			templateStream = templateFile.getContents();
			byte[] content = ByteStreams.toByteArray(templateStream);
			String templateContent = new String(content, templateFile.getCharset());

			ITemplateInstantiator templateInstantiator = new JavaTemplateInstantiator();
			return templateInstantiator.instantiateTemplate(templateContent, packageName, importStatements.toString(),
					className, generatedStatements, generatedParameterCode, generatedMethods);

		} catch (IOException e) {
			TestConnectorPlugin.logError("Exception while reading test case template.", e);
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while reading test case template.", e);
		} finally {
			try {
				if (templateStream != null) {
					templateStream.close();
				}
			} catch (IOException e) {
				// Ignore
			}
		}
		return ERROR_MESSAGE;
	}

	private String generateMethods(Resource resource, boolean selfDescriptionEnabled, Set<String> imports) {

		if (!selfDescriptionEnabled) {
			return "";
		}

		// Generate code to register sentences and comments
		List<EObject> contents = resource.getContents();
		if (contents.isEmpty()) {
			return "";
		}

		// Add import for definition handler as it is used as a parameter type
		imports.add(INatSpecDefinitionHandler.class.getName());

		StringBuilder registrationCode = new StringBuilder();
		registrationCode.append("\n\tpublic static void " + ISelfDescribingNatSpecTemplate.METHOD_NAME + "("
				+ INatSpecDefinitionHandler.class.getSimpleName() + " handler) {\n");
		EObject rootObject = contents.get(0);
		if (rootObject instanceof Document) {
			Document document = (Document) rootObject;
			List<Sentence> sentences = document.getContents();
			for (Sentence sentence : sentences) {
				List<Word> words = sentence.getWords();
				if (SentenceUtil.INSTANCE.isComment(sentence)) {
					registrationCode
							.append("\t\thandler.registerComment(\"" + JavaEscaper.INSTANCE.escape(words) + "\");\n");
				} else {
					registrationCode.append("\t\thandler.register(\"" + JavaEscaper.INSTANCE.escape(words) + "\");\n");
				}
			}
		}
		registrationCode.append("\t}\n");

		return registrationCode.toString();
	}

	private IFile getTemplateFile(Resource resource) {
		IFile file = NATSPEC_ECLIPSE_PROXY.getFileForResource(resource);
		IFile templateFile = JavaTemplateFileFinder.INSTANCE.findTemplateFile(file);
		return templateFile;
	}

	protected abstract boolean extendsNatspecLogger(IFile templateFile);

	protected abstract boolean isSelfDescribingTemplate(IFile templateFile);

	private void saveSafe(String code, IFile file, String encoding) {
		try {
			save(file, code, encoding);
		} catch (Exception e) {
			String message = "Exception while saving generated test class.";
			TestConnectorPlugin.logError(message, e);
		}
	}

	private void save(IFile file, String testCode, String encoding)
			throws FileNotFoundException, IOException, CoreException {

		if (!hasChanged(file, testCode, encoding)) {
			return;
		}

		byte[] bytes = getBytes(testCode, encoding);

		prepare(file.getParent());

		InputStream in = new ByteArrayInputStream(bytes);
		if (file.exists()) {
			file.setContents(in, true, false, NULL_PROGRESS_MONITOR);
		} else {
			file.create(in, true, NULL_PROGRESS_MONITOR);
		}
	}

	private byte[] getBytes(String text, String encoding) throws UnsupportedEncodingException {
		byte[] bytes;
		if (encoding == null) {
			bytes = text.getBytes();
		} else {
			bytes = text.getBytes(encoding);
		}
		return bytes;
	}

	public void prepare(IContainer container) throws CoreException {
		if (!(container instanceof IFolder)) {
			return;
		}
		IFolder folder = (IFolder) container;
		if (folder.exists()) {
			return;
		}

		prepare(folder.getParent());
		folder.create(false, false, null);
	}

	private boolean hasChanged(IFile file, String testCode, String encoding) throws CoreException, IOException {
		if (!file.exists()) {
			return true;
		}
		InputStream contents = file.getContents();
		String currentContents = StringUtils.INSTANCE.toString(contents, encoding);
		if (testCode.equals(currentContents)) {
			// do not write file if content has not changed
			return false;
		}

		return true;
	}

	@Override
	public boolean isBuildingNeeded(URI uri) {
		// Only return true if project has Java nature
		IProject project = getProject(uri);
		if (project == null) {
			return false;
		}
		try {
			IProjectNature nature = project.getNature(JAVA_NATURE);
			if (nature != null) {
				return true;
			}
		} catch (CoreException e) {
			// Ignore this
		}

		return false;
	}

	protected IProject getProject(URI uri) {
		IFile file = NATSPEC_ECLIPSE_PROXY.getFileForURI(uri);
		if (file == null) {
			return null;
		}

		IProject project = file.getProject();
		return project;
	}

	@Override
	public IStatus handleDeletion(URI uri, IProgressMonitor monitor) {
		return Status.OK_STATUS;
	}
}
