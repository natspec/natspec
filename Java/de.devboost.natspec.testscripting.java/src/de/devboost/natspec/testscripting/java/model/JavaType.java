package de.devboost.natspec.testscripting.java.model;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.devboost.natspec.patterns.IComparable;
import de.devboost.natspec.testscripting.patterns.AbstractClass;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.util.ComparisonHelper;

/**
 * The class {@link JavaType} is a simple wrapper to represent Java types.
 */
public class JavaType extends AbstractClass implements IComparable {

	/**
	 * The fully qualified name of this type.
	 */
	private String qualifiedName;

	/**
	 * The type arguments for this type if this type is generic.
	 */
	private List<? extends IClass> typeArguments;

	/**
	 * The list of direct super types of this type (includes the super class and all directly implemented interfaces).
	 */
	private List<? extends IClass> superTypes;

	/**
	 * A cache that stores the qualified names of all super types. This includes all direct super types and all indirect
	 * (i.e., transitive) super types. The cache is filled upon first access (the first call to
	 * {@link #getAllSuperTypes()}).
	 */
	private Set<String> allSuperTypeNames;

	public JavaType(Class<?> clazz) {
		this(clazz.getName(), Collections.<JavaType> emptyList(), Collections.<JavaType> emptyList());
	}

	public JavaType(Class<?> clazz, List<JavaType> typeArguments) {
		this(clazz.getName(), typeArguments, Collections.<JavaType> emptyList());
	}

	public JavaType(String qualifiedTypeName) {
		this(qualifiedTypeName, Collections.<JavaType> emptyList(), Collections.<JavaType> emptyList());
	}

	public JavaType(String qualifiedTypeName, List<JavaType> typeArguments) {
		this(qualifiedTypeName, typeArguments, Collections.<JavaType> emptyList());
	}

	/**
	 * Use {@link #JavaType(String, List, List)} instead. Using this constructor will no work as expected, because the
	 * array of sub type names is not used.
	 */
	@Deprecated
	public JavaType(String qualifiedTypeName, String[] expectedTypeNames) {
		this(qualifiedTypeName, expectedTypeNames, Collections
				.<JavaType> emptyList());
	}

	/**
	 * Use {@link #JavaType(String, List, List)} instead. Using this constructor will no work as expected, because the
	 * array of sub type names is not used.
	 */
	@Deprecated
	public JavaType(String qualifiedName, String[] subTypeNames, List<JavaType> typeArguments) {
		this(qualifiedName, typeArguments, Collections.<JavaType> emptyList());
	}

	public JavaType(String qualifiedName, List<JavaType> typeArguments, List<JavaType> superTypes) {
		this.qualifiedName = qualifiedName;
		this.typeArguments = typeArguments;
		// We make the list unmodifiable to make sure this object is not altered
		// after its creation.
		this.superTypes = Collections.unmodifiableList(superTypes);
	}

	public String getQualifiedNameWithTypeArguments() {
		StringBuilder result = new StringBuilder();

		result.append(qualifiedName);
		if (!typeArguments.isEmpty()) {
			String arguments = explode(typeArguments);
			result.append("<");
			result.append(arguments);
			result.append(">");
		}
		return result.toString();
	}

	private String explode(List<? extends IClass> types) {
		StringBuilder result = new StringBuilder();
		int size = types.size();
		for (int i = 0; i < size; i++) {
			IClass type = types.get(i);
			result.append(type.getQualifiedNameWithTypeArguments());
			if (i < size - 1) {
				result.append(",");
			}
		}
		return result.toString();
	}

	/**
	 * Returns all type arguments for this type. If this type is not a generic
	 * type, an empty list is returned.
	 */
	public List<? extends IClass> getTypeArguments() {
		return typeArguments;
	}

	/**
	 * Returns all super types of this type. This includes the types mentioned
	 * in the <code>extends</code> and the <code>implements</code> declaration
	 * (i.e., the super type of this class as well as all directly implemented
	 * interfaces).
	 */
	public List<? extends IClass> getSuperTypes() {
		return superTypes;
	}

	/**
	 * Returns the fully qualified name of this type.
	 */
	public String getQualifiedName() {
		return qualifiedName;
	}

	/**
	 * Returns the fully qualified names of all super types of this type
	 * including the name of the type itself.
	 */
	public Set<String> getAllSuperTypes() {
		if (allSuperTypeNames == null) {
			allSuperTypeNames = computeAllSuperTypes();
		}
		return allSuperTypeNames;
	}

	private Set<String> computeAllSuperTypes() {
		Set<String> allSuperTypeNames = new LinkedHashSet<String>();

		// Contains all types where we've collected the name already. We use
		// this set to make sure we do not visit types twice which may
		// happen when the type hierarchy contains cycles.
		Set<IClass> visitedTypes = new LinkedHashSet<IClass>();

		// Contains all types where we must still collect the name and their
		// super types.
		Set<IClass> typesToVisit = new LinkedHashSet<IClass>();

		// Start with this type and then go up the type hierarchy
		typesToVisit.add(this);
		while (!typesToVisit.isEmpty()) {
			// Pick next (first) type in queue
			IClass next = typesToVisit.iterator().next();
			visitedTypes.add(next);
			typesToVisit.remove(next);
			
			List<? extends IClass> superTypesOfNext = next.getSuperTypes();
			for (IClass superTypeOfNext : superTypesOfNext) {
				// Only visit super types that haven't been seen before
				if (!visitedTypes.contains(superTypeOfNext)) {
					typesToVisit.addAll(superTypesOfNext);
				}
			}
			allSuperTypeNames.add(next.getQualifiedName());
		}
		
		return allSuperTypeNames;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		result.append("JavaType [");
		result.append(getQualifiedNameWithTypeArguments());
		result.append("]");
		return result.toString();
	}

	@Override
	public boolean isEqualTo(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JavaType other = (JavaType) obj;
		if (!ComparisonHelper.INSTANCE.isEqualTo(qualifiedName,
				other.qualifiedName)) {
			return false;
		}
		if (!ComparisonHelper.INSTANCE.isEqualTo(typeArguments,
				other.typeArguments)) {
			return false;
		}
		if (!ComparisonHelper.INSTANCE.isEqualTo(getAllSuperTypes(),
				other.getAllSuperTypes())) {
			return false;
		}
		return true;
	}

	@Override
	public int computeHashCode() {
		Set<String> allSuperTypeNames = getAllSuperTypes();
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((allSuperTypeNames == null) ? 0 : allSuperTypeNames
						.hashCode());
		result = prime * result
				+ ((qualifiedName == null) ? 0 : qualifiedName.hashCode());
		result = prime * result
				+ ((typeArguments == null) ? 0 : typeArguments.hashCode());
		return result;
	}
}
