package de.devboost.natspec.testscripting.java.quickfixes;

import java.util.List;

import de.devboost.natspec.testscripting.java.model.JavaType;

public interface IDerivedMethod {

	public String getName();
	
	public String getCode();

	public List<JavaType> getParameterTypes();
}
