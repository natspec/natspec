package de.devboost.natspec.testscripting.java.patterns.parts;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.devboost.natspec.Word;
import de.devboost.natspec.completion.IMultiCompletable;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.IWordMatcher;
import de.devboost.natspec.matching.WordMatcherFactory;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.styling.AbstractStyleable;

public class EnumParameter extends AbstractStyleable implements ISyntaxPatternPart, IMultiCompletable {

	private final String qualifiedClassName;
	private final Set<String> literals;

	public EnumParameter(String qualifiedClassName, Set<String> literals) {
		this.qualifiedClassName = qualifiedClassName;
		this.literals = literals;
	}

	@Override
	public ISyntaxPatternPartMatch match(List<Word> words, IPatternMatchContext context) {

		if (words.isEmpty()) {
			return null;
		}

		Word first = words.get(0);
		String text = first.getText();
		if (text == null) {
			return null;
		}
		
		// Try to match first word with literal
		for (String literal : literals) {
			// TODO Caching this WordMatcher can improve performance
			IWordMatcher wordMatcher = WordMatcherFactory.INSTANCE.createWordMatcher(literal);
			if (wordMatcher.match(context, text.toLowerCase())) {
				return new EnumMatch(Collections.singletonList(first), qualifiedClassName, literal);
			}
		}

		// Try to match first words with literal (case insensitive)
		for (String literal : literals) {
			String[] parts = literal.split("_");
			if (words.size() >= parts.length) {
				boolean matchedAllParts = true;
				for (int i = 0; i < parts.length; i++) {
					Word word = words.get(i);
					String part = parts[i];
					// TODO Caching this WordMatcher can improve performance
					IWordMatcher wordMatcher = WordMatcherFactory.INSTANCE.createWordMatcher(part);
					if (!wordMatcher.match(context, word.getLowerCaseText())) {
						matchedAllParts = false;
						break;
					}
				}

				if (matchedAllParts) {
					List<Word> matchedWords = words.subList(0, parts.length);
					return new EnumMatch(matchedWords, qualifiedClassName, literal);
				}
			}
		}

		return null;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}

	@Override
	public String toSimpleString() {
		return "<" + qualifiedClassName + ">";
	}

	@Override
	public Set<String> getCompletionProposals(IPatternMatchContext context) {
		return literals;
	}
	
	@Override
	public boolean isEqualTo(Object object) {
		if (object == null || !(object instanceof EnumParameter)) {
			return false;
		}
		
		EnumParameter other = (EnumParameter) object;
		if (!other.literals.equals(literals)) {
			return false;
		}
		if (!other.qualifiedClassName.equals(qualifiedClassName)) {
			return false;
		}
		return super.isEqualTo(object);
	}

	@Override
	public int computeHashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((literals == null) ? 0 : literals.hashCode());
		result = prime * result + ((qualifiedClassName == null) ? 0 : qualifiedClassName.hashCode());
		return result;
	}
}
