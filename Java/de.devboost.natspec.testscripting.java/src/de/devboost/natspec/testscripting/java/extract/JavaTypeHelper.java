package de.devboost.natspec.testscripting.java.extract;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class JavaTypeHelper {

	public final static JavaTypeHelper INSTANCE = new JavaTypeHelper();

	private static final String BIG_DECIMAL_CLASS_NAME = BigDecimal.class.getName();
	private static final String BOOLEAN_CLASS_NAME = boolean.class.getName();
	private static final String BOOLEAN_OBJECT_CLASS_NAME = Boolean.class.getName();
	private static final String DATE_CLASS_NAME = Date.class.getName();
	private static final String LOCAL_TIME_CLASS_NAME = "java.time.LocalTime";
	private static final String LOCAL_DATE_CLASS_NAME = "java.time.LocalDate";
	private static final String LOCAL_DATE_TIME_CLASS_NAME = "java.time.LocalDateTime";
	private static final String DOUBLE_CLASS_NAME = double.class.getName();
	private static final String DOUBLE_OBJECT_CLASS_NAME = Double.class.getName();
	private static final String FLOAT_CLASS_NAME = float.class.getName();
	private static final String FLOAT_OBJECT_CLASS_NAME = Float.class.getName();
	private static final String INTEGER_CLASS_NAME = Integer.class.getName();
	private static final String INT_CLASS_NAME = int.class.getName();
	private static final String LONG_CLASS_NAME = long.class.getName();
	private static final String BYTE_CLASS_NAME = byte.class.getName();
	private static final String CHAR_CLASS_NAME = char.class.getName();
	private static final String LIST_CLASS_NAME = List.class.getName();
	private static final String OBJECT_CLASS_NAME = Object.class.getName();
	private static final String STRING_CLASS_NAME = String.class.getName();

	private JavaTypeHelper() {
	}

	public boolean isDate(String typeName) {
		return DATE_CLASS_NAME.equals(typeName);
	}

	public boolean isObject(String typeName) {
		return OBJECT_CLASS_NAME.equals(typeName);
	}

	public boolean isList(String typeName) {
		return LIST_CLASS_NAME.equals(typeName);
	}

	public boolean isString(String typeName) {
		return STRING_CLASS_NAME.equals(typeName);
	}

	public boolean isFloat(String typeName) {
		return FLOAT_OBJECT_CLASS_NAME.equals(typeName) || FLOAT_CLASS_NAME.equals(typeName);
	}

	public boolean isDouble(String typeName) {
		return DOUBLE_OBJECT_CLASS_NAME.equals(typeName) || DOUBLE_CLASS_NAME.equals(typeName);
	}

	public boolean isInteger(String typeName) {
		return INT_CLASS_NAME.equals(typeName) || INTEGER_CLASS_NAME.equals(typeName);
	}

	public boolean isBoolean(String typeName) {
		return BOOLEAN_CLASS_NAME.equals(typeName) || BOOLEAN_OBJECT_CLASS_NAME.equals(typeName);
	}

	public boolean isBigDecimal(String typeName) {
		return BIG_DECIMAL_CLASS_NAME.equals(typeName);
	}

	public boolean isPrimitive(String typeName) {
		return BYTE_CLASS_NAME.equals(typeName) || CHAR_CLASS_NAME.equals(typeName) || INT_CLASS_NAME.equals(typeName)
				|| LONG_CLASS_NAME.equals(typeName) || FLOAT_CLASS_NAME.equals(typeName)
				|| DOUBLE_CLASS_NAME.equals(typeName) || BOOLEAN_CLASS_NAME.equals(typeName);
	}

	public boolean isLocalTime(String typeName) {
		return LOCAL_TIME_CLASS_NAME.equals(typeName);
	}

	public boolean isLocalDate(String typeName) {
		return LOCAL_DATE_CLASS_NAME.equals(typeName);
	}

	public boolean isLocalDateTime(String typeName) {
		return LOCAL_DATE_TIME_CLASS_NAME.equals(typeName);
	}
}
