package de.devboost.natspec.testscripting.java.extract;

import java.util.Map;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGeneratorFactory;

/**
 * A factory for the {@link JavaCodeGenerator}.
 */
public class JavaCodeGeneratorFactory implements ICodeGeneratorFactory<JavaCodeGenerator> {

	@Override
	public JavaCodeGenerator createCodeGenerator(ISyntaxPatternMatch<JavaCodeGenerator> match, String qualifiedTypeName,
			String methodName, String[] parameterTypes, String qualifiedReturnType,
			Map<Integer, ISyntaxPatternPartMatch> argumentMatches, String returnVariableName,
			boolean parameterizationCode) {

		return new JavaCodeGenerator(match, qualifiedTypeName, methodName, parameterTypes, qualifiedReturnType,
				argumentMatches, returnVariableName, parameterizationCode);
	}
}
