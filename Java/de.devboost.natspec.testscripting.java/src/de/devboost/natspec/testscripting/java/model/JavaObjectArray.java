package de.devboost.natspec.testscripting.java.model;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link JavaObjectArray} is a concrete {@link JavaExpression} containing
 * a list of AST elements.
 */
public class JavaObjectArray extends JavaExpression {

	private List<JavaExpression> contents = new ArrayList<JavaExpression>();

	public List<JavaExpression> getContents() {
		return contents;
	}
}
