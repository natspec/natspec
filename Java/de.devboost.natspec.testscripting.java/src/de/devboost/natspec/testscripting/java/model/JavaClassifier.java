package de.devboost.natspec.testscripting.java.model;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.testscripting.patterns.IValidationCallback;

/**
 * A {@link JavaClassifier} is an AST element that represents a Java class,
 * which can contain {@link JavaMethod}s.
 */
public class JavaClassifier {

	private final String name;
	private final String projectName;
	// This field is transient to avoid its serialization when patterns are
	// serialized with XStream
	private final transient IValidationCallback validationCallback;
	
	private final List<JavaAnnotation> annotations = new ArrayList<JavaAnnotation>();
	private final List<JavaMethod> methods = new ArrayList<JavaMethod>();
	private final boolean isInterface;

	/**
	 * Creates a new {@link JavaClassifier} with the given name that is located in the project with the given name.
	 */
	public JavaClassifier(String name, String projectName, IValidationCallback validationCallback, boolean isInterface) {
		super();
		this.name = name;
		this.projectName = projectName;
		this.validationCallback = validationCallback;
		this.isInterface = isInterface;
	}
	
	public List<JavaAnnotation> getAnnotations() {
		return annotations;
	}

	public List<JavaMethod> getMethods() {
		return methods;
	}

	public String getName() {
		return name;
	}

	public String getProjectName() {
		return projectName;
	}

	public IValidationCallback getValidationCallback() {
		return validationCallback;
	}

	public boolean isInterface() {
		return isInterface;
	}
}
