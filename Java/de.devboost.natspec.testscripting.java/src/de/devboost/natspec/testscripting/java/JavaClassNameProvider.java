package de.devboost.natspec.testscripting.java;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.common.util.URI;

import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;

/**
 * The {@link JavaClassNameProvider} can be used to derive the names for the Java classes that are generated from
 * NatSpec files. It replaces characters which are not allowed to be used in Java identifiers with escape sequences.
 */
public class JavaClassNameProvider {
	
	public final static JavaClassNameProvider INSTANCE = new JavaClassNameProvider();
	
	private static final String NATSPEC_EXTENSION = new NatspecMetaInformation().getSyntaxName();
	private static final Pattern ESCAPE_PATTERN = Pattern.compile("_[0-9]+_");

	private JavaClassNameProvider() {
	}

	public String getClassName(URI uri) {
		String fileNameWithoutExtension = uri.trimFileExtension().lastSegment();
		return getClassName(fileNameWithoutExtension);
	}

	public String getClassName(String fileNameWithoutExtension) {
		return escape(fileNameWithoutExtension);
	}

	public String getNatSpecFileName(String javaFileName) {
		String result = javaFileName;
		if (result.endsWith(".java")) {
			result = result.substring(0, result.length() - ".java".length());
			result = unescape(result);
			result += "." + NATSPEC_EXTENSION;
		}
		return result;
	}

	private String escape(String text) {
		StringBuilder result = new StringBuilder();
		char[] charArray = text.toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			char next = charArray[i];
			boolean isAllowed;
			if (i == 0) {
				isAllowed = Character.isJavaIdentifierStart(next);
			} else {
				isAllowed = Character.isJavaIdentifierPart(next);
			}
			
			if (isAllowed) {
				result.append(next);
			} else {
				result.append("_" + Character.codePointAt(charArray, i) + "_");
			}
		}
		return result.toString();
	}

	private String unescape(String text) {
		
		StringBuilder result = new StringBuilder();
		Matcher matcher = ESCAPE_PATTERN.matcher(text);
		int lastEnd = 0;
		while (matcher.find()) {
			int start = matcher.start();
			int end = matcher.end();
			String before = text.substring(lastEnd, start);
			result.append(before);
			
			String escapedCharacter = text.substring(start + 1, end - 1);
			int codePoint = Integer.parseInt(escapedCharacter);
			char[] chars = Character.toChars(codePoint);
			result.append(chars);
			lastEnd = end;
		}

		String tail = text.substring(lastEnd, text.length());
		result.append(tail);

		return result.toString();
	}
}
