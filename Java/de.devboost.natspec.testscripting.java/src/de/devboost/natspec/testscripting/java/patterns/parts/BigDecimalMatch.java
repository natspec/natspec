package de.devboost.natspec.testscripting.java.patterns.parts;

import java.math.BigDecimal;
import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

public class BigDecimalMatch extends AbstractSyntaxPatternPartMatch {

	private final BigDecimal value;

	public BigDecimalMatch(List<Word> words, BigDecimal value) {
		super(words);
		this.value = value;
	}
	
	public BigDecimal getValue() {
		return value;
	}
}
