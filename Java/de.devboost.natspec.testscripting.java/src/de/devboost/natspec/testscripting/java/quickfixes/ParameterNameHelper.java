package de.devboost.natspec.testscripting.java.quickfixes;

import java.util.Arrays;
import java.util.List;

import com.google.common.base.CaseFormat;
import com.google.common.base.CharMatcher;

/**
 * The {@link ParameterNameHelper} can be used to obtain names for parameters
 * and makes sure that no name is used twice.
 */
public class ParameterNameHelper {

    private static final String JAVA_KEYWORDS[] = { "abstract", "assert", "boolean",
        "break", "byte", "case", "catch", "char", "class", "const",
        "continue", "default", "do", "double", "else", "extends", "false",
        "final", "finally", "float", "for", "goto", "if", "implements",
        "import", "instanceof", "int", "interface", "long", "native",
        "new", "null", "package", "private", "protected", "public",
        "return", "short", "static", "strictfp", "super", "switch",
        "synchronized", "this", "throw", "throws", "transient", "true",
        "try", "void", "volatile", "while" };

	public boolean isJavaKeyword(String keyword) {
		return (Arrays.binarySearch(JAVA_KEYWORDS, keyword) >= 0);
	}

	/**
	 * Returns a name for a parameter that is based in the given name. If the
	 * given name was not used yet, it used returned as it is. If not, indices
	 * starting from 2 are appended to the name.
	 */
	public String getNewParameterName(List<DerivedMethodPart> parts, String name) {
		name = normalizeNameToLowerCamel(name);
		
		String finalName = getValidIdentifier(name);

		boolean parameterExists = parameterExists(parts, finalName);
		if (!parameterExists) {
			return finalName;
		}

		int index = 2;
		do {
			finalName = finalName + index;
			index++;
		} while (parameterExists(parts, finalName));

		return finalName;
	}
	
	/**
	 * Converts the name to lower camelCase notation.
	 * 
	 * @param name
	 *            the name to convert.
	 * @return name in lower camelCase
	 */
	protected String normalizeNameToLowerCamel(String name) {
		boolean isUpperUnderscore = CharMatcher.JAVA_UPPER_CASE.or(CharMatcher.is('_')).matchesAllOf(name);
		
		if (isUpperUnderscore) {
			return CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, name);
		}
		
		return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_CAMEL, name);
	}

	/**
	 * Returns a valid Java identifier that is similar to the given text. All
	 * invalid characters are removed from the text. If not valid character is
	 * left, an underscore is returned.
	 * 
	 * @param text the text to convert.
	 * @return a valid Java identifier
	 */
	public String getValidIdentifier(String text) {
		StringBuilder identifier = new StringBuilder();
		for (int i = 0; i < text.length(); i++) {
			char nextChar = text.charAt(i);
			if (i == 0) {
				if (Character.isJavaIdentifierStart(nextChar)) {
					identifier.append(nextChar);
					continue;
				} else {
					identifier.append("_");
				}
			}
			
			if (Character.isJavaIdentifierPart(nextChar)) {
				identifier.append(nextChar);
				continue;
			}
		}
		
		if (identifier.length() == 0) {
			return "_";
		}
		
		String identifierString = identifier.toString();
		return identifierString;
	}

	/**
	 * Checks whether the given name was already used as parameter name.
	 */
	private boolean parameterExists(List<DerivedMethodPart> parts, String name) {
		for (DerivedMethodPart part : parts) {
			if (name.equals(part.getParameterNamePart())) {
				return true;
			}
		}
		return false;
	}
}
