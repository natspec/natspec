package de.devboost.natspec.testscripting.java;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;

import de.devboost.natspec.testscripting.TestConnectorPlugin;

public class JavaTestScriptingPlugin extends Plugin {

	public static final String PLUGIN_ID = "de.devboost.natspec.testscripting";

	private static JavaTestScriptingPlugin instance;

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		instance = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		instance = null;
		super.stop(context);
	}

	public static JavaTestScriptingPlugin getInstance() {
		return instance;
	}

	// TODO the log methods are copied from JLoopPlugin
	/**
	 * Helper method for error logging.
	 * 
	 * @param message
	 *            the error message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	public static IStatus logError(String message, Throwable throwable) {
		return log(IStatus.ERROR, message, throwable);
	}

	/**
	 * Helper method for logging informations.
	 * 
	 * @param message
	 *            the information message to log
	 * @param throwable
	 *            the exception that describes the information in detail (can be null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logInfo(String message, Throwable throwable) {
		return log(IStatus.INFO, message, throwable);
	}

	/**
	 * Helper method for logging warnings.
	 * 
	 * @param message
	 *            the warning message to log
	 * @param throwable
	 *            the exception that describes the warning in detail (can be null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logWarning(String message, Throwable throwable) {
		return log(IStatus.WARNING, message, throwable);
	}

	/**
	 * Helper method for logging.
	 * 
	 * @param type
	 *            the type of the message to log
	 * @param message
	 *            the message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	protected static IStatus log(int type, String message, Throwable throwable) {
		IStatus status;
		if (throwable != null) {
			status = new Status(type, PLUGIN_ID, 0, message, throwable);
		} else {
			status = new Status(type, PLUGIN_ID, message);
		}
		
		final TestConnectorPlugin pluginInstance = TestConnectorPlugin.getInstance();
		if (pluginInstance == null) {
			System.err.println(message);
			if (throwable != null) {
				throwable.printStackTrace();
			}
		} else {
			pluginInstance.getLog().log(status);
		}
		return status;
	}
}
