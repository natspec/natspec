package de.devboost.natspec.testscripting.java;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.WordUtil;

public class JavaEscaper {

	public final static JavaEscaper INSTANCE = new JavaEscaper();
	
	private JavaEscaper() {
		super();
	}

	public String escape(List<Word> words) {
		String sentence = WordUtil.INSTANCE.toString(words);
		return escape(sentence);
	}

	public String escape(String sentence) {
		// TODO Is this always sufficient?
		return sentence.replace("\\", "\\\\").replace("\"", "\\\"");
	}
}
