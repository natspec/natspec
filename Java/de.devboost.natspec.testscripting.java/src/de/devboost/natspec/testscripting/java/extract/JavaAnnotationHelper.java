package de.devboost.natspec.testscripting.java.extract;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.annotations.BooleanSyntax;
import de.devboost.natspec.annotations.Many;
import de.devboost.natspec.annotations.NameBasedSyntax;
import de.devboost.natspec.annotations.Optional;
import de.devboost.natspec.annotations.StyledTextSyntax;
import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;
import de.devboost.natspec.annotations.This;
import de.devboost.natspec.annotations.parameters.ParameterSyntax;
import de.devboost.natspec.annotations.rules.AllowedSyntaxPattern;
import de.devboost.natspec.annotations.rules.AllowedSyntaxPatterns;
import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaAssignment;
import de.devboost.natspec.testscripting.java.model.JavaExpression;
import de.devboost.natspec.testscripting.java.model.JavaObjectArray;
import de.devboost.natspec.testscripting.java.model.JavaStringLiteral;
import de.devboost.natspec.testscripting.patterns.IAnnotation;
import de.devboost.natspec.testscripting.patterns.IClass;

public class JavaAnnotationHelper {
	
	public final static JavaAnnotationHelper INSTANCE = new JavaAnnotationHelper();
	
	private static final String TEXT_SYNTAXES_ANNOTATION_SIMPLE_NAME = TextSyntaxes.class.getSimpleName();

	private static final String TEXT_SYNTAXES_ANNOTATION_NAME = TextSyntaxes.class.getName();

	private static final String TEXT_SYNTAX_ANNOTATION_SIMPLE_NAME = TextSyntax.class.getSimpleName();

	private static final String TEXT_SYNTAX_ANNOTATION_NAME = TextSyntax.class.getName();
	
	private static final String NAME_BASED_SYNTAX_ANNOTATION_SIMPLE_NAME = NameBasedSyntax.class.getSimpleName();

	private static final String NAME_BASED_SYNTAX_ANNOTATION_NAME = NameBasedSyntax.class.getName();
	
	private static final String SYNTAX_RULES_ANNOTATION_SIMPLE_NAME = AllowedSyntaxPatterns.class.getSimpleName();

	private static final String SYNTAX_RULES_ANNOTATION_NAME = AllowedSyntaxPatterns.class.getName();
	
	private static final String ALLOWED_SYNTAX_PATTERN_ANNOTATION_SIMPLE_NAME = AllowedSyntaxPattern.class.getSimpleName();

	private static final String ALLOWED_SYNTAX_PATTERN_ANNOTATION_NAME = AllowedSyntaxPattern.class.getName();
	
	private static final String STYLED_TEXT_SYNTAX_ANNOTATION_SIMPLE_NAME = StyledTextSyntax.class.getSimpleName();
	
	private static final String STYLED_TEXT_SYNTAX_ANNOTATION_NAME = StyledTextSyntax.class.getName();
	
	private static final String PARAMETER_SYNTAX_ANNOTATION_SIMPLE_NAME = ParameterSyntax.class.getSimpleName();
	
	private static final String PARAMETER_SYNTAX_ANNOTATION_NAME = ParameterSyntax.class.getName();
	
	private static final Object BOOLEAN_SYNTAX_ANNOTATION_NAME = BooleanSyntax.class.getName();

	private static final Object BOOLEAN_SYNTAX_ANNOTATION_SIMPLE_NAME = BooleanSyntax.class.getSimpleName();

	private static final Object OPTIONAL_ANNOTATION_NAME = Optional.class.getName();

	private static final Object OPTIONAL_ANNOTATION_SIMPLE_NAME = Optional.class.getSimpleName();

	private static final Object MANY_ANNOTATION_NAME = Many.class.getName();

	private static final Object MANY_ANNOTATION_SIMPLE_NAME = Many.class.getSimpleName();

	private static final Object THIS_ANNOTATION_NAME = This.class.getName();

	private static final Object THIS_ANNOTATION_SIMPLE_NAME = This.class.getSimpleName();

	private JavaAnnotationHelper() {
	}

	/**
	 * Returns <code>true</code> if the given list of annotations contains at least one @This annotation.
	 */
	public boolean hasThisAnnotation(List<IAnnotation> annotations) {
		for (IAnnotation annotation : annotations) {
			if (annotation instanceof JavaAnnotation) {
				JavaAnnotation javaAnnotation = (JavaAnnotation) annotation;
				if (isThisAnnotation(javaAnnotation)) {
					return true;
				}
			}
		}
		
		return false;
	}

	/**
	 * Returns <code>true</code> if the given list of annotations contains at least one @Optional annotation.
	 */
	public boolean hasOptionalAnnotation(List<IAnnotation> annotations) {
		for (IAnnotation annotation : annotations) {
			if (annotation instanceof JavaAnnotation) {
				JavaAnnotation javaAnnotation = (JavaAnnotation) annotation;
				if (isOptionalAnnotation(javaAnnotation)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Returns <code>true</code> if the given annotation is an @Optional annotation.
	 */
	public boolean isOptionalAnnotation(JavaAnnotation javaAnnotation) {
		IClass annotationType = javaAnnotation.getAnnotationType();
		String qualifiedName = annotationType.getQualifiedName();
		if (isOptionalAnnotation(qualifiedName)) {
			return true;
		}
		
		return false;
	}

	/**
	 * Returns <code>true</code> if the given annotation is an This annotation.
	 */
	public boolean isThisAnnotation(JavaAnnotation javaAnnotation) {
		IClass annotationType = javaAnnotation.getAnnotationType();
		String qualifiedName = annotationType.getQualifiedName();
		if (isThisAnnotation(qualifiedName)) {
			return true;
		}
		
		return false;
	}

	/**
	 * Returns the first @BooleanSyntax annotation that is found in the given list of annotations. If no such annotation
	 * is contained in the list, <code>null</code> is returned.
	 */
	public JavaAnnotation findBooleanSyntaxAnnotation(List<IAnnotation> annotations) {
		return JavaAnnotationHelper.INSTANCE.findAnnotation(annotations, BooleanSyntax.class);
	}

	/**
	 * Returns the first annotation that is an instance of the given annotation class and which is found in the given
	 * list of annotations. If no matching annotation is contained in the list, <code>null</code> is returned.
	 */
	public JavaAnnotation findAnnotation(List<? extends IAnnotation> annotations, Class<?> annotationClass) {
		
		String simpleAnnotationName = annotationClass.getSimpleName();
		String qualifiedAnnotationName = annotationClass.getName();
		
		for (IAnnotation annotation : annotations) {
			if (!(annotation instanceof JavaAnnotation)) {
				continue;
			}
			
			JavaAnnotation javaAnnotation = (JavaAnnotation) annotation;
			IClass annotationType = javaAnnotation.getAnnotationType();
			String name = annotationType.getQualifiedName();
			if (simpleAnnotationName.equals(name)) {
				return javaAnnotation;
			}
			if (qualifiedAnnotationName.equals(name)) {
				return javaAnnotation;
			}
		}
		
		// Annotation was not found.
		return null;
	}
	
	public boolean isTextSyntaxAnnotation(String annotationTypeName) {
		return TEXT_SYNTAX_ANNOTATION_NAME.equals(annotationTypeName) ||
			TEXT_SYNTAX_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName) ||
			TEXT_SYNTAXES_ANNOTATION_NAME.equals(annotationTypeName) ||
			TEXT_SYNTAXES_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}

	public boolean isNameBasedSyntaxAnnotation(String annotationTypeName) {
		return NAME_BASED_SYNTAX_ANNOTATION_NAME.equals(annotationTypeName) ||
			NAME_BASED_SYNTAX_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}
	
	public boolean isStyledTextSyntaxAnnotation(String annotationTypeName) {
		return STYLED_TEXT_SYNTAX_ANNOTATION_NAME.equals(annotationTypeName)
				|| STYLED_TEXT_SYNTAX_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}
	
	public boolean isParameterSyntaxAnnotation(String annotationTypeName) {
		return PARAMETER_SYNTAX_ANNOTATION_NAME.equals(annotationTypeName)
				|| PARAMETER_SYNTAX_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}

	public boolean isSyntaxRulesAnnotation(String annotationTypeName) {
		return SYNTAX_RULES_ANNOTATION_NAME.equals(annotationTypeName) ||
				SYNTAX_RULES_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}

	public boolean isAllowedSyntaxPatternAnnotation(String annotationTypeName) {
		return ALLOWED_SYNTAX_PATTERN_ANNOTATION_NAME.equals(annotationTypeName) ||
				ALLOWED_SYNTAX_PATTERN_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}

	public boolean isRelevantTypeAnnotation(String annotationTypeName) {
		if (isNameBasedSyntaxAnnotation(annotationTypeName)) {
			return true;
		}
		if (isSyntaxRulesAnnotation(annotationTypeName)) {
			return true;
		}
		if (isAllowedSyntaxPatternAnnotation(annotationTypeName)) {
			return true;
		}
		return false;
	}
	
	public boolean isOptionalAnnotation(String annotationTypeName) {
		return OPTIONAL_ANNOTATION_NAME.equals(annotationTypeName) ||
				OPTIONAL_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}

	public boolean isThisAnnotation(String annotationTypeName) {
		return THIS_ANNOTATION_NAME.equals(annotationTypeName) ||
				THIS_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}

	/**
	 * Returns <code>true</code> if the given annotation is an @BooleanSyntax annotation.
	 */
	public boolean isBooleanSyntaxAnnotation(IAnnotation annotation) {
		IClass annotationType = annotation.getAnnotationType();
		return isBooleanSyntaxAnnotation(annotationType.getQualifiedName());
	}

	/**
	 * Returns <code>true</code> if the given annotation type is the @BooleanSyntax annotation.
	 */
	public boolean isBooleanSyntaxAnnotation(String annotationTypeName) {
		return BOOLEAN_SYNTAX_ANNOTATION_NAME.equals(annotationTypeName) ||
				BOOLEAN_SYNTAX_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}

	/**
	 * Returns <code>true</code> if the given annotation is an @Many annotation.
	 */
	public boolean isManySyntaxAnnotation(IAnnotation annotation) {
		IClass annotationType = annotation.getAnnotationType();
		return isManySyntaxAnnotation(annotationType.getQualifiedName());
	}

	/**
	 * Returns <code>true</code> if the given annotation type is the @Many annotation.
	 */
	public boolean isManySyntaxAnnotation(String annotationTypeName) {
		return MANY_ANNOTATION_NAME.equals(annotationTypeName) ||
				MANY_ANNOTATION_SIMPLE_NAME.equals(annotationTypeName);
	}
	
	/**
	 * Iterates over the given list of annotations and returns the first @Many annotation.
	 * 
	 * @param annotations
	 *            the list of annotations to search in
	 * @return the first @Many annotation or <code>null</code> if none is found
	 */
	public JavaAnnotation findManyAnnotation(List<IAnnotation> annotations) {
		for (IAnnotation annotation : annotations) {
			if (isManySyntaxAnnotation(annotation)) {
				if (annotation instanceof JavaAnnotation) {
					JavaAnnotation javaAnnotation = (JavaAnnotation) annotation;
					return javaAnnotation;
				}
			}
		}
		return null;
	}
	
	/**
	 * Returns all values that are assigned to the value field of the annotation
	 * that holds the given assignment.
	 * 
	 * @param assignment an assignment which is part of a {@link JavaAnnotation}
	 * @return the list of values.
	 */
	public List<String> getValues(JavaAssignment assignment) {
		List<String> values = new ArrayList<String>();
		
		JavaExpression value = assignment.getValue();
		if (value instanceof JavaStringLiteral) {
			JavaStringLiteral stringLiteral = (JavaStringLiteral) value;
			values.add(stringLiteral.getValue());
		}
		
		if (value instanceof JavaObjectArray) {
			JavaObjectArray javaObjectArray = (JavaObjectArray) value;
			List<JavaExpression> contents = javaObjectArray.getContents();
			for (JavaExpression object : contents) {
				if (object instanceof JavaAnnotation) {
					JavaAnnotation javaAnnotation = (JavaAnnotation) object;
					List<JavaAssignment> assignments = javaAnnotation.getAssignments();
					for (JavaAssignment javaAssignment : assignments) {
						values.addAll(getValues(javaAssignment));
					}
				} else if (object instanceof JavaStringLiteral) {
					JavaStringLiteral javaStringLiteral = (JavaStringLiteral) object;
					values.add(javaStringLiteral.getValue());
				}
			}
		}
		
		return values;
	}

	public List<String> getValues(JavaAnnotation annotation, String propertyName) {
		List<String> values = new ArrayList<String>();
		
		List<JavaAssignment> assignments = annotation.getAssignments();
		for (JavaAssignment assignment : assignments) {
			if (!propertyName.equals(assignment.getKey())) {
				continue;
			}
			
			values.addAll(getValues(assignment));
		}
		
		return values;
	}

}
