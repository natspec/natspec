package de.devboost.natspec.testscripting.java.model;

/**
 * A {@link JavaAssignment} is an AST element that represents an assignment from
 * a {@link JavaExpression} to a variable.
 */
public class JavaAssignment {

	private final String key;
	private final JavaExpression value;

	public JavaAssignment(String key, JavaExpression value) {
		super();
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public JavaExpression getValue() {
		return value;
	}
}
