package de.devboost.natspec.testscripting.java.extract;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.print.attribute.TextSyntax;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import com.google.common.base.Joiner;

import de.devboost.natspec.annotations.NameBasedSyntax;
import de.devboost.natspec.annotations.StyledTextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;
import de.devboost.natspec.annotations.rules.AllowedSyntaxPattern;
import de.devboost.natspec.annotations.rules.AllowedSyntaxPatterns;
import de.devboost.natspec.annotations.styling.Bold;
import de.devboost.natspec.annotations.styling.Color;
import de.devboost.natspec.annotations.styling.Italic;
import de.devboost.natspec.annotations.styling.Strikethrough;
import de.devboost.natspec.annotations.styling.Underline;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.styling.Style;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaAssignment;
import de.devboost.natspec.testscripting.java.model.JavaClassifier;
import de.devboost.natspec.testscripting.java.model.JavaExpression;
import de.devboost.natspec.testscripting.java.model.JavaMethod;
import de.devboost.natspec.testscripting.java.model.JavaModifier;
import de.devboost.natspec.testscripting.java.model.JavaObjectArray;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.IValidationCallback;
import de.devboost.natspec.testscripting.patterns.PatternParser;

/**
 * The {@link JavaModelPatternExtractor} can be used to extract NatSpec syntax
 * patterns from a Java model. The {@link JavaModelPatternExtractor} searches
 * for {@link TextSyntax}, {@link TextSyntaxes}, {@link NameBasedSyntax} and {@link StyledTextSyntax}
 * annotations and converts them to {@link ISyntaxPattern}s.
 */
public class JavaModelPatternExtractor {
	
	private static final JavaAnnotationHelper JAVA_ANNOTATION_HELPER = JavaAnnotationHelper.INSTANCE;

	public final static JavaModelPatternExtractor INSTANCE = new JavaModelPatternExtractor();

	private final JavaParameterFactory PARAMETER_FACTORY = new JavaParameterFactory();
	private final JavaCodeGeneratorFactory CODE_GENERATOR_FACTORY = new JavaCodeGeneratorFactory();
	private final PatternParser<JavaCodeGenerator> PATTERN_PARSER = new PatternParser<JavaCodeGenerator>(PARAMETER_FACTORY, CODE_GENERATOR_FACTORY);

	// This is a singleton
	private JavaModelPatternExtractor() {
		super();
	}
	
	/**
	 * Extracts all syntax patterns from the given classifier.
	 */
	public List<ISyntaxPattern<?>> extractPatterns(JavaClassifier classifier) {
		
		IValidationCallback callback = classifier.getValidationCallback();
		callback.beginValidation();
		
		List<Pattern> allowedPatterns = getAllowedSyntaxPatterns(classifier.getAnnotations());

		List<ISyntaxPattern<?>> extractedPatterns = new ArrayList<ISyntaxPattern<?>>();
		
		extractPatterns(classifier, extractedPatterns, allowedPatterns);

		List<JavaMethod> methods = classifier.getMethods();
		for (JavaMethod method : methods) {
			extractPatterns(classifier, method, extractedPatterns, allowedPatterns);
		}

		return extractedPatterns;
	}

	/**
	 * Extracts all patterns from the given method and add them to the patterns collection.
	 */
	private void extractPatterns(JavaClassifier classifier, JavaMethod method,
			Collection<ISyntaxPattern<?>> patterns, List<Pattern> allowedPatterns) {
		
		JavaAnnotationHelper javaAnnotationHelper = JAVA_ANNOTATION_HELPER;

		boolean createdPattern = false;
		List<JavaAnnotation> annotations = method.getAnnotations();
		for (JavaAnnotation annotation : annotations) {
			String annotationTypeName = annotation.getAnnotationTypeName();
			
			boolean isTextSyntax = javaAnnotationHelper.isTextSyntaxAnnotation(annotationTypeName);
			boolean isNameBasedSyntax = javaAnnotationHelper.isNameBasedSyntaxAnnotation(annotationTypeName);
			boolean isStyledTextSyntax = javaAnnotationHelper.isStyledTextSyntaxAnnotation(annotationTypeName);
			boolean isParameterSyntax = javaAnnotationHelper.isParameterSyntaxAnnotation(annotationTypeName);
			if (!isTextSyntax && !isNameBasedSyntax && !isStyledTextSyntax && !isParameterSyntax) {
				continue;
			}

			List<SyntaxSpecification> syntaxSpecifications;
			if (isTextSyntax || isStyledTextSyntax || isParameterSyntax) {
				List<JavaAssignment> assignments = annotation.getAssignments();
				if (assignments.size() != 1) {
					continue;
				}
	
				JavaAssignment assignment = assignments.get(0);
				if (!JavaAnnotation.ANNOTATION_PROPERTY_NAME.equals(assignment.getKey())) {
					continue;
				}
				
				List<String> texts = javaAnnotationHelper.getValues(assignment);
				syntaxSpecifications = new ArrayList<SyntaxSpecification>(texts.size());
				for (String text : texts) {
					IValidationCallback validationResultCallback = annotation.getValidationResultCallback();
					SyntaxSpecification syntaxSpecification = new SyntaxSpecification(text, validationResultCallback, isParameterSyntax);
					syntaxSpecifications.add(syntaxSpecification);
				}
			} else {
				syntaxSpecifications = getSyntaxSpecifications(method);
			}
			
			boolean allSyntaxPatternsAllowed = checkAllowedSyntaxPatterns(syntaxSpecifications, allowedPatterns);
			if (allSyntaxPatternsAllowed) {
				parsePatterns(classifier, method, syntaxSpecifications, annotation, patterns);
				createdPattern = true;
			}
		}
		
		if (createdPattern) {
			checkMethodIsPublic(classifier, method);
		}
	}

	private void checkMethodIsPublic(JavaClassifier classifier, JavaMethod method) {
		boolean isInterface = classifier.isInterface();
		if (isInterface) {
			// For interfaces we assume that the Java compiler has checked that all methods are public
			return;
		}
		
		JavaModifier modifier = method.getModifier();
		if (modifier != JavaModifier.PUBLIC && modifier != JavaModifier.PACKAGE_PROTECTED) {
			IValidationCallback validationCallback = method.getValidationResultCallback();
			validationCallback.addWarning("NatSpec support methods should be public.");
		}
	}
	
	/**
	 * Extracts all patterns from the given classifier and adds them to the patterns collection.
	 * 
	 * @param allowedPatterns 
	 */
	private void extractPatterns(JavaClassifier classifier, Collection<ISyntaxPattern<?>> patterns,
			List<Pattern> allowedPatterns) {
		
		List<JavaAnnotation> annotations = classifier.getAnnotations();

		for (JavaAnnotation annotation : annotations) {
			String annotationTypeName = annotation.getAnnotationTypeName();
			if (!JAVA_ANNOTATION_HELPER.isNameBasedSyntaxAnnotation(annotationTypeName)) {
				continue;
			}
			
			for (JavaMethod method : classifier.getMethods()) {
				checkMethodIsPublic(classifier, method);
				if (method.getModifier() != JavaModifier.PUBLIC) {
					continue;
				}
				
				List<SyntaxSpecification> syntaxSpecifications = getSyntaxSpecifications(method);
				boolean allSyntaxPatternsAllowed = checkAllowedSyntaxPatterns(syntaxSpecifications, allowedPatterns);
				if (allSyntaxPatternsAllowed) {
					parsePatterns(classifier, method, syntaxSpecifications, annotation, patterns);
				}
			}
		}
	}
	
	private boolean checkAllowedSyntaxPatterns(List<SyntaxSpecification> syntaxSpecifications,
			List<Pattern> allowedPatterns) {
		
		if (allowedPatterns.isEmpty()) {
			return true;
		}
		
		for (SyntaxSpecification syntaxSpecification : syntaxSpecifications) {
			String text = syntaxSpecification.getText();
			boolean foundAllowedPattern = false;
			for (Pattern allowedPattern : allowedPatterns) {
				Matcher matcher = allowedPattern.matcher(text);
				if (matcher.matches()) {
					foundAllowedPattern = true;
					break;
				}
			}
			
			if (!foundAllowedPattern) {
				syntaxSpecification.getValidationCallback().addError("Pattern is not allowed (see @" + AllowedSyntaxPattern.class.getSimpleName() + " annotation).");
				return false;
			}
		}
		
		return true;
	}

	private List<Pattern> getAllowedSyntaxPatterns(List<JavaAnnotation> annotations) {
		Set<String> allowedPatterns = new LinkedHashSet<String>();
		allowedPatterns.addAll(getSyntaxRulesAllowedPatterns(annotations));
		allowedPatterns.addAll(getAllowedPatterns(annotations));
		
		List<Pattern> compiledPatterns = new ArrayList<Pattern>(allowedPatterns.size());
		for (String allowedPattern : allowedPatterns) {
			compiledPatterns.add(Pattern.compile(allowedPattern, Pattern.CASE_INSENSITIVE));
		}
		
		return compiledPatterns;
	}

	private Set<String> getSyntaxRulesAllowedPatterns(List<JavaAnnotation> annotations) {
		JavaAnnotation syntaxRulesAnnotation = JAVA_ANNOTATION_HELPER.findAnnotation(annotations, AllowedSyntaxPatterns.class);
		if (syntaxRulesAnnotation == null) {
			return Collections.emptySet();
		}
		
		Set<String> allowedPatterns = new LinkedHashSet<String>();
		List<JavaAssignment> assignments = syntaxRulesAnnotation.getAssignments();
		for (JavaAssignment javaAssignment : assignments) {
			JavaExpression expression = javaAssignment.getValue();
			if (expression instanceof JavaObjectArray) {
				JavaObjectArray javaObjectArray = (JavaObjectArray) expression;
				List<JavaExpression> contents = javaObjectArray.getContents();
				for (JavaExpression javaExpression : contents) {
					if (javaExpression instanceof JavaAnnotation) {
						JavaAnnotation subAnnotation = (JavaAnnotation) javaExpression;
						if (subAnnotation.getAnnotationTypeName().equals(AllowedSyntaxPattern.class.getSimpleName())) {
							List<String> values = JAVA_ANNOTATION_HELPER.getValues(subAnnotation, "value");
							allowedPatterns.addAll(values);
						}
					}
				}
			}
		}
		
		return allowedPatterns;
	}

	private Set<String> getAllowedPatterns(List<JavaAnnotation> annotations) {
		JavaAnnotation allowedSyntaxPatternAnnotation = JAVA_ANNOTATION_HELPER.findAnnotation(annotations,
				AllowedSyntaxPattern.class);
		if (allowedSyntaxPatternAnnotation == null) {
			return Collections.emptySet();
		}
		
		Set<String> allowedPatterns = new LinkedHashSet<String>();

		List<String> values = JAVA_ANNOTATION_HELPER.getValues(allowedSyntaxPatternAnnotation, "value");
		allowedPatterns.addAll(values);
		
		return allowedPatterns;
	}

	private void parsePatterns(JavaClassifier classifier, JavaMethod method,
			List<SyntaxSpecification> syntaxSpecifications, JavaAnnotation annotation,
			Collection<ISyntaxPattern<?>> patterns) {
		
		String containgTypeName = classifier.getName();
		String methodName = method.getName();
		
		List<JavaParameter> parameters = method.getParameters();
		List<JavaType> parameterTypes = new ArrayList<JavaType>(parameters.size());
		for (JavaParameter parameter : parameters) {
			parameterTypes.add(parameter.getType());
		}
		
		JavaType returnType = method.getReturnType();
		
		IValidationCallback callback = annotation.getValidationResultCallback();

		Style style = getStyle(method);
		
		String annotationTypeName = annotation.getAnnotationTypeName();
		boolean isStyledTextSyntaxAnnotation = JAVA_ANNOTATION_HELPER.isStyledTextSyntaxAnnotation(annotationTypeName);

		for (SyntaxSpecification syntaxSpecification : syntaxSpecifications) {
			String text = syntaxSpecification.getText();
			boolean parameterSyntax = syntaxSpecification.isParameterSyntax();
			
			String projectName = classifier.getProjectName();
			ISyntaxPattern<?> pattern = PATTERN_PARSER.parse(projectName, containgTypeName, methodName, text,
					parameters, returnType, callback, style, isStyledTextSyntaxAnnotation, parameterSyntax);
			if (pattern != null) {
				patterns.add(pattern);
			}
		}
	}

	private Style getStyle(JavaMethod method) {
		List<JavaAnnotation> annotations = method.getAnnotations();
		JavaAnnotationHelper annotationHelper = JAVA_ANNOTATION_HELPER;
		
		JavaAnnotation boldAnnotation = annotationHelper.findAnnotation(annotations, Bold.class);
		JavaAnnotation italicAnnotation = annotationHelper.findAnnotation(annotations, Italic.class);
		JavaAnnotation underlineAnnotation = annotationHelper.findAnnotation(annotations, Underline.class);
		JavaAnnotation strikethroughAnnotation = annotationHelper.findAnnotation(annotations, Strikethrough.class);
		JavaAnnotation colorAnnotation = annotationHelper.findAnnotation(annotations, Color.class);
		if (boldAnnotation == null && italicAnnotation == null && underlineAnnotation == null
				&& strikethroughAnnotation == null && colorAnnotation == null) {
			return null;
		}
	
		boolean bold = boldAnnotation != null;
		boolean italic = italicAnnotation != null;
		boolean underline = underlineAnnotation != null;
		boolean strikethrough = strikethroughAnnotation != null;
		
		int[] color = null;
		if (colorAnnotation != null) {
			List<JavaAssignment> assignments = colorAnnotation.getAssignments();
			if (!assignments.isEmpty()) {
				List<String> values = annotationHelper.getValues(assignments.get(0));
				if (!values.isEmpty()) {
					String rgb = values.get(0);
					color = getRGB(rgb);
				}
			}
		}
		return new Style(bold, italic, underline, strikethrough, color);
	}

	private int[] getRGB(String rgb) {
		if (rgb.length() != 7) {
			return null;
		}
		
		int red = Integer.parseInt(rgb.substring(1, 3), 16);
		int green = Integer.parseInt(rgb.substring(3, 5), 16);
		int blue = Integer.parseInt(rgb.substring(5, 7), 16);
		return new int[] {red, green, blue};
	}

	/**
	 * Converts the given method name to a (potentially empty) list of syntax specifications where words are identified
	 * using camel-case notation and parameters are identified by the underscore character. The list contains at most
	 * one syntax specification, where the first word is capitalized and the remaining words are lower case.
	 */
	private List<SyntaxSpecification> getSyntaxSpecifications(JavaMethod method) {
		String methodName = method.getName();
		String[] parts = StringUtils.splitByCharacterTypeCamelCase(methodName);
		if (parts == null) {
			return Collections.emptyList();
		}
		
		int parameterIndex = 1;
		for (int idx = 0; idx < parts.length; idx++) {
			String part = parts[idx];
			if ("_".equals(part)) {
				parts[idx] = "#" + parameterIndex;
				parameterIndex++;
			} else if (idx == 0) {
				parts[idx] = WordUtils.capitalize(part);
			} else if (idx > 0) {
				parts[idx] = part.toLowerCase();
			}
		}
		
		String syntaxSpecification = Joiner.on(' ').join(parts);
		IValidationCallback validationCallback = method.getValidationResultCallback();
		boolean parameterSyntax = false;
		SyntaxSpecification newSpecification = new SyntaxSpecification(syntaxSpecification, validationCallback, parameterSyntax);
		return Collections.singletonList(newSpecification);
	}
}
