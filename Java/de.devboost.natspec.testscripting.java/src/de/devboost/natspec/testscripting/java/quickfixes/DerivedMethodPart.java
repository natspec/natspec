package de.devboost.natspec.testscripting.java.quickfixes;

public class DerivedMethodPart {
	
	private String annotationPart;
	private String methodNamePart;
	private String parameterTypePart;
	private String parameterNamePart;

	public DerivedMethodPart(String annotationPart, String methodNamePart,
			String parameterTypePart, String parameterNamePart) {
		
		super();
		this.annotationPart = annotationPart;
		this.methodNamePart = methodNamePart;
		this.parameterTypePart = parameterTypePart;
		this.parameterNamePart = parameterNamePart;
	}

	public String getAnnotationPart() {
		return annotationPart;
	}

	public String getMethodNamePart() {
		return methodNamePart;
	}

	public String getParameterTypePart() {
		return parameterTypePart;
	}

	public String getParameterNamePart() {
		return parameterNamePart;
	}
}