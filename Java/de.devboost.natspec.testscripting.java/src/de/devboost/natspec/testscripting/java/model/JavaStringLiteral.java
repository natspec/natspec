package de.devboost.natspec.testscripting.java.model;

/**
 * A {@link JavaStringLiteral} is a concrete {@link JavaExpression} for String
 * literals.
 */
public class JavaStringLiteral extends JavaExpression {

	private String value;

	public JavaStringLiteral(String value) {
		super();
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
