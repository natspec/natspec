package de.devboost.natspec.testscripting.java.model;

/**
 * A {@link JavaModifier} is a simple enumeration for the visibility modifiers used for {@link JavaMethod}s.
 */
public enum JavaModifier {

	PUBLIC,
	PROTECTED,
	PRIVATE,
	PACKAGE_PROTECTED
}
