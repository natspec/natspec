package de.devboost.natspec.testscripting.java.patterns.parts;

import java.util.List;

import de.devboost.natspec.Word;
import de.devboost.natspec.matching.AbstractSyntaxPatternPartMatch;

public class LocalTimeMatch extends AbstractSyntaxPatternPartMatch {

	private final String value;

	public LocalTimeMatch(List<Word> words, String value) {
		super(words);
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
