/**
 * This package contains interfaces that can be implemented by NatSpec template
 * classes to trigger additional behavior when running NatSpec scripts.
 */
package de.devboost.natspec.interfaces;
