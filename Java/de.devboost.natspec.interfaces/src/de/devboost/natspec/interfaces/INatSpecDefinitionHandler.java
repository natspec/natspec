package de.devboost.natspec.interfaces;

/**
 * Implement this interface to get notifications about the comments and 
 * sentences are contained in a NatSpec script.
 */
public interface INatSpecDefinitionHandler {

	/**
	 * This method is called before the actual execution of the NatSpec script
	 * to inform this listener about the sentence contained in the NatSpec
	 * script. This method is called once per sentence and in the order in 
	 * which sentences appear in the script.
	 */
	public void register(String sentence);
	
	/**
	 * This method is called before the actual execution of the NatSpec script
	 * to inform this listener about the comments contained in the NatSpec
	 * script. This method is called once per comment and in the order in 
	 * which comments appear in the script.
	 */
	public void registerComment(String comment);
}
