package de.devboost.natspec.interfaces;

/**
 * Let a NatSpec template class implement this interface to log the execution
 * of sentences from a NatSpec script. 
 */
public interface INatSpecLogger {

	public void log(String sentence);
}
