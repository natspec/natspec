package de.devboost.natspec.interfaces;

/**
 * NatSpec templates that are tagged with this interface are automatically
 * extended with a static method <code>createNatSpecDescription</code> that can
 * be used to retrieve information about the contents of the original NatSpec
 * specification.
 */
public interface ISelfDescribingNatSpecTemplate {
	
	public final static String METHOD_NAME = "createNatSpecDescription";

}
