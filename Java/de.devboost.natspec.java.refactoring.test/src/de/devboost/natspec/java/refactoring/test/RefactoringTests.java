package de.devboost.natspec.java.refactoring.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.resource.Resource;
import org.junit.Test;

import de.devboost.natspec.java.refactoring.RefactoringExecutor;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.resource.natspec.util.NatspecResourceUtil;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.PatternMatchContext;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

public class RefactoringTests {

	@Test
	public void testNoChangeRefactoring() {
		String textToRefactor = "Old Friends";
		String expectedScript = textToRefactor;
		String targetPattern = textToRefactor;

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Old"));
		sourcePattern.addPart(new StaticWord("Friends"));
		
		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	@Test
	public void testNoChangeWithParameterRefactoring() {
		String textToRefactor = "Old Friends";
		String expectedScript = textToRefactor;
		String targetPattern = "Old #1";

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Old"));
		sourcePattern.addPart(new StringParameter());
		
		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	@Test
	public void testStaticWordsOnlyRefactoring() {
		String textToRefactor = "Old Friends";
		String expectedScript = "New Friends";
		String targetPattern = "New Friends";

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Old"));
		sourcePattern.addPart(new StaticWord("Friends"));
		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	@Test
	public void testOnlyOneParameterRefactoring() {

		String textToRefactor = "Old Friends drink a lot";
		String expectedScript = "New Friends drink a lot";
		String targetPattern = "New Friends #1 a lot";

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Old"));
		sourcePattern.addPart(new StaticWord("Friends"));
		sourcePattern.addPart(new StringParameter());
		sourcePattern.addPart(new StaticWord("a"));
		sourcePattern.addPart(new StaticWord("lot"));

		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	@Test
	public void tesWithPassengerJohnDoeRefactoring() {

		String textToRefactor = "Given a passenger John Doe";
		String expectedScript = "Create a passenger John Doe";
		String targetPattern = "Create a passenger #1 #2";

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Given"));
		sourcePattern.addPart(new StaticWord("a"));
		sourcePattern.addPart(new StaticWord("passenger"));
		sourcePattern.addPart(new StringParameter());
		sourcePattern.addPart(new StringParameter());
		
		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	@Test
	public void testTwoParameterRefactoring() {

		String textToRefactor = "Old Friends drink a lot of beer";
		String expectedScript = "New Friends drink a lot of fresh beer";
		String targetPattern = "New Friends #1 a lot of fresh #2";

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Old"));
		sourcePattern.addPart(new StaticWord("Friends"));
		sourcePattern.addPart(new StringParameter());
		sourcePattern.addPart(new StaticWord("a"));
		sourcePattern.addPart(new StaticWord("lot"));
		sourcePattern.addPart(new StaticWord("of"));
		sourcePattern.addPart(new StringParameter());

		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	@Test
	public void testIntegerParameterRefactoring() {

		String textToRefactor = "Old Friends drink 10 bottles of beer";
		String expectedScript = "New Friends drink 10 bottles of fresh beer";
		String targetPattern = "New Friends #1 #2 bottles of fresh #3";

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Old"));
		sourcePattern.addPart(new StaticWord("Friends"));
		sourcePattern.addPart(new StringParameter());
		sourcePattern.addPart(new IntegerArgument());
		sourcePattern.addPart(new StaticWord("bottles"));
		sourcePattern.addPart(new StaticWord("of"));
		sourcePattern.addPart(new StringParameter());

		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	@Test
	public void testToJodaSpeechRefactoring() {

		String textToRefactor = "Old Friends drink 10 bottles of beer";
		String expectedScript = "10 bottles of fresh beer new friends drink";
		String targetPattern = "#2 bottles of fresh #3 new friends #1";

		SyntaxPatternMock<?> sourcePattern = new SyntaxPatternMock<Object>();
		sourcePattern.addPart(new StaticWord("Old"));
		sourcePattern.addPart(new StaticWord("Friends"));
		sourcePattern.addPart(new StringParameter());
		sourcePattern.addPart(new IntegerArgument());
		sourcePattern.addPart(new StaticWord("bottles"));
		sourcePattern.addPart(new StaticWord("of"));
		sourcePattern.addPart(new StringParameter());

		String newText = applyRefactoring(textToRefactor.getBytes(), sourcePattern, targetPattern);
		assertThat(newText, is(equalTo(expectedScript)));
	}

	private String applyRefactoring(byte[] scriptToRefactor, ISyntaxPattern<?> sourcePattern, String targetPattern) {

		final List<ISyntaxPattern<? extends Object>> patterns = Collections
				.<ISyntaxPattern<? extends Object>> singletonList(sourcePattern);

		IPatternMatchContext context = new PatternMatchContext(null) {

			@Override
			public Collection<ISyntaxPattern<? extends Object>> getPatternsInContext() {
				return patterns;
			}
		};

		RefactoringExecutor refactoringExcetuor = new RefactoringExecutor();

		Resource resource = NatspecResourceUtil.getResource(scriptToRefactor);
		MatchService matchService = new MatchService(true, true);

		List<ISyntaxPatternMatch<? extends Object>> matches = matchService.match(resource, context);

		return refactoringExcetuor.applyRefactoring(targetPattern, matches.get(0));
	}
}
