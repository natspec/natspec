package de.devboost.natspec.junit4.runner;

import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

import de.devboost.natspec.interfaces.INatSpecDefinitionHandler;
import de.devboost.natspec.interfaces.ISelfDescribingNatSpecTemplate;
import de.devboost.natspec.junit4.runner.internal.NatSpecRunNotifier;

public class NatSpecJUnit4Runner extends BlockJUnit4ClassRunner implements
		INatSpecDefinitionHandler {

	private final static String SET_RUNNER_METHOD_NAME = "setRunner";

	private final Class<?> testClass;
	private final boolean isNatSpecTemplate;

	private Description description;
	private List<Description> steps;
	private RunNotifier runNotifier;
	private Description lastStep;
	private NatSpecRunNotifier customRunNotifier;

	public NatSpecJUnit4Runner(Class<?> testClass) throws InitializationError {
		super(testClass);
		this.testClass = testClass;
		this.isNatSpecTemplate = "_NatSpecTemplate".equals(testClass.getSimpleName());
	}

	@Override
	public Description getDescription() {
		if (this.description != null) {
			return this.description;
		}

		this.description = Description.createSuiteDescription(testClass);
		this.steps = new ArrayList<Description>();

		if (isNatSpecTemplate) {
			return this.description;
		}

		String listenerClassName = INatSpecDefinitionHandler.class.getName();
		String methodName = ISelfDescribingNatSpecTemplate.METHOD_NAME;
		try {
			Class<?> clazz = Class.forName(listenerClassName);
			Method method = testClass.getMethod(methodName,
					new Class<?>[] { clazz });
			method.invoke(null, new Object[] { this });
		} catch (SecurityException e) {
			fail("Can't access method " + methodName);
		} catch (NoSuchMethodException e) {
			fail("Can't find method " + methodName);
		} catch (IllegalArgumentException e) {
			fail("Can't invoke method " + methodName + " : " + e.getMessage());
		} catch (IllegalAccessException e) {
			fail("Can't invoke method " + methodName + " : " + e.getMessage());
		} catch (InvocationTargetException e) {
			fail("Can't invoke method " + methodName + " : " + e.getMessage());
		} catch (ClassNotFoundException e) {
			fail("Can't find class " + listenerClassName);
		}

		return description;
	}

	@Override
	public void run(RunNotifier runNotifier) {
		this.runNotifier = runNotifier;
		
		if (isNatSpecTemplate) {
			runNotifier.fireTestIgnored(this.description);
			return;
		}

		try {
			Method method = testClass.getMethod(SET_RUNNER_METHOD_NAME, new Class[] {NatSpecJUnit4Runner.class});
			method.invoke(null, this);
		} catch (SecurityException e) {
			fail("Can't access method '" + SET_RUNNER_METHOD_NAME
					+ "' in template class. Make sure to extend "
					+ NatSpecJUnit4Template.class.getName() + ".");
		} catch (IllegalArgumentException e) {
			fail("Can't invoke method '" + SET_RUNNER_METHOD_NAME
					+ "' in template class. Make sure to extend "
					+ NatSpecJUnit4Template.class.getName() + ".");
		} catch (IllegalAccessException e) {
			fail("Can't invoke method '" + SET_RUNNER_METHOD_NAME
					+ "' in template class. Make sure to extend "
					+ NatSpecJUnit4Template.class.getName() + ".");
		} catch (NoSuchMethodException e) {
			fail("Can't find method '" + SET_RUNNER_METHOD_NAME
					+ "' in template class. Make sure to extend "
					+ NatSpecJUnit4Template.class.getName() + ".");
		} catch (InvocationTargetException e) {
			fail("Can't invoke method '" + SET_RUNNER_METHOD_NAME
					+ "' in template class. Make sure to extend "
					+ NatSpecJUnit4Template.class.getName() + ".");
		}

		try {
			customRunNotifier = new NatSpecRunNotifier(runNotifier);
			super.run(customRunNotifier);
			fireLastStepFinished();
		} catch (Exception e) {
			runNotifier.fireTestFailure(new Failure(description, e));
		}
	}

	public void log(String sentence) {
		fireEvents();
	}

	private void fireEvents() {
		fireLastStepFinished();
		Description step = steps.remove(0);
		lastStep = step;
		customRunNotifier.setCurrentStep(step);
		runNotifier.fireTestStarted(step);
	}

	private void fireLastStepFinished() {
		if (lastStep != null) {
			runNotifier.fireTestFinished(lastStep);
		}
	}

	@Override
	public void register(String sentence) {
		// NATSPEC-319 We need to have a globally unique id here
		String uniqueID = testClass.getCanonicalName() + "_" + steps.size();
		Description step = Description.createTestDescription(
				testClass.getCanonicalName(), sentence, uniqueID);
		description.addChild(step);
		steps.add(step);
	}

	@Override
	public void registerComment(String comment) {
		// Do nothing. Comments are currently not logged when a NatSpec script
		// is executed. Therefore we must not create test step.
	}
}
