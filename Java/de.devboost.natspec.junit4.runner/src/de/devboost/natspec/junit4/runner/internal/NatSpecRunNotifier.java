package de.devboost.natspec.junit4.runner.internal;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runner.notification.StoppedByUserException;

/**
 * The {@link NatSpecRunNotifier} is used as a facade for the actual
 * {@link RunNotifier} and discards some events and translates other events.
 */
public class NatSpecRunNotifier extends RunNotifier {

	private final RunNotifier runNotifier;
	
	private Description currentStep;

	public NatSpecRunNotifier(RunNotifier runNotifier) {
		super();
		this.runNotifier = runNotifier;
	}

	@Override
	public void addListener(RunListener listener) {
		runNotifier.addListener(listener);
	}

	@Override
	public void removeListener(RunListener listener) {
		runNotifier.removeListener(listener);
	}

	@Override
	public int hashCode() {
		return runNotifier.hashCode();
	}

	@Override
	public void fireTestRunStarted(Description description) {
		// Discard event because the description that is passed to this method
		// is created by the original JUnitRunner and does not correspond to a
		// description that is created for a sentence.
	}

	@Override
	public void fireTestRunFinished(Result result) {
		// Discard event because the description that is passed to this method
		// is created by the original JUnitRunner and does not correspond to a
		// description that is created for a sentence.
	}

	@Override
	public void fireTestStarted(Description description)
			throws StoppedByUserException {
		// Discard event because the description that is passed to this method
		// is created by the original JUnitRunner and does not correspond to a
		// description that is created for a sentence.
	}

	@Override
	public boolean equals(Object obj) {
		return runNotifier.equals(obj);
	}

	@Override
	public void fireTestFailure(Failure failure) {
		// Translate failure object (replace the description with the
		// description for the current step (sentence). This is required to
		// make sure that exceptions are associated with the correct sentence.
		Failure newFailure = new Failure(currentStep, failure.getException());
		runNotifier.fireTestFailure(newFailure);
	}

	@Override
	public void fireTestAssumptionFailed(Failure failure) {
		// Translate failure object (replace the description with the
		// description for the current step (sentence). This is required to
		// make sure that exceptions are associated with the correct sentence.
		Failure newFailure = new Failure(currentStep, failure.getException());
		runNotifier.fireTestAssumptionFailed(newFailure);
	}

	@Override
	public void fireTestIgnored(Description description) {
		runNotifier.fireTestIgnored(description);
	}

	@Override
	public void fireTestFinished(Description description) {
		// Discard event because the description that is passed to this method
		// is created by the original JUnitRunner and does not correspond to a
		// description that is created for a sentence.
	}

	@Override
	public void pleaseStop() {
		runNotifier.pleaseStop();
	}

	@Override
	public void addFirstListener(RunListener listener) {
		runNotifier.addFirstListener(listener);
	}

	@Override
	public String toString() {
		return runNotifier.toString();
	}

	public void setCurrentStep(Description currentStep) {
		this.currentStep = currentStep;
	}
}
