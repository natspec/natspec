package de.devboost.natspec.junit4.runner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;

import de.devboost.natspec.interfaces.INatSpecDefinitionHandler;
import de.devboost.natspec.interfaces.INatSpecLogger;
import de.devboost.natspec.interfaces.ISelfDescribingNatSpecTemplate;

/**
 * The {@link NatSpecJUnit4Template} can be extended by concrete NatSpec
 * templates to enable showing the sentences of the NatSpec scenario as
 * individual JUnit tests. In addition to extending this class, NatSpec
 * templates must specify the {@link NatSpecJUnit4Runner} using the JUnit
 * {@link Runner} annotation at the class level.
 */
public abstract class NatSpecJUnit4Template implements
		INatSpecDefinitionHandler, INatSpecLogger,
		ISelfDescribingNatSpecTemplate {

	// This field is injected by the NatSpecTestRunners
	private static NatSpecJUnit4Runner runner;
	
	public static void setRunner(NatSpecJUnit4Runner runner) {
		NatSpecJUnit4Template.runner = runner;
	}

	@Before
	public void checkTemplateConfiguration() {
		Class<?> templateClass = getClass();
		RunWith annotation = templateClass.getAnnotation(RunWith.class);

		String message = "NatSpec template does not specify @RunWith(NatSpecJUnit4Runner.class)";
		assertNotNull(message, annotation);
		Class<?> runner = annotation.value();
		assertEquals(message, NatSpecJUnit4Runner.class, runner);
	}
	
	/**
	 * This method must be implemented in concrete NatSpec templates and contain
	 * the <code>@MethodBody</code> placeholder. It must also be annotated with
	 * the JUnit {@link Test} annotation.
	 */
	protected abstract void runTest() throws Exception;
	
	@Override
	public void register(String sentence) {
		if (runner != null) {
			runner.register(sentence);
		}
	}

	@Override
	public void registerComment(String comment) {
		if (runner != null) {
			runner.registerComment(comment);
		}
	}

	@Override
	public void log(String sentence) {
		if (runner != null) {
			runner.log(sentence);
		}
	}
}
