name = NatSpec
updatesite = de.devboost.natspec.updatesite
feature = de.devboost.natspec,de.devboost.natspec.junit4
associatesites = http://download.eclipse.org/releases/oxygen
eclipsemirror = 138.201.133.239

type/win64   = http://eclipse.devboost.de/downloads/drops4/R-4.7.1a-201710090410/eclipse-SDK-4.7.1a-win32-x86_64.zip
type/win32   = http://eclipse.devboost.de/downloads/drops4/R-4.7.1a-201710090410/eclipse-SDK-4.7.1a-win32.zip
type/osx     = http://eclipse.devboost.de/downloads/drops4/R-4.7.1a-201710090410/eclipse-SDK-4.7.1a-macosx-cocoa-x86_64.tar.gz
type/linux   = http://eclipse.devboost.de/downloads/drops4/R-4.7.1a-201710090410/eclipse-SDK-4.7.1a-linux-gtk.tar.gz
type/linux64 = http://eclipse.devboost.de/downloads/drops4/R-4.7.1a-201710090410/eclipse-SDK-4.7.1a-linux-gtk-x86_64.tar.gz

platform/plugin.xml/startupForegroundColor = 707173
platform/plugin.xml/startupMessageRect = 8,251,320,20
platform/plugin.xml/startupProgressRect = 7,276,438,10

upload.target_path       = www.nat-spec.com:/home/jenkins/public/natspec/products/trunk
upload.username_property = DEVBOOST_UPLOAD_USER
upload.password_property = DEVBOOST_UPLOAD_PASS
