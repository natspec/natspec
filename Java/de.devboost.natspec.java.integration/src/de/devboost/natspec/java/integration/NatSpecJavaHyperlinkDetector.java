package de.devboost.natspec.java.integration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.hyperlink.AbstractHyperlinkDetector;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.jface.text.hyperlink.IHyperlinkDetector;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.resource.natspec.ui.ErrorMessageHelper;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.java.JavaBodyGenerator;

public class NatSpecJavaHyperlinkDetector extends AbstractHyperlinkDetector implements IHyperlinkDetector {

	private static final String FEATURE_NAME = "'Hyperlink'";

	protected static final String JAVA_FILE_EXTENSION = ".java";

	public NatSpecJavaHyperlinkDetector() {
	}

	@Override
	public IHyperlink[] detectHyperlinks(ITextViewer textViewer, IRegion region, boolean canShowMultipleHyperlinks) {

		IDocument document = textViewer.getDocument();

		IFile natspecFile = getNatspecFile(document);
		if (natspecFile == null) {
			return null;
		}

		Region natSpecSentenceRegion = discoverNatSpecSentence(region, document);
		if (natSpecSentenceRegion == null) {
			return null;
		}

		// We test for valid license at this point because we know that we are dealing with a file related to NatSpec.
		if (!NatSpecPlugin.hasValidLicense()) {
			// no license, no hyper links
			ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();
			errorMessageHelper.showErrorMessage(FEATURE_NAME);
			// FIX by CW do not: return new IHyperlink[0];
			// org.eclipse.jface.text.hyperlink.HyperlinkManager does not allow this, returning null is better.
			return null;
		}

		Scanner scanner = null;
		InputStream inputStream = null;
		try {
			int offset = natSpecSentenceRegion.getOffset();
			int length = natSpecSentenceRegion.getLength();
			String potentialSentence = document.get(offset, length);

			inputStream = natspecFile.getContents();

			scanner = new Scanner(inputStream, natspecFile.getCharset());
			scanner.useDelimiter("\\A");

			String content = scanner.hasNext() ? scanner.next() : "";

			Region sentenceInNatspecFile = findSentenceInNatspecFile(content, potentialSentence, natSpecSentenceRegion,
					document);
			if (sentenceInNatspecFile == null) {
				return null;
			}

			JavaHyperLink hyperLink = new JavaHyperLink(natSpecSentenceRegion, natspecFile, sentenceInNatspecFile);
			return new JavaHyperLink[] { hyperLink };
		} catch (BadLocationException e) {
			return null;
		} catch (CoreException e) {
			return null;
		} finally {
			if (scanner != null) {
				scanner.close();
			}
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private IFile getNatspecFile(IDocument document) {
		String javaFileContents;
		try {
			javaFileContents = document.get(0, document.getLength());
		} catch (BadLocationException e) {
			return null;
		}

		int index = javaFileContents.indexOf(JavaBodyGenerator.GENERATION_SOURCE_STRING);
		if (index < 0) {
			return null;
		}

		int uriStart = index + JavaBodyGenerator.GENERATION_SOURCE_STRING.length();
		int uriEnd = javaFileContents.indexOf("\n", uriStart);
		String uriString = javaFileContents.substring(uriStart, uriEnd);
		URI uri = URI.createPlatformResourceURI(uriString.trim(), true);
		return new NatspecEclipseProxy().getFileForURI(uri);
	}

	private Region findSentenceInNatspecFile(String natspecFileContent, String potentialSentence, Region sentenceRegion,
			IDocument document) {

		try {
			potentialSentence = potentialSentence.trim();
			String string = document.get(0, sentenceRegion.getOffset());
			int previousOccurences = 0;
			int currentMatch = string.indexOf(potentialSentence, 0);
			int potentialSentenceLength = potentialSentence.length();
			while (currentMatch > -1) {
				previousOccurences++;
				currentMatch = string.indexOf(potentialSentence, currentMatch + potentialSentenceLength);
			}

			int currentOffset = 0;
			while (previousOccurences > 0) {
				currentOffset = natspecFileContent.indexOf(potentialSentence, currentOffset) + potentialSentenceLength;
				previousOccurences--;
			}

			int sentenceInNatspecFileOffset = natspecFileContent.indexOf(potentialSentence, currentOffset);
			if (sentenceInNatspecFileOffset < 0) {
				return null;
			}

			return new Region(sentenceInNatspecFileOffset, potentialSentenceLength);
		} catch (BadLocationException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	private Region discoverNatSpecSentence(IRegion region, IDocument document) {
		try {
			IRegion lineInfo = document.getLineInformationOfOffset(region.getOffset());
			int lineInfoOffset = lineInfo.getOffset();
			int lineInfoLength = lineInfo.getLength();

			String line = document.get(lineInfoOffset, lineInfoLength);
			int commentBeginn = line.indexOf("//");
			if (commentBeginn < 0) {
				return null;
			}

			int sentenceOffset = commentBeginn + 3;

			int sentenceLength = lineInfoLength - sentenceOffset;
			int documentOffset = lineInfoOffset + sentenceOffset;

			return new Region(documentOffset, sentenceLength);
		} catch (BadLocationException ex) {
			return null;
		}
	}
}