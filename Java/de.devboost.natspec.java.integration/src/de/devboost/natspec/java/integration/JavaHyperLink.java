package de.devboost.natspec.java.integration;

import static org.eclipse.ui.PlatformUI.getWorkbench;

import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.hyperlink.IHyperlink;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.ide.IDE;

public class JavaHyperLink implements IHyperlink {

	private final Region hyperlinkRegion;
	private final IFile natspecFile;
	private final Region sentenceOccurrence;

	public JavaHyperLink(Region hyperlinkRegion, IFile natspecFile, Region sentenceOccurrence) {
		this.hyperlinkRegion = hyperlinkRegion;
		this.natspecFile = natspecFile;
		this.sentenceOccurrence = sentenceOccurrence;
	}

	@Override
	public void open() {
		try {
			IWorkbench workbench = getWorkbench();
			IWorkbenchWindow activeWindow = workbench.getActiveWorkbenchWindow();
			IWorkbenchPage activePage = activeWindow.getActivePage();
			IEditorPart editorPart = IDE.openEditor(activePage, natspecFile, true);
			if (editorPart instanceof TextEditor) {
				TextEditor textEditor = (TextEditor) editorPart;
				textEditor.selectAndReveal(sentenceOccurrence.getOffset(), sentenceOccurrence.getLength());
			}
			editorPart.setFocus();
		} catch (PartInitException e) {
			// TODO Handle exception
			e.printStackTrace();
		}
	}

	@Override
	public String getTypeLabel() {
		return "NatSpecLink";
	}

	@Override
	public String getHyperlinkText() {
		String hyperLinkText = "Go to NatSpec Sentence";
		return hyperLinkText;
	}

	@Override
	public IRegion getHyperlinkRegion() {
		return hyperlinkRegion;
	}
}