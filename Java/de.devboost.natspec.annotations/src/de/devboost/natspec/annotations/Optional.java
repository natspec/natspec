package de.devboost.natspec.annotations;

/**
 * The &#64;Optional annotation can be added to implicit parameters for NatSpec support methods. If no matching
 * parameter is available in the context, <code>null</code> is passed to the method.
 */
public @interface Optional {

}
