package de.devboost.natspec.annotations.styling;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link Color} annotation can be used to specify that the words of a text syntax pattern match should be displayed
 * in a certain color. An example for an application of the annotation is as follows:
 * <p>
 * <tt>
 * &#64;Color("#800000")<br/>
 * &#64;TextSyntax("Add #1 to #2")<br/>
 * public void add(int x, int y) {<br/>
 * }</tt>
 * <p>
 * The words 'Add' and 'to' will then be displayed in red color in NatSpec scripts.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface Color {

	String value();
}
