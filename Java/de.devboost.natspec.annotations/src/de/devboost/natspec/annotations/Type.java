package de.devboost.natspec.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link Type} annotation can be used to add specify type arguments for generic parameters or return types of support methods.
 * An example of using the annotation is as follows:
 * <p>
 * <code>
 * public void add(&#64;Type("java.util.List&lt;java.lang.String&gt;") List&lt;String&gt; words) {<br/>
 * }
 * </code><br/>
 * <br/>
 */
@Target({ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.CLASS)
public @interface Type {

	String value();
}
