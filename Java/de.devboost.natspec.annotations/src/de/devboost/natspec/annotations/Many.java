package de.devboost.natspec.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link Many} annotation can be used to specify that a parameter can occur 
 * multiple times. An example for an application of the annotation
 * is as follows:<p>
 * <code>
 * &#64;TextSyntax("Print #1")<br/>
 * public void add(&#64;Many String words) {<br/>
 * }
 * </code><br/>
 * <br/>
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.CLASS)
public @interface Many {
}
