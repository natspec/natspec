package de.devboost.natspec.annotations;

/**
 * The &#64;This annotation can be added to implicit parameters for NatSpec support methods. If this annotation is used,
 * the generated NatSpec class passes itself (<code>this</code>) to the test support method. The type of the parameter
 * must be {@link Object}.
 */
public @interface This {

}
