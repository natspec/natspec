package de.devboost.natspec.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link TextSyntaxes} annotation can be used to add multiple text syntax
 * patterns to a method. An example of using the annotation is as follows:
 * <p>
 * <code>
 * &#64;TextSyntaxes({<br/>
 * &nbsp;&nbsp;&#64;TextSyntax("Add #1 to #2"),<br/>
 * &nbsp;&nbsp;&#64;TextSyntax("#1 + #2")<br/>
 * })<br/>
 * public void add(int x, int y) {<br/>
 * }
 * </code><br/>
 * <br/>
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface TextSyntaxes {

	TextSyntax[] value();
}
