package de.devboost.natspec.annotations.rules;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link AllowedSyntaxPatterns} annotation can be used to restrict the syntax patterns that can be defined in a
 * NatSpec support class. The value of the annotation are instances of {@link AllowedSyntaxPattern}. For
 * example, the following annotation defines that only patterns which start with &quot;Assume&quot; or &quot;Assert&quot;
 * can be defined:
 * <p><tt>
 * &#64;AllowedSyntaxPatterns({<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&#64;AllowedSyntaxPattern("Assume.*"),<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&#64;AllowedSyntaxPattern("Assert.*")<br/>
 * })<br/>
 * public class MySupportClass {<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;// Define support methods here<br/>
 * }</tt>
 * <p>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface AllowedSyntaxPatterns {

	AllowedSyntaxPattern[] value();
}
