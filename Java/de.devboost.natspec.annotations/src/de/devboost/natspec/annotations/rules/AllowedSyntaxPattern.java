package de.devboost.natspec.annotations.rules;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link AllowedSyntaxPattern} annotation can be used to restrict the syntax patterns that can be defined in a
 * NatSpec support class. The value of the annotation is a regular expression which all syntax patterns must match. For
 * example, the following annotation defines that only patterns which start with &quot;Assume&quot; can be defined:
 * <p><tt>
 * &#64;AllowedSyntaxPattern("Assume.*")<br/>
 * public class MySupportClass {<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;// Define support methods here<br/>
 * }</tt>
 * <p>
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.CLASS)
public @interface AllowedSyntaxPattern {

	String value();
}
