package de.devboost.natspec.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link BooleanSyntax} annotation can be used to specify text syntax for 
 * boolean parameters of a Java method. An example for an application of the 
 * annotation is as follows:
 * <p><tt>
 * &#64;TextSyntax("#1 user authentication")<br/>
 * public void add(&#64;BooleanSyntax({"Enable", "Disable"}) boolean enable) {<br/>
 * }</tt>
 * <p>
 * The first argument for the annotation specifies the text that represents the
 * value for <code>true</code>, the second one the value for <code>false</code>.
 * One of the two arguments can be an empty string.
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.CLASS)
public @interface BooleanSyntax {

	String[] value();
}
