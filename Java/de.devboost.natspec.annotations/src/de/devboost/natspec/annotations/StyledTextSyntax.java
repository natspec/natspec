package de.devboost.natspec.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link StyledTextSyntax} can be used to define a text syntax pattern with BBCode styles.
 * An example of an application of this syntax is as follows:
 * <p><tt>
 * &#64;TextSyntax("[b]Cancel[/b] customer order [color=red][u]#1[/u][/color]")
 * public void cancelCustomerOrder(String orderID) {<br/>
 * } 
 *</tt>
 * <p>
 * This would render the <tt>Cancel</tt> in bold font and the order ID underlined and in red color.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface StyledTextSyntax {

	String value();
}
