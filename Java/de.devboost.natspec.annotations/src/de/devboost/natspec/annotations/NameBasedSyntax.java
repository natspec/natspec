package de.devboost.natspec.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link NameBasedSyntax} annotation can be used to specify a text syntax 
 * pattern which is automatically derived for a Java method or all public 
 * methods of a class. An example for an application of the annotation is as 
 * follows:<p>
 * <code>
 * &#64;NameBasedSyntax()<br/>
 * public void add_to_(int x, int y) {<br/>
 * }
 * </code><br/>
 * <br/>
 * The underscore character can be used to indicate the position of the 
 * parameters in the text pattern.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.CLASS)
public @interface NameBasedSyntax {
	
}
