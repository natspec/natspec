package de.devboost.natspec.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link TextSyntax} annotation can be used to specify a text syntax 
 * pattern for a Java method. An example for an application of the annotation
 * is as follows:
 * <p><tt>
 * &#64;TextSyntax("Add #1 to #2")<br/>
 * public void add(int x, int y) {<br/>
 * }</tt>
 * <p>
 * The hash character can be used to indicate the position of the parameters 
 * in the text pattern.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
public @interface TextSyntax {

	String value();
}
