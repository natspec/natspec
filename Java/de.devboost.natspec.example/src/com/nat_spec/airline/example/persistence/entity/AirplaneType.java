package com.nat_spec.airline.example.persistence.entity;

/**
 * An {@link AirplaneType} represents a specific type of airplane which is
 * characterized by its name and a number of total seats.
 */
public class AirplaneType extends NamedEntity {

	/**
	 * This field holds the total number of seats available in the airplane.
	 */
	private int totalSeats;

	/**
	 * Creates a new {@link AirplaneType} entity with the given name.
	 * 
	 * @param name the name of the type of airplane
	 */
	public AirplaneType(String name) {
		super(name);
	}

	/**
	 * Returns the total number of seats available in this type of airplane.
	 * 
	 * @return the total seat count
	 */
	public int getTotalSeats() {
		return totalSeats;
	}

	/**
	 * Sets the total number of seats available in this type of airplane.
	 * 
	 * @param totalSeats the new total seat count
	 */
	public void setTotalSeats(int totalSeats) {
		this.totalSeats = totalSeats;
	}
}
