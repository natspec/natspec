package com.nat_spec.airline.example.persistence.entity;

/**
 * This is an abstract super class for all entities. It assigns a unique ID to 
 * each entity object similar to a primary key in a database.
 */
public abstract class Entity {

	/**
	 * This field holds the unique ID of this entity. It is assigned when the
	 * entity is created and cannot be changed afterwards.
	 */
	private final int id;
	
	/**
	 * A global counter that holds the next available entity ID.
	 */
	private static int counter = 0;

	/**
	 * Creates a new entity and initialized its ID.
	 */
	public Entity() {
		super();
		this.id = counter++;
	}

	/**
	 * Returns the ID of this entity.
	 * 
	 * @return the unique ID
	 */
	public int getId() {
		return id;
	}
}
