package com.nat_spec.airline.example.persistence.entity;

import java.util.LinkedList;
import java.util.List;

/**
 * A {@link Flight} represents the opportunity to travel from one airport to
 * another. Each {@link Flight} is executed using a particular type of airplane
 * (see {@link #getAirplane()}) and holds a limited amount of passengers (see
 * {@link #getBookedPassengerIds()}).
 */
public class Flight extends NamedEntity {

	/**
	 * This field holds the total number of seats that can be used on this
	 * flight. This number is not changed when passengers are booked on the
	 * flight. Rather, the number of remaining free seats can be determined by
	 * subtracting the booked passengers from this number (see
	 * {@link #getFreeSeats()}).
	 */
	private int freeSeats;
	
	/**
	 * This field holds a reference to the type of airplane that is used to
	 * execute this flight.
	 */
	private AirplaneType airplane;
	
	/**
	 * This fields holds a list of all IDs of passengers who are booked on this
	 * flight.
	 */
	private List<Integer> passengerIds = new LinkedList<Integer>();

	/**
	 * Creates a new flight with the given name.
	 * 
	 * @param name the new name for the flight
	 */
	public Flight(String name) {
		super(name);
	}

	/**
	 * Returns the type of airplane that is used to execute this flight.
	 * 
	 * @return the used type of airplane
	 */
	public AirplaneType getAirplane() {
		return airplane;
	}

	/**
	 * Set the type of airplane that is used to execute this flight. When this
	 * method is invoked, the total number of seats available on this flight
	 * is derived from the number of total seats present in the airplane (see
	 * {@link AirplaneType#getTotalSeats()}).
	 * 
	 * @param airplane the type of airplane to be used
	 */
	public void setAirplane(AirplaneType airplane) {
		this.airplane = airplane;
		this.freeSeats = airplane.getTotalSeats();
	}

	/**
	 * Adds a passenger to this flight.
	 * 
	 * @param passenger the passenger to add
	 */
	public void addPassenger(Passenger passenger) {
		this.passengerIds.add(passenger.getId());
	}

	/**
	 * Checks whether the given passenger is booked for this flight.
	 * 
	 * @param passenger
	 *            the passenger to check
	 * @return <code>true</code> if the passenger is booked, otherwise
	 *         <code>false</code>
	 */
	public boolean hasPassenger(Passenger passenger) {
		return passengerIds.contains(passenger.getId());
	}

	/**
	 * Returns all IDs of the passengers who are booked on this flight.
	 * 
	 * @return a list of all booked passenger IDs
	 */
	public List<Integer> getBookedPassengerIds() {
		return passengerIds;
	}

	/**
	 * Removes the given passenger from this flight.
	 * 
	 * @param passenger
	 *            the passenger to remove
	 * @return <code>true</code> if the passenger was booked, otherwise
	 *         <code>false</code>
	 */
	public boolean removePassenger(Passenger passenger) {
		return passengerIds.remove(Integer.valueOf(passenger.getId()));
	}

	/**
	 * Returns the number of seats that are still available on this flight.
	 * 
	 * @return the available seat count
	 */
	public int getFreeSeats() {
		return freeSeats - passengerIds.size();
	}

	/**
	 * Sets the number of seats that can be used in total on this flight.
	 * 
	 * @param freeSeats the total number of available seats
	 */
	public void setFreeSeats(int freeSeats) {
		this.freeSeats = freeSeats;
	}
}
