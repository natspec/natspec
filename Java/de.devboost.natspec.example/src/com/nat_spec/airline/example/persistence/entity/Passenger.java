package com.nat_spec.airline.example.persistence.entity;

/**
 * A {@link Passenger} object represents a person who can be booked on flights.
 */
public class Passenger extends Entity {

	/**
	 * The passengers first name.
	 */
	private String firstname;
	
	/**
	 * The passengers last name.
	 */
	private String lastname;

	/**
	 * The passengers age (in years).
	 */
	private int age;

	/**
	 * Creates a new passenger with the given first and last name, having an
	 * initial ago of zero.
	 * 
	 * @param firstname
	 *            the first name of the passenger
	 * @param lastname
	 *            the last name of the passenger
	 */
	public Passenger(String firstname, String lastname) {
		super();
		this.setFirstname(firstname);
		this.setLastname(lastname);
		this.setAge(0);
	}

	/**
	 * Returns this passengers first name.
	 * 
	 * @return the first name
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Sets this passengers first name.
	 * 
	 * @param firstname the new first name
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * Returns this passengers last name.
	 * 
	 * @return the last name
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Sets this passengers last name.
	 * 
	 * @param lastname the new last name
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * Returns this passengers age (in years).
	 * 
	 * @return the passengers age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Sets this passengers age (in years).
	 * 
	 * @param age the new age
	 */
	public void setAge(int age) {
		this.age = age;
	}
}
