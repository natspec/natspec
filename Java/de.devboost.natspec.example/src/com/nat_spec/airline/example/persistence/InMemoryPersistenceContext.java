package com.nat_spec.airline.example.persistence;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import com.nat_spec.airline.example.persistence.entity.AirplaneType;
import com.nat_spec.airline.example.persistence.entity.Entity;
import com.nat_spec.airline.example.persistence.entity.Flight;
import com.nat_spec.airline.example.persistence.entity.NamedEntity;
import com.nat_spec.airline.example.persistence.entity.Passenger;

/**
 * The {@link InMemoryPersistenceContext} is a very basic store for entity
 * objects. It basically holds all objects in memory. In a real application,
 * this will probably be replaced by a Data Access Object (DAO) that retrieves
 * objects from and stores objects to a database.
 */
public class InMemoryPersistenceContext {

	/**
	 * This fields holds the single instance of this class.
	 */
	private static InMemoryPersistenceContext context;
	
	/**
	 * A map that holds all entities of type {@link Flight}.
	 */
	private Map<Integer, Flight> flights = new LinkedHashMap<Integer, Flight>();
	
	/**
	 * A map that holds all entities of type {@link AirplaneType}.
	 */
	private Map<Integer, AirplaneType> airplanes = new LinkedHashMap<Integer, AirplaneType>();
	
	/**
	 * A map that holds all entities of type {@link Passenger}.
	 */
	private Map<Integer, Passenger> passengers = new LinkedHashMap<Integer, Passenger>();

	/**
	 * Returns the one and only instance of this class.
	 * 
	 * @return the singleton instance
	 */
	public static InMemoryPersistenceContext getPersistenceContext() {
		if (context == null) {
			context = new InMemoryPersistenceContext();
		}
		return context;
	}

	/**
	 * Creates a new type of airplane with the given name.
	 * 
	 * @param name the name of the airplane type.
	 * @return the created object
	 */
	public AirplaneType createAirplaneType(String name) {
		AirplaneType a = new AirplaneType(name);
		update(a);
		return a;
	}
	
	/**
	 * Creates a new passenger with the given first and last name.
	 * 
	 * @param firstname the first name of the passenger.
	 * @param lastname the name of the passenger.
	 * @return the created object
	 */
	public Passenger createPassenger(String firstname, String lastname) {
		Passenger p = new Passenger(firstname, lastname);
		update(p);
		return p;
	}

	/**
	 * Creates a new flight with the given name.
	 * 
	 * @param name the name of the flight.
	 * @return the created object
	 */
	public Flight createFlight(String name) {
		Flight f = new Flight(name);
		update(f);
		return f;
	}

	/**
	 * Stores the given entity.
	 * 
	 * @param entity the entity to store
	 */
	public void update(Entity entity) {
		if (entity instanceof Flight) {
			flights.put(entity.getId(), (Flight) entity);
		}
		if (entity instanceof AirplaneType) {
			airplanes.put(entity.getId(), (AirplaneType) entity);
		}
		if (entity instanceof Passenger) {
			passengers.put(entity.getId(), (Passenger) entity);
		}
	}

	/**
	 * Returns the {@link AirplaneType} with the given id.
	 * 
	 * @param id
	 *            the id to search for
	 * @return the {@link AirplaneType} or <code>null</code> if no object with
	 *         the given id was found
	 */
	public AirplaneType getAirplaneType(int id) {
		return airplanes.get(id);
	}

	/**
	 * Returns the {@link Flight} with the given id.
	 * 
	 * @param id
	 *            the id to search for
	 * @return the {@link Flight} or <code>null</code> if no object with
	 *         the given id was found
	 */
	public Flight getFlight(int id) {
		return flights.get(id);
	}

	/**
	 * Returns the {@link Passenger} with the given id.
	 * 
	 * @param id
	 *            the id to search for
	 * @return the {@link Passenger} or <code>null</code> if no object with
	 *         the given id was found
	 */
	public Passenger getPassenger(int id) {
		return passengers.get(id);
	}
	
	/**
	 * Returns the {@link AirplaneType} with the given name.
	 * 
	 * @param name
	 *            the name of the flight to search for
	 * @return the {@link AirplaneType} or <code>null</code> if no object with
	 *         the given name was found	
	 */
	public AirplaneType findAirplaneType(String name) {
		return findNamedEntity(name, airplanes.values());
	}
	
	/**
	 * Returns the {@link Flight} with the given name.
	 * 
	 * @param name
	 *            the name of the flight to search for
	 * @return the {@link Flight} or <code>null</code> if no object with
	 *         the given name was found	
	 */
	public Flight findFlight(String name) {
		return findNamedEntity(name, flights.values());
	}
	
	private <T extends NamedEntity> T findNamedEntity(String name,
			Collection<T> entities) {
		for (T next : entities) {
			if (name.equals(next.getName())) {
				return next;
			}
		}
		return null;
	}
	
	/**
	 * Returns the {@link Passenger} with the given name.
	 * 
	 * @param firstname
	 *            the first name to search for
	 * @param lastname
	 *            the last name to search for
	 * @return the {@link Passenger} or <code>null</code> if no object with
	 *         the given name was found	
	 */
	public Passenger findPassenger(String firstname, String lastname) {
		for (Passenger next : passengers.values()) {
			if (firstname.equals(next.getFirstname())
					&& lastname.equals(next.getLastname())) {
				return next;
			}
		}
		return null;
	}
}
