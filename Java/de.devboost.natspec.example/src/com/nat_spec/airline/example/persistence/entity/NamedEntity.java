package com.nat_spec.airline.example.persistence.entity;

/**
 * This is an abstract super class for all entities that carry a name.
 */
public abstract class NamedEntity extends Entity {

	/**
	 * Holds the name of this entity.
	 */
	private String name;

	/**
	 * Creates a new {@link NamedEntity} with the given name.
	 * 
	 * @param name the new name
	 */
	public NamedEntity(String name) {
		super();
		this.name = name;
	}

	/**
	 * Returns the name of this entity.
	 * 
	 * @return the entities name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of this entity.
	 * 
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
}
