package com.nat_spec.airline.example.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nat_spec.airline.example.persistence.InMemoryPersistenceContext;
import com.nat_spec.airline.example.persistence.entity.AirplaneType;
import com.nat_spec.airline.example.persistence.entity.Flight;
import com.nat_spec.airline.example.persistence.entity.Passenger;
import com.nat_spec.airline.example.service.AirlineService;
import com.nat_spec.airline.example.service.OperationStatus;
import com.nat_spec.airline.example.test.scenarios.BookSeatTwice;

/**
 * The {@link ClassicUnitTest} illustrates how the {@link BookSeatTwice}
 * scenario could be implemented in a plain JUnit test. It serves as an example
 * to compare the NatSpec approach to test specification with the classic JUnit
 * procedure.
 */
public class ClassicUnitTest {

	/**
	 * This field holds an instance of our {@link AirlineService}, which
	 * implements the business logic of the application under test.
	 */
	private AirlineService services;
	
	/**
	 * This field holds a basic manager for the airline entities. In a real
	 * world application this could be replaced by a Data Access Object (DAO).
	 */
	private InMemoryPersistenceContext persistenceContext;

	/**
	 * Sets up the context for the test. To do so, we create an instance of the
	 * {@link AirlineService} (which is used to perform business operations) and
	 * of the {@link InMemoryPersistenceContext} to create and store entities.
	 */
	@Before
	public void setUp() {
		services = new AirlineService();
		persistenceContext = InMemoryPersistenceContext.getPersistenceContext();
	}

	/**
	 * Cleans up after the test run.
	 */
	@After
	public void tearDown() {
		services = null;
	}

	/**
	 * Tests whether booking a seat for passenger John Doe succeeds, assuming 
	 * there are enough free seats available. It also tests whether a second
	 * booking operation for the same passenger fails. This is equivalent to
	 * the code that is generated for the {@link BookSeatTwice} scenario.
	 */
	@Test
	public void testSeatBooking() {
		// Create a new passenger for whom we want to book a set
		Passenger passenger = persistenceContext.createPassenger("John", "Doe");
		// Make sure the passenger was created
		assertNotNull(passenger);

		// Create a new type of airplane which will be used to execute the test
		// flight
		AirplaneType airplane = persistenceContext.createAirplaneType("Boeing 737");
		// Make sure the airplane was created
		assertNotNull(airplane);
		// An airplane of this type has 20 seats in total
		airplane.setTotalSeats(20);
		
		// Create a new flight for which we want to book John Doe
		Flight flight = persistenceContext.createFlight("LH-1234");
		// Make sure the flight was created
		assertNotNull(flight);
		// Assume we have 20 free seats on the flight...
		flight.setFreeSeats(20);
		// ...and that a Boeing 737 is used to execute the flight.
		flight.setAirplane(airplane);

		// Now, book a seat for John Doe...
		OperationStatus status = services.bookSeat(passenger, flight);
		// ...and make sure this succeeds.
		assertTrue(status.getMessage(), status.isValid());

		// Then, try to book a second seat for John Doe...
		status = services.bookSeat(passenger, flight);
		// ...and make sure this fails.
		assertFalse(status.getMessage(), status.isValid());
	}
}
