package com.nat_spec.airline.example.test.scenarios;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nat_spec.airline.example.persistence.InMemoryPersistenceContext;
import com.nat_spec.airline.example.service.AirlineService;
import com.nat_spec.airline.example.testsupport.TestSupport;

/**
 * This class serves as a template for all classes that are generated for
 * NatSpec scenarios (.natspec files). It is recognized by NatSpec based on its
 * special name and applies to all scenarios in the same package and all sub
 * packages (unless there is another template class in one of the sub packages).
 * <p>
 * This particular template is a JUnit test case where the concrete steps of the
 * test are filled by the NatSpec code generator (see {@link #executeScript()}).
 */
public class _NatSpecTemplate {

	/**
	 * This field holds the business service class that is used throughout the
	 * test.
	 */
	private AirlineService service;

	/**
	 * This field holds the entity manager that is used throughout the test to
	 * store entities.
	 */
	private InMemoryPersistenceContext persistenceContext;

	/**
	 * This field holds the class that provides the test support methods which
	 * make the sentences in the .natspec files executable. Having this field is
	 * essential because NatSpec determines the set of available sentences from
	 * the types of the fields declared in this template.
	 */
	protected TestSupport testSupport;

	/**
	 * This method is filled by NatSpec with the concrete code that is defined
	 * by the .natspec file for which this template is instantiated. The comment
	 * <code>@MethodBody</code> is replaced with the generated code for all the
	 * sentences in the NatSpec scenario.
	 * 
	 * @throws Exception if something goes wrong
	 */
	@SuppressWarnings("unused")
	@Test
	public void executeScript() throws Exception {
		int result;
		/* @MethodBody */
	}

	/**
	 * Sets up the test environment (i.e., initialize the service class, the
	 * entity manager and the test support class).
	 */
	@Before
	public void setUp() {
		service = new AirlineService();
		persistenceContext = InMemoryPersistenceContext.getPersistenceContext();
		testSupport = new TestSupport(service, persistenceContext);
	}

	/**
	 * Tears down the test environment (i.e., the test support class).
	 */
	@After
	public void shutdown() {
		testSupport = null;
	}
}
