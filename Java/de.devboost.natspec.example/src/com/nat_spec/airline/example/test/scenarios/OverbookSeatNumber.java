package com.nat_spec.airline.example.test.scenarios;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.nat_spec.airline.example.persistence.InMemoryPersistenceContext;
import com.nat_spec.airline.example.service.AirlineService;
import com.nat_spec.airline.example.testsupport.TestSupport;

/**
 * This class serves as a template for all classes that are generated for
 * NatSpec scenarios (.natspec files). It is recognized by NatSpec based on its
 * special name and applies to all scenarios in the same package and all sub
 * packages (unless there is another template class in one of the sub packages).
 * <p>
 * This particular template is a JUnit test case where the concrete steps of the
 * test are filled by the NatSpec code generator (see {@link #executeScript()}).
 */
public class OverbookSeatNumber {

	/**
	 * This field holds the business service class that is used throughout the
	 * test.
	 */
	private AirlineService service;

	/**
	 * This field holds the entity manager that is used throughout the test to
	 * store entities.
	 */
	private InMemoryPersistenceContext persistenceContext;

	/**
	 * This field holds the class that provides the test support methods which
	 * make the sentences in the .natspec files executable. Having this field is
	 * essential because NatSpec determines the set of available sentences from
	 * the types of the fields declared in this template.
	 */
	protected TestSupport testSupport;

	/**
	 * This method is filled by NatSpec with the concrete code that is defined
	 * by the .natspec file for which this template is instantiated. The comment
	 * <code>@MethodBody</code> is replaced with the generated code for all the
	 * sentences in the NatSpec scenario.
	 * 
	 * @throws Exception if something goes wrong
	 */
	@SuppressWarnings("unused")
	@Test
	public void executeScript() throws Exception {
		int result;
		// The code in this method is generated from: /de.devboost.natspec.example/src/com/nat_spec/airline/example/test/scenarios/OverbookSeatNumber.natspec
		// Never change this method or any contents of this file, all local changes will be overwritten.
		// Change _NatSpecTemplate.java instead.
		
		// Given an airplane Boeing-737-600
		com.nat_spec.airline.example.persistence.entity.AirplaneType airplaneType_Boeing_737_600 = testSupport.givenAnAirplane("Boeing-737-600");
		// Given a flight LH-1234
		com.nat_spec.airline.example.persistence.entity.Flight flight_LH_1234 = testSupport.givenAFlight("LH-1234");
		// that is executed using a Boeing-737-600
		testSupport.withAirplane(airplaneType_Boeing_737_600, flight_LH_1234);
		// with 2 free seats
		testSupport.withFreeSeats(2, flight_LH_1234);
		// Given a passenger John Doe
		com.nat_spec.airline.example.persistence.entity.Passenger passenger_John_Doe = testSupport.givenAPassenger("John", "Doe");
		// Book seat for John Doe at LH-1234
		com.nat_spec.airline.example.service.OperationStatus operationStatus_John_Doe_LH_1234 = testSupport.bookSeat(passenger_John_Doe, flight_LH_1234);
		// Assume a valid ticket is issued
		testSupport.assumeStatusValid(operationStatus_John_Doe_LH_1234);
		// Given a passenger Jane Doe
		com.nat_spec.airline.example.persistence.entity.Passenger passenger_Jane_Doe = testSupport.givenAPassenger("Jane", "Doe");
		// Book seat for Jane Doe at LH-1234
		com.nat_spec.airline.example.service.OperationStatus operationStatus_Jane_Doe_LH_1234 = testSupport.bookSeat(passenger_Jane_Doe, flight_LH_1234);
		// Assume a valid ticket is issued
		testSupport.assumeStatusValid(operationStatus_Jane_Doe_LH_1234);
		// Given a passenger Jim Doe
		com.nat_spec.airline.example.persistence.entity.Passenger passenger_Jim_Doe = testSupport.givenAPassenger("Jim", "Doe");
		// Book seat for Jim Doe at LH-1234
		com.nat_spec.airline.example.service.OperationStatus operationStatus_Jim_Doe_LH_1234 = testSupport.bookSeat(passenger_Jim_Doe, flight_LH_1234);
		// Assume no valid ticket is issued
		testSupport.assumeStatusInvalid(operationStatus_Jim_Doe_LH_1234);
		
	}

	/**
	 * Sets up the test environment (i.e., initialize the service class, the
	 * entity manager and the test support class).
	 */
	@Before
	public void setUp() {
		service = new AirlineService();
		persistenceContext = InMemoryPersistenceContext.getPersistenceContext();
		testSupport = new TestSupport(service, persistenceContext);
	}

	/**
	 * Tears down the test environment (i.e., the test support class).
	 */
	@After
	public void shutdown() {
		testSupport = null;
	}
}
