package com.nat_spec.airline.example.service.validation;

import com.nat_spec.airline.example.persistence.entity.Flight;
import com.nat_spec.airline.example.persistence.entity.Passenger;
import com.nat_spec.airline.example.service.OperationStatus;

/**
 * This class serves as a template for validation rule classes that are written
 * in plain natural language. The template is instantiated by NatSpec for all
 * the .natspec files in this package. The <code>@MethodBody</code> placeholder
 * in {@link #validate()} is replaced with the code that performs the actual
 * validation. See class {@link BookingValidationRules} for a concrete example
 * validation class.
 * <p>
 * This class is an example of applying NatSpec for non-testing purposes.
 */
public class _NatSpecTemplate {

	/**
	 * This field holds a reference to the {@link ValidationSupport} class that
	 * provides all the methods to make the textual validation rules executable.
	 */
	protected ValidationSupport validationSupport;

	/**
	 * Creates a new validation class for the given flight and passenger.
	 * 
	 * @param flight the flight to validate
	 * @param passenger the passenger to validate
	 */
	public _NatSpecTemplate(Flight flight, Passenger passenger) {
		this.validationSupport = new ValidationSupport(flight, passenger);
	}

	/**
	 * Checks that all validation rules are met.
	 * 
	 * @return the status of the validation (i.e., success or failure)
	 */
	public OperationStatus validate() {
		/* @MethodBody */
		return validationSupport.getStatus();
	}
}
