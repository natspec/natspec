package com.nat_spec.airline.example.service.validation;

import com.nat_spec.airline.example.persistence.entity.Flight;
import com.nat_spec.airline.example.persistence.entity.Passenger;
import com.nat_spec.airline.example.service.OperationStatus;

import de.devboost.natspec.annotations.TextSyntax;

/**
 * The {@link ValidationSupport} class defines all methods that are required to
 * execute the textual validation rules present in this example.
 */
public class ValidationSupport {

	/**
	 * The passenger to validate.
	 */
	private Passenger passenger;

	/**
	 * The flight to validate.
	 */
	private Flight flight;
	
	/**
	 * The current status of the validation.
	 */
	private OperationStatus status;

	/**
	 * Creates a new instance of this class that can be used to validate the
	 * given flight and passenger.
	 * 
	 * @param flight the flight to validate
	 * @param passenger the passenger to validate
	 */
	public ValidationSupport(Flight flight, Passenger passenger) {
		super();
		this.flight = flight;
		this.passenger = passenger;
		setStatus(new OperationStatus());
	}

	/**
	 * Returns the status of the validation. This must be called after invoking
	 * one of the check methods.
	 * 
	 * @return the validation status (i.e., success or failure)
	 */
	public OperationStatus getStatus() {
		return status;
	}
	
	/**
	 * Sets the status of this validation to invalid (failure) and adds the
	 * given message.
	 * 
	 * @param message
	 *            the message to add
	 */
	private void setInvalid(String message) {
		getStatus().setValid(false);
		getStatus().addMessage(message);
	}

	/**
	 * Sets the status of this validation.
	 * 
	 * @param status
	 *            the new status
	 */
	private void setStatus(OperationStatus status) {
		this.status = status;
	}

	/**
	 * Checks that the passenger has not already booked a seat on this flight.
	 */
	@TextSyntax("Each Passenger can only be booked once.")
	public void checkUniquePassenger() {
		if (flight.getBookedPassengerIds().contains(passenger.getId())) {
			setInvalid("A passenger can only be booked once for each flight.");
		}
	}

	/**
	 * Checks that the given number of seats is held free.
	 */
	@TextSyntax("There should be at least #1 free seats to handle overbooking.")
	public void checkFreeSeats(int overbookingBuffer) {
		if (flight.getFreeSeats() < overbookingBuffer) {
			setInvalid("There are no free seats for the flight.");
		}
	}

	/**
	 * Checks that there is at least one seat available.
	 */
	@TextSyntax("There needs to be a free seat for the passenger.")
	public void checkFreeSeats() {
		if (flight.getFreeSeats() < 1) {
			setInvalid("There are no free seats for the flight.");
		}
	}

	/**
	 * Checks that the passenger has at least the given age (in years).
	 */
	@TextSyntax("The Passenger needs to be at least #1 years old.")
	public void thePassengerNeedsToBeAtLeastYearsOld(int minimalAge) {
		if (passenger.getAge() < minimalAge) {
			setInvalid("The passenger needs to be at least " + minimalAge
					+ " years.");
		}
	}
}
