package com.nat_spec.airline.example.service;

import com.nat_spec.airline.example.persistence.entity.Flight;
import com.nat_spec.airline.example.persistence.entity.Passenger;
import com.nat_spec.airline.example.service.validation.BookingValidationRules;

/**
 * The {@link AirlineService} provides very basic business functionality for the
 * airline domain. It allows to book seats for passengers on flights (see
 * {@link #bookSeat(Passenger, Flight)}) and to cancel these seats (see
 * {@link #cancelSeat(Passenger, Flight)}).
 */
public class AirlineService {

	/**
	 * Tries to book a seat for the given passenger on the given flight. Before
	 * the seat is actually booked some validation rules are checked to make
	 * sure the constraints of the domain (e.g., that a passenger can book at
	 * most one seat for a flight) are met. If one of the validation rules is
	 * violated, this operation fails.
	 * 
	 * @param passenger
	 *            the passenger to book the seat for
	 * @param flight
	 *            the flight to book the seat on
	 * @return a status object representing the operations success (or failure)
	 */
	public OperationStatus bookSeat(Passenger passenger, Flight flight) {
		BookingValidationRules bookingValidation = new BookingValidationRules(flight,
				passenger);
		OperationStatus status = bookingValidation.validate();
		if (status.isValid()) {
			flight.addPassenger(passenger);
		}
		return status;
	}

	/**
	 * Cancels the seat booked by the given passenger on the given flight. This
	 * operation can fail if the passenger has not booked a seat on the given
	 * flight.
	 * 
	 * @param passenger
	 *            the passenger to cancel the seat for
	 * @param flight
	 *            the flight to cancel the seat on
	 * @return a status object representing the operations success (or failure)
	 */
	public OperationStatus cancelSeat(Passenger passenger, Flight flight) {
		if (flight.removePassenger(passenger)) {
			return new OperationStatus("Passenger was removed", true);
		}
		return new OperationStatus("Passenger was not booked on flight", false);
	}
}
