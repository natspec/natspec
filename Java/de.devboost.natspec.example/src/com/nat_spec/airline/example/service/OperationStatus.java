package com.nat_spec.airline.example.service;

import java.util.Date;

/**
 * Objects of type {@link OperationStatus} are used to capture the result of
 * business operations. Each result comprises of a message (see
 * {@link #getMessage()}) and a validity flag (see {@link #isValid()}) which
 * indicates success or failure of the operation.
 */
public class OperationStatus {

	/**
	 * A message describing the human readable status of the operation.
	 */
	private String message;
	
	/**
	 * A flag that indicates success (<code>true</code>) or failure (
	 * <code>false</code>) of the operation.
	 */
	private boolean valid;
	
	/**
	 * The creation date of this operation status.
	 */
	private Date creationDate;

	/**
	 * Creates a new status object for an operation.
	 * 
	 * @param message a message describing the operations status
	 * @param valid a flag indicating success or failure
	 */
	public OperationStatus(String message, boolean valid) {
		super();
		this.message = message;
		this.valid = valid;
		this.creationDate = new Date();
	}

	/**
	 * Creates a new default status object for an operation. The message is left
	 * empty and the operation is assumed to be successful.
	 */
	public OperationStatus() {
		this("", true);
	}

	/**
	 * Returns the human readable message for the operations status.
	 * 
	 * @return the status message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Adds more text to the operations status message.
	 * 
	 * @param text the text to add
	 */
	public void addMessage(String text) {
		setMessage(getMessage() + text + "\n");
	}

	/**
	 * Returns a string describing the operations validity.
	 */
	private String getValidStatus() {
		if (isValid()) {
			return "Operation Valid";
		} else {
			return "Operation Invalid";
		}
	}

	/**
	 * Returns the status flag for the operation.
	 * 
	 * @return <code>true</code> if the operation was successful, otherwise
	 *         <code>false</code>
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * Sets the status flag for the operation.
	 * 
	 * @param valid
	 *            <code>true</code> if the operation was successful, otherwise
	 *            <code>false</code>
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	/**
	 * Sets the message for the operations status.
	 * 
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * Returns the creation date of this operation status.
	 */
	public Date getCreationDate() {
		return creationDate;
	}

	/**
	 * Returns a string representation of this operation status.
	 */
	@Override
	public String toString() {
		return getValidStatus() + " " + getCreationDate() + " : " + getMessage();
	}
}
