package com.nat_spec.airline.example.testsupport;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;

import com.nat_spec.airline.example.persistence.InMemoryPersistenceContext;
import com.nat_spec.airline.example.persistence.entity.AirplaneType;
import com.nat_spec.airline.example.persistence.entity.Flight;
import com.nat_spec.airline.example.persistence.entity.Passenger;
import com.nat_spec.airline.example.service.AirlineService;
import com.nat_spec.airline.example.service.OperationStatus;
import com.nat_spec.airline.example.service.validation._NatSpecTemplate;

import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;

/**
 * This class contains all test support methods that are required to make the
 * example test specifications executable. Methods that are annotated with the
 * {@link TextSyntax} annotation are recognized by NatSpec.
 * <p>
 * This class is parameterized by two parameters that are passed to the
 * constructor (the service class which implements the business logic and the
 * entity manager that provides access to all entities).
 */
public class TestSupport {

	/**
	 * This fields holds a reference to the business service implementation.
	 */
	private AirlineService service;

	/**
	 * This fields holds a reference to the entity manager.
	 */
	private InMemoryPersistenceContext persistenceContext;

	/**
	 * Creates a new {@link TestSupport} instance. This constructor is usually
	 * invoked from a {@link _NatSpecTemplate} or one of the classes that are
	 * generated from such a template.
	 * 
	 * @param service the business service logic
	 * @param persistenceContext the entity manager to access entities
	 */
	public TestSupport(AirlineService service,
			InMemoryPersistenceContext persistenceContext) {
		super();
		this.service = service;
		this.persistenceContext = persistenceContext;
	}

	/**
	 * Creates a new passenger.
	 * 
	 * @param firstname
	 *            the first name of the passenger
	 * @param lastname
	 *            the last name of the passenger
	 * @return the newly created passenger
	 */
	@TextSyntax("Given a Passenger #1 #2")
	public Passenger givenAPassenger(String firstname, String lastname) {
		return persistenceContext.createPassenger(firstname, lastname);
	}

	/**
	 * Sets the age of the given passenger. The passenger is expected to be
	 * contained in the implicit context (i.e., it must be created by a previous
	 * sentence/test support method). The name of the passenger is not
	 * explicitly required here, because there is no placeholder for the
	 * passenger parameter (i.e., #1 is missing).
	 * 
	 * @param passenger
	 *            the passenger to set the age for
	 * @param age
	 *            the new age for the passenger
	 */
	@TextSyntax("with age of #2 years")
	public void setAge(Passenger passenger, int age) {
		passenger.setAge(age);
	}

	/**
	 * Creates a new type of airplane.
	 * 
	 * @param name
	 *            the name of the type of airplane
	 * @return the newly created airplane type
	 */
	@TextSyntax("Given an Airplane #1")
	public AirplaneType givenAnAirplane(String name) {
		return persistenceContext.createAirplaneType(name);
	}

	/**
	 * Creates a new flight.
	 * 
	 * @param name
	 *            the name of the flight
	 * @return the newly created flight
	 */
	@TextSyntax("Given a flight #1")
	public Flight givenAFlight(String flightName) {
		return persistenceContext.createFlight(flightName);
	}

	/**
	 * Assigns the type of airplane that is used to execute the last mentioned
	 * flight.
	 * 
	 * @param airplaneType
	 *            the type of airplane to assign to the flight
	 * @param flight
	 *            the flight for which to set the airplane type (must be
	 *            available from context)
	 */
	@TextSyntax("that is executed using a #1")
	public void withAirplane(AirplaneType airplaneType, Flight flight) {
		flight.setAirplane(airplaneType);
		persistenceContext.update(flight);
	}

	/**
	 * Sets the number of free seats for the flight mentioned last.
	 * 
	 * @param numberOfSeats
	 *            the number of free seats
	 * @param flight
	 *            the flight to set the seats for (must be available from
	 *            context).
	 */
	@TextSyntax("with #1 free seats")
	public void withFreeSeats(int numberOfSeats, Flight flight) {
		flight.setFreeSeats(numberOfSeats);
		persistenceContext.update(flight);
	}

	/**
	 * Tries to book a seat for the given passenger on the given flight.
	 * 
	 * @param passenger
	 *            the passenger to book the flight for
	 * @param flight
	 *            the flight to book the seat on
	 * @return the status of the booking operation which can be validated using
	 *         {@link #assumeAValidTicketIsIssued(OperationStatus)} or
	 *         {@link #assumeStatusInvalid(OperationStatus)}.
	 */
	@TextSyntax("Book seat for #1 at #2")
	public OperationStatus bookSeat(Passenger passenger, Flight flight) {
		return service.bookSeat(passenger, flight);
	}

	/**
	 * Tries to cancel the given passenger on the given flight.
	 * 
	 * @param passenger
	 *            the passenger to cancel the flight for
	 * @param flight
	 *            the flight to cancel
	 * @return the status of the cancel operation which can be validated using
	 *         {@link #assumeAValidTicketIsIssued(OperationStatus)} or
	 *         {@link #assumeStatusInvalid(OperationStatus)}.
	 */
	@TextSyntax("Cancel seat for #1 at #2")
	public OperationStatus cancelSeat(Passenger passenger, Flight flight) {
		return service.cancelSeat(passenger, flight);
	}

	/**
	 * Checks that the validity of the last operation is <code>true</code>
	 * (i.e., that the operation was successful). This method has multiple
	 * {@link TextSyntax} annotations to map multiple sentences to this method.
	 * 
	 * @param status
	 *            the status of the last operation (must be implicitly available
	 *            from context).
	 */
	@TextSyntaxes({
		@TextSyntax("Assume a valid ticket is issued"),
		@TextSyntax("Assume cancellation successful")
	})
	public void assumeStatusValid(OperationStatus status) {
		assertTrue(status.getMessage(), status.isValid());
	}

	/**
	 * Checks that the validity of the last executed business operations is
	 * <code>false</code>.
	 * 
	 * @param status
	 *            the status of the last operation (implicit parameter, not
	 *            explicitly mentioned in sentence)
	 */
	@TextSyntax("Assume no valid ticket is issued")
	public void assumeStatusInvalid(OperationStatus status) {
		Assert.assertFalse(status.toString(), status.isValid());
	}

	/**
	 * Checks that the given passenger is booked for the given flight. Both the
	 * flight and the passenger must be referenced explicitly.
	 * 
	 * @param flight the flight to check
	 * @param passenger the passenger to check
	 */
	@TextSyntax("Assume #1 has passenger #2")
	public void hasPassenger(Flight flight, Passenger passenger) {
		Assert.assertTrue(flight.hasPassenger(passenger));
	}
}
