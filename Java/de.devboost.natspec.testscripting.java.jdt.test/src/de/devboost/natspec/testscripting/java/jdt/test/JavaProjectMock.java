package de.devboost.natspec.testscripting.java.jdt.test;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jdt.core.IBuffer;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IOpenable;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IRegion;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.WorkingCopyOwner;
import org.eclipse.jdt.core.eval.IEvaluationContext;
import org.eclipse.jdt.internal.core.JavaProject;

@SuppressWarnings("restriction")
public class JavaProjectMock extends JavaProject {

	private Map<String, IType> nameToTypeMap = new LinkedHashMap<String, IType>();

	@Override
	public IJavaElement[] getChildren() throws JavaModelException {

		return null;
	}

	@Override
	public boolean hasChildren() throws JavaModelException {

		return false;
	}

	@Override
	public boolean exists() {

		return false;
	}

	@Override
	public IJavaElement getAncestor(int ancestorType) {

		return null;
	}

	@Override
	public String getAttachedJavadoc(IProgressMonitor monitor) throws JavaModelException {

		return null;
	}

	@Override
	public IResource getCorrespondingResource() throws JavaModelException {

		return null;
	}

	@Override
	public String getElementName() {

		return null;
	}

	@Override
	public int getElementType() {

		return 0;
	}

	@Override
	public String getHandleIdentifier() {

		return null;
	}

	@Override
	public IJavaModel getJavaModel() {

		return null;
	}

	@Override
	public IJavaProject getJavaProject() {

		return null;
	}

	@Override
	public IOpenable getOpenable() {

		return null;
	}

	@Override
	public IJavaElement getParent() {

		return null;
	}

	@Override
	public IPath getPath() {

		return null;
	}

	@Override
	public IJavaElement getPrimaryElement() {

		return null;
	}

	@Override
	public IResource getResource() {

		return null;
	}

	@Override
	public ISchedulingRule getSchedulingRule() {

		return null;
	}

	@Override
	public IResource getUnderlyingResource() throws JavaModelException {

		return null;
	}

	@Override
	public boolean isReadOnly() {

		return false;
	}

	@Override
	public boolean isStructureKnown() throws JavaModelException {

		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Object getAdapter(Class adapter) {

		return null;
	}

	@Override
	public void close() throws JavaModelException {

	}

	@Override
	public String findRecommendedLineSeparator() throws JavaModelException {

		return null;
	}

	@Override
	public IBuffer getBuffer() throws JavaModelException {

		return null;
	}

	@Override
	public boolean hasUnsavedChanges() throws JavaModelException {

		return false;
	}

	@Override
	public boolean isOpen() {

		return false;
	}

	@Override
	public void makeConsistent(IProgressMonitor progress) throws JavaModelException {

	}

	@Override
	public void open(IProgressMonitor progress) throws JavaModelException {

	}

	@Override
	public void save(IProgressMonitor progress, boolean force) throws JavaModelException {

	}

	@Override
	public IClasspathEntry decodeClasspathEntry(String encodedEntry) {

		return null;
	}

	@Override
	public String encodeClasspathEntry(IClasspathEntry classpathEntry) {

		return null;
	}

	@Override
	public IJavaElement findElement(IPath path) throws JavaModelException {

		return null;
	}

	@Override
	public IJavaElement findElement(IPath path, WorkingCopyOwner owner) throws JavaModelException {

		return null;
	}

	@Override
	public IJavaElement findElement(String bindingKey, WorkingCopyOwner owner) throws JavaModelException {

		return null;
	}

	@Override
	public IPackageFragment findPackageFragment(IPath path) throws JavaModelException {

		return null;
	}

	@Override
	public IPackageFragmentRoot findPackageFragmentRoot(IPath path) throws JavaModelException {

		return null;
	}

	@Override
	public IPackageFragmentRoot[] findPackageFragmentRoots(IClasspathEntry entry) {

		return null;
	}

	@Override
	public IType findType(String fullyQualifiedName) throws JavaModelException {
		return nameToTypeMap.get(fullyQualifiedName);
	}

	@Override
	public IType findType(String fullyQualifiedName, IProgressMonitor progressMonitor) throws JavaModelException {

		return null;
	}

	@Override
	public IType findType(String fullyQualifiedName, WorkingCopyOwner owner) throws JavaModelException {

		return null;
	}

	@Override
	public IType findType(String fullyQualifiedName, WorkingCopyOwner owner, IProgressMonitor progressMonitor)
			throws JavaModelException {

		return null;
	}

	@Override
	public IType findType(String packageName, String typeQualifiedName) throws JavaModelException {

		return null;
	}

	@Override
	public IType findType(String packageName, String typeQualifiedName, IProgressMonitor progressMonitor)
			throws JavaModelException {

		return null;
	}

	@Override
	public IType findType(String packageName, String typeQualifiedName, WorkingCopyOwner owner)
			throws JavaModelException {

		return null;
	}

	@Override
	public IType findType(String packageName, String typeQualifiedName, WorkingCopyOwner owner,
			IProgressMonitor progressMonitor) throws JavaModelException {

		return null;
	}

	@Override
	public IPackageFragmentRoot[] getAllPackageFragmentRoots() throws JavaModelException {

		return null;
	}

	@Override
	public Object[] getNonJavaResources() throws JavaModelException {

		return null;
	}

	@Override
	public String getOption(String optionName, boolean inheritJavaCoreOptions) {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getOptions(boolean inheritJavaCoreOptions) {

		return null;
	}

	@Override
	public IPath getOutputLocation() throws JavaModelException {

		return null;
	}

	@Override
	public IPackageFragmentRoot getPackageFragmentRoot(String externalLibraryPath) {

		return null;
	}

	@Override
	public IPackageFragmentRoot getPackageFragmentRoot(IResource resource) {

		return null;
	}

	@Override
	public IPackageFragmentRoot[] getPackageFragmentRoots() throws JavaModelException {

		return null;
	}

	@Override
	public IPackageFragmentRoot[] getPackageFragmentRoots(IClasspathEntry entry) {

		return null;
	}

	@Override
	public IPackageFragment[] getPackageFragments() throws JavaModelException {

		return null;
	}

	@Override
	public IProject getProject() {

		return null;
	}

	@Override
	public IClasspathEntry[] getRawClasspath() throws JavaModelException {

		return null;
	}

	@Override
	public String[] getRequiredProjectNames() throws JavaModelException {

		return null;
	}

	@Override
	public IClasspathEntry[] getResolvedClasspath(boolean ignoreUnresolvedEntry) throws JavaModelException {

		return null;
	}

	@Override
	public boolean hasBuildState() {

		return false;
	}

	@Override
	public boolean hasClasspathCycle(IClasspathEntry[] entries) {

		return false;
	}

	@Override
	public boolean isOnClasspath(IJavaElement element) {

		return false;
	}

	@Override
	public boolean isOnClasspath(IResource resource) {

		return false;
	}

	@Override
	public IEvaluationContext newEvaluationContext() {

		return null;
	}

	@Override
	public ITypeHierarchy newTypeHierarchy(IRegion region, IProgressMonitor monitor) throws JavaModelException {

		return null;
	}

	@Override
	public ITypeHierarchy newTypeHierarchy(IRegion region, WorkingCopyOwner owner, IProgressMonitor monitor)
			throws JavaModelException {

		return null;
	}

	@Override
	public ITypeHierarchy newTypeHierarchy(IType type, IRegion region, IProgressMonitor monitor)
			throws JavaModelException {

		return null;
	}

	@Override
	public ITypeHierarchy newTypeHierarchy(IType type, IRegion region, WorkingCopyOwner owner, IProgressMonitor monitor)
			throws JavaModelException {

		return null;
	}

	@Override
	public IPath readOutputLocation() {

		return null;
	}

	@Override
	public IClasspathEntry[] readRawClasspath() {

		return null;
	}

	@Override
	public void setOption(String optionName, String optionValue) {

	}

	@SuppressWarnings("rawtypes")
	@Override
	public void setOptions(Map newOptions) {

	}

	@Override
	public void setOutputLocation(IPath path, IProgressMonitor monitor) throws JavaModelException {

	}

	@Override
	public void setRawClasspath(IClasspathEntry[] entries, IPath outputLocation, boolean canModifyResources,
			IProgressMonitor monitor) throws JavaModelException {

	}

	@Override
	public void setRawClasspath(IClasspathEntry[] entries, boolean canModifyResources, IProgressMonitor monitor)
			throws JavaModelException {

	}

	@Override
	public void setRawClasspath(IClasspathEntry[] entries, IClasspathEntry[] referencedEntries, IPath outputLocation,
			IProgressMonitor monitor) throws JavaModelException {

	}

	@Override
	public IClasspathEntry[] getReferencedClasspathEntries() throws JavaModelException {

		return null;
	}

	@Override
	public void setRawClasspath(IClasspathEntry[] entries, IProgressMonitor monitor) throws JavaModelException {

	}

	@Override
	public void setRawClasspath(IClasspathEntry[] entries, IPath outputLocation, IProgressMonitor monitor)
			throws JavaModelException {

	}

	public void addType(String typeName, TypeMock typeMock) {
		nameToTypeMap.put(typeName, typeMock);
	}
}
