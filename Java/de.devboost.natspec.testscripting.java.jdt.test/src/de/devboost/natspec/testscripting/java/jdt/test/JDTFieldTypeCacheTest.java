package de.devboost.natspec.testscripting.java.jdt.test;

import static org.junit.Assert.assertEquals;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IType;
import org.junit.Test;

import de.devboost.natspec.testscripting.Field;
import de.devboost.natspec.testscripting.java.jdt.JDTFieldTypeCache;

public class JDTFieldTypeCacheTest {
	
	private Map<IFile, IType> fileToTypeMap = new LinkedHashMap<IFile, IType>();
	private Map<IType, IType> typeToSuperTypeMap = new LinkedHashMap<IType, IType>();

	private final class JDTFieldTypeCacheMock extends JDTFieldTypeCache {
		
		public JDTFieldTypeCacheMock() {
			super();
		}
		
		@Override
		public Map<String, Set<Field>> getFileToFieldTypesMap() {
			return super.getFileToFieldTypesMap();
		}
		
		@Override
		public Map<String, String> getTypeToSuperTypeMap() {
			return super.getTypeToSuperTypeMap();
		}
		
		@Override
		protected IType getType(IFile file, IProject referencingProject) {
			return fileToTypeMap.get(file);
		}

		@Override
		protected IType getSuperType(IType type, IProject templateFileProject) {
			return typeToSuperTypeMap.get(type);
		}
		
		@Override
		protected IFile getFile(IType type) {
			for (IFile file : fileToTypeMap.keySet()) {
				IType nextType = fileToTypeMap.get(file);
				if (nextType.equals(type)) {
					return file;
				}
			}
			return null;
		}
		
		@Override
		protected String getPath(IType type) {
			IFile file = getFile(type);
			IPath fullPath = file.getFullPath();
			return fullPath.toString();
		}
	}

	/**
	 * Checks that fields with the same name are handled correctly by the
	 * {@link JDTFieldTypeCache}.
	 */
	@Test
	public void testFieldInheritanceHandling() {
		JDTFieldTypeCacheMock cache = new JDTFieldTypeCacheMock();
		
		String subclassFilePath = "Subclass.java";
		String superclassFilePath = "Superclass.java";

		final IFile subclassFile = new FileMock(subclassFilePath);
		Set<Field> subclassFields = new LinkedHashSet<Field>();
		subclassFields.add(new Field("f1", "T1"));
		subclassFields.add(new Field("f2", "T2"));
		
		final IFile superclassFile = new FileMock(superclassFilePath);
		Set<Field> superclassFields = new LinkedHashSet<Field>();
		superclassFields.add(new Field("f1", "SuperT1"));
		superclassFields.add(new Field("f2", "SuperT2"));
		
		cache.getTypeToSuperTypeMap().put(subclassFilePath, superclassFilePath);
		cache.getTypeToSuperTypeMap().put(superclassFilePath, null);
		
		cache.getFileToFieldTypesMap().put(subclassFilePath, subclassFields);
		cache.getFileToFieldTypesMap().put(superclassFilePath, superclassFields);
		
		IType subclass = new TypeMock();
		fileToTypeMap.put(subclassFile, subclass);
		IType superclass = new TypeMock();
		fileToTypeMap.put(superclassFile, superclass);
		
		typeToSuperTypeMap.put(subclass, superclass);

		Map<String, String> nameToTypeMap = cache.getFieldNameToTypeMap(subclassFile);
		assertEquals("Wrong type for field 'f1'.", "T1", nameToTypeMap.get("f1"));
		assertEquals("Wrong type for field 'f2'.", "T2", nameToTypeMap.get("f2"));
	}
}
