package de.devboost.natspec.jdt.ui;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;

import de.devboost.natspec.testscripting.java.GeneratedJavaFileFinder;

public class LaunchShortcutDelegator {
	
	private final ILaunchDelegate delegate;
	
	public LaunchShortcutDelegator(ILaunchDelegate delegate) {
		super();
		this.delegate = delegate;
	}

	public void launch(IEditorPart editor, String mode) {
		if (editor == null) {
			return;
		}
		
        IEditorInput input = editor.getEditorInput();
        IFile file = getIFileFromAdaptable(input);
		if (file == null) {
			return;
		}

		launchFile(file, mode);
	}

	public void launch(ISelection selection, String mode) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			Object obj = ssel.getFirstElement();
			IFile file = getIFileFromAdaptable(obj);
			if (file == null) {
				return;
			}
			launchFile(file, mode);
		}
	}

	private IFile getIFileFromAdaptable(Object input) {
		IAdapterManager adapterManager = Platform.getAdapterManager();
		IFile file = (IFile) adapterManager.getAdapter(input, IFile.class);
		if (file != null) {
			return file;
		}
		if (input instanceof IAdaptable) {
			IAdaptable adaptable = (IAdaptable) input;
			return (IFile) adaptable.getAdapter(IFile.class);
		}
		return null;
	}

	private void launchFile(IFile natspecFile, String mode) {
		GeneratedJavaFileFinder finder = new GeneratedJavaFileFinder();
		IFile testFile = finder.getGeneratedFileForSpecification(natspecFile);
		StructuredSelection selection = new StructuredSelection(testFile);
		
		delegate.launchSelection(selection, mode);
	}
}
