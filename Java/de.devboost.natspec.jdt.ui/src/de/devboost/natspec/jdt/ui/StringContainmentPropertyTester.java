package de.devboost.natspec.jdt.ui;

import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;

import com.google.common.io.ByteStreams;

import de.devboost.natspec.testscripting.java.JavaTemplateFileFinder;

public abstract class StringContainmentPropertyTester extends PropertyTester {

	@Override
	public boolean test(Object receiver, String property, Object[] args,
			Object expectedValue) {
		
		if (receiver instanceof IFile) {
			IFile file = (IFile) receiver;
			IFile templateFile = JavaTemplateFileFinder.INSTANCE.findTemplateFile(file);
			
			byte[] byteArray;
			InputStream contents = null;
			try {
				contents = templateFile.getContents();
				byteArray = ByteStreams.toByteArray(contents);
				String content = new String(byteArray, templateFile.getCharset());
				if (content.contains(getSearchString())) {
					return true;
				}
			} catch (CoreException e) {
				// Ignore
			} catch (IOException e) {
				// Ignore
			} finally {
				if (contents != null) {
					try {
						contents.close();
					} catch (IOException e) {
						// Ignore
					}
				}
			}
		}
		return false;
	}

	protected abstract String getSearchString();
}
