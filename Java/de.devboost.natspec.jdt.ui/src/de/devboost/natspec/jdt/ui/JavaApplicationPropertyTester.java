package de.devboost.natspec.jdt.ui;

/**
 * The {@link JavaApplicationPropertyTester} is used by Eclipse to determine
 * whether the {@link RunNatSpecAsJavaApplicationShortcut} is enabled or not. To
 * do so, it checks whether the NatSpec template contains the text
 * <code>public static void main(</code>. This is not exact, but running the JDT
 * parser to figure out whether the template defines a main method seems to be
 * overkill.
 */
public class JavaApplicationPropertyTester extends StringContainmentPropertyTester {

	@Override
	protected String getSearchString() {
		return "public static void main(";
	}
}
