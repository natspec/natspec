package de.devboost.natspec.jdt.ui;

import org.eclipse.jdt.debug.ui.launchConfigurations.JavaApplicationLaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;

public class RunNatSpecAsJavaApplicationShortcut extends
		JavaApplicationLaunchShortcut implements ILaunchDelegate {

	@Override
	public void launch(IEditorPart editor, String mode) {
		new LaunchShortcutDelegator(this).launch(editor, mode);
	}

	@Override
	public void launch(ISelection selection, String mode) {
		new LaunchShortcutDelegator(this).launch(selection, mode);
	}

	@Override
	public void launchSelection(ISelection selection, String mode) {
		super.launch(selection, mode);
	}
}
