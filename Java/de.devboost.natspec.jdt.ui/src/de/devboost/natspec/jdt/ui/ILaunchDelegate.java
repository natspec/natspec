package de.devboost.natspec.jdt.ui;

import org.eclipse.jface.viewers.ISelection;

public interface ILaunchDelegate {

	public void launchSelection(ISelection selection, String mode);

}
