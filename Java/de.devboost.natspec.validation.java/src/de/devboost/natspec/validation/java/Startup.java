package de.devboost.natspec.validation.java;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.ui.IStartup;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.java.jdt.ConcreteJDTUtility;
import de.devboost.natspec.validation.IValidationPredicate;
import de.devboost.natspec.validation.NatSpecValidationPlugin;

public class Startup implements IStartup {
	
	private final static JDTUtility JDT_UTILITY = ConcreteJDTUtility.INSTANCE;
	
	private final static NatspecEclipseProxy NATSPEC_ECLIPSE_PROXY = new NatspecEclipseProxy();

	@Override
	public void earlyStartup() {
		NatSpecValidationPlugin instance = NatSpecValidationPlugin.getInstance();
		instance.addValidationPredicate(new IValidationPredicate() {
			
			@Override
			public boolean shouldValidate(URI uri) {
				return !isInOutputFolder(uri);
			}
		});
	}

	private boolean isInOutputFolder(URI uri) {
		IProject project = getProject(uri);
		if (project == null) {
			return false;
		}

		String platformString;
		if (uri.isPlatformResource()) {
			platformString = uri.toPlatformString(false);
		} else {
			return true;
		}
		
		return JDT_UTILITY.isInOutputFolder(project, platformString);
	}

	private IProject getProject(URI uri) {
		IFile file = NATSPEC_ECLIPSE_PROXY.getFileForURI(uri);
		if (file == null) {
			return null;
		}

		IProject project = file.getProject();
		return project;
	}
}
