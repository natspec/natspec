package de.devboost.natspec.testscripting.java.test;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;

@RunWith(Parameterized.class)
public class SynonymsTest extends AbstractNatSpecTestCase {

	public SynonymsTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testSynonyms() throws UnsupportedTypeException {

		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Assert"));
		pattern.addPart(new StaticWord("result"));
		pattern.addPart(new StaticWord("equals"));
		ISyntaxPatternPart integerValuePart = new JavaParameterFactory()
				.createParameter(new JavaType(Integer.class));
		pattern.addPart(integerValuePart);

		ISynonymProvider synonymProvider = new ISynonymProvider() {

			@Override
			public Set<Set<String>> getSynonyms(URI contextURI) {
				Set<String> synonymSet = new LinkedHashSet<String>();
				synonymSet.add("result");
				synonymSet.add("sum");

				Set<String> synonymSet2 = new LinkedHashSet<String>();
				synonymSet2.add("equals");
				synonymSet2.add("is");

				Set<Set<String>> synonyms = new LinkedHashSet<Set<String>>();
				synonyms.add(synonymSet);
				synonyms.add(synonymSet2);
				return synonyms;
			}
		};

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null, synonymProvider);
		// should match directly
		assertMatch(pattern, "Assert result equals 1", true, context);
		// should match directly using result synonym
		assertMatch(pattern, "Assert sum equals 1", true, context);

		// should match using equals synonym
		assertMatch(pattern, "Assert result is 1", true, context);

		// should match using both synonyms
		assertMatch(pattern, "Assert sum is 1", true, context);
	}

	@After
	public void tearDown() {
		super.tearDown();
	}
}
