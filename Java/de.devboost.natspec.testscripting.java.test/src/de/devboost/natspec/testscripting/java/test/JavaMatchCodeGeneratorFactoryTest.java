package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.resource.natspec.util.NatspecResourceUtil;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.builder.CodeGenerationContext;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

@RunWith(Parameterized.class)
public class JavaMatchCodeGeneratorFactoryTest {

	private final boolean useOptimizedMatcher;
	private final boolean usePrioritizer;
	
	public JavaMatchCodeGeneratorFactoryTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super();
		this.useOptimizedMatcher = useOptimizedMatcher;
		this.usePrioritizer = usePrioritizer;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		return new BooleanCombinator().createBooleanCombinations();
	}

	@Test
	public void testUnicodeEscaping() {
		// Use Unicode sequence for 'ö' as input
		String value = "B\\u00f6rnsen"; // = B\u00f6rnsen
		// We expect upper case characters for the Unicode sequence, because the
		// sequence is unescaped (i.e., converted to the actual character) and
		// then escaped (i.e., converted back to a Unicode sequence).
		String expected = "\"B\\u00F6rnsen\""; // = "B\u00F6rnsen"
		
		assertCorrectEscaping(expected, value);
	}

	@Test
	public void testUnicodeEscaping2() {
		// Use actual 'ö' as input
		String value = "B\u00f6rnsen"; // = Börnsen
		// We expect upper case characters for the Unicode sequence, because the
		// sequence is unescaped and then escaped (i.e., converted back to a
		// Unicode sequence).
		String expected = "\"B\\u00F6rnsen\""; // == "B\u00F6rnsen"
		
		assertCorrectEscaping(expected, value);
	}

	@Test
	public void testLineBreakEscaping() {
		String value = "a\\nb"; // = a\nb
		String expected = "\"a\\nb\""; // = "a\nb"
		
		assertCorrectEscaping(expected, value);
	}

	private void assertCorrectEscaping(String expected, String value) {
		Resource resource = NatspecResourceUtil.getResource(value.getBytes());
		IPatternMatchContext matchContext = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(resource.getURI());
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		StringParameter stringParameter = new StringParameter();
		pattern.addPart(stringParameter);
		matchContext.getPatternsInContext().add(pattern);
		List<ISyntaxPatternMatch<? extends Object>> matches = new MatchService(useOptimizedMatcher, usePrioritizer).match(resource, matchContext);
		assertFalse("No match found", matches.isEmpty());
		assertEquals(1, matches.size());
		
		ISyntaxPatternMatch<? extends Object> match = matches.get(0);
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
		ISyntaxPatternPartMatch matchPart = partsToMatchesMap.get(stringParameter);

		// Invoke code generator
		JavaMatchCodeGeneratorFactory factory = new JavaMatchCodeGeneratorFactory();
		ICodeFragment generator = factory.createGenerator(matchPart);
		CodeGenerationContext context = new CodeGenerationContext();
		generator.generateSourceCode(context);
		String code = context.getCode();
		assertEquals(expected, code);
	}
}
