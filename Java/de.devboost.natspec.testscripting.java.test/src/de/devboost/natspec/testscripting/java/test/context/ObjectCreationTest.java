package de.devboost.natspec.testscripting.java.test.context;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;

@RunWith(Parameterized.class)
public class ObjectCreationTest extends AbstractNatSpecTestCase {

	public ObjectCreationTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testReturnVariableNaming1() {
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		String simpleName = "List<java.lang.String>";
		String qualifiedName = "java.util." + simpleName;
		List<String> identifiers = Collections.emptyList();

		ObjectCreation objectCreation = new ObjectCreation(context, new JavaType(qualifiedName), identifiers);
		assertEquals("list_", objectCreation.getVariableName());
	}

	/**
	 * This is a test for NATSPEC-292.
	 */
	@Test
	public void testReturnVariableNaming2() {
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		String simpleName = "String[]";
		String qualifiedName = "java.lang." + simpleName;
		List<String> identifiers = Collections.emptyList();

		ObjectCreation objectCreation = new ObjectCreation(context, new JavaType(qualifiedName), identifiers);
		// The array brackets '[]' must be replaced by underscores
		assertEquals("string___", objectCreation.getVariableName());
	}

	@Test
	public void testSetAsReturnValue() {
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		String simpleName = "Set<java.lang.String>";
		String qualifiedName = "java.util." + simpleName;

		List<ISyntaxPatternPart> createParts = Collections.singletonList((ISyntaxPatternPart) new StaticWord("create"));
		DynamicSyntaxPattern<?> createPattern = new DynamicSyntaxPattern<JavaCodeGenerator>(null, "TestSupportWithSet",
				"createSet", null, new JavaType(qualifiedName), createParts,
				Collections.<ISyntaxPatternPart, Integer> emptyMap(), null, new JavaCodeGeneratorFactory());

		List<ISyntaxPatternPart> consumeParts = new ArrayList<ISyntaxPatternPart>();
		ImplicitParameter parameter = new ImplicitParameter(qualifiedName);
		consumeParts.add(parameter);
		consumeParts.add(new StaticWord("consume"));
		Map<ISyntaxPatternPart, Integer> map = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		map.put(parameter, 1);
		DynamicSyntaxPattern<?> consumePattern = new DynamicSyntaxPattern<JavaCodeGenerator>(null,
				"TestSupportWithSet", "consumeSet", null, (JavaType) null, consumeParts, map, null,
				new JavaCodeGeneratorFactory());

		assertMatch(consumePattern, "consume", false, context);
		assertMatch(createPattern, "create", true, context);
		assertMatch(consumePattern, "consume", true, context);
	}

	@Test
	public void testOptionalImplicitParameter() {
		testOptionalImplicitParameter(true);
		testOptionalImplicitParameter(false);
	}

	private void testOptionalImplicitParameter(boolean optional) {
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		String simpleName = "Set<java.lang.String>";
		String qualifiedName = "java.util." + simpleName;

		List<ISyntaxPatternPart> consumeParts = new ArrayList<ISyntaxPatternPart>();
		ImplicitParameter optionalParameter = new ImplicitParameter(qualifiedName, false, optional);
		consumeParts.add(optionalParameter);
		consumeParts.add(new StaticWord("consume"));
		Map<ISyntaxPatternPart, Integer> map = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		map.put(optionalParameter, 1);
		DynamicSyntaxPattern<?> consumePattern = new DynamicSyntaxPattern<JavaCodeGenerator>(null,
				"TestSupportWithSet", "consumeOptionalImplicitParameter", null, (JavaType) null, consumeParts, map,
				null, new JavaCodeGeneratorFactory());

		boolean mustMatch = optional;
		assertMatch(consumePattern, "consume", mustMatch, context);
	}
}
