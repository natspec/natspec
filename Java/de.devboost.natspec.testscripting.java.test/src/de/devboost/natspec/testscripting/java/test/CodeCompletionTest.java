package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DoubleArgument;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;
import de.devboost.natspec.resource.natspec.ui.NatspecCodeCompletionHelper;
import de.devboost.natspec.resource.natspec.ui.NatspecCompletionProposal;
import de.devboost.natspec.resource.natspec.util.NatspecResourceUtil;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.test.ITestDataProvider3;
import de.devboost.natspec.test.SyntaxPatternMock;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.java.test.patterns.ImplicitParameterPattern;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.parts.BooleanParameter;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.ListParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;
import de.devboost.natspec.testscripting.patterns.parts.WrappedPart;

@RunWith(Parameterized.class)
public class CodeCompletionTest extends AbstractNatSpecTestCase {

	public static class ExpectedProposal {

		private final String displayString;
		private final String insertString;
		private final String prefix;

		public ExpectedProposal(String displayString, String insertString, String prefix) {
			this.displayString = displayString;
			this.insertString = insertString;
			this.prefix = prefix;
		}

		public String getDisplayString() {
			return displayString;
		}

		public String getInsertString() {
			return insertString;
		}

		public String getPrefix() {
			return prefix;
		}

		@Override
		public String toString() {
			return displayString;
		}
	}

	private final ISyntaxPattern<?>[] patterns;
	private final String testText;
	private final ExpectedProposal[] expectations;
	private final IPatternMatchContext context;
	private final boolean matchPatternsBeforeCompletion;
	private final String expectedMatchString;

	public CodeCompletionTest(ISyntaxPattern<?>[] patterns, String testText, ExpectedProposal[] expectations,
			IPatternMatchContext context, boolean useOptimizedMatcher, boolean usePrioritizer,
			boolean matchPatternsBeforeCompletion, String expectedMatchString) {

		super(useOptimizedMatcher, usePrioritizer);
		this.patterns = patterns;
		this.testText = testText;
		this.expectations = expectations;
		this.matchPatternsBeforeCompletion = matchPatternsBeforeCompletion;
		this.expectedMatchString = expectedMatchString;
		if (context == null) {
			this.context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		} else {
			this.context = context;
		}
	}

	@Test
	public void test() {
		assertCodeCompletionResult();
	}

	private static ISyntaxPattern<?> addPatternToPatternsList(ISyntaxPatternPart... parts) {
		String simpleName = "MyType";
		String type = "com.acme." + simpleName;

		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		ISyntaxPattern<?> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(null, null, "method1", null,
				new JavaType(type), Arrays.asList(parts), partToParameterIndexMap, null, new JavaCodeGeneratorFactory(),
				false);
		return pattern;
	}

	private void assertCodeCompletionResult() {
		if (matchPatternsBeforeCompletion) {
			assertMatch(Arrays.asList(patterns), this.expectedMatchString, true, this.context);
		}

		new NatspecMetaInformation().registerResourceFactory();
		Resource resource = NatspecResourceUtil.getResource(testText.getBytes());

		NatspecCodeCompletionHelper natspecCodeCompletionHelper = new NatspecCodeCompletionHelper();

		NatspecCompletionProposal[] completionProposals = natspecCodeCompletionHelper.doComputeCompletionProposals(
				(INatspecTextResource) resource, testText, testText.length(), context, Arrays.asList(patterns),
				useOptimizedMatcher(), usePrioritizer());

		int i = 0;
		for (NatspecCompletionProposal proposal : completionProposals) {
			if (i > expectations.length - 1) {
				fail("Found fewer proposals (" + completionProposals.length + ") than expected (" + expectations.length + ")");
			}
			
			ExpectedProposal expectation = expectations[i++];
			assertEquals("Wrong insert String for proposal " + i, expectation.getInsertString(),
					proposal.getInsertString());
			assertEquals("Wrong display String for proposal " + i, expectation.getDisplayString(),
					proposal.getDisplayString());
			assertEquals("Wrong prefix String for proposal " + i, expectation.getPrefix(), proposal.getPrefix());
		}
		
		assertEquals("Not all expected proposals found", expectations.length, completionProposals.length);
	}

	@Parameters(name = "{index}:{6}:{1} tree:{4} split:{5}")
	public static Collection<Object[]> getTestData() throws UnsupportedTypeException {

		ITestDataProvider3 provider = new ITestDataProvider3() {

			@Override
			public Collection<Object[]> getTestData(boolean useOptimizedMatcher, boolean usePrioritizer,
					boolean matchPatternsBeforeCompletion) {
				return CodeCompletionTest.getTestData(useOptimizedMatcher, usePrioritizer,
						matchPatternsBeforeCompletion);
			}
		};
		return new BooleanCombinator().createBooleanCombinations(provider);
	}

	@Parameters
	public static Collection<Object[]> getTestData(boolean useOptimizedMatcher, boolean usePrioritizer,
			boolean matchPatternsBeforeCompletion) {

		Collection<Object[]> testData = new LinkedList<Object[]>();

		getTestData1(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData2(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData3(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData4(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData5(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData6(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData7(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData8(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData9(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData10(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData11(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData12(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData13(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData14(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		try {
			getTestData15(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

			getTestData16(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		} catch (UnsupportedTypeException e) {
			fail(e.getMessage());
		}

		getTestData17(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		getTestData18(testData, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion);

		return testData;
	}

	protected static void getTestData18(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test completion for patterns that use a BooleanParameter where the
		// value for 'false' is empty
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Assert"));
		pattern.addPart(new BooleanParameter("no", ""));

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);

		testData.add(new Object[] { new ISyntaxPattern<?>[] { pattern }, "Ass",
				new ExpectedProposal[] { new ExpectedProposal("Assert", "Assert", "Ass"),
						new ExpectedProposal("Assert no", "Assert no", "Ass"), },
				context, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Assert no" });
	}

	protected static void getTestData17(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test completion for patterns that use a BooleanParameter where the
		// value for 'false' is empty
		SyntaxPatternMock<Object> pattern = new SyntaxPatternMock<Object>();
		pattern.addPart(new StaticWord("Assert"));
		pattern.addPart(new BooleanParameter("no", ""));
		pattern.addPart(new StaticWord("failure"));

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);

		testData.add(new Object[] { new ISyntaxPattern<?>[] { pattern }, "Ass",
				new ExpectedProposal[] { new ExpectedProposal("Assert failure", "Assert failure", "Ass"),
						new ExpectedProposal("Assert no failure", "Assert no failure", "Ass"), },
				context, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Assert no failure" });
	}

	protected static void getTestData16(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) throws UnsupportedTypeException {

		// test completion for patterns that use implicit context and have
		// a matching object in context
		String simpleName3 = "Mother";
		String type3 = "com.acme." + simpleName3;
		ImplicitParameterPattern implicitParameterPattern2 = new ImplicitParameterPattern("sons", type3, simpleName3,
				Integer.class.getName());

		IPatternMatchContext context2 = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);

		context2.addObjectToContext(
				new ObjectCreation(context2, new JavaType(type3), Collections.singletonList(simpleName3)));

		testData.add(new Object[] { new ISyntaxPattern<?>[] { implicitParameterPattern2 }, "With ",
				new ExpectedProposal[] { new ExpectedProposal("With 1 sons", "With 1 sons", "With ") }, context2,
				useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "With 1 sons" });
	}

	protected static void getTestData15(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) throws UnsupportedTypeException {

		IPatternMatchContext contextForComplexType;
		// test no completion for patterns that use implicit context and no
		// matching object in context
		String simpleName = "Father";
		String type = "com.acme." + simpleName;
		ImplicitParameterPattern implicitParameterPattern = new ImplicitParameterPattern("children", type, simpleName,
				String.class.getName());

		contextForComplexType = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);

		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { implicitParameterPattern,
						addPatternToPatternsList(new StaticWord("With"), new StaticWord("always")) },
				"With ", new ExpectedProposal[] { new ExpectedProposal("With always", "With always", "With ")

				}, contextForComplexType, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion,
				"With always" });
	}

	protected static void getTestData14(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {
		// test sentence ending with double parameter placeholder followed by dot
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("test"),
						new WrappedPart(new DoubleArgument(), "", ".")) },
				"tes", new ExpectedProposal[] { new ExpectedProposal("test 1.0.", "test 1.0.", "tes") }, null,
				useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "test 1.0."

		});
	}

	protected static void getTestData13(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {
		// test for correct sorting of list of proposals
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Sorting"), new StaticWord("Test"),
						new StaticWord("Alex")),
				addPatternToPatternsList(new StaticWord("Sorting"), new StaticWord("Test"), new StaticWord("Alexei")),
				addPatternToPatternsList(new StaticWord("Sorting"), new StaticWord("Test"), new StaticWord("Adam")) },
				"Sorting Test",
				new ExpectedProposal[] { new ExpectedProposal("Sorting Test Adam", "Sorting Test Adam", "Sorting Test"),
						new ExpectedProposal("Sorting Test Alex", "Sorting Test Alex", "Sorting Test"),
						new ExpectedProposal("Sorting Test Alexei", "Sorting Test Alexei", "Sorting Test") },
				null, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Sorting Test Alex"

		});
	}

	protected static void getTestData12(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test case insensitive partial completion
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"),
						new StaticWord("PartialCompletion"), new StaticWord("Pattern")) },
				"test partial",
				new ExpectedProposal[] { new ExpectedProposal("test PartialCompletion Pattern",
						"test PartialCompletion Pattern", "test partial") },
				null, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion,
				"Test PartialCompletion Pattern" });
	}

	protected static void getTestData11(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {
		// test partial completion with static words
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"),
						new StaticWord("PartialCompletion"), new StaticWord("Pattern")) },
				"Test Partial",
				new ExpectedProposal[] { new ExpectedProposal("Test PartialCompletion Pattern",
						"Test PartialCompletion Pattern", "Test Partial") },
				null, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion,
				"Test PartialCompletion Pattern" });
	}

	protected static void getTestData10(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test completion with integer list parameters
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"),
						new ListParameter(new IntegerArgument(), new JavaType(Integer.class)), new StaticWord("List"),
						new StaticWord("Pattern")) },
				"Test 1 2 3 ",
				new ExpectedProposal[] {
						new ExpectedProposal("Test 1 2 3 List Pattern", "Test 1 2 3 List Pattern", "Test 1 2 3 ") },
				null, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Test 1 2 3 List Pattern" });
	}

	protected static void getTestData9(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {
		// test completion with string parameters
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"), new StringParameter(),
						new StaticWord("Pattern")) },
				"Test my",
				new ExpectedProposal[] { new ExpectedProposal("Test my Pattern", "Test my Pattern", "Test my") }, null,
				useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Test my Pattern" });
	}

	protected static void getTestData8(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test completion with empty input
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"), new StaticWord("Empty"),
						new StaticWord("Input")) },
				"", new ExpectedProposal[] { new ExpectedProposal("Test Empty Input", "Test Empty Input", "") }, null,
				useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Test Empty Input" });
	}

	protected static void getTestData7(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		IPatternMatchContext contextForComplexType = createContext1();

		// test partial completions with complex parameter
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"), new StaticWord("Pattern"),
						new ComplexParameter("Type")) },
				"Test Patt",
				new ExpectedProposal[] { new ExpectedProposal("Test Pattern Type", "Test Pattern Type", "Test Patt") },
				contextForComplexType, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion,
				"Test Pattern Type" });
	}

	protected static void getTestData6(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		IPatternMatchContext contextForComplexType = createContext1();
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"), new StaticWord("Pattern"),
						new ComplexParameter("Type")) },
				"Test Pattern ",
				new ExpectedProposal[] {
						new ExpectedProposal("Test Pattern Type", "Test Pattern Type", "Test Pattern ") },
				contextForComplexType, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion,
				"Test Pattern Type" });
	}

	protected static void getTestData5(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		IPatternMatchContext contextForComplexType = createContext1();

		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"), new ComplexParameter("Type"),
						new StaticWord("Pattern")) },
				"Test ",
				new ExpectedProposal[] { new ExpectedProposal("Test Type Pattern", "Test Type Pattern", "Test ") },
				contextForComplexType, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion,
				"Test Type Pattern" });
	}

	protected static IPatternMatchContext createContext1() {
		// test matching with complex parameters
		IPatternMatchContext contextForComplexType = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		String simpleNameForComplexType = "Type";
		String typeForComplexType = simpleNameForComplexType;

		contextForComplexType.addObjectToContext(new ObjectCreation(contextForComplexType,
				new JavaType(typeForComplexType), Collections.singletonList(simpleNameForComplexType)));
		return contextForComplexType;
	}

	protected static void getTestData4(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test that whitespace in front of sentence are preserved when
		// providing proposals
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"), new StaticWord("Simple"),
						new StaticWord("Whitespace")) },
				"  Test",
				new ExpectedProposal[] {
						new ExpectedProposal("Test Simple Whitespace", "Test Simple Whitespace", "Test") },
				null, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Test Simple Whitespace" });
	}

	protected static void getTestData3(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test matching of simple pattern using just static words
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] { addPatternToPatternsList(new StaticWord("Test"), new StaticWord("Static"),
						new StaticWord("Words")) },
				"Test",
				new ExpectedProposal[] { new ExpectedProposal("Test Static Words", "Test Static Words", "Test") }, null,
				useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Test Static Words" });
	}

	protected static void getTestData2(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// Test matching of simple word patterns with multiple proposals
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] {
						addPatternToPatternsList(new StaticWord("Test"), new StaticWord("first"),
								new StaticWord("Pattern")),
						addPatternToPatternsList(new StaticWord("Test"), new StaticWord("first"),
								new StaticWord("additional"), new StaticWord("Pattern")),
				addPatternToPatternsList(new StaticWord("Test"), new StaticWord("second"), new StaticWord("Pattern")) },
				"Test first",
				new ExpectedProposal[] { new ExpectedProposal("Test first Pattern", "Test first Pattern", "Test first"),
						new ExpectedProposal("Test first additional Pattern", "Test first additional Pattern",
								"Test first") },
				null, useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "Test first Pattern" });
	}

	protected static void getTestData1(Collection<Object[]> testData, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean matchPatternsBeforeCompletion) {

		// test sentence
		testData.add(new Object[] {
				new ISyntaxPattern<?>[] {
						addPatternToPatternsList(new StaticWord("test"), new StaticWord("something")) },
				"t", new ExpectedProposal[] { new ExpectedProposal("test something", "test something", "t") }, null,
				useOptimizedMatcher, usePrioritizer, matchPatternsBeforeCompletion, "test something" });
	}
}
