package de.devboost.natspec.testscripting.java.test.patterns.parts;

import static org.junit.Assert.assertEquals;

import java.util.regex.Matcher;

import org.junit.Test;

import de.devboost.natspec.testscripting.java.patterns.parts.LocalTimeParameter;

public class LocalTimeParameterTest {

	@Test
	public void testValidExamples() {
		assertMatch("10:10", true);
		assertMatch("10:10:15", true);
		assertMatch("10:10:15.100", true);
		assertMatch("10:10:15.123456789", true);
	}

	@Test
	public void testInvalidExamples() {
		assertMatch("10:1", false);
		assertMatch("10:10:1", false);
		assertMatch("10:10:15.1234567890", false);
	}

	private void assertMatch(String input, boolean expected) {
		Matcher matcher = LocalTimeParameter.PATTERN.matcher(input);
		assertEquals(expected, matcher.matches());
	}
}
