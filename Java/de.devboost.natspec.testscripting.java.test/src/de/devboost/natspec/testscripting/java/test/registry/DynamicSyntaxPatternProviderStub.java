package de.devboost.natspec.testscripting.java.test.registry;

import java.util.List;

import org.eclipse.core.resources.IFile;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.patterns.AbstractDynamicSyntaxPatternProvider;

public class DynamicSyntaxPatternProviderStub extends AbstractDynamicSyntaxPatternProvider {

	@Override
	protected List<ISyntaxPattern<?>> doGetPatterns(IFile natspecFile) {
		// Do not return anything as this is not required for the tests.
		return null;
	}

	@Override
	public boolean registerPatterns(String qualifiedTypeName,
			List<ISyntaxPattern<?>> newPatterns) {
		return super.registerPatterns(qualifiedTypeName, newPatterns);
	}

	public void clear() {
		getTestSupportClassToPatternsMap().clear();
	}
}
