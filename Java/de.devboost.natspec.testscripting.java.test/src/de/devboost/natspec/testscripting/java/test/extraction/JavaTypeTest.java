package de.devboost.natspec.testscripting.java.test.extraction;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import de.devboost.natspec.testscripting.java.model.JavaType;

/**
 * The {@link JavaTypeTest} checks the behavior of the {@link JavaType} class.
 */
public class JavaTypeTest {

	/**
	 * This test checks that {@link JavaType#getAllSuperTypes()} does not run
	 * into an infinite loop if the type hierarchy contains cycles.
	 */
	@Test
	public void testTerminationOfSuperTypeComputation() {
		List<JavaType> superTypesOfA = new ArrayList<JavaType>();
		List<JavaType> superTypesOfB = new ArrayList<JavaType>();
		JavaType typeA = new JavaType("A", Collections.<JavaType>emptyList(), superTypesOfA);
		JavaType typeB = new JavaType("B", Collections.<JavaType>emptyList(), superTypesOfB);
		superTypesOfA.add(typeB);
		superTypesOfB.add(typeA);
		
		Set<String> allSuperTypesOfA = typeA.getAllSuperTypes();
		assertTrue(allSuperTypesOfA.contains("B"));
		
		Set<String> allSuperTypesOfB = typeB.getAllSuperTypes();
		assertTrue(allSuperTypesOfB.contains("A"));
	}
}
