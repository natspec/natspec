package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;

import de.devboost.natspec.testscripting.java.JavaClassNameProvider;

public class JavaClassNameProviderTest {

	@Test
	public void testClassNameEscaping() {
		assertCorrectEscaping("RegularFile", "RegularFile");
		assertCorrectEscaping("File With Whitespace", "File_32_With_32_Whitespace");
		assertCorrectEscaping("File_With_Underscore", "File_With_Underscore");
		assertCorrectEscaping("File.With.Dots", "File_46_With_46_Dots");
		assertCorrectEscaping("File.Two..Dots", "File_46_Two_46__46_Dots");
		assertCorrectEscaping("File.With.Number.5", "File_46_With_46_Number_46_5");
		assertCorrectEscaping("File-With-Dashes", "File_45_With_45_Dashes");
		assertCorrectEscaping("1FileStartingWithANumber", "_49_FileStartingWithANumber");
	}

	private void assertCorrectEscaping(String uriWithoutExtension, String expectedClassName) {
		URI uri = URI.createURI(uriWithoutExtension + ".natspec");
		String actualClassName = JavaClassNameProvider.INSTANCE.getClassName(uri);
		assertEquals("Wrong class name", expectedClassName, actualClassName);
		
		String actualNatSpecFileName = JavaClassNameProvider.INSTANCE.getNatSpecFileName(actualClassName + ".java");
		assertEquals("Wrong NatSpec file name", uriWithoutExtension + ".natspec", actualNatSpecFileName);
	}
}
