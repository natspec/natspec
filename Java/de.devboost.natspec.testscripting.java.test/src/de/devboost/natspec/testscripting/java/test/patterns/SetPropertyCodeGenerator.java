package de.devboost.natspec.testscripting.java.test.patterns;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.JavaMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;

public class SetPropertyCodeGenerator extends AbstractCodeGenerator {

	private ObjectCreation entityCreation;
	private String propertyName;
	private ISyntaxPatternPartMatch valueMatch;
	
	public SetPropertyCodeGenerator(
			ISyntaxPatternMatch<?> match,
			ObjectCreation entityCreation,
			String propertyName, 
			ISyntaxPatternPartMatch valueMatch) {
		
		super(match, new JavaMatchCodeGeneratorFactory(), new PrefixBasedCommentGenerator("//"));
		this.entityCreation = entityCreation;
		this.propertyName = propertyName;
		this.valueMatch = valueMatch;
	}

	@Override
	public void generateSourceCode(ICodeGenerationContext context) {
		context.addCode(getComment() + entityCreation.getVariableName() + ".set" + propertyName + "(");
		new JavaMatchCodeGeneratorFactory().createGenerator(valueMatch).generateSourceCode(context);
		context.addCode(");\n");
	}
}
