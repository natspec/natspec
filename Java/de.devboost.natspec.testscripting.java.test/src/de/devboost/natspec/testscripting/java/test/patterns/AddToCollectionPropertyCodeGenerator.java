package de.devboost.natspec.testscripting.java.test.patterns;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.JavaMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;

public class AddToCollectionPropertyCodeGenerator extends AbstractCodeGenerator {

	private ISyntaxPatternPartMatch valueMatch;
	private ObjectCreation entityCreation;
	private String propertyName;

	public AddToCollectionPropertyCodeGenerator(
			ISyntaxPatternMatch<AddToCollectionPropertyCodeGenerator> match,
			ObjectCreation entityCreation, String propertyName,
			ISyntaxPatternPartMatch valueMatch) {
		
		super(match, new JavaMatchCodeGeneratorFactory(), new PrefixBasedCommentGenerator("//"));
		this.valueMatch = valueMatch;
		this.entityCreation = entityCreation;
		this.propertyName = propertyName;
	}

	@Override
	public void generateSourceCode(ICodeGenerationContext context) {
		context.addCode(getComment() + entityCreation.getVariableName() + ".get" + propertyName + "().add(");
		new JavaMatchCodeGeneratorFactory().createGenerator(valueMatch).generateSourceCode(context);
		context.addCode(");\n");
	}
}
