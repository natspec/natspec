/**
 * This package contains sample implementation of the 
 * {@link de.devboost.natspec.patterns.ISyntaxPattern} interface which are used 
 * for testing.
 */
package de.devboost.natspec.testscripting.java.test.patterns;