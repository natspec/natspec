package de.devboost.natspec.testscripting.java.test.codegen;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.IntegerArgument;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.util.NatspecResourceUtil;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.MatchHelper;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

/**
 * The {@link JavaBodyGeneratorTest} tests the {@link JavaCodeGenerator}, in particular the list of statements (method
 * body) that is created for a list of sentences.
 */
// This test is parameterized to execute it both using the classic (simple) and the new (tree-based) pattern matcher.
@RunWith(Parameterized.class)
public class JavaBodyGeneratorTest extends AbstractNatSpecTestCase {

	private final static URI TEMP_URI = URI.createURI("temp.natspec");

	public JavaBodyGeneratorTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testType() {
		AccessibleJavaBodyGenerator generator = new AccessibleJavaBodyGenerator();
		String input = "Prefix 1 Suffix1\nPrefix 1 Suffix2";
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(TEMP_URI);
		addPatterns(context);
		INatspecTextResource resource = createAndMatchResource(context, input);
		Map<String, String> templateFieldNameToTypeMap = Collections.emptyMap();
		List<ISyntaxPatternMatch<?>> matches = MatchHelper.INSTANCE.getMatches(resource, templateFieldNameToTypeMap);
		ICodeGenerationContext body = generator.generateBody(resource, matches, false, false);
		String code = body.getCode();
		System.out.println("JavaBodyGeneratorTest.testType() " + code);
		String expected = getHeader() + 
				"// Prefix 1 Suffix1\n" + 
				"supportClassFieldNotAvailable.m1(1);\n" + 
				"// Prefix 1 Suffix2\n" + 
				"supportClassFieldNotAvailable.m2(\"1\");\n";
		assertEquals(expected, code);
	}

	private INatspecTextResource createAndMatchResource(IPatternMatchContext context, String input) {
		INatspecTextResource resource = (INatspecTextResource) NatspecResourceUtil.getResource(input.getBytes());
		MatchService matchService = new MatchService(useOptimizedMatcher(), usePrioritizer());
		matchService.match(resource, context);
		return resource;
	}

	@Test
	public void testListItems() {
		AccessibleJavaBodyGenerator generator = new AccessibleJavaBodyGenerator();
		String input = "List\nItem 1\nItem 2";
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(TEMP_URI);
		addListPatterns(context);
		INatspecTextResource resource = createAndMatchResource(context, input);
		Map<String, String> templateFieldNameToTypeMap = Collections.emptyMap();
		List<ISyntaxPatternMatch<?>> matches = MatchHelper.INSTANCE.getMatches(resource, templateFieldNameToTypeMap);
		ICodeGenerationContext body = generator.generateBody(resource, matches, false, false);
		String code = body.getCode();
		System.out.println("testListItems() " + code);
		String expected = getHeader() + 
				"// List\n" + 
				"java.util.List list_ = supportClassFieldNotAvailable.createList();\n" + 
				"// Item 1\n" + 
				"java.util.ListItem listItem_1 = supportClassFieldNotAvailable.addListItem(1, list_);\n"+
				"// Item 2\n" + 
				"java.util.ListItem listItem_2 = supportClassFieldNotAvailable.addListItem(2, list_);\n"
				;
		assertEquals(expected, code);
	}

	private void addListPatterns(IPatternMatchContext context) {
		addListPattern(context);
		addListItemPattern(context);
	}

	private void addListPattern(IPatternMatchContext context) {
		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(new StaticWord("List"));
		
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				(String) null, 
				"TestSupport", 
				"createList",
				Collections.<JavaParameter>emptyList(), 
				new JavaType(List.class),
				parts, 
				Collections.<ISyntaxPatternPart, Integer>emptyMap(), 
				new JavaParameterFactory(),
				new JavaCodeGeneratorFactory());
		
		context.getPatternsInContext().add(pattern);
	}

	private void addListItemPattern(IPatternMatchContext context) {
		IntegerArgument parameter = new IntegerArgument();
		ImplicitParameter listParameter = new ImplicitParameter(List.class.getName());

		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(listParameter);
		parts.add(new StaticWord("Item"));
		parts.add(parameter);
		
		Map<ISyntaxPatternPart, Integer> parameterMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		parameterMap.put(parameter, 1);
		parameterMap.put(listParameter, 2);
		
		List<JavaParameter> parameters = new ArrayList<JavaParameter>();
		parameters.add(new JavaParameter(null, new JavaType(String.class), Collections.<JavaAnnotation>emptyList()));
		parameters.add(new JavaParameter(null, new JavaType(List.class), Collections.<JavaAnnotation>emptyList()));
		
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				(String) null, 
				"TestSupport", 
				"addListItem",
				parameters, 
				new JavaType("java.util.ListItem"),
				parts, 
				parameterMap, 
				new JavaParameterFactory(),
				new JavaCodeGeneratorFactory());
		
		context.getPatternsInContext().add(pattern);
	}

	private String getHeader() {
		return "// The code in this method is generated from: null\n"+
			"// Never change this method or any contents of this file, all local changes will be overwritten.\n" +
			"// Change _NatSpecTemplate.java instead.\n\n";
	}

	private void addPatterns(IPatternMatchContext context) {
		addPattern(context, "m1", new IntegerArgument(), int.class, 1);
		addPattern(context, "m2", new StringParameter(), String.class, 2);
	}

	private void addPattern(IPatternMatchContext context, String methodName,
			ISyntaxPatternPart parameterPart, Class<?> parameterType, int suffix) {
		
		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(new StaticWord("Prefix"));
		parts.add(parameterPart);
		parts.add(new StaticWord("Suffix" + suffix));
		
		Map<ISyntaxPatternPart, Integer> parameterMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		parameterMap.put(parameterPart, 1);
		
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				(String) null, 
				"TestSupport", 
				methodName,
				Collections.singletonList(new JavaParameter(null, new JavaType(parameterType), Collections.<JavaAnnotation>emptyList())), 
				(IClass) null,
				parts, 
				parameterMap, 
				new JavaParameterFactory(),
				new JavaCodeGeneratorFactory());
		
		context.getPatternsInContext().add(pattern);
	}
}
