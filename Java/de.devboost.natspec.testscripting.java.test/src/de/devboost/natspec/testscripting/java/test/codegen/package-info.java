/**
 * This package contains tests that are related to the NatSpec code generator
 * for Java.
 */
package de.devboost.natspec.testscripting.java.test.codegen;