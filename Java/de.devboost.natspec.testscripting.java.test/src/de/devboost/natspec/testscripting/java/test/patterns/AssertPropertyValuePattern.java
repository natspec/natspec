package de.devboost.natspec.testscripting.java.test.patterns;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.AbstractSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.ObjectMatch;
import de.devboost.natspec.testscripting.patterns.parts.PropertyWord;

public class AssertPropertyValuePattern extends
		AbstractSyntaxPattern<AssertPropertyValueCodeGenerator> {

	private final List<ISyntaxPatternPart> parts;
	private final String propertyName;
	private final ComplexParameter objectReferencePart;
	private final ISyntaxPatternPart assertedValuePart;

	public AssertPropertyValuePattern(String propertyName,
			String qualifiedName, String simpleName, String qualifiedTypeName) throws UnsupportedTypeException {
		super(new JavaParameterFactory());
		this.propertyName = propertyName;

		parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(new StaticWord("Assert"));
		parts.add(new StaticWord("that"));
		// add property name
		parts.add(new PropertyWord(propertyName));
		parts.add(new StaticWord("of"));

		// add object reference
		objectReferencePart = new ComplexParameter(qualifiedName);
		parts.add(objectReferencePart);
		parts.add(new StaticWord("is"));

		// add argument (new value)
		assertedValuePart = getParameterFactory().createParameter(
				new JavaType(qualifiedTypeName));
		parts.add(assertedValuePart);
	}

	@Override
	public AssertPropertyValueCodeGenerator createUserData(
			ISyntaxPatternMatch<AssertPropertyValueCodeGenerator> match) {
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match
				.getPartsToMatchesMap();
		ISyntaxPatternPartMatch referenceMatch = partsToMatchesMap
				.get(objectReferencePart);
		if (referenceMatch instanceof ObjectMatch) {
			ObjectMatch objectMatch = (ObjectMatch) referenceMatch;
			ObjectCreation entityCreation = objectMatch.getObjectCreation();
			ISyntaxPatternPartMatch valueMatch = partsToMatchesMap
					.get(assertedValuePart);
			return new AssertPropertyValueCodeGenerator(match, entityCreation,
					propertyName, valueMatch);
		}

		return null;
	}

	@Override
	public List<ISyntaxPatternPart> getParts() {
		return parts;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [" + propertyName + "]";
	}
}
