package de.devboost.natspec.testscripting.java.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;

/**
 * This test checks whether the type hierarchy is considered during pattern
 * matching. For example, patterns that require more general types (i.e.,
 * super types) must also match specific types.
 */
@RunWith(Parameterized.class)
public class InheritanceTest extends AbstractNatSpecTestCase {

	public InheritanceTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	/**
	 * This test check whether a pattern that requires a super type matches
	 * a sub type.
	 */
	@Test
	public void testMatchSubclass() {
		TestPatternCreator patternCreator = new TestPatternCreator();
		
		JavaType superType = new JavaType("com.acme.Supertype");
		List<JavaType> superTypes = new ArrayList<JavaType>();
		superTypes.add(superType);
		JavaType subtype = new JavaType("com.acme.Subtype", Collections.<JavaType>emptyList(), superTypes);
		
		ISyntaxPattern<?> createSubTypePattern = patternCreator.newCreatePattern(subtype);
		ISyntaxPattern<?> destroySupertypePattern = patternCreator.newDestroyPattern(superType);
		
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		
		// the destroy pattern must not match, because the implicit parameter is
		// missing
		assertMatch(createSubTypePattern, "CreateSubtype", true, context);
		assertMatch(destroySupertypePattern, "DestroySupertype", true, context);
	}
}
