package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertFalse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.IParameter;
import de.devboost.natspec.testscripting.patterns.IValidationCallback;
import de.devboost.natspec.testscripting.patterns.PatternParser;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;

/**
 * This is a regression test for NATSPEC-264 (Add error marker for 
 * <code>@TextSyntax("")</code>).
 */
public class ParseEmptyPatternTest {

	@Test
	public void testParseEmptyPattern() {
		ICodeGeneratorFactory<ICodeFragment> codeGeneratorFactory = null;
		IParameterFactory parameterFactory = new JavaParameterFactory();
		PatternParser<ICodeFragment> patternParser = new PatternParser<ICodeFragment>(parameterFactory, codeGeneratorFactory);
		List<? extends IParameter> parameters = Collections.emptyList();
		IClass returnType = null;
		final List<String> errors = new ArrayList<String>();
		IValidationCallback callback = new IValidationCallback() {
			
			@Override
			public void beginValidation() {
			}
			
			@Override
			public void addWarning(String message) {
			}
			
			@Override
			public void addError(String message) {
				errors.add(message);
			}
		};
		patternParser.parse(null, null, "methodA", "", parameters, returnType, callback, null, false, false);
		
		assertFalse("There must be at least one error.", errors.isEmpty());
	}
}
