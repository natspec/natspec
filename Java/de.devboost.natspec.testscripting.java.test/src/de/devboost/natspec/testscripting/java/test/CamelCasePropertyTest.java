package de.devboost.natspec.testscripting.java.test;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.java.test.patterns.SetPropertyPattern;

@RunWith(Parameterized.class)
public class CamelCasePropertyTest extends AbstractNatSpecTestCase {

	public CamelCasePropertyTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testSetPropertyPattern() throws UnsupportedTypeException {
		String simpleName = "Flight";
		String type = "com.acme." + simpleName;
		// add pattern for setName(String)
		ISyntaxPattern<?> pattern = new SetPropertyPattern("totalSeats", type,
				simpleName, Integer.class.getName());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(type), Collections.singletonList(simpleName)));
		assertMatch(pattern, "Set totalSeats of Flight to 200", true, context);
		// splitted camel case property name
		assertMatch(pattern, "Set total seats of flight to 200", true, context);
	}
}
