package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.java.test.patterns.AddToCollectionPropertyPattern;
import de.devboost.natspec.testscripting.java.test.patterns.CreateEntityPattern;
import de.devboost.natspec.testscripting.java.test.patterns.SetPropertyPattern;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

@RunWith(Parameterized.class)
public class ConnectorTest extends AbstractNatSpecTestCase {

	public ConnectorTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testSpaceAtEOL() throws UnsupportedTypeException {
		ISyntaxPattern<?> pattern = new CreateEntityPattern("com.acme.A", "A",
				Collections.<String> emptyList());
		assertMatch(pattern, "Create A", true);
		// add space character at EOL
		assertMatch(pattern, "Create A ", true);
		// and another one
		assertMatch(pattern, "Create A  ", true);
	}

	@Test
	public void testTabAtBeginningOfLine() throws UnsupportedTypeException {
		ISyntaxPattern<?> pattern = new CreateEntityPattern("com.acme.A", "A",
				Collections.<String> emptyList());
		assertMatch(pattern, "\tCreate A", true);
	}

	@Test
	public void testContextCreation() throws UnsupportedTypeException {
		String simpleName = "A";
		String type = "com.acme." + simpleName;

		List<String> parameters = new ArrayList<String>();
		parameters.add(String.class.getName());

		ISyntaxPattern<?> pattern = new CreateEntityPattern(type, simpleName,
				parameters);
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);

		Collection<Object> objectsInContext = context.getObjectsInContext();
		assertEquals(0, objectsInContext.size());
		assertMatch(pattern, "Create A SomeIdentifier", true, context);
		// check whether the created object was added to the context
		objectsInContext = context.getObjectsInContext();
		assertEquals(1, objectsInContext.size());
		Object first = objectsInContext.iterator().next();
		assertTrue(first instanceof ObjectCreation);
		ObjectCreation entityCreation = (ObjectCreation) first;
		assertEquals(type, entityCreation.getObjectType().getQualifiedName());
		List<String> identifiers = entityCreation.getIdentifiers();
		assertEquals(1, identifiers.size());
		assertEquals("SomeIdentifier", identifiers.get(0));
	}

	@Test
	public void testSetPropertyPattern() throws UnsupportedTypeException {
		String simpleName = "A";
		String type = "com.acme." + simpleName;
		// add pattern for setName(String)
		ISyntaxPattern<?> pattern = new SetPropertyPattern("name", type,
				simpleName, String.class.getName());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		// context is empty. therefore, the reference to 'A' cannot be found
		assertMatch(pattern, "Set name of A to Mueller", false, context);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(
				type), Collections.singletonList(simpleName)));
		assertMatch(pattern, "Set name of A to Mueller", true, context);
		// try integer argument (must be converted to string)
		assertMatch(pattern, "Set name of A to 1", true, context);
		// try different case
		assertMatch(pattern, "Set nAmE of A to Mueller", true, context);
	}

	@Test
	public void testAddToCollectionPropertyPatternPrimitive() throws UnsupportedTypeException {
		String simpleName = "A";
		String type = "com.acme." + simpleName;
		// add pattern for setNames(Collection<String>)
		ISyntaxPattern<?> pattern = new AddToCollectionPropertyPattern("names",
				type, String.class.getName());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		// context is empty. therefore, the reference to 'A' cannot be found
		assertMatch(pattern, "Add Mueller to names of A", false, context);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(
				type), Collections.singletonList(simpleName)));
		assertMatch(pattern, "Add Mueller to names of A", true, context);
		// try integer argument (must be converted to string)
		assertMatch(pattern, "Add 1 to names of A", true, context);
		// try different case
		assertMatch(pattern, "Add Mueller to nAmEs of A", true, context);
	}

	@Test
	public void testAddToCollectionPropertyPatternComplex() throws UnsupportedTypeException {
		String simpleName = "A";
		String type = "com.acme." + simpleName;
		// add pattern for setNames(Collection<String>)
		ISyntaxPattern<?> pattern = new AddToCollectionPropertyPattern(
				"others", type, type);

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		// context is empty. therefore, the reference to 'A' cannot be found
		assertMatch(pattern, "Add A to others of A", false, context);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(
				type), Collections.singletonList(simpleName)));
		assertMatch(pattern, "Add A to others of A", true, context);
	}

	@Test
	public void testAddToObjectCollection() throws UnsupportedTypeException {
		String simpleName = "A";
		String type = "com.acme." + simpleName;
		// add pattern for setNames(Collection<String>)
		ISyntaxPattern<?> pattern = new AddToCollectionPropertyPattern("names",
				type, Object.class.getName());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		// context is empty. therefore, the reference to 'A' cannot be found
		assertMatch(pattern, "Add Mueller to names of A", false, context);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(
				type), Collections.singletonList(simpleName)));
		// try string argument (must be converted to object)
		assertMatch(pattern, "Add Mueller to names of A", true, context);
		// try integer argument (must be converted to object)
		assertMatch(pattern, "Add 1 to names of A", true, context);
	}

	@Test
	public void testObjectReferenceWithoutIdentifiers1() throws UnsupportedTypeException {
		String simpleName = "MyType";
		String type = "com.acme." + simpleName;
		// add pattern for setNames(Collection<String>)
		ISyntaxPattern<?> pattern = new AddToCollectionPropertyPattern("names",
				type, String.class.getName());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		// context is empty. therefore, the reference to 'MyType' cannot be
		// found
		assertMatch(pattern, "Add Mueller to names of MyType", false, context);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(
				type), Collections.<String> emptyList()));
		// the reference to 'MyType' must be resolved even though there are no
		// identifiers. this is possible because there is only one object of
		// type 'MyType'.
		assertMatch(pattern, "Add Mueller to names of MyType", true, context);
	}

	@Test
	public void testObjectReferenceWithoutIdentifiers2()
			throws UnsupportedTypeException {
		
		String simpleName = "MyType";
		String type = "com.acme." + simpleName;
		// add pattern for setNames(Collection<String>)
		ISyntaxPattern<?> pattern = new SetPropertyPattern("start", type,
				simpleName, Date.class.getName());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		// context is empty. therefore, the reference to 'MyType' cannot be
		// found
		assertMatch(pattern, "Set start of MyType to 01.12.1978", false,
				context);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(
				type), Collections.<String> emptyList()));
		// the reference to 'MyType' must be resolved even though there are no
		// identifiers. this is possible because there is only one object of
		// type 'MyType'.
		assertMatch(pattern, "Set start of MyType to 01.12.1978", true, context);
	}

	@Test
	public void testObjectReferenceWithTypeSynonym()
			throws UnsupportedTypeException {
		
		final String simpleName = "MyType";
		String type = "com.acme." + simpleName;

		ISynonymProvider synonymProvider = new ISynonymProvider() {

			@Override
			public Set<Set<String>> getSynonyms(URI contextURI) {
				Set<String> synonymSet = new LinkedHashSet<String>();
				synonymSet.add(simpleName);
				synonymSet.add("SynonymForType");

				Set<Set<String>> synonyms = new LinkedHashSet<Set<String>>();
				synonyms.add(synonymSet);
				return synonyms;
			}
		};

		// add pattern for setNames(Collection<String>)
		ISyntaxPattern<?> pattern = new SetPropertyPattern("start", type,
				simpleName, Date.class.getName());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null, synonymProvider);
		// context is empty. therefore, the reference to 'SynonymForType' cannot
		// be found
		assertMatch(pattern, "Set start of SynonymForType to 01.12.1978",
				false, context);
		context.addObjectToContext(new ObjectCreation(context, new JavaType(
				type), Collections.<String> emptyList()));
		// the reference to 'SynonymForType' must be resolved even though there
		// are no
		// identifiers. this is possible because there is only one object of
		// type 'MyType' and 'SynonymForType' is a synonym for 'MyType'.
		assertMatch(pattern, "Set start of SynonymForType to 01.12.1978", true,
				context);
	}

	@Test
	public void testObjectReferenceWithDynamicPatternWithReturnType()
			throws UnsupportedTypeException {
		
		String simpleName = "MyType";
		String type = "com.acme." + simpleName;

		// add dynamic pattern 'Custom create MyType <String>'
		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		;
		parts.add(new StaticWord("Custom"));
		parts.add(new StaticWord("Create"));
		parts.add(new StaticWord(simpleName));
		StringParameter stringParameter = new StringParameter();
		parts.add(stringParameter);

		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		partToParameterIndexMap.put(stringParameter, 1);
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				null, null, "customCreate", null, new JavaType(type), parts,
				partToParameterIndexMap, null, new JavaCodeGeneratorFactory());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);

		@SuppressWarnings("unchecked")
		ISyntaxPatternMatch<JavaCodeGenerator> match = (ISyntaxPatternMatch<JavaCodeGenerator>) assertMatch(
				pattern, "Custom create MyType 1", true, context);
		pattern.createUserData(match);

		assertFalse(context.getObjectsInContext().isEmpty());
		ISyntaxPattern<?> setPattern = new SetPropertyPattern("start", type,
				simpleName, Date.class.getName());
		assertMatch(setPattern, "Set start of 1 to 01.12.1978", true, context);
		assertMatch(setPattern, "Set start of MyType 1 to 01.12.1978", true,
				context);
		assertMatch(setPattern, "Set start of MyType 2 to 01.12.1978", false,
				context);
	}

	@After
	public void tearDown() {
		super.tearDown();
	}
}
