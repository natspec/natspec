package de.devboost.natspec.testscripting.java.test.patterns;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.JavaMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;

public class AssertCollectionPropertyContainsCodeGenerator extends AbstractCodeGenerator {

	private ISyntaxPatternPartMatch valueMatch;
	private ObjectCreation entityCreation;
	private String propertyName;

	public AssertCollectionPropertyContainsCodeGenerator(
			ISyntaxPatternMatch<AssertCollectionPropertyContainsCodeGenerator> match,
			ObjectCreation entityCreation, String propertyName,
			ISyntaxPatternPartMatch valueMatch) {
		
		super(match, new JavaMatchCodeGeneratorFactory(), new PrefixBasedCommentGenerator("//"));
		this.entityCreation = entityCreation;
		this.propertyName = propertyName;
		this.valueMatch = valueMatch;
	}

	@Override
	public void generateSourceCode(ICodeGenerationContext context) {
		context.addCode(getComment() + "assertTrue(" + entityCreation.getVariableName() + ".get" + propertyName + "().contains(");
		new JavaMatchCodeGeneratorFactory().createGenerator(valueMatch).generateSourceCode(context);
		context.addCode("));\n");
	}
}
