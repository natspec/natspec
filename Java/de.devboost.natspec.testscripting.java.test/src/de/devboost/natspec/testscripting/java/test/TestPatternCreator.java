package de.devboost.natspec.testscripting.java.test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;

/**
 * The {@link TestPatternCreator} class can be used to create
 * {@link ISyntaxPattern}s that can be used for testing purposes.
 */
public class TestPatternCreator {

	public ISyntaxPattern<JavaCodeGenerator> newDestroyPattern(String type) {
		return newDestroyPattern(new JavaType(type));
	}

	public ISyntaxPattern<JavaCodeGenerator> newDestroyPattern(JavaType type) {

		String qualifiedName = type.getQualifiedName();
		String simpleName = NameHelper.INSTANCE.getSimpleName(qualifiedName);

		// add dynamic pattern 'DestroyMyType' (one implicit argument)
		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(new ImplicitParameter(qualifiedName));
		parts.add(new StaticWord("Destroy" + simpleName));

		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				null, null, "destroy", null, (JavaType) null, parts,
				partToParameterIndexMap, null, new JavaCodeGeneratorFactory());
		return pattern;
	}

	public ISyntaxPattern<JavaCodeGenerator> newCreatePattern(String type) {
		return newCreatePattern(new JavaType(type));
	}

	public ISyntaxPattern<JavaCodeGenerator> newCreatePattern(JavaType type) {

		String qualifiedName = type.getQualifiedName();
		String simpleName = NameHelper.INSTANCE.getSimpleName(qualifiedName);

		// add dynamic pattern 'CreateMyType' (no arguments, but return type)
		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		;
		parts.add(new StaticWord("Create" + simpleName));

		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				null, null, "create", null, type, parts,
				partToParameterIndexMap, null, new JavaCodeGeneratorFactory());
		return pattern;
	}
}
