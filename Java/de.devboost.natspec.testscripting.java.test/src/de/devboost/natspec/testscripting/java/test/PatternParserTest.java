package de.devboost.natspec.testscripting.java.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.test.BooleanCombinator;
import de.devboost.natspec.test.ITestDataProvider2;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.NullValidationCallback;
import de.devboost.natspec.testscripting.patterns.PatternParser;

@RunWith(Parameterized.class)
public class PatternParserTest extends AbstractNatSpecTestCase {

	private String syntaxSpecification;
	private String input;
	private List<JavaType> parameterTypes;
	private boolean isMatchExpected;

	public PatternParserTest(String syntaxSpecification, String input,
			List<JavaType> parameterTypes, boolean useOptimizedMatcher,
			boolean usePrioritizer, boolean isMatchExpected) {

		super(useOptimizedMatcher, usePrioritizer);
		this.syntaxSpecification = syntaxSpecification;
		this.input = input;
		this.parameterTypes = parameterTypes;
		this.isMatchExpected = isMatchExpected;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		ITestDataProvider2 provider = new ITestDataProvider2() {
			
			@Override
			public Collection<Object[]> getTestData(boolean useOptimizedMatcher, boolean usePrioritizer) {
				return PatternParserTest.getTestData(useOptimizedMatcher, usePrioritizer);
			}
		};
		return new BooleanCombinator().createBooleanCombinations(provider);
	}

	public static Collection<Object[]> getTestData(boolean useOptimizedMatcher,
			boolean usePrioritizer) {

		Collection<Object[]> testData = new ArrayList<Object[]>();
		// test normal argument
		testData.add(new Object[] { "call #1", "call text",
				Collections.singletonList(new JavaType(String.class)),
				useOptimizedMatcher, usePrioritizer, true });
		// test argument with two parenthesis (suffix and prefix)
		testData.add(new Object[] { "call (#1)", "call (text)",
				Collections.singletonList(new JavaType(String.class)),
				useOptimizedMatcher, usePrioritizer, true });
		// test argument with one parenthesis (suffix only)
		testData.add(new Object[] { "call (#1", "call (text",
				Collections.singletonList(new JavaType(String.class)),
				useOptimizedMatcher, usePrioritizer, true });
		// test argument with one parenthesis (prefix only)
		testData.add(new Object[] { "call #1)", "call text)",
				Collections.singletonList(new JavaType(String.class)),
				useOptimizedMatcher, usePrioritizer, true });
		// test double argument with dot at the end
		testData.add(new Object[] { "test #1.", "test 1.0.",
				Collections.singletonList(new JavaType(Double.class)),
				useOptimizedMatcher, usePrioritizer, true });

		// test prefix and suffix for a list
		testData.add(new Object[] {
				"call (#1)",
				"call (text another)",
				Collections.singletonList(new JavaType(List.class, Arrays
						.asList(new JavaType[] { new JavaType(String.class) }))),
				useOptimizedMatcher, usePrioritizer, true });

		// test list just with a prefix
		testData.add(new Object[] {
				"call +#1",
				"call +text another",
				Collections.singletonList(new JavaType(List.class, Arrays
						.asList(new JavaType[] { new JavaType(String.class) }))),
				useOptimizedMatcher, usePrioritizer, true });

		// test list with just a suffix
		testData.add(new Object[] {
				"call #1.",
				"call text another.",
				Collections.singletonList(new JavaType(List.class, Arrays
						.asList(new JavaType[] { new JavaType(String.class) }))),
				useOptimizedMatcher, usePrioritizer, true });

		// Test that we not forget an parameter, this one has to fail
		testData.add(new Object[] {
				"test #1. #2",
				"test for this.",
				Arrays.asList(
						new JavaType(List.class, Arrays
								.asList(new JavaType[] { new JavaType(
										String.class) })), new JavaType(
								String.class)), useOptimizedMatcher,
				usePrioritizer, false });

		// test if the wrapped list supports empty lists
		testData.add(new Object[] {
				"test #1.",
				"test .",
				Arrays.asList(new JavaType(List.class, Arrays
						.asList(new JavaType[] { new JavaType(String.class) }))),
				useOptimizedMatcher, usePrioritizer, true });

		// test wrapped list followed by another parameter
		testData.add(new Object[] {
				"test #1: #2",
				"test for this: one",
				Arrays.asList(
						new JavaType(List.class, Arrays
								.asList(new JavaType[] { new JavaType(
										String.class) })), new JavaType(
								String.class)), useOptimizedMatcher,
				usePrioritizer, true });

		// test argument with multiple arguments within parenthesis
		// THIS DOES NOT WORK YET
		/*
		 * testData.add(new Object[] { "call (#1,#2)", "call (text2,text2)",
		 * Arrays.asList(new JavaType[] {new JavaType(String.class), new
		 * JavaType(String.class)}) });
		 */
		return testData;
	}

	@Test
	public void testIssue_NATSPEC_9() {
		assertMatch(syntaxSpecification, input, parameterTypes);
	}

	private void assertMatch(String syntaxSpecification, String input,
			List<JavaType> parameterTypes) {
		PatternParser<?> patternParser = new PatternParser<JavaCodeGenerator>(
				new JavaParameterFactory(), new JavaCodeGeneratorFactory());
		String methodName = "method1";
		JavaType returnType = null;

		List<JavaParameter> parameters = new ArrayList<JavaParameter>();
		for (JavaType parameterType : parameterTypes) {
			parameters.add(new JavaParameter(null, parameterType, Collections
					.<JavaAnnotation> emptyList()));
		}

		ISyntaxPattern<?> pattern = patternParser.parse(null, "SomeType",
				methodName, syntaxSpecification, parameters, returnType,
				NullValidationCallback.INSTANCE, null, false, false);

		assertMatch(pattern, input, isMatchExpected);
	}
}
