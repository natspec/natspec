package de.devboost.natspec.testscripting.java.test.codegen;

import java.util.List;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.testscripting.java.JavaBodyGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;

/**
 * The {@link AccessibleJavaBodyGenerator} subclasses the {@link JavaBodyGenerator} to make the method
 * {@link JavaBodyGenerator#generateBodyCode} accessible for testing by overriding the methods visibility from
 * <code>protected</code> to <code>public</code>.
 */
public class AccessibleJavaBodyGenerator extends JavaBodyGenerator {

	@Override
	public ICodeGenerationContext generateBody(INatspecTextResource resource, List<ISyntaxPatternMatch<?>> matches,
			boolean enableLogging, boolean generateParameterizationCode) {

		return super.generateBody(resource, matches, enableLogging, generateParameterizationCode);
	}
}
