package de.devboost.natspec.testscripting.java.test.registry;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.ICodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.IParameter;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;

/**
 * The {@link PatternComparisonTest} verifies that the
 * {@link SyntaxPatternRegistry} is correctly updated/invalidated when a
 * Java-based syntax pattern is modified.
 */
public class PatternComparisonTest {

	private DynamicSyntaxPatternProviderStub provider;
	private PatternParameters patternParameters;

	@Before
	public void setUp() {
		provider = new DynamicSyntaxPatternProviderStub();
		SyntaxPatternRegistry.REGISTRY.add(provider);

		assertPatternCount(0);

		patternParameters = new PatternParameters();
	}
	
	@Test
	public void testChangeTypeName() {

		createAndRegisterPattern(patternParameters);
		
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		String simpleTypeName = patternParameters.getSimpleTypeName();
		assertEquals(simpleTypeName, NameHelper.INSTANCE.getSimpleName(firstPattern.getQualifiedTypeName()));
		
		// Change type name
		patternParameters.setSimpleTypeName("NewTypeName");
		createAndRegisterPattern(patternParameters);
		// When the name is changed, the pattern is recognized as new pattern
		assertPatternCount(2);
	}

	@Test
	public void testChangeReturnType() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("No return type expected.", null, firstPattern.getReturnType());
		// Change return type
		patternParameters.setReturnType(new JavaType("NewReturnType"));
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong return type.", "NewReturnType", firstPattern.getReturnType().getQualifiedName());
	}

	@Test
	public void testChangeReturnTypeSuperTypes() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 0, firstPattern.getParts().size());
		
		// Set return type
		JavaType returnTypeWithoutSuperType = new JavaType("MyReturnType");
		patternParameters.setReturnType(returnTypeWithoutSuperType);
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong return type.", "MyReturnType", firstPattern.getReturnType().getQualifiedName());
		assertEquals("Wrong return type super types.", 1, firstPattern.getReturnType().getAllSuperTypes().size());
		
		List<JavaType> superTypes = Collections.singletonList(new JavaType("MyReturnSuperType"));
		// Add super type to return type
		JavaType returnTypeWithSuperType = new JavaType("MyReturnType", Collections.<JavaType>emptyList(), superTypes);
		patternParameters.setReturnType(returnTypeWithSuperType);
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong return type super types.", 2, firstPattern.getReturnType().getAllSuperTypes().size());
	}

	@Test
	public void testChangeProjectName() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong project name.", PatternParameters.INITIAL_PROJECT_NAME, firstPattern.getProjectName());
		// Change project name
		patternParameters.setProjectName("NewProjectName");
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong project name.", "NewProjectName", firstPattern.getProjectName());
	}

	@Test
	public void testChangePackageName() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong package name.", PatternParameters.INITIAL_PACKAGE + "." + PatternParameters.INITIAL_SIMPLE_TYPE_NAME, firstPattern.getQualifiedTypeName());
		// Change project name
		patternParameters.setPackageName("newpackagename");
		createAndRegisterPattern(patternParameters);

		// When the package is changed, the pattern is recognized as new pattern
		assertPatternCount(2);
		DynamicSyntaxPattern<?> secondPattern = getPattern(2);
		assertEquals("Wrong package name.", "newpackagename" + "." + PatternParameters.INITIAL_SIMPLE_TYPE_NAME, secondPattern.getQualifiedTypeName());
	}

	@Test
	public void testChangeMethodName() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong project name.", PatternParameters.INITIAL_METHOD_NAME, firstPattern.getMethodName());
		// Change method name
		patternParameters.setMethodName("newMethodName");
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong method name.", "newMethodName", firstPattern.getMethodName());
	}

	@Test
	public void testAddParameter() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong parameter count.", 0, firstPattern.getParameters().size());
		// Add parameter
		patternParameters.getParameters().add(new JavaParameter("p1", new JavaType(String.class), Collections.<JavaAnnotation>emptyList()));
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong parameter count.", 1, firstPattern.getParameters().size());
	}

	@Test
	public void testAddStaticPart() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 0, firstPattern.getParts().size());
		// Add part
		patternParameters.getParts().add(new StaticWord("foo"));
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 1, firstPattern.getParts().size());
	}

	@Test
	public void testAddImplicitParameterPart() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 0, firstPattern.getParts().size());
		// Add part
		patternParameters.getParts().add(new ImplicitParameter("MyImplicitType"));
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 1, firstPattern.getParts().size());
	}

	@Test
	public void testChangePartMapping() {
		createAndRegisterPattern(patternParameters);
		DynamicSyntaxPattern<?> firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 0, firstPattern.getParts().size());
		// Add part and mapping
		ImplicitParameter part = new ImplicitParameter("MyImplicitType");
		patternParameters.getParts().add(part);
		patternParameters.getPartToParameterIndexMap().put(part, 1);
		
		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 1, firstPattern.getParts().size());
		assertEquals("Wrong mapping count.", 1, firstPattern.getPartToParameterIndexMap().keySet().size());
		assertEquals("Wrong mapping.", 1, (int) firstPattern.getPartToParameterIndexMap().get(part));
		
		// Change mapping (from 1 to 0)
		patternParameters.getPartToParameterIndexMap().put(part, 0);

		createAndRegisterPattern(patternParameters);
		firstPattern = assertOnePattern();
		assertEquals("Wrong part count.", 1, firstPattern.getParts().size());
		assertEquals("Wrong mapping count.", 1, firstPattern.getPartToParameterIndexMap().keySet().size());
		assertEquals("Wrong mapping.", 0, (int) firstPattern.getPartToParameterIndexMap().get(part));
	}

	protected DynamicSyntaxPattern<JavaCodeGenerator> createAndRegisterPattern(PatternParameters patternParameters) {
		
		DynamicSyntaxPattern<JavaCodeGenerator> pattern = createPattern(patternParameters);
		registerPattern(pattern);
		return pattern;
	}

	protected void registerPattern(DynamicSyntaxPattern<JavaCodeGenerator> pattern) {
		List<ISyntaxPattern<?>> newPatterns = Collections.<ISyntaxPattern<?>>singletonList(pattern);
		provider.registerPatterns(pattern.getQualifiedTypeName(), newPatterns);
	}

	protected DynamicSyntaxPattern<JavaCodeGenerator> createPattern(PatternParameters patternParameters) {
		// These are the properties of the DynamicSyntaxPattern. We will change
		// each one individually later on in this test and check whether this
		// change triggers a fresh registration of the pattern.
		String projectName = patternParameters.getProjectName();
		String qualifiedTypeName = patternParameters.getQualifiedTypeName();
		String methodName = patternParameters.getMethodName();
		List<? extends IParameter> parameters = patternParameters.getParameters();
		IClass returnType = patternParameters.getReturnType();
		List<ISyntaxPatternPart> parts = patternParameters.getParts();
		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = patternParameters.getPartToParameterIndexMap();
		IParameterFactory parameterFactory = patternParameters.getParameterFactory();
		ICodeGeneratorFactory<JavaCodeGenerator> codeGeneratorFactory = patternParameters.getCodeGeneratorFactory();
		
		DynamicSyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				projectName, qualifiedTypeName, methodName, parameters,
				returnType, parts, partToParameterIndexMap, parameterFactory,
				codeGeneratorFactory);
		return pattern;
	}

	protected DynamicSyntaxPattern<?> assertOnePattern() {
		assertEquals("One pattern expected.", 1, provider.getPatterns(null).size());
		return (DynamicSyntaxPattern<?>) getFirstPattern();
	}

	protected void assertPatternCount(int expectedCount) {
		assertEquals("Wrong number of patterns.", expectedCount, provider.getPatterns(null).size());
	}

	protected DynamicSyntaxPattern<?> getFirstPattern() {
		return (DynamicSyntaxPattern<?>) getPattern(1);
	}
	
	protected DynamicSyntaxPattern<?> getPattern(int index) {
		Iterator<ISyntaxPattern<? extends Object>> iterator = provider.getPatterns(null).iterator();
		for (int i = 1; i < index; i++) {
			iterator.next();
		}
		return (DynamicSyntaxPattern<?>) iterator.next();
	}
	
	@After
	public void tearDown() {
		SyntaxPatternRegistry.REGISTRY.remove(provider);
	}
}
