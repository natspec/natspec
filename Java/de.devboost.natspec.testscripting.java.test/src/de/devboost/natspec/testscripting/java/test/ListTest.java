package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.builder.CodeGenerationContext;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.JavaMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.ICodeFragment;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;
import de.devboost.natspec.testscripting.patterns.parts.ListMatch;
import de.devboost.natspec.testscripting.patterns.parts.ListParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;

@RunWith(Parameterized.class)
public class ListTest extends AbstractNatSpecTestCase {

	public ListTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testSimpleList() {
		ISyntaxPattern<JavaCodeGenerator> pattern = createPatternWithList(true,
				false, new Class<?>[] { String.class });

		assertMatch(pattern, "Prefix", true, createContext());
		ISyntaxPatternMatch<? extends Object> match1 = assertMatch(pattern,
				"Prefix listElement1", true, createContext());
		ISyntaxPatternMatch<? extends Object> match2 = assertMatch(pattern,
				"Prefix listElement1 listElement2", true, createContext());
		ISyntaxPatternMatch<? extends Object> match3 = assertMatch(pattern,
				"Prefix listElement1 listElement2 listElement3", true, createContext());

		assertListCode(match1, "\"listElement1\"");
		assertListCode(match2, "\"listElement1\", \"listElement2\"");
		assertListCode(match3,
				"\"listElement1\", \"listElement2\", \"listElement3\"");
	}

	private IPatternMatchContext createContext() {
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);
		return context;
	}

	private void assertListCode(ISyntaxPatternMatch<? extends Object> match1,
			String listCode) {
		assertCode(match1, "java.util.Arrays.asList(new java.lang.String[] {"
				+ listCode + "})");
	}

	private void assertCode(ISyntaxPatternMatch<? extends Object> match,
			String expectedCode) {
		AbstractCodeGenerator codeGenerator = new AbstractCodeGenerator(match, new JavaMatchCodeGeneratorFactory(), new PrefixBasedCommentGenerator("//")) {

			@Override
			public void generateSourceCode(ICodeGenerationContext context) {
				ISyntaxPatternMatch<?> patternMatch = getMatch();
				Collection<ISyntaxPatternPartMatch> partMatches = patternMatch
						.getPartsToMatchesMap().values();
				for (ISyntaxPatternPartMatch partMatch : partMatches) {
					if (partMatch instanceof ListMatch) {
						ICodeFragment generator = new JavaMatchCodeGeneratorFactory()
								.createGenerator(partMatch);
						generator.generateSourceCode(context);
					}
				}
			}
		};
		ICodeGenerationContext codeGenContext = new CodeGenerationContext();
		codeGenerator.generateSourceCode(codeGenContext);
		String code = codeGenContext.getCode();
		assertEquals(expectedCode, code);
	}

	@Test
	public void testListWithPrefixAndSuffix() {
		ISyntaxPattern<JavaCodeGenerator> pattern = createPatternWithList(true,
				true, new Class<?>[] { String.class });

		IPatternMatchContext context = createContext();

		assertMatch(pattern, "Prefix Suffix", true, context);
		assertMatch(pattern, "Prefix listElement1 Suffix", true, context);
		assertMatch(pattern, "Prefix listElement1 listElement2 Suffix", true,
				context);
		assertMatch(pattern,
				"Prefix listElement1 listElement2 listElement3 Suffix", true,
				context);
	}

	@Test
	public void testListWithoutPrefixButSuffix() {
		ISyntaxPattern<JavaCodeGenerator> pattern = createPatternWithList(
				false, true, new Class<?>[] { String.class });

		IPatternMatchContext context = createContext();

		assertMatch(pattern, "Suffix", true, context);
		assertMatch(pattern, "listElement1 Suffix", true, context);
		assertMatch(pattern, "listElement1 listElement2 Suffix", true, context);
		assertMatch(pattern, "listElement1 listElement2 listElement3 Suffix",
				true, context);
	}

	@Test
	public void testTwoSubsequentLists() {
		ISyntaxPattern<JavaCodeGenerator> pattern = createPatternWithList(
				false, false, new Class<?>[] { String.class, Integer.class });

		IPatternMatchContext context = createContext();

		assertMatch(pattern, "", true, context);
		assertMatch(pattern, "s1", true, context);
		assertMatch(pattern, "s1 s2", true, context);
		assertMatch(pattern, "s1 s2 s3", true, context);
		assertMatch(pattern, "s1 s2 s3 1", true, context);
		assertMatch(pattern, "s1 s2 s3 1 2", true, context);
		assertMatch(pattern, "s1 s2 s3 1 2 3", true, context);
	}

	private ISyntaxPattern<JavaCodeGenerator> createPatternWithList(
			boolean addPrefix, boolean addSuffix, Class<?>[] listTypes) {
		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		if (addPrefix) {
			parts.add(new StaticWord("Prefix"));
		}
		for (Class<?> listType : listTypes) {
			parts.add(new ListParameter(new StringParameter(), new JavaType(
					listType)));
		}
		if (addSuffix) {
			parts.add(new StaticWord("Suffix"));
		}

		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				null, null, "doSomethingWithList", null, (JavaType) null,
				parts, partToParameterIndexMap, null,  new JavaCodeGeneratorFactory());
		return pattern;
	}
}
