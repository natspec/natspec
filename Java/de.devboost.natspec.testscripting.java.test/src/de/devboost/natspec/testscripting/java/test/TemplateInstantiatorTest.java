package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.testscripting.java.JavaTemplateInstantiator;

@RunWith(Parameterized.class)
public class TemplateInstantiatorTest {

	private String templateContent;
	private String packageName;
	private String importStatements;
	private String className;
	private String generatedStatements;
	private String expectedOutput;
	private String generatedMethods;
	
	public TemplateInstantiatorTest(String templateContent, String packageName,
			String importStatements, String className,
			String generatedStatements, String generatedMethods,
			String expectedOutput) {
		
		super();
		this.templateContent = templateContent;
		this.packageName = packageName;
		this.importStatements = importStatements;
		this.className = className;
		this.generatedStatements = generatedStatements;
		this.generatedMethods = generatedMethods;
		this.expectedOutput = expectedOutput;
	}

	@Parameters
	public static Collection<Object[]> getTestData() {
		
		Collection<Object[]> data = new ArrayList<Object[]>();
		
		// test empty template
		data.add(new Object[] {
			"",
			"new.ackage",
			"",
			"MyTest",
			"",
			"",
			"",
		});

		// test simple package and class name replacement
		data.add(new Object[] {
			"package com.pany;public class _NatSpecTemplate {}",
			"new.ackage",
			"",
			"MyTest",
			"",
			"",
			"package new.ackage;public class MyTest {}",
		});

		// test simple package and class name replacement
		data.add(new Object[] {
			"package com.pany;\nimport java.util.List;\npublic class _NatSpecTemplate {}",
			"new.ackage",
			"",
			"MyTest",
			"",
			"",
			"package new.ackage;\nimport java.util.List;\npublic class MyTest {}",
		});

		// test simple package and class name replacement, add import
		data.add(new Object[] {
			"package com.pany;\nimport java.util.List;\npublic class _NatSpecTemplate {}",
			"new.ackage",
			"import java.util.Set;",
			"MyTest",
			"",
			"",
			"package new.ackage;\nimport java.util.Set;\nimport java.util.List;\npublic class MyTest {}",
		});

		// test simple package and class name replacement when template is 
		// located in default package
		data.add(new Object[] {
			"public class _NatSpecTemplate {}",
			"new.ackage",
			"",
			"MyTest",
			"",
			"",
			"package new.ackage;\n\npublic class MyTest {}",
		});

		// test simple package and class name replacement when template is 
		// located in default package and has imports
		data.add(new Object[] {
			"import java.util.List;public class _NatSpecTemplate {}",
			"new.ackage",
			"",
			"MyTest",
			"",
			"",
			"package new.ackage;\n\nimport java.util.List;public class MyTest {}",
		});

		// test adding an import when no class is imported in the template
		data.add(new Object[] {
			"package com.pany;public class _NatSpecTemplate {}",
			"new.ackage",
			"import java.util.List;",
			"MyTest",
			"",
			"",
			"package new.ackage;import java.util.List;\npublic class MyTest {}",
		});

		// test adding an import when no class is imported in the template and
		// the template is in the default package
		data.add(new Object[] {
			"public class _NatSpecTemplate {}",
			"new.ackage",
			"import java.util.List;",
			"MyTest",
			"",
			"",
			"package new.ackage;\n\nimport java.util.List;\npublic class MyTest {}",
		});

		// test adding an import when template already contains multiple imports
		data.add(new Object[] {
			"package com.pany;import java.util.Set;import java.util.Collection;public class _NatSpecTemplate {}",
			"new.ackage",
			"import java.util.List;",
			"MyTest",
			"",
			"",
			"package new.ackage;import java.util.List;\nimport java.util.Set;import java.util.Collection;public class MyTest {}",
		});

		// test replacement of method body annotation with generated statements
		data.add(new Object[] {
			"public class _NatSpecTemplate {public void m() {/* @MethodBody */}}",
			"new.ackage",
			"",
			"MyTest",
			"System.out.println();",
			"",
			"package new.ackage;\n\npublic class MyTest {public void m() {System.out.println();}}",
		});

		// test replacement of method body annotation with generated statements
		// that contain back slashes
		data.add(new Object[] {
			"public class _NatSpecTemplate {public void m() {/* @MethodBody */}}",
			"new.ackage",
			"",
			"MyTest",
			"System.out.println(\"Hello \\\"World\\\"\");",
			"",
			"package new.ackage;\n\npublic class MyTest {public void m() {System.out.println(\"Hello \\\"World\\\"\");}}",
		});

		// test add methods to the template
		data.add(new Object[] {
			"public class _NatSpecTemplate {public void m() {/* @MethodBody */}}",
			"new.ackage",
			"",
			"MyTest",
			"",
			"public static register() {}",
			"package new.ackage;\n\npublic class MyTest {public void m() {}public static register() {}}",
		});

		// test adding an import when no class is imported in the template using
		// @Import placeholder
		data.add(new Object[] {
				"package com.pany;/* @Imports */public class _NatSpecTemplate {}",
				"new.ackage",
				"import java.util.List;",
				"MyTest",
				"",
				"",
				"package new.ackage;import java.util.List;public class MyTest {}", });

		// test adding an import when template already contains multiple
		// imports, before all imports with @Imports
		data.add(new Object[] {
				"package com.pany;/* @Imports */import java.util.Set;import java.util.Collection;public class _NatSpecTemplate {}",
				"new.ackage",
				"import java.util.List;",
				"MyTest",
				"",
				"",
				"package new.ackage;import java.util.List;import java.util.Set;import java.util.Collection;public class MyTest {}", });

		// test adding an import when template already contains multiple
		// imports, after all imports with @Imports
		data.add(new Object[] {
				"package com.pany;import java.util.Set;import java.util.Collection;/* @Imports */public class _NatSpecTemplate {}",
				"new.ackage",
				"import java.util.List;",
				"MyTest",
				"",
				"",
				"package new.ackage;import java.util.Set;import java.util.Collection;import java.util.List;public class MyTest {}", });

		// test adding an import when template already contains multiple
		// imports, between all imports with @Imports
		data.add(new Object[] {
				"package com.pany;import java.util.Set;/* @Imports */import java.util.Collection;public class _NatSpecTemplate {}",
				"new.ackage",
				"import java.util.List;",
				"MyTest",
				"",
				"",
				"package new.ackage;import java.util.Set;import java.util.List;import java.util.Collection;public class MyTest {}", });

		return data;
	}

	@Test
	public void testTemplateInstantiation() {
		
		JavaTemplateInstantiator templateInstantiator = new JavaTemplateInstantiator();
		String actualOutput = templateInstantiator.instantiateTemplate(
				templateContent, packageName, importStatements, className,
				generatedStatements, "", generatedMethods);
		System.out.println("-------------------------------------------------");
		System.out.println("expectedOutput >>>" + expectedOutput + "<<<");
		System.out.println("actualOutput   >>>" + actualOutput + "<<<");
		assertEquals("Unexpected output.", expectedOutput, actualOutput);
	}
}
