package de.devboost.natspec.testscripting.java.test;

import java.util.Collections;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.java.test.patterns.ImplicitParameterAtTheEndPattern;
import de.devboost.natspec.testscripting.java.test.patterns.ImplicitParameterPattern;

@RunWith(Parameterized.class)
public class ImplicitParameterTest extends AbstractNatSpecTestCase {
	
	public ImplicitParameterTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testImplicitParameterPattern() throws UnsupportedTypeException {
		String simpleName = "Father";
		String type = "com.acme." + simpleName;
		// add pattern for setChilden(Integer)
		ISyntaxPattern<?> implicitPattern = new ImplicitParameterPattern("children", type, simpleName, String.class.getName());
		
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		// context is empty. therefore, the implicit reference to 'Father' cannot be found	
		assertMatch(implicitPattern, "With 2 children", false, context);
	
		// fill context with a father
		context.addObjectToContext(new ObjectCreation(context, new JavaType(type), Collections.singletonList(simpleName)));
		// context is now filled, implicit reference should be found and matched
		assertMatch(implicitPattern, "With 2 children", true, context);
	}
	
	@Test
	public void testImplicitParameterAtEndPattern() throws UnsupportedTypeException {
		String simpleName = "Father";
		String type = "com.acme." + simpleName;
		// add pattern for setChilden(Integer)
		ISyntaxPattern<?> implicitPattern = new ImplicitParameterAtTheEndPattern("children", type, simpleName, String.class.getName());
		
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		// context is empty. therefore, the implicit reference to 'Father' cannot be found	
		assertMatch(implicitPattern, "With 2 children", false, context);
	
		// fill context with a father
		context.addObjectToContext(new ObjectCreation(context, new JavaType(type), Collections.singletonList(simpleName)));
		// context is now filled, implicit reference should be found and matched
		assertMatch(implicitPattern, "With 2 children", true, context);
		
	}

	@Test
	public void testConsiderLastObjectOnly() throws UnsupportedTypeException {
		String simpleName = "Father";
		String type = "com.acme." + simpleName;
		// add pattern for setChilden(Integer), but set 'considerLastObjectOnly'
		boolean considerLastObjectOnly = true;
		ISyntaxPattern<?> implicitPattern = new ImplicitParameterPattern("children", type, simpleName, String.class.getName(), considerLastObjectOnly);
		
		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE.createPatternMatchContext(null);
		// context is empty. therefore, the implicit reference to 'Father' cannot be found	
		assertMatch(implicitPattern, "With 2 children", false, context);
		
		// fill context with a father
		context.addObjectToContext(new ObjectCreation(context, new JavaType(type), Collections.singletonList(simpleName)));
		// context is now filled, implicit reference should be found and matched
		assertMatch(implicitPattern, "With 2 children", true, context);
		
		// fill context with a father
		String otherType = "OtherType";
		context.addObjectToContext(new ObjectCreation(context, new JavaType("com.acme." + otherType), Collections.singletonList(otherType)));
		// father is in context, but not the last added object. implicit
		// reference must not be found.
		assertMatch(implicitPattern, "With 2 children", false, context);
	}

	@After
	public void tearDown() {
		super.tearDown();
	}
}
