package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;

@RunWith(Parameterized.class)
public class ReturnValueUsageTest extends AbstractNatSpecTestCase {

	public ReturnValueUsageTest(boolean useOptimizedMatcher,
			boolean usePrioritizer) {
		
		super(useOptimizedMatcher, usePrioritizer);
	}

	/**
	 * This test checks whether objects which were created as result of invoking
	 * a test support method are available from the object context for later
	 * patterns that require an object of the created type.
	 */
	@Test
	public void testReturnValueUsage() {
		String type = "com.acme.MyType";

		TestPatternCreator patternCreator = new TestPatternCreator();
		ISyntaxPattern<JavaCodeGenerator> pattern1 = patternCreator
				.newCreatePattern(type);
		ISyntaxPattern<JavaCodeGenerator> pattern2 = patternCreator
				.newDestroyPattern(type);

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);

		// the destroy pattern must not match, because the implicit parameter is
		// missing
		assertMatch(pattern2, "DestroyMyType", false, context);
		assertMatch(pattern1, "CreateMyType", true, context);

		Collection<Object> objectsInContext = context.getObjectsInContext();
		assertEquals(1, objectsInContext.size());

		// now, the destroy pattern can match, because there is an object in the
		// context that matched the implicit parameter
		assertMatch(pattern2, "DestroyMyType", true, context);
	}

	/**
	 * This is a test that checks whether implicit objects can be accessed even
	 * if objects of another type have been created in the meantime.
	 */
	@Test
	public void testReturnValueUsage2() {
		String type1 = "com.acme.MyType1";
		String type2 = "com.acme.MyType2";

		TestPatternCreator patternCreator = new TestPatternCreator();
		ISyntaxPattern<JavaCodeGenerator> pattern1 = patternCreator
				.newCreatePattern(type1);
		ISyntaxPattern<JavaCodeGenerator> pattern2 = patternCreator
				.newCreatePattern(type2);
		ISyntaxPattern<JavaCodeGenerator> pattern3 = patternCreator
				.newDestroyPattern(type1);

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);

		// the destroy pattern must not match, because the implicit parameter is
		// missing
		assertMatch(pattern3, "DestroyMyType1", false, context);
		assertMatch(pattern1, "CreateMyType1", true, context);

		Collection<Object> objectsInContext = context.getObjectsInContext();
		assertEquals(1, objectsInContext.size());

		// now, the destroy pattern can match, because there is an object in the
		// context that matched the implicit parameter
		assertMatch(pattern3, "DestroyMyType1", true, context);
		assertMatch(pattern2, "CreateMyType2", true, context);

		objectsInContext = context.getObjectsInContext();
		assertEquals(2, objectsInContext.size());

		// the destroy type1 pattern must still match, even if a second type
		// was created in the meantime
		assertMatch(pattern3, "DestroyMyType1", true, context);
	}
}
