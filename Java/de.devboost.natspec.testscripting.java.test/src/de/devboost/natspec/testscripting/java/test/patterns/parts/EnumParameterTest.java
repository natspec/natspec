package de.devboost.natspec.testscripting.java.test.patterns.parts;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Word;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.PatternMatchContext;
import de.devboost.natspec.testscripting.java.patterns.parts.EnumParameter;

@RunWith(Parameterized.class)
public class EnumParameterTest {

	private final String sentence;
	private final String literal1;
	private final String literal2;
	private final boolean matchExpected;

	public EnumParameterTest(String sentence, String literal1, String literal2, boolean matchExpected) {
		this.sentence = sentence;
		this.literal1 = literal1;
		this.literal2 = literal2;
		this.matchExpected = matchExpected;
	}
	
	@Parameters(name = "{0}")
	public static Collection<Object[]> getTestData() {
		Collection<Object[]> data = new ArrayList<Object[]>();
		// Basic positive examples
		data.add(new Object[] { "Oneword", "ONEWORD", "TWO_WORDS", true });
		data.add(new Object[] { "Two_Words", "ONEWORD", "TWO_WORDS", true });
		data.add(new Object[] { "Two Words", "ONEWORD", "TWO_WORDS", true });
		data.add(new Object[] { "Two Words And Tail", "ONEWORD", "TWO_WORDS", true });
		// Test synonyms
		data.add(new Object[] { "2 Words", "ONEWORD", "TWO_WORDS", true });
		data.add(new Object[] { "2 Words And Tail", "ONEWORD", "TWO_WORDS", true });
		return data;
	}
	
	@Test
	public void testMatching() throws Exception {
		assertMatch(sentence, literal1, literal2);
	}

	private void assertMatch(String text, String... literalsArray) {
		String qualifiedClassName = null;
		Set<String> literals = new LinkedHashSet<>(Arrays.asList(literalsArray));
		EnumParameter enumParameter = new EnumParameter(qualifiedClassName, literals);
		List<Word> words = new ArrayList<>();
		String[] parts = text.split(" ");
		for (String part : parts) {
			Word word = NatspecFactory.eINSTANCE.createWord();
			word.setText(part);
			words.add(word);
		}
		ISynonymProvider localSynonymProvider = new ISynonymProvider() {
			
			@Override
			public Set<Set<String>> getSynonyms(URI contextURI) {
				Set<String> synonyms1 = new LinkedHashSet<String>();
				synonyms1.add("one");
				synonyms1.add("1");
				Set<String> synonyms2 = new LinkedHashSet<String>();
				synonyms2.add("two");
				synonyms2.add("2");
				
				Set<Set<String>> synonyms = new LinkedHashSet<Set<String>>();
				synonyms.add(synonyms1);
				synonyms.add(synonyms2);
				return synonyms;
			}
		};
		IPatternMatchContext context = new PatternMatchContext(null, localSynonymProvider);
		ISyntaxPatternPartMatch match = enumParameter.match(words, context);
		if (matchExpected) {
			assertNotNull("Match expected", match);
		} else {
			assertNull("No match expected", match);
		}
	}
}
