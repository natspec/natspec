/**
 * This package contains test that verify the extraction of syntax patterns
 * from NatSpec Java models.
 */
package de.devboost.natspec.testscripting.java.test.extraction;