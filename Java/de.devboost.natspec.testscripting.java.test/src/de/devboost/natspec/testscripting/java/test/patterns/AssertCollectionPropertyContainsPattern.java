package de.devboost.natspec.testscripting.java.test.patterns;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.AbstractSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.ObjectMatch;
import de.devboost.natspec.testscripting.patterns.parts.PropertyWord;

public class AssertCollectionPropertyContainsPattern extends AbstractSyntaxPattern<AssertCollectionPropertyContainsCodeGenerator> {
	
	private final List<ISyntaxPatternPart> parts;
	private final String propertyName;
	private final ComplexParameter objectReferencePart;
	private final ISyntaxPatternPart valueReferencePart;

	public AssertCollectionPropertyContainsPattern(String propertyName, String qualifiedName, String qualifiedCollectionContentTypeName) throws UnsupportedTypeException {
		super(new JavaParameterFactory());
		this.propertyName = propertyName;
		
		parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(new StaticWord("Assert"));
		parts.add(new StaticWord("that"));
		// add property name
		parts.add(new PropertyWord(propertyName));
		parts.add(new StaticWord("of"));
		// add object reference
		objectReferencePart = new ComplexParameter(qualifiedName);
		parts.add(objectReferencePart);
		
		parts.add(new StaticWord("contains"));
		// add value reference
		valueReferencePart = getParameterFactory().createParameter(new JavaType(qualifiedCollectionContentTypeName));
		parts.add(valueReferencePart);
	}

	@Override
	public AssertCollectionPropertyContainsCodeGenerator createUserData(ISyntaxPatternMatch<AssertCollectionPropertyContainsCodeGenerator> match) {
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match.getPartsToMatchesMap();
		ISyntaxPatternPartMatch referenceMatch = partsToMatchesMap.get(objectReferencePart);
		if (referenceMatch instanceof ObjectMatch) {
			ObjectMatch objectMatch = (ObjectMatch) referenceMatch;
			ObjectCreation entityCreation = objectMatch.getObjectCreation();
			ISyntaxPatternPartMatch valueMatch = partsToMatchesMap.get(valueReferencePart);
			return new AssertCollectionPropertyContainsCodeGenerator(match, entityCreation, propertyName, valueMatch);
		}
		
		return null;
	}

	@Override
	public List<ISyntaxPatternPart> getParts() {
		return parts;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + parts;
	}
}
