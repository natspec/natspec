package de.devboost.natspec.testscripting.java.test.patterns;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import de.devboost.natspec.NatspecFactory;
import de.devboost.natspec.Word;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.parts.ListParameter;
import de.devboost.natspec.testscripting.patterns.parts.StringParameter;
import de.devboost.natspec.testscripting.patterns.parts.WrappedPart;

public class ManyWrappedPartTest {

		@Test
		public void listTest() {
			assertMatch(match("\"joe john max\"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class))));
		}
	
		@Test
		public void testIssueNATSPEC373() {
			// Check that the exception is not present anymore
			
			match("\" joe\"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class)));
			match("\"joe \"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class)));
			
			// FIXME NATSPEC-373 Enable this test if we decided if this should match or not
			//assertMatch(match("\" joe\"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class))));
			//assertMatch(match("\"joe \"", "\"", "\"", new ListParameter(new StringParameter(), new JavaType(String.class))));
		}
	
		private void assertMatch(ISyntaxPatternPartMatch match) {
			assertNotNull(match);
			assertTrue(match.hasMatched());
		}
		
		protected ISyntaxPatternPartMatch match(String input, String prefix, String suffix) {
			ISyntaxPatternPart delegate = new StringParameter();;
			return match(input, prefix, suffix, delegate);
		}
		
		protected ISyntaxPatternPartMatch match(String input, String prefix, String suffix, ISyntaxPatternPart delegate) {
			WrappedPart wrappedPart = new WrappedPart(delegate, prefix, suffix);
			
			List<Word> words = new ArrayList<Word>();
			String[] stringWords = input.split(" ");
			for (String stringWord : stringWords) {
				Word word = NatspecFactory.eINSTANCE.createWord();
				word.setText(stringWord);
				words.add(word);
			}
			
			return wrappedPart.match(words, null);
		}
	}