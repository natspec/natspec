package de.devboost.natspec.testscripting.java.test.registry;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.patterns.IClass;
import de.devboost.natspec.testscripting.patterns.ICodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.IParameter;
import de.devboost.natspec.testscripting.patterns.parts.IParameterFactory;

public class PatternParameters {
	
	public static final String INITIAL_METHOD_NAME = "methodA";
	public static final String INITIAL_SIMPLE_TYPE_NAME = "MyTestSupportClass";
	public static final String INITIAL_PACKAGE = "mypackage";
	public static final String INITIAL_PROJECT_NAME = "projectA";
	public static final IClass INITIAL_RETURN_TYPE = null;

	private String projectName = INITIAL_PROJECT_NAME;
	private String packageName = INITIAL_PACKAGE;
	private String simpleTypeName = INITIAL_SIMPLE_TYPE_NAME;
	private String methodName = INITIAL_METHOD_NAME;
	private List<IParameter> parameters = new ArrayList<IParameter>();
	private IClass returnType = INITIAL_RETURN_TYPE;
	private List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
	private Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
	private IParameterFactory parameterFactory = new JavaParameterFactory();
	private ICodeGeneratorFactory<JavaCodeGenerator> codeGeneratorFactory = new JavaCodeGeneratorFactory();

	public PatternParameters() {
		super();
	}

	public String getProjectName() {
		return projectName;
	}

	public String getQualifiedTypeName() {
		return getPackage() + "." + getSimpleTypeName();
	}

	protected String getPackage() {
		return packageName;
	}

	public String getMethodName() {
		return methodName;
	}

	public List<IParameter> getParameters() {
		return parameters;
	}

	public IClass getReturnType() {
		return returnType;
	}

	public List<ISyntaxPatternPart> getParts() {
		return parts;
	}

	public Map<ISyntaxPatternPart, Integer> getPartToParameterIndexMap() {
		return partToParameterIndexMap;
	}

	public IParameterFactory getParameterFactory() {
		return parameterFactory;
	}

	public ICodeGeneratorFactory<JavaCodeGenerator> getCodeGeneratorFactory() {
		return codeGeneratorFactory;
	}

	public String getSimpleTypeName() {
		return simpleTypeName;
	}

	public void setSimpleTypeName(String simpleTypeName) {
		this.simpleTypeName = simpleTypeName;
	}

	public void setReturnType(IClass returnType) {
		this.returnType = returnType;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
}
