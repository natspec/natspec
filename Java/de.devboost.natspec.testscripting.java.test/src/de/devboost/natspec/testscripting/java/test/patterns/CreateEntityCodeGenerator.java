package de.devboost.natspec.testscripting.java.test.patterns;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.base.Joiner;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.JavaMatchCodeGeneratorFactory;
import de.devboost.natspec.testscripting.patterns.AbstractCodeGenerator;
import de.devboost.natspec.testscripting.patterns.ICodeGenerationContext;
import de.devboost.natspec.testscripting.patterns.PrefixBasedCommentGenerator;

public class CreateEntityCodeGenerator extends AbstractCodeGenerator {

	private Collection<ISyntaxPatternPartMatch> parameterMatches;
	private String qualifiedClassName;
	private ObjectCreation entityCreation;
	
	public CreateEntityCodeGenerator(
			ISyntaxPatternMatch<CreateEntityCodeGenerator> match, Collection<ISyntaxPatternPartMatch> parameterMatches,
			String qualifiedClassName, ObjectCreation entityCreation) {
		
		super(match, new JavaMatchCodeGeneratorFactory(), new PrefixBasedCommentGenerator("//"));
		this.parameterMatches = parameterMatches;
		this.qualifiedClassName = qualifiedClassName;
		this.entityCreation = entityCreation;
	}

	@Override
	public void generateSourceCode(ICodeGenerationContext context) {
		// assign to new variable
		context.addCode(getComment() + qualifiedClassName + " " + entityCreation.getVariableName() +" = new " + qualifiedClassName + "(");
		// collect arguments
		List<String> arguments = new ArrayList<String>();
		for (ISyntaxPatternPartMatch match : parameterMatches) {
			String code = getCode(match);
			arguments.add(code);
		}

		String argumentList = Joiner.on(", ").join(arguments);
		context.addCode(argumentList);
		context.addCode(");\n");
	}
}
