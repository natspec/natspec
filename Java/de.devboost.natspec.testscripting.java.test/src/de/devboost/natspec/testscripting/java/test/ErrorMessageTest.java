package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.ExpectationHelper;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;

@RunWith(Parameterized.class)
public class ErrorMessageTest extends AbstractNatSpecTestCase {

	public ErrorMessageTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testExpectationMessage() {
		String simpleName = "MyType";
		String type = "com.acme." + simpleName;

		// add dynamic pattern 'Custom create MyType <String>'
		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(new StaticWord("Prefix"));
		parts.add(new StaticWord("Suffix1"));

		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		ISyntaxPattern<?> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				null, null, "method1", null, new JavaType(type), parts,
				partToParameterIndexMap, null, new JavaCodeGeneratorFactory());

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);

		assertMatch(pattern, "Prefix Suffix1", true, context);
		assertMatch(pattern, "Prefix Suffix2", false, context);

		List<ISyntaxPatternMatch<? extends Object>> matches = getAllMatches(
				pattern, "Prefix Suffix2", context);

		List<ISyntaxPatternPart> expectedParts = new ExpectationHelper()
				.getExpectedParts(matches);
		assertEquals(1, expectedParts.size());
		ISyntaxPatternPart expectedPart = expectedParts.get(0);
		assertTrue(expectedPart instanceof StaticWord);
		StaticWord expectedWord = (StaticWord) expectedPart;
		assertEquals("Suffix1", expectedWord.getText());
	}
}
