/**
 * This package contains tests that ensure the Java-based syntax patterns are
 * correctly handled by the 
 * {@link de.devboost.natspec.registries.SyntaxPatternRegistry}.
 */
package de.devboost.natspec.testscripting.java.test.registry;