/**
 * This package contains tests that verify the behavior of the 
 * {@link de.devboost.natspec.testscripting.context.PatternMatchContext}.
 */
package de.devboost.natspec.testscripting.java.test.context;