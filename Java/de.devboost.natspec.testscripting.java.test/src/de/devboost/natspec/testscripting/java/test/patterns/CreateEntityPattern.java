package de.devboost.natspec.testscripting.java.test.patterns;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.matching.WordUtil;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.AbstractSyntaxPattern;

public class CreateEntityPattern extends
		AbstractSyntaxPattern<CreateEntityCodeGenerator> {

	private final List<ISyntaxPatternPart> parts;
	private final List<ISyntaxPatternPart> identifierParts = new ArrayList<ISyntaxPatternPart>();
	private final String simpleClassName;
	private final String qualifiedClassName;

	public CreateEntityPattern(String qualifiedClassName,
			String simpleClassName, List<String> boundQualifiedTypes) throws UnsupportedTypeException {
		super(new JavaParameterFactory());
		this.simpleClassName = simpleClassName;
		this.qualifiedClassName = qualifiedClassName;

		parts = new ArrayList<ISyntaxPatternPart>();
		parts.add(new StaticWord("Create"));
		parts.add(new StaticWord(simpleClassName));
		// add arguments to pattern
		for (String boundType : boundQualifiedTypes) {
			ISyntaxPatternPart part = getParameterFactory().createParameter(
					new JavaType(boundType));
			parts.add(part);
			identifierParts.add(part);
		}
	}

	@Override
	public List<ISyntaxPatternPart> getParts() {
		return parts;
	}

	@Override
	public CreateEntityCodeGenerator createUserData(
			ISyntaxPatternMatch<CreateEntityCodeGenerator> match) {

		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match
				.getPartsToMatchesMap();

		List<ISyntaxPatternPartMatch> parameterMatches = new ArrayList<ISyntaxPatternPartMatch>(
				parts.size() - 2);

		List<String> identifiers = new ArrayList<String>();
		for (ISyntaxPatternPart part : identifierParts) {
			ISyntaxPatternPartMatch partMatch = partsToMatchesMap.get(part);
			List<String> words = WordUtil.INSTANCE.toStringList(partMatch
					.getMatchedWords());
			identifiers.addAll(words);
			parameterMatches.add(partMatch);
		}
		
		IPatternMatchContext context = match.getContext();
		ObjectCreation entityCreation = new ObjectCreation(context,
				new JavaType(qualifiedClassName), identifiers);
		context.addObjectToContext(entityCreation);

		return new CreateEntityCodeGenerator(match, parameterMatches,
				qualifiedClassName, entityCreation);
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + simpleClassName + " ["
				+ qualifiedClassName + "]";
	}
}
