package de.devboost.natspec.testscripting.java.test.patterns;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.ISyntaxPatternPartMatch;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.patterns.parts.UnsupportedTypeException;
import de.devboost.natspec.testscripting.context.ObjectCreation;
import de.devboost.natspec.testscripting.java.extract.JavaParameterFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.AbstractSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.parts.ComplexParameter;
import de.devboost.natspec.testscripting.patterns.parts.ImplicitParameter;
import de.devboost.natspec.testscripting.patterns.parts.ObjectMatch;
import de.devboost.natspec.testscripting.patterns.parts.PropertyWord;

public class ImplicitParameterPattern extends
		AbstractSyntaxPattern<SetPropertyCodeGenerator> {

	private final List<ISyntaxPatternPart> parts;
	private final String propertyName;
	// TODO This field is never initialized
	private ComplexParameter objectReferencePart;
	private final ISyntaxPatternPart newValuePart;
	private final String qualifiedName;
	private final String simpleName;

	public ImplicitParameterPattern(String propertyName, String qualifiedName,
			String simpleName, String qualifiedTypeName)
			throws UnsupportedTypeException {
		this(propertyName, qualifiedName, simpleName, qualifiedTypeName, false);
	}

	public ImplicitParameterPattern(String propertyName, String qualifiedName,
			String simpleName, String qualifiedTypeName,
			boolean considerLastObjectOnly) throws UnsupportedTypeException {

		super(new JavaParameterFactory());
		this.propertyName = propertyName;
		this.qualifiedName = qualifiedName;
		this.simpleName = simpleName;
		
		parts = new ArrayList<ISyntaxPatternPart>();

		// add implicit object reference
		ISyntaxPatternPart objectReferencePart = new ImplicitParameter(qualifiedName, considerLastObjectOnly);
		parts.add(objectReferencePart);

		parts.add(new StaticWord("With"));

		// add argument (new value)
		newValuePart = getParameterFactory()
				.createParameter(new JavaType(qualifiedTypeName));
		parts.add(newValuePart);

		// add property name
		parts.add(new PropertyWord(propertyName));
	}

	@Override
	public SetPropertyCodeGenerator createUserData(
			ISyntaxPatternMatch<SetPropertyCodeGenerator> match) {
		Map<ISyntaxPatternPart, ISyntaxPatternPartMatch> partsToMatchesMap = match
				.getPartsToMatchesMap();
		ISyntaxPatternPartMatch referenceMatch = partsToMatchesMap
				.get(objectReferencePart);
		if (referenceMatch instanceof ObjectMatch) {
			ObjectMatch objectMatch = (ObjectMatch) referenceMatch;
			ObjectCreation entityCreation = objectMatch.getObjectCreation();
			ISyntaxPatternPartMatch valueMatch = partsToMatchesMap
					.get(newValuePart);
			return new SetPropertyCodeGenerator(match, entityCreation,
					propertyName, valueMatch);
		}

		return null;
	}

	@Override
	public List<ISyntaxPatternPart> getParts() {
		return parts;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " : " + simpleName + "."
				+ propertyName + " [" + qualifiedName + "]";
	}
}
