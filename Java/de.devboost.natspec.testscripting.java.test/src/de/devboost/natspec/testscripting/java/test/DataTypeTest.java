package de.devboost.natspec.testscripting.java.test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternPart;
import de.devboost.natspec.patterns.parts.DoubleArgument;
import de.devboost.natspec.patterns.parts.StaticWord;
import de.devboost.natspec.test.AbstractNatSpecTestCase;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;

/**
 * A test that checks whether different data types are detected correctly.
 */
@RunWith(Parameterized.class)
public class DataTypeTest extends AbstractNatSpecTestCase {

	public DataTypeTest(boolean useOptimizedMatcher, boolean usePrioritizer) {
		super(useOptimizedMatcher, usePrioritizer);
	}

	@Test
	public void testDoubleMatching() {
		ISyntaxPattern<JavaCodeGenerator> pattern = createFloatPattern();

		IPatternMatchContext context = PatternMatchContextFactory.INSTANCE
				.createPatternMatchContext(null);

		assertMatch(pattern, "Float 1.1", true, context);
		assertMatch(pattern, "Float 1", true, context);
	}

	private ISyntaxPattern<JavaCodeGenerator> createFloatPattern() {

		List<ISyntaxPatternPart> parts = new ArrayList<ISyntaxPatternPart>();
		;
		parts.add(new StaticWord("Float"));
		parts.add(new DoubleArgument());

		Map<ISyntaxPatternPart, Integer> partToParameterIndexMap = new LinkedHashMap<ISyntaxPatternPart, Integer>();
		ISyntaxPattern<JavaCodeGenerator> pattern = new DynamicSyntaxPattern<JavaCodeGenerator>(
				null, null, "handleFloat", null, (JavaType) null, parts,
				partToParameterIndexMap, null, new JavaCodeGeneratorFactory());
		return pattern;
	}
}
