package de.devboost.natspec.testscripting.java.test;

import static org.junit.Assert.assertTrue;

import java.util.Collections;

import org.junit.Test;

import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.util.ComparisonHelper;

public class ParameterComparisonTest {

	/**
	 * This test checks whether one of the root causes for NATSPEC-52 has been
	 * resolved (identical parameters were not identified as equal which caused
	 * infinite revalidation of NatSpec files) because NatSpec thought patterns
	 * have change while they actually did not change.
	 */
	@Test
	public void testParameterComparison() {
		ComparisonHelper comparisonHelper = ComparisonHelper.INSTANCE;
		JavaParameter parameter1 = new JavaParameter("p1", new JavaType(String.class), Collections.<JavaAnnotation>emptyList());
		JavaParameter parameter2 = new JavaParameter("p1", new JavaType(String.class), Collections.<JavaAnnotation>emptyList());
		boolean equal = comparisonHelper.isEqualTo(parameter1, parameter2);
		assertTrue("Parameters with same name and same type must be equal", equal);
	}
}
