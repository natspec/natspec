package de.devboost.natspec.testscripting.java.ui.links;

import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.hyperlink.IHyperlink;

import de.devboost.natspec.patterns.ISyntaxPatternSourceProvider;
import de.devboost.natspec.testscripting.java.ui.actions.GoToTestSupportMethodAction;

public class TestSupportMethodLink implements IHyperlink {

	private final IRegion region;
	private final ISyntaxPatternSourceProvider pattern;

	public TestSupportMethodLink(IRegion region, ISyntaxPatternSourceProvider pattern) {
		this.region = region;
		this.pattern = pattern;
	}

	@Override
	public IRegion getHyperlinkRegion() {
		return region;
	}

	@Override
	public String getTypeLabel() {
		return null;
	}

	@Override
	public String getHyperlinkText() {
		String typeName = pattern.getQualifiedTypeName();
		String methodName = pattern.getMethodName();
		if (methodName == null) {
			return "Go to " + typeName;
		} else {
			return "Go to " + typeName + "." + methodName;
		}
	}

	@Override
	public void open() {
		GoToTestSupportMethodAction action = new GoToTestSupportMethodAction(pattern);
		action.execute();
	}
}
