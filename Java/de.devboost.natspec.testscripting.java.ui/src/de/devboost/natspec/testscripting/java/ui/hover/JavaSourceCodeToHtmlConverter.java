package de.devboost.natspec.testscripting.java.ui.hover;

public class JavaSourceCodeToHtmlConverter {

	public final static JavaSourceCodeToHtmlConverter INSTANCE = new JavaSourceCodeToHtmlConverter();

	private JavaSourceCodeToHtmlConverter() {
	}

	public String toHtml(String source) {
		source = shiftLeft(source);
		source = doToHtml(source);
		return source;
	}

	private String shiftLeft(String source) {
		// Remove Javadoc
		source = source.replaceFirst("^/\\*\\*(.|\\s)+?\\*/", "");
		int tabsLeft = Integer.MAX_VALUE;
		String[] lines = source.split("(\r\n)|(\r)|(\n)");
		if (lines.length == 0) {
			return source;
		}
		for (int i = 1; i < lines.length; i++) {
			String line = lines[i];
			if (line.trim().isEmpty()) {
				continue;
			}
			int tabsLeftInLine = 0;
			for (int charIndex = 0; charIndex < line.length(); charIndex++) {
				boolean isTab = line.charAt(charIndex) == '\t';
				if (!isTab) {
					break;
				}
				tabsLeftInLine++;
			}
			
			if (tabsLeftInLine < tabsLeft) {
				tabsLeft = tabsLeftInLine;
			}
		}
		
		StringBuilder newSource = new StringBuilder();
		newSource.append(lines[0]);
		newSource.append("\n");
		for (int i = 1; i < lines.length; i++) {
			String line = lines[i];
			if (!line.trim().isEmpty()) {
				newSource.append(line.substring(tabsLeft));
			}
			newSource.append("\n");
		}
		return newSource.toString();
	}

	private String doToHtml(String source) {
		source = source.replace("\r\n", "<br/>");
		source = source.replace("\r", "<br/>");
		source = source.replace("\n", "<br/>");
		source = source.replace("public", "<b>public</b>");
		source = source.replace("protected", "<b>protected</b>");
		source = source.replace("private", "<b>private</b>");
		source = source.replace("void", "<b>void</b>");
		source = source.replace("return", "<b>return</b>");
		source = source.replace("new", "<b>new</b>");
		source = source.replace("final", "<b>final</b>");
		return "<pre>" + source + "</pre>";
	}
}
