package de.devboost.natspec.testscripting.java.ui.quickfixes;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.internal.corext.refactoring.util.RefactoringASTParser;
import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.internal.ui.actions.WorkbenchRunnableAdapter;
import org.eclipse.jdt.internal.ui.javaeditor.ASTProvider;
import org.eclipse.jdt.internal.ui.util.BusyIndicatorRunnableContext;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.java.jdt.ConcreteJDTUtility;

@SuppressWarnings("restriction")
public class AddTestSupportMethodExecutor {

	public void execute(Sentence sentence, URI uri, String pathToProviderClass) {
		ConcreteJDTUtility jdtUtility = ConcreteJDTUtility.INSTANCE;

		IType[] types;
		try {
			types = jdtUtility.getJavaTypes(pathToProviderClass);
		} catch (JavaModelException e) {
			TestConnectorPlugin.logError("Exception while preparing quick fix to add test support method.", e);
			return;
		}
		
		if (types == null || types.length < 1) {
			return;
		}
		
		IType type = types[0];
		
		int level = ASTProvider.SHARED_AST_LEVEL;
		RefactoringASTParser parser = new RefactoringASTParser(level);
		ICompilationUnit compilationUnit = type.getCompilationUnit();
		CompilationUnit unit = parser.parse(compilationUnit, true);

		AddTestSupportMethodOperation operation = new AddTestSupportMethodOperation(type, unit, sentence, uri);

		IRunnableContext context = JavaPlugin.getActiveWorkbenchWindow();
		if (context == null) {
			context = new BusyIndicatorRunnableContext();
		}

		try {
			ISchedulingRule schedulingRule = operation.getSchedulingRule();
			WorkbenchRunnableAdapter adapter = new WorkbenchRunnableAdapter(operation, schedulingRule);
			IWorkbench workbench = PlatformUI.getWorkbench();
			IProgressService progressService = workbench.getProgressService();
			progressService.runInUI(context, adapter, schedulingRule);
		} catch (InvocationTargetException e) {
			TestConnectorPlugin.logError("Exception while executing quick fix to add test support method.", e);
		} catch (InterruptedException e) {
			TestConnectorPlugin.logError("Exception while executing quick fix to add test support method.", e);
		}
	}
}
