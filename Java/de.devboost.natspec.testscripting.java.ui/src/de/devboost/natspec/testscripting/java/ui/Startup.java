package de.devboost.natspec.testscripting.java.ui;

import org.eclipse.ui.IStartup;

import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.resource.natspec.ui.DynamicPatternHoverTextProvider;
import de.devboost.natspec.resource.natspec.ui.NatspecHoverTextProvider;
import de.devboost.natspec.resource.natspec.ui.NatspecUIMetaInformation;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.java.GeneratedJavaFileFinder;
import de.devboost.natspec.testscripting.java.extract.JavaCodeGeneratorFactory;
import de.devboost.natspec.testscripting.java.jdt.debug.JavaBreakpointUtils;
import de.devboost.natspec.testscripting.java.ui.actions.GoToTestSupportClassAction;
import de.devboost.natspec.testscripting.java.ui.actions.GoToTestSupportMethodAction;
import de.devboost.natspec.testscripting.java.ui.hover.JavaHoverTextProvider;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.ui.debug.GeneratedClassLineBreakpointAdapter;
import de.devboost.natspec.testscripting.ui.views.IPatternClassClickHandler;
import de.devboost.natspec.testscripting.ui.views.IPatternClickHandler;
import de.devboost.natspec.testscripting.ui.views.SyntaxPatternView;

public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		TestConnectorPlugin.getInstance();
		// This triggers the activation of this bundle, which causes some side
		// effects (e.g., registration of listeners).
		Activator.getDefault();
		
		registerBreakPointAdapter();
		registerSyntaxViewClickHandlers();
		new PerspectiveRestorer().restorePerspectiveIfRequired();
		new ExampleProjectImporter().importExampleProjectIfRequired();
		
		NatspecHoverTextProvider.ADDITIONAL_PROVIDERS.add(new JavaHoverTextProvider());
		NatspecHoverTextProvider.ADDITIONAL_PROVIDERS.add(new DynamicPatternHoverTextProvider());
	}

	private void registerSyntaxViewClickHandlers() {
		SyntaxPatternView.CLICK_HANDLERS.add(new IPatternClickHandler() {
			
			@Override
			public boolean handle(ISyntaxPattern<?> pattern) {
				if (pattern instanceof DynamicSyntaxPattern) {
					DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
					if (!(dynamicSyntaxPattern.getCodeGeneratorFactory() instanceof JavaCodeGeneratorFactory)) {
						return false;
					}
					new GoToTestSupportMethodAction(dynamicSyntaxPattern).execute();
					return true;
				}
				
				// Can't handle this kind of pattern. Maybe someone else can.
				return false;
			}
		});
		SyntaxPatternView.CLASS_CLICK_HANDLERS.add(new IPatternClassClickHandler() {
			
			@Override
			public boolean handle(String projectName, String qualifiedClassName) {
				new GoToTestSupportClassAction(projectName, qualifiedClassName).execute();
				return true;
			}
		});
	}

	private void registerBreakPointAdapter() {
		if (!TestConnectorPlugin.getInstance().isDebugFeatureActive()) {
			return;
		}
		
		// register break point adapter
		GeneratedJavaFileFinder generatedFileFinder = new GeneratedJavaFileFinder();
		JavaBreakpointUtils breakpointUtils = new JavaBreakpointUtils(generatedFileFinder);
		GeneratedClassLineBreakpointAdapter javaLineBreakpointAdapter = new GeneratedClassLineBreakpointAdapter(
				generatedFileFinder, breakpointUtils);
		NatspecUIMetaInformation.REGISTERED_TOGGLE_BREAKPOINT_TARGETS.add(javaLineBreakpointAdapter);
	}
}
