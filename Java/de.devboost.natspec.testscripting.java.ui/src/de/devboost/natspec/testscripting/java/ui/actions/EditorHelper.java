package de.devboost.natspec.testscripting.java.ui.actions;

import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.internal.ui.javaeditor.JavaEditor;

import de.devboost.eclipse.jdtutilities.ui.JavaEditorHelper;
import de.devboost.natspec.testscripting.java.jdt.ConcreteJDTUtility;
import de.devboost.natspec.testscripting.patterns.IParameter;
import de.devboost.natspec.testscripting.ui.util.TypeHelper;

public class EditorHelper {
	
	public final static EditorHelper INSTANCE = new EditorHelper();

	private EditorHelper() {
	}

	public void openEditor(String typeName, String methodName, List<? extends IParameter> parameters,
			String projectName) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject(projectName);
		if (project == null) {
			return;
		}

		ConcreteJDTUtility jdtUtility = ConcreteJDTUtility.INSTANCE;
		IJavaProject javaProject = jdtUtility.getJavaProject(project);
		if (javaProject == null) {
			return;
		}
		try {
			IType type = javaProject.findType(typeName);
			if (type == null) {
				return;
			}
			
			openEditor(type, methodName, parameters);
		} catch (JavaModelException e) {
			// ignore
		}
	}

	private void openEditor(IType type, String methodName, List<? extends IParameter> parameters) {
		List<String> parameterTypeNames = new TypeHelper().getTypeNamesForParameters(parameters);
		JavaEditorHelper editorHelper = JavaEditorHelper.INSTANCE;
		if (methodName == null) {
			JavaEditor javaEditor = editorHelper.openJavaEditor(type);
			javaEditor.setSelection(type);
		} else {
			editorHelper.switchToJavaEditor(type, methodName, parameterTypeNames);
		}
	}
}
