package de.devboost.natspec.testscripting.java.ui;

import java.util.Collections;
import java.util.Map;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import de.devboost.natspec.resource.natspec.ui.NatspecEditor;
import de.devboost.natspec.resource.natspec.ui.NatspecNewProjectWizard;
import de.devboost.natspec.resource.natspec.ui.NatspecNewProjectWizardLogic;
import de.devboost.natspec.resource.natspec.ui.NatspecUIPlugin;
import de.devboost.shared.eclipse.projectimport.AbstractExampleProjectImporter;

public class ExampleProjectImporter extends AbstractExampleProjectImporter {

	private static final String EXAMPLE_PROJECT_NAME = "com.nat-spec.airline.example";
	private static final String EXAMPLE_FILE_PATH = "/" + EXAMPLE_PROJECT_NAME + "/src/com/nat_spec/airline/example/test/scenarios/BookSeat.natspec";

	@Override
	protected AbstractUIPlugin getPlugin() {
		return Activator.getDefault();
	}

	@Override
	protected Map<String, String> getExampleProjectMap() {
		return Collections.singletonMap(EXAMPLE_PROJECT_NAME,
				NatspecNewProjectWizard.NEW_PROJECT_ZIP_FILE_NAME);
	}

	@Override
	protected void createExampleProject(IProgressMonitor monitor,
			IPath projectPath, String projectName, String newProjectZip)
			throws InterruptedException {

		String pluginID = NatspecUIPlugin.PLUGIN_ID;
		new NatspecNewProjectWizardLogic().createExampleProject(monitor,
				projectPath, projectName, pluginID, newProjectZip);
	}

	@Override
	protected void logError(String message, Exception e) {
		NatspecUIPlugin.logError(message, e);
	}

	@Override
	protected String getExampleFilePath() {
		return EXAMPLE_FILE_PATH;
	}

	@Override
	protected String getEditorID() {
		return NatspecEditor.class.getName();
	}

	@Override
	protected String getJobName() {
		return "Open NatSpec example file";
	}
}
