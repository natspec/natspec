package de.devboost.natspec.testscripting.java.ui.actions;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.testscripting.patterns.IParameter;

public class GoToTestSupportClassAction {

	private final String projectName;
	private final String qualifiedClassName;

	public GoToTestSupportClassAction(String projectName, String qualifiedClassName) {
		this.projectName = projectName;
		this.qualifiedClassName = qualifiedClassName;
	}

	public void execute() {
		String methodName = null;
		List<IParameter> parameters = Collections.emptyList();
		EditorHelper.INSTANCE.openEditor(qualifiedClassName, methodName, parameters, projectName);
	}
}
