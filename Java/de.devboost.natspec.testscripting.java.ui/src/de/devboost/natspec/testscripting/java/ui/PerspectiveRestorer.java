package de.devboost.natspec.testscripting.java.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import de.devboost.natspec.doc.NatSpecHelpView;
import de.devboost.natspec.resource.natspec.ui.NatspecUIPlugin;
import de.devboost.shared.eclipse.projectimport.AbstractPerspectiveRestorer;

public class PerspectiveRestorer extends AbstractPerspectiveRestorer {

	@Override
	protected IStatus doInitWindowLayout() {
		IStatus superStatus = super.doInitWindowLayout();
		if (!superStatus.isOK()) {
			return superStatus;
		}
		
		// Show NatSpec help
		final IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		final IWorkbenchPage activePage = workbenchWindow.getActivePage();
		try {
			activePage.showView(NatSpecHelpView.ID_HELP_VIEW);
		} catch (PartInitException e) {
			logError(e.getMessage(), e);
		}
		return Status.OK_STATUS;
	}
	
	@Override
	protected AbstractUIPlugin getPlugin() {
		return Activator.getDefault();
	}
	
	@Override
	protected void logError(String message, Exception e) {
		NatspecUIPlugin.logError(message, e);
	}
	
	@Override
	protected String getJobName() {
		return "Initializing NatSpec window layout";
	}
}
