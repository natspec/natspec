package de.devboost.natspec.testscripting.java.ui.quickfixes;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;

import de.devboost.natspec.resource.natspec.INatspecQuickFix;
import de.devboost.natspec.resource.natspec.ui.NatspecImageProvider;
import de.devboost.natspec.resource.natspec.ui.NatspecUIMetaInformation;

public class TestSupportQuickFixProposal implements ICompletionProposal {

	private static final NatspecUIMetaInformation NATSPEC_UI_META_INFORMATION = new NatspecUIMetaInformation();

	private final INatspecQuickFix quickFix;
	private final ISourceViewer sourceViewer;

	public TestSupportQuickFixProposal(INatspecQuickFix quickFix, ISourceViewer sourceViewer) {
		this.quickFix = quickFix;
		this.sourceViewer = sourceViewer;
	}

	public Point getSelection(IDocument document) {
		return null;
	}

	public Image getImage() {
		NatspecImageProvider imageProvider = NATSPEC_UI_META_INFORMATION.getImageProvider();
		String imageKey = quickFix.getImageKey();
		return imageProvider.getImage(imageKey);
	}

	public String getDisplayString() {
		return quickFix.getDisplayString();
	}

	public IContextInformation getContextInformation() {
		return null;
	}

	public String getAdditionalProposalInfo() {
		return null;
	}

	public void apply(IDocument document) {
		IDocument sourceViewerDocument = sourceViewer.getDocument();
		String currentContent = sourceViewerDocument.get();
		String newContent = quickFix.apply(currentContent);
		if (newContent != null) {
			sourceViewerDocument.set(newContent);
		}
	}
}
