package de.devboost.natspec.testscripting.java.ui.quickfixes;

import java.util.Comparator;

import de.devboost.natspec.resource.natspec.INatspecQuickFix;

public class QuickFixComparator implements Comparator<INatspecQuickFix> {
	
	public static final Comparator<? super INatspecQuickFix> INSTANCE = new QuickFixComparator();
	
	private QuickFixComparator() {
		super();
	}

	@Override
	public int compare(INatspecQuickFix fix1, INatspecQuickFix fix2) {
		String displayString1 = fix1.getDisplayString();
		String displayString2 = fix2.getDisplayString();
		return displayString1.compareTo(displayString2);
	}
}
