package de.devboost.natspec.testscripting.java.ui.quickfixes;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.NodeFinder;
import org.eclipse.jdt.core.dom.rewrite.ASTRewrite;
import org.eclipse.jdt.core.dom.rewrite.ImportRewrite;
import org.eclipse.jdt.core.dom.rewrite.ListRewrite;
import org.eclipse.jdt.core.formatter.CodeFormatter;
import org.eclipse.jdt.internal.corext.codemanipulation.StubUtility;
import org.eclipse.jdt.internal.corext.codemanipulation.StubUtility2;
import org.eclipse.jdt.internal.corext.dom.ASTNodes;
import org.eclipse.jdt.internal.corext.util.CodeFormatterUtil;
import org.eclipse.jdt.internal.corext.util.Resources;
import org.eclipse.jdt.ui.CodeStyleConfiguration;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.text.edits.TextEdit;

import de.devboost.eclipse.jdtutilities.ui.JavaEditorHelper;
import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.java.quickfixes.IDerivedMethod;
import de.devboost.natspec.testscripting.java.quickfixes.SentenceToMethodConverter;
import de.devboost.natspec.testscripting.ui.util.TypeHelper;

/**
 * Workspace runnable to add test support methods for unknown sentences.
 */
@SuppressWarnings({ "restriction", "deprecation" })
public class AddTestSupportMethodOperation implements IWorkspaceRunnable {

	/** The type declaration to add the test support method to */
	private final IType type;

	/** The compilation unit AST node */
	private final CompilationUnit compilationUnitASTRoot;

	/** The yet unknown sentence we'd like to create a method for */
	private final Sentence sentence;

	/** The URI of the document containing the yet unknown sentence */
	private final URI uri;

	private final SentenceToMethodConverter converter = new SentenceToMethodConverter(
			NatSpecPlugin.USE_TREE_BASED_MATCHER, NatSpecPlugin.USE_PRIORITIZER);

	/** The resulting text edit */
	private TextEdit textEdit = null;

	/**
	 * Creates a new add test support method operation.
	 * 
	 * @param type
	 *            the type to add the test support method to
	 * @param unit
	 *            the compilation unit AST node
	 * @param sentence
	 *            the sentence to derive the method from
	 */
	public AddTestSupportMethodOperation(IType type, CompilationUnit unit, Sentence sentence, URI uri) {
		Assert.isNotNull(type);
		Assert.isNotNull(unit);
		Assert.isNotNull(sentence);

		this.type = type;
		this.compilationUnitASTRoot = unit;
		this.sentence = sentence;
		this.uri = uri;
	}

	/**
	 * Adds a new test support method.
	 * 
	 * @param type
	 *            the type
	 * @param contents
	 *            the contents of the method
	 * @param rewrite
	 *            the list rewrite to use
	 * @param insertion
	 *            the insertion point
	 * @param javaProject
	 * @return
	 * 
	 * @throws JavaModelException
	 *             if an error occurs
	 */
	private void addTestSupportMethod(IType type, String contents, ListRewrite rewrite, ASTNode insertion,
			IJavaProject javaProject) throws JavaModelException {

		String delimiter = StubUtility.getLineDelimiterUsed(type);
		ASTRewrite astRewrite = rewrite.getASTRewrite();
		int formatterKind = CodeFormatter.K_CLASS_BODY_DECLARATIONS;
		String format = CodeFormatterUtil.format(formatterKind, contents, 0, delimiter, javaProject);
		int methodDeclaration = ASTNode.METHOD_DECLARATION;
		MethodDeclaration declaration = (MethodDeclaration) astRewrite.createStringPlaceholder(format,
				methodDeclaration);
		if (insertion != null) {
			rewrite.insertBefore(declaration, insertion, null);
		} else {
			rewrite.insertLast(declaration, null);
		}
	}

	/**
	 * Generates a new test support method.
	 * 
	 * @param type
	 *            the type to add the method to
	 * @param rewrite
	 *            the list rewrite to use
	 * 
	 * @return the derived method
	 * 
	 * @throws CoreException
	 *             if an error occurs
	 * @throws OperationCanceledException
	 *             if the operation has been cancelled
	 */
	private IDerivedMethod generateTestSupportMethod(IType type, ListRewrite rewrite)
			throws CoreException, OperationCanceledException {

		IDerivedMethod derivedMethod = converter.deriveMethod(sentence, uri);

		// insert after last element
		IJavaElement sibling = null;
		ASTNode insertion = StubUtility2.getNodeToInsertBefore(rewrite, sibling);
		String methodCode = derivedMethod.getCode();
		addTestSupportMethod(type, methodCode, rewrite, insertion, type.getJavaProject());
		return derivedMethod;
	}

	/**
	 * Returns the resulting text edit.
	 * 
	 * @return the resulting text edit
	 */
	public TextEdit getResultingEdit() {
		return textEdit;
	}

	/**
	 * Returns the scheduling rule for this operation.
	 * 
	 * @return the scheduling rule
	 */
	public ISchedulingRule getSchedulingRule() {
		return ResourcesPlugin.getWorkspace().getRoot();
	}

	@Override
	public void run(IProgressMonitor monitor) throws CoreException {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		try {
			monitor.setTaskName("Generate test support method");
			monitor.beginTask("", 1); //$NON-NLS-1$

			ICompilationUnit unit = type.getCompilationUnit();

			TextEdit importEdit = createImportEdit(unit);

			ASTRewrite astRewrite = ASTRewrite.create(compilationUnitASTRoot.getAST());
			ListRewrite listRewriter = createAddMethodRewrite(astRewrite);

			IDerivedMethod derivedMethod = generateTestSupportMethod(type, listRewriter);
			textEdit = astRewrite.rewriteAST();
			textEdit.addChild(importEdit);
			boolean save = true;
			applyEdit(unit, textEdit, save, new SubProgressMonitor(monitor, 1));

			String methodName = derivedMethod.getName();

			List<JavaType> parameterTypes = derivedMethod.getParameterTypes();
			List<String> parameterTypeNames = new TypeHelper().getTypeNames(parameterTypes);
			JavaEditorHelper.INSTANCE.switchToJavaEditor(type, methodName, parameterTypeNames);
		} finally {
			monitor.done();
		}
	}

	/**
	 * Applies an text edit to a compilation unit. Filed bug 117694 against jdt.core.
	 * 
	 * @param cu
	 *            the compilation unit to apply the edit to
	 * @param edit
	 *            the edit to apply
	 * @param save
	 *            is set, save the CU after the edit has been applied
	 * @param monitor
	 *            the progress monitor to use
	 * @throws CoreException
	 *             Thrown when the access to the CU failed
	 */
	private void applyEdit(ICompilationUnit cu, TextEdit edit, boolean save, IProgressMonitor monitor)
			throws CoreException {

		IFile file = (IFile) cu.getResource();
		if (!save || !file.exists()) {
			cu.applyTextEdit(edit, monitor);
		} else {
			if (monitor == null) {
				monitor = new NullProgressMonitor();
			}
			monitor.beginTask("Applying quick fix", 2);
			try {
				IStatus status = Resources.makeCommittable(file, null);
				if (!status.isOK()) {
					throw new CoreException(status);
				}

				cu.applyTextEdit(edit, new SubProgressMonitor(monitor, 1));

				cu.save(new SubProgressMonitor(monitor, 1), true);
			} finally {
				monitor.done();
			}
		}
	}

	private ListRewrite createAddMethodRewrite(ASTRewrite astRewrite) throws JavaModelException, CoreException {
		ListRewrite listRewriter = null;
		AbstractTypeDeclaration declaration = (AbstractTypeDeclaration) ASTNodes.getParent(
				NodeFinder.perform(compilationUnitASTRoot, type.getNameRange()), AbstractTypeDeclaration.class);
		if (declaration != null) {
			listRewriter = astRewrite.getListRewrite(declaration, declaration.getBodyDeclarationsProperty());
		}
		if (listRewriter == null) {
			Status status = new Status(IStatus.ERROR, JavaUI.ID_PLUGIN, IStatus.ERROR,
					"Could not find the selected type element", null);
			throw new CoreException(status);
		}
		return listRewriter;
	}

	private TextEdit createImportEdit(ICompilationUnit unit) throws JavaModelException, CoreException {
		ImportRewrite importRewrite = CodeStyleConfiguration.createImportRewrite(unit, true);
		importRewrite.addImport(TextSyntax.class.getName());
		TextEdit importEdit = importRewrite.rewriteImports(null);
		return importEdit;
	}
}
