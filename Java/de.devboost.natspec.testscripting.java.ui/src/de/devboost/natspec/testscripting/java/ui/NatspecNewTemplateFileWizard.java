package de.devboost.natspec.testscripting.java.ui;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWizard;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.natspec.resource.natspec.ui.NatspecUIPlugin;

public class NatspecNewTemplateFileWizard extends Wizard implements INewWizard {

	// TODO we should use JavaNatSpecConstants here
	private final static String NATSPEC_TEMPLATE_CLASS_NAME = "_NatSpecTemplate";
	public final static String NATSPEC_TEMPLATE_FILE_NAME = NATSPEC_TEMPLATE_CLASS_NAME + ".java";

	private static final String NATSPEC_TEMPLATE_FILE_CONTENTS = ""
			+ "import org.junit.After;\n"
			+ "import org.junit.Before;\n"
			+ "import org.junit.Test;\n"
			+ "\n"
			+ "// TODO Import TestSupport classes.\n"
			+ "\n"
			+ "// TODO Import classes to test and other test dependencies.\n"
			+ "\n"
			+ "public class " + NATSPEC_TEMPLATE_CLASS_NAME + " {\n"
			+ "\n"
			+ "\t// TODO Declare fields for test support classes.\n"
			+ "\t// TODO Declare fields for test context.\n" + "\n"
			+ "\t@Test\n"
			+ "\tpublic void executeScript() throws Exception {\n"
			+ "\t\t// generated code will be inserted here\n"
			+ "\t\t/* @MethodBody */\n" + "\t}\n" + "\n" + "\t@Before\n"
			+ "\tpublic void setUp() {\n"
			+ "\t\t// TODO Initialize fields for test support classes.\n"
			+ "\t\t// TODO Initialize fields for test context.\n" + "\t}\n"
			+ "\t\n" + "\t@After\n" + "\tpublic void shutdown() {\n"
			+ "\t\t// TODO Shutdown text context.\n" + "\t}\n" + "}";

	private String categoryId = null;
	private NatspecNewTemplateFileWizardPage page;
	private ISelection selection;
	private String newName = null;

	public NatspecNewTemplateFileWizard() {
		super();
		setNeedsProgressMonitor(true);
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String id) {
		categoryId = id;
	}

	/**
	 * Adds the pages to the wizard.
	 */
	public void addPages() {
		page = new NatspecNewTemplateFileWizardPage(selection, this);
		addPage(page);
	}

	/**
	 * This method is called when 'Finish' button is pressed in the wizard. We
	 * will create an operation and run it using the wizard as execution
	 * context.
	 */
	public boolean performFinish() {
		final String containerName = page.getContainerName();
		final String fileName = page.getFileName();
		this.newName = fileName;
		int seperatorIdx = newName.indexOf('.');
		if (seperatorIdx != -1) {
			newName = newName.substring(0, seperatorIdx);
		}
		final IFile file;
		try {
			file = getFile(fileName, containerName);
		} catch (CoreException e1) {
			NatspecUIPlugin.logError("Exception while initializing new file",
					e1);
			return false;
		}

		if (file.exists()) {
			// ask for confirmation
			MessageBox messageBox = new MessageBox(getShell(),
					SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			messageBox.setMessage("File \"" + fileName
					+ "\" already exists. Do you want to override it?");
			messageBox.setText("File exists");
			int response = messageBox.open();
			if (response == org.eclipse.swt.SWT.NO) {
				return true;
			}
		}

		IRunnableWithProgress op = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor)
					throws InvocationTargetException {
				try {
					doFinish(containerName, fileName, monitor);
				} catch (CoreException e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
		};
		try {
			getContainer().run(true, false, op);
		} catch (InterruptedException e) {
			return false;
		} catch (InvocationTargetException e) {
			Throwable realException = e.getTargetException();
			MessageDialog.openError(getShell(), "Error",
					realException.getMessage());
			NatspecUIPlugin
					.logError("Exception while initializing new file", e);
			return false;
		}
		return true;
	}

	/**
	 * The worker method. It will find the container, create the file if missing
	 * or just replace its contents, and open the editor on the newly created
	 * file.
	 */
	private void doFinish(String containerName, String fileName,
			IProgressMonitor monitor)
			throws CoreException {
		// create a sample file
		monitor.beginTask("Creating " + fileName, 2);
		final IFile file = getFile(fileName, containerName);
		try {
			String packageName = getPackageName(containerName);
			InputStream stream = openContentStream(packageName);
			if (file.exists()) {
				file.setContents(stream, true, true, monitor);
			} else {
				file.create(stream, true, monitor);
			}
			stream.close();
		} catch (IOException e) {
		}
		monitor.worked(1);
		monitor.setTaskName("Opening file for editing...");
		getShell().getDisplay().asyncExec(new Runnable() {
			public void run() {
				IWorkbenchPage page = PlatformUI
						.getWorkbench().getActiveWorkbenchWindow()
						.getActivePage();
				try {
					IDE.openEditor(page, file, true);
				} catch (PartInitException e) {
				}
			}
		});
		monitor.worked(1);
	}

	private String getPackageName(String containerName) {
		IPath path = new Path(containerName);
		path = path.append(NATSPEC_TEMPLATE_CLASS_NAME);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IFile file = root.getFile(path);
		if (file == null) {
			return null;
		}
		
		JDTUtility jdtUtility = new JDTUtility() {

			@Override
			protected void logWarning(String message, Exception e) {
				// TODO Redirect this to logWarning instead
				NatspecUIPlugin.logError(message, e);
			}
		};
		return jdtUtility.getPackageName(file);
	}

	private IFile getFile(String fileName,
			String containerName) throws CoreException {
		IWorkspaceRoot root = ResourcesPlugin
				.getWorkspace().getRoot();
		IResource resource = root
				.findMember(new Path(containerName));
		if (!resource.exists()
				|| !(resource instanceof IContainer)) {
			throwCoreException("Container \"" + containerName
					+ "\" does not exist.");
		}
		IContainer container = (IContainer) resource;
		final IFile file = container
				.getFile(new Path(fileName));
		return file;
	}

	/**
	 * We will initialize file contents with a sample text.
	 * 
	 * @param packageName the name of the package where the template will be located
	 */
	private InputStream openContentStream(String packageName) {
		String content = NATSPEC_TEMPLATE_FILE_CONTENTS;
		if (packageName != null) {
			content = "package " + packageName + ";\n\n" + content;
		}
		return new ByteArrayInputStream(content.getBytes(StandardCharsets.UTF_8));
	}

	private void throwCoreException(String message) throws CoreException {
		IStatus status = new Status(IStatus.ERROR, "NewFileContentPrinter",
				IStatus.OK, message, null);
		throw new CoreException(status);
	}

	/**
	 * We will accept the selection in the workbench to see if we can initialize
	 * from it.
	 * 
	 * @see IWorkbenchWizard#init(org.eclipse.ui.IWorkbench,
	 *      org.eclipse.jface.viewers.IStructuredSelection)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.selection = selection;
	}
}
