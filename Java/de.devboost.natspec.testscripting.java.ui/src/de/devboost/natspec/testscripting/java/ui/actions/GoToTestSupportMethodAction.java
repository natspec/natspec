package de.devboost.natspec.testscripting.java.ui.actions;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.patterns.ISyntaxPatternSourceProvider;
import de.devboost.natspec.testscripting.patterns.DynamicSyntaxPattern;
import de.devboost.natspec.testscripting.patterns.IParameter;

public class GoToTestSupportMethodAction {

	private final ISyntaxPatternSourceProvider pattern;

	public GoToTestSupportMethodAction(ISyntaxPatternSourceProvider pattern) {
		this.pattern = pattern;
	}

	public void execute() {
		String typeName = pattern.getQualifiedTypeName();
		String methodName = pattern.getMethodName();
		List<? extends IParameter> parameters = getParameters();
		
		execute(pattern, typeName, methodName, parameters);
	}

	private List<? extends IParameter> getParameters() {
		if (pattern instanceof DynamicSyntaxPattern) {
			DynamicSyntaxPattern<?> dynamicSyntaxPattern = (DynamicSyntaxPattern<?>) pattern;
			return dynamicSyntaxPattern.getParameters();
		}
		
		return Collections.emptyList();
	}

	private void execute(ISyntaxPatternSourceProvider pattern, String typeName, String methodName,
			List<? extends IParameter> parameters) {
		
		String projectName = pattern.getProjectName();
		if (projectName == null) {
			return;
		}

		EditorHelper.INSTANCE.openEditor(typeName, methodName, parameters, projectName);
	}
}
