package de.devboost.natspec.testscripting.java.ui.handlers;

import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.IDocumentProvider;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.SentenceUtil;
import de.devboost.natspec.matching.IPatternMatchContext;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.matching.MatchService;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.patterns.ISyntaxPatternSourceProvider;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.ui.NatspecEditor;
import de.devboost.natspec.testscripting.context.PatternMatchContextFactory;
import de.devboost.natspec.testscripting.java.ui.actions.GoToTestSupportMethodAction;

/**
 * The {@link NatspecGoToTestSupportMethodHandler} handles the F3 key (go to declaration) for NatSpec files. It
 * determines the test support method that backs the current sentence in the active editor and navigates to this method
 * (i.e., it opens an editor that show the respective test support class and selects the test support method).
 */
public class NatspecGoToTestSupportMethodHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IEditorPart activeEditorPart = HandlerUtil.getActiveEditor(event);
		NatspecEditor activeEditor = null;

		ITextOperationTarget operationTarget = null;
		IDocument document = null;
		if (activeEditorPart instanceof NatspecEditor) {
			activeEditor = (NatspecEditor) activeEditorPart;
			// This explicit cast is required to compile against older Eclipse versions
			operationTarget = (ITextOperationTarget) activeEditorPart.getAdapter(ITextOperationTarget.class);
			IDocumentProvider documentProvider = activeEditor.getDocumentProvider();
			IEditorInput editorInput = activeEditor.getEditorInput();
			document = documentProvider.getDocument(editorInput);
		}

		if (activeEditor == null || operationTarget == null || document == null) {
			return null;
		}

		ISelectionProvider selectionProvider = activeEditor.getSelectionProvider();
		ISelection currentSelection = selectionProvider.getSelection();
		if (currentSelection instanceof ITextSelection) {
			ITextSelection textSelection = (ITextSelection) currentSelection;
			int startLine = textSelection.getStartLine();
			INatspecTextResource resource = activeEditor.getResource();
			URI uri = resource.getURI();
			String text = document.get();

			findAndGoToTestSupportMethod(uri, startLine, text);
		}

		return null;
	}

	private void findAndGoToTestSupportMethod(URI uri, int startLine, String text) {
		PatternMatchContextFactory contextFactory = PatternMatchContextFactory.INSTANCE;
		IPatternMatchContext matchContext = contextFactory.createPatternMatchContext(uri);

		String[] lines = text.split("\r\n|\n|\n");

		boolean useOptimizedMatcher = NatSpecPlugin.USE_TREE_BASED_MATCHER;
		boolean usePrioritizer = NatSpecPlugin.USE_PRIORITIZER;
		MatchService matchService = new MatchService(useOptimizedMatcher, usePrioritizer);

		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];
			Sentence sentence = SentenceUtil.INSTANCE.createSentence(line);
			if (line.trim().isEmpty()) {
				continue;
			}

			// ignore comments
			if (SentenceUtil.INSTANCE.isComment(sentence)) {
				continue;
			}

			// We need to match all lines to ensure that all created objects are put into the match context
			List<ISyntaxPatternMatch<?>> matches = matchService.match(sentence, matchContext);
			// However, we do only process the matches for the current line, because we want to navigate to the test
			// support method that backs the sentence in this line.
			if (i != startLine) {
				continue;
			}

			for (ISyntaxPatternMatch<?> match : matches) {
				ISyntaxPattern<?> pattern = match.getPattern();
				if (pattern instanceof ISyntaxPatternSourceProvider) {
					ISyntaxPatternSourceProvider syntaxPatternSourceProvider = (ISyntaxPatternSourceProvider) pattern;
					GoToTestSupportMethodAction action = new GoToTestSupportMethodAction(syntaxPatternSourceProvider);
					action.execute();
					return;
				}
			}
		}
	}
}
