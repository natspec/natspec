package de.devboost.natspec.testscripting.java.ui.quickfixes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.quickassist.IQuickAssistInvocationContext;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.TextInvocationContext;

import de.devboost.natspec.NatSpecPlugin;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.resource.natspec.INatspecLocationMap;
import de.devboost.natspec.resource.natspec.INatspecQuickFix;
import de.devboost.natspec.resource.natspec.INatspecResourceProvider;
import de.devboost.natspec.resource.natspec.INatspecTextResource;
import de.devboost.natspec.resource.natspec.ui.ErrorMessageHelper;
import de.devboost.natspec.resource.natspec.ui.IExtendedNatspecQuickAssistProcessor;
import de.devboost.natspec.resource.natspec.ui.INatspecAnnotationModelProvider;
import de.devboost.natspec.resource.natspec.util.NatspecEclipseProxy;
import de.devboost.natspec.testscripting.IFieldCache;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.java.JavaTemplateFileFinder;
import de.devboost.natspec.testscripting.java.jdt.ConcreteJDTUtility;
import de.devboost.natspec.testscripting.java.jdt.JDTJavaConnectorPlugin;
import de.devboost.natspec.testscripting.java.jdt.JavaDynamicSyntaxPatternProvider;

public class TestSupportQuickAssistProcessor implements IExtendedNatspecQuickAssistProcessor {

	private static final NatspecEclipseProxy NATSPEC_ECLIPSE_PROXY = new NatspecEclipseProxy();

	private static final String FEATURE_NAME = "'Quickfix'";
	
	private INatspecResourceProvider resourceProvider;
	private INatspecAnnotationModelProvider annotationModelProvider;

	@Override
	public void setResourceProvider(INatspecResourceProvider resourceProvider) {
		this.resourceProvider = resourceProvider;
	}

	@Override
	public void setAnnotationModelProvider(INatspecAnnotationModelProvider annotationModelProvider) {
		this.annotationModelProvider = annotationModelProvider;
	}

	@Override
	public boolean canAssist(IQuickAssistInvocationContext invocationContext) {
		return false;
	}

	@Override
	public boolean canFix(Annotation annotation) {
		return false;
	}

	@Override
	public ICompletionProposal[] computeQuickAssistProposals(IQuickAssistInvocationContext invocationContext) {
		
		if (!NatSpecPlugin.hasValidLicense()) {
			// no license, no quick fixes
			ErrorMessageHelper errorMessageHelper = new ErrorMessageHelper();
			errorMessageHelper.showErrorMessage(FEATURE_NAME);
			return new ICompletionProposal[0];
		}

		ISourceViewer sourceViewer = invocationContext.getSourceViewer();
		int offset = -1;
		int length = 0;
		if (invocationContext instanceof TextInvocationContext) {
			TextInvocationContext textContext = (TextInvocationContext) invocationContext;
			offset = textContext.getOffset();
			length = textContext.getLength();
		}
		
		int documentLength = sourceViewer.getDocument().getLength();
		List<INatspecQuickFix> quickFixes = getQuickFixes(sourceViewer, offset, length, documentLength);
		
		int numberOfQuickFixes = quickFixes.size();
		if (numberOfQuickFixes == 0) {
			return null;
		}
		
		ICompletionProposal[] proposals = new ICompletionProposal[quickFixes.size()];
		for (int i = 0; i < proposals.length; i++) {
			INatspecQuickFix quickFix = quickFixes.get(i);
			proposals[i] = createCompletionProposal(sourceViewer, quickFix);
		}
		return proposals;
	}

	private ICompletionProposal createCompletionProposal(ISourceViewer sourceViewer, INatspecQuickFix quickFix) {
		return new TestSupportQuickFixProposal(quickFix, sourceViewer);
	}

	private List<INatspecQuickFix> getQuickFixes(ISourceViewer sourceViewer, int offset, int length, int documentLength) {
		
		if (length < 0) {
			length = 0;
		}
		if (offset < 0) {
			offset = 0;
		}
		
		List<INatspecQuickFix> foundFixes = new ArrayList<INatspecQuickFix>();
		INatspecTextResource resource = resourceProvider.getResource();
		IAnnotationModel model = annotationModelProvider.getAnnotationModel();

		if (model == null) {
			return foundFixes;
		}

		Set<Sentence> sentencesToFix = new LinkedHashSet<Sentence>();
		Iterator<?> iterator = model.getAnnotationIterator();
		while (iterator.hasNext()) {
			Object next = iterator.next();
			if (!(next instanceof Annotation)) {
				continue;
			}
			
			Annotation annotation = (Annotation) next;
			Position position = model.getPosition(annotation);
			if (offset >= 0) {
				boolean isAtEndOfDocument = position.getOffset() + position.getLength() == documentLength;
				if (!position.overlapsWith(offset, length) && !isAtEndOfDocument) {
					continue;
				}
			}

			INatspecLocationMap locationMap = resource.getLocationMap();
			List<EObject> objects = locationMap.getElementsAt(offset);
			// We do also consider objects at position offset - 1 to make sure
			// that we also get quick fixes when the caret it at the end of a
			// sentence.
			objects.addAll(locationMap.getElementsAt(offset - 1));
			for (EObject eObject : objects) {
				if (eObject instanceof Sentence) {
					Sentence sentence = (Sentence) eObject;
					sentencesToFix.add(sentence);
				}
			}
		}

		JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();

		URI uri = resource.getURI();
		IFile natSpecFile = NATSPEC_ECLIPSE_PROXY.getFileForURI(uri);

		JavaDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider = plugin.getDynamicSyntaxPatternProvider();
		IFieldCache fieldTypeCache = dynamicSyntaxPatternProvider.getFieldTypeCache();
		IFile templateFile = JavaTemplateFileFinder.INSTANCE.findTemplateFile(natSpecFile);
		Collection<String> allFieldTypes = fieldTypeCache.getAllFieldTypes(templateFile);
		IJavaProject javaProject = ConcreteJDTUtility.INSTANCE.getJavaProject(natSpecFile);
		foundFixes.addAll(getQuickFixes(allFieldTypes, sentencesToFix, javaProject, uri));
		Collections.sort(foundFixes, QuickFixComparator.INSTANCE);
		return foundFixes;
	}

	/**
	 * Generates appropriate quick fixes.
	 * 
	 * @param foundFixes
	 *            a list to which the found quick fixes are added.
	 * @param allFieldTypes
	 *            the types of all fields defined in the NatSpec template
	 * @param sentencesToFix
	 *            the sentences that shall fixed
	 * @param javaProject
	 *            the project containing the NatSpec file
	 * @param uri
	 *            the URI of the NatSpec file
	 */
	// This method is intentionally protected to access it in test cases
	protected List<INatspecQuickFix> getQuickFixes(Collection<String> allFieldTypes, Set<Sentence> sentencesToFix,
			IJavaProject javaProject, URI uri) {

		List<INatspecQuickFix> foundFixes = new ArrayList<INatspecQuickFix>();
		// For each field that is defined in the NatSpec template we propose to add a test support method to the type of
		// the field (unless the type is binary and cannot be changed therefore).
		Set<String> uniqueFieldTypes = new LinkedHashSet<String>(allFieldTypes);
		for (String typename : uniqueFieldTypes) {
			typename = NameHelper.INSTANCE.getNameWithoutTypeArguments(typename);
			IType type;
			try {
				type = javaProject.findType(typename);
			} catch (JavaModelException e) {
				continue;
			}
			
			if (type == null) {
				// TODO Handle this correctly
				System.out.println("Can't find type " + typename);
				continue;
			}
			
			if (type.isBinary()) {
				continue;
			}

			String typeElementName = type.getElementName();
			IPath typePath = type.getPath();
			String path = typePath.toString();
			for (Sentence sentence : sentencesToFix) {
				String displayString = "Add test support method to class " + typeElementName;
				// TODO add support for inner classes?!?
				FixSentenceQuickFix fix = new FixSentenceQuickFix(sentence, uri, path, displayString);
				foundFixes.add(fix);
			}
		}
		return foundFixes;
	}

	@Override
	public String getErrorMessage() {
		return null;
	}
}
