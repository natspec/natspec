package de.devboost.natspec.testscripting.java.ui.hover;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.ui.JavadocContentAccess;

import com.google.common.io.CharStreams;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.natspec.Sentence;
import de.devboost.natspec.matching.ISyntaxPatternMatch;
import de.devboost.natspec.resource.natspec.INatspecHoverTextProvider;
import de.devboost.natspec.resource.natspec.ui.AbstractHoverTextProvider;
import de.devboost.natspec.testscripting.java.JavaCodeGenerator;
import de.devboost.natspec.testscripting.java.ui.Activator;

public class JavaHoverTextProvider extends AbstractHoverTextProvider implements INatspecHoverTextProvider {

	private final static JDTUtility JDT_UTILITY = new JDTUtility() {

		@Override
		protected void logWarning(String message, Exception e) {
			Activator.logWarning(message, e);
		}
	};

	@Override
	protected String getHoverText(EObject object, List<ISyntaxPatternMatch<? extends Object>> matches,
			String projectName, Sentence sentence) {

		if (sentence != object) {
			return null;
		}

		for (ISyntaxPatternMatch<? extends Object> match : matches) {
			Object userData = match.getUserData();
			if (userData instanceof JavaCodeGenerator) {
				JavaCodeGenerator javaCodeGenerator = (JavaCodeGenerator) userData;
				return getHoverText(projectName, javaCodeGenerator);
			}
		}

		return null;
	}

	private String getHoverText(String projectName, JavaCodeGenerator javaCodeGenerator) {
		String qualifiedTypeName = javaCodeGenerator.getQualifiedTypeName();
		// Find type
		IType type = JDT_UTILITY.getType(projectName, qualifiedTypeName);
		if (type == null) {
			return "";
		}

		String methodName = javaCodeGenerator.getMethodName();
		String[] parameterTypes = javaCodeGenerator.getParameterTypes();

		// Find method
		try {
			IMethod method = getMethod(type, methodName, parameterTypes);
			if (method == null) {
				return "";
			}

			String source = method.getSource();
			source = JavaSourceCodeToHtmlConverter.INSTANCE.toHtml(source);

			String javaDoc = getJavadoc(method);
			if (javaDoc == null) {
				return source + "";
			}

			return javaDoc + source;
		} catch (JavaModelException jme) {
			return "";
		} catch (IOException ioe) {
			return "";
		}
	}

	private IMethod getMethod(IType type, String methodName, String[] parameterTypes) throws JavaModelException {
		IMethod[] methods = type.getMethods();
		for (IMethod method : methods) {
			if (!methodName.equals(method.getElementName())) {
				continue;
			}
			
			if (!hasSameParameterTypes(parameterTypes, method)) {
				continue;
			}
			
			return method;
		}
		
		return null;
	}

	private boolean hasSameParameterTypes(String[] parameterTypes, IMethod method) {
		String[] parameterSignatures = method.getParameterTypes();
		if (parameterSignatures.length != parameterTypes.length) {
			return false;
		}
		
		for (int i = 0; i < parameterSignatures.length; i++) {
			String parameterSignature = parameterSignatures[i];
			String nextParameter = Signature.getSimpleName(Signature.toString(Signature.getTypeErasure(parameterSignature)));
			if (!nextParameter.equals(Signature.getSimpleName(parameterTypes[i]))) {
				return false;
			}
		}
		
		return true;
	}

	private String getJavadoc(IMethod method) throws JavaModelException, IOException {
		boolean allowInherited = true;
		boolean useAttachedJavadoc = true;
		Reader htmlContentReader = JavadocContentAccess.getHTMLContentReader(method, allowInherited,
				useAttachedJavadoc);
		if (htmlContentReader == null) {
			return null;
		}

		return CharStreams.toString(htmlContentReader);
	}
}
