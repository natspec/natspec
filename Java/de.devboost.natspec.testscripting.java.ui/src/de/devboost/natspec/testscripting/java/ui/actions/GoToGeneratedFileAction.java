package de.devboost.natspec.testscripting.java.ui.actions;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;

import de.devboost.eclipse.jdtutilities.ui.JavaEditorHelper;

public class GoToGeneratedFileAction {

	private final IFile generatedFile;
	private final int lineNumber;

	public GoToGeneratedFileAction(IFile generatedFile, int lineNumber) {
		this.generatedFile = generatedFile;
		this.lineNumber = lineNumber;
	}

	public void execute() {
		IJavaElement element = JavaCore.create(generatedFile);
		
		if (element instanceof ICompilationUnit) {
			ICompilationUnit comilationUnit = (ICompilationUnit) element;
			JavaEditorHelper.INSTANCE.switchToJavaEditor(comilationUnit, lineNumber);
		}
	}
}
