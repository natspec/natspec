package de.devboost.natspec.testscripting.java.ui.quickfixes;

import java.util.Collection;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import de.devboost.natspec.Sentence;
import de.devboost.natspec.resource.natspec.INatspecQuickFix;
import de.devboost.natspec.testscripting.ui.Activator;

public class FixSentenceQuickFix implements INatspecQuickFix {

	private final Sentence sentence;
	private final URI uri;
	private final String pathToProviderClass;
	private final String displayString;

	public FixSentenceQuickFix(Sentence sentence, URI uri, String pathToProviderClass, String displayString) {
		this.sentence = sentence;
		this.uri = uri;
		this.pathToProviderClass = pathToProviderClass;
		this.displayString = displayString;
	}

	@Override
	public String getDisplayString() {
		return displayString;
	}

	@Override
	public String getImageKey() {
		return "platform:/plugin/" + Activator.PLUGIN_ID + "/icons/addtestsupportmethod.gif";
	}

	@Override
	public String apply(String currentText) {
		AddTestSupportMethodExecutor executor = new AddTestSupportMethodExecutor();
		executor.execute(sentence, uri, pathToProviderClass);

		// The text of the NatSpec file stays the same. Only Java classes are
		// modified by this quick fix.
		return currentText;
	}

	@Override
	public Collection<EObject> getContextObjects() {
		return null;
	}

	@Override
	public String getContextAsString() {
		return null;
	}
}
