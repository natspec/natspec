package de.devboost.natspec.junit.jdt.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.Command;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.State;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IAdapterManager;
import org.eclipse.core.runtime.Platform;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.junit.launcher.JUnitLaunchShortcut;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.commands.ICommandService;

import com.google.common.io.Files;

import de.devboost.natspec.jdt.ui.ILaunchDelegate;
import de.devboost.natspec.jdt.ui.LaunchShortcutDelegator;

public class RunNatspecAsJUnitLaunchShortcut extends JUnitLaunchShortcut implements ILaunchDelegate {

	private static final String DATABASE_TESTMODE_COMMAND_ID = "de.devboost.eclipse.databaseenv.commands.sampleCommand";
	private static final String COMMAND_TOGGLE_STATE_ID = "org.eclipse.ui.commands.toggleState";
	private static final String LAUNCH_CONFIG_ENV_COMMAND_ID = "de.devboost.eclipse.databaseenv.commands.singleLaunchConfigEnvironments";
	private static final String LAUNCH_CONFIG_PARAM_ID = "de.devboost.eclipse.databaseenv.commandParameter.launchConfig";

	@Override
	public void launch(IEditorPart editor, String mode) {
		new LaunchShortcutDelegator(this).launch(editor, mode);
	}

	@Override
	public void launch(ISelection selection, String mode) {
		new LaunchShortcutDelegator(this).launch(selection, mode);
	}

	@Override
	public void launchSelection(ISelection selection, String mode) {
		// check if we already have a launch configuration
		ILaunchConfiguration[] launchConfigurations = super.getLaunchConfigurations(selection);
		if (launchConfigurations == null || launchConfigurations.length == 0) {
			boolean testModeActivated = isDatabaseTestModeActivated();
			if (testModeActivated) {
				IJavaElement javaElement = getJavaElementFromSelection(selection);
				if (javaElement != null && javaElement instanceof ICompilationUnit) {
					ICompilationUnit compilationUnit = (ICompilationUnit) javaElement;
					String elementName = Files.getNameWithoutExtension(javaElement.getElementName());
					IType type = compilationUnit.getType(elementName);
					try {
						// 1. create a launch config but we don't need it
						ILaunchConfigurationWorkingCopy launchConfiguration = super.createLaunchConfiguration(type);
						// 2. activate run on database
						Command launchConfigEnvCommand = getCommand(LAUNCH_CONFIG_ENV_COMMAND_ID);
						if (launchConfigEnvCommand != null) {
							Map<String, Object> parameters = new HashMap<String, Object>();
							parameters.put(LAUNCH_CONFIG_PARAM_ID, launchConfiguration);
							ExecutionEvent executionEvent = new ExecutionEvent(null, parameters, null, null);
							launchConfigEnvCommand.executeWithChecks(executionEvent);
						}
					} catch (CoreException e) {
						// just do nothing
					} catch (ExecutionException e) {
						// just do nothing
					} catch (NotDefinedException e) {
						// just do nothing
					} catch (NotEnabledException e) {
						// just do nothing
					} catch (NotHandledException e) {
						// just do nothing
					}
				}
			}
		}

		super.launch(selection, mode);
	}

	private boolean isDatabaseTestModeActivated() {
		Command command = getCommand(DATABASE_TESTMODE_COMMAND_ID);
		if (command == null) {
			return false;
		}
		State toggleState = command.getState(COMMAND_TOGGLE_STATE_ID);
		if (toggleState == null) {
			return false;
		}
		Object value = toggleState.getValue();
		if (!(value instanceof Boolean)) {
			return false;
		}
		return (Boolean) value;
	}

	private Command getCommand(String commandId) {
		ICommandService service = (ICommandService) PlatformUI.getWorkbench().getService(ICommandService.class);
		Command command = service.getCommand(commandId);
		return command;
	}

	private IJavaElement getJavaElementFromAdaptable(Object input) {
		IAdapterManager adapterManager = Platform.getAdapterManager();
		IJavaElement javaElement = (IJavaElement) adapterManager.getAdapter(input, IJavaElement.class);
		if (javaElement != null) {
			return javaElement;
		}
		if (input instanceof IAdaptable) {
			IAdaptable adaptable = (IAdaptable) input;
			return (IJavaElement) adaptable.getAdapter(IJavaElement.class);
		}
		return null;
	}

	private IJavaElement getJavaElementFromSelection(ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			Object obj = ssel.getFirstElement();
			IJavaElement javaElement = getJavaElementFromAdaptable(obj);
			return javaElement;
		}
		return null;
	}
}
