package de.devboost.natspec.junit.jdt.ui;

import de.devboost.natspec.jdt.ui.StringContainmentPropertyTester;

/**
 * The {@link JUnitPropertyTester} is used by Eclipse to determine whether the
 * {@link RunNatspecAsJUnitLaunchShortcut} is enabled or not. To do so, it checks
 * whether the NatSpec template contains the text <code>@Test</code>. This is
 * not exact, but running the JDT parser to figure out whether the template
 * defines a test seems to be overkill.
 */
public class JUnitPropertyTester extends StringContainmentPropertyTester {

	@Override
	protected String getSearchString() {
		return "@Test";
	}
}
