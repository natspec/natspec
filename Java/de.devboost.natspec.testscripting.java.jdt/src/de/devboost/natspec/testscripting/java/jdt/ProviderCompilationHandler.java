package de.devboost.natspec.testscripting.java.jdt;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;

import de.devboost.eclipse.jdtutilities.ClassDependencyUtility;
import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.testscripting.TestConnectorPlugin;

/**
 * The {@link ProviderCompilationHandler} is responsible to detect syntax/synonym pattern provider classes (i.e.,
 * classes implementing either {@link ISyntaxPatternProvider} or {@link ISynonymProvider} or both). It does also take
 * appropriate actions if classes that providers depend on are changed.
 */
public class ProviderCompilationHandler extends AbstractAnnotationExtractor {
	
	private final static ClassDependencyUtility CLASS_DEPENDENCY_UTILITY = new ClassDependencyUtility() {

		@Override
		protected void logWarning(String message, Exception e) {
			JDTJavaConnectorPlugin.logWarning(message, e);
		}
	};

	private Map<String, Set<String>> providerPathToDependenciesMap = new LinkedHashMap<String, Set<String>>();

	@Override
	protected Collection<IFile> processUnits(List<ICompilationUnit> compilationUnits, IProgressMonitor monitor) {

		// First, find providers in list of compiled classes
		Set<IFile> foundProviders = findProviders(compilationUnits, monitor);
		if (monitor.isCanceled()) {
			return foundProviders;
		}

		// Second, check whether classes were compiled that a provider depends on
		Set<IFile> affectedProviders = findAffectedProviders(compilationUnits, foundProviders, monitor);
		if (monitor.isCanceled()) {
			return foundProviders;
		}

		// Reload affected providers
		callProviderAnnotationExtractor(affectedProviders, monitor);

		return foundProviders;
	}

	private Set<IFile> findAffectedProviders(List<ICompilationUnit> compilationUnits, Set<IFile> foundProviders,
			IProgressMonitor monitor) {
		
		Set<IFile> affectedProviders = new LinkedHashSet<IFile>();
		for (ICompilationUnit compilationUnit : compilationUnits) {
			if (monitor.isCanceled()) {
				return affectedProviders;
			}
			
			IResource resource;
			try {
				resource = compilationUnit.getCorrespondingResource();
			} catch (JavaModelException e) {
				TestConnectorPlugin.logError("Exception while processing Java class.", e);
				continue;
			}
			
			if (resource instanceof IFile) {
				IFile file = (IFile) resource;
				IPath fullPath = file.getFullPath();
				String path = fullPath.toString();

				IWorkspace workspace = file.getWorkspace();
				IWorkspaceRoot workspaceRoot = workspace.getRoot();

				Set<String> providers = getAffectedProviders(path);
				for (String providerPath : providers) {
					if (monitor.isCanceled()) {
						return affectedProviders;
					}

					IFile providerFile = workspaceRoot.getFile(new Path(providerPath));
					// Skip providers that were already reloaded, because they were also compiled.
					if (!foundProviders.contains(providerFile)) {
						affectedProviders.add(providerFile);
					}
				}
			}
		}
		
		return affectedProviders;
	}

	private Set<IFile> findProviders(List<ICompilationUnit> compilationUnits, IProgressMonitor monitor) {
		Set<IFile> foundProviders = new LinkedHashSet<IFile>();
		for (ICompilationUnit compilationUnit : compilationUnits) {
			if (monitor.isCanceled()) {
				return foundProviders;
			}
			
			IResource resource = ConcreteJDTUtility.INSTANCE.getCorrespondingResource(compilationUnit);
			if (resource == null) {
				continue;
			}
			
			if (resource instanceof IFile) {
				IFile file = (IFile) resource;
				processFile(foundProviders, file, monitor);
			}
		}
		
		return foundProviders;
	}

	private void processFile(Set<IFile> foundProviders, IFile file, IProgressMonitor monitor) {
		IPath fullPath = file.getFullPath();
		String path = fullPath.toString();

		boolean foundProvider = callProviderAnnotationExtractor(Collections.singletonList(file), monitor);
		if (!foundProvider) {
			return;
		}
		
		foundProviders.add(file);
		
		// Determine classes that are referenced by the provider
		try {
			Set<String> dependencies = CLASS_DEPENDENCY_UTILITY.findReferencesFromTransitively(path);
			providerPathToDependenciesMap.put(path, dependencies);
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while determining dependencies.", e);
		}
	}

	private boolean callProviderAnnotationExtractor(Collection<IFile> files, IProgressMonitor monitor) {
		JDTProviderAnnotationExtractor extractor = new JDTProviderAnnotationExtractor();
		Collection<IFile> providerFiles = extractor.process(files, monitor);
		return !providerFiles.isEmpty();
	}

	private Set<String> getAffectedProviders(String path) {
		Set<String> affectedProviders = new LinkedHashSet<String>();
		for (Entry<String, Set<String>> entry : providerPathToDependenciesMap.entrySet()) {
			String providerPath = entry.getKey();
			Set<String> dependencies = entry.getValue();
			if (dependencies.contains(path)) {
				affectedProviders.add(providerPath);
			}
		}
		
		return affectedProviders;
	}
}
