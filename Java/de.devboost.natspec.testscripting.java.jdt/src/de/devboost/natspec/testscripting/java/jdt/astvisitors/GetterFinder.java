package de.devboost.natspec.testscripting.java.jdt.astvisitors;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;

/**
 * A {@link GetterFinder} can be used to find all getters in an AST provided by
 * the JDT.
 */
public class GetterFinder extends AbstractMethodFinder {

	@Override
	public boolean visit(MethodDeclaration methodDeclaration) {
		if (methodDeclaration.isConstructor()) {
			return false;
		}
		
		// check name
		SimpleName methodName = methodDeclaration.getName();
		String methodIdentifier = methodName.getIdentifier();
		if (!methodIdentifier.startsWith("get")) {
			return false;
		}
		
		// check number of arguments (must be zero)
		if (methodDeclaration.parameters().size() > 0) {
			return false;
		}
		
		add(methodDeclaration);
		return false;
	}
}
