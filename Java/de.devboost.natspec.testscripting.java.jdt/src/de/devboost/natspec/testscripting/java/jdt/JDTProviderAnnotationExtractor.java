package de.devboost.natspec.testscripting.java.jdt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import de.devboost.natspec.ISynonymProvider;
import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.registries.IRegistry;
import de.devboost.natspec.registries.SynonymProviderRegistry;
import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.testscripting.TestConnectorPlugin;

/**
 * The ProviderAnnotationExtractor searches for classes that implement either the {@link ISyntaxPatternProvider} of the
 * {@link ISynonymProvider} interface. If such a class is found, the bundle containing the class is reloaded. Also an
 * instance of the class (i.e., the pattern provider) is registered. By doing so, the patterns provided by provider
 * classes are made available to NatSpec. If the provider is changed, the patterns are replaced.
 * <p>
 * Note that one must use a single class that implements both interfaces (i.e., {@link ISyntaxPatternProvider} and
 * {@link ISynonymProvider}) if one wants to provide synonyms and patterns. Having two separate classes, each
 * implementing one of the interfaces, does (currently) not work.
 */
public class JDTProviderAnnotationExtractor extends AbstractAnnotationExtractor {

	private static final String SYNONYM_PROVIDER_INTERFACE_NAME = ISynonymProvider.class.getSimpleName();
	private static final String SYNTAX_PATTERN_PROVIDER_INTERFACE_NAME = ISyntaxPatternProvider.class.getSimpleName();

	@Override
	protected List<IFile> processUnits(List<ICompilationUnit> compilationUnits, IProgressMonitor monitor) {
		
		List<IFile> providerFiles = new ArrayList<IFile>();
		Map<String, String> providerNameToBundleMap = new LinkedHashMap<String, String>();
		for (ICompilationUnit compilationUnit : compilationUnits) {
			getProviderInformation(compilationUnit, providerFiles, providerNameToBundleMap);
		}
		
		Set<String> bundleNames = new LinkedHashSet<String>(providerNameToBundleMap.values());
		if (bundleNames.isEmpty()) {
			return Collections.emptyList();
		}

		Map<String, Bundle> bundleNameToBundleMap = new LinkedHashMap<String, Bundle>();
		for (String bundleName : bundleNames) {
			if (monitor.isCanceled()) {
				return providerFiles;
			}
			
			Bundle bundle = reloadBundle(bundleName);
			bundleNameToBundleMap.put(bundleName, bundle);
		}
		
		for (String providerName : providerNameToBundleMap.keySet()) {
			if (monitor.isCanceled()) {
				return providerFiles;
			}
			
			String bundleName = providerNameToBundleMap.get(providerName);
			Bundle bundle = bundleNameToBundleMap.get(bundleName);
			if (bundle == null) {
				continue;
			}
			
			registerProvider(bundle, providerName);
		}
		
		return providerFiles;
	}

	private void getProviderInformation(ICompilationUnit compilationUnit, List<IFile> providerFiles,
			Map<String, String> providerNameToBundleMap) {

		IResource resource = ConcreteJDTUtility.INSTANCE.getCorrespondingResource(compilationUnit);
		if (resource == null) {
			return;
		}
		
		String providerName = null;
		try {
			providerName = getProviderName(compilationUnit);
		} catch (JavaModelException e) {
			TestConnectorPlugin.logError("Exception while processing Java class.", e);
		}
		
		if (providerName == null) {
			return;
		}
		
		if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			IProject project = file.getProject();
			String projectName = project.getName();
			providerFiles.add(file);
			if (projectName != null) {
				providerNameToBundleMap.put(providerName, projectName);
			}
		}
	}

	private boolean registerProvider(Bundle bundle, String providerClass) {
		boolean foundProvider = false;
		try {
			Class<?> loadedProviderClass = bundle.loadClass(providerClass);
			Object newInstance = loadedProviderClass.newInstance();
			if (newInstance instanceof ISyntaxPatternProvider) {
				ISyntaxPatternProvider provider = (ISyntaxPatternProvider) newInstance;
				removeAndRegister(SyntaxPatternRegistry.REGISTRY, provider);
				foundProvider = true;
			}
			if (newInstance instanceof ISynonymProvider) {
				ISynonymProvider provider = (ISynonymProvider) newInstance;
				removeAndRegister(SynonymProviderRegistry.REGISTRY, provider);
				foundProvider = true;
			}
		} catch (Throwable e) {
			TestConnectorPlugin.logError("Exception while loading provider class.", e);
		}
		return foundProvider;
	}

	private <T> void removeAndRegister(IRegistry<T> registry, T provider) {
		removeOldProvidersWithSameType(provider, registry);
		registry.add(provider);
	}

	private <T> void removeOldProvidersWithSameType(T provider, IRegistry<T> registry) {
		Class<?> providerClass = provider.getClass();
		String providerClassName = providerClass.getName();

		List<T> providers = registry.getRegistryEntries();
		List<T> providersToRemove = new ArrayList<T>();
		for (T nextProvider : providers) {
			Class<?> nextProviderClass = nextProvider.getClass();
			if (nextProviderClass.getName().equals(providerClassName)) {
				// Remove old providers of same type
				providersToRemove.add(nextProvider);
			}
		}
		
		for (T providerToRemove : providersToRemove) {
			registry.remove(providerToRemove);
		}
	}

	/**
	 * Returns the name of the provider contained in the compilation unit or <code>null</code> if the unit does not
	 * contain a provider.
	 */
	private String getProviderName(ICompilationUnit compilationUnit) throws JavaModelException {
		
		IType[] types = getTypesSafe(compilationUnit);
		for (IType type : types) {
			String[] superInterfaceNames = type.getSuperInterfaceNames();
			for (String superInterface : superInterfaceNames) {
				if (SYNTAX_PATTERN_PROVIDER_INTERFACE_NAME.equals(superInterface)) {
					// found pattern provider class
					return type.getFullyQualifiedName();
				}
				
				if (SYNONYM_PROVIDER_INTERFACE_NAME.equals(superInterface)) {
					// found synonym provider class
					return type.getFullyQualifiedName();
				}
			}
		}
		
		return null;
	}

	private Bundle reloadBundle(String projectName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(projectName);
		
		if (!project.exists()) {
			return null;
		}
		
		boolean hasErrors = true;
		try {
			hasErrors = hasErrors(project);
		} catch (CoreException ce) {
			TestConnectorPlugin.logError("Can't determine whether project " + projectName + " has errors.", ce);
		}
		
		if (hasErrors) {
			TestConnectorPlugin.logError("Project " + projectName + " has errors. Can't load dynamic syntax patterns.");
			return null;
		}
		
		String[] dependencies = getDependencies(project);

		// first remove the bundle itself
		modifyBundle(projectName, false);
		// then reload dependencies
		for (String dependency : dependencies) {
			reloadBundle(dependency);
		}
		// install bundle itself again
		Bundle reloadedBundle = modifyBundle(projectName, true);

		try {
			if (reloadedBundle != null) {
				reloadedBundle.start();
			}
		} catch (BundleException e) {
			TestConnectorPlugin.logError(
					"Exception while starting plug-in " + projectName + " containing a pattern provider class.", e);
		}
		
		return reloadedBundle;
	}

	/**
	 * Returns whether the given project contains any errors.
	 * 
	 * @param project
	 *            the project to search for markers
	 * @return <code>true</code> if the given project contains any errors, otherwise <code>false</code>
	 * 
	 * @throws CoreException
	 *             if an error occurs while searching for problem markers
	 */
	private boolean hasErrors(IProject project) throws CoreException {
		
		int depth = IResource.DEPTH_INFINITE;
		boolean includeSubtypes = true;
		String type = IMarker.PROBLEM;
		
		IMarker[] markers = project.findMarkers(type, includeSubtypes, depth);
		if (markers.length > 0) {
			for (int i = 0; i < markers.length; i++) {
				if (isError(markers[i])) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Returns whether the given marker has severity {@link IMarker#SEVERITY_ERROR}.
	 * 
	 * @param marker
	 *            the marker to examine
	 * @return <code>true</code> if the given problem has severity {@link IMarker#SEVERITY_ERROR}, otherwise
	 *         <code>false</code>
	 * @throws CoreException
	 *             if any exceptions occur while accessing marker attributes
	 */
	private boolean isError(IMarker marker) throws CoreException {
		Object attributeValue = marker.getAttribute(IMarker.SEVERITY);
		if (attributeValue == null) {
			return false;
		}

		if (attributeValue instanceof Integer) {
			Integer severity = (Integer) attributeValue;
			return severity.intValue() >= IMarker.SEVERITY_ERROR;
		}
		
		return false;
	}

	// FIXME 2.3 What the heck is this? Can't we ask the project for its plug-in dependencies?
	private String[] getDependencies(IProject project) {
		IFile manifestFile = project.getFile(new Path("META-INF/MANIFEST.MF"));
		if (manifestFile == null) {
			return new String[0];
		}
		if (!manifestFile.exists()) {
			return new String[0];
		}

		try {
			Manifest manifest = new Manifest(manifestFile.getContents());
			Attributes mainAttributes = manifest.getMainAttributes();
			String value = mainAttributes.getValue("Require-Bundle");
			if (value != null) {
				String[] requiredBundles = value.split(",");
				return requiredBundles;
			}
		} catch (IOException e) {
			TestConnectorPlugin.logError("Exception while determining plug-in dependencies.", e);
		} catch (CoreException e) {
			TestConnectorPlugin.logError("Exception while determining plug-in dependencies.", e);
		}
		
		return new String[0];
	}

	private Bundle modifyBundle(String projectName, boolean install) {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IProject project = root.getProject(projectName);
		if (project == null) {
			return null;
		}
		
		IPath location = project.getLocation();
		if (location == null) {
			return null;
		}
		
		File projectFile = location.toFile();
		String projectURI = projectFile.toURI().toString();
		try {
			TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
			Bundle hostBundle = plugin.getBundle();
			BundleContext hostBundleContext = hostBundle.getBundleContext();
			if (install) {
				Bundle bundle = hostBundleContext.installBundle(projectURI);
				TestConnectorPlugin.logInfo("Installed bundle " + bundle, null);
				bundle.start();
				return bundle;
			} else {
				Bundle existingBundle = hostBundleContext.getBundle(projectURI);
				if (existingBundle != null) {
					existingBundle.uninstall();
					TestConnectorPlugin.logInfo("Uninstalled bundle " + existingBundle, null);
				}
			}
		} catch (BundleException e) {
			String message = "Exception while " + (install ? "" : "un") + "installing plug-in.";
			TestConnectorPlugin.logError(message, e);
		}
		
		return null;
	}
}
