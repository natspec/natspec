package de.devboost.natspec.testscripting.java.jdt;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;

import de.devboost.natspec.testscripting.patterns.IValidationCallback;

public class JDTValidationResultCallback implements IValidationCallback {

	private final static String MARKER_TYPE = "de.devboost.natspec.testscripting.ui.annotationmarker";
	
	private final IResource resource;
	private final int offset;
	private final int length;

	public JDTValidationResultCallback(IResource resource, int offset,
			int length) {
		
		super();
		this.resource = resource;
		this.offset = offset;
		this.length = length;
	}

	@Override
	public void beginValidation() {
		removeMarkers();
	}

	@Override
	public void addError(String message) {
		addMarker(message, IMarker.SEVERITY_ERROR);
	}
	
	@Override
	public void addWarning(String message) {
		addMarker(message, IMarker.SEVERITY_WARNING);
	}

	private void addMarker(String message, int severity) {
		if (resource == null) {
			return;
		}
		
		if (offset < 0) {
			return;
		}
		
		addMarker(offset, length, message, severity);
	}

	private void addMarker(int start, int length, String message, int severity) {
		if (resource == null) {
			return;
		}
		
		try {
			IMarker marker = resource.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.CHAR_START, start);
			marker.setAttribute(IMarker.CHAR_END, start + length);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
		} catch (CoreException e) {
			// Ignore
		}
	}

	private void removeMarkers() {
		if (resource == null) {
			return;
		}
		
		try {
			resource.deleteMarkers(MARKER_TYPE, false, 0);
		} catch (CoreException e) {
			// Ignore
		}
	}
}
