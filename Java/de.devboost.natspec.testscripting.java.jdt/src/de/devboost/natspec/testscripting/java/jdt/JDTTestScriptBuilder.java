package de.devboost.natspec.testscripting.java.jdt;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.natspec.interfaces.INatSpecLogger;
import de.devboost.natspec.interfaces.ISelfDescribingNatSpecTemplate;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;
import de.devboost.natspec.testscripting.IFieldCache;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.java.GeneratedJavaFileFinder;
import de.devboost.natspec.testscripting.java.JavaTestScriptBuilder;
import de.devboost.natspec.testscripting.java.jdt.debug.JavaBreakpointUtils;

/**
 * The {@link JDTTestScriptBuilder} uses JDT functionality to extend the
 * abstract {@link JavaTestScriptBuilder} in order to provide a fully functional
 * builder for NatSpec documents that generates Java classes from NatSpec files.
 * The JDT functionality is required:
 * <ol>
 * <li>to determine whether a NatSpec file is contained in an output folder of
 * the Java project (see {@link #isBuildingNeeded(URI)}),</li>
 * <li>to determine the super type of the used NatSpec template class (see
 * {@link #extendsNatspecLogger(IFile)},
 * {@link #isSelfDescribingTemplate(IFile)} and
 * {@link #extendsSuperClass(IFile, Class)}), and</li>
 * <li>to determine the fields and their types in the NatSpec template class</li>
 * </ol>
 */
public class JDTTestScriptBuilder extends JavaTestScriptBuilder {

	private final static JDTUtility jdtUtility = ConcreteJDTUtility.INSTANCE;

	@Override
	public boolean isBuildingNeeded(URI uri) {
		boolean buildingNeeded = super.isBuildingNeeded(uri);
		if (!buildingNeeded) {
			return false;
		}
		
		IProject project = getProject(uri);
		if (project == null) {
			return false;
		}
		String platformString;
		if (uri.isPlatformResource()) {
			platformString = uri.toPlatformString(false);
		} else {
			return true;
		}
		
		boolean inOutputFolder = jdtUtility.isInOutputFolder(project, platformString);
		return !inOutputFolder;
	}
	
	@Override
	protected boolean extendsNatspecLogger(IFile templateFile) {
		return extendsSuperClass(templateFile, INatSpecLogger.class);
	}

	@Override
	protected boolean isSelfDescribingTemplate(IFile templateFile) {
		return extendsSuperClass(templateFile, ISelfDescribingNatSpecTemplate.class);
	}

	private boolean extendsSuperClass(IFile templateFile, Class<?> superClass) {
		ICompilationUnit compilationUnit = jdtUtility.getCompilationUnit(templateFile);
		if (compilationUnit == null) {
			return false;
		}
		
		try {
			IType[] types = compilationUnit.getTypes();
			if (types == null) {
				return false;
			}
			
			if (types.length == 0) {
				return false;
			}
			
			IType type = types[0];
			if (type == null) {
				return false;
			}
			
			JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();
			if (plugin == null) {
				return false;
			}
			
			String superClassName = superClass.getName();

			JDTTypeHierarchyCache typeHierarchyCache = plugin.getTypeHierarchyCache();
			Set<IType> superTypes = typeHierarchyCache.getAllSuperTypes(type);
			for (IType superType : superTypes) {
				String nextSuperTypeName = superType.getFullyQualifiedName();
				if (superClassName.equals(nextSuperTypeName)) {
					return true;
				}
			}
		} catch (JavaModelException e) {
			// ignore
		}
		return false;
	}
	
	@Override
	protected void postGenerateClassForScenario(NatspecResource resource, IFile file, IFile outputFile) {
		// Refresh break points in generated code (transfer them from the
		// corresponding NatSpec specification).
		try {
			GeneratedJavaFileFinder fileFinder = new GeneratedJavaFileFinder();
			JavaBreakpointUtils javaBreakpointUtils = new JavaBreakpointUtils(fileFinder);
			javaBreakpointUtils.synchronizeBreakpoints(resource, file, outputFile);
		} catch (CoreException e) {
			TestConnectorPlugin.logError(
					"Exception while synchronizing break points.", e);
		}
	}

	@Override
	protected Map<String, String> getFieldMap(IFile templateFile) {
		JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();
		if (plugin == null) {
			return Collections.emptyMap();
		}
		
		JavaDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider = plugin.getDynamicSyntaxPatternProvider();
		IFieldCache fieldTypeCache = dynamicSyntaxPatternProvider.getFieldTypeCache();
		Map<String, String> fieldMap = fieldTypeCache.getFieldNameToTypeMap(templateFile);
		return fieldMap;
	}
}
