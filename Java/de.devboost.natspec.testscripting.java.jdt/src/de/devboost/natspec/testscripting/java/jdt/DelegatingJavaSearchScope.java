package de.devboost.natspec.testscripting.java.jdt;

import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.search.IJavaSearchScope;

/**
 * The {@link DelegatingJavaSearchScope} is a facade for
 * {@link IJavaSearchScope}s that forwards all calls to a delegate except some
 * special cases to exclude the Aspose PDF library from the search scope.
 */
public class DelegatingJavaSearchScope implements IJavaSearchScope {

	private final IJavaSearchScope delegate;

	public DelegatingJavaSearchScope(IJavaSearchScope delegate) {
		super();
		this.delegate = delegate;
	}

	public boolean encloses(String resourcePath) {
		if (resourcePath != null && resourcePath.contains("|aspose/")) {
			return false;
		}
		if (resourcePath != null && resourcePath.contains("|com/aspose/")) {
			return false;
		}
		return delegate.encloses(resourcePath);
	}

	public boolean encloses(IJavaElement element) {
		return delegate.encloses(element);
	}

	public IPath[] enclosingProjectsAndJars() {
		return delegate.enclosingProjectsAndJars();
	}

	@Deprecated
	public boolean includesBinaries() {
		return delegate.includesBinaries();
	}

	@Deprecated
	public boolean includesClasspaths() {
		return delegate.includesClasspaths();
	}

	@Deprecated
	public void setIncludesBinaries(boolean includesBinaries) {
		delegate.setIncludesBinaries(includesBinaries);
	}

	@Deprecated
	public void setIncludesClasspaths(boolean includesClasspaths) {
		delegate.setIncludesClasspaths(includesClasspaths);
	}
}
