/**
 * This package contains classes that use the Eclipse Java Development Tools 
 * (JDT) to extract annotations from Java source code.
 */
package de.devboost.natspec.testscripting.java.jdt;