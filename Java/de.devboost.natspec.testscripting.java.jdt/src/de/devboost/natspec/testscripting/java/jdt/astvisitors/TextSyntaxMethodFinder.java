package de.devboost.natspec.testscripting.java.jdt.astvisitors;

import javax.print.attribute.TextSyntax;

import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;

/**
 * A {@link TextSyntaxMethodFinder} can be used to find methods which are
 * annotated with {@link de.devboost.natspec.annotations.TextSyntax} in an
 * AST provided by the JDT.
 */
public class TextSyntaxMethodFinder extends AbstractMethodFinder {

	private static final String TEXT_SYNTAX_CLASS_NAME = TextSyntax.class
			.getSimpleName();
	
	@Override
	public boolean visit(MethodDeclaration methodDeclaration) {
		IMethodBinding binding = methodDeclaration.resolveBinding();
		if (binding == null) {
			// this can be caused by compilation errors
			return false;
		}

		IMethod method = (IMethod) binding.getJavaElement();
		if (isMethodWithTextSyntax(method)) {
			add(methodDeclaration);
		}
		return false;
	}

	private boolean isMethodWithTextSyntax(IMethod method) {
		IAnnotation annotation = method.getAnnotation(TEXT_SYNTAX_CLASS_NAME);
		if (annotation != null) {
			return true;
		}
		return false;
	}
}