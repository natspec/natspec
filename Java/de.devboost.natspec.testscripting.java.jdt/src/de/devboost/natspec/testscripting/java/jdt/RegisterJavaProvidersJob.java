package de.devboost.natspec.testscripting.java.jdt;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import de.devboost.natspec.testscripting.AbstractWorkspaceJob;

public class RegisterJavaProvidersJob extends AbstractWorkspaceJob {

	private static final String REGISTERING_JAVA_SYNTAX_PATTERN_PROVIDERS = "Registering Java Syntax Pattern Providers";

	public RegisterJavaProvidersJob() {
		super(REGISTERING_JAVA_SYNTAX_PATTERN_PROVIDERS);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();
		if (plugin != null) {
			plugin.registerProviders(monitor);
		} else {
			String className = getClass().getSimpleName();
			String message = className + ".run() plugin is null";
			JDTJavaConnectorPlugin.logWarning(message, null);
		}
		return Status.OK_STATUS;
	}
}
