package de.devboost.natspec.testscripting.java.jdt;

import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import de.devboost.natspec.resource.natspec.INatspecBuilder;
import de.devboost.natspec.resource.natspec.NatspecBuilderRegistry;
import de.devboost.natspec.resource.natspec.mopp.NatspecMetaInformation;
import de.devboost.natspec.resource.natspec.mopp.NatspecResource;
import de.devboost.natspec.testscripting.IRevalidationVisitor;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.java.JavaTestScriptBuilder;

/**
 * The {@link JDTRevalidationVisitor} is used to visit the contents of the
 * Eclipse workspace and to re-validate and re-build NatSpec files. It does only
 * re-validate NatSpec files in projects which have the Java nature.
 */
public class JDTRevalidationVisitor implements IRevalidationVisitor {

	private static final NullProgressMonitor NULL_MONITOR = new NullProgressMonitor();

	private static final String NATSPEC_FILE_EXTENSION = new NatspecMetaInformation()
			.getSyntaxName();

	/**
	 * We store the output path of the current project in order to ignore
	 * resources located in this folder.
	 */
	private IPath currentOutputPath = null;

	@Override
	public boolean visit(IResource resource) {
		if (resource == null) {
			return true;
		}

		if (resource instanceof IProject) {
			IProject project = (IProject) resource;
			updateCurrentOutputPath(project);
		}

		// do not visit sub folders of the current project's output folder
		IPath fullPath = resource.getFullPath();
		if (fullPath != null && fullPath.equals(currentOutputPath)) {
			return false;
		}

		if (!(resource instanceof IFile)) {
			return true;
		}

		String fileExtension = resource.getFileExtension();
		if (NATSPEC_FILE_EXTENSION.equals(fileExtension)) {
			// re-validate
			if (!resource.exists()) {
				// skip resources that do not exist
				return true;
			}
			String path = fullPath.toString();
			revalidate(path);
		}

		return true;
	}

	private void updateCurrentOutputPath(IProject project) {
		IJavaProject javaProject = JavaCore.create(project);
		if (!javaProject.exists()) {
			return;
		}

		try {
			currentOutputPath = javaProject.getOutputLocation();
		} catch (JavaModelException e) {
			// ignore
		}
	}

	/**
	 * Loads and builds the resource at the given path.
	 * 
	 * @param path
	 *            a path to a NatSpec resource
	 */
	private void revalidate(String path) {
		ResourceSet rs = new ResourceSetImpl();
		URI uri = URI.createPlatformResourceURI(path, true);
		String uriString = uri.toString();
		//TestConnectorPlugin.logInfo("Revalidating resource at " + uriString,
		//		null);
		Resource resource = rs.getResource(uri, true);
		if (resource == null) {
			TestConnectorPlugin.logInfo("Resource not found at " + uriString,
					null);
			return;
		}

		if (resource instanceof NatspecResource) {
			NatspecResource natSpecResource = (NatspecResource) resource;
			// load resource to trigger validation
			natSpecResource.getContents();
			// also re-generate test code
			JavaTestScriptBuilder javaTestScriptBuilder = getJavaTestScriptBuilder();
			if (javaTestScriptBuilder != null) {
				boolean isBuildingNeeded = javaTestScriptBuilder.isBuildingNeeded(resource.getURI());
				if (!isBuildingNeeded) {
					return;
				}
				javaTestScriptBuilder.build(natSpecResource, NULL_MONITOR);

				//TestConnectorPlugin.logInfo(
				//		"Revalidating resource complete at " + uriString, null);
			}
		} else {
			TestConnectorPlugin.logWarning(
					"Found resource with extension .natspec that is not a NatSpec resource at "
							+ uriString, null);
		}
	}

	private JavaTestScriptBuilder getJavaTestScriptBuilder() {

		NatspecBuilderRegistry registry = NatspecBuilderRegistry.REGISTRY;
		Set<INatspecBuilder> builders = registry.getBuilders();
		for (INatspecBuilder builder : builders) {
			if (builder instanceof JavaTestScriptBuilder) {
				JavaTestScriptBuilder javaTestScriptBuilder = (JavaTestScriptBuilder) builder;
				return javaTestScriptBuilder;
			}
		}

		return null;
	}
}
