package de.devboost.natspec.testscripting.java.jdt.astvisitors;

import org.eclipse.jdt.core.dom.MethodDeclaration;

/**
 * A {@link MethodDeclarationFinder} can be used to find all declared methods in an AST provided by the JDT.
 */
public class MethodDeclarationFinder extends AbstractMethodFinder {
	
	@Override
	public boolean visit(MethodDeclaration methodDeclaration) {
		add(methodDeclaration);
		return false;
	}
}
