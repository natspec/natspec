package de.devboost.natspec.testscripting.java.jdt.astvisitors;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SimpleName;

/**
 * A {@link SetterFinder} can be used to find all setters in an AST provided by
 * the JDT.
 */
public class SetterFinder extends AbstractMethodFinder {
	
	@Override
	public boolean visit(MethodDeclaration methodDeclaration) {
		if (methodDeclaration.isConstructor()) {
			return false;
		}
		
		// check name
		SimpleName methodName = methodDeclaration.getName();
		String methodIdentifier = methodName.getIdentifier();
		if (!methodIdentifier.startsWith("set")) {
			return false;
		}
		
		// check number of arguments
		if (methodDeclaration.parameters().size() != 1) {
			return false;
		}
		
		add(methodDeclaration);
		return false;
	}
}