package de.devboost.natspec.testscripting.java.jdt.astvisitors;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import de.devboost.natspec.testscripting.Field;
import de.devboost.natspec.testscripting.NameHelper;

/**
 * The {@link FieldFinder} can be used to visit an AST provided by the JDT
 * to determine the qualified type names of all field declarations contained in
 * the AST as well as their names.
 */
public class FieldFinder extends ASTVisitor {
	
	private final Set<Field> fields = new LinkedHashSet<Field>();

	@Override
	public boolean visit(FieldDeclaration node) {
		// TODO check field visibility (any visibility for the template class
		// itself, protected or public for super types)
		Type fieldType = node.getType();
		ITypeBinding typeBinding = fieldType.resolveBinding();
		if (typeBinding == null) {
			return true;
		}
		String qualifiedName = typeBinding.getQualifiedName();
		String name = null;
		List<?> fragments = node.fragments();
		if (fragments.isEmpty()) {
			return true;
		}
		
		Object fragment0 = fragments.get(0);
		if (fragment0 instanceof VariableDeclarationFragment) {
			VariableDeclarationFragment vdf = (VariableDeclarationFragment) fragment0;
			name = vdf.getName().getIdentifier();
		}
		
		if (name == null) {
			return true;
		}
		
		// Remove type parameters from type name
		qualifiedName = NameHelper.INSTANCE.getNameWithoutTypeArguments(qualifiedName);
		Field field = new Field(name, qualifiedName);
		fields.add(field);
		return true;
	}

	public Set<Field> getFields() {
		return fields;
	}
}
