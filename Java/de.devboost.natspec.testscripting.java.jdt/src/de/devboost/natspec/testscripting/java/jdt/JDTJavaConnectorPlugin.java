package de.devboost.natspec.testscripting.java.jdt;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.osgi.framework.BundleContext;

import de.devboost.natspec.registries.SyntaxPatternRegistry;
import de.devboost.natspec.resource.natspec.NatspecBuilderRegistry;
import de.devboost.natspec.testscripting.IRevalidationVisitor;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.WorkspaceChangeCollector;
import de.devboost.natspec.testscripting.java.JavaFileChangeListener;
import de.devboost.natspec.testscripting.java.JavaTestScriptBuilder;

public class JDTJavaConnectorPlugin extends Plugin {

	private static final String PLUGIN_ID = JDTJavaConnectorPlugin.class.getPackage().getName();

	/**
	 * The single instance of this plug-in.
	 */
	private static JDTJavaConnectorPlugin instance;

	private final CompositeCompilationHandler compositeExtractor = new CompositeCompilationHandler();

	private final JavaDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider = new JavaDynamicSyntaxPatternProvider();

	private final WorkspaceChangeCollector workspaceChangeCollector = new WorkspaceChangeCollector();

	private final JDTTypeHierarchyCache typeHierarchyCache = new JDTTypeHierarchyCache();

	private final IRevalidationVisitor revalidationListener = new JDTRevalidationVisitor();

	@Override
	public void start(BundleContext context) throws Exception {
		instance = this;
		registerJavaBuilder();
		TestConnectorPlugin testConnectorPlugin = TestConnectorPlugin.getInstance();
		if (testConnectorPlugin != null) {
			testConnectorPlugin.addScriptValidator(workspaceChangeCollector);
			testConnectorPlugin.addRevalidationVisitor(revalidationListener);
		} else {
			logError("Can't register script validator and revalidation visitor.", null);
		}

		Job job = new RegisterJavaProvidersJob();
		job.schedule();

		super.start(context);
	}

	private void registerJavaBuilder() {
		// register test class builder
		JavaTestScriptBuilder scriptBuilder = new JDTTestScriptBuilder();
		NatspecBuilderRegistry.REGISTRY.registerBuilder(scriptBuilder);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		compositeExtractor.tearDown();

		// we must do this at the very end, since the tearDown() method requires
		// access to the plug-in instance (to save its state to the workspace
		// metadata folder)
		instance = null;
		super.stop(context);
	}

	public static JDTJavaConnectorPlugin getInstance() {
		return instance;
	}

	public JavaDynamicSyntaxPatternProvider getDynamicSyntaxPatternProvider() {
		return dynamicSyntaxPatternProvider;
	}

	public CompositeCompilationHandler getCompositeExtractor() {
		return compositeExtractor;
	}

	public void registerProviders(IProgressMonitor monitor) {
		SyntaxPatternRegistry.REGISTRY.add(dynamicSyntaxPatternProvider);

		compositeExtractor.setUp(monitor);
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		// TODO Use parameter event mask?
		// workspace.addResourceChangeListener(new JDTJavaTemplateChangeListener(), IResourceChangeEvent.POST_CHANGE);
		if (TestConnectorPlugin.getInstance().isDebugFeatureActive()) {
			// The JavaFileChangeListener is only registered if the (experimental) debug feature is active. The listener
			// causes serious performance issues when the workspace contains a log of error markers.
			workspace.addResourceChangeListener(new JavaFileChangeListener(), IResourceChangeEvent.POST_CHANGE);
		}
	}

	// FIXME 2.3 Use PluginLogger instead
	/**
	 * Helper method for error logging.
	 * 
	 * @param message
	 *            the error message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	public static IStatus logError(String message, Throwable throwable) {
		return log(IStatus.ERROR, message, throwable);
	}

	/**
	 * Helper method for logging informations.
	 * 
	 * @param message
	 *            the information message to log
	 * @param throwable
	 *            the exception that describes the information in detail (can be null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logInfo(String message, Throwable throwable) {
		return log(IStatus.INFO, message, throwable);
	}

	/**
	 * Helper method for logging warnings.
	 * 
	 * @param message
	 *            the warning message to log
	 * @param throwable
	 *            the exception that describes the warning in detail (can be null)
	 * 
	 * @return the status object describing the warning
	 */
	public static IStatus logWarning(String message, Throwable throwable) {
		return log(IStatus.WARNING, message, throwable);
	}

	/**
	 * Helper method for logging.
	 * 
	 * @param type
	 *            the type of the message to log
	 * @param message
	 *            the message to log
	 * @param throwable
	 *            the exception that describes the error in detail (can be null)
	 * 
	 * @return the status object describing the error
	 */
	protected static IStatus log(int type, String message, Throwable throwable) {
		IStatus status;
		if (throwable != null) {
			status = new Status(type, JDTJavaConnectorPlugin.PLUGIN_ID, 0, message, throwable);
		} else {
			status = new Status(type, JDTJavaConnectorPlugin.PLUGIN_ID, message);
		}
		
		final JDTJavaConnectorPlugin pluginInstance = JDTJavaConnectorPlugin.getInstance();
		if (pluginInstance == null) {
			System.err.println(message);
			if (throwable != null) {
				throwable.printStackTrace();
			}
		} else {
			pluginInstance.getLog().log(status);
		}
		return status;
	}

	public JDTTypeHierarchyCache getTypeHierarchyCache() {
		return typeHierarchyCache;
	}
}
