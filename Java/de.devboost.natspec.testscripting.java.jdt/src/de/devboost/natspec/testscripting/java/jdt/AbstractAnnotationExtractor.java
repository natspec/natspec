package de.devboost.natspec.testscripting.java.jdt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import de.devboost.natspec.testscripting.IFileProcessor;
import de.devboost.natspec.testscripting.TestConnectorPlugin;

public abstract class AbstractAnnotationExtractor implements IFileProcessor {

	@Override
	public Collection<IFile> process(Collection<IFile> files, IProgressMonitor monitor) {
		List<ICompilationUnit> compilationUnits = getCompilationUnits(files);
		return processUnits(compilationUnits, monitor);
	}

	/**
	 * Returns all compilation units contained in the given list of files.
	 */
	private List<ICompilationUnit> getCompilationUnits(Collection<IFile> files) {
		List<ICompilationUnit> compilationUnits = new ArrayList<ICompilationUnit>(files.size());
		for (IFile file : files) {
			ICompilationUnit compilationUnit = JavaCore.createCompilationUnitFrom(file);
			if (compilationUnit == null) {
				continue;
			}
			
			compilationUnits.add(compilationUnit);
		}
		
		return compilationUnits;
	}

	/**
	 * Returns all types contained in the given compilation unit. If the
	 * compilation unit does not contain types or if the types cannot be
	 * determined, an empty array is returned.
	 */
	protected IType[] getTypesSafe(ICompilationUnit compilationUnit) {
		try {
			return compilationUnit.getTypes();
		} catch (JavaModelException e) {
			TestConnectorPlugin.logWarning("Exception while analyzing Java class.", e);
		}
		
		return new IType[0];
	}

	/**
	 * This is a template methods for concrete sub classes of this class.
	 * 
	 * @param monitor a progress monitor (used to handle cancellation)
	 */
	protected abstract Collection<IFile> processUnits(List<ICompilationUnit> compilationUnits,
			IProgressMonitor monitor);
}
