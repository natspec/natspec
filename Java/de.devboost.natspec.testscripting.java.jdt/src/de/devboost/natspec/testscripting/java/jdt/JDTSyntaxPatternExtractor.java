package de.devboost.natspec.testscripting.java.jdt;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.TextSyntax;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

import de.devboost.natspec.annotations.NameBasedSyntax;
import de.devboost.natspec.annotations.StyledTextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.IPatternRegisterCallback;
import de.devboost.natspec.testscripting.IPatternRegistration;
import de.devboost.natspec.testscripting.PatternRegistration;
import de.devboost.natspec.testscripting.java.extract.JavaModelPatternExtractor;
import de.devboost.natspec.testscripting.java.model.JavaClassifier;
import de.devboost.natspec.testscripting.patterns.IValidationCallback;

/**
 * The {@link JDTSyntaxPatternExtractor} class can be used to extract text syntax patterns from Java classes that
 * contain annotations of type {@link TextSyntax}, {@link TextSyntaxes}, {@link NameBasedSyntax} or
 * {@link StyledTextSyntax}. To perform this task, the {@link JDTSyntaxPatternExtractor} uses a
 * {@link JDTJavaModelExtractor} to extract an abstract representation of the Java classes, which is called a Java
 * model. This model is passed on to a {@link JavaModelPatternExtractor}, which extracts {@link ISyntaxPattern}s from
 * the information contained in the model. All discovered patterns are registered using a
 * {@link IPatternRegisterCallback}.
 */
public class JDTSyntaxPatternExtractor extends AbstractAnnotationExtractor {

	private final JDTJavaModelExtractor jdtJavaModelExtractor = new JDTJavaModelExtractor();

	private final IPatternRegisterCallback patternRegisterCallback;

	public JDTSyntaxPatternExtractor(IPatternRegisterCallback patternRegisterCallback) {
		this.patternRegisterCallback = patternRegisterCallback;
	}

	/**
	 * Analyzes the given compilation units and registers all found syntax patterns. For all compilation units that
	 * contain patterns, the corresponding file is returned.
	 */
	@Override
	protected List<IFile> processUnits(List<ICompilationUnit> compilationUnits, IProgressMonitor monitor) {

		List<IFile> foundFiles = new ArrayList<IFile>();
		for (ICompilationUnit compilationUnit : compilationUnits) {
			if (monitor.isCanceled()) {
				return foundFiles;
			}

			IFile file = processInternal(compilationUnit);
			if (file == null) {
				continue;
			}
			foundFiles.add(file);
		}
		
		return foundFiles;
	}

	/**
	 * Analyzes the Java class contained in the given compilation unit and registers all found syntax patterns. If
	 * patterns were found, the file that corresponds to the compilation unit is returned. If no patterns were detected,
	 * <code>null</code> is returned.
	 */
	private IFile processInternal(ICompilationUnit compilationUnit) {

		IResource resource = ConcreteJDTUtility.INSTANCE.getCorrespondingResource(compilationUnit);
		if (resource == null) {
			return null;
		}

		if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			return processInternal(compilationUnit, file);
		}

		return null;
	}

	private IFile processInternal(ICompilationUnit compilationUnit, IFile file) {
		IValidationCallback callback = new JDTValidationResultCallback(file, -1, -1);
		IProject project = file.getProject();
		String projectName = project.getName();

		boolean foundPatterns = false;
		IType[] types = getTypesSafe(compilationUnit);
		for (IType type : types) {
			foundPatterns |= processInternal(file, callback, projectName, type);
		}

		if (foundPatterns) {
			return file;
		}

		return null;
	}

	private boolean processInternal(IFile file, IValidationCallback callback, String projectName, IType type) {

		String qualifiedName = type.getFullyQualifiedName();
		boolean isInterface = false;
		try {
			isInterface = type.isInterface();
		} catch (JavaModelException e) {
			// Ignore this
		}
		
		JavaClassifier javaClassifier = new JavaClassifier(qualifiedName, projectName, callback, isInterface);

		// Extract Java model from JDT type
		jdtJavaModelExtractor.findMethods(type, javaClassifier);
		
		// Extract patterns from Java model
		JavaModelPatternExtractor patternExtractor = JavaModelPatternExtractor.INSTANCE;
		List<ISyntaxPattern<?>> patterns = patternExtractor.extractPatterns(javaClassifier);
		String className = javaClassifier.getName();
		
		// Register patterns using the callback
		IPatternRegistration registration = new PatternRegistration(file, className, patterns);
		patternRegisterCallback.register(registration);
		
		return !patterns.isEmpty();
	}
}
