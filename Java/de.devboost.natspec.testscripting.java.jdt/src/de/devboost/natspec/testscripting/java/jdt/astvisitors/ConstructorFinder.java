package de.devboost.natspec.testscripting.java.jdt.astvisitors;

import org.eclipse.jdt.core.dom.MethodDeclaration;

/**
 * A {@link ConstructorFinder} can be used to find all declared constructors in
 * an AST provided by the JDT.
 */
public class ConstructorFinder extends AbstractMethodFinder {

	@Override
	public boolean visit(MethodDeclaration methodDeclaration) {
		if (!methodDeclaration.isConstructor()) {
			return false;
		}
		add(methodDeclaration);
		return false;
	}
}
