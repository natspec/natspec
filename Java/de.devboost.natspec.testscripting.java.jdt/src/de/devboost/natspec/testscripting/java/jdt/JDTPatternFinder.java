package de.devboost.natspec.testscripting.java.jdt;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.MethodDeclaration;

import de.devboost.natspec.testscripting.java.jdt.astvisitors.AbstractMethodFinder;
import de.devboost.natspec.testscripting.java.jdt.astvisitors.ConstructorFinder;
import de.devboost.natspec.testscripting.java.jdt.astvisitors.GetterFinder;
import de.devboost.natspec.testscripting.java.jdt.astvisitors.SetterFinder;
import de.devboost.natspec.testscripting.java.jdt.astvisitors.TextSyntaxMethodFinder;

/**
 * The {@link JDTPatternFinder} can be used extract information (e.g, the list 
 * of constructors) from a JDT type.
 */
public class JDTPatternFinder {

	private static final ParserHelper PARSER_HELPER = new ParserHelper();

	private final IType type;
	private final ASTNode compilationUnit;

	public JDTPatternFinder(IType type) {
		super();
		if (type == null) {
			throw new IllegalArgumentException("Type must not be null.");
		}
		if (type.isBinary()) {
			throw new IllegalArgumentException("Type must not be a binary type.");
		}
		
		this.type = type;
		this.compilationUnit = PARSER_HELPER.parse(type.getCompilationUnit());
	}

	/**
	 * Returns all constructors declared in the type that was passed to the
	 * constructor of this {@link JDTPatternFinder}.
	 */
	public List<MethodDeclaration> findConstructors() {
		ConstructorFinder finder = new ConstructorFinder();
		compilationUnit.accept(finder);
		return finder.getMethods();
	}

	/**
	 * Returns all getters declared in the type that was passed to the
	 * constructor of this {@link JDTPatternFinder}. This method does not return
	 * getters that are declared in super types.
	 * 
	 * Calling this method is equivalent to calling {@link #findGetters(false)}.
	 */
	public List<MethodDeclaration> findGetters() {
		return findGetters(false);
	}

	/**
	 * Returns all getters declared in the type that was passed to the
	 * constructor of this {@link JDTPatternFinder}. If 'includeSuperTypes' is
	 * <code>true</code>, the getters declared by all super types are returned
	 * as well.
	 * 
	 * Attention: If the super types are included, a type hierarchy is created,
	 * which can take significantly longer then determining the getters for 
	 * this type only.
	 */
	public List<MethodDeclaration> findGetters(boolean includeSuperTypes) {
		return findMethods(new GetterFinder(), includeSuperTypes);
	}
	
	/**
	 * Returns all setters declared in the type that was passed to the
	 * constructor of this {@link JDTPatternFinder}. This method does not return
	 * setters that are declared in super types.
	 * 
	 * Calling this method is equivalent to calling {@link #findSetters(false)}.
	 */
	public List<MethodDeclaration> findSetters() {
		return findSetters(false);
	}

	/**
	 * Returns all setters declared in the type that was passed to the
	 * constructor of this {@link JDTPatternFinder}. If 'includeSuperTypes' is
	 * <code>true</code>, the setters declared by all super types are returned
	 * as well.
	 * 
	 * Attention: If the super types are included, a type hierarchy is created,
	 * which can take significantly longer then determining the setters for 
	 * this type only.
	 */
	public List<MethodDeclaration> findSetters(boolean includeSuperTypes) {
		return findMethods(new SetterFinder(), includeSuperTypes);
	}
	
	private List<MethodDeclaration> findMethods(AbstractMethodFinder finder,
			boolean includeSuperTypes) {
		
		// Find methods in current type
		finder.clear();
		compilationUnit.accept(finder);
		List<MethodDeclaration> methods = new ArrayList<MethodDeclaration>();
		methods.addAll(finder.getMethods());
		
		// Find methods in super types (if required)
		if (!includeSuperTypes) {
			return methods;
		}

		try {
			NullProgressMonitor monitor = new NullProgressMonitor();
			ITypeHierarchy supertypeHierarchy = type.newSupertypeHierarchy(monitor);
			IType[] supertypes = supertypeHierarchy.getSupertypes(type);
			if (supertypes == null) {
				return methods;
			}

			for (IType superType : supertypes) {
				if (superType == null) {
					continue;
				}
				if (superType.isBinary()) {
					// The JDTPatternFinder cannot be used for binary types.
					// If we want to handle those as well, we must implement
					// a special finder for binary types.
					continue;
				}
				
				JDTPatternFinder superPatternFinder = new JDTPatternFinder(superType);
				methods.addAll(superPatternFinder.findMethods(finder, includeSuperTypes));
			}
		} catch (JavaModelException e) {
			JDTJavaConnectorPlugin.logWarning("Exception while determining super type.", e);
		}
		return methods;
	}

	public List<MethodDeclaration> findMethodsWithTextSyntax() {
		TextSyntaxMethodFinder finder = new TextSyntaxMethodFinder();
		compilationUnit.accept(finder);
		return finder.getMethods();
	}

	/**
	 * Returns the type that was passed to the constructor of this
	 * {@link JDTPatternFinder}.
	 */
	public IType getType() {
		return type;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " [type=" + type.getFullyQualifiedName() + "]";
	}
}
