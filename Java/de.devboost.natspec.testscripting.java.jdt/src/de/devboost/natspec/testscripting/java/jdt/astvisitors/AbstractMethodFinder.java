package de.devboost.natspec.testscripting.java.jdt.astvisitors;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.MethodDeclaration;

public abstract class AbstractMethodFinder extends ASTVisitor {

	private final List<MethodDeclaration> methodDeclarations = new ArrayList<MethodDeclaration>();

	public List<MethodDeclaration> getMethods() {
		return methodDeclarations;
	}

	protected void add(MethodDeclaration methodDeclaration) {
		methodDeclarations.add(methodDeclaration);
	}

	public void clear() {
		methodDeclarations.clear();
	}
}
