package de.devboost.natspec.testscripting.java.jdt;

import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;

import de.devboost.natspec.testscripting.Field;
import de.devboost.natspec.testscripting.IFieldCache;
import de.devboost.natspec.testscripting.TestConnectorPlugin;
import de.devboost.natspec.testscripting.java.JavaNatSpecConstants;

public class TemplateClassUtil {
	
	public final static TemplateClassUtil INSTANCE = new TemplateClassUtil();
	
	private TemplateClassUtil() {
	}

	public void templateClassChanged(IResource resource) {
		boolean isTemplateClassFile = isTemplateFile(resource);
		boolean isTemplateSuperClassFile = isTemplateSuperClassFile(resource);
		boolean isNeitherTemplateNorSuperClassFile = !isTemplateClassFile && !isTemplateSuperClassFile;
		if (isNeitherTemplateNorSuperClassFile) {
			return;
		}
		
		TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
		if (plugin == null) {
			return;
		}

		if (isTemplateClassFile) {
			// Validate all scripts that are located in the same directory or in sub folders
			IContainer parent = resource.getParent();
			plugin.triggerScriptValidation(parent);
		} else {
			// Validate all scripts that are located in the same workspace
			IResource workspace = resource.getWorkspace().getRoot();
			plugin.triggerScriptValidation(workspace);
		}
	}
	
	public boolean isTemplateOrSuperClassFile(IResource resource) {
		boolean isTemplateClassFile = isTemplateFile(resource);
		if (isTemplateClassFile) {
			return true;
		}
		
		boolean isTemplateSuperClassFile = isTemplateSuperClassFile(resource);
		return isTemplateSuperClassFile;
	}

	public boolean isTemplateFile(IResource resource) {
		String name = resource.getName();
		boolean isTemplateFile = JavaNatSpecConstants.TEST_CASE_TEMPLATE_FILE_NAME.equals(name);
		return isTemplateFile;
	}

	public boolean isTemplateSuperClassFile(IResource resource) {
		JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();
		if (plugin == null) {
			return false;
		}
		
		JavaDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider = plugin.getDynamicSyntaxPatternProvider();
		IFieldCache fieldTypeCache = dynamicSyntaxPatternProvider.getFieldTypeCache();
		if (fieldTypeCache instanceof JDTFieldTypeCache) {
			JDTFieldTypeCache jdtFieldTypeCache = (JDTFieldTypeCache) fieldTypeCache;
			Map<String, Set<Field>> fileToFieldTypesMap = jdtFieldTypeCache.getFileToFieldTypesMap();
			String fullPath = resource.getFullPath().toString();
			if (fileToFieldTypesMap.containsKey(fullPath)) {
				return true;
			}
		}
		return false;
	}
}
