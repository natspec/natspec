package de.devboost.natspec.testscripting.java.jdt.debug;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IBreakpointManager;
import org.eclipse.debug.core.model.IBreakpoint;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.debug.core.JDIDebugModel;

import de.devboost.natspec.testscripting.AbstractGeneratedFileFinder;
import de.devboost.natspec.testscripting.debug.AbstractBreakpointUtils;
import de.devboost.natspec.testscripting.debug.IDebugConstants;
import de.devboost.natspec.testscripting.util.Position;

public class JavaBreakpointUtils extends AbstractBreakpointUtils {

	public JavaBreakpointUtils(AbstractGeneratedFileFinder generatedFileFinder) {
		super(generatedFileFinder);
	}

	@Override
	public void addBreakpointToGeneratedFile(IFile generatedFile,
			Position positionInCode, IFile natspecFile, long markerID) throws CoreException {

		if (!positionInCode.isValid()) {
			return;
		}
		
		int line = positionInCode.getLine();
		
		IJavaElement element = JavaCore.create(generatedFile);
		if (!(element instanceof ICompilationUnit)) {
			return;
		}
		
		ICompilationUnit compilationUnit = (ICompilationUnit) element;
		IType[] types = compilationUnit.getTypes();
		if (types == null || types.length < 1) {
			return;
		}
		
		IType type = types[0];
		String typeName = type.getFullyQualifiedName();
		
		// We must 1 because line numbers start at zero and one more because the
		// code for the sentence is in the next line.
		int codeLine = line + 1;
		Map<String, Object> attributes = new LinkedHashMap<String, Object>();
		attributes.put(IDebugConstants.CORRESSPONDING_RESOURCE, natspecFile.getFullPath().toString());
		attributes.put(IDebugConstants.CORRESSPONDING_MARKER_ID, Long.toString(markerID));
		
		IBreakpoint lineBreakpoint = JDIDebugModel.createLineBreakpoint(
				generatedFile, typeName, codeLine, -1, -1, 0, true, attributes);
		
		// Register break point with manager
		DebugPlugin defaultPlugin = DebugPlugin.getDefault();
		IBreakpointManager breakpointManager = defaultPlugin.getBreakpointManager();
		breakpointManager.addBreakpoint(lineBreakpoint);
	}
}
