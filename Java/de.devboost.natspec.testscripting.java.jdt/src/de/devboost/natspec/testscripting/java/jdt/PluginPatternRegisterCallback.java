package de.devboost.natspec.testscripting.java.jdt;

import java.util.Collections;
import java.util.List;

import de.devboost.natspec.testscripting.IPatternRegisterCallback;
import de.devboost.natspec.testscripting.IPatternRegistration;

public class PluginPatternRegisterCallback implements IPatternRegisterCallback {

	/**
	 * Registers the given patterns with the {@link JavaDynamicSyntaxPatternProvider} assuming they were found in the
	 * given file.
	 */
	@Override
	public void register(IPatternRegistration registration) {
		register(Collections.singletonList(registration));
	}

	@Override
	public void register(List<IPatternRegistration> registrations) {
		JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();
		if (plugin == null) {
			// this can happen during Eclipse shutdown
			return;
		}

		JavaDynamicSyntaxPatternProvider dynamicPatternProvider = plugin.getDynamicSyntaxPatternProvider();
		if (dynamicPatternProvider == null) {
			return;
		}

		dynamicPatternProvider.register(registrations);
	}
}
