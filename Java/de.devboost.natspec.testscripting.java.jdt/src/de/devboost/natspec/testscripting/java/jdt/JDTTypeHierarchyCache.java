package de.devboost.natspec.testscripting.java.jdt;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.WorkingCopyOwner;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.internal.core.CreateTypeHierarchyOperation;
import org.eclipse.jdt.internal.core.DefaultWorkingCopyOwner;
import org.eclipse.jdt.internal.core.JavaModelManager;

import de.devboost.eclipse.jdtutilities.JDTUtility;

/**
 * The {@link JDTTypeHierarchyCache} is used to cache information about super
 * class relationships between classes. The cache is required because obtaining
 * this information from the JDT is expensive and we must reduce the number of
 * requests about super types to a minimum.
 */
// TODO Invalidate or update registered patterns when the type hierarchy is
// changed
@SuppressWarnings("restriction")
public class JDTTypeHierarchyCache {

	private Map<IType, Set<IType>> typeToSuperTypesMap = new LinkedHashMap<IType, Set<IType>>();

	/**
	 * Returns the direct super types for the given type.
	 * 
	 * @param type
	 *            the type to look up super types for
	 * @return all direct super types (i.e., the super class and all directly
	 *         implemented interfaces)
	 * @throws JavaModelException
	 *             if something goes wrong
	 */
	public Set<IType> getSuperTypes(IType type) throws JavaModelException {
		Set<IType> directSuperTypes = typeToSuperTypesMap.get(type);
		if (directSuperTypes != null) {
			return directSuperTypes;
		}

		String typeName = type.getFullyQualifiedName();
			
		// We must not create a type hierarchy for classes contained in
		// packages called 'aspose.*' as these classes cause
		// StackOverflowErrors in JDT. See also:
		// https://bugs.eclipse.org/bugs/show_bug.cgi?id=422832
		if (typeName.startsWith("aspose.")) {
			directSuperTypes = new LinkedHashSet<IType>(0);
			return directSuperTypes;
		}
		
		ITypeHierarchy typeHierarchy;
		try {
			typeHierarchy = newTypeHierarchy(type);
		} catch (NullPointerException e) {
			return new LinkedHashSet<IType>(0);
		} catch (StackOverflowError e) {
			JDTJavaConnectorPlugin.logError("StackOverflowError while creating type hierarchy.", e);
			throw e;
		}
		
		processTypeHierarchy(type, typeHierarchy);
		directSuperTypes = typeToSuperTypesMap.get(type);
		return directSuperTypes;
	}

	// This code is copied from class SourceType in order to use a different
	// search scope while creating the type hierarchies.
	public ITypeHierarchy newTypeHierarchy(IType type)
			throws JavaModelException {

		WorkingCopyOwner owner =  DefaultWorkingCopyOwner.PRIMARY;

		JavaModelManager javaModelManager = JavaModelManager.getJavaModelManager();
		ICompilationUnit[] workingCopies = javaModelManager.getWorkingCopies(owner, true/*add primary working copies*/);
		IJavaSearchScope workspaceScope = SearchEngine.createWorkspaceScope();
		IJavaSearchScope scope = new DelegatingJavaSearchScope(workspaceScope);
		
		IProgressMonitor monitor = null;
		boolean computeSubTypes = false;
		CreateTypeHierarchyOperation op = new CreateTypeHierarchyOperation(type, workingCopies, scope, computeSubTypes);
		op.runOperation(monitor);
		return op.getResult();
	}

	private void processTypeHierarchy(IType type, ITypeHierarchy typeHierarchy) {
		
		// TODO This does not return Object as super type of interfaces. Is this
		// a problem?
		IType[] superTypeArray = typeHierarchy.getSupertypes(type);
		List<IType> superTypeList = Arrays.asList(superTypeArray);
		Set<IType> superTypes = new LinkedHashSet<IType>(superTypeList);
		typeToSuperTypesMap.put(type, superTypes);
		for (IType superType : superTypes) {
			processTypeHierarchy(superType, typeHierarchy);
		}
	}

	public void clearCache(IFile file) {
		JDTUtility jdtUtility = ConcreteJDTUtility.INSTANCE;
		ICompilationUnit compilationUnit = jdtUtility.getCompilationUnit(file);
		if (compilationUnit == null) {
			return;
		}
		
		try {
			IType[] types = compilationUnit.getTypes();
			if (types == null) {
				return;
			}
			
			for (IType type : types) {
				typeToSuperTypesMap.remove(type);
			}
		} catch (JavaModelException e) {
			// Ignore
		}
	}

	public Set<IType> getAllSuperTypes(IType type) throws JavaModelException {
		Set<IType> allSuperTypes = new LinkedHashSet<IType>();
		Set<IType> superTypes = getSuperTypes(type);
		allSuperTypes.addAll(superTypes);
		for (IType superType : superTypes) {
			Set<IType> upperTypes = getAllSuperTypes(superType);
			allSuperTypes.addAll(upperTypes);
		}
		return allSuperTypes;
	}
}
