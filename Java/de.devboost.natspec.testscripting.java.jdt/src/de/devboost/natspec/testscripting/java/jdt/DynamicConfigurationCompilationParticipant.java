package de.devboost.natspec.testscripting.java.jdt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.compiler.BuildContext;

import de.devboost.eclipse.jdtutilities.AbstractCompilationParticipant;
import de.devboost.eclipse.jdtutilities.CompilationEvent;
import de.devboost.natspec.annotations.NameBasedSyntax;
import de.devboost.natspec.annotations.StyledTextSyntax;
import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;
import de.devboost.natspec.testscripting.IFieldCache;

/**
 * The {@link DynamicConfigurationCompilationParticipant} is registered with the Eclipse JDT to listen to build events.
 * Whenever a class is compiled by the JDT, this class triggers an analysis of the class to search for annotations of
 * type {@link TextSyntax}, {@link TextSyntaxes}, {@link NameBasedSyntax} and {@link StyledTextSyntax}. Also, cached
 * data about fields contained in NatSpec template classes and their super classes is updated when a template class is
 * compiled.
 */
public class DynamicConfigurationCompilationParticipant extends AbstractCompilationParticipant {

	/**
	 * Does nothing as no action is required when builds are started.
	 */
	@Override
	public void buildStarting(CompilationEvent event) {
		// do nothing
	}

	/**
	 * Triggers the analysis for all compiled classes.
	 */
	@Override
	public void buildFinished(Collection<CompilationEvent> events) {
		JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();
		if (plugin == null) {
			return;
		}

		List<IFile> files = new ArrayList<IFile>();
		for (CompilationEvent event : events) {
			BuildContext context = event.getContext();
			IFile file = context.getFile();
			files.add(file);
		}

		JDTTypeHierarchyCache typeHierarchyCache = plugin.getTypeHierarchyCache();
		JavaDynamicSyntaxPatternProvider dynamicSyntaxPatternProvider = plugin.getDynamicSyntaxPatternProvider();
		IFieldCache fieldTypeCache = dynamicSyntaxPatternProvider.getFieldTypeCache();
		
		// Save the current field names and types here to compare them later for modifications
		Map<IFile, Map<String, String>> oldFieldTypes = new LinkedHashMap<>();
		for (IFile file : files) {
			boolean isTemplateFile = TemplateClassUtil.INSTANCE.isTemplateFile(file);
			boolean templateSuperClassFile = TemplateClassUtil.INSTANCE.isTemplateSuperClassFile(file);
			if (templateSuperClassFile && !isTemplateFile) {
				oldFieldTypes.put(file, fieldTypeCache.getFieldNameToTypeMap(file));
			}
			fieldTypeCache.clearCache(file);
			typeHierarchyCache.clearCache(file);
		}

		CompositeCompilationHandler extractor = plugin.getCompositeExtractor();
		extractor.process(files);

		for (IFile file : files) {
			// We compare the field names and types here to avoid the a script revalidation is triggered even when all
			// field names and types of the template and its super classes are unchanged.
			if (TemplateClassUtil.INSTANCE.isTemplateOrSuperClassFile(file)) {
				Map<String, String> oldFields = oldFieldTypes.get(file);
				Map<String, String> newFields = fieldTypeCache.getFieldNameToTypeMap(file);
				if (oldFields != null && oldFields.equals(newFields)) {
					continue;
				}
				
				TemplateClassUtil.INSTANCE.templateClassChanged(file);
			}
		}
	}
}
