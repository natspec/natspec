package de.devboost.natspec.testscripting.java.jdt;

import java.util.ArrayList;
import java.util.List;

import de.devboost.natspec.testscripting.IPatternRegisterCallback;
import de.devboost.natspec.testscripting.IPatternRegistration;

/**
 * The {@link AggregatingPatternRegisterCallback} can be used to collect a
 * number of pattern registration and push them collectively to another
 * {@link IPatternRegisterCallback} later on. This is useful to delay the
 * registration of syntax pattern if one knows that there will be more more
 * registrations soon.
 */
public class AggregatingPatternRegisterCallback implements
		IPatternRegisterCallback {
	
	private final List<IPatternRegistration> registrations = new ArrayList<IPatternRegistration>();

	@Override
	public void register(IPatternRegistration registration) {
		this.registrations.add(registration);
	}

	@Override
	public void register(List<IPatternRegistration> registrations) {
		this.registrations.addAll(registrations);
	}

	/**
	 * Sends all collected registrations collectively to the given callback.
	 */
	public void flush(IPatternRegisterCallback callback) {
		callback.register(registrations);
	}
}
