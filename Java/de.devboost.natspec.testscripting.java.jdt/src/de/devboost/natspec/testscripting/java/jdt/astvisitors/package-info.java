/**
 * This package contains classes that visit ASTs provided by the JDT to find 
 * information in these syntax trees.
 */
package de.devboost.natspec.testscripting.java.jdt.astvisitors;