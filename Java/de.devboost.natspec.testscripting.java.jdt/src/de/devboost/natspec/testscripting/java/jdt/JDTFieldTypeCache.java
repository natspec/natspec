package de.devboost.natspec.testscripting.java.jdt;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeHierarchy;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.dom.CompilationUnit;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.essentials.ComparisonUtils;
import de.devboost.natspec.testscripting.Field;
import de.devboost.natspec.testscripting.IFieldCache;
import de.devboost.natspec.testscripting.java.jdt.astvisitors.FieldFinder;

/**
 * A JDT-specific implementation of the {@link IFieldCache} interface.
 */
public class JDTFieldTypeCache implements IFieldCache {

	private static final String CLASS_FILE_SUFFIX = ".class";
	
	private static final NullProgressMonitor NULL_MONITOR = new NullProgressMonitor();

	private static final ParserHelper PARSER_HELPER = new ParserHelper();

	private static final JDTUtility JDT_UTILITY = ConcreteJDTUtility.INSTANCE;

	/**
	 * A map from file paths of Java classes (source or binary) to a set of all fields in the class. We do not map class
	 * names to sets of fields, because there can be multiple classes with the same name in the workspace (e.g., in
	 * different projects) that contains different fields.
	 */
	private Map<String, Set<Field>> pathToFieldTypesMap = new LinkedHashMap<String, Set<Field>>();
	
	private Set<String> pathsWithInvalidFields = new LinkedHashSet<String>();

	/**
	 * A map from file paths of Java classes (source or binary) to the paths of the files containing their super class.
	 * We do not map class names to super class names, because there can be multiple classes with the same name in the
	 * workspace (e.g., in different projects) that have different super classes. 
	 * 
	 * <p>
	 * If the super type was determined, but was not found, the map will contain <code>null</code> as value.
	 */
	private Map<String, String> typePathToSuperTypePathMap = new LinkedHashMap<String, String>();

	@Override
	public Collection<String> getAllFieldTypes(IFile templateFile) {
		return getFieldNameToTypeMap(templateFile).values();
	}

	@Override
	public Map<String, String> getFieldNameToTypeMap(IFile templateFile) {
		if (templateFile == null) {
			return new LinkedHashMap<String, String>();
		}
		
		Map<String, String> fieldNameToTypeNameMap = new LinkedHashMap<String, String>();

		IProject templateFileProject = templateFile.getProject();
		// Find the type that is contained in the template file
		IType type = getType(templateFile, templateFileProject);
		// Here we climb up the type hierarchy to get all fields the are defined in the template file and in all its
		// super classes.
		while (type != null) {
			// Determine the field of the current type
			Set<Field> fields = getFields(type);
			for (Field field : fields) {
				String fieldName = field.getName();
				// We must not override the type of fields to avoid that super classes redefine the fields type.
				if (!fieldNameToTypeNameMap.containsKey(fieldName)) {
					fieldNameToTypeNameMap.put(fieldName, field.getType());
				}
			}
			
			// Move to the super type
			type = getSuperType(type, templateFileProject);
		}
		
		return fieldNameToTypeNameMap;
	}

	@Override
	public void clearCache(IFile file) {
		String fullPath = file.getFullPath().toString();
		
		invalidate(fullPath, 0);
		//pathToFieldTypesMap.remove(fullPath);
		typePathToSuperTypePathMap.remove(fullPath);
	}

	private void invalidate(String fullPath, int depth) {
		if (depth > 50) {
			return;
		}
		
		pathsWithInvalidFields.add(fullPath);
		
		Set<Entry<String, String>> entrySet = typePathToSuperTypePathMap.entrySet();
		for (Entry<String, String> entry : entrySet) {
			String subTypePath = entry.getKey();
			String superTypePath = entry.getValue();
			if (ComparisonUtils.INSTANCE.equals(superTypePath, fullPath)) {
				invalidate(subTypePath, depth + 1);
			}
		}
	}

	/**
	 * Returns the super type of the given type or <code>null</code> if the types super class could not be determined.
	 * 
	 * @param templateFileProject
	 *            The project that contains the original type for which we climb up the type hierarchy. This project is
	 *            required to resolve some binary types (e.g., JRE library classes).
	 */
	// This method is intentionally protected instead of private to override it in test cases.
	protected IType getSuperType(IType type, IProject templateFileProject) {
		IFile file = getFile(type);
		if (file == null) {
			return null;
		}
		
		IPath fullPath = file.getFullPath();
		String fullPathString = fullPath.toString();
		String superTypeFilePath = typePathToSuperTypePathMap.get(fullPathString);
		if (superTypeFilePath == null && typePathToSuperTypePathMap.containsKey(fullPathString)) {
			// The type does not have a super type
			return null;
		}
		
		IFile superTypeFile = getFile(superTypeFilePath);
		IType superType = null;
		if (superTypeFile != null) {
			superType = getType(superTypeFile, templateFileProject);
			if (superType != null) {
				return superType;
			}
		}
		
		superType = computeSuperType(type);
		if (superType == null) {
			// For types the do not have super type we explicitly put 'null' in the map
			typePathToSuperTypePathMap.put(fullPathString, null);
			return null;
		}
		
		superTypeFilePath = getPath(superType);
		typePathToSuperTypePathMap.put(fullPathString, superTypeFilePath);
		return superType;
	}

	/**
	 * Returns the file located at the given path in the current workspace or <code>null</code> if the path is not
	 * contained in the workspace.
	 */
	private IFile getFile(String fullPath) {
		if (fullPath == null) {
			return null;
		}
		
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = workspace.getRoot();
		IPath path = new Path(fullPath);
		return root.getFile(path);
	}

	// This method is intentionally protected instead of private to override it
	// in test cases.
	protected IType getType(IFile file, IProject referencingProject) {
		IType type = getTypeForSourceFile(file);
		if (type != null) {
			return type;
		}
		
		// File is not a source file or does not contain a type. Try to get type assuming the file is a class file.
		type = getTypeForBinaryFile(file);
		if (type != null) {
			return type;
		}
		
		if (referencingProject == null) {
			return null;
		}
		
		// If the type could not be resolved yet, it is probably a class from the JRE library. Therfore, we try to
		// resolved the type using its class name.
		IJavaElement element = JavaCore.create(file);
		IJavaProject javaProject = JDT_UTILITY.getJavaProject(referencingProject);
		if (javaProject == null) {
			return null;
		}
		
		try {
			String className = element.getElementName();
			// Remove suffix
			if (className.endsWith(CLASS_FILE_SUFFIX)) {
				className = className.substring(0, className.length() - CLASS_FILE_SUFFIX.length());
			}
			// Look up the class in the project based on its fully qualified name
			IJavaElement foundElement = javaProject.findType(className);
			if (foundElement != null && foundElement.exists()) {
				if (foundElement instanceof IType) {
					return (IType) foundElement;
				}
			}
		} catch (JavaModelException e) {
			// Ignore
			return null;
		}
		
		return type;
	}

	/**
	 * Returns the first type that is contained in the given file. The file must be a Java source file.
	 */
	private IType getTypeForSourceFile(IFile file) {
		ICompilationUnit compilationUnit = getCompilationUnit(file);
		if (compilationUnit == null) {
			return null;
		}
		
		IType firstType = getFirstType(compilationUnit);
		return firstType;
	}

	/**
	 * Returns the type that is contained in the given file. The file must be a Java class file.
	 */
	private IType getTypeForBinaryFile(IFile file) {
		return JDT_UTILITY.getType(file);
	}

	/**
	 * Returns the path to the file that contains the given type.
	 */
	// This method is intentionally protected instead of private to access it in test cases.
	protected String getPath(IType type) {
		if (type.isBinary()) {
			IJavaElement parent = type.getParent();
			if (parent instanceof IClassFile) {
				IClassFile classFile = (IClassFile) parent;
				IPath classFilePath = classFile.getPath();
				// Some class files may be contained in JAR files. In this case
				// getPath() returns the path to the JAR file. To ensure that
				// different paths are returned for the different classes in the
				// JAR, we append the fully qualified name of the class to the
				// JAR file path.
				String fullPath = classFilePath  + "/" + type.getFullyQualifiedName() + CLASS_FILE_SUFFIX;
				return fullPath;
			}
		} else {
			IResource superTypeResource = type.getResource();
			if (superTypeResource instanceof IFile) {
				IFile superTypeFile = (IFile) superTypeResource;
				return superTypeFile.getFullPath().toString();
			}
		}
		return null;
	}

	/**
	 * Returns the file that contains the given type.
	 */
	// This method is intentionally protected instead of private to access it in test cases.
	protected IFile getFile(IType type) {
		IResource resource = type.getResource();
		if (resource instanceof IFile) {
			IFile file = (IFile) resource;
			return file;
		}
		
		return null;
	}

	/**
	 * Returns the first type that is contained in the compilation unit.
	 */
	private IType getFirstType(ICompilationUnit compilationUnit) {
		IType firstType;
		try {
			IType[] types = compilationUnit.getTypes();
			if (types.length < 1) {
				return null;
			}

			firstType = types[0];
		} catch (JavaModelException e) {
			// Ignore
			return null;
		}
		return firstType;
	}

	private IType computeSuperType(IType type) {
		// Go up the type hierarchy and collect all types
		ITypeHierarchy supertypeHierarchy;
		try {
			supertypeHierarchy = type.newSupertypeHierarchy(NULL_MONITOR);
		} catch (JavaModelException e) {
			// Ignore
			return null;
		}

		IType superType = supertypeHierarchy.getSuperclass(type);
		if (superType == null) {
			return null;
		}
		
		return superType;
	}

	/**
	 * Returns the fields that are contained in the given type. If the fields have not been computed yet, they are
	 * computed.
	 */
	private Set<Field> getFields(IType type) {
		String path = getPath(type);
		return getFields(path, type);
	}

	/**
	 * Returns the fields that are contained in the given type. If the fields have not been computed yet, they are
	 * computed.
	 * 
	 * @param fullPath the path of the file that contains the type
	 */
	private Set<Field> getFields(String fullPath, IType type) {
		Set<Field> fields = pathToFieldTypesMap.get(fullPath);
		
		boolean isInvalid = pathsWithInvalidFields.contains(fullPath);
		boolean isValid = !isInvalid;
		if (isValid) {
			if (fields != null) {
				return fields;
			}
		}
		
		fields = computeFields(type);
		pathToFieldTypesMap.put(fullPath, fields);
		pathsWithInvalidFields.remove(fullPath);
		return fields;
	}

	/**
	 * Computes the fields that are contained in the given type.
	 */
	private Set<Field> computeFields(IType type) {
		if (type.isBinary()) {
			// The type is a binary type.
			return computeFieldsFromBinary(type);
		} else {
			// The type is a source type.
			return computeFieldsFromSource(type);
		}
	}

	/**
	 * Uses the JDT parser to analyze the fields of the given type.
	 */
	private Set<Field> computeFieldsFromSource(IType type) {
		IFile typeFile = getFile(type);
		FieldFinder fieldTypeFinder = new FieldFinder();
		ICompilationUnit cu = getCompilationUnit(typeFile);
		try {
			CompilationUnit compilationUnit = PARSER_HELPER.parse(cu);
			compilationUnit.accept(fieldTypeFinder);
		} catch (IllegalStateException e) {
			// Ignore this. There is nothing we can do about.
		}

		Set<Field> fields = fieldTypeFinder.getFields();
		return fields;
	}

	private ICompilationUnit getCompilationUnit(IFile typeFile) {
		return JDT_UTILITY.getCompilationUnit(typeFile);
	}
	
	/**
	 * Uses the JDT Java model to analyze the fields of the given type.
	 */
	private Set<Field> computeFieldsFromBinary(IType type) {
		try {
			IField[] fields = type.getFields();
			Set<Field> result = new LinkedHashSet<Field>(fields.length);
			for (IField field : fields) {
				Field newField = createField(field);
				if (newField == null) {
					continue;
				}
				result.add(newField);
			}
			return result;
		} catch (JavaModelException e) {
			// Ignore this.
			return Collections.emptySet();
		}
	}

	private Field createField(IField field) throws JavaModelException {
		// Check visibility
		int flags = field.getFlags();
		boolean isPrivate = Flags.isPrivate(flags);
		if (isPrivate) {
			return null;
		}
		
		String name = field.getElementName();
		String typeSignature = field.getTypeSignature();
		String type = Signature.toString(typeSignature);
		return new Field(name, type);
	}

	public Map<String, Set<Field>> getFileToFieldTypesMap() {
		return pathToFieldTypesMap;
	}
	
	public Map<String, String> getTypeToSuperTypeMap() {
		return typePathToSuperTypePathMap;
	}
}
