package de.devboost.natspec.testscripting.java.jdt;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;

import de.devboost.eclipse.jdtutilities.AbstractWorkspaceCleaner;
import de.devboost.natspec.testscripting.TestConnectorPlugin;

public class WorkspaceCleaner extends AbstractWorkspaceCleaner {

	private static final String PREFERENCE_KEY_RAN_BEFORE = "ranBefore";

	@Override
	protected void setCleaned() {
		IEclipsePreferences node = getPreferenceNode();
		node.putBoolean(PREFERENCE_KEY_RAN_BEFORE, true);
		try {
			node.flush();
		} catch (BackingStoreException e) {
			logException(e);
		}
	}

	@Override
	protected boolean mustClean() {
		IEclipsePreferences node = getPreferenceNode();
		boolean mustClean = !node.getBoolean(PREFERENCE_KEY_RAN_BEFORE, false);
		return mustClean;
	}

	private IEclipsePreferences getPreferenceNode() {
		return InstanceScope.INSTANCE.getNode(TestConnectorPlugin.PLUGIN_ID);
	}

	@Override
	protected void logException(Exception e) {
		TestConnectorPlugin.logError(e.getMessage(), e);
	}

	@Override
	protected void logInfo(String message) {
		TestConnectorPlugin.logInfo(message, null);
	}
}
