package de.devboost.natspec.testscripting.java.jdt;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.Flags;
import org.eclipse.jdt.core.IAnnotatable;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IField;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMemberValuePair;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.dom.Annotation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IExtendedModifier;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MemberValuePair;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.NormalAnnotation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleMemberAnnotation;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.StringLiteral;
import org.eclipse.jdt.core.dom.Type;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.natspec.annotations.BooleanSyntax;
import de.devboost.natspec.annotations.NameBasedSyntax;
import de.devboost.natspec.annotations.StyledTextSyntax;
import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;
import de.devboost.natspec.annotations.parameters.ParameterSyntax;
import de.devboost.natspec.annotations.rules.AllowedSyntaxPattern;
import de.devboost.natspec.annotations.rules.AllowedSyntaxPatterns;
import de.devboost.natspec.annotations.styling.Bold;
import de.devboost.natspec.annotations.styling.Color;
import de.devboost.natspec.annotations.styling.Italic;
import de.devboost.natspec.annotations.styling.Strikethrough;
import de.devboost.natspec.annotations.styling.Underline;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.java.jdt.astvisitors.MethodDeclarationFinder;
import de.devboost.natspec.testscripting.java.model.JavaAnnotation;
import de.devboost.natspec.testscripting.java.model.JavaAssignment;
import de.devboost.natspec.testscripting.java.model.JavaClassifier;
import de.devboost.natspec.testscripting.java.model.JavaEnum;
import de.devboost.natspec.testscripting.java.model.JavaExpression;
import de.devboost.natspec.testscripting.java.model.JavaMethod;
import de.devboost.natspec.testscripting.java.model.JavaModifier;
import de.devboost.natspec.testscripting.java.model.JavaObjectArray;
import de.devboost.natspec.testscripting.java.model.JavaParameter;
import de.devboost.natspec.testscripting.java.model.JavaStringLiteral;
import de.devboost.natspec.testscripting.java.model.JavaType;
import de.devboost.natspec.testscripting.patterns.IValidationCallback;
import de.devboost.natspec.testscripting.patterns.NullValidationCallback;

/**
 * The {@link JDTJavaModelExtractor} processes Java classes and searches for the {@link TextSyntax}, the
 * {@link TextSyntaxes}, {@link NameBasedSyntax} and the {@link StyledTextSyntax} annotation. For each method that is
 * augmented with such an annotation, a respective syntax pattern is created and registered with the
 * {@link JavaDynamicSyntaxPatternProvider}. This allows to modify classes (e.g., to add or remove {@link TextSyntax}
 * annotations) and use these syntax patterns immediately in NatSpec documents.
 * <p>
 * The {@link JDTJavaModelExtractor} does also process the {@link BooleanSyntax} annotation for method parameters.
 */
public class JDTJavaModelExtractor {

	private final static JDTUtility JDT_UTILITY = ConcreteJDTUtility.INSTANCE;
	
	private final static Class<?>[] METHOD_ANNOTATIONS = new Class<?>[] { TextSyntax.class, TextSyntaxes.class,
			NameBasedSyntax.class, StyledTextSyntax.class, Bold.class, Color.class, Italic.class, Strikethrough.class,
			Underline.class, ParameterSyntax.class };

	public void findMethods(IType type, JavaClassifier javaClassifier) {
		// For performance reasons, we check whether the type is annotated or contains annotated methods to avoid
		// parsing types without @TextSyntax or @NameBasedSyntax annotations.
		boolean hasTextSyntax = hasTextSyntaxAnnotations(type);
		boolean hasNameBasedSyntaxMethod = hasNameBasedSyntaxMethodAnnotations(type);
		boolean hasStyledSyntax = hasStyledSyntaxAnnotations(type);
		boolean hasParameterSyntax = hasParameterSyntaxAnnotations(type);
		boolean hasRelevantTypeAnnotations = hasRelevantTypeAnnotations(type);
		if (!hasTextSyntax && !hasNameBasedSyntaxMethod && !hasRelevantTypeAnnotations && !hasStyledSyntax && !hasParameterSyntax) {
			return;
		}

		findMethods(type, javaClassifier, hasRelevantTypeAnnotations);

		// We only need to get type annotations if the type is annotated with @NameBasedSyntax
		if (hasRelevantTypeAnnotations) {
			convertType(type, javaClassifier);
		}
	}

	private void findMethods(IType type, JavaClassifier javaClassifier, boolean hasNameBasedSyntaxType) {
		if (type.isBinary()) {
			findMethodsInJavaBinary(type, javaClassifier, hasNameBasedSyntaxType);
		} else {
			findMethodsInJavaSource(type, javaClassifier, hasNameBasedSyntaxType);
		}
	}

	private void findMethodsInJavaBinary(IType type, JavaClassifier javaClassifier, boolean hasNameBasedSyntaxType) {
		List<JavaMethod> javaMethods = javaClassifier.getMethods();
		try {
			IMethod[] methods = type.getMethods();
			for (IMethod method : methods) {
				JavaMethod javaMethod = convertBinaryMethod(method, hasNameBasedSyntaxType);
				if (javaMethod == null) {
					continue;
				}
				
				javaMethods.add(javaMethod);
			}
		} catch (JavaModelException e) {
			// Ignore
		}
	}

	protected void findMethodsInJavaSource(IType type, JavaClassifier javaClassifier, boolean hasNameBasedSyntaxType) {
		ICompilationUnit compilationUnit = type.getCompilationUnit();
		CompilationUnit parsedCompilationUnit = JDT_UTILITY.parse(compilationUnit);
		MethodDeclarationFinder visitor = new MethodDeclarationFinder();
		parsedCompilationUnit.accept(visitor);

		List<JavaMethod> javaMethods = javaClassifier.getMethods();
		List<MethodDeclaration> methodDeclarations = visitor.getMethods();
		for (MethodDeclaration methodDeclaration : methodDeclarations) {
			IResource resource = type.getResource();
			JavaMethod javaMethod = convertMethod(resource, methodDeclaration, hasNameBasedSyntaxType);
			if (javaMethod == null) {
				continue;
			}
			
			javaMethods.add(javaMethod);
		}
	}

	private JavaMethod convertBinaryMethod(IMethod method, boolean hasNameBasedSyntaxTypeAnnotation)
			throws JavaModelException {

		String methodName = method.getElementName();
		JDTValidationResultCallback validationCallback = createValidationCallback(method);
		JavaMethod javaMethod = new JavaMethod(methodName, validationCallback);

		// determine annotations
		convertBinaryAnnotations(method, javaMethod);
		if (javaMethod.getAnnotations().isEmpty() && !hasNameBasedSyntaxTypeAnnotation) {
			return null;
		}

		// determine modifier
		JavaModifier javaModifier = convertModifier(method);
		javaMethod.setModifier(javaModifier);

		// determine return type
		JavaType returnType = convertBinaryReturnType(method);
		javaMethod.setReturnType(returnType);

		// determine parameters
		List<JavaParameter> parameters = convertBinaryParameters(method);
		javaMethod.getParameters().addAll(parameters);

		return javaMethod;
	}

	private JDTValidationResultCallback createValidationCallback(IMethod method) throws JavaModelException {
		IResource resource = method.getResource();
		ISourceRange sourceRange = method.getSourceRange();
		int offset = sourceRange.getOffset();
		int length = sourceRange.getLength();
		JDTValidationResultCallback validationCallback = new JDTValidationResultCallback(resource, offset, length);
		return validationCallback;
	}

	/**
	 * Derives a {@link JavaMethod} from the given method declaration.
	 * 
	 * @param resource
	 *            the resource of the Java class that contains the method
	 */
	private JavaMethod convertMethod(IResource resource, MethodDeclaration methodDeclaration,
			boolean hasNameBasedSyntaxTypeAnnotation) {

		String methodName = methodDeclaration.getName().getIdentifier();
		int offset = methodDeclaration.getStartPosition();
		int length = methodDeclaration.getLength();
		JDTValidationResultCallback validationCallback = new JDTValidationResultCallback(resource, offset, length);
		JavaMethod javaMethod = new JavaMethod(methodName, validationCallback);

		// determine annotations
		convertAnnotations(methodDeclaration, javaMethod);
		if (javaMethod.getAnnotations().isEmpty() && !hasNameBasedSyntaxTypeAnnotation) {
			return null;
		}

		// determine modifier
		JavaModifier javaModifier = convertModifier(methodDeclaration);
		javaMethod.setModifier(javaModifier);

		// determine return type
		JavaType returnType = convertReturnType(methodDeclaration);
		javaMethod.setReturnType(returnType);

		// determine parameters
		List<JavaParameter> parameters = convertParameters(resource, methodDeclaration);
		javaMethod.getParameters().addAll(parameters);

		return javaMethod;
	}

	private void convertType(IType type, JavaClassifier javaClassifier) {
		// determine annotations
		convertTypeAnnotation(type, javaClassifier, NameBasedSyntax.class);
		convertTypeAnnotation(type, javaClassifier, AllowedSyntaxPatterns.class);
		convertTypeAnnotation(type, javaClassifier, AllowedSyntaxPattern.class);
	}

	private void convertTypeAnnotation(IType type, JavaClassifier javaClassifier, Class<?> typeAnnotationClass) {
		JavaAnnotation javaAnnotation = getAnnotation(type, typeAnnotationClass);
		if (javaAnnotation == null) {
			return;
		}

		List<JavaAnnotation> javaAnnotations = javaClassifier.getAnnotations();
		javaAnnotations.add(javaAnnotation);
	}

	/**
	 * Derives the {@link JavaModifier} from the given method declaration.
	 */
	private JavaModifier convertModifier(MethodDeclaration methodDeclaration) {
		List<?> modifiers = methodDeclaration.modifiers();
		for (Object modifier : modifiers) {
			if (modifier instanceof IExtendedModifier) {
				IExtendedModifier extendedModifier = (IExtendedModifier) modifier;
				if (extendedModifier.isModifier()) {
					Modifier mod = (Modifier) extendedModifier;
					if (mod.isPublic()) {
						return JavaModifier.PUBLIC;
					} else if (mod.isProtected()) {
						return JavaModifier.PROTECTED;
					} else if (mod.isPrivate()) {
						return JavaModifier.PRIVATE;
					}
				}

			}
		}

		return JavaModifier.PACKAGE_PROTECTED;
	}

	/**
	 * Derives the {@link JavaModifier} from the given method declaration.
	 */
	private JavaModifier convertModifier(IMethod method) throws JavaModelException {
		int flags = method.getFlags();
		if (Flags.isPublic(flags)) {
			return JavaModifier.PUBLIC;
		} else if (Flags.isProtected(flags)) {
			return JavaModifier.PROTECTED;
		} else if (Flags.isPrivate(flags)) {
			return JavaModifier.PRIVATE;
		}

		return JavaModifier.PACKAGE_PROTECTED;
	}

	public List<JavaParameter> convertParameters(IResource resource, MethodDeclaration methodDeclaration) {
		// determine parameter types of the method
		List<?> parameters = methodDeclaration.parameters();
		List<JavaParameter> javaParameters = new ArrayList<JavaParameter>(parameters.size());
		for (Object parameter : parameters) {
			JavaParameter javaParameter = convertParameter(resource, parameter);
			if (javaParameter == null) {
				continue;
			}
			
			javaParameters.add(javaParameter);
		}

		return javaParameters;
	}

	private List<JavaParameter> convertBinaryParameters(IMethod method) throws JavaModelException {

		// determine parameter types of the method
		ILocalVariable[] parameters = method.getParameters();
		List<JavaParameter> javaParameters = new ArrayList<JavaParameter>(parameters.length);
		for (ILocalVariable parameter : parameters) {
			JavaParameter javaParameter = convertBinaryParameter(parameter);
			if (javaParameter == null) {
				continue;
			}
			javaParameters.add(javaParameter);
		}

		return javaParameters;
	}

	private JavaType convertBinaryReturnType(IMethod method) throws JavaModelException {

		// determine return type of method (void is represented as null)
		String returnTypeName = getTypeAnnotationValue(method);
		if (returnTypeName == null) {
			returnTypeName = Signature.toString(method.getReturnType());
		}

		JavaType returnType = null;
		if (returnTypeName != null) {
			if (!void.class.getName().equals(returnTypeName)) {
				IJavaProject javaProject = method.getJavaProject();
				returnType = createJavaType(javaProject, returnTypeName, true);
			}
		}
		
		return returnType;
	}

	private String getTypeAnnotationValue(IMethod method) {
		JavaAnnotation typeAnnotation = getAnnotation(method, de.devboost.natspec.annotations.Type.class);
		if (typeAnnotation == null) {
			return null;
		}

		List<JavaAssignment> assignments = typeAnnotation.getAssignments();
		if (assignments.isEmpty()) {
			return null;
		}

		JavaAssignment javaAssignment = assignments.get(0);
		JavaExpression value = javaAssignment.getValue();
		if (value instanceof JavaStringLiteral) {
			JavaStringLiteral stringLiteral = (JavaStringLiteral) value;
			return stringLiteral.getValue();
		}

		return null;
	}

	private JavaType convertReturnType(MethodDeclaration methodDeclaration) {
		// determine return type of method (void is represented as null)
		Type returnType2 = methodDeclaration.getReturnType2();
		if (returnType2 == null) {
			return null;
		}
		
		ITypeBinding returnTypeBinding = returnType2.resolveBinding();
		if (returnTypeBinding == null) {
			return null;
		}
		
		String qualifiedReturnType = returnTypeBinding.getQualifiedName();
		if (!void.class.getName().equals(qualifiedReturnType)) {
			return createJavaType(returnTypeBinding, true);
		}
		
		return null;
	}

	private JavaParameter convertBinaryParameter(ILocalVariable parameter) throws JavaModelException {
		ILocalVariable localVariable = (ILocalVariable) parameter;
		String parameterName = localVariable.getElementName();
		String typeSignature = localVariable.getTypeSignature();
		String parameterTypeName = Signature.toString(typeSignature);
		IJavaProject javaProject = parameter.getJavaProject();
		JavaType parameterType = createJavaType(javaProject, parameterTypeName, false);
		List<JavaAnnotation> javaAnnotations = getBinaryJavaAnnotations(localVariable);
		return new JavaParameter(parameterName, parameterType, javaAnnotations);
	}

	private JavaParameter convertParameter(IResource resource, Object parameter) {
		if (parameter instanceof SingleVariableDeclaration) {
			SingleVariableDeclaration parameterDeclaration = (SingleVariableDeclaration) parameter;
			IVariableBinding binding = parameterDeclaration.resolveBinding();
			if (binding == null) {
				return null;
			}
			
			ITypeBinding type = binding.getType();
			IJavaElement javaElement = type.getJavaElement();
			SimpleName parameterName = parameterDeclaration.getName();
			String parameterIdentifier = parameterName.getIdentifier();

			List<JavaAnnotation> javaAnnotations = getJavaAnnotation(resource, parameterDeclaration);

			// TODO Figure out if we can merge both branches (they seem to be identical)
			if (javaElement instanceof IType) {
				IType javaType = (IType) javaElement;
				JavaType parameterType = createJavaType(javaType, type);
				return new JavaParameter(parameterIdentifier, parameterType, javaAnnotations);
			} else {
				JavaType parameterType = createJavaType(null, type);
				return new JavaParameter(parameterIdentifier, parameterType, javaAnnotations);
			}
		} else {
			// According to the documentation of
			// MethodDeclaration.parameters() there must not be elements of
			// different types in the list of parameters. Thus, we can
			// safely ignore this case.
			return null;
		}
	}

	private List<JavaAnnotation> getJavaAnnotation(IResource resource,
			SingleVariableDeclaration parameterDeclaration) {

		List<JavaAnnotation> javaAnnotations = new ArrayList<JavaAnnotation>(1);
		List<?> modifiers = parameterDeclaration.modifiers();
		for (Object modifier : modifiers) {
			if (modifier instanceof Annotation) {
				Annotation annotation = (Annotation) modifier;
				JavaAnnotation javaAnnotation = getJavaAnnotation(resource,
						annotation);

				javaAnnotations.add(javaAnnotation);
			}
		}
		return javaAnnotations;
	}

	private JavaAnnotation getJavaAnnotation(IAnnotation annotation) throws JavaModelException {

		List<JavaAssignment> assignments = new ArrayList<JavaAssignment>();

		IMemberValuePair[] memberValuePairs = annotation.getMemberValuePairs();
		for (IMemberValuePair memberValuePair : memberValuePairs) {
			String memberName = memberValuePair.getMemberName();
			Object value = memberValuePair.getValue();
			if (value instanceof String) {
				String stringValue = (String) value;
				assignments.add(new JavaAssignment(memberName, new JavaStringLiteral(stringValue)));
			}
			if (value instanceof Object[]) {
				JavaObjectArray objectArray = new JavaObjectArray();
				Object[] values = (Object[]) value;
				for (Object next : values) {
					if (next instanceof String) {
						String nextString = (String) next;
						objectArray.getContents().add(new JavaStringLiteral(nextString));
					}
				}

				assignments.add(new JavaAssignment(memberName, objectArray));
			}
			// FIXME Handle other kinds of values
		}
		
		String annotationName = annotation.getElementName();
		JavaType javaAnnotationType = new JavaType(annotationName);
		// For annotations in binary files there is not need to use a validation callback as we cannot add error markers
		// to binaries.
		IValidationCallback validationCallback = NullValidationCallback.INSTANCE;
		JavaAnnotation javaAnnotation = new JavaAnnotation(javaAnnotationType, validationCallback);
		javaAnnotation.getAssignments().addAll(assignments);
		return javaAnnotation;
	}

	private List<JavaAnnotation> getBinaryJavaAnnotations(ILocalVariable parameter)
			throws JavaModelException {

		List<JavaAnnotation> javaAnnotations = new ArrayList<JavaAnnotation>(1);
		IAnnotation[] annotations = parameter.getAnnotations();
		for (IAnnotation annotation : annotations) {
			JavaAnnotation javaAnnotation = getJavaAnnotation(annotation);
			if (javaAnnotation != null) {
				javaAnnotations.add(javaAnnotation);
			}
		}
		return javaAnnotations;
	}

	/**
	 * Converts the given JDT annotation to a {@link JavaAnnotation}.
	 * 
	 * @param resource
	 *            the resource (source file) that contains the annotation
	 * @param annotation
	 *            an annotation from a JDT AST
	 * @return the converted {@link JavaAnnotation}
	 */
	private JavaAnnotation getJavaAnnotation(IResource resource,
			Annotation annotation) {

		Name annotationType = annotation.getTypeName();
		String annotationTypeName = annotationType.getFullyQualifiedName();
		JavaType javaAnnotationType = new JavaType(annotationTypeName);
		IValidationCallback validationCallback = newValidationCallback(resource, annotation);
		JavaAnnotation javaAnnotation = new JavaAnnotation(javaAnnotationType, validationCallback);

		if (annotation instanceof SingleMemberAnnotation) {
			SingleMemberAnnotation singleMemberAnnotation = (SingleMemberAnnotation) annotation;
			Expression value = singleMemberAnnotation.getValue();
			String annotationPropertyName = JavaAnnotation.ANNOTATION_PROPERTY_NAME;
			JavaAssignment javaAssignment = getJavaAssignment(value, annotationPropertyName);
			if (javaAnnotation != null) {
				javaAnnotation.getAssignments().add(javaAssignment);
			}
		} else if (annotation instanceof NormalAnnotation) {
			NormalAnnotation normalAnnotation = (NormalAnnotation) annotation;
			List<?> values = normalAnnotation.values();
			for (Object valueObject : values) {
				if (valueObject instanceof MemberValuePair) {
					MemberValuePair memberValuePair = (MemberValuePair) valueObject;
					SimpleName name = memberValuePair.getName();
					Expression value = memberValuePair.getValue();
					JavaAssignment javaAssignment = getJavaAssignment(value, name.getIdentifier());
					if (javaAnnotation != null) {
						javaAnnotation.getAssignments().add(javaAssignment);
					}
				}
			}
		}
		return javaAnnotation;
	}

	/**
	 * Converts the given JDT expression to a {@link JavaAssignment}.
	 * 
	 * @param expression
	 *            an expression from a JDT AST
	 * @param annotationPropertyName
	 *            the target of the assignment
	 * @return the converted {@link JavaAssignment}
	 */
	private JavaAssignment getJavaAssignment(Expression expression,
			String annotationPropertyName) {

		JavaExpression javaExpression = getJavaExpression(expression);
		if (javaExpression == null) {
			return null;
		}

		return new JavaAssignment(annotationPropertyName, javaExpression);
	}

	/**
	 * Converts the given JDT expression to a {@link JavaExpression}.
	 * 
	 * @param expression
	 *            an expression from a JDT AST
	 * @return the converted {@link JavaAssignment} or <code>null</code> if the expression could not be converted
	 */
	private JavaExpression getJavaExpression(Expression expression) {
		JavaExpression javaExpression = null;
		if (expression instanceof ArrayInitializer) {
			JavaObjectArray javaObjectArray = new JavaObjectArray();
			javaExpression = javaObjectArray;
			ArrayInitializer arrayInitializer = (ArrayInitializer) expression;
			List<?> expressions = arrayInitializer.expressions();
			for (Object expressionObject : expressions) {
				if (expressionObject instanceof StringLiteral) {
					StringLiteral stringLiteral = (StringLiteral) expressionObject;
					String literalValue = stringLiteral.getLiteralValue();
					javaObjectArray.getContents().add(new JavaStringLiteral(literalValue));
				}
			}
		}

		return javaExpression;
	}

	private void convertAnnotations(MethodDeclaration methodDeclaration,
			JavaMethod javaMethod) {

		for (Class<?> methodAnnotation : METHOD_ANNOTATIONS) {
			getTextSyntaxValue(methodDeclaration, javaMethod, methodAnnotation);
		}
	}

	private void convertBinaryAnnotations(IMethod method,
			JavaMethod javaMethod) {

		for (Class<?> methodAnnotation : METHOD_ANNOTATIONS) {
			getBinaryAnnotationValue(method, javaMethod, methodAnnotation);
		}
	}

	private void getBinaryAnnotationValue(IMethod method,
			JavaMethod javaMethod, Class<?> annotationClass) {

		JavaAnnotation javaAnnotation = getAnnotation(method, annotationClass);
		if (javaAnnotation != null) {
			List<JavaAnnotation> javaAnnotations = javaMethod.getAnnotations();
			javaAnnotations.add(javaAnnotation);
		}
	}

	private void getTextSyntaxValue(MethodDeclaration methodDeclaration,
			JavaMethod javaMethod, Class<?> annotationClass) {

		IMethodBinding resolvedBinding = methodDeclaration.resolveBinding();
		if (resolvedBinding == null) {
			return;
		}

		IMethod method = (IMethod) resolvedBinding.getJavaElement();

		JavaAnnotation javaAnnotation = getAnnotation(method, annotationClass);
		if (javaAnnotation != null) {
			List<JavaAnnotation> javaAnnotations = javaMethod.getAnnotations();
			javaAnnotations.add(javaAnnotation);
		}
	}

	private JavaAnnotation getAnnotation(IAnnotatable annotatable,
			Class<?> annotationClass) {

		// Normally, we should return all annotations, but for performance
		// reasons, we only return the ones which are processed by NatSpec.
		String annotationClassName = annotationClass.getSimpleName();
		String annotationClassFullName = annotationClass.getName();
		try {
			// We must not use method.getAnnotation(String) because this method
			// returns annotations that do not exist anymore!
			IAnnotation[] annotations = annotatable.getAnnotations();
			for (IAnnotation annotation : annotations) {
				if (annotation == null) {
					continue;
				}
				String annotationName = annotation.getElementName();
				if (annotationClassName.equals(annotationName) ||
						annotationClassFullName.equals(annotationName)) {
					return createAnnotation(annotation);
				}
			}
		} catch (JavaModelException e) {
			// ignore
		}
		return null;
	}

	private JavaAnnotation createAnnotation(IAnnotation annotation) {
		if (annotation == null) {
			return null;
		}

		try {
			// This is required to check whether the annotation exists
			annotation.getMemberValuePairs();
		} catch (JavaModelException e) {
			return null;
		}

		String annotationTypeName = annotation.getElementName();
		IValidationCallback callback = newValidationCallback(annotation);
		JavaType annotationType = new JavaType(annotationTypeName);
		JavaAnnotation javaAnnotation = new JavaAnnotation(annotationType, callback);
		addAnnotationPropertyValues(annotation, javaAnnotation);
		return javaAnnotation;
	}

	private IValidationCallback newValidationCallback(IResource resource, Annotation annotation) {
		int offset = annotation.getStartPosition();
		int length = annotation.getLength();
		return new JDTValidationResultCallback(resource, offset, length);
	}

	private IValidationCallback newValidationCallback(IAnnotation annotation) {

		IResource resource = annotation.getResource();
		ISourceRange sourceRange = null;
		try {
			sourceRange = annotation.getSourceRange();
		} catch (JavaModelException e) {
			// ignore
		}

		int offset;
		int length;
		if (sourceRange == null) {
			offset = -1;
			length = -1;
		} else {
			offset = sourceRange.getOffset();
			length = sourceRange.getLength();
		}
		return new JDTValidationResultCallback(resource, offset, length);
	}

	private void addAnnotationPropertyValues(IAnnotation annotation,
			JavaAnnotation javaAnnotation) {
		try {
			IMemberValuePair[] memberValuePairs = annotation
					.getMemberValuePairs();
			for (IMemberValuePair memberValuePair : memberValuePairs) {
				String key = memberValuePair.getMemberName();
				Object value = memberValuePair.getValue();
				if (value == null) {
					continue;
				}
				if (value instanceof String) {
					String stringValue = (String) value;
					JavaAssignment assignment = new JavaAssignment(key,
							new JavaStringLiteral(stringValue));
					javaAnnotation.getAssignments().add(assignment);
				}
				if (value instanceof Object[]) {
					JavaObjectArray objectArray = new JavaObjectArray();
					Object[] values = (Object[]) value;
					if (values != null) {
						for (Object next : values) {
							if (next instanceof IAnnotation) {
								IAnnotation nextAnnotation = (IAnnotation) next;
								IValidationCallback callback = newValidationCallback(annotation);
								String annotationTypeName = nextAnnotation.getElementName();
								JavaType annotationType = new JavaType(annotationTypeName);
								JavaAnnotation subAnnotation = new JavaAnnotation(
										annotationType, callback);
								addAnnotationPropertyValues(nextAnnotation,
										subAnnotation);
								objectArray.getContents().add(subAnnotation);
							}
						}
					}

					JavaAssignment assignment = new JavaAssignment(key,
							objectArray);
					javaAnnotation.getAssignments().add(assignment);
				}
			}
		} catch (JavaModelException e) {
			// ignore
		}
	}

	private JavaType createJavaType(ITypeBinding typeBinding, boolean includeSuperTypes) {

		String qualifiedName = typeBinding.getQualifiedName();
		qualifiedName = NameHelper.INSTANCE.getNameWithoutTypeArguments(qualifiedName);

		List<JavaType> javaSuperTypes = new ArrayList<JavaType>();
		if (includeSuperTypes) {
			IJavaElement javaElement = typeBinding.getJavaElement();
			if (javaElement instanceof IType) {
				IType type = (IType) javaElement;
				javaSuperTypes = getSuperTypes(type, includeSuperTypes);
			}
		}

		ITypeBinding[] typeArguments = typeBinding.getTypeArguments();
		return new JavaType(qualifiedName, createJavaTypes(typeArguments), javaSuperTypes);
	}

	private JavaType createJavaType(IJavaProject javaProject,
			String qualifiedName, boolean includeSuperTypes)
			throws JavaModelException {

		String qualifiedNameWithoutTypeArguments = NameHelper.INSTANCE
				.getNameWithoutTypeArguments(qualifiedName);

		List<JavaType> javaSuperTypes = new ArrayList<JavaType>();
		if (includeSuperTypes) {
			IType type = javaProject.findType(qualifiedNameWithoutTypeArguments);
			if (type != null) {
				javaSuperTypes = getSuperTypes(type, includeSuperTypes);
			}
		}

		List<String> typeArguments = NameHelper.INSTANCE
				.getTypeArguments(qualifiedName);
		List<JavaType> javaTypeArguments = createJavaTypes(javaProject, typeArguments);
		return new JavaType(qualifiedNameWithoutTypeArguments, javaTypeArguments,
				javaSuperTypes);
	}

	private JavaType createJavaType(IType type, boolean includeSuperTypes) {
		List<JavaType> javaSuperTypes = getSuperTypes(type, includeSuperTypes);
		String qualifiedName = type.getFullyQualifiedName();
		boolean isEnum;
		try {
			isEnum = type.isEnum();
		} catch (JavaModelException e) {
			return null;
		}
		
		if (isEnum) {
			return createJavaEnum(type, qualifiedName);
		} else {
			return new JavaType(qualifiedName, Collections.<JavaType> emptyList(), javaSuperTypes);
		}
	}

	private JavaType createJavaEnum(IType type, String qualifiedName) {
		IField[] fields;
		try {
			fields = type.getFields();
		} catch (JavaModelException e) {
			return null;
		}
		
		Set<String> literals = new LinkedHashSet<>();
		for (IField field : fields) {
			try {
				if (field.isEnumConstant()) {
					literals.add(field.getElementName());
				}
			} catch (JavaModelException e) {
				continue;
			}
		}
		return new JavaEnum(qualifiedName, literals);
	}

	private List<JavaType> getSuperTypes(IType type, boolean includeSuperTypes) {
		JDTJavaConnectorPlugin plugin = JDTJavaConnectorPlugin.getInstance();
		if (plugin == null) {
			return Collections.emptyList();
		}

		JDTTypeHierarchyCache typeHierarchyCache = plugin.getTypeHierarchyCache();
		List<JavaType> javaSuperTypes = new ArrayList<JavaType>();
		try {
			Set<IType> superTypes = typeHierarchyCache.getSuperTypes(type);
			for (IType superType : superTypes) {
				javaSuperTypes
						.add(createJavaType(superType, includeSuperTypes));
			}
		} catch (JavaModelException e) {
			// ignore
		}
		return javaSuperTypes;
	}

	private List<JavaType> createJavaTypes(ITypeBinding[] typeBindings) {
		List<JavaType> types = new ArrayList<JavaType>(typeBindings.length);
		for (ITypeBinding typeBinding : typeBindings) {
			types.add(createJavaType(null, typeBinding));
		}
		return types;
	}

	private List<JavaType> createJavaTypes(IJavaProject javaProject,
			List<String> typeNames) throws JavaModelException {

		List<JavaType> types = new ArrayList<JavaType>(typeNames.size());
		for (String typeName : typeNames) {
			types.add(createJavaType(javaProject, typeName, false));
		}
		return types;
	}

	private JavaType createJavaType(IType type, ITypeBinding typeBinding) {
		String qualifiedName = typeBinding.getQualifiedName();
		// Remove type arguments from name
		qualifiedName = NameHelper.INSTANCE.getNameWithoutTypeArguments(qualifiedName);
		ITypeBinding[] typeArguments = typeBinding.getTypeArguments();
		if (typeBinding.isEnum()) {
			if (type == null) {
				return null;
			} else {
				return createJavaEnum(type, qualifiedName);
			}
		} else {
			return new JavaType(qualifiedName, createJavaTypes(typeArguments));
		}
	}

	private boolean hasTextSyntaxAnnotations(IType type) {
		IMethod[] methods;
		try {
			methods = type.getMethods();
		} catch (JavaModelException e) {
			return false;
		}

		for (IMethod method : methods) {
			if (getTextSyntaxAnnotation(method) != null) {
				return true;
			}
			if (getTextSyntaxesAnnotation(method) != null) {
				return true;
			}
		}
		return false;
	}

	private boolean hasParameterSyntaxAnnotations(IType type) {
		IMethod[] methods;
		try {
			methods = type.getMethods();
		} catch (JavaModelException e) {
			return false;
		}

		for (IMethod method : methods) {
			if (getParameterSyntaxAnnotation(method) != null) {
				return true;
			}
		}
		return false;
	}

	private boolean hasStyledSyntaxAnnotations(IType type) {
		IMethod[] methods;
		try {
			methods = type.getMethods();
		} catch (JavaModelException e) {
			return false;
		}

		for (IMethod method : methods) {
			if (getStyledTextSyntaxAnnotation(method) != null) {
				return true;
			}
		}
		return false;
	}

	private boolean hasNameBasedSyntaxMethodAnnotations(IType type) {
		IMethod[] methods;
		try {
			methods = type.getMethods();
		} catch (JavaModelException e) {
			return false;
		}

		for (IMethod method : methods) {
			if (getNameBasedSyntaxAnnotation(method) != null) {
				return true;
			}
		}
		return false;
	}

	private boolean hasRelevantTypeAnnotations(IType type) {
		if (getNameBasedSyntaxAnnotation(type) != null) {
			return true;
		}
		if (getSyntaxRulesAnnotation(type) != null) {
			return true;
		}
		if (getAllowedSyntaxPatternAnnotation(type) != null) {
			return true;
		}
		return false;
	}

	private JavaAnnotation getTextSyntaxAnnotation(IMethod method) {
		return getAnnotation(method, TextSyntax.class);
	}

	private JavaAnnotation getStyledTextSyntaxAnnotation(IMethod method) {
		return getAnnotation(method, StyledTextSyntax.class);
	}
	
	private JavaAnnotation getParameterSyntaxAnnotation(IMethod method) {
		return getAnnotation(method, ParameterSyntax.class);
	}

	private JavaAnnotation getTextSyntaxesAnnotation(IMethod method) {
		return getAnnotation(method, TextSyntaxes.class);
	}

	private JavaAnnotation getNameBasedSyntaxAnnotation(IType type) {
		return getAnnotation(type, NameBasedSyntax.class);
	}

	private JavaAnnotation getSyntaxRulesAnnotation(IType type) {
		return getAnnotation(type, AllowedSyntaxPatterns.class);
	}

	private JavaAnnotation getAllowedSyntaxPatternAnnotation(IType type) {
		return getAnnotation(type, AllowedSyntaxPattern.class);
	}

	private JavaAnnotation getNameBasedSyntaxAnnotation(IMethod method) {
		return getAnnotation(method, NameBasedSyntax.class);
	}
}
