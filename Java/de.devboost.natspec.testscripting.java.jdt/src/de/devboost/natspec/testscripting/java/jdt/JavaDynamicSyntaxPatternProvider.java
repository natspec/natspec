package de.devboost.natspec.testscripting.java.jdt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaModelException;

import de.devboost.eclipse.jdtutilities.JDTUtility;
import de.devboost.natspec.annotations.NameBasedSyntax;
import de.devboost.natspec.annotations.StyledTextSyntax;
import de.devboost.natspec.annotations.TextSyntax;
import de.devboost.natspec.annotations.TextSyntaxes;
import de.devboost.natspec.patterns.ISyntaxPattern;
import de.devboost.natspec.testscripting.IFieldCache;
import de.devboost.natspec.testscripting.NameHelper;
import de.devboost.natspec.testscripting.java.JavaTemplateFileFinder;
import de.devboost.natspec.testscripting.java.JavaTestScriptingPlugin;
import de.devboost.natspec.testscripting.java.extract.JavaModelPatternExtractor;
import de.devboost.natspec.testscripting.java.model.JavaClassifier;
import de.devboost.natspec.testscripting.patterns.AbstractDynamicSyntaxPatternProvider;
import de.devboost.natspec.testscripting.patterns.IValidationCallback;
import de.devboost.natspec.testscripting.patterns.NullValidationCallback;

/**
 * The {@link JavaDynamicSyntaxPatternProvider} is used to provide patterns which are derived from methods that use the
 * {@link TextSyntax}, the {@link TextSyntaxes}, {@link NameBasedSyntax} or the {@link StyledTextSyntax} annotation.
 * These patterns are passed to the {@link JavaDynamicSyntaxPatternProvider} by the
 * {@link DynamicConfigurationCompilationParticipant} which uses the {@link JDTJavaModelExtractor} to analyze compiled
 * classes.
 */
public class JavaDynamicSyntaxPatternProvider extends AbstractDynamicSyntaxPatternProvider {

	private final JDTFieldTypeCache fieldTypeCache = new JDTFieldTypeCache();

	@Override
	protected List<ISyntaxPattern<?>> doGetPatterns(IFile natspecFile) {
		IFile templateFile = JavaTemplateFileFinder.INSTANCE.findTemplateFile(natspecFile);
		Collection<String> fieldTypes = fieldTypeCache.getAllFieldTypes(templateFile);
		List<ISyntaxPattern<?>> allPatterns = new ArrayList<ISyntaxPattern<?>>();
		for (String fieldType : fieldTypes) {
			doGetPatterns(natspecFile, allPatterns, fieldType);
		}
		return allPatterns;
	}

	private void doGetPatterns(IFile natspecFile, List<ISyntaxPattern<?>> allPatterns, String fieldType) {

		Map<String, List<ISyntaxPattern<?>>> supportClassToPatternsMap = getTestSupportClassToPatternsMap();
		fieldType = NameHelper.INSTANCE.getNameWithoutTypeArguments(fieldType);
		List<ISyntaxPattern<?>> patterns = supportClassToPatternsMap.get(fieldType);
		if (patterns != null) {
			// Found source type for the field
			allPatterns.addAll(patterns);
		} else {
			// No patterns in testSupportClassToPatternsMap. Maybe the fields type is a binary type? Let's try that.
			patterns = getBinaryPatterns(natspecFile, fieldType);
			if (patterns != null) {
				// Save patterns is cache to avoid extracting them every time.
				supportClassToPatternsMap.put(fieldType, patterns);
				allPatterns.addAll(patterns);
			}
		}
	}

	private List<ISyntaxPattern<?>> getBinaryPatterns(IFile natspecFile, String qualifiedClassname) {
		JDTUtility jdtUtility = ConcreteJDTUtility.INSTANCE;
		IJavaProject javaProject = jdtUtility.getJavaProject(natspecFile);
		if (javaProject == null) {
			return null;
		}

		try {
			IType type = javaProject.findType(qualifiedClassname);
			if (type == null) {
				return null;
			}

			if (type.isBinary()) {
				String projectName = javaProject.getProject().getName();
				return getBinaryPatterns(type, projectName);
			}
		} catch (JavaModelException jme) {
			JavaTestScriptingPlugin.logWarning("Exception while getting patterns from binary type", jme);
		}

		return null;
	}

	private List<ISyntaxPattern<?>> getBinaryPatterns(IType type, String projectName) throws JavaModelException {
		String qualifiedName = type.getFullyQualifiedName();
		boolean isInterface = type.isInterface();
		IValidationCallback validationCallback = NullValidationCallback.INSTANCE;
		JavaClassifier javaClassifier = new JavaClassifier(qualifiedName, projectName, validationCallback, isInterface);

		JDTJavaModelExtractor jdtJavaModelExtractor = new JDTJavaModelExtractor();
		// Extract Java model
		jdtJavaModelExtractor.findMethods(type, javaClassifier);
		JavaModelPatternExtractor patternExtractor = JavaModelPatternExtractor.INSTANCE;
		List<ISyntaxPattern<?>> patterns = patternExtractor.extractPatterns(javaClassifier);
		return patterns;
	}

	public IFieldCache getFieldTypeCache() {
		return fieldTypeCache;
	}
}
