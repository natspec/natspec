package de.devboost.natspec.testscripting.java.jdt;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaModelStatus;
import org.eclipse.jdt.core.IJavaModelStatusConstants;
import org.eclipse.jdt.core.JavaModelException;

import de.devboost.eclipse.jdtutilities.JDTUtility;

public class ConcreteJDTUtility extends JDTUtility {
	
	public final static ConcreteJDTUtility INSTANCE = new ConcreteJDTUtility();
	
	private ConcreteJDTUtility() {
		super();
	}

	@Override
	protected void logWarning(String message, Exception e) {
		JDTJavaConnectorPlugin.logWarning(message, e);
	}

	/**
	 * Returns the resource that corresponds to the given compilation unit or
	 * <code>null</code> if the resource cannot be determined.
	 * 
	 * @param compilationUnit
	 *            the compilation unit for which the resource shall be found
	 * @return the resource that contains the compilation unit or
	 *         <code>null</code>
	 */
	public IResource getCorrespondingResource(ICompilationUnit compilationUnit) {

		try {
			return compilationUnit.getCorrespondingResource();
		} catch (JavaModelException e) {
			IStatus status = e.getStatus();
			if (status instanceof IJavaModelStatus) {
				IJavaModelStatus javaModelStatus = (IJavaModelStatus) status;
				int code = javaModelStatus.getCode();
				if (code == IJavaModelStatusConstants.ELEMENT_DOES_NOT_EXIST) {
					// We ignore it if an element does not exist
					return null;
				}
			}

			JDTJavaConnectorPlugin.logError(
					"Exception while processing Java class.", e);
			return null;
		}
	}
}
