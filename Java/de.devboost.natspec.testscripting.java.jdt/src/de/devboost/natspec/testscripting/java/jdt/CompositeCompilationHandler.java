package de.devboost.natspec.testscripting.java.jdt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.print.attribute.TextSyntax;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;

import de.devboost.natspec.patterns.ISyntaxPatternProvider;
import de.devboost.natspec.testscripting.IFileProcessor;
import de.devboost.natspec.testscripting.IPatternRegisterCallback;
import de.devboost.natspec.testscripting.TestConnectorPlugin;

/**
 * The {@link CompositeCompilationHandler} can be used to run multiple extractors (processors) when a file was compiled
 * by the JDT. It serves as a hub to handle compilation events.
 */
public class CompositeCompilationHandler {

	private static final String CACHE_FILE_WRITE_EXCEPTION_MESSAGE = "Exception while writing cache file.";

	private static final String CACHE_FILE_READ_EXCEPTION_MESSAGE = "Exception while reading cache file.";

	private final PluginPatternRegisterCallback callback = new PluginPatternRegisterCallback();

	private final ProviderCompilationHandler providerCompilationHandler = new ProviderCompilationHandler();

	/**
	 * A set of paths of all files which contain syntax patterns (either by having methods with {@link TextSyntax}
	 * annotations or by implementing {@link ISyntaxPatternProvider}).
	 */
	private Set<String> patternProviderPaths = new LinkedHashSet<String>();

	/**
	 * Process the given list of files using all compilation handlers.
	 * 
	 * @param files
	 *            the files to process
	 */
	public void process(List<IFile> files) {
		process(files, callback, new NullProgressMonitor());
	}

	private void process(List<IFile> files, IPatternRegisterCallback callback, IProgressMonitor monitor) {
		// In this list we collect all files where one of the compilation handlers detected syntax patterns
		Set<IFile> filesContainingPatterns = new LinkedHashSet<>();

		// The ProviderCompilationHandler must be reused as it carries state
		runFileProcessor(files, providerCompilationHandler, monitor, filesContainingPatterns);

		// The JDTSyntaxPatternExtractor can be recreated as it is state-less
		runFileProcessor(files, new JDTSyntaxPatternExtractor(callback), monitor, filesContainingPatterns);

		// After running all compilation handlers, we update the set of paths of all files the provide syntax patterns
		updatePaths(files, filesContainingPatterns);
	}

	/**
	 * Runs the given processor on all files.
	 * 
	 * @param monitor
	 *            a progress monitor (used to handle cancellation)
	 */
	private void runFileProcessor(List<IFile> files, IFileProcessor processor, IProgressMonitor monitor,
			Set<IFile> filesContainingPatterns) {
		
		filesContainingPatterns.addAll(processor.process(files, monitor));
	}

	private void updatePaths(List<IFile> filesToProcess, Set<IFile> filesContainingPatterns) {
		for (IFile file : filesToProcess) {
			IPath fullPath = file.getFullPath();
			String fullPathString = fullPath.toString();
			if (filesContainingPatterns.contains(file)) {
				// If a processed file does provide syntax patterns, we add its path to the list of paths
				patternProviderPaths.add(fullPathString);
			} else {
				// If a processed file does not provide syntax patterns anymore, we remove it
				patternProviderPaths.remove(fullPathString);
			}
		}
	}

	/**
	 * Sets this class up. To do so, a list of paths of files containing syntax patterns is restored from a metadata
	 * file. This is required to analyze these classes again after Eclipse was restarted. The analysis is required to
	 * make the syntax patterns contained in the files available in the current session. Without this procedure, one
	 * would need to compile all classes containing syntax patterns after restarting Eclipse to make the patterns
	 * available.
	 */
	public void setUp(IProgressMonitor monitor) {
		File cacheFile = getCacheFile();
		if (cacheFile == null || !cacheFile.exists()) {
			JDTJavaConnectorPlugin.logInfo("No cache file containing syntax pattern providing classes found.", null);
			// no cache available
			return;
		}

		List<IFile> files = readCacheFile(cacheFile);
		
		// During setup we aggregate all pattern registrations and write them at once after all the pattern providing
		// classes have been analyzed. This is done to avoid multiple revalidation of NatSpec scripts when Eclipse is
		// started.
		AggregatingPatternRegisterCallback aggregatingCallback = new AggregatingPatternRegisterCallback();
		process(files, aggregatingCallback, monitor);
		aggregatingCallback.flush(callback);
	}

	/**
	 * Reads the list of files that provide syntax patterns from the cache file (where this list is persisted).
	 */
	private List<IFile> readCacheFile(File cacheFile) {
		List<IFile> files = new ArrayList<IFile>();
		try {
			FileInputStream fileInputStream = new FileInputStream(cacheFile);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			Object root = objectInputStream.readObject();
			objectInputStream.close();
			if (root instanceof List<?>) {
				List<?> list = (List<?>) root;
				for (Object next : list) {
					if (next instanceof String) {
						String path = (String) next;
						IWorkspace workspace = ResourcesPlugin.getWorkspace();
						IWorkspaceRoot workspaceRoot = workspace.getRoot();
						IFile file = workspaceRoot.getFile(new Path(path));
						files.add(file);
					}
				}
			}
		} catch (IOException e) {
			TestConnectorPlugin.logError(CACHE_FILE_READ_EXCEPTION_MESSAGE, e);
		} catch (ClassNotFoundException e) {
			TestConnectorPlugin.logError(CACHE_FILE_READ_EXCEPTION_MESSAGE, e);
		}

		JDTJavaConnectorPlugin.logInfo("Cache file contained following classes: " + files, null);
		return files;
	}

	/**
	 * Serializes the list of files containing syntax patterns and saves their paths to a file. See {@link #setUp()} for
	 * further information why this is required.
	 */
	public void tearDown() {
		List<String> pathList = new ArrayList<String>(patternProviderPaths);

		File cacheFile = getCacheFile();
		if (cacheFile == null) {
			return;
		}

		writeCacheFile(pathList, cacheFile);
	}

	/**
	 * Writes the list of files that provide syntax patterns to the cache file (where this list is persisted).
	 */
	private void writeCacheFile(List<String> paths, File cacheFile) {
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(cacheFile);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
			objectOutputStream.writeObject(paths);
			objectOutputStream.close();
		} catch (IOException e) {
			TestConnectorPlugin.logWarning(CACHE_FILE_WRITE_EXCEPTION_MESSAGE, e);
		}
	}

	/**
	 * Returns a cache file located in the workspace metadata folder. This method can return <code>null</code> if
	 * Eclipse is not running or about to shut down.
	 */
	private File getCacheFile() {
		TestConnectorPlugin plugin = TestConnectorPlugin.getInstance();
		if (plugin == null) {
			JDTJavaConnectorPlugin.logError("Can't determine cache file because TestConnectorPlugin is null.", null);
			return null;
		}

		IPath stateLocation = plugin.getStateLocation();
		File stateDirectory = stateLocation.toFile();
		if (!stateDirectory.exists()) {
			stateDirectory.mkdirs();
		}

		String cacheFileName = getClass().getSimpleName() + ".cache";
		File cacheFile = new File(stateDirectory, cacheFileName);
		return cacheFile;
	}
}
