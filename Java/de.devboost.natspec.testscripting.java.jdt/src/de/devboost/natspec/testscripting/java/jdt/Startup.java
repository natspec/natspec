package de.devboost.natspec.testscripting.java.jdt;

import org.eclipse.ui.IStartup;

/**
 * This class runs when Eclipse is started and performs a clean build (if required).
 */
public class Startup implements IStartup {

	@Override
	public void earlyStartup() {
		new WorkspaceCleaner().performCleanBuildIfRequired();
	}
}
